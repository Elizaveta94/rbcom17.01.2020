<?
$_max_value_room = 50; // максимальная площадь комнаты
$_max_value_square_commer = 80; // максимальная площадь коммерческого помещения
$_max_value_country = 250; // максимальная площадь загородной недвижимости
$_max_value_country_land = 20; // максимальная площадь участка в сотках
$_max_value_rooms_commer = 5; // максимальное кол-во комнат коммерческого помещения
$_min_value_square_full = 1; // максимальная общая площадь
$_max_value_square_full = 150; // максимальная общая площадь
$_max_value_square_live = 60; // максимальная жилая площадь
$_max_value_delivery = 2019; // максимальный год сдачи квартир
$_min_value_square_kitchen = 8; // максимальная площадь кухни
$_max_value_square_kitchen = 30; // максимальная площадь кухни
$_max_value_floor = 30; // максимальное количество этажей в доме
$_max_value_floor_commer = 30; // максимальное количество этажей в здании ком.помещения

/*
*  Цены на типы недвижимостей
*/

/*
*  Новостройки
*/
$start2 = microtime(true);
$res = mysql_query("
	SELECT 
		MIN(price) AS min_price, 
		MAX(price) AS max_price, 
		MIN(full_square) AS min_full_square, 
		MAX(full_square) AS max_full_square,
		MIN(kitchen_square) AS min_kitchen_square, 
		MAX(kitchen_square) AS max_kitchen_square,
		(SELECT MIN(floors) FROM ".$template."_m_catalogue_left WHERE activation='1') AS min_floors, 
		(SELECT MAX(floors) FROM ".$template."_m_catalogue_left WHERE activation='1') AS max_floors
	FROM ".$template."_new_flats
	WHERE activation='1'
");
if(mysql_num_rows($res)>0){//_count_params
	$row = mysql_fetch_assoc($res);
	$min_price_new_sell = $row['min_price'];
	$max_price_new_sell = $row['max_price'];
	$min_full_square_new_sell = $row['min_full_square'];
	$max_full_square_new_sell = $row['max_full_square'];
	$min_kitchen_square_new_sell = $row['min_kitchen_square'];
	$max_kitchen_square_new_sell = $row['max_kitchen_square'];
	$min_floors_new_sell = $row['min_floors'];
	$max_floors_new_sell = $row['max_floors'];
	
	$min_full_square_new_sell = floor($min_full_square_new_sell);
	$max_full_square_new_sell = ceil($max_full_square_new_sell);
	
	if($min_full_square_new_sell==0){
		$min_full_square_new_sell = 1;
	}
	
	if($max_full_square_new_sell==0){
		$max_full_square_new_sell = 1;
	}
	$min_meter_price_new_sell = floor($min_price_new_sell/$min_full_square_new_sell);
	$max_meter_price_new_sell = ceil($max_price_new_sell/$max_full_square_new_sell);
	
	$min_meter_price_new_sell = floor($min_meter_price_new_sell/1000);
	$max_meter_price_new_sell = ceil($max_meter_price_new_sell/1000);

	if($min_price_new_sell<1000000){
		$min_price_new_sell = floor($min_price_new_sell/100000);
		$min_price_new_sell = $min_price_new_sell/10;
	}
	else {
		$min_price_new_sell = floor($min_price_new_sell/1000000);
	}
	$max_price_new_sell = ceil($max_price_new_sell/1000000);
	
	$_min_value_square_full = $min_full_square_new_sell;
	$_min_value_square_full = 1;
	$_max_value_square_full = $max_full_square_new_sell;
	
	$min_kitchen_square_new_sell = floor($min_kitchen_square_new_sell);
	$min_kitchen_square_new_sell = 1;
	$max_kitchen_square_new_sell = ceil($max_kitchen_square_new_sell);
}

/*
*  Переуступки
*/
$start2 = microtime(true);
$res = mysql_query("
	SELECT 
		MIN(price) AS min_price, 
		MAX(price) AS max_price, 
		MIN(full_square) AS min_full_square, 
		MAX(full_square) AS max_full_square,
		MIN(kitchen_square) AS min_kitchen_square, 
		MAX(kitchen_square) AS max_kitchen_square,
		(SELECT MIN(floors) FROM ".$template."_m_catalogue_left WHERE activation='1') AS min_floors, 
		(SELECT MAX(floors) FROM ".$template."_m_catalogue_left WHERE activation='1') AS max_floors
	FROM ".$template."_cessions
	WHERE activation='1' && price!='0' && how_buy!='Бронь'
");
if(mysql_num_rows($res)>0){//_count_params
	$row = mysql_fetch_assoc($res);
	$min_price_cession_sell = $row['min_price'];
	$max_price_cession_sell = $row['max_price'];
	$min_full_square_cession_sell = $row['min_full_square'];
	$max_full_square_cession_sell = $row['max_full_square'];
	$min_kitchen_square_cession_sell = $row['min_kitchen_square'];
	$max_kitchen_square_cession_sell = $row['max_kitchen_square'];
	$min_floors_cession_sell = $row['min_floors'];
	$max_floors_cession_sell = $row['max_floors'];
	
	$min_full_square_cession_sell = floor($min_full_square_cession_sell);
	$max_full_square_cession_sell = ceil($max_full_square_cession_sell);
	
	if($min_full_square_cession_sell==0){
		$min_full_square_cession_sell = 1;
	}
	
	if($max_full_square_cession_sell==0){
		$max_full_square_cession_sell = 1;
	}
	$min_meter_price_cession_sell = floor($min_price_cession_sell/$min_full_square_cession_sell);
	$max_meter_price_cession_sell = ceil($max_price_cession_sell/$max_full_square_cession_sell);
	
	$min_meter_price_cession_sell = floor($min_meter_price_cession_sell/1000);
	$max_meter_price_cession_sell = ceil($max_meter_price_cession_sell/1000);

	$min_price_cession_sell = floor($min_price_cession_sell/1000000);
	$max_price_cession_sell = ceil($max_price_cession_sell/1000000);
	
	$_min_value_square_full_cession = $min_full_square_cession_sell;
	$_min_value_square_full_cession = 1;
	$_max_value_square_full_cession = $max_full_square_cession_sell;
	
	$min_kitchen_square_cession_sell = floor($min_kitchen_square_cession_sell);
	$min_kitchen_square_cession_sell = 1;
	$max_kitchen_square_cession_sell = ceil($max_kitchen_square_cession_sell);
}

/*
*  Квартиры
*/
$res = mysql_query("
	SELECT 
		MIN(price) AS min_price, 
		MAX(price) AS max_price, 
		MIN(full_square) AS min_full_square, 
		MAX(full_square) AS max_full_square,
		MIN(price/full_square) AS min_full_square_meter, 
		MAX(price/full_square) AS max_full_square_meter,
		MIN(kitchen_square) AS min_kitchen_square, 
		MAX(kitchen_square) AS max_kitchen_square,
		MIN(live_square) AS min_live_square, 
		MAX(live_square) AS max_live_square,
		MIN(floors) AS min_floors, 
		MAX(floors) AS max_floors
	FROM ".$template."_second_flats
	WHERE activation='1' && type='sell'
");
if(mysql_num_rows($res)>0){//_count_params
	$row = mysql_fetch_assoc($res);
	$min_price_flat_sell = $row['min_price'];
	$max_price_flat_sell = $row['max_price'];
	$min_full_square_flat_sell = $row['min_full_square'];
	$max_full_square_flat_sell = $row['max_full_square'];
	$min_full_square_meter = $row['min_full_square_meter'];
	$max_full_square_meter = $row['max_full_square_meter'];
	$min_kitchen_square_flat_sell = $row['min_kitchen_square'];
	$max_kitchen_square_flat_sell = $row['max_kitchen_square'];
	$min_live_square_flat_sell = $row['min_live_square'];
	$max_live_square_flat_sell = $row['max_live_square'];
	$min_floors_flat_sell = $row['min_floors'];
	$max_floors_flat_sell = $row['max_floors'];

	$min_meter_price_flat_sell = floor($min_full_square_meter);
	$max_meter_price_flat_sell = ceil($max_full_square_meter);
	
	$min_meter_price_flat_sell = floor($min_meter_price_flat_sell/1000);
	$max_meter_price_flat_sell = ceil($max_meter_price_flat_sell/1000);
	
	$min_price_flat_sell = floor($min_price_flat_sell/1000000);
	$max_price_flat_sell = ceil($max_price_flat_sell/1000000);
	
	$min_full_square_flat_sell = floor($min_full_square_flat_sell);
	$min_full_square_flat_sell = 1;
	$max_full_square_flat_sell = ceil($max_full_square_flat_sell);

	$min_kitchen_square_flat_sell = floor($min_kitchen_square_flat_sell);
	$min_kitchen_square_flat_sell = 1;
	$max_kitchen_square_flat_sell = ceil($max_kitchen_square_flat_sell);

	$min_live_square_flat_sell = floor($min_live_square_flat_sell);
	$min_live_square_flat_sell = 1;
	$max_live_square_flat_sell = ceil($max_live_square_flat_sell);
}
$res = mysql_query("
	SELECT 
		MIN(price) AS min_price, 
		MAX(price) AS max_price, 
		MIN(full_square) AS min_full_square, 
		MAX(full_square) AS max_full_square,
		MIN(kitchen_square) AS min_kitchen_square, 
		MAX(kitchen_square) AS max_kitchen_square,
		MIN(live_square) AS min_live_square, 
		MAX(live_square) AS max_live_square,
		MIN(floors) AS min_floors, 
		MAX(floors) AS max_floors
	FROM ".$template."_second_flats
	WHERE activation='1' && type='rent'
");
if(mysql_num_rows($res)>0){//_count_params
	$row = mysql_fetch_assoc($res);
	$min_price_flat_rent = $row['min_price'];
	$max_price_flat_rent = $row['max_price'];

	$min_full_square_flat_rent = floor($row['min_full_square']);
	$min_full_square_flat_rent = 1;
	$max_full_square_flat_rent = ceil($row['max_full_square']);

	$min_kitchen_square_flat_rent = floor($row['min_kitchen_square']);
	$min_kitchen_square_flat_rent = 1;
	$max_kitchen_square_flat_rent = ceil($row['max_kitchen_square']);

	$min_live_square_flat_rent = floor($row['min_live_square']);
	$min_live_square_flat_rent = 1;
	$max_live_square_flat_rent = ceil($row['min_live_square']);

	$min_floors_flat_rent = $row['min_floors'];
	$max_floors_flat_rent = $row['max_floors'];

	$min_price_flat_rent = floor($min_price_flat_rent/1000);
	$max_price_flat_rent = ceil($max_price_flat_rent/1000);
	
	if($min_full_square_flat_rent==0){
		$min_full_square_flat_rent = 1;
	}
	
	if($max_full_square_flat_rent==0){
		$max_full_square_flat_rent = 1;
	}
	$min_meter_price_flat_rent = floor($min_price_flat_rent/$min_full_square_flat_rent);
	$max_meter_price_flat_rent = ceil($max_price_flat_rent/$max_full_square_flat_rent);
	
	$min_meter_price_flat_rent = floor($min_meter_price_flat_rent/1000);
	$max_meter_price_flat_rent = ceil($max_meter_price_flat_rent/1000);
}

/*
*  Комнаты
*/
$res = mysql_query("
	SELECT 
		MIN(price) AS min_price, 
		MAX(price) AS max_price, 
		MIN(room_square) AS min_room_square, 
		MAX(room_square) AS max_room_square,
		MIN(full_square) AS min_full_square, 
		MAX(full_square) AS max_full_square,
		MIN(price/full_square) AS min_full_square_meter, 
		MAX(price/full_square) AS max_full_square_meter, 
		MIN(kitchen_square) AS min_kitchen_square, 
		MAX(kitchen_square) AS max_kitchen_square,
		MIN(floors) AS min_floors, 
		MAX(floors) AS max_floors
	FROM ".$template."_rooms
	WHERE activation='1' && type='sell'
");
if(mysql_num_rows($res)>0){//_count_params
	$row = mysql_fetch_assoc($res);
	$min_price_room_sell = $row['min_price'];
	$max_price_room_sell = $row['max_price'];
	$min_room_square_room_sell = floor($row['min_room_square']);
	$min_room_square_room_sell = 1;
	$max_room_square_room_sell = ceil($row['max_room_square']);

	$min_full_square_room_sell = floor($row['min_full_square']);
	$min_full_square_room_sell = 1;
	$max_full_square_room_sell = ceil($row['max_full_square']);

	$min_full_square_meter = floor($row['min_full_square_meter']);
	$min_full_square_meter = 1;
	$max_full_square_meter = ceil($row['max_full_square_meter']);

	$min_kitchen_square_room_sell = floor($row['min_kitchen_square']);
	$min_kitchen_square_room_sell = 1;
	$max_kitchen_square_room_sell = ceil($row['max_kitchen_square']);

	$min_floors_room_sell = $row['min_floors'];
	$min_floors_room_sell = 1;
	$max_floors_room_sell = $row['max_floors'];

	$min_meter_price_room_sell = $min_full_square_meter;
	$max_meter_price_room_sell = $max_full_square_meter;
	
	$min_meter_price_room_sell = floor($min_meter_price_room_sell/1000);
	$max_meter_price_room_sell = ceil($max_meter_price_room_sell/1000);
	
	$min_price_room_sell = floor($min_price_room_sell/1000);
	$max_price_room_sell = ceil($max_price_room_sell/1000);
}
$res = mysql_query("
	SELECT 
		MIN(price) AS min_price, 
		MAX(price) AS max_price, 
		MIN(full_square) AS min_full_square, 
		MAX(full_square) AS max_full_square,
		MIN(kitchen_square) AS min_kitchen_square, 
		MAX(kitchen_square) AS max_kitchen_square,
		MIN(floors) AS min_floors, 
		MAX(floors) AS max_floors
	FROM ".$template."_rooms
	WHERE activation='1' && type='rent'
");
if(mysql_num_rows($res)>0){//_count_params
	$row = mysql_fetch_assoc($res);
	$min_price_room_rent = $row['min_price'];
	$max_price_room_rent = $row['max_price'];
	$min_room_square_room_rent = floor($row['min_room_square']);
	$max_room_square_room_rent = ceil($row['max_room_square']);

	$min_full_square_room_rent = floor($row['min_full_square']);
	$min_full_square_room_rent = 1;
	$max_full_square_room_rent = ceil($row['max_full_square']);

	$min_kitchen_square_room_rent = floor($row['min_kitchen_square']);
	$min_kitchen_square_room_rent = 1;
	$max_kitchen_square_room_rent = ceil($row['max_kitchen_square']);

	$min_floors_room_rent = $row['min_floors'];
	$max_floors_room_rent = $row['max_floors'];

	$min_price_room_rent = floor($min_price_room_rent/1000);
	$max_price_room_rent = ceil($max_price_room_rent/1000);
	
	if($min_full_square_room_rent==0){
		$min_full_square_room_rent = 1;
	}	
	if($max_full_square_room_rent==0){
		$max_full_square_room_rent = 1;
	}
	$min_meter_price_room_rent = floor($min_price_room_rent/$min_full_square_room_rent);
	$max_meter_price_room_rent = ceil($max_price_room_rent/$max_full_square_room_rent);
	
	$min_meter_price_room_rent = floor($min_meter_price_room_rent/1000);
	$max_meter_price_room_rent = ceil($max_meter_price_room_rent/1000);
}


$finish = round(microtime(true) - $start2,5);
// echo '<div>'.$finish.'</div>';


$_minPriceNew = $min_price_new_sell; // минимальная цена новостройки за всё
$_maxPriceNew = $max_price_new_sell; // максимальная новостройки цена за всё

$_minMeterNewPrice = 40; // минимальная цена новостройки за метр
$_maxMeterNewPrice = 160; // максимальная цена новостройки за метр

$_minPriceFlat = $min_price_flat_sell; // минимальная цена вторички за всё (купить)
$_maxPriceFlat = $max_price_flat_sell; // максимальная вторички цена за всё (купить)

$_minMeterFlatPrice = 40; // минимальная цена вторички за метр (купить)
$_maxMeterFlatPrice = 160; // максимальная цена вторички за метр (купить)

$_minPriceFlatRent = $min_price_flat_rent; // минимальная цена вторички за всё (снять)
$_maxPriceFlatRent = $max_price_flat_rent; // максимальная вторички цена за всё (снять)

$_minPriceRoom = $min_price_room_sell; // минимальная цена комнаты за всё (купить)
$_maxPriceRoom = $max_price_room_sell; // максимальная комнаты цена за всё (купить)

$_minMeterRoomPrice = 30; // минимальная цена комнаты за метр (купить)
$_maxMeterRoomPrice = 150; // максимальная цена комнаты за метр (купить)

$_minPriceRoomRent = $min_price_room_rent; // минимальная цена комнаты за всё (снять)
$_maxPriceRoomRent = $max_price_room_rent; // максимальная комнаты цена за всё (снять)

$_minPriceCountry = 1.1; // минимальная цена загородной за всё (купить)
$_maxPriceCountry = 15; // максимальная загородной цена за всё (купить)

$_minMeterCountryPrice = 70; // минимальная цена загородной за метр (купить)
$_maxMeterCountryPrice = 200; // максимальная цена загородной за метр (купить)

$_minPriceCountryRent = 1.1; // минимальная цена загородной за всё (снять)
$_maxPriceCountryRent = 15; // максимальная загородной цена за всё (снять)

$_minMeterCountryPrice = 70; // минимальная цена загородной за метр (снять)
$_maxMeterCountryPrice = 200; // максимальная цена загородной за метр (снять)

$_minPriceCommercial = 3.3; // минимальная цена коммерческой за всё
$_maxPriceCommercial = 30; // максимальная коммерческой цена за всё

$_minMeterCommercialPrice = 70; // минимальная цена коммерческой за метр
$_maxMeterCommercialPrice = 260; // максимальная цена коммерческой за метр

$_minPriceCommercialRent = 10; // минимальная цена коммерческой за всё (снять)
$_maxPriceCommercialRent = 200; // максимальная цена коммерческой за всё (снять)

$start = microtime(true);

$searchPage = false;
if($params[0] && $params[0]=='search'){
	$searchPage = true;
}

$estateValue = 1;
$typeValue = 'sell';
$delivery_new_var = '';
if(count($arrayLinkSearch['type'])==0){
	$sellNewParam = true;
}
if(count($arrayLinkSearch['type'])>0){
	$typeValue = $arrayLinkSearch['type'];
}
if(count($arrayLinkSearch['estate'])>0){
	$estateValue = $arrayLinkSearch['estate'];
}
if($arrayLinkSearch['type']=='sell' && $arrayLinkSearch['estate']==1){
	$sellNewParam = true;
}

$_ESTATE_ARRAY = array("1"=>"new","2"=>"flat","3"=>"room","4"=>"country","5"=>"commercial","6"=>"cession");

if(is_array($arrayLinkSearch) && count($arrayLinkSearch)>0){
	foreach($arrayLinkSearch as $name => $value){
		$var = $name."_".$_ESTATE_ARRAY[$arrayLinkSearch['estate']]."_".$arrayLinkSearch['type']."_var";
		// echo $var.'<br>';
		$$var = $value;
	}
}

if(!$estate_flat_sell_var && !$estate_flat_rent_var && !$estate_room_rent_var && !$estate_room_sell_var && !$estate_country_rent_var && !$estate_country_sell_var && !$estate_commercial_rent_var && !$estate_commercial_sell_var && !$estate_cession_sell_var){
	$estate_new_sell_var = 1;
}

if(!$type_new_sell_var && !$searchPage){
	$type_new_sell_var = 'sell';
}
?>

<div id="filter_search">
	<form action="/search" method="get">
		<div class="simple_search">
			<div class="inner_block">
				<div class="row">
					<div class="checkbox_block types">
						<input type="hidden" name="type" value="<?=$typeValue?>">
						<div class="select_checkbox">
							<ul>
								<li <?$typeValue=='sell'?print'class="checked"':''?>><a data-type="sell" href="javascript:void(0)" data-id="sell">Купить</a></li>
								<?
								// if($estateValue==1 || $estateValue==6){
									// echo '<li class="none"><a data-type="rent" href="javascript:void(0)" data-id="rent">Снять</a><i class="not_active"></i></li>';
								// }
								// else {
									$checked = '';
									if($typeValue=='rent'){
										$checked = ' class="checked"';
									}
									echo '<li'.$checked.'><a data-type="rent" href="javascript:void(0)" data-id="rent">Снять</a></li>';
								// }
								?>
							</ul>
						</div>
					</div>
					<div class="checkbox_block estate">
						<input type="hidden" name="estate" value="<?=$estateValue?>">
						<div class="select_checkbox">
							<ul>
								<li <?$estateValue==1?print'class="checked"':''?>><a data-type="new" href="javascript:void(0)" data-id="1">Новостройку</a></li>
								<li <?$estateValue==6?print'class="checked"':''?>><a data-type="cession" href="javascript:void(0)" data-id="6">Переуступку</a></li>
								<li <?$estateValue==2?print'class="checked"':''?>><a data-type="flat" href="javascript:void(0)" data-id="2">Квартиру</a></li>
								<li <?$estateValue==3?print'class="checked"':''?>><a data-type="room" href="javascript:void(0)" data-id="3">Комнату</a></li>
								<!--<li class="none" <?$estateValue==4?print'class="checked"':''?>><a data-type="country" href="javascript:void(0)" data-id="4">Загород.</a><i class="not_active"></i></li>-->
								<li class="none" <?$estateValue==5?print'class="checked"':''?>><a data-type="commercial" href="javascript:void(0)" data-id="5">Коммерческую</a><i class="not_active"></i></li>
							</ul>
						</div>
					</div>
					<div <?!$priceAll_new_sell_var && !$sellNewParam?print'style="display:none"':'';?> class="price_block new sell">
						<div <?$priceAll_new_sell_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<div style="margin-left:30px" class="select_block price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeNewSell = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_new_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeNewSell = '<li><a class="min" data-name_select="price" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_new_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeNewSell = '<li><a class="max" data-name_select="price" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_new_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeNewSell = '<li><a class="min max" data-name_select="price" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'. до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minPriceNew; $c<=$_maxPriceNew;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price" data-tags="от '.str_replace(".", ",", $c).' '.$name_type.'." data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceNew){
												break;
											}
											if($c<=5){
												$c = $c+0.1;
											}
											if($c>5 && $c<=10){
												$c = $c+0.2;
											}
											if($c>10 && $c<=20){
												$c = $c+0.5;
											}
											if($c>20){
												$c = $c+1;
											}
											if($c>$_maxPriceNew){
												$c = $_maxPriceNew;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
										for($c=$_minPriceNew; $c<=$_maxPriceNew;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price" data-tags="до '.str_replace(".", ",", $c).' '.$name_type.'." data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceNew){
												break;
											}
											if($c<=5){
												$c = $c+0.1;
											}
											if($c>5 && $c<=10){
												$c = $c+0.2;
											}
											if($c>10 && $c<=20){
												$c = $c+0.5;
											}
											if($c>20){
												$c = $c+1;
											}
											if($c>$_maxPriceNew){
												$c = $_maxPriceNew;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_new_sell_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters<?!$searchPage?print' nosearchpage':''?>">
							<div style="margin-left:30px" class="select_block price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_new_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeNewSell = '<li><a class="min" data-name_select="price_meter" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_new_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeNewSell = '<li><a class="max" data-name_select="price_meter" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_new_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeNewSell = '<li><a class="min max" data-name_select="price_meter" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'. до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$min_meter_price_new_sell; $c<=$max_meter_price_new_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter" data-tags="от '.str_replace(".", ",", $c).' '.$name_type.'." data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_meter_price_new_sell; $c<=$max_meter_price_new_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter" data-tags="до '.str_replace(".", ",", $c).' '.$name_type.'." data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($sellNewParam && $arrayLinkSearch['priceAll']=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">За все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">За м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_cession_sell_var?print'style="display:none"':print'style="display:block"';?> class="price_block cession sell">
						<div <?$priceAll_cession_sell_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<div style="margin-left:30px" class="select_block price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCessionSell = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_cession_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCessionSell = '<li><a class="min" data-name_select="price" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_cession_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCessionSell = '<li><a class="max" data-name_select="price" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_cession_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCessionSell = '<li><a class="min max" data-name_select="price" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'. до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_cession_sell_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$min_price_cession_sell; $c<=$max_price_cession_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price" data-tags="от '.str_replace(".", ",", $c).' '.$name_type.'." data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_price_cession_sell){
												break;
											}
											if($c<=5){
												$c = $c+0.1;
											}
											if($c>5 && $c<=10){
												$c = $c+0.2;
											}
											if($c>10 && $c<=20){
												$c = $c+0.5;
											}
											if($c>20){
												$c = $c+1;
											}
											if($c>$max_price_cession_sell){
												$c = $max_price_cession_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_cession_sell_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
										for($c=$min_price_cession_sell; $c<=$max_price_cession_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price" data-tags="до '.str_replace(".", ",", $c).' '.$name_type.'." data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_price_cession_sell){
												break;
											}
											if($c<=5){
												$c = $c+0.1;
											}
											if($c>5 && $c<=10){
												$c = $c+0.2;
											}
											if($c>10 && $c<=20){
												$c = $c+0.5;
											}
											if($c>20){
												$c = $c+1;
											}
											if($c>$max_price_cession_sell){
												$c = $max_price_cession_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_cession_sell_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters<?!$searchPage?print' nosearchpage':''?>">
							<div style="margin-left:30px" class="select_block price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_cession_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCessionSell = '<li><a class="min" data-name_select="price_meter" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_cession_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCessionSell = '<li><a class="max" data-name_select="price_meter" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_cession_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCessionSell = '<li><a class="min max" data-name_select="price_meter" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'. до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_cession_sell_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$min_meter_price_new_sell; $c<=$max_meter_price_new_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter" data-tags="от '.str_replace(".", ",", $c).' '.$name_type.'." data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_cession_sell_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_meter_price_new_sell; $c<=$max_meter_price_new_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter" data-tags="до '.str_replace(".", ",", $c).' '.$name_type.'." data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_cession_sell_var && $arrayLinkSearch['priceAll']=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">За все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">За м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_flat_sell_var?print'style="display:none"':print'style="display:block"';?> class="price_block flat sell">
						<div <?$priceAll_flat_sell_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<div style="margin-left:30px" class="select_block price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeFlatSell = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_flat_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeFlatSell = '<li><a class="min" data-name_select="priceflat" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_flat_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeFlatSell = '<li><a class="max" data-name_select="priceflat" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_flat_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeFlatSell = '<li><a class="min max" data-name_select="priceflat" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'. до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_flat_sell_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$min_price_flat_sell; $c<=$max_price_flat_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="priceflat" data-tags="от '.str_replace(".", ",", $c).' '.$name_type.'." data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_price_flat_sell){
												break;
											}
											if($c<=5){
												$c = $c+0.1;
											}
											if($c>5 && $c<=10){
												$c = $c+0.2;
											}
											if($c>10 && $c<=20){
												$c = $c+0.5;
											}
											if($c>20){
												$c = $c+1;
											}
											if($c>$max_price_flat_sell){
												$c = $max_price_flat_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_flat_sell_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
										for($c=$min_price_flat_sell; $c<=$max_price_flat_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="priceflat" data-tags="до '.str_replace(".", ",", $c).' '.$name_type.'." data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_price_flat_sell){
												break;
											}
											if($c<=5){
												$c = $c+0.1;
											}
											if($c>5 && $c<=10){
												$c = $c+0.2;
											}
											if($c>10 && $c<=20){
												$c = $c+0.5;
											}
											if($c>20){
												$c = $c+1;
											}
											if($c>$max_price_flat_sell){
												$c = $max_price_flat_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_flat_sell_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<div style="margin-left:30px" class="select_block price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_flat_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeFlatSell = '<li><a class="min" data-name_select="pricefloat_meter" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_flat_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeFlatSell = '<li><a class="max" data-name_select="pricefloat_meter" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_flat_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeFlatSell = '<li><a class="min max" data-name_select="pricefloat_meter" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'. до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_flat_sell_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$min_meter_price_flat_sell; $c<=$max_meter_price_flat_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricefloat_meter" data-tags="от '.str_replace(".", ",", $c).' '.$name_type.'." data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_meter_price_flat_sell){
												break;
											}
											$c = $c+1;
											if($c>$max_meter_price_flat_sell){
												$c = $max_meter_price_flat_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_flat_sell_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_meter_price_flat_sell; $c<=$max_meter_price_flat_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricefloat_meter" data-tags="до '.str_replace(".", ",", $c).' '.$name_type.'." data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_meter_price_flat_sell){
												break;
											}
											$c = $c+1;
											if($c>$max_meter_price_flat_sell){
												$c = $max_meter_price_flat_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_flat_sell_var && $priceAll_flat_sell_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">За все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">За м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_flat_rent_var?print'style="display:none"':print'style="display:block"';?> class="price_block flat rent">
						<div class="prices_choose all">
							<div style="margin-left:30px" class="select_block price">
								<?
								$mult = 1000;
								$name_type = 'тыс';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_flat_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeFlatRent = '<li><a class="min" data-name_select="priceflat_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_flat_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeFlatRent = '<li><a class="max" data-name_select="priceflat_rent" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_flat_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeFlatRent = '<li><a class="min max" data-name_select="priceflat_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'. до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_flat_rent_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$min_price_flat_rent; $c<=$max_price_flat_rent;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="priceflat_rent" data-tags="от '.str_replace(".", ",", $c).' '.$name_type.'." data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_flat_rent_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_price_flat_rent; $c<=$max_price_flat_rent;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="priceflat_rent" data-tags="до '.str_replace(".", ",", $c).' '.$name_type.'." data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<input type="hidden" name="priceAll" value="all">
							<div class="choosed_block">За все</div>
							<div class="scrollbar-inner">
								<ul>
									<li class="current"><a href="javascript:void(0)" data-id="all">За все</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_room_sell_var?print'style="display:none"':print'style="display:block"';?> class="price_block room sell">
						<div <?$priceAll_room_sell_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<div style="margin-left:30px" class="select_block price">
								<?
								$name_type = 'тыс';
								$mult = 100000;
								$priceChangeRoomSell = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_room_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeRoomSell = '<li><a class="min" data-name_select="priceroom" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_room_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeRoomSell = '<li><a class="max" data-name_select="priceroom" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_room_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeRoomSell = '<li><a class="min max" data-name_select="priceroom" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'. до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_room_sell_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$min_price_room_sell; $c<=$max_price_room_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="priceroom" data-tags="от '.str_replace(".", ",", $c).' '.$name_type.'." data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_price_room_sell){
												break;
											}
											$c = $c+50;
											if($c>$max_price_room_sell){
												$c = $max_price_room_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_room_sell_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
										for($c=$min_price_room_sell; $c<=$max_price_room_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="priceroom" data-tags="до '.str_replace(".", ",", $c).' '.$name_type.'." data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_price_room_sell){
												break;
											}
											$c = $c+50;
											if($c>$max_price_room_sell){
												$c = $max_price_room_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_room_sell_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<div style="margin-left:30px" class="select_block price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_room_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeRoomSell = '<li><a class="min" data-name_select="price_meter_room" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_room_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeRoomSell = '<li><a class="max" data-name_select="price_meter_room" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_room_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeRoomSell = '<li><a class="min max" data-name_select="price_meter_room" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'. до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_room_sell_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_meter_price_room_sell; $c<=$max_meter_price_room_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_room" data-tags="от '.str_replace(".", ",", $c).' '.$name_type.'." data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_meter_price_room_sell){
												break;
											}
											$c = $c+1;
											if($c>$max_meter_price_room_sell){
												$c = $max_meter_price_room_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_room_sell_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_meter_price_room_sell; $c<=$max_meter_price_room_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_room" data-tags="до '.str_replace(".", ",", $c).' '.$name_type.'." data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_meter_price_room_sell){
												break;
											}
											$c = $c+1;
											if($c>$max_meter_price_room_sell){
												$c = $max_meter_price_room_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_room_sell_var && $priceAll_room_sell_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">За все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">За м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_room_rent_var?print'style="display:none"':print'style="display:block"';?> class="price_block room rent">
						<div class="prices_choose all">
							<div style="margin-left:30px" class="select_block price">
								<?
								$mult = 1000;
								$name_type = 'тыс';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_room_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeRoomRent = '<li><a class="min" data-name_select="priceroom_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_room_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeRoomRent = '<li><a class="max" data-name_select="priceroom_rent" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_room_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeRoomRent = '<li><a class="min max" data-name_select="priceroom_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'. до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_room_rent_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$min_price_room_rent; $c<=$max_price_room_rent;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="priceroom_rent" data-tags="от '.str_replace(".", ",", $c).' '.$name_type.'." data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_price_room_rent){
												break;
											}
											$c = $c+1;
											if($c>$max_price_room_rent){
												$c = $max_price_room_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_room_rent_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_price_room_rent; $c<=$max_price_room_rent;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="priceroom_rent" data-tags="до '.str_replace(".", ",", $c).' '.$name_type.'." data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_price_room_rent){
												break;
											}
											$c = $c+1;
											if($c>$max_price_room_rent){
												$c = $max_price_room_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<input type="hidden" name="priceAll" value="all">
							<div class="choosed_block">За все</div>
							<div class="scrollbar-inner">
								<ul>
									<li class="current"><a href="javascript:void(0)" data-id="all">За все</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_country_sell_var?print'style="display:none"':print'style="display:block"';?> class="price_block country sell">
						<div <?$priceAll_country_sell_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<div style="margin-left:30px" class="select_block price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCountrySell = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_country_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCountrySell = '<li><a class="min" data-name_select="pricecountry" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_country_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCountrySell = '<li><a class="max" data-name_select="pricecountry" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_country_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCountrySell = '<li><a class="min max" data-name_select="pricecountry" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'. до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_country_sell_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minPriceCountry; $c<=$_maxPriceCountry;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecountry" data-tags="от '.str_replace(".", ",", $c).' '.$name_type.'." data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+0.1;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_country_sell_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
										for($c=$_minPriceCountry; $c<=$_maxPriceCountry;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecountry" data-tags="до '.str_replace(".", ",", $c).' '.$name_type.'." data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+0.1;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_country_sell_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<div style="margin-left:30px" class="select_block price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_country_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCountrySell = '<li><a class="min" data-name_select="price_meter_country" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_country_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCountrySell = '<li><a class="max" data-name_select="price_meter_country" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_country_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCountrySell = '<li><a class="min max" data-name_select="price_meter_country" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'. до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_country_sell_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minMeterCountryPrice; $c<=$_maxMeterCountryPrice;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_country" data-tags="от '.str_replace(".", ",", $c).' '.$name_type.'." data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_country_sell_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$_minMeterCountryPrice; $c<=$_maxMeterCountryPrice;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_country" data-tags="до '.str_replace(".", ",", $c).' '.$name_type.'." data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_country_sell_var && $priceAll_country_sell_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">За все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">За м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_country_rent_var?print'style="display:none"':print'style="display:block"';?> class="price_block country rent">
						<div class="prices_choose all">
							<div style="margin-left:30px" class="select_block price">
								<?
								$mult = 1000000;
								$name_type = 'млн';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_country_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCountryRent = '<li><a class="min" data-name_select="pricecountry_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_country_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCountryRent = '<li><a class="max" data-name_select="pricecountry_rent" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_country_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCountryRent = '<li><a class="min max" data-name_select="pricecountry_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'. до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_country_rent_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minPriceCountryRent; $c<=$_maxPriceCountryRent;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecountry_rent" data-tags="от '.str_replace(".", ",", $c).' '.$name_type.'." data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_country_rent_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$_minPriceCountryRent; $c<=$_maxPriceCountryRent;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecountry_rent" data-tags="до '.str_replace(".", ",", $c).' '.$name_type.'." data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<input type="hidden" name="priceAll" value="all">
							<div class="choosed_block">За все</div>
							<div class="scrollbar-inner">
								<ul>
									<li class="current"><a href="javascript:void(0)" data-id="all">За все</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_commercial_sell_var?print'style="display:none"':print'style="display:block"';?> class="price_block commercial sell">
						<div <?$priceAll_commercial_sell_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<div style="margin-left:30px" class="select_block price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCommercialSell = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCommercialSell = '<li><a class="min" data-name_select="pricecommercial" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="max" data-name_select="pricecommercial" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="min max" data-name_select="pricecommercial" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'. до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minPriceCommercial; $c<=$_maxPriceCommercial;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от '.str_replace(".", ",", $c).' '.$name_type.'." data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+0.1;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
										for($c=$_minPriceCommercial; $c<=$_maxPriceCommercial;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="до '.str_replace(".", ",", $c).' '.$name_type.'." data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+0.1;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_commercial_sell_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<div style="margin-left:30px" class="select_block price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCommercialSell = '<li><a class="min" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="max" data-name_select="price_meter_commercial" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="min max" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'. до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minMeterCommercialPrice; $c<=$_maxMeterCommercialPrice;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="от '.str_replace(".", ",", $c).' '.$name_type.'." data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$_minMeterCommercialPrice; $c<=$_maxMeterCommercialPrice;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="до '.str_replace(".", ",", $c).' '.$name_type.'." data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_commercial_sell_var && $priceAll_commercial_sell_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">За все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">За м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_commercial_rent_var?print'style="display:none"':print'style="display:block"';?> class="price_block commercial rent">
						<div class="prices_choose all">
							<div style="margin-left:30px" class="select_block price">
								<?
								$mult = 1000;
								$name_type = 'тыс';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCommercialRent = '<li><a class="min" data-name_select="pricecommercial_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="max" data-name_select="pricecommercial_rent" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="min max" data-name_select="pricecommercial_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'. до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minPriceCommercialRent; $c<=$_maxPriceCommercialRent;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от '.str_replace(".", ",", $c).' '.$name_type.'." data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$_minPriceCommercialRent; $c<=$_maxPriceCommercialRent;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="до '.str_replace(".", ",", $c).' '.$name_type.'." data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<input type="hidden" name="priceAll" value="all">
							<div class="choosed_block">За все</div>
							<div class="scrollbar-inner">
								<ul>
									<li class="current"><a href="javascript:void(0)" data-id="all">За все</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div <?!$type_new_sell_var || !$estate_new_sell_var?print'style="display:none"':''?> class="change_block_filter new sell">
					<div class="checkbox_block">
						<?
						$ex_rooms_new_sell_var = array();
						$valueInput = '';
						$roomsChangeNewSell = '';
						if(isset($rooms_new_sell_var)){
							$ex_rooms_new_sell_var = explode(',',$rooms_new_sell_var);
							$valueInput = ' value="'.$rooms_new_sell_var.'"';
							$id_room = 0;
							$name_room = '';
							if(in_array(0,$ex_rooms_new_sell_var)){ // Студия
								$roomsChangeNewSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="0" href="javascript:void(0)">Студии<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(1,$ex_rooms_new_sell_var)){ // 1-комн.
								$roomsChangeNewSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="1" href="javascript:void(0)">1-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(2,$ex_rooms_new_sell_var)){ // 2-комн.
								$roomsChangeNewSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="2" href="javascript:void(0)">2-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(3,$ex_rooms_new_sell_var)){ // 3-комн.
								$roomsChangeNewSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="3" href="javascript:void(0)">3-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(4,$ex_rooms_new_sell_var)){ // 4+-комн.
								$roomsChangeNewSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="4" href="javascript:void(0)">4+-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
						}
						?>
						<input type="hidden" name="rooms"<?=$valueInput?>>
						<div class="block_checkbox">
							<ul>
								<li <?in_array(0,$ex_rooms_new_sell_var)?print'class="checked"':''?>><a data-tags="Студии" href="javascript:void(0)" data-id="0"><i class="triangle-bottomleft studio"></i>Студию</a></li>
								<li <?in_array(1,$ex_rooms_new_sell_var)?print'class="checked"':''?>><a data-tags="1-комн." href="javascript:void(0)" data-id="1"><i class="triangle-bottomleft one"></i>1</a></li>
								<li <?in_array(2,$ex_rooms_new_sell_var)?print'class="checked"':''?>><a data-tags="2-комн." href="javascript:void(0)" data-id="2"><i class="triangle-bottomleft two"></i>2</a></li>
								<li <?in_array(3,$ex_rooms_new_sell_var)?print'class="checked"':''?>><a data-tags="3-комн." href="javascript:void(0)" data-id="3"><i class="triangle-bottomleft three"></i>3</a></li>
								<li <?in_array(4,$ex_rooms_new_sell_var)?print'class="checked"':''?>><a data-tags="4+-комн." href="javascript:void(0)" data-id="4"><i class="triangle-bottomleft more"></i>4+</a></li>
								<li class="last">Комнат</li>
							</ul>
						</div>
					</div>
				</div>
				<div <?$type_cession_sell_var?print'style="display:block"':'style="display:none"'?> class="change_block_filter cession sell">
					<div class="checkbox_block">
						<?
						$ex_rooms_cession_sell_var = array();
						$valueInput = '';
						$roomsChangeCessionSell = '';
						if(isset($rooms_cession_sell_var)){
							$ex_rooms_cession_sell_var = explode(',',$rooms_cession_sell_var);
							$valueInput = ' value="'.$rooms_cession_sell_var.'"';
							$id_room = 0;
							$name_room = '';
							if(in_array(0,$ex_rooms_cession_sell_var)){ // Студия
								$roomsChangeCessionSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="0" href="javascript:void(0)">Студии<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(1,$ex_rooms_cession_sell_var)){ // 1-комн.
								$roomsChangeCessionSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="1" href="javascript:void(0)">1-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(2,$ex_rooms_cession_sell_var)){ // 2-комн.
								$roomsChangeCessionSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="2" href="javascript:void(0)">2-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(3,$ex_rooms_cession_sell_var)){ // 3-комн.
								$roomsChangeCessionSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="3" href="javascript:void(0)">3-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(4,$ex_rooms_cession_sell_var)){ // 4+-комн.
								$roomsChangeCessionSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="4" href="javascript:void(0)">4+-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
						}
						?>
						<input type="hidden" name="rooms"<?=$valueInput?>>
						<div class="block_checkbox">
							<ul>
								<li <?in_array(0,$ex_rooms_cession_sell_var)?print'class="checked"':''?>><a data-tags="Студии" href="javascript:void(0)" data-id="0"><i class="triangle-bottomleft studio"></i>Студию</a></li>
								<li <?in_array(1,$ex_rooms_cession_sell_var)?print'class="checked"':''?>><a data-tags="1-комн." href="javascript:void(0)" data-id="1"><i class="triangle-bottomleft one"></i>1</a></li>
								<li <?in_array(2,$ex_rooms_cession_sell_var)?print'class="checked"':''?>><a data-tags="2-комн." href="javascript:void(0)" data-id="2"><i class="triangle-bottomleft two"></i>2</a></li>
								<li <?in_array(3,$ex_rooms_cession_sell_var)?print'class="checked"':''?>><a data-tags="3-комн." href="javascript:void(0)" data-id="3"><i class="triangle-bottomleft three"></i>3</a></li>
								<li <?in_array(4,$ex_rooms_cession_sell_var)?print'class="checked"':''?>><a data-tags="4+-комн." href="javascript:void(0)" data-id="4"><i class="triangle-bottomleft more"></i>4+</a></li>
								<li class="last">Комнат</li>
							</ul>
						</div>
					</div>
				</div>
				<div <?$type_flat_sell_var?print'style="display:block"':'style="display:none"'?> class="change_block_filter flat sell">
					<div class="checkbox_block">
						<?
						$ex_rooms_flat_sell_var = array();
						$valueInput = '';
						$roomsChangeFlatSell = '';
						if($rooms_flat_sell_var){
							$ex_rooms_flat_sell_var = explode(',',$rooms_flat_sell_var);
							$valueInput = ' value="'.$rooms_flat_sell_var.'"';
							$id_room = 0;
							$name_room = '';
							if(in_array(0,$ex_rooms_flat_sell_var)){ // Студия
								$roomsChangeFlatSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="0" href="javascript:void(0)">Студии<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(1,$ex_rooms_flat_sell_var)){ // 1-комн.
								$roomsChangeFlatSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="1" href="javascript:void(0)">1-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(2,$ex_rooms_flat_sell_var)){ // 2-комн.
								$roomsChangeFlatSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="2" href="javascript:void(0)">2-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(3,$ex_rooms_flat_sell_var)){ // 3-комн.
								$roomsChangeFlatSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="3" href="javascript:void(0)">3-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(4,$ex_rooms_flat_sell_var)){ // 4-комн.
								$roomsChangeFlatSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="4" href="javascript:void(0)">4-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(5,$ex_rooms_flat_sell_var)){ // 5+-комн.
								$roomsChangeFlatSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="5" href="javascript:void(0)">5+-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
						}
						?>
						<input type="hidden" name="rooms"<?=$valueInput?>>
						<div class="block_checkbox">
							<ul>
								<li <?in_array(0,$ex_rooms_flat_sell_var)?print'class="checked"':''?>><a data-tags="Студии" href="javascript:void(0)" data-id="0"><i class="triangle-bottomleft studio"></i>Студию</a></li>
								<li <?in_array(1,$ex_rooms_flat_sell_var)?print'class="checked"':''?>><a data-tags="1-комн." href="javascript:void(0)" data-id="1"><i class="triangle-bottomleft one"></i>1</a></li>
								<li <?in_array(2,$ex_rooms_flat_sell_var)?print'class="checked"':''?>><a data-tags="2-комн." href="javascript:void(0)" data-id="2"><i class="triangle-bottomleft two"></i>2</a></li>
								<li <?in_array(3,$ex_rooms_flat_sell_var)?print'class="checked"':''?>><a data-tags="3-комн." href="javascript:void(0)" data-id="3"><i class="triangle-bottomleft three"></i>3</a></li>
								<li <?in_array(4,$ex_rooms_flat_sell_var)?print'class="checked"':''?>><a data-tags="4-комн." href="javascript:void(0)" data-id="4"><i class="triangle-bottomleft more"></i>4</a></li>
								<li <?in_array(5,$ex_rooms_flat_sell_var)?print'class="checked"':''?>><a data-tags="5+-комн." href="javascript:void(0)" data-id="5"><i class="triangle-bottomleft five"></i>5+</a></li>
								<li class="last">Комнат</li>
							</ul>
						</div>
					</div>
					<div class="checkbox_block">
						<?
						$valueInput = '';
						$shareChangeFlatSell = '';
						if($share_flat_sell_var){
							$valueInput = ' value="'.$share_flat_sell_var.'"';
							$shareChangeFlatSell .= '<li><a data-name="share" data-type="checkbox_select" data-id="1" href="javascript:void(0)">Студии<span onclick="return delTags(this)" class="close"></span></a></li>';
						}
						?>
						<input type="hidden" name="share"<?=$valueInput?>>
						<div class="block_checkbox">
							<ul>
								<li class="vert_line"></li>
								<li <?isset($share_flat_sell_var)?print'class="checked"':''?> class="share"><a data-tags="Доля" href="javascript:void(0)" data-id="1">Доля</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div <?$type_flat_rent_var?print'style="display:block"':'style="display:none"'?> class="change_block_filter flat rent">
					<div class="checkbox_block">
						<?
						$ex_rooms_flat_rent_var = array();
						$valueInput = '';
						$roomsChangeFlatRent = '';
						if($rooms_flat_rent_var){
							$ex_rooms_flat_rent_var = explode(',',$rooms_flat_rent_var);
							$valueInput = ' value="'.$rooms_flat_rent_var.'"';
							if(in_array(0,$ex_rooms_flat_rent_var)){ // Студия
								$roomsChangeFlatRent .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="0" href="javascript:void(0)">Студии<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(1,$ex_rooms_flat_rent_var)){ // 1-комн.
								$roomsChangeFlatRent .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="1" href="javascript:void(0)">1-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(2,$ex_rooms_flat_rent_var)){ // 2-комн.
								$roomsChangeFlatRent .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="2" href="javascript:void(0)">2-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(3,$ex_rooms_flat_rent_var)){ // 3-комн.
								$roomsChangeFlatRent .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="3" href="javascript:void(0)">3-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(4,$ex_rooms_flat_rent_var)){ // 4+-комн.
								$roomsChangeFlatRent .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="4" href="javascript:void(0)">4+-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
						}
						?>
						<input type="hidden" name="rooms"<?=$valueInput?>>
						<div class="block_checkbox">
							<ul>
								<li <?in_array(0,$ex_rooms_flat_rent_var)?print'class="checked"':''?>><a data-tags="Студии" href="javascript:void(0)" data-id="0"><i class="triangle-bottomleft studio"></i>Студию</a></li>
								<li <?in_array(1,$ex_rooms_flat_rent_var)?print'class="checked"':''?>><a data-tags="1-комн." href="javascript:void(0)" data-id="1"><i class="triangle-bottomleft one"></i>1</a></li>
								<li <?in_array(2,$ex_rooms_flat_rent_var)?print'class="checked"':''?>><a data-tags="2-комн." href="javascript:void(0)" data-id="2"><i class="triangle-bottomleft two"></i>2</a></li>
								<li <?in_array(3,$ex_rooms_flat_rent_var)?print'class="checked"':''?>><a data-tags="3-комн." href="javascript:void(0)" data-id="3"><i class="triangle-bottomleft three"></i>3</a></li>
								<li <?in_array(4,$ex_rooms_flat_rent_var)?print'class="checked"':''?>><a data-tags="4+-комн." href="javascript:void(0)" data-id="4"><i class="triangle-bottomleft more"></i>4+</a></li>
								<li class="last">Комнат</li>
							</ul>
						</div>
					</div>
				</div>
				<div <?$type_room_sell_var?print'style="display:block"':'style="display:none"'?> class="change_block_filter room sell">
					<div class="checkbox_block">
						<?
						$ex_rooms_room_sell_var = array();
						$valueInput = '';
						$roomsChangeRoomSell = '';
						if(isset($rooms_room_sell_var)){
							$ex_rooms_room_sell_var = explode(',',$rooms_room_sell_var);
							$valueInput = ' value="'.$rooms_room_sell_var.'"';
							$id_room = 0;
							$name_room = '';
							// if(in_array(0,$ex_rooms_room_sell_var)){ // Студия
								// $roomsChangeRoomSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="0" href="javascript:void(0)">Студии<span onclick="return delTags(this)" class="close"></span></a></li>';
							// }
							// if(in_array(1,$ex_rooms_room_sell_var)){ // 1-комн.
								// $roomsChangeRoomSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="1" href="javascript:void(0)">1-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							// }
							if(in_array(2,$ex_rooms_room_sell_var)){ // 2-комн.
								$roomsChangeRoomSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="2" href="javascript:void(0)">в 2-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(3,$ex_rooms_room_sell_var)){ // 3-комн.
								$roomsChangeRoomSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="3" href="javascript:void(0)">в 3-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(4,$ex_rooms_room_sell_var)){ // 4+-комн.
								$roomsChangeRoomSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="4" href="javascript:void(0)">в 4-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(5,$ex_rooms_room_sell_var)){ // 5-комн.
								$roomsChangeRoomSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="5" href="javascript:void(0)">в 5-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(5,$ex_rooms_room_sell_var)){ // 6+-комн.
								$roomsChangeRoomSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="6" href="javascript:void(0)">в 6+-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
						}
						?>
						<input type="hidden" name="rooms"<?=$valueInput?>>
						<div class="block_checkbox">
							<ul>
								<li <?in_array(2,$ex_rooms_room_sell_var)?print'class="checked"':''?>><a data-tags="в 2-комн." href="javascript:void(0)" data-id="2"><i class="triangle-bottomleft two"></i>2</a></li>
								<li <?in_array(3,$ex_rooms_room_sell_var)?print'class="checked"':''?>><a data-tags="в 3-комн." href="javascript:void(0)" data-id="3"><i class="triangle-bottomleft three"></i>3</a></li>
								<li <?in_array(4,$ex_rooms_room_sell_var)?print'class="checked"':''?>><a data-tags="в 4-комн." href="javascript:void(0)" data-id="4"><i class="triangle-bottomleft more"></i>4</a></li>
								<li <?in_array(5,$ex_rooms_room_sell_var)?print'class="checked"':''?>><a data-tags="в 5-комн." href="javascript:void(0)" data-id="5"><i class="triangle-bottomleft five"></i>5</a></li>
								<li <?in_array(6,$ex_rooms_room_sell_var)?print'class="checked"':''?>><a data-tags="в 6+-комн." href="javascript:void(0)" data-id="6"><i class="triangle-bottomleft five"></i>6+</a></li>
								<li class="last">квартире</li>
							</ul>
						</div>
					</div>
					<div class="checkbox_block">
						<?
						$valueInput = '';
						$shareChangeRoomSell = '';
						if($share_room_sell_var){
							$valueInput = ' value="'.$share_room_sell_var.'"';
							$shareChangeRoomSell .= '<li><a data-name="share" data-type="checkbox_select" data-id="1" href="javascript:void(0)">Студии<span onclick="return delTags(this)" class="close"></span></a></li>';
						}
						?>
						<input type="hidden" name="share"<?=$valueInput?>>
						<div class="block_checkbox">
							<ul>
								<li class="vert_line"></li>
								<li <?isset($share_room_sell_var)?print'class="checked"':''?> class="share"><a data-tags="Доля" href="javascript:void(0)" data-id="1">Доля</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div <?$type_room_rent_var?print'style="display:block"':'style="display:none"'?> class="change_block_filter room rent">
					<div class="metric ceil">
						<div class="label_select">Площадь</div>
						<div class="select_block count">
							<?
							$name_type = 'кв.м';
							if($arrayLinkSearch['squareRoomMin'] && !$arrayLinkSearch['squareRoomMax'] && $type_room_rent_var){ //только площадь "от"
								$c1 = $arrayLinkSearch['squareRoomMin']; 
								$squareFullChangeRoomRent = '<li><a class="min" data-name_select="squarerentroom" data-name="squareRoomMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareRoomMin'].'" href="javascript:void(0)">от '.$c1.' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(!$arrayLinkSearch['squareRoomMin'] && $arrayLinkSearch['squareRoomMax'] && $type_room_rent_var){ //только площадь "до"
								$c2 = $arrayLinkSearch['squareRoomMax'];
								$squareFullChangeRoomRent = '<li><a class="max" data-name_select="squarerentroom" data-name="squareRoomMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareRoomMax'].'" href="javascript:void(0)">до '.$c2.' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if($arrayLinkSearch['squareRoomMin'] && $arrayLinkSearch['squareRoomMax'] && $type_room_rent_var){ //промежуток площадей
								$c1 = $arrayLinkSearch['squareRoomMin'];
								$c2 = $arrayLinkSearch['squareRoomMax'];
								$squareFullChangeRoomRent = '<li><a class="min max" data-name_select="squarerentroom" data-name="squareRoomMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareRoomMin'].'" href="javascript:void(0)">от '.$c1.' '.$name_type.'. до '.$c2.' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}

							$nameParams = 'От';
							$valueParams = '';
							$currentParams = '';
							if($type_room_rent_var && $arrayLinkSearch['squareRoomMin']){
								$nameParams = $arrayLinkSearch['squareRoomMin'];
								$currentParams = $arrayLinkSearch['squareRoomMin'];
								$valueParams = ' value="'.$arrayLinkSearch['squareRoomMin'].'"';
							}
							?>
							<input type="hidden" name="squareRoomMin"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
									<?									
									if($min_room_square_room_rent==0){
										$min_room_square_room_rent = 1;
									}
									if($max_room_square_room_rent==0){
										$max_room_square_room_rent = 10;
									}
									for($c=$min_room_square_room_rent; $c<=$max_room_square_room_rent;){
										$current = "";
										if($c==$currentParams){
											$current = ' class="current"';
										}
										echo '<li'.$current.'><a data-name="squarerentroom" data-tags="от '.$c.' '.$name_type.'" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
										if($c==$max_room_square_room_rent){
											break;
										}
										$c = $c+1;
										if($c>$max_room_square_room_rent){
											$c = $max_room_square_room_rent;
										}
									}
									?>
								</ul>
							</div>
						</div>
						<div class="mdash">–</div>
						<div class="select_block count">

							<?
							$nameParams = 'До';
							$valueParams = '';
							$currentParams = '';
							if($type_room_rent_var && $arrayLinkSearch['squareRoomMax']){
								$nameParams = $arrayLinkSearch['squareRoomMax'];
								$currentParams = $arrayLinkSearch['squareRoomMax'];
								$valueParams = ' value="'.$arrayLinkSearch['squareRoomMax'].'"';
							}
							?>
							<input type="hidden" name="squareRoomMax"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
									<?
									if($min_room_square_room_rent==0){
										$min_room_square_room_rent = 1;
									}
									if($max_room_square_room_rent==0){
										$max_room_square_room_rent = 10;
									}
									for($c=$min_room_square_room_rent; $c<=$max_room_square_room_rent;){
										$current = "";
										if($c==$currentParams){
											$current = ' class="current"';
										}
										echo '<li'.$current.'><a data-name="squarerentroom" data-tags="до '.$c.' '.$name_type.'" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
										if($c==$max_room_square_room_rent){
											break;
										}
										$c = $c+1;
										if($c>$max_room_square_room_rent){
											$c = $max_room_square_room_rent;
										}
									}
									?>
								</ul>
							</div>
						</div>
						<div class="com">м<sup>2</sup></div>
					</div>
				</div>
				<div <?$type_country_sell_var?print'style="display:block"':'style="display:none"'?> class="change_block_filter country sell">
					<div class="checkbox_block">
						<?
						$ex_type_country_country_sell_var = array();
						$valueInput = '';
						$typeCountryChangeCountrySell = '';
						if($type_country_country_sell_var){
							$ex_type_country_country_sell_var = explode(',',$type_country_country_sell_var);
							$valueInput = ' value="'.$type_country_country_sell_var.'"';
							$id_room = 0;
							$name_room = '';
							if(in_array(1,$ex_type_country_country_sell_var)){ // Дом
								$typeCountryChangeCountrySell .= '<li><a data-name="type_country" data-type="checkbox_select" data-id="1" href="javascript:void(0)">Дом<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(2,$ex_type_country_country_sell_var)){ // Таунхаус
								$typeCountryChangeCountrySell .= '<li><a data-name="type_country" data-type="checkbox_select" data-id="2" href="javascript:void(0)">Таунхаус<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(3,$ex_type_country_country_sell_var)){ // Участок
								$typeCountryChangeCountrySell .= '<li><a data-name="type_country" data-type="checkbox_select" data-id="3" href="javascript:void(0)">Участок<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
						}
						?>
						<input type="hidden" name="type_country"<?=$valueInput?>>
						<div class="block_checkbox big">
							<ul>
								<li <?in_array(1,$ex_type_country_country_sell_var)?print'class="checked"':''?>><a data-tags="Дом" href="javascript:void(0)" data-id="1">Дом</a></li>
								<li <?in_array(2,$ex_type_country_country_sell_var)?print'class="checked"':''?>><a data-tags="Таунхаус" href="javascript:void(0)" data-id="2">Таунхаус</a></li>
								<li <?in_array(3,$ex_type_country_country_sell_var)?print'class="checked"':''?>><a data-tags="Участок" href="javascript:void(0)" data-id="3">Участок</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div <?$type_country_rent_var?print'style="display:block"':'style="display:none"'?> class="change_block_filter country rent">
					<div class="checkbox_block">
						<?
						$ex_type_country_country_rent_var = array();
						$valueInput = '';
						$typeCountryChangeCountryRent = '';
						if($type_country_country_rent_var){
							$ex_type_country_country_rent_var = explode(',',$type_country_country_rent_var);
							$valueInput = ' value="'.$type_country_country_rent_var.'"';
							$id_room = 0;
							$name_room = '';
							if(in_array(1,$ex_type_country_country_rent_var)){ // Дом
								$typeCountryChangeCountryRent .= '<li><a data-name="type_country" data-type="checkbox_select" data-id="1" href="javascript:void(0)">Дом<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(2,$ex_type_country_country_rent_var)){ // Таунхаус
								$typeCountryChangeCountryRent .= '<li><a data-name="type_country" data-type="checkbox_select" data-id="2" href="javascript:void(0)">Таунхаус<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(3,$ex_type_country_country_rent_var)){ // Участок
								$typeCountryChangeCountryRent .= '<li><a data-name="type_country" data-type="checkbox_select" data-id="3" href="javascript:void(0)">Участок<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
						}
						?>
						<input type="hidden" name="type_country"<?=$valueInput?>>
						<div class="block_checkbox big">
							<ul>
								<li <?in_array(1,$ex_type_country_country_rent_var)?print'class="checked"':''?>><a data-tags="Дом" href="javascript:void(0)" data-id="1">Дом</a></li>
								<li <?in_array(2,$ex_type_country_country_rent_var)?print'class="checked"':''?>><a data-tags="Таунхаус" href="javascript:void(0)" data-id="2">Таунхаус</a></li>
								<li <?in_array(3,$ex_type_country_country_rent_var)?print'class="checked"':''?>><a data-tags="Участок" href="javascript:void(0)" data-id="3">Участок</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div <?$type_commercial_sell_var?print'style="display:block"':'style="display:none"'?> class="change_block_filter commercial sell">
					<div class="checkbox_block">
						<?
						$ex_type_commer_commercial_sell_var = array();
						$valueInput = '';
						$typeCommerChangeCommercialSell = '';
						if($type_commer_commercial_sell_var){
							$ex_type_commer_commercial_sell_var = explode(',',$type_commer_commercial_sell_var);
							$valueInput = ' value="'.$type_commer_commercial_sell_var.'"';
							$id_room = 0;
							$name_room = '';
							if(in_array(1,$ex_type_commer_commercial_sell_var)){ // Офис
								$typeCommerChangeCommercialSell .= '<li><a data-name="type_commer" data-type="checkbox_select" data-id="1" href="javascript:void(0)">Офис<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(2,$ex_type_commer_commercial_sell_var)){ // Магазин
								$typeCommerChangeCommercialSell .= '<li><a data-name="type_commer" data-type="checkbox_select" data-id="2" href="javascript:void(0)">Магазин<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(3,$ex_type_commer_commercial_sell_var)){ // Производство
								$typeCommerChangeCommercialSell .= '<li><a data-name="type_commer" data-type="checkbox_select" data-id="3" href="javascript:void(0)">Производство<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(4,$ex_type_commer_commercial_sell_var)){ // Склад
								$typeCommerChangeCommercialSell .= '<li><a data-name="type_commer" data-type="checkbox_select" data-id="4" href="javascript:void(0)">Склад<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(5,$ex_type_commer_commercial_sell_var)){ // Другое
								$typeCommerChangeCommercialSell .= '<li><a data-name="type_commer" data-type="checkbox_select" data-id="5" href="javascript:void(0)">Другое<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
						}
						?>
						<input type="hidden" name="type_commer"<?=$valueInput?>>
						<div class="block_checkbox big">
							<ul>
								<li <?in_array(1,$ex_type_commer_commercial_sell_var)?print'class="checked"':''?>><a data-tags="Офис" href="javascript:void(0)" data-id="1">Офис</a></li>
								<li <?in_array(2,$ex_type_commer_commercial_sell_var)?print'class="checked"':''?>><a data-tags="Магазин" href="javascript:void(0)" data-id="2">Магазин</a></li>
								<li <?in_array(3,$ex_type_commer_commercial_sell_var)?print'class="checked"':''?>><a data-tags="Производство" href="javascript:void(0)" data-id="3">Производство</a></li>
								<li <?in_array(4,$ex_type_commer_commercial_sell_var)?print'class="checked"':''?>><a data-tags="Склад" href="javascript:void(0)" data-id="4">Склад</a></li>
								<li <?in_array(5,$ex_type_commer_commercial_sell_var)?print'class="checked"':''?>><a data-tags="Другое" href="javascript:void(0)" data-id="5">Другое</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div <?$type_commercial_rent_var?print'style="display:block"':'style="display:none"'?> class="change_block_filter commercial rent">
					<div class="checkbox_block">
						<?
						$ex_type_commer_commercial_rent_var = array();
						$valueInput = '';
						$typeCommerChangeCommercialRent = '';
						if($type_commer_commercial_rent_var){
							$ex_type_commer_commercial_rent_var = explode(',',$type_commer_commercial_rent_var);
							$valueInput = ' value="'.$type_commer_commercial_rent_var.'"';
							$id_room = 0;
							$name_room = '';
							if(in_array(1,$ex_type_commer_commercial_rent_var)){ // Офис
								$typeCommerChangeCommercialRent .= '<li><a data-name="type_commer" data-type="checkbox_select" data-id="1" href="javascript:void(0)">Офис<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(2,$ex_type_commer_commercial_rent_var)){ // Магазин
								$typeCommerChangeCommercialRent .= '<li><a data-name="type_commer" data-type="checkbox_select" data-id="2" href="javascript:void(0)">Магазин<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(3,$ex_type_commer_commercial_rent_var)){ // Производство
								$typeCommerChangeCommercialRent .= '<li><a data-name="type_commer" data-type="checkbox_select" data-id="3" href="javascript:void(0)">Производство<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(4,$ex_type_commer_commercial_rent_var)){ // Склад
								$typeCommerChangeCommercialRent .= '<li><a data-name="type_commer" data-type="checkbox_select" data-id="4" href="javascript:void(0)">Склад<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(5,$ex_type_commer_commercial_rent_var)){ // Другое
								$typeCommerChangeCommercialRent .= '<li><a data-name="type_commer" data-type="checkbox_select" data-id="5" href="javascript:void(0)">Другое<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
						}
						?>
						<input type="hidden" name="type_commer"<?=$valueInput?>>
						<div class="block_checkbox big">
							<ul>
								<li <?in_array(1,$ex_type_commer_commercial_rent_var)?print'class="checked"':''?>><a data-tags="Офис" href="javascript:void(0)" data-id="1">Офис</a></li>
								<li <?in_array(2,$ex_type_commer_commercial_rent_var)?print'class="checked"':''?>><a data-tags="Магазин" href="javascript:void(0)" data-id="2">Магазин</a></li>
								<li <?in_array(3,$ex_type_commer_commercial_rent_var)?print'class="checked"':''?>><a data-tags="Производство" href="javascript:void(0)" data-id="3">Производство</a></li>
								<li <?in_array(4,$ex_type_commer_commercial_rent_var)?print'class="checked"':''?>><a data-tags="Склад" href="javascript:void(0)" data-id="4">Склад</a></li>
								<li <?in_array(5,$ex_type_commer_commercial_rent_var)?print'class="checked"':''?>><a data-tags="Другое" href="javascript:void(0)" data-id="5">Другое</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div <?$type_commercial_rent_var || $type_commercial_sell_var?print'style="margin-left:320px"':''?> class="row new flat room country commercial cession margin">
					<div class="location_change">
						<!--<div data-type="city" class="city filter_local"><a href="javascript:void(0)"><span class="text">Санкт-Петербург</span></a>
							<input type="hidden" name="city" value="1">
							<div class="hidden_block">
								<div class="angles">
									<div class="angle-top-border"></div>
									<div class="angle-top-white"></div>
								</div>
								<div class="inner">
									<span onclick="return closeLocDial(this)" class="close"></span>
									<div class="list_areas one_value">
										<?
										// $word = array();
										// $data = array();
										// $res = mysql_query("
											// SELECT *
											// FROM ".$template."_location
											// WHERE activation='1'
											// ORDER BY name
										// ");
										// if(mysql_num_rows($res)>0){
											// while($row = mysql_fetch_assoc($res)){
												// $w = substr(rawurldecode($row['name']),0,2);
												// if(!array_search($w,$word)){
													// array_push($word,$w);
												// }
												// $arr = array(
													// "id" => $row['id'],
													// "name" => $row['name'],
													// "word" => $w,
												// );
												// array_push($data,$arr);
											// }
										// }
										// $column_count = ceil(count($data)/4);
										// $n2 = 0;
										// $newWord = array();
										// $column = array();
										// $count = 1;
										// for($d=0; $d<count($data); $d++){
											// $open_ul = '';
											// $close_ul = '';
											// if(!in_array($n2,$column)){
												// array_push($column,$n2);
												// $open_ul = '<ul>';
											// }
											// echo $open_ul;
											// if(!in_array($data[$d]['word'],$newWord)){
												// array_push($newWord,$data[$d]['word']);
												// echo '<li class="title">'.$data[$d]['word'].'</li>';
											// }
											// echo '<li><a data-tags="'.$data[$d]['name'].'" data-id="'.$data[$d]['id'].'" href="javascript:void(0)"><span>'.$data[$d]['name'].'</span></a></li>';
											// if($count==$column_count){
												// if(!in_array($data[$d+1]['word'],$newWord)){
													// $close_ul = '</ul>';
													// $n2++;
													// $count = 1;
												// }
											// }
											// else {
												// $count++;
											// }
											// echo $close_ul;
										// }
										?>
									</div>
								</div>
							</div>
						</div>-->
						<div <?$type_flat_rent_var || $type_flat_sell_var || $type_room_rent_var || $type_room_sell_var?print'style="margin-left:130px"':''?> data-type="area" class="area filter_local">
							<a href="javascript:void(0)"><span class="icon"><i class="fa fa-plus-circle"></i></span><span class="text">Район</span></a>
							<?
							$ex_area_var = array();
							$valueInput = '';
							if($area_commercial_sell_var){
								$ex_area_var = explode(',',$area_commercial_sell_var);
								$valueInput = ' value="'.$area_commercial_sell_var.'"';
							}
							if($area_commercial_rent_var){
								$ex_area_var = explode(',',$area_commercial_rent_var);
								$valueInput = ' value="'.$area_commercial_rent_var.'"';
							}
							if($area_country_sell_var){
								$ex_area_var = explode(',',$area_country_sell_var);
								$valueInput = ' value="'.$area_country_sell_var.'"';
							}
							if($area_country_rent_var){
								$ex_area_var = explode(',',$area_country_rent_var);
								$valueInput = ' value="'.$area_country_rent_var.'"';
							}
							if($area_room_sell_var){
								$ex_area_var = explode(',',$area_room_sell_var);
								$valueInput = ' value="'.$area_room_sell_var.'"';
							}
							if($area_room_rent_var){
								$ex_area_var = explode(',',$area_room_rent_var);
								$valueInput = ' value="'.$area_room_rent_var.'"';
							}
							if($area_flat_sell_var){
								$ex_area_var = explode(',',$area_flat_sell_var);
								$valueInput = ' value="'.$area_flat_sell_var.'"';
							}
							if($area_flat_rent_var){
								$ex_area_var = explode(',',$area_flat_rent_var);
								$valueInput = ' value="'.$area_flat_rent_var.'"';
							}
							if($area_new_sell_var){
								$ex_area_var = explode(',',$area_new_sell_var);
								$valueInput = ' value="'.$area_new_sell_var.'"';
							}
							if($area_cession_sell_var){
								$ex_area_var = explode(',',$area_cession_sell_var);
								$valueInput = ' value="'.$area_cession_sell_var.'"';
							}
							?>
							<input type="hidden" name="area"<?=$valueInput?>>
							<div class="hidden_block">
								<div class="angles">
									<div class="angle-top-border"></div>
									<div class="angle-top-white"></div>
								</div>
								<div class="inner">
									<span onclick="return closeLocDial(this)" class="close"></span>
									<div class="btn_save top">
										<label class="btn"><input onclick="return list_areas_close(true,this)" type="button" value="Сохранить"><span class="angle-right"></span></label>
									</div>
									<div class="list_areas">
										<?
										$word = array();
										$data = array();
										$res = mysql_query("
											SELECT *
											FROM ".$template."_areas
											WHERE activation='1' && city_id='1'
											ORDER BY title
										");
										if(mysql_num_rows($res)>0){
											while($row = mysql_fetch_assoc($res)){
												$w = substr(rawurldecode($row['title']),0,2);
												if(!array_search($w,$word)){
													array_push($word,$w);
												}
												$arr = array(
													"id" => $row['id'],
													"title" => $row['title'],
													"word" => $w,
												);
												array_push($data,$arr);
											}
										}
										$column_count = ceil(count($data)/4);
										$n2 = 0;
										$newWord = array();
										$column = array();
										$count = 1;
										$areaChange = '';
										for($d=0; $d<count($data); $d++){
											$open_ul = '';
											$close_ul = '';
											if(!in_array($n2,$column)){
												array_push($column,$n2);
												$open_ul = '<ul>';
											}
											echo $open_ul;
											if(!in_array($data[$d]['word'],$newWord)){
												array_push($newWord,$data[$d]['word']);
												echo '<li class="title">'.$data[$d]['word'].'</li>';
											}
											$current = '';
											if(in_array($data[$d]['id'],$ex_area_var)){
												$current = ' class="current"';
												$areaChange .= '<li><a data-name="area" data-type="list_areas" data-id="'.$data[$d]['id'].'" href="javascript:void(0)">'.$data[$d]['title'].' рн.<span onclick="return delTags(this)" class="close"></span></a></li>';
											}
											echo '<li'.$current.'><a data-tags="'.$data[$d]['title'].' рн." data-id="'.$data[$d]['id'].'" href="javascript:void(0)"><span>'.$data[$d]['title'].'</span></a></li>';
											if($count==$column_count){
												if(!in_array($data[$d+1]['word'],$newWord)){
													$close_ul = '</ul>';
													$n2++;
													$count = 1;
												}
											}
											else {
												$count++;
											}
											echo $close_ul;
										}
										?>
									</div>
								</div>
							</div>
						</div>
						<div data-type="station" class="station filter_local">
							<a href="javascript:void(0)"><span class="icon"><i class="fa fa-plus-circle"></i></span><span class="text">Метро</span></a>
							<?
							$ex_station_var = array();
							$valueInput = '';
							if($station_commercial_sell_var){
								$ex_station_var = explode(',',$station_commercial_sell_var);
								$valueInput = ' value="'.$station_commercial_sell_var.'"';
							}
							if($station_commercial_rent_var){
								$ex_station_var = explode(',',$station_commercial_rent_var);
								$valueInput = ' value="'.$station_commercial_rent_var.'"';
							}
							if($station_country_sell_var){
								$ex_station_var = explode(',',$station_country_sell_var);
								$valueInput = ' value="'.$station_country_sell_var.'"';
							}
							if($station_country_rent_var){
								$ex_station_var = explode(',',$station_country_rent_var);
								$valueInput = ' value="'.$station_country_rent_var.'"';
							}
							if($station_room_sell_var){
								$ex_station_var = explode(',',$station_room_sell_var);
								$valueInput = ' value="'.$station_room_sell_var.'"';
							}
							if($station_room_rent_var){
								$ex_station_var = explode(',',$station_room_rent_var);
								$valueInput = ' value="'.$station_room_rent_var.'"';
							}
							if($station_flat_sell_var){
								$ex_station_var = explode(',',$station_flat_sell_var);
								$valueInput = ' value="'.$station_flat_sell_var.'"';
							}
							if($station_flat_rent_var){
								$ex_station_var = explode(',',$station_flat_rent_var);
								$valueInput = ' value="'.$station_flat_rent_var.'"';
							}
							if($station_cession_sell_var){
								$ex_station_var = explode(',',$station_cession_sell_var);
								$valueInput = ' value="'.$station_cession_sell_var.'"';
							}
							if($station_new_sell_var){
								$ex_station_var = explode(',',$station_new_sell_var);
								$valueInput = ' value="'.$station_new_sell_var.'"';
							}
							?>
							<input type="hidden" name="station"<?=$valueInput?>>
							<div class="hidden_block">
								<div class="angles">
									<div class="angle-top-border"></div>
									<div class="angle-top-white"></div>
								</div>
								<div class="inner">
									<span onclick="return closeLocDial(this)" class="close"></span>
									<div class="tabs_view">
										<ul>
											<li class="current"><a onclick="return tabsView(this)" data-type="map" href="javascript:void(0)"><span>Схема метрополитена</span></a></li>
											<li><a onclick="return tabsView(this)" data-type="list" href="javascript:void(0)"><span>Список станций</span></a></li>
											<li class="last">
												<div class="btn_save">
													<label class="btn"><input onclick="return list_areas_close(true,this)" type="button" value="Сохранить"><span class="angle-right"></span></label>
												</div>
											</li>
										</ul>
									</div>
									<div class="items map show">
										<div class="metro_stations">
											<div class="map_image">
												<img width="854" height="738" src="/images/Metro_map.png">
											</div>
											<div class="list_stations">
												<?
												if(count($ex_station_var)>0){
													$metroArray = $ex_station_var;
												}
												$id = 1;
												$res = mysql_query("
													SELECT t.title,b.coords,b.station_id AS id_station,b.parent_id, 
													(SELECT COUNT(id) FROM ".$template."_borders_station WHERE parent_id=b.id) AS parent
													FROM ".$template."_stations AS t
													LEFT JOIN ".$template."_borders_station AS b
													ON b.station_id=t.id && b.activation='1'
													WHERE t.city_id='".$id."' && t.activation='1'
													ORDER BY title, b.parent_id
												",$db) or die(mysql_error());
												$metro = '';
												$text = '';
												$change_metro = '';
												$metro_list = '';

												$map = '/images/Metro_map.png';
												$map_hover = '/images/Metro_map_hover.png';
												if(mysql_num_rows($res)>0){
													$parentCount = 0;
													$main_id = 0;
													$parent = -1;
													$parent_id = '';
													while($row = mysql_fetch_assoc($res)){ // $width.":".$height.":".$x1.":".$y1.":".$x2.":".$y2;
														$coords = explode(':',$row['coords']);
														$width = $coords[0];
														$height = $coords[1];
														$left = $coords[2];
														$top = $coords[3];
														$styles = 'background-image:none;background-position:-'.$left.'px -'.$top.'px;';
														$checked = '';
														if($metroArray){
															for($m=0; $m<count($metroArray); $m++){
																if($metroArray[$m]==$row['id_station'] || !empty($parent_id) && $parent_id==$metroArray[$m]){
																	$checked = ' current';
																	$styles = 'background-image:url('.$map_hover.');background-position:-'.$left.'px -'.$top.'px;background-repeat: no-repeat;';
																	break;
																}
															}
														}
														$q = '';
														if(!empty($row['parent'])){ // 3
															$main_id = $row['id_station'];
															$metro .= '<div data-main="'.$main_id.'" class="parent_block">';
															$q = ' parent';
															$parentCount = $row['parent'];
															$parent_id = $main_id;
														}
														if(!empty($parentCount)){
															$parent = $parent+1;
														}
														$metro .= '<div style="left:'.$left.'px;top:'.$top.'px" class="metro item'.$checked.''.$q.'"><a style="width:'.$width.'px;height:'.$height.'px;'.$styles.'" title="'.$row['title'].'" data-id="'.$row['id_station'].'" data-tags="'.$row['title'].'" href="javascript:void(0)"></a></div>';
														if($parent==$parentCount && !empty($parentCount)){
															$metro .= '</div>';
															// $metro .= '<span></span>';
															$parentCount = 0;
															$parent = -1;
															$parent_id = '';
														}
														
														$metro_list .= '<li><a href="javascript:void(0)" class="cities" data-values="'.$row['id_station'].'"><span class="element">'.$row['title'].'</span></a></li>';
													}
												}
												echo $metro;
												?>
											</div>
										</div>
									</div>
									<div class="items list">
										<div style="width:859px" class="list_areas">
											<?
											$word = array();
											$data = array();
											$res = mysql_query("
												SELECT *
												FROM ".$template."_stations
												WHERE activation='1'
												ORDER BY title
											");
											if(mysql_num_rows($res)>0){
												while($row = mysql_fetch_assoc($res)){
													$w = substr(rawurldecode($row['title']),0,2);
													if(!array_search($w,$word)){
														array_push($word,$w);
													}
													$arr = array(
														"id" => $row['id'],
														"title" => $row['title'],
														"word" => $w,
													);
													array_push($data,$arr);
												}
											}
											$column_count = ceil(count($data)/4);
											$n2 = 0;
											$newWord = array();
											$column = array();
											$count = 1;
											$stationChange = '';
											for($d=0; $d<count($data); $d++){
												$open_ul = '';
												$close_ul = '';
												if(!in_array($n2,$column)){
													array_push($column,$n2);
													$open_ul = '<ul style="width:24%">';
												}
												echo $open_ul;
												if(!in_array($data[$d]['word'],$newWord)){
													array_push($newWord,$data[$d]['word']);
													echo '<li class="title">'.$data[$d]['word'].'</li>';
												}
												$current = '';
												if(in_array($data[$d]['id'],$ex_station_var)){
													$current = ' class="current"';
													$stationChange .= '<li><a data-name="station" data-type="list_areas" data-id="'.$data[$d]['id'].'" href="javascript:void(0)">'.$data[$d]['title'].'<span onclick="return delTags(this)" class="close"></span></a></li>';
												}
												echo '<li'.$current.'><a data-tags="'.$data[$d]['title'].'" data-id="'.$data[$d]['id'].'" href="javascript:void(0)"><span>'.$data[$d]['title'].'</span></a></li>';
												if($count==$column_count){
													if(!in_array($data[$d+1]['word'],$newWord)){
														$close_ul = '</ul>';
														$n2++;
														$count = 1;
													}
												}
												else {
													$count++;
												}
												echo $close_ul;
											}
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="just_build" <?$estateValue==1 || $estateValue==6?print'style="display:inline-block"':print'style="display:none"'?> data-type="build">
							<div data-type="build" class="area filter_local">
								<a href="javascript:void(0)"><span class="icon"><i class="fa fa-plus-circle"></i></span><span class="text">ЖК</span></a>
								<?
								$ex_build_var = array();
								$valueInput = '';
								if($build_new_sell_var){
									$ex_build_var = explode(',',$build_new_sell_var);
									$valueInput = ' value="'.$build_new_sell_var.'"';
								}
								if($build_cession_sell_var){
									$ex_build_var = explode(',',$build_cession_sell_var);
									$valueInput = ' value="'.$build_cession_sell_var.'"';
								}
								?>
								<input type="hidden" name="build"<?=$valueInput?>>
								<div class="hidden_block">
									<div class="angles">
										<div class="angle-top-border"></div>
										<div class="angle-top-white"></div>
									</div>
									<div class="inner">
										<span onclick="return closeLocDial(this)" class="close"></span>
										<div class="btn_save top">
											<label class="btn"><input onclick="return list_areas_close(true,this)" type="button" value="Сохранить"><span class="angle-right"></span></label>
										</div>
										<div class="list_areas">
											<?
											$word = array();
											$data = array();
											$res = mysql_query("
												SELECT *
												FROM ".$template."_m_catalogue_left
												WHERE activation='1'
												ORDER BY name
											");
											if(mysql_num_rows($res)>0){
												while($row = mysql_fetch_assoc($res)){
													$name = trim(htmlspecialchars_decode($row['name']));
													$name = str_replace('ЖК ', '', $name);
													$name = str_replace('"', '', $name);
													$w = substr($name,0,2);
													if(mb_strlen($w,'UTF-8')==2){
														$w = substr($name,0,1);
													}
													if(!array_search($w,$word)){
														array_push($word,$w);
													}
													$arr = array(
														"id" => $row['id'],
														"title" => $name,
														"word" => $w,
													);
													array_push($data,$arr);
												}
											}
											$column_count = ceil(count($data)/5);
											$column_count_word = 0;
											$columns = mysql_query("
												SELECT *
												FROM ".$template."_build_info
												WHERE id='1'
											");
											if(mysql_num_rows($columns)>0){
												$column = mysql_fetch_assoc($columns);
												$column_count_word = $column['col'];
											}
											$n2 = 0;
											$newWord = array();
											$columnArray = array();
											$count = 1;
											$countColumn = 1;
											$buildChange = '';
											// for($d=0; $d<count($data); $d++){
												// $open_ul = '';
												// $close_ul = '';
												// if(!in_array($n2,$columnArray)){
													// array_push($columnArray,$n2);
													// $open_ul = '<ul>';
												// }
												// echo $open_ul;
												// if(!in_array($data[$d]['word'],$newWord)){
													// array_push($newWord,$data[$d]['word']);
													// echo '<li class="title">'.$data[$d]['word'].'</li>';
												// }
												// $current = '';
												// if(in_array($data[$d]['id'],$ex_build_var)){
													// $current = ' class="current"';
													// $buildChange .= '<li><a data-name="build" data-type="list_areas" data-id="'.$data[$d]['id'].'" href="javascript:void(0)">'.$data[$d]['title'].'<span onclick="return delTags(this)" class="close"></span></a></li>';
												// }
												// echo '<li'.$current.'><a data-tags="'.$data[$d]['title'].'" data-id="'.$data[$d]['id'].'" href="javascript:void(0)"><span>'.$data[$d]['title'].'</span></a></li>';
												// if($count==$column_count){
													// if(!in_array($data[$d+1]['word'],$newWord)){
														// $close_ul = '</ul>';
														// $n2++;
														// $count = 1;
													// }
												// }
												// else {
													// $count++;
												// }
												// echo $close_ul;
											// }
											for($g=1; $g<=$column_count_word; $g++){
												echo '<ul>';
												for($d=0; $d<count($data); $d++){
													$countThisCol = $column['column_'.$g];
													if($countThisCol<=$countThisCol){
														if(!in_array($data[$d]['word'],$newWord)){
															array_push($newWord,$data[$d]['word']);
															echo '<li class="title">'.$data[$d]['word'].'</li>';
															$countColumn++;
														}
														$current = '';
														if(in_array($data[$d]['id'],$ex_build_var)){
															$current = ' class="current"';
															$buildChange .= '<li><a data-name="build" data-type="list_areas" data-id="'.$data[$d]['id'].'" href="javascript:void(0)">'.$data[$d]['title'].'X<span onclick="return delTags(this)" class="close"></span></a></li>';
														}
														echo '<li'.$current.'><a data-tags="'.$data[$d]['title'].'" data-id="'.$data[$d]['id'].'" href="javascript:void(0)"><span>'.$data[$d]['title'].'</span></a></li>';
													}
												}
												echo '</ul>';
											}
											?>
										</div>
									</div>
								</div>
							</div>
							<div data-type="developer" class="area filter_local">
								<a href="javascript:void(0)"><span class="icon"><i class="fa fa-plus-circle"></i></span><span class="text">Застройщики</span></a>
								<?
								$ex_developer_var = array();
								$valueInput = '';
								if($developer_new_sell_var){
									$ex_developer_var = explode(',',$developer_new_sell_var);
									$valueInput = ' value="'.$developer_new_sell_var.'"';
								}
								if($developer_cession_sell_var){
									$ex_developer_var = explode(',',$developer_cession_sell_var);
									$valueInput = ' value="'.$developer_cession_sell_var.'"';
								}
								?>
								<input type="hidden" name="developer"<?=$valueInput?>>
								<div class="hidden_block">
									<div class="angles">
										<div class="angle-top-border"></div>
										<div class="angle-top-white"></div>
									</div>
									<div class="inner">
										<span onclick="return closeLocDial(this)" class="close"></span>
										<div class="btn_save top">
											<label class="btn"><input onclick="return list_areas_close(true,this)" type="button" value="Сохранить"><span class="angle-right"></span></label>
										</div>
										<div class="list_areas one_value_sec">
											<?
											$word = array();
											$data = array();
											$res = mysql_query("
												SELECT *
												FROM ".$template."_developers
												WHERE activation='1'
												ORDER BY name
											");
											if(mysql_num_rows($res)>0){
												while($row = mysql_fetch_assoc($res)){
													$name = trim(htmlspecialchars_decode($row['name']));
													$w = substr($name,0,2);
													if(mb_strlen($w,'UTF-8')==2){
														$w = substr($name,0,1);
													}
													if(!array_search($w,$word)){
														array_push($word,$w);
													}
													$arr = array(
														"id" => $row['id'],
														"title" => $name,
														"word" => $w,
													);
													array_push($data,$arr);
												}
											}
											$column_count = ceil(count($data)/5);
											$n2 = 0;
											$newWord = array();
											$column = array();
											$count = 1;
											$developerChange = '';
											for($d=0; $d<count($data); $d++){
												$open_ul = '';
												$close_ul = '';
												if(!in_array($n2,$column)){
													array_push($column,$n2);
													$open_ul = '<ul>';
												}
												echo $open_ul;
												if(!in_array($data[$d]['word'],$newWord)){
													array_push($newWord,$data[$d]['word']);
													echo '<li class="title">'.$data[$d]['word'].'</li>';
												}
												$current = '';
												if(in_array($data[$d]['id'],$ex_developer_var)){
													$current = ' class="current"';
													$developerChange .= '<li><a data-name="developer" data-type="list_areas" data-id="'.$data[$d]['id'].'" href="javascript:void(0)">'.$data[$d]['title'].'<span onclick="return delTags(this)" class="close"></span></a></li>';
												}
												echo '<li'.$current.'><a data-tags="'.$data[$d]['title'].'" data-id="'.$data[$d]['id'].'" href="javascript:void(0)"><span>'.$data[$d]['title'].'</span></a></li>';
												if($count==$column_count){
													if(!in_array($data[$d+1]['word'],$newWord)){
														$close_ul = '</ul>';
														$n2++;
														$count = 1;
													}
												}
												else {
													$count++;
												}
												echo $close_ul;
											}
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div <?$type_commercial_rent_var || $type_commercial_sell_var?print'style="margin-left:45px"':''?> class="advance_open">
						<a href="javascript:void(0)"><span>Расширенный поиск</span></a>
					</div>
				</div>
				<div class="btn_save">
					<label class="btn"><input type="submit" value="Подобрать"><span class="angle-right"></span></label>
					<div class="clear_filter"><span>Очистить поиск</span></div>
				</div>
			</div>
		</div>
		<div class="advanced_search">
			<div <?$estateValue!=1?print'style="display:none"':''?> class="inner_block new">
				<div class="row sell">
					<div class="left_block">
						<div class="metric">
							<div class="label_select">Общая площадь</div>
							<div class="select_block count">
								<?
								$squareChangeNewSell = '';
								if($arrayLinkSearch['squareFullMin'] && !$arrayLinkSearch['squareFullMax'] && $sellNewParam){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareFullMin'];
									$squareChangeNewSell = '<li><a class="min" data-name_select="fullsquare" data-name="squareFullMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $sellNewParam){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareFullMax'];
									$squareChangeNewSell = '<li><a class="max" data-name_select="fullsquare" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMax'].'" href="javascript:void(0)">общая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $sellNewParam){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareFullMin'];
									$c2 = $arrayLinkSearch['squareFullMax'];
									$squareChangeNewSell = '<li><a class="min max" data-name_select="fullsquare" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['squareFullMin']){
									$nameParams = $arrayLinkSearch['squareFullMin'];
									$currentParams = $arrayLinkSearch['squareFullMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMin'].'"';
								}
								?>
								<input type="hidden" name="squareFullMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$_min_value_square_full; $c<=$_max_value_square_full;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquare" data-explanation="общая" data-tags="от '.$c.' кв.м" data-type="min" data-id="'.$c.'" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$_max_value_square_full){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$_max_value_square_full){
												$c = $_max_value_square_full;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['squareFullMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareFullMax']);
									$currentParams = $arrayLinkSearch['squareFullMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMax'].'"';
								}
								?>
								<input type="hidden" name="squareFullMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										$n = 2;
										for($c=$_min_value_square_full; $c<=$_max_value_square_full;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquare" data-explanation="общая" data-tags="до '.$c.' кв.м" data-type="max" data-id="'.$c.'"  href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$_max_value_square_full){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$_max_value_square_full){
												$c = $_max_value_square_full;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div class="label_select">кухня</div>
							<div class="select_block count">
								<?
								$squareKitchenChangeNewSell = '';
								if($arrayLinkSearch['squareKitchenMin'] && !$arrayLinkSearch['squareKitchenMax'] && $sellNewParam){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeNewSell = '<li><a class="min" data-name_select="kitchensquare" data-name="squareKitchenMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMin'].'" href="javascript:void(0)">кухня от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareKitchenMin'] && $arrayLinkSearch['squareKitchenMax'] && $sellNewParam){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeNewSell = '<li><a class="max" data-name_select="kitchensquare" data-name="squareKitchenMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMax'].'" href="javascript:void(0)">кухня до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareKitchenMin'] && $arrayLinkSearch['squareKitchenMax'] && $sellNewParam){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareKitchenMin'];
									$c2 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeNewSell = '<li><a class="min max" data-name_select="kitchensquare" data-name="squareKitchenMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMin'].'" href="javascript:void(0)">кухня от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['squareKitchenMin']){
									$nameParams = $arrayLinkSearch['squareKitchenMin'];
									$currentParams = $arrayLinkSearch['squareKitchenMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareKitchenMin'].'"';
								}
								?>
								<input type="hidden" name="squareKitchenMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_kitchen_square_new_sell; $c<=$max_kitchen_square_new_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="kitchensquare" data-explanation="кухня" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['squareKitchenMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareKitchenMax']);
									$currentParams = $arrayLinkSearch['squareKitchenMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareKitchenMax'].'"';
								}
								?>
								<input type="hidden" name="squareKitchenMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_kitchen_square_new_sell; $c<=$max_kitchen_square_new_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="kitchensquare" data-explanation="кухня" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div class="label_select">Тип дома</div>
							<div class="select_block count">
								<?

								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['type_house']){
									$currentParams = (int)$arrayLinkSearch['type_house'];
									$valueParams = ' value="'.$arrayLinkSearch['type_house'].'"';
								}
								$type_house = '';
								$type_houseArray = array();;
								$devs = mysql_query("
									SELECT *, (SELECT name FROM ".$template."_type_house WHERE activation='1' && id='".$currentParams."') AS choose_value
									FROM ".$template."_type_house
									WHERE activation='1'
									ORDER BY num
								");
								if(mysql_num_rows($devs)>0){
									$n = 0;
									while($dev = mysql_fetch_assoc($devs)){
										if($n==0){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = 'Не важно';
											}
											$type_house .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
										}
										$current = '';
										if($currentParams==$dev['id']){
											$current = ' class="current"';
											$nameParams = $dev['name'];
										}
										$type_house .= '<li'.$current.'><a data-name="type_house" data-explanation="" data-tags="'.clearValueText($dev['name']).'" href="javascript:void(0)" data-id="'.$dev['id'].'">'.$dev['name'].'</a></li>';
										$type_houseArray[$dev['id']] = $dev['name'];										
										$n++;
									}
								}
								$type_houseChangeNewSell = '';
								if($arrayLinkSearch['type_house'] && $sellNewParam){
									$c1 = $arrayLinkSearch['type_house'];
									$type_houseChangeNewSell = '<li><a class="min" data-name_select="type_house" data-name="type_house" data-type="checkbox_block" data-id="'.$arrayLinkSearch['type_house'].'" href="javascript:void(0)">'.$type_houseArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								echo '<input type="hidden" name="type_house"'.$valueParams.'>';
								echo '<div style="width:140px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $type_house;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
					</div>
					<div class="center_block">
						<div class="floor">
							<div class="label_select">Этаж</div>
							<div class="select_block count">
								<?
								$floorChangeNewSell = '';
								if($arrayLinkSearch['floorMin'] && !$arrayLinkSearch['floorMax'] && $sellNewParam){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorMin'];
									$floorChangeNewSell = '<li><a class="min" data-name_select="floorsquare" data-name="floorMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $sellNewParam){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeNewSell = '<li><a class="max" data-name_select="floorsquare" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMax'].'" href="javascript:void(0)">этаж по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $sellNewParam){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorMin'];
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeNewSell = '<li><a class="min max" data-name_select="floorsquare" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['floorMin']){
									$nameParams = $arrayLinkSearch['floorMin'];
									$currentParams = $arrayLinkSearch['floorMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMin'].'"';
								}
								?>
								<input type="hidden" name="floorMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$max_floors_new_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquare" data-explanation="этаж" data-tags="с '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['floorMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorMax']);
									$currentParams = $arrayLinkSearch['floorMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMax'].'"';
								}
								?>
								<input type="hidden" name="floorMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_new_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquare" data-explanation="этаж" data-tags="по '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="floor">
							<div class="label_select">в доме</div>
							<div class="select_block count">
								<?
								$floorsChangeNewSell = '';
								if($arrayLinkSearch['floorsMin'] && !$arrayLinkSearch['floorsMax'] && $sellNewParam){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorsMin'];
									$floorsChangeNewSell = '<li><a class="min" data-name_select="floorscount" data-name="floorsMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">в доме от '.$c1.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $sellNewParam){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorsMax'];
									$floorsChangeNewSell = '<li><a class="max" data-name_select="floorscount" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMax'].'" href="javascript:void(0)">в доме по '.$c2.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $sellNewParam){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorsMin'];
									$c2 = $arrayLinkSearch['floorsMax'];
									$floorsChangeNewSell = '<li><a class="min max" data-name_select="floorscount" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">в доме от '.$c1.' по '.$c2.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['floorsMin']){
									$nameParams = $arrayLinkSearch['floorsMin'];
									$currentParams = $arrayLinkSearch['floorsMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorsMin'].'"';
								}
								?>
								<input type="hidden" name="floorsMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$max_floors_new_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorscount" data-explanation="в доме" data-tags="от '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['floorsMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorsMax']);
									$currentParams = $arrayLinkSearch['floorsMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorsMax'].'"';
								}
								?>
								<input type="hidden" name="floorsMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_new_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorscount" data-explanation="в доме" data-tags="до '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">этажей</div>
						</div>
						<div class="floor">
							<div class="check_block">
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_first_new_sell_var)){
									if($except_first_new_sell_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_first_new_sell_var.'"';
									}
								}
								?>
								<div class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_first"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме первого</span>
									</div>
								</div>
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_last_new_sell_var)){
									if($except_last_new_sell_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_last_new_sell_var.'"';
									}
								}
								?>
								<div class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_last"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме последнего</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="line"></div>
					<div class="right_block">
						<div class="metric">
							<div class="cell">
								<div style="width:46px;margin-left:-6px;" class="label_select">Отделка</div>
								<div class="select_block count">
									<?
									$finishingChangeNewSell = '';
									if($arrayLinkSearch['finishing'] && $sellNewParam){
										$c1 = $arrayLinkSearch['finishing'];
										$finishingChangeNewSell = '<li><a class="min" data-name_select="finishing" data-name="finishing" data-type="checkbox_block" data-id="'.$arrayLinkSearch['finishing'].'" href="javascript:void(0)">'.$_TYPE_FINISHING[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($sellNewParam && $finishing_new_sell_var){
										$currentParams = (int)$finishing_new_sell_var;
										$valueParams = ' value="'.$finishing_new_sell_var.'"';
									}
									$finishing = '';
									for($n=0; $n<count($_TYPE_FINISHING); $n++){
										if($n==0){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = $_TYPE_FINISHING[0];
											}
											$finishing .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_TYPE_FINISHING[0].'</a></li>';
										}
										else {
											$current = '';
											if($currentParams==$n){
												$current = ' class="current"';
												$nameParams = $_TYPE_FINISHING[$n];
											}
											$tooltip = '';
											if(!empty($_TYPE_FIN_DESCR[$n])){
												$tooltip = ' data-much="true" data-pos="left" data-hover="tooltip" data-title="'.$_TYPE_FIN_DESCR[$n].'"';
											}
											$finishing .= '<li'.$current.'><a'.$tooltip.' data-name="finishing" data-explanation="" data-tags="'.clearValueText($_TYPE_FINISHING[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_FINISHING[$n].'</a></li>';
										}
									}
									echo '<input type="hidden" name="finishing"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $finishing;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="metric">
							<!--<div class="select_block balcony">
								<?
								// $nameParams = 'Любой санузел';
								// $valueParams = '';
								// if($sellNewParam && $arrayLinkSearch['wc']){
									// $nameParams = $_TYPE_WC[$arrayLinkSearch['wc']];
									// $valueParams = ' value="'.$arrayLinkSearch['wc'].'"';
								// }
								?>
								<input type="hidden" name="wc"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<?
										// for($c=0; $c<count($_TYPE_WC); $c++){
											// $current = '';
											// if(!$wc_new_sell_var){
												// if($c==0){
													// $current = ' class="current"';
												// }
											// }
											// else {
												// if($c==$wc_new_sell_var){
													// $current = ' class="current"';
												// }
											// }
											// echo '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_WC[$c].'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>-->
							<div style="width:40px" class="label_select">Сдача</div>
							<!--<div class="select_block count">
								<input type="hidden" name="delivery" value="<?=$delivery_new_sell_var?>">
								<?
								// if(isset($delivery_new_sell_var) && !empty($delivery_new_sell_var)){
									// echo '<div style="width:95px" class="choosed_block">'.$delivery_new_sell_var.'</div>';
								// }
								// else {
									// echo '<div style="width:95px" class="choosed_block">Не важно</div>';
								// }
								?>
								<div class="scrollbar-inner">
									<ul>
										<?
										// for($c=date("Y"); $c<=$_max_value_delivery; $c++){
											// $current = '';
											// $currentNull = '';
											// if(!$delivery_new_sell_var){
												// if($c==date("Y")){
													// $currentNull = ' class="current"';
												// }
											// }
											// else {
												// if($c==$delivery_new_sell_var){
													// $current = ' class="current"';
												// }
											// }
											// if($c==date("Y")){
												// echo '<li'.$currentNull.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
											// }
											// echo '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>-->
							<div class="select_block count">
								<?
								
								$ex_min_value_delivery_new_sell = explode(' ',$min_value_delivery_new_sell);
								$ex_max_value_delivery_new_sell = explode(' ',$max_value_delivery_new_sell);
								$_min_value_delivery_year = (int)$ex_min_value_delivery_new_sell[1];
								$_max_value_delivery_year = (int)$ex_max_value_delivery_new_sell[1];

								$n = 1;
								$deliveryAdd = '';
								$deliveryArrayMin = '';
								$_QUATRE = array("I","II","III","IV");
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['deliveryMin']){
									$currentParams = $arrayLinkSearch['deliveryMin'];
								}
								for($c=$_min_value_delivery_year; $c<=$_max_value_delivery_year; $c++){
									$min_search = array_search($ex_min_value_delivery_new_sell[0],$_QUATRE);
									$max_search = array_search($ex_max_value_delivery_new_sell[0],$_QUATRE);
									if($c==$_min_value_delivery_year){
										for($q=$min_search; $q<count($_QUATRE); $q++){
											$q_name = $_QUATRE[$q]." кв. ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMin = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="сдача" data-tags="'.$q_name.'" data-type="min" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
									else if($c==$_max_value_delivery_year){
										for($q=0; $q<=$max_search; $q++){
											$q_name = $_QUATRE[$q]." кв. ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMin = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="сдача" data-tags="'.$q_name.'" data-type="min" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
									else {
										for($q=0; $q<count($_QUATRE); $q++){
											$q_name = $_QUATRE[$q]." кв. ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMin = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="сдача" data-tags="'.$q_name.'" data-type="min" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
								}
								$nameParams = 'От';
								$valueParams = '';
								if($sellNewParam && $arrayLinkSearch['deliveryMin']){
									$nameParams = $arrayLinkSearch['deliveryMin'];
									if(!empty($deliveryArrayMin)){
										$nameParams = $deliveryArrayMin;
									}
									$currentParams = $arrayLinkSearch['deliveryMin'];
									$valueParams = ' value="'.$arrayLinkSearch['deliveryMin'].'"';
								}
								?>
								<input type="hidden" name="deliveryMin"<?=$valueParams?>>
								<div style="width:85px" class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?=$deliveryAdd?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$n = 1;
								$deliveryAdd = '';
								$deliveryArrayMax = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['deliveryMax']){
									$currentParams = $arrayLinkSearch['deliveryMax'];
								}
								for($c=$_min_value_delivery_year; $c<=$_max_value_delivery_year; $c++){
									$min_search = array_search($ex_min_value_delivery_new_sell[0],$_QUATRE);
									$max_search = array_search($ex_max_value_delivery_new_sell[0],$_QUATRE);
									if($c==$_min_value_delivery_year){
										for($q=$min_search; $q<count($_QUATRE); $q++){
											$q_name = $_QUATRE[$q]." кв. ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMax = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="сдача" data-tags="по '.$q_name.'" data-type="max" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
									else if($c==$_max_value_delivery_year){
										for($q=0; $q<=$max_search; $q++){
											$q_name = $_QUATRE[$q]." кв. ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMax = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="сдача" data-tags="по '.$q_name.'" data-type="max" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
									else {
										for($q=0; $q<count($_QUATRE); $q++){
											$q_name = $_QUATRE[$q]." кв. ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMax = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="сдача" data-tags="по '.$q_name.'" data-type="max" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
								}

								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['deliveryMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['deliveryMax']);
									if(!empty($deliveryArrayMax)){
										$nameParams = $deliveryArrayMax;
									}
									$currentParams = $arrayLinkSearch['deliveryMax'];
									$valueParams = ' value="'.$arrayLinkSearch['deliveryMax'].'"';
								}
								
								$deliveryChangeNewSell = '';
								if($arrayLinkSearch['deliveryMin'] && !$arrayLinkSearch['deliveryMax'] && $sellNewParam){ //только метраж "от"
									$c1 = $deliveryArrayMin;
									$deliveryChangeNewSell = '<li><a class="min" data-name_select="delivery" data-name="deliveryMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['deliveryMin'].'" href="javascript:void(0)">сдача с '.$c1.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['deliveryMin'] && $arrayLinkSearch['deliveryMax'] && $sellNewParam){ //только метраж "до"
									$c2 = $deliveryArrayMax;
									$deliveryChangeNewSell = '<li><a class="max" data-name_select="delivery" data-name="deliveryMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['deliveryMax'].'" href="javascript:void(0)">сдача по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['deliveryMin'] && $arrayLinkSearch['deliveryMax'] && $sellNewParam){ //промежуток метражей
									$c1 = $deliveryArrayMin;
									$c2 = $deliveryArrayMax;
									$deliveryChangeNewSell = '<li><a class="min max" data-name_select="delivery" data-name="deliveryMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['deliveryMin'].'" href="javascript:void(0)">сдача с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								?>
								<input type="hidden" name="deliveryMax"<?=$valueParams?>>
								<div style="width:85px" class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?=$deliveryAdd?>
									</ul>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="metric">
							<div class="cell">
								<div style="width:40px" class="label_select">Стадия</div>
								<div class="select_block count">
									<?
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($sellNewParam && $arrayLinkSearch['ready']){
										$currentParams = (int)$arrayLinkSearch['ready'];
										$valueParams = ' value="'.$arrayLinkSearch['ready'].'"';
									}
									$ready = '';
									$readyArray = array();
									$devs = mysql_query("
										SELECT *, (SELECT name FROM ".$template."_ready WHERE activation='1' && id='".$currentParams."') AS choose_value
										FROM ".$template."_ready
										WHERE activation='1'
										ORDER BY num
									");
									if(mysql_num_rows($devs)>0){
										$n = 0;
										while($dev = mysql_fetch_assoc($devs)){
											if($n==0){
												$current = '';
												if($currentParams==0){
													$current = ' class="current"';
													$nameParams = 'Не важно';
												}
												$ready .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
											}
											// else {
												$current = '';
												if($currentParams==$dev['id']){
													$current = ' class="current"';
													$nameParams = $dev['name'];
												}
												$ready .= '<li'.$current.'><a data-name="ready" data-explanation="" data-tags="'.clearValueText($dev['name']).'" href="javascript:void(0)" data-id="'.$dev['id'].'">'.$dev['name'].'</a></li>';
											// }
											$readyArray[$dev['id']] = $dev['name'];
											$n++;
										}
									}
									$readyChangeNewSell = '';
									if($arrayLinkSearch['ready'] && $sellNewParam){
										$c1 = $arrayLinkSearch['ready'];
										$readyChangeNewSell = '<li><a class="min" data-name_select="ready" data-name="ready" data-type="checkbox_block" data-id="'.$arrayLinkSearch['ready'].'" href="javascript:void(0)">'.$readyArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									echo '<input type="hidden" name="ready"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $ready;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<div class="cell">
								<div style="width:60px" class="label_select">До метро</div>
								<div class="select_block count">
									<?
									$far_subwayChangeNewSell = '';
									if($arrayLinkSearch['far_subway'] && $sellNewParam){
										$c1 = $arrayLinkSearch['far_subway'];
										$name_value = $_FAR_SUBWAY[$c1];
										if($c1==1){
											$name_value = '< 500 м';
										}
										$far_subwayChangeNewSell = '<li><a class="min" data-name_select="far_subway" data-name="far_subway" data-type="checkbox_block" data-id="'.$arrayLinkSearch['far_subway'].'" href="javascript:void(0)">До метро '.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($sellNewParam && $arrayLinkSearch['far_subway']){
										$currentParams = (int)$arrayLinkSearch['far_subway'];
										$valueParams = ' value="'.$arrayLinkSearch['far_subway'].'"';
									}
									$far_subway = '';
									for($f=1; $f<=count($_FAR_SUBWAY); $f++){
										if($f==1){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = 'Не важно';
											}
											$far_subway .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
										}
										$current = '';
										if($currentParams==$f){
											$current = ' class="current"';
											$nameParams = $_FAR_SUBWAY[$f];
										}
										$name_value = $_FAR_SUBWAY[$f];
										if($f==1){
											$name_value = '< 500 м';
										}
										$far_subway .= '<li'.$current.'><a data-name="far_subway" data-explanation="До метро" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'">'.$_FAR_SUBWAY[$f].'</a></li>';
									}
									echo '<input type="hidden" name="far_subway"'.$valueParams.'>';
									echo '<div style="width:100px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $far_subway;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<!--<div class="select_block balcony">
								<input type="hidden" name="balcony" value="0">
								<div class="choosed_block">Балкон не важен</div>
								<div class="scrollbar-inner">
									<ul>
										<?
										// for($c=0; $c<count($_TYPE_BALCONY); $c++){
											// $current = '';
											// if(!$balcony_new_sell_var){
												// if($c==0){
													// $current = ' class="current"';
												// }
											// }
											// else {
												// if($c==$balcony_new_sell_var){
													// $current = ' class="current"';
												// }
											// }
											// echo '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_BALCONY[$c].'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>-->
						</div>
					</div>
				</div>
			</div>
			<div <?$estateValue!=6?print'style="display:none"':''?> class="inner_block cession">
				<div class="row sell">
					<div class="left_block">
						<div class="metric">
							<div class="label_select">Общая площадь</div>
							<div class="select_block count">
								<?
								$squareChangeCessionSell = '';
								if($arrayLinkSearch['squareFullMin'] && !$arrayLinkSearch['squareFullMax'] && $type_cession_sell_var){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareFullMin'];
									$squareChangeCessionSell = '<li><a class="min" data-name_select="fullsquare" data-name="squareFullMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_cession_sell_var){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareFullMax'];
									$squareChangeCessionSell = '<li><a class="max" data-name_select="fullsquare" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMax'].'" href="javascript:void(0)">общая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_cession_sell_var){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareFullMin'];
									$c2 = $arrayLinkSearch['squareFullMax'];
									$squareChangeCessionSell = '<li><a class="min max" data-name_select="fullsquare" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['squareFullMin']){
									$nameParams = $arrayLinkSearch['squareFullMin'];
									$currentParams = $arrayLinkSearch['squareFullMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMin'].'"';
								}
								?>
								<input type="hidden" name="squareFullMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$_min_value_square_full_cession; $c<=$_max_value_square_full_cession;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquare" data-explanation="общая" data-tags="от '.$c.' кв.м" data-type="min" data-id="'.$c.'" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$_max_value_square_full_cession){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$_max_value_square_full_cession){
												$c = $_max_value_square_full_cession;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['squareFullMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareFullMax']);
									$currentParams = $arrayLinkSearch['squareFullMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMax'].'"';
								}
								?>
								<input type="hidden" name="squareFullMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										$n = 2;
										for($c=$_min_value_square_full_cession; $c<=$_max_value_square_full_cession;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquare" data-explanation="общая" data-tags="до '.$c.' кв.м" data-type="max" data-id="'.$c.'"  href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$_max_value_square_full_cession){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$_max_value_square_full_cession){
												$c = $_max_value_square_full_cession;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div class="label_select">кухня</div>
							<div class="select_block count">
								<?
								$squareKitchenChangeCessionSell = '';
								if($arrayLinkSearch['squareKitchenMin'] && !$arrayLinkSearch['squareKitchenMax'] && $type_cession_sell_var){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeCessionSell = '<li><a class="min" data-name_select="kitchensquare" data-name="squareKitchenMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMin'].'" href="javascript:void(0)">кухня от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareKitchenMin'] && $arrayLinkSearch['squareKitchenMax'] && $type_cession_sell_var){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeCessionSell = '<li><a class="max" data-name_select="kitchensquare" data-name="squareKitchenMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMax'].'" href="javascript:void(0)">кухня до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareKitchenMin'] && $arrayLinkSearch['squareKitchenMax'] && $type_cession_sell_var){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareKitchenMin'];
									$c2 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeCessionSell = '<li><a class="min max" data-name_select="kitchensquare" data-name="squareKitchenMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMin'].'" href="javascript:void(0)">кухня от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['squareKitchenMin']){
									$nameParams = $arrayLinkSearch['squareKitchenMin'];
									$currentParams = $arrayLinkSearch['squareKitchenMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareKitchenMin'].'"';
								}
								?>
								<input type="hidden" name="squareKitchenMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										if($max_kitchen_square_cession_sell==0){
											$max_kitchen_square_cession_sell = 10;
										}
										for($c=$min_kitchen_square_cession_sell; $c<=$max_kitchen_square_cession_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="kitchensquare" data-explanation="кухня" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['squareKitchenMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareKitchenMax']);
									$currentParams = $arrayLinkSearch['squareKitchenMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareKitchenMax'].'"';
								}
								?>
								<input type="hidden" name="squareKitchenMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										if($max_kitchen_square_cession_sell==0){
											$max_kitchen_square_cession_sell = 10;
										}
										for($c=$min_kitchen_square_cession_sell; $c<=$max_kitchen_square_cession_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="kitchensquare" data-explanation="кухня" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div class="label_select">Тип дома</div>
							<div class="select_block count">
								<?
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['type_house']){
									$currentParams = (int)$arrayLinkSearch['type_house'];
									$valueParams = ' value="'.$arrayLinkSearch['type_house'].'"';
								}
								$type_house = '';
								$type_houseArray = array();;
								$devs = mysql_query("
									SELECT *, (SELECT name FROM ".$template."_type_house WHERE activation='1' && id='".$currentParams."') AS choose_value
									FROM ".$template."_type_house
									WHERE activation='1'
									ORDER BY num
								");
								if(mysql_num_rows($devs)>0){
									$n = 0;
									while($dev = mysql_fetch_assoc($devs)){
										if($n==0){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = 'Не важно';
											}
											$type_house .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
										}
										$current = '';
										if($currentParams==$dev['id']){
											$current = ' class="current"';
											$nameParams = $dev['name'];
										}
										$type_house .= '<li'.$current.'><a data-name="type_house" data-explanation="" data-tags="'.clearValueText($dev['name']).'" href="javascript:void(0)" data-id="'.$dev['id'].'">'.$dev['name'].'</a></li>';
										$type_houseArray[$dev['id']] = $dev['name'];										
										$n++;
									}
								}
								$type_houseChangeCessionSell = '';
								if($arrayLinkSearch['type_house'] && $type_cession_sell_var){
									$c1 = $arrayLinkSearch['type_house'];
									$type_houseChangeCessionSell = '<li><a class="min" data-name_select="type_house" data-name="type_house" data-type="checkbox_block" data-id="'.$arrayLinkSearch['type_house'].'" href="javascript:void(0)">'.$type_houseArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								echo '<input type="hidden" name="type_house"'.$valueParams.'>';
								echo '<div style="width:140px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $type_house;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
					</div>
					<div class="center_block">
						<div class="floor">
							<div class="label_select">Этаж</div>
							<div class="select_block count">
								<?
								$floorChangeCessionSell = '';
								if($arrayLinkSearch['floorMin'] && !$arrayLinkSearch['floorMax'] && $type_cession_sell_var){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorMin'];
									$floorChangeCessionSell = '<li><a class="min" data-name_select="floorsquare" data-name="floorMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_cession_sell_var){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeCessionSell = '<li><a class="max" data-name_select="floorsquare" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMax'].'" href="javascript:void(0)">этаж по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_cession_sell_var){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorMin'];
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeCessionSell = '<li><a class="min max" data-name_select="floorsquare" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['floorMin']){
									$nameParams = $arrayLinkSearch['floorMin'];
									$currentParams = $arrayLinkSearch['floorMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMin'].'"';
								}
								?>
								<input type="hidden" name="floorMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$max_floors_cession_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquare" data-explanation="этаж" data-tags="с '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['floorMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorMax']);
									$currentParams = $arrayLinkSearch['floorMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMax'].'"';
								}
								?>
								<input type="hidden" name="floorMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_cession_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquare" data-explanation="этаж" data-tags="по '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="floor">
							<div class="label_select">в доме</div>
							<div class="select_block count">
								<?
								$floorsChangeCessionSell = '';
								if($arrayLinkSearch['floorsMin'] && !$arrayLinkSearch['floorsMax'] && $type_cession_sell_var){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorsMin'];
									$floorsChangeCessionSell = '<li><a class="min" data-name_select="floorscount" data-name="floorsMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">в доме от '.$c1.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $type_cession_sell_var){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorsMax'];
									$floorsChangeCessionSell = '<li><a class="max" data-name_select="floorscount" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMax'].'" href="javascript:void(0)">в доме по '.$c2.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $type_cession_sell_var){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorsMin'];
									$c2 = $arrayLinkSearch['floorsMax'];
									$floorsChangeCessionSell = '<li><a class="min max" data-name_select="floorscount" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">в доме от '.$c1.' по '.$c2.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['floorsMin']){
									$nameParams = $arrayLinkSearch['floorsMin'];
									$currentParams = $arrayLinkSearch['floorsMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorsMin'].'"';
								}
								?>
								<input type="hidden" name="floorsMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$max_floors_cession_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorscount" data-explanation="в доме" data-tags="от '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['floorsMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorsMax']);
									$currentParams = $arrayLinkSearch['floorsMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorsMax'].'"';
								}
								?>
								<input type="hidden" name="floorsMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_cession_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorscount" data-explanation="в доме" data-tags="до '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">этажей</div>
						</div>
						<div class="floor">
							<div class="check_block">
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_first_cession_sell_var)){
									if($except_first_cession_sell_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_first_cession_sell_var.'"';
									}
								}
								?>
								<div class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_first"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме первого</span>
									</div>
								</div>
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_last_cession_sell_var)){
									if($except_last_cession_sell_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_last_cession_sell_var.'"';
									}
								}
								?>
								<div class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_last"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме последнего</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="line"></div>
					<div class="right_block">
						<div class="metric">
							<div class="cell">
								<div style="width:46px;margin-left:-6px;" class="label_select">Отделка</div>
								<div class="select_block count">
									<?
									$finishingChangeCessionSell = '';
									if($arrayLinkSearch['finishing'] && $finishing_cession_sell_var){
										$c1 = $arrayLinkSearch['finishing'];
										$finishingChangeCessionSell = '<li><a class="min" data-name_select="finishing" data-name="finishing" data-type="checkbox_block" data-id="'.$arrayLinkSearch['finishing'].'" href="javascript:void(0)">'.$_TYPE_FINISHING[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($finishing_cession_sell_var && $finishing_cession_sell_var){
										$currentParams = (int)$finishing_cession_sell_var;
										$valueParams = ' value="'.$finishing_cession_sell_var.'"';
									}
									$finishing = '';
									for($n=0; $n<count($_TYPE_FINISHING); $n++){
										if($n==0){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = $_TYPE_FINISHING[0];
											}
											$finishing .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_TYPE_FINISHING[0].'</a></li>';
										}
										else {
											$current = '';
											if($currentParams==$n){
												$current = ' class="current"';
												$nameParams = $_TYPE_FINISHING[$n];
											}
											$tooltip = '';
											if(!empty($_TYPE_FIN_DESCR[$n])){
												$tooltip = ' data-much="true" data-pos="left" data-hover="tooltip" data-title="'.$_TYPE_FIN_DESCR[$n].'"';
											}
											$finishing .= '<li'.$current.'><a'.$tooltip.' data-name="finishing" data-explanation="" data-tags="'.clearValueText($_TYPE_FINISHING[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_FINISHING[$n].'</a></li>';
										}
									}
									echo '<input type="hidden" name="finishing"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $finishing;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="metric">
							<!--<div class="select_block balcony">
								<?
								// $nameParams = 'Любой санузел';
								// $valueParams = '';
								// if($type_cession_sell_var && $arrayLinkSearch['wc']){
									// $nameParams = $_TYPE_WC[$arrayLinkSearch['wc']];
									// $valueParams = ' value="'.$arrayLinkSearch['wc'].'"';
								// }
								?>
								<input type="hidden" name="wc"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<?
										// for($c=0; $c<count($_TYPE_WC); $c++){
											// $current = '';
											// if(!$wc_cession_sell_var){
												// if($c==0){
													// $current = ' class="current"';
												// }
											// }
											// else {
												// if($c==$wc_cession_sell_var){
													// $current = ' class="current"';
												// }
											// }
											// echo '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_WC[$c].'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>-->
							<div style="width:40px" class="label_select">Сдача</div>
							<!--<div class="select_block count">
								<input type="hidden" name="delivery" value="<?=$delivery_cession_sell_var?>">
								<?
								// if(isset($delivery_cession_sell_var) && !empty($delivery_cession_sell_var)){
									// echo '<div style="width:95px" class="choosed_block">'.$delivery_cession_sell_var.'</div>';
								// }
								// else {
									// echo '<div style="width:95px" class="choosed_block">Не важно</div>';
								// }
								?>
								<div class="scrollbar-inner">
									<ul>
										<?
										// for($c=date("Y"); $c<=$_max_value_delivery; $c++){
											// $current = '';
											// $currentNull = '';
											// if(!$delivery_cession_sell_var){
												// if($c==date("Y")){
													// $currentNull = ' class="current"';
												// }
											// }
											// else {
												// if($c==$delivery_cession_sell_var){
													// $current = ' class="current"';
												// }
											// }
											// if($c==date("Y")){
												// echo '<li'.$currentNull.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
											// }
											// echo '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>-->
							
							
							
							
							
							<div class="select_block count">
								<?
								
								$ex_min_value_delivery_cession_sell = explode(' ',$min_value_delivery_cession_sell);
								$ex_max_value_delivery_cession_sell = explode(' ',$max_value_delivery_cession_sell);
								$_min_value_delivery_year = (int)$ex_min_value_delivery_cession_sell[1];
								$_max_value_delivery_year = (int)$ex_max_value_delivery_cession_sell[1];

								$n = 1;
								$deliveryAdd = '';
								$deliveryArrayMin = '';
								$_QUATRE = array("I","II","III","IV");
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['deliveryMin']){
									$currentParams = $arrayLinkSearch['deliveryMin'];
								}
								for($c=$_min_value_delivery_year; $c<=$_max_value_delivery_year; $c++){
									$min_search = array_search($ex_min_value_delivery_cession_sell[0],$_QUATRE);
									$max_search = array_search($ex_max_value_delivery_cession_sell[0],$_QUATRE);
									if($c==$_min_value_delivery_year){
										for($q=$min_search; $q<count($_QUATRE); $q++){
											$q_name = $_QUATRE[$q]." ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMin = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="сдача" data-tags="с '.$q_name.'" data-type="min" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
									else if($c==$_max_value_delivery_year){
										for($q=0; $q<=$max_search; $q++){
											$q_name = $_QUATRE[$q]." ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMin = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="сдача" data-tags="с '.$q_name.'" data-type="min" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
									else {
										for($q=0; $q<count($_QUATRE); $q++){
											$q_name = $_QUATRE[$q]." ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMin = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="сдача" data-tags="с '.$q_name.'" data-type="min" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
								}

								$nameParams = 'От';
								$valueParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['deliveryMin']){
									$nameParams = $arrayLinkSearch['deliveryMin'];
									if(!empty($deliveryArrayMin)){
										$nameParams = $deliveryArrayMin;
									}
									$currentParams = $arrayLinkSearch['deliveryMin'];
									$valueParams = ' value="'.$arrayLinkSearch['deliveryMin'].'"';
								}
								
								?>
								<input type="hidden" name="deliveryMin"<?=$valueParams?>>
								<div style="width:85px" class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?=$deliveryAdd?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$n = 1;
								$deliveryAdd = '';
								$deliveryArrayMax = '';
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['deliveryMax']){
									$currentParams = $arrayLinkSearch['deliveryMax'];
								}
								for($c=$_min_value_delivery_year; $c<=$_max_value_delivery_year; $c++){
									$min_search = array_search($ex_min_value_delivery_cession_sell[0],$_QUATRE);
									$max_search = array_search($ex_max_value_delivery_cession_sell[0],$_QUATRE);
									if($c==$_min_value_delivery_year){
										for($q=$min_search; $q<count($_QUATRE); $q++){
											$q_name = $_QUATRE[$q]." ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMax = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="сдача по" data-tags="по '.$q_name.'" data-type="max" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
									else if($c==$_max_value_delivery_year){
										for($q=0; $q<=$max_search; $q++){
											$q_name = $_QUATRE[$q]." ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMax = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="сдача" data-tags="по '.$q_name.'" data-type="max" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
									else {
										for($q=0; $q<count($_QUATRE); $q++){
											$q_name = $_QUATRE[$q]." ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMax = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="сдача" data-tags="по '.$q_name.'" data-type="max" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
								}
								
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['deliveryMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['deliveryMax']);
									if(!empty($deliveryArrayMax)){
										$nameParams = $deliveryArrayMax;
									}
									$currentParams = $arrayLinkSearch['deliveryMax'];
									$valueParams = ' value="'.$arrayLinkSearch['deliveryMax'].'"';
								}
								
								$deliveryChangeCessionSell = '';
								if($arrayLinkSearch['deliveryMin'] && !$arrayLinkSearch['deliveryMax'] && $type_cession_sell_var){ //только метраж "от"
									$c1 = $deliveryArrayMin;
									$deliveryChangeCessionSell = '<li><a class="min" data-name_select="delivery" data-name="deliveryMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['deliveryMin'].'" href="javascript:void(0)">сдача '.str_replace(".", ",", $c1).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['deliveryMin'] && $arrayLinkSearch['deliveryMax'] && $type_cession_sell_var){ //только метраж "до"
									$c2 = $deliveryArrayMax;
									$deliveryChangeCessionSell = '<li><a class="max" data-name_select="delivery" data-name="deliveryMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['deliveryMax'].'" href="javascript:void(0)">сдача '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['deliveryMin'] && $arrayLinkSearch['deliveryMax'] && $type_cession_sell_var){ //промежуток метражей
									$c1 = $deliveryArrayMin;
									$c2 = $deliveryArrayMax;
									$deliveryChangeCessionSell = '<li><a class="min max" data-name_select="delivery" data-name="deliveryMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['deliveryMin'].'" href="javascript:void(0)">сдача с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								?>
								<input type="hidden" name="deliveryMax"<?=$valueParams?>>
								<div style="width:85px" class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?=$deliveryAdd?>
									</ul>
								</div>
							</div>
							
							
							
							
							
							
						</div>
						<div class="clear"></div>
						<div class="metric">
							<div class="cell">
								<div style="width:40px" class="label_select">Стадия</div>
								<div class="select_block count">
									<?
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_cession_sell_var && $arrayLinkSearch['ready']){
										$currentParams = (int)$arrayLinkSearch['ready'];
										$valueParams = ' value="'.$arrayLinkSearch['ready'].'"';
									}
									$ready = '';
									$readyArray = array();
									$devs = mysql_query("
										SELECT *, (SELECT name FROM ".$template."_ready WHERE activation='1' && id='".$currentParams."') AS choose_value
										FROM ".$template."_ready
										WHERE activation='1'
										ORDER BY num
									");
									if(mysql_num_rows($devs)>0){
										$n = 0;
										while($dev = mysql_fetch_assoc($devs)){
											if($n==0){
												$current = '';
												if($currentParams==0){
													$current = ' class="current"';
													$nameParams = 'Не важно';
												}
												$ready .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
											}
											// else {
												$current = '';
												if($currentParams==$dev['id']){
													$current = ' class="current"';
													$nameParams = $dev['name'];
												}
												$ready .= '<li'.$current.'><a data-name="ready" data-explanation="" data-tags="'.clearValueText($dev['name']).'" href="javascript:void(0)" data-id="'.$dev['id'].'">'.$dev['name'].'</a></li>';
											// }
											$readyArray[$dev['id']] = $dev['name'];
											$n++;
										}
									}
									$readyChangeCessionSell = '';
									if($arrayLinkSearch['ready'] && $type_cession_sell_var){
										$c1 = $arrayLinkSearch['ready'];
										$readyChangeCessionSell = '<li><a class="min" data-name_select="ready" data-name="ready" data-type="checkbox_block" data-id="'.$arrayLinkSearch['ready'].'" href="javascript:void(0)">'.$readyArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									echo '<input type="hidden" name="ready"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $ready;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<div class="cell">
								<div style="width:60px" class="label_select">До метро</div>
								<div class="select_block count">
									<?
									$far_subwayChangeCessionSell = '';
									if($arrayLinkSearch['far_subway'] && $type_cession_sell_var){
										$c1 = $arrayLinkSearch['far_subway'];
										$name_value = $_FAR_SUBWAY[$c1];
										if($c1==1){
											$name_value = '< 500 м';
										}
										$far_subwayChangeCessionSell = '<li><a class="min" data-name_select="far_subway" data-name="far_subway" data-type="checkbox_block" data-id="'.$arrayLinkSearch['far_subway'].'" href="javascript:void(0)">До метро '.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_cession_sell_var && $arrayLinkSearch['far_subway']){
										$currentParams = (int)$arrayLinkSearch['far_subway'];
										$valueParams = ' value="'.$arrayLinkSearch['far_subway'].'"';
									}
									$far_subway = '';
									for($f=1; $f<=count($_FAR_SUBWAY); $f++){
										if($f==1){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = 'Не важно';
											}
											$far_subway .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
										}
										$current = '';
										if($currentParams==$f){
											$current = ' class="current"';
											$nameParams = $_FAR_SUBWAY[$f];
										}
										$name_value = $_FAR_SUBWAY[$f];
										if($f==1){
											$name_value = '< 500 м';
										}
										$far_subway .= '<li'.$current.'><a data-name="far_subway" data-explanation="До метро" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'">'.$_FAR_SUBWAY[$f].'</a></li>';
									}
									echo '<input type="hidden" name="far_subway"'.$valueParams.'>';
									echo '<div style="width:100px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $far_subway;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<!--<div class="select_block balcony">
								<input type="hidden" name="balcony" value="0">
								<div class="choosed_block">Балкон не важен</div>
								<div class="scrollbar-inner">
									<ul>
										<?
										// for($c=0; $c<count($_TYPE_BALCONY); $c++){
											// $current = '';
											// if(!$balcony_cession_sell_var){
												// if($c==0){
													// $current = ' class="current"';
												// }
											// }
											// else {
												// if($c==$balcony_cession_sell_var){
													// $current = ' class="current"';
												// }
											// }
											// echo '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_BALCONY[$c].'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>-->
						</div>
					</div>
				</div>
			</div>
			<div <?$estateValue!=2?print'style="display:none"':'style="display:block"'?> class="inner_block flat">
				<div class="row rent">
					<div class="select_block type">
						<input type="hidden" name="period">
						<div class="choosed_block">Любой срок</div>
						<div class="scrollbar-inner">
							<ul>
								<li class="current"><a href="javascript:void(0)" data-id="0">Любой срок</a></li>
								<?
								for($c=1; $c<=count($_TYPE_PERIOD_RENT); $c++){
									echo '<li><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_PERIOD_RENT[$c].'</a></li>';
								}
								?>
							</ul>
						</div>
					</div>
					<div class="select_block type">
						<input type="hidden" name="prepayment">
						<div class="choosed_block">Любая предоплата</div>
						<div class="scrollbar-inner">
							<ul>
								<li class="current"><a href="javascript:void(0)" data-id="0">Любая предоплата</a></li>
								<?
								for($c=1; $c<=count($_TYPE_PREPAYMENT); $c++){
									echo '<li><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_PREPAYMENT[$c].'</a></li>';
								}
								?>
							</ul>
						</div>
					</div>
					<div class="select_block type">
						<input type="hidden" name="deposit">
						<div class="choosed_block">Залог не важен</div>
						<div class="scrollbar-inner">
							<ul>
								<li class="current"><a href="javascript:void(0)" data-id="0">Залог не важен</a></li>
								<?
								for($c=1; $c<=count($_TYPE_DEPOSIT); $c++){
									echo '<li><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_DEPOSIT[$c].'</a></li>';
								}
								?>
							</ul>
						</div>
					</div>
					<div class="can_block">
						<?
						$checked = '';
						$valueParams = '';
						$disabled = ' disabled="disabled"';
						if(isset($animal_flat_rent_var)){
							if($animal_flat_rent_var==1){
								$disabled = '';
								$checked = ' checked';
								$valueParams = ' value="'.$animal_flat_rent_var.'"';
							}
						}
						?>
						<div class="checkbox_single<?=$checked?>">
							<input type="hidden"<?=$valueParams?> name="animal"<?=$disabled?>>
							<div class="container">
								<span class="check"></span><span class="label">Можно с животным</span>
							</div>
						</div>
						<?
						$checked = '';
						$valueParams = '';
						$disabled = ' disabled="disabled"';
						if(isset($children_flat_rent_var)){
							if($children_flat_rent_var==1){
								$disabled = '';
								$checked = ' checked';
								$valueParams = ' value="'.$children_flat_rent_var.'"';
							}
						}
						?>
						<div class="checkbox_single<?=$checked?>">
							<input type="hidden"<?=$valueParams?> name="children"<?=$disabled?>>
							<div class="container">
								<span class="check"></span><span class="label">Можно с детьми</span>
							</div>
						</div>
					</div>
				</div>
				<div class="separate"></div>
				<div class="row sell">
					<div class="left_block">
						<div class="metric">
							<div class="label_select">Общая площадь</div>
							<div class="select_block count">
							<?
							$squareChangeFlatSell = '';
							if($arrayLinkSearch['squareFullMin'] && !$arrayLinkSearch['squareFullMax'] && $type_flat_sell_var){ //только метраж "от"
								$c1 = $arrayLinkSearch['squareFullMin'];
								$squareChangeFlatSell = '<li><a class="min" data-name_select="fullsquareflatsell" data-name="squareFullMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(!$arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_flat_sell_var){ //только метраж "до"
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeFlatSell = '<li><a class="max" data-name_select="fullsquareflatsell" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMax'].'" href="javascript:void(0)">общая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if($arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_flat_sell_var){ //промежуток метражей
								$c1 = $arrayLinkSearch['squareFullMin'];
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeFlatSell = '<li><a class="min max" data-name_select="fullsquareflatsell" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}

							$nameParams = 'От';
							$valueParams = '';
							$currentParams = '';
							if($type_flat_sell_var && $arrayLinkSearch['squareFullMin']){
								$nameParams = $arrayLinkSearch['squareFullMin'];
								$currentParams = $arrayLinkSearch['squareFullMin'];
								$valueParams = ' value="'.$arrayLinkSearch['squareFullMin'].'"';
							}
							?>
							<input type="hidden" name="squareFullMin"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_full_square_flat_sell; $c<=$max_full_square_flat_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquareflatsell" data-explanation="общая" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_flat_sell){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_flat_sell){
												$c = $max_full_square_flat_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_sell_var && $arrayLinkSearch['squareFullMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareFullMax']);
									$currentParams = $arrayLinkSearch['squareFullMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMax'].'"';
								}
								?>
								<input type="hidden" name="squareFullMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_full_square_flat_sell; $c<=$max_full_square_flat_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquareflatsell" data-explanation="общая" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_flat_sell){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_flat_sell){
												$c = $max_full_square_flat_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div class="label_select">жилая</div>
							<div class="select_block count">
								<?
								$squareLiveChangeFlatSell = '';
								if($arrayLinkSearch['squareLiveMin'] && !$arrayLinkSearch['squareLiveMax'] && $type_flat_sell_var){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareLiveMax'];
									$squareLiveChangeFlatSell = '<li><a class="min" data-name_select="livesquareflatsell" data-name="squareLiveMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareLiveMin'].'" href="javascript:void(0)">жилая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareLiveMin'] && $arrayLinkSearch['squareLiveMax'] && $type_flat_sell_var){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareLiveMax'];
									$squareLiveChangeFlatSell = '<li><a class="max" data-name_select="livesquareflatsell" data-name="squareLiveMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareLiveMax'].'" href="javascript:void(0)">жилая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareLiveMin'] && $arrayLinkSearch['squareLiveMax'] && $type_flat_sell_var){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareLiveMin'];
									$c2 = $arrayLinkSearch['squareLiveMax'];
									$squareLiveChangeFlatSell = '<li><a class="min max" data-name_select="livesquareflatsell" data-name="squareLiveMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareLiveMin'].'" href="javascript:void(0)">жилая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_sell_var && $arrayLinkSearch['squareLiveMin']){
									$nameParams = $arrayLinkSearch['squareLiveMin'];
									$currentParams = $arrayLinkSearch['squareLiveMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareLiveMin'].'"';
								}
								?>
								<input type="hidden" name="squareLiveMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_live_square_flat_sell; $c<=$max_live_square_flat_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="livesquareflatsell" data-explanation="жилая" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_live_square_flat_sell){
												break;
											}
											$c = $c+1;
											if($c>$max_live_square_flat_sell){
												$c = $max_live_square_flat_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_sell_var && $arrayLinkSearch['squareLiveMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareLiveMax']);
									$currentParams = $arrayLinkSearch['squareLiveMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareLiveMax'].'"';
								}
								?>
								<input type="hidden" name="squareLiveMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_live_square_flat_sell; $c<=$max_live_square_flat_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="livesquareflatsell" data-explanation="жилая" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_live_square_flat_sell){
												break;
											}
											$c = $c+1;
											if($c>$max_live_square_flat_sell){
												$c = $max_live_square_flat_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div class="label_select">кухня</div>
							<div class="select_block count">
								<?
								$squareKitchenChangeFlatSell = '';
								if($arrayLinkSearch['squareKitchenMin'] && !$arrayLinkSearch['squareKitchenMax'] && $type_flat_sell_var){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeFlatSell = '<li><a class="min" data-name_select="kitchensquareflatsell" data-name="squareKitchenMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMin'].'" href="javascript:void(0)">кухня от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareKitchenMin'] && $arrayLinkSearch['squareKitchenMax'] && $type_flat_sell_var){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeFlatSell = '<li><a class="max" data-name_select="kitchensquareflatsell" data-name="squareKitchenMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMax'].'" href="javascript:void(0)">кухня до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareKitchenMin'] && $arrayLinkSearch['squareKitchenMax'] && $type_flat_sell_var){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareKitchenMin'];
									$c2 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeFlatSell = '<li><a class="min max" data-name_select="kitchensquareflatsell" data-name="squareKitchenMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMin'].'" href="javascript:void(0)">кухня от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_sell_var && $arrayLinkSearch['squareKitchenMin']){
									$nameParams = $arrayLinkSearch['squareKitchenMin'];
									$currentParams = $arrayLinkSearch['squareKitchenMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareKitchenMin'].'"';
								}
								?>
								<input type="hidden" name="squareKitchenMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_kitchen_square_flat_sell; $c<=$max_kitchen_square_flat_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="kitchensquareflatsell" data-explanation="кухня" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_kitchen_square_flat_sell){
												break;
											}
											$c = $c+1;
											if($c>$max_kitchen_square_flat_sell){
												$c = $max_kitchen_square_flat_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_sell_var && $arrayLinkSearch['squareKitchenMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareKitchenMax']);
									$currentParams = $arrayLinkSearch['squareKitchenMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareKitchenMax'].'"';
								}
								?>
								<input type="hidden" name="squareKitchenMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_kitchen_square_flat_sell; $c<=$max_kitchen_square_flat_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="kitchensquareflatsell" data-explanation="кухня" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_kitchen_square_flat_sell){
												break;
											}
											$c = $c+1;
											if($c>$max_kitchen_square_flat_sell){
												$c = $max_kitchen_square_flat_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
					</div>
					<div class="center_block">
						<div class="floor">
							<div class="label_select">Этаж</div>
							<div class="select_block count">
								<?
								$floorChangeFlatSell = '';
								if($arrayLinkSearch['floorMin'] && !$arrayLinkSearch['floorMax'] && $type_flat_sell_var){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorMin'];
									$floorChangeFlatSell = '<li><a class="min" data-name_select="floorsquareflatsell" data-name="floorMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_flat_sell_var){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeFlatSell = '<li><a class="max" data-name_select="floorsquareflatsell" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMax'].'" href="javascript:void(0)">этаж по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_flat_sell_var){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorMin'];
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeFlatSell = '<li><a class="min max" data-name_select="floorsquareflatsell" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_sell_var && $arrayLinkSearch['floorMin']){
									$nameParams = $arrayLinkSearch['floorMin'];
									$currentParams = $arrayLinkSearch['floorMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMin'].'"';
								}
								?>
								<input type="hidden" name="floorMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$max_floors_flat_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquareflatsell" data-explanation="этаж" data-tags="с '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_sell_var && $arrayLinkSearch['floorMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorMax']);
									$currentParams = $arrayLinkSearch['floorMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMax'].'"';
								}
								?>
								<input type="hidden" name="floorMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_flat_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquareflatsell" data-explanation="этаж" data-tags="по '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="floor">
							<div class="label_select">в доме</div>
							<div class="select_block count">
								<?
								$floorsChangeFlatSell = '';
								if($arrayLinkSearch['floorsMin'] && !$arrayLinkSearch['floorsMax'] && $type_flat_sell_var){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorsMin'];
									$floorsChangeFlatSell = '<li><a class="min" data-name_select="floorscountflatsell" data-name="floorsMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">в доме от '.$c1.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $type_flat_sell_var){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorsMax'];
									$floorsChangeFlatSell = '<li><a class="max" data-name_select="floorscountflatsell" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMax'].'" href="javascript:void(0)">в доме по '.$c2.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $type_flat_sell_var){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorsMin'];
									$c2 = $arrayLinkSearch['floorsMax'];
									$floorsChangeFlatSell = '<li><a class="min max" data-name_select="floorscountflatsell" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">в доме от '.$c1.' по '.$c2.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_sell_var && $arrayLinkSearch['floorsMin']){
									$nameParams = $arrayLinkSearch['floorsMin'];
									$currentParams = $arrayLinkSearch['floorsMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorsMin'].'"';
								}
								?>
								<input type="hidden" name="floorsMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$max_floors_flat_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorscountflatsell" data-explanation="в доме" data-tags="от '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_sell_var && $arrayLinkSearch['floorsMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorsMax']);
									$currentParams = $arrayLinkSearch['floorsMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorsMax'].'"';
								}
								?>
								<input type="hidden" name="floorsMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_flat_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorscountflatsell" data-explanation="в доме" data-tags="до '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">этажей</div>
						</div>
						<div class="floor">
							<div class="check_block">
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_first_flat_sell_var)){
									if($except_first_flat_sell_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_first_flat_sell_varexcept_first_flat_sell_var.'"';
									}
								}
								?>
								<div class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_first"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме первого</span>
									</div>
								</div>
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_last_flat_sell_var)){
									if($except_last_flat_sell_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_last_flat_sell_var.'"';
									}
								}
								?>
								<div class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_last"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме последнего</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="line"></div>
					
					
					<div class="right_block">
						<div class="metric">
							<div class="cell">
								<div style="width:46px;margin-left:-6px;" class="label_select">Ремонт</div>
								<div class="select_block count">
									<?
									$repairsChangeFlatSell = '';
									if($arrayLinkSearch['repairs'] && $type_flat_sell_var){
										$c1 = $arrayLinkSearch['repairs'];
										$repairsChangeFlatSell = '<li><a class="min" data-name_select="repairs" data-name="repairs" data-type="checkbox_block" data-id="'.$arrayLinkSearch['repairs'].'" href="javascript:void(0)">'.$_REPAIRS[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_flat_sell_var && $arrayLinkSearch['repairs']){
										$currentParams = (int)$arrayLinkSearch['repairs'];
										$valueParams = ' value="'.$arrayLinkSearch['repairs'].'"';
									}
									$repairs = '';
									for($n=0; $n<count($_REPAIRS); $n++){
										if($n==0){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = $_REPAIRS[0];
											}
											$repairs .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_REPAIRS[0].'</a></li>';
										}
										else {
											$current = '';
											if($currentParams==$n){
												$current = ' class="current"';
												$nameParams = $_REPAIRS[$n];
											}
											$tooltip = '';
											if(!empty($_REPAIRS_DESCR[$n])){
												$tooltip = ' data-much="true" data-pos="left" data-hover="tooltip" data-title="'.$_REPAIRS_DESCR[$n].'"';
											}
											$repairs .= '<li'.$current.'><a'.$tooltip.' data-name="repairs" data-explanation="" data-tags="'.clearValueText($_REPAIRS[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_REPAIRS[$n].'</a></li>';
										}
									}
									echo '<input type="hidden" name="repairs"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $repairs;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<div class="cell">
								<div style="width:60px;margin-left:0;" class="label_select">Санузел</div>
								<div class="select_block balcony">
									<?
									$wcChangeFlatSell = '';
									if($arrayLinkSearch['wc'] && $type_flat_sell_var){
										$c1 = $arrayLinkSearch['wc'];
										$wcChangeFlatSell = '<li><a class="min" data-name_select="wc" data-name="wc" data-type="checkbox_block" data-id="'.$arrayLinkSearch['wc'].'" href="javascript:void(0)">'.$_TYPE_WC[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_flat_sell_var && $arrayLinkSearch['wc']){
										$currentParams = (int)$arrayLinkSearch['wc'];
										$valueParams = ' value="'.$arrayLinkSearch['wc'].'"';
									}
									$wc = '';
									for($n=0; $n<count($_TYPE_WC); $n++){
										if($n==0){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = $_TYPE_WC[0];
											}
											$wc .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_TYPE_WC[0].'</a></li>';
										}
										else {
											$current = '';
											if($currentParams==$n){
												$current = ' class="current"';
												$nameParams = $_TYPE_WC[$n];
											}
											$wc .= '<li'.$current.'><a data-name="wc" data-explanation="" data-tags="'.clearValueText($_TYPE_WC[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_WC[$n].'</a></li>';
										}
									}
									echo '<input type="hidden" name="wc"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $wc;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="metric">
							<div class="cell">
								<div style="width:55px;margin-left:-15px;" class="label_select">Тип дома</div>
								<div class="select_block count">
									<?
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_flat_sell_var && $arrayLinkSearch['type_house']){
										$currentParams = (int)$arrayLinkSearch['type_house'];
										$valueParams = ' value="'.$arrayLinkSearch['type_house'].'"';
									}
									$type_house = '';
									$type_houseArray = array();
									$devs = mysql_query("
										SELECT *, (SELECT name FROM ".$template."_type_build WHERE activation='1' && id='".$currentParams."') AS choose_value
										FROM ".$template."_type_build
										WHERE activation='1'
										ORDER BY name
									");
									if(mysql_num_rows($devs)>0){
										$n = 0;
										while($dev = mysql_fetch_assoc($devs)){
											if($n==0){
												$current = '';
												if($currentParams==0){
													$current = ' class="current"';
													$nameParams = 'Не важно';
												}
												$type_house .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
											}
											$current = '';
											if($currentParams==$dev['id']){
												$current = ' class="current"';
												$nameParams = $dev['name'];
											}
											$type_house .= '<li'.$current.'><a data-name="type_house" data-explanation="" data-tags="'.clearValueText($dev['name']).'" href="javascript:void(0)" data-id="'.$dev['id'].'">'.$dev['name'].'</a></li>';
											$type_houseArray[$dev['id']] = clearValueText($dev['name']);
											$n++;
										}
									}
									$type_houseChangeFlatSell = '';
									if($arrayLinkSearch['type_house'] && $type_flat_sell_var){
										$c1 = $arrayLinkSearch['type_house'];
										$type_houseChangeFlatSell = '<li><a class="min" data-name_select="type_house" data-name="type_house" data-type="checkbox_block" data-id="'.$arrayLinkSearch['type_house'].'" href="javascript:void(0)">'.$type_houseArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									echo '<input type="hidden" name="type_house"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $type_house;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<div class="cell">
								<div style="width:60px" class="label_select">До метро</div>
								<div class="select_block count">
									<?
									$far_subwayChangeFlatSell = '';
									if($arrayLinkSearch['far_subway'] && $type_flat_sell_var){
										$c1 = $arrayLinkSearch['far_subway'];
										$name_value = $_FAR_SUBWAY[$c1];
										if($c1==1){
											$name_value = '< 500 м';
										}
										$far_subwayChangeFlatSell = '<li><a class="min" data-name_select="far_subway" data-name="far_subway" data-type="checkbox_block" data-id="'.$arrayLinkSearch['far_subway'].'" href="javascript:void(0)">До метро '.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_flat_sell_var && $arrayLinkSearch['far_subway']){
										$currentParams = (int)$arrayLinkSearch['far_subway'];
										$valueParams = ' value="'.$arrayLinkSearch['far_subway'].'"';
									}
									$far_subway = '';
									for($f=1; $f<=count($_FAR_SUBWAY); $f++){
										if($f==1){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = 'Не важно';
											}
											$far_subway .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
										}
										$current = '';
										if($currentParams==$f){
											$current = ' class="current"';
											$nameParams = $_FAR_SUBWAY[$f];
										}
										$name_value = $_FAR_SUBWAY[$f];
										if($f==1){
											$name_value = '< 500 м';
										}
										$far_subway .= '<li'.$current.'><a data-name="far_subway" data-explanation="До метро" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'">'.$_FAR_SUBWAY[$f].'</a></li>';
									}
									echo '<input type="hidden" name="far_subway"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $far_subway;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<!--<div class="select_block balcony">
								<input type="hidden" name="balcony" value="0">
								<div class="choosed_block">Балкон не важен</div>
								<div class="scrollbar-inner">
									<ul>
										<?
										// for($c=0; $c<count($_TYPE_BALCONY); $c++){
											// $current = '';
											// if(!$balcony_new_sell_var){
												// if($c==0){
													// $current = ' class="current"';
												// }
											// }
											// else {
												// if($c==$balcony_new_sell_var){
													// $current = ' class="current"';
												// }
											// }
											// echo '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_BALCONY[$c].'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>-->
						</div>
						<div class="clear"></div>
						<div class="metric">
							<div class="cell">
								<div style="width:70px;margin-left:-30px;" class="label_select">Тип сделки</div>
								<div class="select_block count">
									<?
									$transactionChangeFlatSell = '';
									if($arrayLinkSearch['transaction'] && $type_flat_sell_var){
										$c1 = $arrayLinkSearch['transaction'];
										$transactionChangeFlatSell = '<li><a class="min" data-name_select="transaction" data-name="transaction" data-type="checkbox_block" data-id="'.$arrayLinkSearch['transaction'].'" href="javascript:void(0)">'.$_TYPE_DEAL[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_flat_sell_var && $arrayLinkSearch['transaction']){
										$currentParams = (int)$arrayLinkSearch['transaction'];
										$valueParams = ' value="'.$arrayLinkSearch['transaction'].'"';
									}
									$transaction = '';
									for($n=0; $n<count($_TYPE_DEAL); $n++){
										if($n==0){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = $_TYPE_DEAL[0];
											}
											$transaction .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_TYPE_DEAL[0].'</a></li>';
										}
										else {
											$current = '';
											if($currentParams==$n){
												$current = ' class="current"';
												$nameParams = $_TYPE_DEAL[$n];
											}
											$transaction .= '<li'.$current.'><a data-name="transaction" data-explanation="" data-tags="'.clearValueText($_TYPE_DEAL[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_DEAL[$n].'</a></li>';
										}
									}
									echo '<input type="hidden" name="transaction"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $transaction;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<div class="cell">
								<div style="margin-top:5px;margin-left:10px;" class="check_block">
									<?
									$checked = '';
									$valueParams = '';
									$disabled = ' disabled="disabled"';
									if(isset($balcony_flat_sell_var)){
										if($balcony_flat_sell_var==1){
											$disabled = '';
											$checked = ' checked';
											$valueParams = ' value="'.$balcony_flat_sell_var.'"';
										}
									}
									?>
									<div class="checkbox_single<?=$checked?>">
										<input type="hidden"<?=$valueParams?> name="balcony"<?=$disabled?>>
										<div class="container">
											<span class="check"></span><span class="label">с балконом</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					
					
					<!--<div class="right_block">
						
						<div class="checkbox_block">
							<?
							$valueParams = '';
							if($type_flat_sell_var && $furniture_flat_sell_var){
								$valueParams = ' value="'.$furniture_flat_sell_var.'"';
							}
							?>
							<input type="hidden" name="furniture"<?=$valueParams?>>
							<div class="select_checkbox">
								<ul>
									<?
									for($c=0; $c<count($_TYPE_FURNITURE); $c++){
										$checked = '';
										if(!$furniture_flat_sell_var){
											if($c==0){
												$checked = ' class="checked"';
											}
										}
										else {
											if($c==$furniture_flat_sell_var){
												$checked = ' class="checked"';
											}
										}
										echo '<li'.$checked.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_FURNITURE[$c].'</a></li>';
									}
									?>
								</ul>
							</div>
						</div>
						<div class="clear"></div>
						<div class="check_block">
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($tv_flat_sell_var)){
								if($tv_flat_sell_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$tv_flat_sell_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="tv"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Телевизор</span>
								</div>
							</div>
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($phone_flat_sell_var)){
								if($phone_flat_sell_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$phone_flat_sell_var.'"';
								}
							}
							?>
							<div class="checkbox_single first<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="phone"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Городской телефон</span>
								</div>
							</div>
						</div>
						<div class="check_block last">
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($fridge_flat_sell_var)){
								if($fridge_flat_sell_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$fridge_flat_sell_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="fridge"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Холодильник</span>
								</div>
							</div>
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($washer_flat_sell_var)){
								if($washer_flat_sell_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$washer_flat_sell_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="washer"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Стиральная машина</span>
								</div>
							</div>
						</div>
					</div>-->
				</div>
				<div class="row rent">
					<div class="left_block">
						<div class="metric">
							<div class="label_select">Общая площадь</div>
							<div class="select_block count">
							<?
							$squareChangeFlatRent = '';
							if($arrayLinkSearch['squareFullMin'] && !$arrayLinkSearch['squareFullMax'] && $type_flat_rent_var){ //только метраж "от"
								$c1 = $arrayLinkSearch['squareFullMin'];
								$squareChangeFlatRent = '<li><a class="min" data-name_select="fullsquareflatrent" data-name="squareFullMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(!$arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_flat_rent_var){ //только метраж "до"
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeFlatRent = '<li><a class="max" data-name_select="fullsquareflatrent" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMax'].'" href="javascript:void(0)">общая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if($arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_flat_rent_var){ //промежуток метражей
								$c1 = $arrayLinkSearch['squareFullMin'];
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeFlatRent = '<li><a class="min max" data-name_select="fullsquareflatrent" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}

							$nameParams = 'От';
							$valueParams = '';
							$currentParams = '';
							if($type_flat_rent_var && $arrayLinkSearch['squareFullMin']){
								$nameParams = $arrayLinkSearch['squareFullMin'];
								$currentParams = $arrayLinkSearch['squareFullMin'];
								$valueParams = ' value="'.$arrayLinkSearch['squareFullMin'].'"';
							}
							?>
							<input type="hidden" name="squareFullMin"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										if($min_full_square_flat_rent==0){
											$min_full_square_flat_rent = 1;
										}
										if($max_full_square_flat_rent==0){
											$max_full_square_flat_rent = 10;
										}
										for($c=$min_full_square_flat_rent; $c<=$max_full_square_flat_rent;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquareflatrent" data-explanation="общая" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_flat_rent){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_flat_rent){
												$c = $max_full_square_flat_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && $arrayLinkSearch['squareFullMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareFullMax']);
									$currentParams = $arrayLinkSearch['squareFullMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMax'].'"';
								}
								?>
								<input type="hidden" name="squareFullMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										if($min_full_square_flat_rent==0){
											$min_full_square_flat_rent = 1;
										}
										if($max_full_square_flat_rent==0){
											$max_full_square_flat_rent = 10;
										}
										for($c=$min_full_square_flat_rent; $c<=$max_full_square_flat_rent;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquareflatrent" data-explanation="общая" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_flat_rent){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_flat_rent){
												$c = $max_full_square_flat_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div class="label_select">жилая</div>
							<div class="select_block count">
								<?
								$squareLiveChangeFlatRent = '';
								if($arrayLinkSearch['squareLiveMin'] && !$arrayLinkSearch['squareLiveMax'] && $type_flat_rent_var){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareLiveMax'];
									$squareLiveChangeFlatRent = '<li><a class="min" data-name_select="livesquareflatrent" data-name="squareLiveMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareLiveMin'].'" href="javascript:void(0)">жилая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareLiveMin'] && $arrayLinkSearch['squareLiveMax'] && $type_flat_rent_var){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareLiveMax'];
									$squareLiveChangeFlatRent = '<li><a class="max" data-name_select="livesquareflatrent" data-name="squareLiveMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareLiveMax'].'" href="javascript:void(0)">жилая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareLiveMin'] && $arrayLinkSearch['squareLiveMax'] && $type_flat_rent_var){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareLiveMin'];
									$c2 = $arrayLinkSearch['squareLiveMax'];
									$squareLiveChangeFlatRent = '<li><a class="min max" data-name_select="livesquareflatrent" data-name="squareLiveMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareLiveMin'].'" href="javascript:void(0)">жилая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && $arrayLinkSearch['squareLiveMin']){
									$nameParams = $arrayLinkSearch['squareLiveMin'];
									$currentParams = $arrayLinkSearch['squareLiveMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareLiveMin'].'"';
								}
								?>
								<input type="hidden" name="squareLiveMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										if($min_live_square_flat_rent==0){
											$min_live_square_flat_rent = 1;
										}
										if($max_live_square_flat_rent==0){
											$max_live_square_flat_rent = 10;
										}
										for($c=$min_live_square_flat_rent; $c<=$max_live_square_flat_rent;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="livesquareflatrent" data-explanation="жилая" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_live_square_flat_rent){
												break;
											}
											$c = $c+1;
											if($c>$max_live_square_flat_rent){
												$c = $max_live_square_flat_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && $arrayLinkSearch['squareLiveMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareLiveMax']);
									$currentParams = $arrayLinkSearch['squareLiveMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareLiveMax'].'"';
								}
								?>
								<input type="hidden" name="squareLiveMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										if($min_live_square_flat_rent==0){
											$min_live_square_flat_rent = 1;
										}
										if($max_live_square_flat_rent==0){
											$max_live_square_flat_rent = 10;
										}
										for($c=$min_live_square_flat_rent; $c<=$max_live_square_flat_rent;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="livesquareflatrent" data-explanation="жилая" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_live_square_flat_rent){
												break;
											}
											$c = $c+1;
											if($c>$max_live_square_flat_rent){
												$c = $max_live_square_flat_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div class="label_select">кухня</div>
							<div class="select_block count">
								<?
								$squareKitchenChangeFlatRent = '';
								if($arrayLinkSearch['squareKitchenMin'] && !$arrayLinkSearch['squareKitchenMax'] && $type_flat_rent_var){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeFlatRent = '<li><a class="min" data-name_select="kitchensquareflatrent" data-name="squareKitchenMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMin'].'" href="javascript:void(0)">кухня от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareKitchenMin'] && $arrayLinkSearch['squareKitchenMax'] && $type_flat_rent_var){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeFlatRent = '<li><a class="max" data-name_select="kitchensquareflatrent" data-name="squareKitchenMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMax'].'" href="javascript:void(0)">кухня до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareKitchenMin'] && $arrayLinkSearch['squareKitchenMax'] && $type_flat_rent_var){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareKitchenMin'];
									$c2 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeFlatRent = '<li><a class="min max" data-name_select="kitchensquareflatrent" data-name="squareKitchenMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMin'].'" href="javascript:void(0)">кухня от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && $arrayLinkSearch['squareKitchenMin']){
									$nameParams = $arrayLinkSearch['squareKitchenMin'];
									$currentParams = $arrayLinkSearch['squareKitchenMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareKitchenMin'].'"';
								}
								?>
								<input type="hidden" name="squareKitchenMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										if($min_kitchen_square_flat_rent==0){
											$min_kitchen_square_flat_rent = 1;
										}
										if($max_kitchen_square_flat_rent==0){
											$max_kitchen_square_flat_rent = 10;
										}
										for($c=$min_kitchen_square_flat_rent; $c<=$max_kitchen_square_flat_rent;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="kitchensquareflatrent" data-explanation="кухня" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_kitchen_square_flat_rent){
												break;
											}
											$c = $c+1;
											if($c>$max_kitchen_square_flat_rent){
												$c = $max_kitchen_square_flat_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && $arrayLinkSearch['squareKitchenMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareKitchenMax']);
									$currentParams = $arrayLinkSearch['squareKitchenMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareKitchenMax'].'"';
								}
								?>
								<input type="hidden" name="squareKitchenMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										if($min_kitchen_square_flat_rent==0){
											$min_kitchen_square_flat_rent = 1;
										}
										if($max_kitchen_square_flat_rent==0){
											$max_kitchen_square_flat_rent = 10;
										}
										for($c=$min_kitchen_square_flat_rent; $c<=$max_kitchen_square_flat_rent;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="kitchensquareflatrent" data-explanation="кухня" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_kitchen_square_flat_rent){
												break;
											}
											$c = $c+1;
											if($c>$max_kitchen_square_flat_rent){
												$c = $max_kitchen_square_flat_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
					</div>
					<div class="center_block">
						<div class="floor">
							<div class="label_select">Этаж</div>
							<div class="select_block count">
								<?
								$floorChangeFlatRent = '';
								if($arrayLinkSearch['floorMin'] && !$arrayLinkSearch['floorMax'] && $type_flat_rent_var){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorMin'];
									$floorChangeFlatRent = '<li><a class="min" data-name_select="floorflatrent" data-name="floorMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_flat_rent_var){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeFlatRent = '<li><a class="max" data-name_select="floorflatrent" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMax'].'" href="javascript:void(0)">этаж по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_flat_rent_var){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorMin'];
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeFlatRent = '<li><a class="min max" data-name_select="floorflatrent" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && $arrayLinkSearch['floorMin']){
									$nameParams = $arrayLinkSearch['floorMin'];
									$currentParams = $arrayLinkSearch['floorMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMin'].'"';
								}
								?>
								<input type="hidden" name="floorMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										if($max_floors_flat_rent==0){
											$max_floors_flat_rent = 10;
										}
										for($f=1; $f<=$max_floors_flat_rent; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorflatrent" data-explanation="этаж" data-tags="с '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && $arrayLinkSearch['floorMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorMax']);
									$currentParams = $arrayLinkSearch['floorMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMax'].'"';
								}
								?>
								<input type="hidden" name="floorMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_flat_rent; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorflatrent" data-explanation="этаж" data-tags="по '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="floor">
							<div class="label_select">в доме</div>
							<div class="select_block count">
								<?
								$floorsChangeFlatRent = '';
								if($arrayLinkSearch['floorsMin'] && !$arrayLinkSearch['floorsMax'] && $type_flat_rent_var){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorsMin'];
									$floorsChangeFlatRent = '<li><a class="min" data-name_select="floorscountflatrent" data-name="floorsMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">в доме от '.$c1.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $type_flat_rent_var){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorsMax'];
									$floorsChangeFlatRent = '<li><a class="max" data-name_select="floorscountflatrent" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMax'].'" href="javascript:void(0)">в доме по '.$c2.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $type_flat_rent_var){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorsMin'];
									$c2 = $arrayLinkSearch['floorsMax'];
									$floorsChangeFlatRent = '<li><a class="min max" data-name_select="floorscountflatrent" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">в доме от '.$c1.' по '.$c2.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && $arrayLinkSearch['floorsMin']){
									$nameParams = $arrayLinkSearch['floorsMin'];
									$currentParams = $arrayLinkSearch['floorsMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorsMin'].'"';
								}
								?>
								<input type="hidden" name="floorsMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$max_floors_flat_rent; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorscountflatrent" data-explanation="в доме" data-tags="от '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && $arrayLinkSearch['floorsMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorsMax']);
									$currentParams = $arrayLinkSearch['floorsMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorsMax'].'"';
								}
								?>
								<input type="hidden" name="floorsMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_flat_rent; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorscountflatrent" data-explanation="в доме" data-tags="до '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">этажей</div>
						</div>
						<div class="floor">
							<div class="check_block">
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_first_flat_rent_var)){
									if($except_first_flat_rent_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_first_flat_rent_var.'"';
									}
								}
								?>
								<div class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_first"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме первого</span>
									</div>
								</div>
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_last_flat_rent_var)){
									if($except_last_flat_rent_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_last_flat_rent_var.'"';
									}
								}
								?>
								<div class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_last"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме последнего</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="line"></div>
					<div class="right_block">
						<div class="checkbox_block">
							<?
							$valueParams = '';
							if($type_flat_rent_var && $furniture_flat_rent_var){
								$valueParams = ' value="'.$furniture_flat_rent_var.'"';
							}
							?>
							<input type="hidden" name="furniture"<?=$valueParams?>>
							<div class="select_checkbox">
								<ul>
									<?
									for($c=0; $c<count($_TYPE_FURNITURE); $c++){
										$checked = '';
										if(!$furniture_flat_rent_var){
											if($c==0){
												$checked = ' class="checked"';
											}
										}
										else {
											if($c==$furniture_flat_rent_var){
												$checked = ' class="checked"';
											}
										}
										echo '<li'.$checked.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_FURNITURE[$c].'</a></li>';
									}
									?>
								</ul>
							</div>
						</div>
						<div class="clear"></div>
						<div class="check_block">
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($tv_flat_rent_var)){
								if($tv_flat_rent_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$tv_flat_rent_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="tv"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Телевизор</span>
								</div>
							</div>
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($phone_flat_rent_var)){
								if($phone_flat_rent_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$phone_flat_rent_var.'"';
								}
							}
							?>
							<div class="checkbox_single first<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="phone"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Городской телефон</span>
								</div>
							</div>
						</div>
						<div class="check_block last">
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($fridge_flat_rent_var)){
								if($fridge_flat_rent_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$fridge_flat_rent_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="fridge"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Холодильник</span>
								</div>
							</div>
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($washer_flat_rent_var)){
								if($washer_flat_rent_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$washer_flat_rent_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="washer"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Стиральная машина</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div <?$estateValue!=3?print'style="display:none"':'style="display:block"'?> class="inner_block room">
				<div class="row rent">
					<div class="select_block type">
						<input type="hidden" name="period">
						<div class="choosed_block">Любой срок</div>
						<div class="scrollbar-inner">
							<ul>
								<li class="current"><a href="javascript:void(0)" data-id="0">Любой срок</a></li>
								<?
								for($c=1; $c<=count($_TYPE_PERIOD_RENT); $c++){
									echo '<li><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_PERIOD_RENT[$c].'</a></li>';
								}
								?>
							</ul>
						</div>
					</div>
					<div class="select_block type">
						<input type="hidden" name="prepayment">
						<div class="choosed_block">Любая предоплата</div>
						<div class="scrollbar-inner">
							<ul>
								<li class="current"><a href="javascript:void(0)" data-id="0">Любая предоплата</a></li>
								<?
								for($c=1; $c<=count($_TYPE_PREPAYMENT); $c++){
									echo '<li><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_PREPAYMENT[$c].'</a></li>';
								}
								?>
							</ul>
						</div>
					</div>
					<div class="select_block type">
						<input type="hidden" name="deposit">
						<div class="choosed_block">Залог не важен</div>
						<div class="scrollbar-inner">
							<ul>
								<li class="current"><a href="javascript:void(0)" data-id="0">Залог не важен</a></li>
								<?
								for($c=1; $c<=count($_TYPE_DEPOSIT); $c++){
									echo '<li><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_DEPOSIT[$c].'</a></li>';
								}
								?>
							</ul>
						</div>
					</div>
					<div class="can_block">
						<div class="checkbox_single">
							<input type="hidden" name="animal" disabled="disabled">
							<div class="container">
								<span class="check"></span><span class="label">Можно с животным</span>
							</div>
						</div>
						<div class="checkbox_single">
							<input type="hidden" name="children" disabled="disabled">
							<div class="container">
								<span class="check"></span><span class="label">Можно с детьми</span>
							</div>
						</div>
					</div>
				</div>
				<div class="separate"></div>
				<div class="row rent">
					<div style="margin-left:192px;width:305px;" class="center_block">
						<div class="floor">
							<div style="width:110px" class="label_select">Комнат в квартире</div>
							<div style="margin-left:0;" class="checkbox_block">
								<?
								$ex_rooms_room_rent_var = array();
								$valueInput = '';
								$roomsChangeRoomRent = '';
								if(isset($rooms_flat_room_rent_var)){
									$ex_rooms_room_rent_var = explode(',',$rooms_flat_room_rent_var);
									$valueInput = ' value="'.$rooms_flat_room_rent_var.'"';
									$id_room = 0;
									$name_room = '';
									if(in_array(2,$ex_rooms_room_rent_var)){ // 2-комнаты
										$roomsChangeRoomRent .= '<li><a data-name="rooms_flat" data-type="checkbox_select" data-id="2" href="javascript:void(0)">2-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									if(in_array(3,$ex_rooms_room_rent_var)){ // 3-комнаты
										$roomsChangeRoomRent .= '<li><a data-name="rooms_flat" data-type="checkbox_select" data-id="3" href="javascript:void(0)">3-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									if(in_array(4,$ex_rooms_room_rent_var)){ // 4+-комнат
										$roomsChangeRoomRent .= '<li><a data-name="rooms_flat" data-type="checkbox_select" data-id="4" href="javascript:void(0)">4+-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
								}
								?>
								<input type="hidden" name="rooms_flat"<?=$valueInput?>>
								<div class="block_checkbox big">
									<ul>
										<li <?in_array(2,$ex_rooms_room_rent_var)?print'class="checked"':''?>><a data-tags="2-комн." href="javascript:void(0)" data-id="2">2</a></li>
										<li <?in_array(3,$ex_rooms_room_rent_var)?print'class="checked"':''?>><a data-tags="3-комн." href="javascript:void(0)" data-id="3">3</a></li>
										<li <?in_array(4,$ex_rooms_room_rent_var)?print'class="checked"':''?>><a data-tags="4+-комн." href="javascript:void(0)" data-id="4">4+</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="floor">
							<div style="width:110px" class="label_select">Этаж</div>
							<div class="select_block count">
								<?
								$floorChangeRoomRent = '';
								if($arrayLinkSearch['floorMin'] && !$arrayLinkSearch['floorMax'] && $type_room_rent_var){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorMin'];
									$floorChangeRoomRent = '<li><a class="min" data-name_select="floorssquareroomrent" data-name="floorMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_room_rent_var){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeRoomRent = '<li><a class="max" data-name_select="floorssquareroomrent" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMax'].'" href="javascript:void(0)">этаж по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_room_rent_var){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorMin'];
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeRoomRent = '<li><a class="min max" data-name_select="floorssquareroomrent" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_room_rent_var && $arrayLinkSearch['floorMin']){
									$nameParams = $arrayLinkSearch['floorMin'];
									$currentParams = $arrayLinkSearch['floorMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMin'].'"';
								}
								?>
								<input type="hidden" name="floorMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$max_floors_room_rent; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorssquareroomrent" data-explanation="этаж" data-tags="с '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_room_rent_var && $arrayLinkSearch['floorMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorMax']);
									$currentParams = $arrayLinkSearch['floorMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMax'].'"';
								}
								?>
								<input type="hidden" name="floorMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_room_rent; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorssquareroomrent" data-explanation="этаж" data-tags="по '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="line"></div>
					<div class="right_block">
						<div class="checkbox_block">
							<input type="hidden" name="furniture">
							<div class="select_checkbox">
								<ul>
									<?
									for($c=0; $c<count($_TYPE_FURNITURE); $c++){
										$checked = '';
										if(!$furniture_room_rent_var){
											if($c==0){
												$checked = ' class="checked"';
											}
										}
										else {
											if($c==$furniture_room_rent_var){
												$checked = ' class="checked"';
											}
										}
										echo '<li'.$checked.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_FURNITURE[$c].'</a></li>';
									}
									?>
								</ul>
							</div>
						</div>
						<div class="clear"></div>
						<div class="check_block">
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($tv_room_rent_var)){
								if($tv_room_rent_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$tv_room_rent_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="tv"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Телевизор</span>
								</div>
							</div>
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($phone_room_rent_var)){
								if($phone_room_rent_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$phone_room_rent_var.'"';
								}
							}
							?>
							<div class="checkbox_single first<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="phone"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Городской телефон</span>
								</div>
							</div>
						</div>
						<div class="check_block last">
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($fridge_room_rent_var)){
								if($fridge_room_rent_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$fridge_room_rent_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="fridge"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Холодильник</span>
								</div>
							</div>
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($washer_room_rent_var)){
								if($washer_room_rent_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$washer_room_rent_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="washer"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Стиральная машина</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row sell">
					<div class="left_block">
						<div class="metric">
							<div style="width:110px;" class="label_select">Площадь комнаты</div>
							<div class="select_block count">
								<?
								$name_type = 'кв.м';
								if($arrayLinkSearch['squareRoomMin'] && !$arrayLinkSearch['squareRoomMax'] && $type_room_sell_var){ //только площадь "от"
									$c1 = $arrayLinkSearch['squareRoomMin'];
									$squareFullChangeRoomSell = '<li><a class="min" data-name_select="squaresellroom" data-name="squareRoomMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareRoomMin'].'" href="javascript:void(0)">от '.$c1.' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareRoomMin'] && $arrayLinkSearch['squareRoomMax'] && $type_room_sell_var){ //только площадь "до"
									$c2 = $arrayLinkSearch['squareRoomMax'];
									$squareFullChangeRoomSell = '<li><a class="max" data-name_select="squaresellroom" data-name="squareRoomMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareRoomMax'].'" href="javascript:void(0)">до '.$c2.' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareRoomMin'] && $arrayLinkSearch['squareRoomMax'] && $type_room_sell_var){ //промежуток площадей
									$c1 = $arrayLinkSearch['squareRoomMin'];
									$c2 = $arrayLinkSearch['squareRoomMax'];
									$squareFullChangeRoomSell = '<li><a class="min max" data-name_select="squaresellroom" data-name="squareRoomMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareRoomMin'].'" href="javascript:void(0)">от '.$c1.' '.$name_type.'. до '.$c2.' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_room_sell_var && $arrayLinkSearch['squareRoomMin']){
									$nameParams = $arrayLinkSearch['squareRoomMin'];
									$currentParams = $arrayLinkSearch['squareRoomMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareRoomMin'].'"';
								}
								?>
								<input type="hidden" name="squareRoomMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										if($min_room_square_room_sell==0){
											$min_room_square_room_sell = 1;
										}
										if($max_room_square_room_sell==0){
											$max_room_square_room_sell = 10;
										}
										for($c=$min_room_square_room_sell; $c<=$max_room_square_room_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="squaresellroom" data-tags="от '.$c.' '.$name_type.'" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_room_square_room_sell){
												break;
											}
											$c = $c+1;
											if($c>$max_room_square_room_sell){
												$c = $max_room_square_room_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">–</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_room_sell_var && $arrayLinkSearch['squareRoomMax']){
									$nameParams = $arrayLinkSearch['squareRoomMax'];
									$currentParams = $arrayLinkSearch['squareRoomMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareRoomMax'].'"';
								}
								?>
								<input type="hidden" name="squareRoomMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										if($min_room_square_room_sell==0){
											$min_room_square_room_sell = 1;
										}
										if($max_room_square_room_sell==0){
											$max_room_square_room_sell = 10;
										}
										for($c=$min_room_square_room_sell; $c<=$max_room_square_room_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="squaresellroom" data-tags="до '.$c.' '.$name_type.'" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_room_square_room_sell){
												break;
											}
											$c = $c+1;
											if($c>$max_room_square_room_sell){
												$c = $max_room_square_room_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div style="margin-top:2px" class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div style="width:110px" class="label_select">кухня</div>
							<div class="select_block count">
							
								<?
								$squareKitchenChangeRoomSell = '';
								if($arrayLinkSearch['squareKitchenMin'] && !$arrayLinkSearch['squareKitchenMax'] && $type_room_sell_var){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeRoomSell = '<li><a class="min" data-name_select="kitchensquareroomsell" data-name="squareKitchenMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMin'].'" href="javascript:void(0)">кухня от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareKitchenMin'] && $arrayLinkSearch['squareKitchenMax'] && $type_room_sell_var){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeRoomSell = '<li><a class="max" data-name_select="kitchensquareroomsell" data-name="squareKitchenMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMax'].'" href="javascript:void(0)">кухня до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareKitchenMin'] && $arrayLinkSearch['squareKitchenMax'] && $type_room_sell_var){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareKitchenMin'];
									$c2 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeRoomSell = '<li><a class="min max" data-name_select="kitchensquareroomsell" data-name="squareKitchenMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMin'].'" href="javascript:void(0)">кухня от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_room_sell_var && $arrayLinkSearch['squareKitchenMin']){
									$nameParams = $arrayLinkSearch['squareKitchenMin'];
									$currentParams = $arrayLinkSearch['squareKitchenMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareKitchenMin'].'"';
								}
								?>
								<input type="hidden" name="squareKitchenMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_kitchen_square_room_sell; $c<=$max_kitchen_square_room_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="kitchensquareroomsell" data-explanation="кухня" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_room_sell_var && $arrayLinkSearch['squareKitchenMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareKitchenMax']);
									$currentParams = $arrayLinkSearch['squareKitchenMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareKitchenMax'].'"';
								}
								?>
								<input type="hidden" name="squareKitchenMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_kitchen_square_room_sell; $c<=$max_kitchen_square_room_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="kitchensquareroomsell" data-explanation="кухня" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
							<div style="margin-top:2px" class="com">м<sup>2</sup></div>
						</div>
					</div>
					<div class="center_block">
						<!--<div class="floor">
							<div style="width:110px" class="label_select">Комнат в квартире</div>
							<div style="margin-left:0;" class="checkbox_block">
								<?
								// $ex_rooms_room_sell_var = array();
								// $valueInput = '';
								// $roomsFlatChangeRoomSell = '';
								// if(isset($rooms_flat_room_sell_var)){
									// $ex_rooms_room_sell_var = explode(',',$rooms_flat_room_sell_var);
									// $valueInput = ' value="'.$rooms_flat_room_sell_var.'"';
									// $id_room = 0;
									// $name_room = '';
									// if(in_array(2,$ex_rooms_room_sell_var)){ // 2-комнаты
										// $roomsFlatChangeRoomSell .= '<li><a data-name="rooms_flat" data-type="checkbox_select" data-id="2" href="javascript:void(0)">2-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
									// }
									// if(in_array(3,$ex_rooms_room_sell_var)){ // 3-комнаты
										// $roomsFlatChangeRoomSell .= '<li><a data-name="rooms_flat" data-type="checkbox_select" data-id="3" href="javascript:void(0)">3-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
									// }
									// if(in_array(4,$ex_rooms_room_sell_var)){ // 4+-комнат
										// $roomsFlatChangeRoomSell .= '<li><a data-name="rooms_flat" data-type="checkbox_select" data-id="4" href="javascript:void(0)">4+-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
									// }
								// }
								?>
								<input type="hidden" name="rooms_flat"<?=$valueInput?>>
								<div class="block_checkbox big">
									<ul>
										<li <?in_array(2,$ex_rooms_room_sell_var)?print'class="checked"':''?>><a data-tags="2-комн." href="javascript:void(0)" data-id="2">2</a></li>
										<li <?in_array(3,$ex_rooms_room_sell_var)?print'class="checked"':''?>><a data-tags="3-комн." href="javascript:void(0)" data-id="3">3</a></li>
										<li <?in_array(4,$ex_rooms_room_sell_var)?print'class="checked"':''?>><a data-tags="4+-комн." href="javascript:void(0)" data-id="4">4+</a></li>
									</ul>
								</div>
							</div>
						</div>-->
						<div class="floor">
							<div class="label_select">Этаж</div>
							<div class="select_block count">
								<?
								$floorChangeRoomSell = '';
								if($arrayLinkSearch['floorMin'] && !$arrayLinkSearch['floorMax'] && $type_room_sell_var){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorMin'];
									$floorChangeRoomSell = '<li><a class="min" data-name_select="floorsquareroomsell" data-name="floorMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_room_sell_var){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeRoomSell = '<li><a class="max" data-name_select="floorsquareroomsell" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMax'].'" href="javascript:void(0)">этаж по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_room_sell_var){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorMin'];
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeRoomSell = '<li><a class="min max" data-name_select="floorsquareroomsell" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_room_sell_var && $arrayLinkSearch['floorMin']){
									$nameParams = $arrayLinkSearch['floorMin'];
									$currentParams = $arrayLinkSearch['floorMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMin'].'"';
								}
								?>
								<input type="hidden" name="floorMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$max_floors_room_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquareroomsell" data-explanation="этаж" data-tags="с '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_room_sell_var && $arrayLinkSearch['floorMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorMax']);
									$currentParams = $arrayLinkSearch['floorMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMax'].'"';
								}
								?>
								<input type="hidden" name="floorMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li class="current"><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_room_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquareroomsell" data-explanation="этаж" data-tags="по '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
						</div>					
						<div class="floor">
							<div class="label_select">в доме</div>
							<div class="select_block count">
								<?
								$floorsChangeRoomSell = '';
								if($arrayLinkSearch['floorsMin'] && !$arrayLinkSearch['floorsMax'] && $type_room_sell_var){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorsMin'];
									$floorsChangeRoomSell = '<li><a class="min" data-name_select="fullsquarefloors" data-name="floorsMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">в доме от '.$c1.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $type_room_sell_var){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorsMax'];
									$floorsChangeRoomSell = '<li><a class="max" data-name_select="fullsquarefloors" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMax'].'" href="javascript:void(0)">в доме по '.$c2.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $type_room_sell_var){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorsMin'];
									$c2 = $arrayLinkSearch['floorsMax'];
									$floorsChangeRoomSell = '<li><a class="min max" data-name_select="fullsquarefloors" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">в доме от '.$c1.' по '.$c2.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_room_sell_var && $arrayLinkSearch['floorsMin']){
									$nameParams = $arrayLinkSearch['floorsMin'];
									$currentParams = $arrayLinkSearch['floorsMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorsMin'].'"';
								}
								?>
								<input type="hidden" name="floorsMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										if($max_floors_room_sell==0){
											$max_floors_room_sell = 10;
										}
										for($f=1; $f<=$max_floors_room_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquarefloors" data-explanation="в доме" data-tags="от '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_room_sell_var && $arrayLinkSearch['floorsMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorsMax']);
									$currentParams = $arrayLinkSearch['floorsMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorsMax'].'"';
								}
								?>
								<input type="hidden" name="floorsMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										if($max_floors_room_sell==0){
											$max_floors_room_sell = 10;
										}
										for($f=1; $f<=$max_floors_room_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquarefloors" data-explanation="в доме" data-tags="до '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">этажей</div>
						</div>
						
						<div class="floor">
							<div class="check_block">
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_first_room_sell_var)){
									if($except_first_room_sell_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_first_room_sell_var.'"';
									}
								}
								?>
								<div class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_first"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме первого</span>
									</div>
								</div>
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_last_room_sell_var)){
									if($except_last_room_sell_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_last_room_sell_var.'"';
									}
								}
								?>
								<div class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_last"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме последнего</span>
									</div>
								</div>
							</div>
						</div>
						<!--<div class="floor">
							<div class="check_block">
								<?
								// $checked = '';
								// $valueParams = '';
								// $disabled = ' disabled="disabled"';
								// if(isset($separate_room_sell_var)){
									// if($separate_room_sell_var==1){
										// $disabled = '';
										// $checked = ' checked';
										// $valueParams = ' value="'.$separate_room_sell_var.'"';
									// }
								// }
								?>
								<div style="margin-left:84px" class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="separate"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">Раздельный санузел</span>
									</div>
								</div>
							</div>
						</div>-->
					</div>
					<div class="line"></div>
					<div class="right_block">
						<div class="metric">
							<div class="cell">
								<div style="width:46px;margin-left:-6px;" class="label_select">Ремонт</div>
								<div class="select_block count">
									<?
									$repairsChangeRoomSell = '';
									if($arrayLinkSearch['repairs'] && $type_room_sell_var){
										$c1 = $arrayLinkSearch['repairs'];
										$repairsChangeRoomSell = '<li><a class="min" data-name_select="repairs" data-name="repairs" data-type="checkbox_block" data-id="'.$arrayLinkSearch['repairs'].'" href="javascript:void(0)">'.$_REPAIRS[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_room_sell_var && $arrayLinkSearch['repairs']){
										$currentParams = (int)$arrayLinkSearch['repairs'];
										$valueParams = ' value="'.$arrayLinkSearch['repairs'].'"';
									}
									$repairs = '';
									for($n=0; $n<count($_REPAIRS); $n++){
										if($n==0){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = $_REPAIRS[0];
											}
											$repairs .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_REPAIRS[0].'</a></li>';
										}
										else {
											$current = '';
											if($currentParams==$n){
												$current = ' class="current"';
												$nameParams = $_REPAIRS[$n];
											}
											$tooltip = '';
											if(!empty($_REPAIRS_DESCR[$n])){
												$tooltip = ' data-much="true" data-pos="left" data-hover="tooltip" data-title="'.$_REPAIRS_DESCR[$n].'"';
											}
											$repairs .= '<li'.$current.'><a'.$tooltip.' data-name="repairs" data-explanation="" data-tags="'.clearValueText($_REPAIRS[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_REPAIRS[$n].'</a></li>';
										}
									}
									echo '<input type="hidden" name="repairs"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $repairs;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<div class="cell">
								<div style="width:60px;margin-left:0;" class="label_select">Санузел</div>
								<div class="select_block balcony">
									<?
									$wcChangeRoomSell = '';
									if($arrayLinkSearch['wc'] && $type_room_sell_var){
										$c1 = $arrayLinkSearch['wc'];
										$wcChangeRoomSell = '<li><a class="min" data-name_select="wc" data-name="wc" data-type="checkbox_block" data-id="'.$arrayLinkSearch['wc'].'" href="javascript:void(0)">'.$_TYPE_WC[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									
									$nameParams = 'Любой санузел';
									$valueParams = '';
									$currentParams = '';
									if($type_room_sell_var && $arrayLinkSearch['wc']){
										$currentParams = (int)$arrayLinkSearch['wc'];
										$valueParams = ' value="'.$arrayLinkSearch['wc'].'"';
									}
									$wc = '';
									for($n=0; $n<count($_TYPE_WC); $n++){
										if($n==0){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = $_TYPE_WC[0];
											}
											$wc .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_TYPE_WC[0].'</a></li>';
										}
										else {
											$current = '';
											if($currentParams==$n){
												$current = ' class="current"';
												$nameParams = $_TYPE_WC[$n];
											}
											$wc .= '<li'.$current.'><a data-name="wc" data-explanation="" data-tags="'.clearValueText($_TYPE_WC[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_WC[$n].'</a></li>';
										}
									}
									echo '<input type="hidden" name="wc"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $wc;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
						</div>
						<div class="metric">
							<div class="cell">
								<div style="width:55px;margin-left:-15px;" class="label_select">Тип дома</div>
								<div class="select_block count">
									<?
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_room_sell_var && $arrayLinkSearch['type_house']){
										$currentParams = (int)$arrayLinkSearch['type_house'];
										$valueParams = ' value="'.$arrayLinkSearch['type_house'].'"';
									}
									$type_house = '';
									$type_houseArray = array();
									$devs = mysql_query("
										SELECT *, (SELECT name FROM ".$template."_type_build WHERE activation='1' && id='".$currentParams."') AS choose_value
										FROM ".$template."_type_build
										WHERE activation='1'
										ORDER BY name
									");
									if(mysql_num_rows($devs)>0){
										$n = 0;
										while($dev = mysql_fetch_assoc($devs)){
											if($n==0){
												$current = '';
												if($currentParams==0){
													$current = ' class="current"';
													$nameParams = 'Не важно';
												}
												$type_house .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
											}
											$current = '';
											if($currentParams==$dev['id']){
												$current = ' class="current"';
												$nameParams = $dev['name'];
											}
											$type_house .= '<li'.$current.'><a data-name="type_house" data-explanation="" data-tags="'.clearValueText($dev['name']).'" href="javascript:void(0)" data-id="'.$dev['id'].'">'.$dev['name'].'</a></li>';
											$type_houseArray[$dev['id']] = clearValueText($dev['name']);
											$n++;
										}
									}
									$type_houseChangeRoomSell = '';
									if($arrayLinkSearch['type_house'] && $type_room_sell_var){
										$c1 = $arrayLinkSearch['type_house'];
										$type_houseChangeRoomSell = '<li><a class="min" data-name_select="type_house" data-name="type_house" data-type="checkbox_block" data-id="'.$arrayLinkSearch['type_house'].'" href="javascript:void(0)">'.$type_houseArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									echo '<input type="hidden" name="type_house"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $type_house;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<div class="cell">
								<div style="width:60px" class="label_select">До метро</div>
								<div class="select_block count">
									<?
									$far_subwayChangeRoomSell = '';
									if($arrayLinkSearch['far_subway'] && $type_room_sell_var){
										$c1 = $arrayLinkSearch['far_subway'];
										$name_value = $_FAR_SUBWAY[$c1];
										if($c1==1){
											$name_value = '< 500 м';
										}
										$far_subwayChangeRoomSell = '<li><a class="min" data-name_select="far_subway" data-name="far_subway" data-type="checkbox_block" data-id="'.$arrayLinkSearch['far_subway'].'" href="javascript:void(0)">До метро '.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_room_sell_var && $arrayLinkSearch['far_subway']){
										$currentParams = (int)$arrayLinkSearch['far_subway'];
										$valueParams = ' value="'.$arrayLinkSearch['far_subway'].'"';
									}
									$far_subway = '';
									for($f=1; $f<=count($_FAR_SUBWAY); $f++){
										if($f==1){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = 'Не важно';
											}
											$far_subway .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
										}
										$current = '';
										if($currentParams==$f){
											$current = ' class="current"';
											$nameParams = $_FAR_SUBWAY[$f];
										}
										$name_value = $_FAR_SUBWAY[$f];
										if($f==1){
											$name_value = '< 500 м';
										}
										$far_subway .= '<li'.$current.'><a data-name="far_subway" data-explanation="До метро" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'">'.$_FAR_SUBWAY[$f].'</a></li>';
									}
									echo '<input type="hidden" name="far_subway"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $far_subway;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<!--<div class="select_block balcony">
								<input type="hidden" name="balcony" value="0">
								<div class="choosed_block">Балкон не важен</div>
								<div class="scrollbar-inner">
									<ul>
										<?
										// for($c=0; $c<count($_TYPE_BALCONY); $c++){
											// $current = '';
											// if(!$balcony_new_sell_var){
												// if($c==0){
													// $current = ' class="current"';
												// }
											// }
											// else {
												// if($c==$balcony_new_sell_var){
													// $current = ' class="current"';
												// }
											// }
											// echo '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_BALCONY[$c].'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>-->
						</div>						
						<div class="metric">
							<div class="cell">
								<div style="width:70px;margin-left:-30px;" class="label_select">Тип сделки</div>
								<div class="select_block count">
									<?
									$transactionChangeRoomSell = '';
									if($arrayLinkSearch['transaction'] && $type_room_sell_var){
										$c1 = $arrayLinkSearch['transaction'];
										$transactionChangeRoomSell = '<li><a class="min" data-name_select="transaction" data-name="transaction" data-type="checkbox_block" data-id="'.$arrayLinkSearch['transaction'].'" href="javascript:void(0)">'.$_TYPE_DEAL[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_room_sell_var && $arrayLinkSearch['transaction']){
										$currentParams = (int)$arrayLinkSearch['transaction'];
										$valueParams = ' value="'.$arrayLinkSearch['transaction'].'"';
									}
									$transaction = '';
									for($n=0; $n<count($_TYPE_DEAL); $n++){
										if($n==0){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = $_TYPE_DEAL[0];
											}
											$transaction .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_TYPE_DEAL[0].'</a></li>';
										}
										else {
											$current = '';
											if($currentParams==$n){
												$current = ' class="current"';
												$nameParams = $_TYPE_DEAL[$n];
											}
											$transaction .= '<li'.$current.'><a data-name="transaction" data-explanation="" data-tags="'.clearValueText($_TYPE_DEAL[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_DEAL[$n].'</a></li>';
										}
									}
									echo '<input type="hidden" name="transaction"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $transaction;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<div class="cell">
								<div style="margin-top:5px;margin-left:10px;" class="check_block">
									<?
									$checked = '';
									$valueParams = '';
									$disabled = ' disabled="disabled"';
									if(isset($balcony_room_sell_var)){
										if($balcony_room_sell_var==1){
											$disabled = '';
											$checked = ' checked';
											$valueParams = ' value="'.$balcony_room_sell_var.'"';
										}
									}
									?>
									<div class="checkbox_single<?=$checked?>">
										<input type="hidden"<?=$valueParams?> name="balcony"<?=$disabled?>>
										<div class="container">
											<span class="check"></span><span class="label">с балконом</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div <?$estateValue!=4?print'style="display:none"':'style="display:block"'?> class="inner_block country">
				<div class="row sell">
					<div class="country_left_block">
						<div class="left_block">
							<div class="metric">
								<div style="width:90px;text-align:left;margin-left:8px;" class="label_select">Площадь дома</div>
								<div class="select_block count">
									<input type="hidden" name="squareFullMin">
									<div class="choosed_block">От</div>
									<div class="scrollbar-inner">
										<ul>
											<li class="current"><a href="javascript:void(0)" data-id="">От</a></li>
											<?
											$n = 2;
											for($c=8; $c<=$_max_value_country; $c++){
												if($n==2){
													
													echo '<li><a data-name="squarefullcountry" data-explanation="площадь дома" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
													$n=1;
												}
												else {
													$n++;
												}
											}
											?>
										</ul>
									</div>
								</div>
								<div class="mdash">&ndash;</div>
								<div class="select_block count">
									<input type="hidden" name="squareFullMax">
									<div class="choosed_block">До</div>
									<div class="scrollbar-inner">
										<ul>
											<li class="current"><a href="javascript:void(0)" data-id="">До</a></li>
											<?
											$n = 2;
											for($c=8; $c<=$_max_value_country; $c++){
												if($n==2){
													echo '<li><a data-name="squarefullcountry" data-explanation="площадь дома" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
													$n=1;
												}
												else {
													$n++;
												}
											}
											?>
										</ul>
									</div>
								</div>
								<div class="com">м<sup>2</sup></div>
							</div>
						</div>
						<div class="center_block">
							<div class="metric">
								<div style="width:122px" class="label_select">Площадь участка</div>
								<div class="select_block count">
									<input type="hidden" name="squareLandMin">
									<div class="choosed_block">От</div>
									<div class="scrollbar-inner">
										<ul>
											<li class="current"><a href="javascript:void(0)" data-id="">От</a></li>
											<?
											$n = 2;
											for($c=8; $c<=$_max_value_country_land; $c++){
												if($n==2){
													echo '<li><a data-name="squarelandcountry" data-explanation="площадь участка" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
													$n=1;
												}
												else {
													$n++;
												}
											}
											?>
										</ul>
									</div>
								</div>
								<div class="mdash">&ndash;</div>
								<div class="select_block count">
									<input type="hidden" name="squareLandMax">
									<div class="choosed_block">До</div>
									<div class="scrollbar-inner">
										<ul>
											<li class="current"><a href="javascript:void(0)" data-id="">До</a></li>
											<?
											$n = 2;
											for($c=8; $c<=$_max_value_country_land; $c++){
												if($n==2){
													echo '<li><a data-name="squarelandcountry" data-explanation="площадь участка" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
													$n=1;
												}
												else {
													$n++;
												}
											}
											?>
										</ul>
									</div>
								</div>
								<div style="margin-top:6px" class="com">соток</div>
							</div>
						</div>
						<div class="type_country">
							<div class="check_block">
								<div class="checkbox_single">
									<input type="hidden" name="igs" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">ИЖС</span>
									</div>
								</div>
								<div class="checkbox_single">
									<input type="hidden" name="snt" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">СНТ</span>
									</div>
								</div>
								<div class="checkbox_single">
									<input type="hidden" name="ferma" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">Фермерское хоз-во</span>
									</div>
								</div>
								<div class="checkbox_single">
									<input type="hidden" name="dnp" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">Дачное некоммерческое парт-во</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="line"></div>
					<div class="right_block">
						<div class="check_block secondary">
							<div class="left">
								<div class="checkbox_single">
									<input type="hidden" name="water" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">Вода</span>
									</div>
								</div>
								<div class="checkbox_single first_row">
									<input type="hidden" name="heating" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">Отопление</span>
									</div>
								</div>
								<div class="checkbox_single first_row">
									<input type="hidden" name="gas" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">Газ</span>
									</div>
								</div>
							</div>
							<div class="right">
								<div class="checkbox_single">
									<input type="hidden" name="electricity" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">Электричество</span>
									</div>
								</div>
								<div class="checkbox_single">
									<input type="hidden" name="sewerage" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">Канализация</span>
									</div>
								</div>
								<div class="checkbox_single">
									<input type="hidden" name="sauna" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">Сауна/Баня</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row rent">
					<div class="country_left_block">
						<div class="left_block">
							<div class="metric">
								<div style="width:90px;text-align:left;margin-left:8px;" class="label_select">Площадь дома</div>
								<div class="select_block count">
									<input type="hidden" name="squareFullMin">
									<div class="choosed_block">От</div>
									<div class="scrollbar-inner">
										<ul>
											<li class="current"><a href="javascript:void(0)" data-id="">От</a></li>
											<?
											$n = 2;
											for($c=8; $c<=$_max_value_country; $c++){
												if($n==2){
													
													echo '<li><a data-name="squarefullcountry" data-explanation="площадь дома" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
													$n=1;
												}
												else {
													$n++;
												}
											}
											?>
										</ul>
									</div>
								</div>
								<div class="mdash">&ndash;</div>
								<div class="select_block count">
									<input type="hidden" name="squareFullMax">
									<div class="choosed_block">До</div>
									<div class="scrollbar-inner">
										<ul>
											<li class="current"><a href="javascript:void(0)" data-id="">До</a></li>
											<?
											$n = 2;
											for($c=8; $c<=$_max_value_country; $c++){
												if($n==2){
													echo '<li><a data-name="squarefullcountry" data-explanation="площадь дома" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
													$n=1;
												}
												else {
													$n++;
												}
											}
											?>
										</ul>
									</div>
								</div>
								<div class="com">м<sup>2</sup></div>
							</div>
						</div>
						<div class="center_block">
							<div class="metric">
								<div style="width:122px" class="label_select">Площадь участка</div>
								<div class="select_block count">
									<input type="hidden" name="squareLandMin">
									<div class="choosed_block">От</div>
									<div class="scrollbar-inner">
										<ul>
											<li class="current"><a href="javascript:void(0)" data-id="">От</a></li>
											<?
											$n = 2;
											for($c=8; $c<=$_max_value_country_land; $c++){
												if($n==2){
													echo '<li><a data-name="squarelandcountry" data-explanation="площадь участка" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
													$n=1;
												}
												else {
													$n++;
												}
											}
											?>
										</ul>
									</div>
								</div>
								<div class="mdash">&ndash;</div>
								<div class="select_block count">
									<input type="hidden" name="squareLandMax">
									<div class="choosed_block">До</div>
									<div class="scrollbar-inner">
										<ul>
											<li class="current"><a href="javascript:void(0)" data-id="">До</a></li>
											<?
											$n = 2;
											for($c=8; $c<=$_max_value_country_land; $c++){
												if($n==2){
													echo '<li><a data-name="squarelandcountry" data-explanation="площадь участка" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
													$n=1;
												}
												else {
													$n++;
												}
											}
											?>
										</ul>
									</div>
								</div>
								<div style="margin-top:6px" class="com">соток</div>
							</div>
						</div>
						<div class="type_country">
							<div class="check_block">
								<div class="checkbox_single">
									<input type="hidden" name="igs" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">ИЖС</span>
									</div>
								</div>
								<div class="checkbox_single">
									<input type="hidden" name="snt" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">СНТ</span>
									</div>
								</div>
								<div class="checkbox_single">
									<input type="hidden" name="ferma" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">Фермерское хоз-во</span>
									</div>
								</div>
								<div class="checkbox_single">
									<input type="hidden" name="dnp" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">Дачное некоммерческое парт-во</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="line"></div>
					<div class="right_block">
						<div class="check_block secondary">
							<div class="left">
								<div class="checkbox_single">
									<input type="hidden" name="water" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">Вода</span>
									</div>
								</div>
								<div class="checkbox_single first_row">
									<input type="hidden" name="heating" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">Отопление</span>
									</div>
								</div>
								<div class="checkbox_single first_row">
									<input type="hidden" name="gas" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">Газ</span>
									</div>
								</div>
							</div>
							<div class="right">
								<div class="checkbox_single">
									<input type="hidden" name="electricity" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">Электричество</span>
									</div>
								</div>
								<div class="checkbox_single">
									<input type="hidden" name="sewerage" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">Канализация</span>
									</div>
								</div>
								<div class="checkbox_single">
									<input type="hidden" name="sauna" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">Сауна/Баня</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div <?$estateValue!=5?print'style="display:none"':'style="display:block"'?> class="inner_block commercial">
				<div class="row sell">
					<div class="left_block">
						<div class="metric">
							<div style="width:122px" class="label_select">Площадь помещения</div>
							<div class="select_block count">
								<input type="hidden" name="squareFullMin">
								<div class="choosed_block">От</div>
								<div class="scrollbar-inner">
									<ul>
										<li class="current"><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										$n = 2;
										for($c=8; $c<=$_max_value_square_commer; $c++){
											if($n==2){
												echo '<li><a data-name="fullsquarespace" data-explanation="площадь" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
												$n=1;
											}
											else {
												$n++;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<input type="hidden" name="squareFullMax">
								<div class="choosed_block">До</div>
								<div class="scrollbar-inner">
									<ul>
										<li class="current"><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										$n = 2;
										for($c=8; $c<=$_max_value_square_commer; $c++){
											if($n==2){
												echo '<li><a data-name="fullsquarespace" data-explanation="площадь" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
												$n=1;
											}
											else {
												$n++;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div style="width:122px" class="label_select">Комнат</div>
							<div class="select_block count">
								<input type="hidden" name="roomsMin">
								<div class="choosed_block">От</div>
								<div class="scrollbar-inner">
									<ul>
										<li class="current"><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=1; $c<=$_max_value_rooms_commer; $c++){
											echo '<li><a data-name="roomsspace" data-explanation="комнат" data-tags="от '.$c.'" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<input type="hidden" name="roomsMax">
								<div class="choosed_block">До</div>
								<div class="scrollbar-inner">
									<ul>
										<li class="current"><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=1; $c<=$_max_value_rooms_commer; $c++){
											echo '<li><a data-name="roomsspace" data-explanation="комнат" data-tags="до '.$c.'" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
					</div>
					<div class="center_block">
						<div class="floor">
							<div class="label_select">Этаж</div>
							<div class="select_block count">
								<input type="hidden" name="floorMin">
								<div class="choosed_block">От</div>
								<div class="scrollbar-inner">
									<ul>
										<li class="current"><a href="javascript:void(0)" data-id="">От</a></li> 
										<?
										for($f=1; $f<=$_max_value_floor_commer; $f++){
											echo '<li><a data-name="fullsquarefloor" data-explanation="этаж" data-tags="с '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<input type="hidden" name="floorMax">
								<div class="choosed_block">До</div>
								<div class="scrollbar-inner">
									<ul>
										<li class="current"><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$_max_value_floor_commer; $f++){
											echo '<li><a data-name="fullsquarefloor" data-explanation="этаж" data-tags="по '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="floor">
							<div class="label_select">в доме</div>
							<div class="select_block count">
								<input type="hidden" name="floorsMin">
								<div class="choosed_block">От</div>
								<div class="scrollbar-inner">
									<ul>
										<li class="current"><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$_max_value_floor_commer; $f++){
											echo '<li><a data-name="fullsquarefloors" data-explanation="в доме" data-tags="от '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<input type="hidden" name="floorsMax">
								<div class="choosed_block">До</div>
								<div class="scrollbar-inner">
									<ul>
										<li class="current"><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$_max_value_floor_commer; $f++){
											echo '<li><a data-name="fullsquarefloors" data-explanation="в доме" data-tags="до '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">этажей</div>
						</div>
						<div class="floor">
							<div class="check_block">
								<div class="checkbox_single">
									<input type="hidden" name="semibasement" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">+ Полподвал</span>
									</div>
								</div>
								<div class="checkbox_single">
									<input type="hidden" name="basement" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">+ Подвал</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="line"></div>
					<div class="right_block">
						<div class="checkbox_block">
							<input type="hidden" name="building">
							<div class="select_checkbox">
								<ul>
									<?
									for($c=0; $c<count($_TYPE_BUILDING); $c++){
										$checked = '';
										if(!$building_commercial_sell_var){
											if($c==0){
												$checked = ' class="checked"';
											}
										}
										else {
											if($c==$building_commercial_sell_var){
												$checked = ' class="checked"';
											}
										}
										echo '<li'.$checked.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_BUILDING[$c].'</a></li>';
									}
									?>
								</ul>
							</div>
						</div>
						<div class="clear"></div>
						<div style="margin-left:0;margin-top:17px;" class="checkbox_block">
							<input type="hidden" name="furniture">
							<div class="select_checkbox">
								<ul>
									<?
									for($c=0; $c<count($_TYPE_FURNITURE); $c++){
										$checked = '';
										if(!$furniture_commercial_sell_var){
											if($c==0){
												$checked = ' class="checked"';
											}
										}
										else {
											if($c==$furniture_commercial_sell_var){
												$checked = ' class="checked"';
											}
										}
										echo '<li'.$checked.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_FURNITURE[$c].'</a></li>';
									}
									?>
								</ul>
							</div>
						</div>
						<div class="clear"></div>
						<div style="margin-top:18px" class="check_block">
							<div class="checkbox_single">
								<input type="hidden" name="sep_entrance" disabled="disabled">
								<div class="container">
									<span class="check"></span><span class="label">Только отдельный вход</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row rent">
					<div class="left_block">
						<div class="metric">
							<div style="width:122px" class="label_select">Площадь помещения</div>
							<div class="select_block count">
								<input type="hidden" name="squareFullMin">
								<div class="choosed_block">От</div>
								<div class="scrollbar-inner">
									<ul>
										<li class="current"><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										$n = 2;
										for($c=8; $c<=$_max_value_square_commer; $c++){
											if($n==2){
												echo '<li><a href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
												$n=1;
											}
											else {
												$n++;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<input type="hidden" name="squareFullMax">
								<div class="choosed_block">До</div>
								<div class="scrollbar-inner">
									<ul>
										<li class="current"><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										$n = 2;
										for($c=8; $c<=$_max_value_square_commer; $c++){
											if($n==2){
												echo '<li><a href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
												$n=1;
											}
											else {
												$n++;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div style="width:122px" class="label_select">Комнат</div>
							<div class="select_block count">
								<input type="hidden" name="roomsMin">
								<div class="choosed_block">От</div>
								<div class="scrollbar-inner">
									<ul>
										<li class="current"><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=1; $c<=$_max_value_rooms_commer; $c++){
											echo '<li><a href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<input type="hidden" name="roomsMax">
								<div class="choosed_block">До</div>
								<div class="scrollbar-inner">
									<ul>
										<li class="current"><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=1; $c<=$_max_value_rooms_commer; $c++){
											echo '<li><a href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
					</div>
					<div class="center_block">
						<div class="floor">
							<div class="label_select">Этаж</div>
							<div class="select_block count">
								<input type="hidden" name="floorMin">
								<div class="choosed_block">От</div>
								<div class="scrollbar-inner">
									<ul>
										<li class="current"><a href="javascript:void(0)" data-id="">От</a></li> 
										<?
										for($f=1; $f<=$_max_value_floor_commer; $f++){
											echo '<li><a href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<input type="hidden" name="floorMax">
								<div class="choosed_block">До</div>
								<div class="scrollbar-inner">
									<ul>
										<li class="current"><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$_max_value_floor_commer; $f++){
											echo '<li><a href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="floor">
							<div class="label_select">в доме</div>
							<div class="select_block count">
								<input type="hidden" name="floorsMin">
								<div class="choosed_block">От</div>
								<div class="scrollbar-inner">
									<ul>
										<li class="current"><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$_max_value_floor_commer; $f++){
											echo '<li><a href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<input type="hidden" name="floorsMax">
								<div class="choosed_block">До</div>
								<div class="scrollbar-inner">
									<ul>
										<li class="current"><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$_max_value_floor_commer; $f++){
											echo '<li><a href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">этажей</div>
						</div>
						<div class="floor">
							<div class="check_block">
								<div class="checkbox_single">
									<input type="hidden" name="semibasement" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">+ Полподвал</span>
									</div>
								</div>
								<div class="checkbox_single">
									<input type="hidden" name="basement" disabled="disabled">
									<div class="container">
										<span class="check"></span><span class="label">+ Подвал</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="line"></div>
					<div class="right_block">
						<div class="checkbox_block">
							<input type="hidden" name="building">
							<div class="select_checkbox">
								<ul>
									<?
									for($c=0; $c<count($_TYPE_BUILDING); $c++){
										$checked = '';
										if(!$building_commercial_rent_var){
											if($c==0){
												$checked = ' class="checked"';
											}
										}
										else {
											if($c==$building_commercial_rent_var){
												$checked = ' class="checked"';
											}
										}
										echo '<li'.$checked.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_BUILDING[$c].'</a></li>';
									}
									?>
								</ul>
							</div>
						</div>
						<div class="clear"></div>
						<div style="margin-left:0;margin-top:17px;" class="checkbox_block">
							<input type="hidden" name="furniture">
							<div class="select_checkbox">
								<ul>
									<?
									for($c=0; $c<count($_TYPE_FURNITURE); $c++){
										$checked = '';
										if(!$furniture_commercial_rent_var){
											if($c==0){
												$checked = ' class="checked"';
											}
										}
										else {
											if($c==$furniture_commercial_rent_var){
												$checked = ' class="checked"';
											}
										}
										echo '<li'.$checked.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_FURNITURE[$c].'</a></li>';
									}
									?>
								</ul>
							</div>
						</div>
						<div class="clear"></div>
						<div style="margin-top:18px" class="check_block">
							<div class="checkbox_single">
								<input type="hidden" name="sep_entrance" disabled="disabled">
								<div class="container">
									<span class="check"></span><span class="label">Только отдельный вход</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div <?!$type_new_sell_var || !$estate_new_sell_var?print'style="display:none"':''?> class="selected_params new sell"><ul><?=$priceChangeNewSell?><?=$roomsChangeNewSell?><?=$areaChange?><?=$stationChange?><?=$buildChange?><?=$developerChange?><?=$squareChangeNewSell?><?=$squareLiveChangeNewSell?><?=$squareKitchenChangeNewSell?><?=$floorChangeNewSell?><?=$floorsChangeNewSell?><?=$deliveryChangeNewSell?><?=$finishingChangeNewSell?><?=$readyChangeNewSell?><?=$far_subwayChangeNewSell?><?=$type_houseChangeNewSell?></ul></div>
		<div <?$type_cession_sell_var?print'style="display:block"':'style="display:none"'?> class="selected_params cession sell"><ul><?=$priceChangeCessionSell?><?=$roomsChangeCessionSell?><?=$areaChange?><?=$stationChange?><?=$buildChange?><?=$developerChange?><?=$squareChangeCessionSell?><?=$squareLiveChangeCessionSell?><?=$squareKitchenChangeCessionSell?><?=$floorChangeCessionSell?><?=$floorsChangeCessionSell?><?=$deliveryChangeCessionSell?><?=$finishingChangeCessionSell?><?=$readyChangeCessionSell?><?=$far_subwayChangeCessionSell?><?=$type_houseChangeCessionSell?></ul></div>
		<div <?$type_flat_sell_var?print'style="display:block"':'style="display:none"'?> class="selected_params flat sell"><ul><?=$priceChangeFlatSell?><?=$roomsChangeFlatSell?><?=$areaChange?><?=$stationChange?><?=$squareChangeFlatSell?><?=$squareLiveChangeFlatSell?><?=$squareKitchenChangeFlatSell?><?=$floorChangeFlatSell?><?=$floorsChangeFlatSell?><?=$far_subwayChangeFlatSell?><?=$repairsChangeFlatSell?><?=$wcChangeFlatSell?><?=$type_houseChangeFlatSell?><?=$transactionChangeFlatSell?><?=$shareChangeFlatSell?></ul></div>
		<div <?$type_flat_rent_var?print'style="display:block"':'style="display:none"'?> class="selected_params flat rent"><ul><?=$priceChangeFlatRent?><?=$roomsChangeFlatRent?><?=$areaChange?><?=$stationChange?><?=$squareChangeFlatRent?><?=$squareLiveChangeFlatRent?><?=$squareKitchenChangeFlatRent?><?=$floorChangeFlatRent?><?=$floorsChangeFlatRent?><?=$far_subwayChangeFlatRent?><?=$repairsChangeFlatRent?><?=$wcChangeFlatRent?><?=$type_houseChangeFlatRent?></ul></div>
		<div <?$type_room_sell_var?print'style="display:block"':'style="display:none"'?> class="selected_params room sell"><ul><?=$priceChangeRoomSell?><?=$roomsChangeRoomSell?><?=$squareFullChangeRoomSell?><?=$areaChange?><?=$stationChange?><?=$squareChangeRoomSell?><?=$squareLiveChangeRoomSell?><?=$squareKitchenChangeRoomSell?><?=$floorChangeRoomSell?><?=$floorsChangeRoomSell?><?=$far_subwayChangeRoomSell?><?=$repairsChangeRoomSell?><?=$wcChangeRoomSell?><?=$type_houseChangeRoomSell?><?=$transactionChangeRoomSell?><?=$shareChangeRoomSell?></ul></div>
		<div <?$type_room_rent_var?print'style="display:block"':'style="display:none"'?> class="selected_params room rent"><ul><?=$priceChangeRoomRent?><?=$roomsChangeRoomRent?><?=$squareFullChangeRoomRent?><?=$areaChange?><?=$stationChange?><?=$squareChangeRoomRent?><?=$squareLiveChangeRoomRent?><?=$squareKitchenChangeRoomRent?><?=$floorChangeRoomRent?><?=$floorsChangeRoomRent?><?=$far_subwayChangeRoomRent?><?=$repairsChangeRoomRent?><?=$wcChangeRoomRent?><?=$type_houseChangeRoomRent?></ul></div>
		<div <?$type_country_sell_var?print'style="display:block"':'style="display:none"'?> class="selected_params country sell"><ul><?=$priceChangeCountrySell?><?=$typeCountryChangeCountrySell?><?=$areaChange?><?=$stationChange?><?=$squareChangeCountrySell?><?=$squareLiveChangeCountrySell?><?=$squareKitchenChangeCountrySell?><?=$floorChangeCountrySell?><?=$floorsChangeCountrySell?><?=$far_subwayChangeCountrySell?><?=$repairsChangeCountrySell?><?=$wcChangeCountrySell?><?=$type_houseChangeCountrySell?><?=$transactionChangeCountrySell?></ul></div>
		<div <?$type_country_rent_var?print'style="display:block"':'style="display:none"'?> class="selected_params country rent"><ul><?=$priceChangeCountryRent?><?=$typeCountryChangeCountryRent?><?=$areaChange?><?=$stationChange?><?=$squareChangeCountryRent?><?=$squareLiveChangeCountryRent?><?=$squareKitchenChangeCountryRent?><?=$floorChangeCountryRent?><?=$floorsChangeCountryRent?><?=$far_subwayChangeCountryRent?><?=$repairsChangeCountryRent?><?=$wcChangeCountryRent?><?=$type_houseChangeCountryRent?></ul></div>
		<div <?$type_commercial_sell_var?print'style="display:block"':'style="display:none"'?> class="selected_params commercial sell"><ul><?=$priceChangeCommercialSell?><?=$typeCommerChangeCommercialSell?><?=$areaChange?><?=$stationChange?><?=$squareChangeCommercialSell?><?=$squareLiveChangeCommercialSell?><?=$squareKitchenChangeCommercialSell?><?=$floorChangeCommercialSell?><?=$floorsChangeCommercialSell?><?=$far_subwayChangeCommercialSell?><?=$repairsChangeCommercialSell?><?=$wcChangeCommercialSell?><?=$type_houseChangeCommercialSell?><?=$transactionChangeCommercialSell?></ul></div>
		<div <?$type_commercial_rent_var?print'style="display:block"':'style="display:none"'?> class="selected_params commercial rent"><ul><?=$priceChangeCommercialRent?><?=$typeCommerChangeCommercialRent?><?=$areaChange?><?=$stationChange?><?=$squareChangeCommercialRent?><?=$squareLiveChangeCommercialRent?><?=$squareKitchenChangeCommercialRent?><?=$floorChangeCommercialRent?><?=$floorsChangeCommercialRent?><?=$far_subwayChangeCommercialRent?><?=$repairsChangeCommercialRent?><?=$wcChangeCommercialRent?><?=$type_houseChangeCommercialRent?></ul></div>
	</form>
</div>
<?
// echo '<div>Время выполнения скрипта: '.(microtime(true) - $start).' сек.</div>';
?>