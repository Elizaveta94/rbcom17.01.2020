<?
	header("content-type:text/html;charset=UTF-8");
	include($_SERVER['DOCUMENT_ROOT'].'/admin_2/include/bd.php');
	include($_SERVER['DOCUMENT_ROOT'].'/admin_2/include/functions.php');

	$SearchParameters = array();
	$SearchParamStation = array();
	$SearchParamRailway = array();
	$SearchParamAddress = array();
	// $start = microtime(true);
	/* 
	*  Все имеющиеся ЖК
	*/
	$res = mysql_query("
		SELECT c.*,
		(SELECT title FROM ".$template."_stations WHERE activation='1' && id=c.station) AS stationName,
		(SELECT name FROM ".$template."_location WHERE activation='1' && id=c.location) AS locationName
		FROM ".$template."_m_catalogue_left AS c
		WHERE c.activation='1'
		ORDER BY c.name
	") or die(mysql_error());
	$id_array = 0;
	if(mysql_num_rows($res)>0){
		while($row = mysql_fetch_assoc($res)){
			$name = htmlspecialchars_decode($row['name']);
			$name = str_replace('ЖК "','',$name);
			$name = str_replace('"','',$name);
			$name = "ЖК ".$name;
			$dist_value = $row['dist_value'];
			$dist_value2 = $row['dist_value'];
			if($dist_value/1000>=1){
				$dist_value = price_cell($dist_value/1000,2).' км';
			}
			else {
				$dist_value = price_cell($dist_value,0).' м';
			}
			$type = array(
				'id' => $row['id'],
				'name' => $name.' ('.$row['locationName'].')',
				'alias' => $row['link'],
				'address' => $row['address'],
				'station_name' => $row['stationName'],
				'station' => $row['station'],
				'coords' => $row['coords'],
				'distance' => $dist_value,
				'dist_value' => $dist_value2,
			);
			array_push($SearchParameters,$type);
		}
	}
	
	$_SearchParameters = $SearchParameters;
	$SearchParameters = json_encode($SearchParameters);
	$SearchParameters = normJsonStr($SearchParameters);
	
	/* 
	*  Все имеющиеся метро Санкт-Петербурга
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_stations
		WHERE activation='1' && city_id='1'
		ORDER BY title
	") or die(mysql_error());
	$id_array = 0;
	if(mysql_num_rows($res)>0){
		while($row = mysql_fetch_assoc($res)){
			$name = htmlspecialchars_decode($row['title']);
			if($dist_value/1000>=1){
				$dist_value = price_cell($dist_value/1000,2).' км';
			}
			else {
				$dist_value = price_cell($dist_value,0).' м';
			}
			$type = array(
				'id' => $row['id'],
				'name' => $name,
				'alias' => $row['link'],
			);
			array_push($SearchParamStation,$type);
		}
	}
		
	$_SearchParameters = $SearchParamStation;
	$SearchParamStation = json_encode($SearchParamStation);
	$SearchParamStation = normJsonStr($SearchParamStation);
	
	
	/* 
	*  Все улицы новостроек
	*/
	// $res = mysql_query("
		// SELECT c.*,
		// (SELECT title FROM ".$template."_stations WHERE activation='1' && id=c.station) AS stationName
		// FROM ".$template."_m_catalogue_left as c
		// WHERE c.activation='1'
		// GROUP BY c.address
		// ORDER BY c.address
	// ") or die(mysql_error());
	// if(mysql_num_rows($res)>0){
		// while($row = mysql_fetch_assoc($res)){
			// $name = htmlspecialchars_decode($row['name']);
			// $name = str_replace('ЖК "','',$name);
			// $name = str_replace('"','',$name);
			// $name = "ЖК ".$name;
			// $dist_value = $row['dist_value'];
			// if($dist_value/1000>=1){
				// $dist_value = price_cell($dist_value/1000,2).' км';
			// }
			// else {
				// $dist_value = price_cell($dist_value,0).' м';
			// }
			// $type = array(
				// 'id' => $row['id'],
				// 'name' => htmlspecialchars_decode($row['address']),
				// 'build_text' => $name,
				// 'build' => htmlspecialchars_decode($row['id']),
				// 'station' => $row['station'],
				// 'station_name' => $row['stationName'],
				// 'distance' => $dist_value,
			// );
			// array_push($SearchParamAddress,$type);
		// }
	// }
		
	// $_SearchParameters = $SearchParamAddress;
	// $SearchParamAddress = json_encode($SearchParamAddress);
	// $SearchParamAddress = normJsonStr($SearchParamAddress);
	
	
	/* 
	*  Все имеющиеся Ж/Д станции в базе
	*/
	$res = mysql_query("
		SELECT r.*,
		(SELECT name FROM ".$template."_railway_lines WHERE activation=1 && id=r.p_main) AS nameWay
		FROM ".$template."_railways AS r
		WHERE r.activation='1'
		ORDER BY r.name
	") or die(mysql_error());
	$id_array = 0;
	if(mysql_num_rows($res)>0){
		while($row = mysql_fetch_assoc($res)){
			$name = htmlspecialchars_decode($row['name']);
			$type = array(
				'id' => $row['id'],
				'name' => $name,
				'nameWay' => $row['nameWay'],
			);
			array_push($SearchParamRailway,$type);
		}
	}
		
	$_SearchParameters = $SearchParamRailway;
	$SearchParamRailway = json_encode($SearchParamRailway);
	$SearchParamRailway = normJsonStr($SearchParamRailway);	
	// echo round(microtime(true) - $start,5);
?>
SearchParam = <?=$SearchParameters?>;
SearchParamStation = <?=$SearchParamStation?>;
SearchParamRailway = <?=$SearchParamRailway?>;
<!--SearchParamAddress = <?=$SearchParamAddress?>;-->