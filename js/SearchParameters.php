<?
	include($_SERVER['DOCUMENT_ROOT'].'/admin_2/include/bd.php');
	include($_SERVER['DOCUMENT_ROOT'].'/admin_2/include/functions.php');

	$SearchParameters = array();

	/*
	*  Задаём начало массива для новостроек
	*/
	$arr = array(
		"id_part" => 1, //ID раздела для поиска
		"name" => "Новостройки",
		"searchArray" => array()
	);
	array_push($SearchParameters,$arr);
	
	// echo '<pre>';
	// print_r($SearchParameters);
	// echo '</pre>';
	
	/* 
	*  Все имеющиеся ЖК
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_m_catalogue_left
		WHERE activation='1'
		ORDER BY name
	") or die(mysql_error());
	$id_array = 0;
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 1,
			'name'  => 'Название ЖК',
			'searchName'  => 'build',
			'important'  => 6,
			'searchValues'  => array(),
		);
		for($c=0; $c<count($SearchParameters); $c++){
			if($SearchParameters[$c]['id_part']==1){
				$id_array = $c;
				break;
			}
		}
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$type = array(
				'id' => $row['id'],
				'name' => htmlspecialchars_decode($row['name']),
				'parentLink' => 'newbuilding',
				'alias' => $row['link'],
			);
			array_push($SearchParameters[$id_array]['searchArray'][0]['searchValues'],$type);
		}
	}
	
	/*
	*  Все квартиры новостроек
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_new_flats
		WHERE activation='1'
		GROUP BY rooms
		ORDER BY rooms
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 2,
			'name'  => '',
			'searchName'  => 'rooms',
			'important'  => 4,
			'searchValues'  => array(),
		);
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$name_room = $row['rooms'].' комн. квартира';
			if($row['rooms']==0){
				$name_room = 'Студия';
			}
			$type = array(
				'id' => $row['rooms'],
				'name' => $name_room,
				'parentLink' => 'rooms',
				'alias' => $row['link'],
			);
			array_push($SearchParameters[$id_array]['searchArray'][1]['searchValues'],$type);
		}
	}

	/* 
	*  Станции метро
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_stations
		WHERE activation='1'
		GROUP BY title
		ORDER BY title
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 3,
			'name'  => 'Станции метро',
			'searchName'  => 'station',
			'important'  => 5,
			'searchValues'  => array(),
		);
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$type = array(
				'id' => $row['id'],
				'name' => 'м. '.htmlspecialchars_decode($row['title']),
				'parentLink' => '',
				'alias' => '',
			);
			array_push($SearchParameters[$id_array]['searchArray'][2]['searchValues'],$type);
		}
	}

	/* 
	*  Все районы города
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_areas
		WHERE activation='1' && city_id='1'
		GROUP BY title
		ORDER BY title
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 4,
			'name'  => 'Районы',
			'searchName'  => 'area',
			'important'  => 4,
			'searchValues'  => array(),
		);
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$type = array(
				'id' => $row['id'],
				'name' => htmlspecialchars_decode($row['title']).' район',
				'parentLink' => '',
				'alias' => '',
			);
			array_push($SearchParameters[$id_array]['searchArray'][3]['searchValues'],$type);
		}
	}

	/* 
	*  Все улицы города
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_m_catalogue_left
		WHERE activation='1'
		GROUP BY address
		ORDER BY address
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 5,
			'name'  => 'Адрес',
			'searchName'  => 'address',
			'important'  => 3,
			'searchValues'  => array(),
		);
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$type = array(
				'id' => $row['id'],
				'name' => htmlspecialchars_decode($row['address']),
				'parentLink' => '',
				'alias' => '',
			);
			array_push($SearchParameters[$id_array]['searchArray'][4]['searchValues'],$type);
		}
	}
	
	
	

	/*
	*  Задаём начало массива для Переуступок
	*/
	$arr = array(
		"id_part" => 2, //ID раздела для поиска
		"name" => "Переуступки",
		"searchArray" => array()
	);
	array_push($SearchParameters,$arr);
	
	// echo '<pre>';
	// print_r($SearchParameters);
	// echo '</pre>';
	
	/* 
	*  Все имеющиеся ЖК
	*/
	$res = mysql_query("
		SELECT m.name, m.id, m.link
		FROM ".$template."_cessions AS c
		LEFT JOIN ".$template."_m_catalogue_left AS m
		ON m.id=c.p_main && m.activation='1'
		WHERE c.activation='1' && m.activation='1'
		ORDER BY m.name
	") or die(mysql_error());
	$id_array = 0;
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 2,
			'name'  => 'Название ЖК',
			'searchName'  => 'build',
			'important'  => 5,
			'searchValues'  => array(),
		);
		for($c=0; $c<count($SearchParameters); $c++){
			if($SearchParameters[$c]['id_part']==2){
				$id_array = $c;
				break;
			}
		}
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		/*
		*  Массив исключиельных ID
		*/
		$arrayBuilds = array();
		$arrayBuildsInfo = array();
		while($row = mysql_fetch_assoc($res)){
			if(!in_array($row['id'],$arrayBuilds)){
				$type = array(
					'id' => $row['id'],
					'name' => htmlspecialchars_decode($row['name']),
					'parentLink' => 'cession',
					'alias' => $row['link'],
				);
				array_push($arrayBuilds,$row['id']);
				array_push($arrayBuildsInfo,$type);
			}
		}
		for($p=0; $p<count($arrayBuildsInfo); $p++){
			// echo '<pre>';
			// print_r($arrayBuildsInfo[$p]);
			// echo '</pre>';
			array_push($SearchParameters[$id_array]['searchArray'][0]['searchValues'],$arrayBuildsInfo[$p]);
		}
	}
	
	/*
	*  Все квартиры новостроек
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_cessions
		WHERE activation='1'
		GROUP BY rooms
		ORDER BY rooms
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 2,
			'name'  => '',
			'searchName'  => 'rooms',
			'important'  => 4,
			'searchValues'  => array(),
		);
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$name_room = $row['rooms'].' комн. квартира';
			if($row['rooms']==0){
				$name_room = 'Студия';
			}
			$type = array(
				'id' => $row['rooms'],
				'name' => $name_room,
				'parentLink' => 'rooms',
				'alias' => $row['link'],
			);
			array_push($SearchParameters[$id_array]['searchArray'][1]['searchValues'],$type);
		}
	}

	/* 
	*  Станции метро
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_stations
		WHERE activation='1'
		GROUP BY title
		ORDER BY title
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 3,
			'name'  => 'Станции метро',
			'searchName'  => 'station',
			'important'  => 5,
			'searchValues'  => array(),
		);
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$type = array(
				'id' => $row['id'],
				'name' => 'м. '.htmlspecialchars_decode($row['title']),
				'parentLink' => '',
				'alias' => '',
			);
			array_push($SearchParameters[$id_array]['searchArray'][2]['searchValues'],$type);
		}
	}

	/* 
	*  Все районы города
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_areas
		WHERE activation='1' && city_id='1'
		GROUP BY title
		ORDER BY title
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 3,
			'name'  => 'Районы',
			'searchName'  => 'area',
			'important'  => 4,
			'searchValues'  => array(),
		);
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$type = array(
				'id' => $row['id'],
				'name' => htmlspecialchars_decode($row['title']).' район',
				'parentLink' => '',
				'alias' => '',
			);
			array_push($SearchParameters[$id_array]['searchArray'][3]['searchValues'],$type);
		}
	}

	/* 
	*  Все улицы города
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_cessions
		WHERE activation='1'
		GROUP BY address
		ORDER BY address
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 4,
			'name'  => 'Адрес',
			'searchName'  => 'address',
			'important'  => 3,
			'searchValues'  => array(),
		);
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$type = array(
				'id' => $row['id'],
				'name' => htmlspecialchars_decode($row['address']),
				'parentLink' => '',
				'alias' => '',
			);
			array_push($SearchParameters[$id_array]['searchArray'][4]['searchValues'],$type);
		}
	}
	
	
	
	
	/*
	*  Задаём начало массива для квартир (купить)
	*/
	$arr = array(
		"id_part" => 3, //ID раздела для поиска
		"name" => "Квартира (купить)",
		"searchArray" => array()
	);
	array_push($SearchParameters,$arr);
		
	/* 
	*  Все имеющиеся квартиры
	*/
	$res = mysql_query("
		SELECT s.type,s.address,s.rooms
		FROM ".$template."_second_flats AS s
		WHERE s.activation='1' && type='sell'
		GROUP BY s.rooms
		ORDER BY s.rooms
	") or die(mysql_error());
	$id_array = 0;
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 1,
			'name'  => '',
			'searchName'  => 'rooms',
			'important'  => 4,
			'searchValues'  => array(),
		);
		for($c=0; $c<count($SearchParameters); $c++){
			if($SearchParameters[$c]['id_part']==3){
				$id_array = $c;
				break;
			}
		}
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			// echo '<pre>';
			// print_r($row);
			// echo '</pre>';
			$type = array(
				'id' => $row['rooms'],
				'name' => $row['rooms'].' комн. квартира',
				'parentLink' => 'flats',
				'alias' => $row['link'],
			);
			array_push($SearchParameters[$id_array]['searchArray'][0]['searchValues'],$type);
		}
	}

	/* 
	*  Станции метро
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_stations
		WHERE activation='1'
		GROUP BY title
		ORDER BY title
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 2,
			'name'  => 'Станции метро',
			'searchName'  => 'station',
			'important'  => 5,
			'searchValues'  => array(),
		);
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$type = array(
				'id' => $row['id'],
				'name' => 'м. '.htmlspecialchars_decode($row['title']),
				'parentLink' => '',
				'alias' => '',
			);
			array_push($SearchParameters[$id_array]['searchArray'][1]['searchValues'],$type);
		}
	}

	/* 
	*  Все районы города
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_areas
		WHERE activation='1' && city_id='1'
		GROUP BY title
		ORDER BY title
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 3,
			'name'  => 'Районы',
			'searchName'  => 'area',
			'important'  => 4,
			'searchValues'  => array(),
		);
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$type = array(
				'id' => $row['id'],
				'name' => htmlspecialchars_decode($row['title']).' район',
				'parentLink' => '',
				'alias' => '',
			);
			array_push($SearchParameters[$id_array]['searchArray'][2]['searchValues'],$type);
		}
	}

	/* 
	*  Все улицы города
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_second_flats
		WHERE activation='1' && type='sell'
		GROUP BY address
		ORDER BY address
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 4,
			'name'  => 'Адрес',
			'searchName'  => 'address',
			'important'  => 3,
			'searchValues'  => array(),
		);
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$type = array(
				'id' => $row['id'],
				'name' => htmlspecialchars_decode($row['address']),
				'parentLink' => '',
				'alias' => '',
			);
			array_push($SearchParameters[$id_array]['searchArray'][3]['searchValues'],$type);
		}
	}
	
	
	
	
	/*
	*  Задаём начало массива для квартир (снять)
	*/
	$arr = array(
		"id_part" => 4, //ID раздела для поиска
		"name" => "Квартира (купить)",
		"searchArray" => array()
	);
	array_push($SearchParameters,$arr);
		
	/* 
	*  Все имеющиеся квартиры
	*/
	$res = mysql_query("
		SELECT s.type,s.address,s.rooms
		FROM ".$template."_second_flats AS s
		WHERE s.activation='1' && type='rent'
		GROUP BY s.rooms
		ORDER BY s.rooms
	") or die(mysql_error());
	$id_array = 0;
	for($c=0; $c<count($SearchParameters); $c++){
		if($SearchParameters[$c]['id_part']==4){
			$id_array = $c;
			break;
		}
	}
	$type = array(
		'id'    => 1,
		'name'  => '',
		'searchName'  => 'rooms',
		'important'  => 4,
		'searchValues'  => array(),
	);
	array_push($SearchParameters[$id_array]['searchArray'],$type);
	if(mysql_num_rows($res)>0){
		while($row = mysql_fetch_assoc($res)){
			// echo '<pre>';
			// print_r($row);
			// echo '</pre>';
			$type = array(
				'id' => $row['rooms'],
				'name' => $row['rooms'].' комн. квартира',
				'parentLink' => 'flats',
				'alias' => $row['link'],
			);
			array_push($SearchParameters[$id_array]['searchArray'][0]['searchValues'],$type);
		}
	}

	/* 
	*  Станции метро
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_stations
		WHERE activation='1'
		GROUP BY title
		ORDER BY title
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 2,
			'name'  => 'Станции метро',
			'searchName'  => 'station',
			'important'  => 5,
			'searchValues'  => array(),
		);
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$type = array(
				'id' => $row['id'],
				'name' => 'м. '.htmlspecialchars_decode($row['title']),
				'parentLink' => '',
				'alias' => '',
			);
			array_push($SearchParameters[$id_array]['searchArray'][1]['searchValues'],$type);
		}
	}

	/* 
	*  Все районы города
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_areas
		WHERE activation='1' && city_id='1'
		GROUP BY title
		ORDER BY title
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 3,
			'name'  => 'Районы',
			'searchName'  => 'area',
			'important'  => 4,
			'searchValues'  => array(),
		);
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$type = array(
				'id' => $row['id'],
				'name' => htmlspecialchars_decode($row['title']).' район',
				'parentLink' => '',
				'alias' => '',
			);
			array_push($SearchParameters[$id_array]['searchArray'][2]['searchValues'],$type);
		}
	}

	/* 
	*  Все улицы города
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_second_flats
		WHERE activation='1' && type='sell'
		GROUP BY address
		ORDER BY address
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 4,
			'name'  => 'Адрес',
			'searchName'  => 'address',
			'important'  => 3,
			'searchValues'  => array(),
		);
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$type = array(
				'id' => $row['id'],
				'name' => htmlspecialchars_decode($row['address']),
				'parentLink' => '',
				'alias' => '',
			);
			array_push($SearchParameters[$id_array]['searchArray'][3]['searchValues'],$type);
		}
	}
	
	
	
	
	/*
	*  Задаём начало массива для комнат (купить)
	*/
	$arr = array(
		"id_part" => 5, //ID раздела для поиска
		"name" => "Комната (купить)",
		"searchArray" => array()
	);
	array_push($SearchParameters,$arr);
		
	/* 
	*  Все имеющиеся квартиры
	*/
	$res = mysql_query("
		SELECT s.type,s.address,s.rooms_flat
		FROM ".$template."_rooms AS s
		WHERE s.activation='1' && type='sell'
		GROUP BY s.rooms_flat
		ORDER BY s.rooms_flat
	") or die(mysql_error());
	$id_array = 0;
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 1,
			'name'  => '',
			'searchName'  => 'rooms',
			'important'  => 4,
			'searchValues'  => array(),
		);
		for($c=0; $c<count($SearchParameters); $c++){
			if($SearchParameters[$c]['id_part']==5){
				$id_array = $c;
				break;
			}
		}
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			// echo '<pre>';
			// print_r($row);
			// echo '</pre>';
			$type = array(
				'id' => $row['rooms'],
				'name' => $row['rooms'].' комн. квартира',
				'parentLink' => 'flats',
				'alias' => $row['link'],
			);
			array_push($SearchParameters[$id_array]['searchArray'][0]['searchValues'],$type);
		}
	}

	/* 
	*  Станции метро
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_stations
		WHERE activation='1'
		GROUP BY title
		ORDER BY title
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 2,
			'name'  => 'Станции метро',
			'searchName'  => 'station',
			'important'  => 5,
			'searchValues'  => array(),
		);
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$type = array(
				'id' => $row['id'],
				'name' => 'м. '.htmlspecialchars_decode($row['title']),
				'parentLink' => '',
				'alias' => '',
			);
			array_push($SearchParameters[$id_array]['searchArray'][1]['searchValues'],$type);
		}
	}

	/* 
	*  Все районы города
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_areas
		WHERE activation='1' && city_id='1'
		GROUP BY title
		ORDER BY title
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 3,
			'name'  => 'Районы',
			'searchName'  => 'area',
			'important'  => 4,
			'searchValues'  => array(),
		);
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$type = array(
				'id' => $row['id'],
				'name' => htmlspecialchars_decode($row['title']).' район',
				'parentLink' => '',
				'alias' => '',
			);
			array_push($SearchParameters[$id_array]['searchArray'][2]['searchValues'],$type);
		}
	}

	/* 
	*  Все улицы города
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_rooms
		WHERE activation='1' && type='sell'
		GROUP BY address
		ORDER BY address
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 4,
			'name'  => 'Адрес',
			'searchName'  => 'address',
			'important'  => 3,
			'searchValues'  => array(),
		);
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$type = array(
				'id' => $row['id'],
				'name' => htmlspecialchars_decode($row['address']),
				'parentLink' => '',
				'alias' => '',
			);
			array_push($SearchParameters[$id_array]['searchArray'][3]['searchValues'],$type);
		}
	}
	
	
	
	
	/*
	*  Задаём начало массива для комнат (снять)
	*/
	$arr = array(
		"id_part" => 6, //ID раздела для поиска
		"name" => "Комната (снять)",
		"searchArray" => array()
	);
	array_push($SearchParameters,$arr);
		
	/* 
	*  Все имеющиеся квартиры
	*/
	$res = mysql_query("
		SELECT s.type,s.address,s.rooms_flat
		FROM ".$template."_rooms AS s
		WHERE s.activation='1' && type='rent'
		GROUP BY s.rooms_flat
		ORDER BY s.rooms_flat
	") or die(mysql_error());
	$id_array = 0;
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 1,
			'name'  => '',
			'searchName'  => 'rooms',
			'important'  => 4,
			'searchValues'  => array(),
		);
		for($c=0; $c<count($SearchParameters); $c++){
			if($SearchParameters[$c]['id_part']==6){
				$id_array = $c;
				break;
			}
		}
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			// echo '<pre>';
			// print_r($row);
			// echo '</pre>';
			$type = array(
				'id' => $row['rooms'],
				'name' => $row['rooms'].' комн. квартира',
				'parentLink' => 'flats',
				'alias' => $row['link'],
			);
			array_push($SearchParameters[$id_array]['searchArray'][0]['searchValues'],$type);
		}
	}

	/* 
	*  Станции метро
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_stations
		WHERE activation='1'
		GROUP BY title
		ORDER BY title
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 2,
			'name'  => 'Станции метро',
			'searchName'  => 'station',
			'important'  => 5,
			'searchValues'  => array(),
		);
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$type = array(
				'id' => $row['id'],
				'name' => 'м. '.htmlspecialchars_decode($row['title']),
				'parentLink' => '',
				'alias' => '',
			);
			array_push($SearchParameters[$id_array]['searchArray'][1]['searchValues'],$type);
		}
	}

	/* 
	*  Все районы города
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_areas
		WHERE activation='1' && city_id='1'
		GROUP BY title
		ORDER BY title
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 3,
			'name'  => 'Районы',
			'searchName'  => 'area',
			'important'  => 4,
			'searchValues'  => array(),
		);
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$type = array(
				'id' => $row['id'],
				'name' => htmlspecialchars_decode($row['title']).' район',
				'parentLink' => '',
				'alias' => '',
			);
			array_push($SearchParameters[$id_array]['searchArray'][2]['searchValues'],$type);
		}
	}

	/* 
	*  Все улицы города
	*/
	$res = mysql_query("
		SELECT *
		FROM ".$template."_rooms
		WHERE activation='1' && type='rent'
		GROUP BY address
		ORDER BY address
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$type = array(
			'id'    => 4,
			'name'  => 'Адрес',
			'searchName'  => 'address',
			'important'  => 3,
			'searchValues'  => array(),
		);
		array_push($SearchParameters[$id_array]['searchArray'],$type);
		while($row = mysql_fetch_assoc($res)){
			$type = array(
				'id' => $row['id'],
				'name' => htmlspecialchars_decode($row['address']),
				'parentLink' => '',
				'alias' => '',
			);
			array_push($SearchParameters[$id_array]['searchArray'][3]['searchValues'],$type);
		}
	}
	
	$_SearchParameters = $SearchParameters;
	$SearchParameters = json_encode($SearchParameters);
	function normJsonStr($str){
		$str = preg_replace_callback('/\\\u([a-f0-9]{4})/i', create_function('$m', 'return chr(hexdec($m[1])-1072+224);'), $str);
		return iconv('cp1251', 'utf-8', $str);
	}
	$SearchParameters = normJsonStr($SearchParameters);
?>
SearchParameters = <?=$SearchParameters?>;