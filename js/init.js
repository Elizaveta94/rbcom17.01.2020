$(document).ready(function(){
	var start = new Date();	
	console.time('trigger');
	mainPage();
	placement();
	tooltipInstall();
	initSliderFunc();
	sendFormPay();
	loadPeppermint();
	phoneShowClick();
	// scrollLoad();
	
	if($('body').find('div[data-max-element]').length>0){
		var scrH = $(window).height();
		var grand = $('body').find('div[data-max-element]').attr('id');
		if(grand===undefined){grand = '#'+grand;}
		var way = grand;
		var way = $(way);
		var scrHP = way.height();
		var max = way.data("max-element");
		
		var scro = $(this).scrollTop();
		var scrHP = way.height()+250;
		var scrH2 = 0;
		scrH2 = scrH + scro;
		var leftH = scrHP - scrH2;
		var col = way.find('.item').length;
		var maxCol = way.data('max-element');

		if(leftH < 0){
			if(maxCol>col){
				getNextPage(way);
			}
		}
	}
	
	var rowAdd = $('#housing_estate .info_housing .row.add');
	rowAdd.find('.cell:last').removeClass('desc');
	var h = rowAdd.find('.cell:last').height();
	if(h<=100){
		rowAdd.find('.cell:last').removeClass('desc');
	}
	else {
		rowAdd.find('.cell:last').addClass('desc');
	}

	
	$('#tabs_estate ul li a').click(function(){
		var self = $(this);
		var ul = self.parent().parent();
		var type = self.data('type');
		var id = self.data('id');
		var types = false;
		var estates = false;
		var addParams = false;
		var arrEstates = ["","new","flat","room","country","commercial"];
		
		// Продажа/Аренда
		if(self.parent().parent().parent().is('.types')){
			var block = self.parents('.types');
			var input = block.find('input[type="hidden"]');
			input.val(type);
			types = true;
		}
		
		// Новостройки/Квартиры...
		if(self.parent().parent().parent().is('.estates')){
			var block = self.parents('.estates');
			var input = block.find('input[type="hidden"]');
			input.val(id);
			estates = true;
			$('#tabs_estate .second_inform').find('input[type="hidden"]').attr('disabled',true);
			$('#tabs_estate .second_inform.'+type+'.'+arrEstates[id]).find('input[type="hidden"]').attr('disabled',false);
			if(id==4 || id==5){
				if(id==4){
					var inputSecond = $('#tabs_estate .second_inform.'+type+'.'+arrEstates[id]+' .add_params').find('input[type="hidden"][name="type_country"]');
					if(inputSecond.val()===undefined || inputSecond.val()==''){
						$('#tabs_estate .second_inform.'+type+'.'+arrEstates[id]+' .add_params ul li:first a').click();
					}
				}
				if(id==5){
					var inputSecond = $('#tabs_estate .second_inform.'+type+'.'+arrEstates[id]+' .add_params').find('input[type="hidden"][name="type_commer"]');
					if(inputSecond.val()===undefined || inputSecond.val()==''){
						$('#tabs_estate .second_inform.'+type+'.'+arrEstates[id]+' .add_params ul li:first a').click();
					}
				}
			}
		}
		
		// Офис/Склад...
		if(self.parent().parent().parent().is('.add_params')){
			var block = self.parents('.add_params');
			var input = block.find('input[type="hidden"]');
			block.parent('.second_inform').find('.personal_params').hide();
			input.val(id);
			addParams = true;
			if(id==9){
				block.parent('.second_inform').find('.personal_params').show();
			}
			if(input.attr('name')=='type_country'){
				if(id===undefined){
					block.parent('.second_inform').find('.personal_params').show();
				}
			}
		}
		
		// Автобизнесы/Аптечный...
		if(self.parent().parent().parent().is('.children_params')){
			var block = self.parents('.children_params');
			var input = block.find('input[type="hidden"]');
			input.val(id);
			type_bussines = true;
		}
		
		if(!self.parent().is('.current')){
			ul.find('li').removeClass('current');
			self.parent().addClass('current');
		}
		var typeName = $('#tabs_estate').find('input[name="typeName"]').val();
		var estateName = $('#tabs_estate').find('.estates li.current a').data('type');
		var estateId = $('#tabs_estate').find('.estates li.current a').data('id');
		if(typeName=='rent' && estateName=='new'){
			if(types){
				$('#tabs_estate .estates ul li a[data-type="flat"]').click();
			}
			else {
				$('#tabs_estate .types ul li a[data-type="sell"]').click();
			}
		}
		else {
			if(typeName=='rent' && estateName=='country'){
				$('#tabs_estate .second_inform.'+typeName+'.'+estateName).show().find('.rent_block').removeClass('hide').removeClass('show').addClass('hide');
				$('#tabs_estate .second_inform.'+typeName+'.'+estateName).find('.rent_block').removeClass('show').removeClass('hide').addClass('show');
				$('#tabs_estate .second_inform.'+typeName+'.'+estateName).find('.add_params').removeClass('sell');
			}
			if(typeName=='sell' && estateName=='country'){
				$('#tabs_estate .second_inform.'+typeName+'.'+estateName).show().find('.rent_block').removeClass('hide').removeClass('show').addClass('hide');
				$('#tabs_estate .second_inform.'+typeName+'.'+estateName).find('.add_params').addClass('sell');
			}
			$('#tabs_estate .second_inform').hide();
			$('#tabs_estate .second_inform.'+typeName+'.'+estateName).show();
		}
		var page = window.location;// idParam=5&type=rent&type_commer=5
		if(estateName=='commercial'){
			$('#center #tabs_estate .second_inform.commercial .add_params').find('li .disabled_block').remove();
			var type_commer = $('#tabs_estate').find('input[name="type_commer"]').val();
			page = page+'?idParam='+estateId+'&type='+typeName+'&type_commer='+type_commer;
			if(type_commer==9){
				var type_business = $('#tabs_estate').find('input[name="type_business"]').val();
				page = page+'&type_business='+type_business;
			}
			if(typeName=='rent'){
				$('#center #tabs_estate .second_inform.commercial .add_params').find('li').each(function(){
					var idCom = $(this).find('a').data('id');
					if(idCom==4 || idCom==7){
						$(this).append('<i class="disabled_block"></i>');	
					}
				});
				if(type_commer==4 || type_commer==7){
					$('#center #tabs_estate .add_params ul li').removeClass('current');
					$('#center #tabs_estate .add_params input[type="hidden"][name="type_commer"]').val(3);
					$('#center #tabs_estate .add_params ul li a[data-id="3"]').parent().addClass('current');
				}
			}
			else {
				$('#center #tabs_estate .second_inform.commercial .add_params').find('li .disabled_block').remove();
			}
		}
		else if(estateName=='country'){
			var type_country = $('#tabs_estate').find('input[name="type_country"]').val();
			page = page+'?idParam='+estateId+'&type='+typeName+'&type_country='+type_country;
			if(type_country==1 || type_country==''){
				var block = self.parents('.children_params');
				var input = block.find('input[type="hidden"]');
				input.attr('disabled',false).val(id);
				if(id===undefined){
					$('#tabs_estate .children_params ul li').removeClass('current');
					$('#tabs_estate .children_params ul li a[data-id="5"]').parent().addClass('current').parents('.children_params').find('input[name="type_country"]').val(5);
				}
				var type_country = $('#tabs_estate').find('input[name="type_country"]:last').val();
				page = window.location+'?idParam='+estateId+'&type='+typeName+'&type_country='+type_country;
			}
			else {
				var input = $('#tabs_estate .children_params input[type="hidden"]');
				input.attr('disabled',true);
			}
		}
		else {
			page = page+'?idParam='+estateId+'&type='+typeName
		}
		var xhr;
		if(xhr && xhr.readyState != 4){
			xhr.abort();
		}
		xhr = $('#center .reload_block').load(page+' .upload', function(){
			mainPage();
			tooltipInstall();
			initSliderFunc();
			initiallSelected2('',true);
			initiallChecked();
			searchFunction();
			searchFunction2();
			searchFunctionRailway();
			searchFunctionStation();
			searchFunctionAddress();
			mainSettings(true);
			placement();
			
			$('.reload_block .checkbox_single').click(function(){
				if(!$(this).is('.none')){
					var agency_true = false;
					var checkIds = [];
					if($(this).find('input[type="hidden"]').attr('name')=='s[agency_true]'){
						agency_true = true;
					}
					if($(this).is('.checked')){
						$(this).removeClass('checked');
						$(this).find('input[type="hidden"]:last').attr('disabled',true).removeAttr('value');
						if(agency_true){
							$(this).parents('.table_form').find('.row.agency').removeClass('show').find(':input').attr('disabled',true);
						}
					}
					else {
						$(this).addClass('checked');
						if(agency_true){
							$(this).parents('.table_form').find('.row.agency').addClass('show').find(':input').attr('disabled',false);
						}
						if($(this).find('input[type="hidden"]').attr('name')=='ready' || $(this).find('input[type="hidden"]').attr('name')=='s[ready]'){
							$(this).find('input[type="hidden"]:last').attr('disabled',false).val(5);
						}
						else {
							$(this).find('input[type="hidden"]:last').attr('disabled',false).val(1);
						}
					}
					if($(this).children('input[type="hidden"]').length==0 && $(this).parents('.checkbox_block').length>0){
						var id = $(this).data('id');
						$(this).parents('.checkbox_block').find('.checkbox_single').each(function(){
							if($(this).is('.checked')){
								checkIds.push($(this).data('id'));
							}
						});
						$(this).parents('.checkbox_block').children('input[type="hidden"]').val(checkIds);
					}
				}
			});

			$('#center .reload_block .upload .scrollbar-inner').scrollbar();
			
			var centerParam = [59.939095,30.315868];
			if(coordsPlace){
				centerParam = coordsPlace;
			}
			ymaps.ready(function () {
					var myMap = new ymaps.Map('map', {
						center: centerParam,
						zoom: 15
					}, {
						searchControlProvider: 'yandex#search'
					}),
					myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
						hintContent: 'Метка на карте',
						balloonContent: ''
					}, {
						// Опции.
						// Необходимо указать данный тип макета.
						iconLayout: 'default#image',
						// Своё изображение иконки метки.
						iconImageHref: '/images/neitralIcon.png',
						// Размеры метки.
						iconImageSize: [52, 44],
						// Смещение левого верхнего угла иконки относительно
						// её "ножки" (точки привязки).
						iconImageOffset: [-3, -42]
					});

					myMap.geoObjects.add(myPlacemark);
				});
		});
	});
		
	$('#tabs_estate.favorites ul li a').click(function(){
		var id = $(this).data('id');
		$('.add_block .add_list .result_search tr').not('tr:first').hide();
		if(id==0){
			$('.add_block .add_list .result_search tr').not('tr:first').show();
		}
		$('.add_block .add_list .result_search').find('tr').not('tr:first').each(function(){
			if($(this).data('estate')==id && id!=0){
				$(this).show();
			}
		});
	});

	$('#news_list .scrollbar-inner ul li .name a').off('click');
	$('#news_list .scrollbar-inner ul li .name a').on('click', function(e){
		e.preventDefault();
		var ul = $(this).parents('ul');
		var li = $(this).parents('li');
		if(!li.is('.current')){
			ul.find('>li').removeClass('current');
			li.addClass('current');
			ul.find('>li .hidden_block').slideUp(300);
			li.find('.hidden_block').slideDown(300);
		}
	});
	
	$('#page .select_block.price.ch_price .choosed_block input[type=text]').priceFormat({
		prefix: '',
		centsSeparator: ' ',
		thousandsSeparator: ' '
	});
	
	$('#filter_search form').keydown(function(event){
		if(event.keyCode == 13) {
			event.preventDefault();
			return false;
		}
	});
	
	var priceInput = $('#page .select_block.price.ch_price .choosed_block input[type=text]');
	priceInput.focus(function(event){
		$(this).parents('.select_block').addClass('focus');
	});
	
	priceInput.blur(function(){
		$(this).parents('.select_block').removeClass('focus');
		var estate = $('#filter_search form input[type="hidden"][name="estate"]').parent().find('li.checked a').data('type');
		var mainType = $('#filter_search .simple_search .checkbox_block.types .select_checkbox ul li.checked a').data('type');
		var select = $(this).parents('.select_block');
		var valPrice = select.find('.choosed_block input[type="text"]').val();
		var hiddenPrice = select.find('input[type="hidden"]');
		var namePrice = select.find('.namePrice');
		valPrice = valPrice.replace(' ', "");
		valPrice = valPrice.replace(' ', "");
		valPrice = valPrice.replace(' ', "");
		var type = select.find('ul li a:eq(1)').data('type');
		
		if(select.find('.choosed_block .namePrice').length==0){
			select.find('.choosed_block input[type="text"]').hide();
			select.find('.choosed_block .namePrice').show();
		}
		else {
			select.find('.choosed_block input[type="text"]').hide();
			select.find('.choosed_block .namePrice').show();
		}
		if(select.find('.choosed_block .namePrice').text()==''){
			select.find('.choosed_block input[type="text"]').show();
			select.find('.choosed_block .namePrice').hide();
		}
		
		hiddenPrice.val(valPrice);
						
		if(valPrice!=''){
			var defValue = Math.round((valPrice/1000000)*100)/100;
			defValue = defValue.toString().replace('.', ",");
			namePrice.text(defValue+' млн.');					
			var text = $(this).text();
			var name = select.find('input[type="hidden"]').attr('name');
			if(name=='priceMeterMin' || name=='priceMeterMax' || name=='priceNightMin' || name=='priceNightMax'){
				var defValue = Math.round((valPrice/1000)*100)/100;
				defValue = defValue.toString().replace('.', ",");
				namePrice.text(defValue+' тыс.');
			}
			else {
				if(name!='priceMin' && name!='priceMax'){
					namePrice.text(valPrice);				
				}
			}
			if(type!==undefined){ // min/max
				var nameSelect = 'price';
				nameSelect = select.find('ul li a:last').data('name');
				if(type=='min'){
					// var tags = 'от '+defValue+' млн. ';
					var tags = 'от '+number_format(valPrice,'','',' ');
					length = $('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name_select="'+nameSelect+'"]').length;
					var inputMin = $('#filter_search form input[type="hidden"][name="'+name+'"]:enabled');
					if(length==0){
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name="'+name+'"][data-name_select="'+nameSelect+'"].min').parent().remove();
						var inputVal = inputMin.val();
						var tagMin = inputMin.parent().find('li:eq(1) a').data('tags');
						var exp = inputMin.parent().find('li:eq(1) a').data('explanation');
						var ids = inputMin.parent().find('li:eq(1) a').data('id');
						tagMin = tagMin.replace(ids,inputVal);
						if(exp!==undefined){
							tags = exp+' '+tagMin;
						}
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul').append('<li><a class="min" data-name_select="'+nameSelect+'" data-name="'+name+'" data-type="checkbox_block" href="javascript:void(0)">'+tags+'<span onclick="return delTags(this)" class="close"></span></a></li>');
					}
					else {
						var tagMax = '';
						var numArray = $.inArray(name,arrayParamSelectMin);
						var maxValue = arrayParamSelectMax[numArray];
						var inputMax = $('#filter_search form input[type="hidden"][name="'+maxValue+'"]:enabled');
						if(inputMax.length>1){
							$('#filter_search .advanced_search .inner_block').each(function(){
								if($(this).is(':visible')){
									inputMax = $(this).find('input[type="hidden"][name="'+maxValue+'"]:enabled');
								}
							});
						}
						var inputVal = inputMax.val();
						var ids = 0;
						var exp = '';
						if(inputVal!=0){
							tagMax = ' '+inputMax.parent().find('li:eq(1) a').data('tags');
							exp = inputMax.parent().find('li:eq(1) a').data('explanation');
							ids = inputMax.parent().find('li:eq(1) a').data('id');
							if(maxValue=='priceMax' || maxValue=='priceMin' || maxValue=='priceMeterMin' || maxValue=='priceMeterMax' || maxValue=='priceNightMin' || maxValue=='priceNightMax'){
								tagMax = tagMax.replace(number_format(ids,'','',' '),number_format(inputVal,'','',' '));
							}
							else {
								tagMax = tagMax.replace(ids,inputVal);
							}
							if(exp!==undefined){
								tagMax = exp+' '+tags+' '+tagMax;
							}
							else {
								tagMax = tags+' '+tagMax;
							}
							tags = tagMax;
						}
						else {
							var inputVal = inputMin.val();
							var tagMin = inputMin.parent().find('li:eq(1) a').data('tags');
							var exp = inputMin.parent().find('li:eq(1) a').data('explanation');
							var ids = inputMin.parent().find('li:eq(1) a').data('id');
							tagMin = tagMin.replace(ids,inputVal);
							if(exp!==undefined){
								tags = exp+' '+tagMin;
							}
						}
						// if(tagMax===undefined){
							// tagMax = '';
						// }
						if(maxValue=='priceMax' || maxValue=='priceMin' || maxValue=='priceMeterMin' || maxValue=='priceMeterMax' || maxValue=='priceNightMin' || maxValue=='priceNightMax'){
							if(inputMax.parents('.select_block').find('.namePrice').text()!=''){
								// tagMax = ' до '+inputMax.parents('.select_block').find('.namePrice').text();
								// tagMax = ' до '+number_format(inputMax.val(),'','',' ');
							}
						}
						container = tags+'<span onclick="return delTags(this)" class="close"></span>';
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name_select="'+nameSelect+'"]').html(container).removeClass('min').addClass('min');
					}
				}
				if(type=='max'){
					// var tags = ' до '+defValue+' млн.';
					var tags = ' до '+number_format(valPrice,'','',' ');
					length = $('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name_select="'+nameSelect+'"]').length;
					var inputMax = $('#filter_search form input[type="hidden"][name="'+name+'"]:enabled');
					if(length==0){
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name="'+name+'"][data-name_select="'+nameSelect+'"].max').parent().remove();
						var inputVal = inputMax.val();
						var tagMax = inputMax.parent().find('li:eq(1) a').data('tags');
						var exp = inputMax.parent().find('li:eq(1) a').data('explanation');
						var ids = inputMax.parent().find('li:eq(1) a').data('id');
						tagMax = tagMax.replace(ids,inputVal);
						if(exp!==undefined){
							tags = exp+' '+tagMax;
						}
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul').append('<li><a class="max" data-name_select="'+nameSelect+'" data-name="'+name+'" data-type="checkbox_block" href="javascript:void(0)">'+tags+'<span onclick="return delTags(this)" class="close"></span></a></li>');
					}
					else {
						var tagMin = '';
						var numArray = $.inArray(name,arrayParamSelectMax);
						var minValue = arrayParamSelectMin[numArray];
						var inputMin = $('#filter_search form input[type="hidden"][name="'+minValue+'"]:enabled');
						if(inputMin.length>1){
							$('#filter_search .advanced_search .inner_block').each(function(){
								if($(this).is(':visible')){
									inputMin = $(this).find('input[type="hidden"][name="'+minValue+'"]:enabled');
								}
							});
						}
						var inputVal = inputMin.val();
						var ids = 0;
						var exp = '';
						if(inputVal!=0){
							tagMin = inputMin.parent().find('li:eq(1) a').data('tags')+' ';
							exp = inputMin.parent().find('li:eq(1) a').data('explanation');
							ids = inputMin.parent().find('li:eq(1) a').data('id');
							tagMin = tagMin.replace(ids,inputVal);
							if(exp!==undefined){
								tagMin = exp+' '+tagMin;
							}
						}
						else {
							var inputVal = inputMax.val();
							var tagMax = inputMax.parent().find('li:eq(1) a').data('tags');
							var exp = inputMax.parent().find('li:eq(1) a').data('explanation');
							var ids = inputMax.parent().find('li:eq(1) a').data('id');
							tagMax = tagMax.replace(ids,inputVal);
							if(exp!==undefined){
								tags = exp+' '+tagMax;
							}
						}
						
						if(minValue=='priceMax' || minValue=='priceMin' || minValue=='priceMeterMin' || minValue=='priceMeterMax' || minValue=='priceNightMin' || minValue=='priceNightMax'){
							tagMin = 'от '+number_format(inputVal,'','',' ');
						}
						else {
							tagMin = tagMin.replace(ids,inputVal);
						}
						
						if(minValue=='priceMax' || minValue=='priceMin' || minValue=='priceMeterMin' || minValue=='priceMeterMax' || minValue=='priceNightMin' || minValue=='priceNightMax'){
							if(inputMin.parents('.select_block').find('.namePrice').text()!=''){
								// tagMin = 'от '+inputMin.parents('.select_block').find('.namePrice').text()+' ';
								// tagMin = 'от '+number_format(inputMin.val(),'','',' ')+' ';
							}
						}
						var container = tagMin+tags+'<span onclick="return delTags(this)" class="close"></span>';
						if(inputVal==''){
							container = tags+'<span onclick="return delTags(this)" class="close"></span>';
						}
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name_select="'+nameSelect+'"]').html(container).removeClass('max').addClass('max');
					}
				}
				// $('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name="'+name+'"][data-id="'+id+'"]').parent().remove();
			}
		}
		else {
			var name = select.find('input[type="hidden"]').attr('name');
			var nameSelect = 'price';
			nameSelect = select.find('ul li a:last').data('name');
			if(type=='min'){
				var inputMin = $('#filter_search form input[type="hidden"][name="'+name+'"]:enabled');
				inputMin.parent().find('.namePrice').text('цена От');
				var length = $('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name_select="'+nameSelect+'"]').length;
				if(length>0){
					var tagMax = '';
					var numArray = $.inArray(name,arrayParamSelectMin);
					var maxValue = arrayParamSelectMax[numArray];
					var inputMax = $('#filter_search form input[type="hidden"][name="'+maxValue+'"]:enabled');
					if(inputMax.length>1){
						$('#filter_search .advanced_search .inner_block').each(function(){
							if($(this).is(':visible')){
								inputMax = $(this).find('input[type="hidden"][name="'+maxValue+'"]:enabled');
							}
						});
					}
					var inputVal = inputMax.val();
					if(inputVal!=''){
						var ids = 0;
						var exp = '';
						tagMax = ' '+inputMax.parent().find('li:eq(1) a').data('tags');
						exp = inputMax.parent().find('li:eq(1) a').data('explanation');
						ids = inputMax.parent().find('li:eq(1) a').data('id');
						if(maxValue=='priceMax' || maxValue=='priceMin' || maxValue=='priceMeterMin' || maxValue=='priceMeterMax' || maxValue=='priceNightMin' || maxValue=='priceNightMax'){
							tagMax = tagMax.replace(number_format(ids,'','',' '),number_format(inputVal,'','',' '));
						}
						else {
							tagMax = tagMax.replace(ids,inputVal);
						}
						if(exp!==undefined){
							tagMax = exp+' '+tagMax;
						}
						else {
							tagMax = tagMax;
						}
						tags = tagMax;
							
						if(maxValue=='priceMax' || maxValue=='priceMin' || maxValue=='priceMeterMin' || maxValue=='priceMeterMax' || maxValue=='priceNightMin' || maxValue=='priceNightMax'){
							if(inputMax.parents('.select_block').find('.namePrice').text()!=''){
								// tagMax = ' до '+inputMax.parents('.select_block').find('.namePrice').text();
								// tagMax = ' до '+number_format(inputMax.val(),'','',' ');
							}
						}
						container = tags+'<span onclick="return delTags(this)" class="close"></span>';
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name_select="'+nameSelect+'"]').html(container).removeClass('min').addClass('min');
					}
					else {
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name_select="'+nameSelect+'"]').parent().remove();
					}
				}
			}
			if(type=='max'){
				var inputMax = $('#filter_search form input[type="hidden"][name="'+name+'"]:enabled');
				inputMax.parent().find('.namePrice').text('цена До');
				length = $('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name_select="'+nameSelect+'"]').length;
				var inputMax = $('#filter_search form input[type="hidden"][name="'+name+'"]:enabled');
				if(length>0){
					var tagMin = '';
					var numArray = $.inArray(name,arrayParamSelectMax);
					var maxValue = arrayParamSelectMin[numArray];
					var inputMax = $('#filter_search form input[type="hidden"][name="'+maxValue+'"]:enabled');
					if(inputMax.length>1){
						$('#filter_search .advanced_search .inner_block').each(function(){
							if($(this).is(':visible')){
								inputMax = $(this).find('input[type="hidden"][name="'+maxValue+'"]:enabled');
							}
						});
					}
					var inputVal = inputMax.val();
					if(inputVal!=''){
						var ids = 0;
						var exp = '';
						tagMin = ' '+inputMax.parent().find('li:eq(1) a').data('tags');
						exp = inputMax.parent().find('li:eq(1) a').data('explanation');
						ids = inputMax.parent().find('li:eq(1) a').data('id');
						if(maxValue=='priceMax' || maxValue=='priceMin' || maxValue=='priceMeterMin' || maxValue=='priceMeterMax' || maxValue=='priceNightMin' || maxValue=='priceNightMax'){
							tagMin = tagMin.replace(number_format(ids,'','',' '),number_format(inputVal,'','',' '));
						}
						else {
							tagMin = tagMin.replace(ids,inputVal);
						}
						if(exp!==undefined){
							tagMin = exp+' '+tagMin;
						}
						else {
							tagMin = tagMin;
						}
						tags = tagMin;
							
						if(maxValue=='priceMax' || maxValue=='priceMin' || maxValue=='priceMeterMin' || maxValue=='priceMeterMax' || maxValue=='priceNightMin' || maxValue=='priceNightMax'){
							if(inputMax.parents('.select_block').find('.namePrice').text()!=''){
								// tagMin = ' до '+inputMax.parents('.select_block').find('.namePrice').text();
								// tagMin = ' до '+number_format(inputMax.val(),'','',' ');
							}
						}
						container = tags+'<span onclick="return delTags(this)" class="close"></span>';
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name_select="'+nameSelect+'"]').html(container).removeClass('max').addClass('max');
					}
					else {
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name_select="'+nameSelect+'"]').parent().remove();
					}
				}
			}
		}
	});
	
	priceInput.keyup(function(){
		var p = $(this).parents('.select_block');
		var valPrice = priceInput.val();
		var scrll = p.find('.scrollbar-inner');
		var hid = p.find('input[type="hidden"]');
		valPrice = valPrice.replace(' ', "");
		valPrice = valPrice.replace(' ', "");
		valPrice = valPrice.replace(' ', "");
		scrll.find('li').each(function(){
			var id = $(this).find('a').data('id');
			if(hid.attr('name')=='priceMin'){
				var c = 1000;
				if(valPrice>10000 && valPrice<=100000){
					c = 10000;
				}
				if(valPrice>100000 && valPrice<=4000000){
					c = 100000;
				}
				if(valPrice>4000000 && valPrice<=8000000){
					c = 200000;
				}
				if(valPrice>8000000 && valPrice<=12000000){
					c = 300000;
				}
				if(valPrice>20000000 && valPrice<=30000000){
					c = 1000000;
				}
				if(valPrice>30000000 && valPrice<=50000000){
					c = 2000000;
				}
				if(valPrice>50000000){
					c = 5000000;
				}
				if(id !='' && id>=valPrice-c && id<=valPrice){
					$(this).show();
					p.addClass('open');
				}
				else {
					$(this).hide();
					// p.removeClass('open');
				}
			}
		});
	});
	
	// Открытие меню второго уровня
	$('.nav ul li').hover(function(){
		$(this).addClass('active');
		$(this).find('ul').css('display','block');
	},function(){
		$(this).removeClass('active');
		$(this).find('ul').css('display','none');	
	});
	
	if($('#info_block').find('.block.news').length==0){
		$('#info_block .center_block').addClass('none');
	}
	
	if(!arrLinkPage[1].match(/search/)){
		$('.btns-show .show .item a').removeAttr('onclick').parent().addClass('disabled');
		$('.btns-show .show .item a').removeAttr('onclick').parent().addClass('disabled');
	}
	else {
		var linkAll = arrLinkPage[arrLinkPage.length-1];
		if(linkAll.indexOf('view')==-1){
			$('.btns-show .show .item.list').addClass('current');
			$('.btns-show .show .item.list').addClass('current');
		}
	}	
	
	$('.checkbox_single').click(function(){
		if(!$(this).is('.none')){
			var agency_true = false;
			var tags = $(this).data('tag');
			var checkIds = [];
			var name = $(this).find('input[type="hidden"]').attr('name');
			var estate = $('#filter_search form input[type="hidden"][name="estate"]').parent().find('li.checked a').data('type');
			var mainType = $('#filter_search .simple_search .checkbox_block.types .select_checkbox ul li.checked a').data('type');
			if($(this).find('input[type="hidden"]').attr('name')=='s[agency_true]'){
				agency_true = true;
			}
			if($(this).is('.checked')){
				$(this).removeClass('checked');
				$(this).find('input[type="hidden"]:last').attr('disabled',true).removeAttr('value');
				if(agency_true){
					$(this).parents('.table_form').find('.row.agency').removeClass('show').find(':input').attr('disabled',true);
				}
				if($(this).parents('#filter_search').length>0){
					length = $('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name_select="'+name+'"]').length;
					$('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name="'+name+'"][data-name_select="'+name+'"]').parent().remove();
				}
				if(name=='agree'){
					$(this).parents('.table_form').find('input[type="submit"]').attr('disabled',true).parents('.btn').addClass('disabled');
					$(this).parents('form').find('input[type="submit"]').attr('disabled',true).parents('label.btn').addClass('disabled');
				}
			}
			else {
				$(this).addClass('checked');
				if(agency_true){
					$(this).parents('.table_form').find('.row.agency').addClass('show').find(':input').attr('disabled',false);
				}
				if($(this).find('input[type="hidden"]').attr('name')=='ready' || $(this).find('input[type="hidden"]').attr('name')=='s[ready]'){
					$(this).find('input[type="hidden"]:last').attr('disabled',false).val(5);
				}
				else {
					$(this).find('input[type="hidden"]:last').attr('disabled',false).val(1);
				}
				if(tags===undefined){
					tags = $(this).find('.label').text();
				}
				if($(this).parents('.reload_block').length==0){
					if($(this).parents('#filter_search').length>0){
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul').append('<li><a class="min" data-single="true" data-name_select="'+name+'" data-name="'+name+'" data-type="checkbox_block" href="javascript:void(0)">'+tags+'<span onclick="return delTags(this)" class="close"></span></a></li>');
					}
				}
				if(name=='agree'){
					$(this).parents('.table_form').find('input[type="submit"]').attr('disabled',false).parents('.btn').removeClass('disabled');
					$(this).parents('form').find('input[type="submit"]').attr('disabled',false).parents('label.btn').removeClass('disabled');
				}
			}
			if($(this).children('input[type="hidden"]').length==0 && $(this).parents('.checkbox_block').length>0){
				var id = $(this).data('id');
				$(this).parents('.checkbox_block').find('.checkbox_single').each(function(){
					if($(this).is('.checked')){
						checkIds.push($(this).data('id'));
					}
				});
				$(this).parents('.checkbox_block').children('input[type="hidden"]').val(checkIds);
			}
			if(arrLinkPage){
				loadRequestUnreload();
			}
		}
	});

	mainSettings();
	
	if($('#result_search.developer_block .text_full .text').length>0){
		$('#result_search.developer_block .text_full .text').each(function(){
			var h = $(this).height();
			if(h<156){
				$(this).next('.more_read').remove();
			}
			$(this).height(156);
		});
	}
	
	if($('#result_search.company_block  .text_full .text').length>0){
		$('#result_search.company_block  .text_full .text').each(function(){
			var h = $(this).height();
			if(h<156){
				$(this).next('.more_read').remove();
			}
			$(this).height(156);
		});
	}
	
	if($('#housing_estate .text_info .text').length>0){
		var h = $('#housing_estate .text_info .text').height();
		if(h<156){
			$('#housing_estate .text_info .more_read').remove();
		}
		$('#housing_estate .text_info .text').height(156);
	}
	
	$('#filter_search form').submit(function(){
		var estate = $('#filter_search form input[type="hidden"][name="estate"]').parent().find('li.checked a').data('type');
		var mainType = $('#filter_search .simple_search .checkbox_block.types .select_checkbox ul li.checked a').data('type');
		// var form = $(this);
		// $('#filter_search .simple_search .inner_block .change_block_filter input[type="hidden"],input[type="text"]').attr('disabled',true);
		$('#filter_search .simple_search .inner_block .change_block_filter').each(function(){
			if(!$(this).is('.'+estate+'.'+mainType)){
				$(this).find('input[type="hidden"]').attr('disabled',true);
			}
			else {
				$(this).find('input[type="hidden"]').each(function(){
					if($(this).val()==''){
						$(this).attr('disabled',true);
					}
					else {
						$(this).attr('disabled',false);
					}
				});
			}
		});

		// $('#filter_search .advanced_search .inner_block .row.'+estate+'.'+mainType+' input[type="hidden"]').each(function(){
			// if($(this).val()==''){
				// $(this).attr('disabled',true);
			// }
			// else {
				// $(this).attr('disabled',false);
			// }
		// });
		$('#filter_search .advanced_search .inner_block').each(function(){
			var typeCommer = '';
			if(estate=='commercial'){
				typeCommer = $('input[type="hidden"][name="type_commer"]').val();
			}
			var typeCountry = '';
			if(estate=='country'){
				typeCountry = $('input[type="hidden"][name="type_country"]').val();
			}
			if($(this).is('.'+estate)){
				$(this).find('.row').each(function(){
					if(typeCommer!=''){
						if($(this).is('.'+mainType) && $(this).is('.type_commer_'+typeCommer)){
							$(this).find('input[type="hidden"]').each(function(){
								if($(this).val()==''){
									$(this).attr('disabled',true);
								}
								else {
									$(this).attr('disabled',false);
								}
							});
						}
						else {
							$(this).find('input[type="hidden"]').attr('disabled',true);
						}
					}
					else if(typeCountry!=''){
						if($(this).is('.'+mainType) && $(this).is('.type_country_'+typeCountry)){
							$(this).find('input[type="hidden"]').each(function(){
								if($(this).val()==''){
									$(this).attr('disabled',true);
								}
								else {
									$(this).attr('disabled',false);
								}
							});
						}
						else {
							$(this).find('input[type="hidden"]').attr('disabled',true);
						}
					}
					else {
						if($(this).is('.'+mainType)){
							$(this).find('input[type="hidden"]').each(function(){
								if($(this).val()==''){
									$(this).attr('disabled',true);
								}
								else {
									$(this).attr('disabled',false);
								}
							});
						}
						else {
							$(this).find('input[type="hidden"]').attr('disabled',true);
						}
					}
				});
			}
			else {
				$(this).find('input[type="hidden"]').attr('disabled',true);
			}
		});
		
		$('#filter_search .location_change .filter_local input[type="hidden"]').each(function(){
			if($(this).val()==''){
				$(this).attr('disabled',true);
			}
			else {
				$(this).attr('disabled',false);
			}
		});
		
		$('#filter_search .simple_search .price_block').each(function(){
			var cession = $('#filter_search .change_block_filter.new input[type="hidden"][name="cession"]');
			var valCession = cession.val();
			var estate2 = estate;
			if(valCession==1 && cession.is(':enabled')){
				estate2 = 'cession';
			}
			if(!$(this).is('.'+estate2+'.'+mainType)){
				$(this).find('input[type="hidden"]').attr('disabled',true);
			}
			else {
				if(estate2=='commercial'){
					var ch = $('.price_block.commercial.'+mainType).find('input[type="hidden"][name="priceAll"]').val();
					var typeComm = $('.change_block_filter.commercial.'+mainType).find('input[name="type_commer"]');
					var typeCommValue = typeComm.val();
					
					$('.price_block.commercial.'+mainType).find('.select_block.price.all').find('input[type="hidden"]').attr('disabled',true);
					$('.price_block.commercial.'+mainType).find('.prices_choose').find('input[type="hidden"]').attr('disabled',true);
					$('.price_block.commercial.'+mainType+'.type_commer_'+typeCommValue+' .prices_choose.'+ch).find('input[type="hidden"]').attr('disabled',false);
					$('.price_block.commercial.'+mainType+'.type_commer_'+typeCommValue+' .select_block.price.all').find('input[type="hidden"]').attr('disabled',false);
					var priceAll = ch;
					var priceAll2 = 'meters';
					if(priceAll=='meters'){
						priceAll2 = 'all';
					}
					$('.price_block.commercial.'+mainType).find('.prices_choose').find('input[type="hidden"]').each(function(){
						$(this).attr('disabled',true);
					});
					$('.price_block.commercial.'+mainType+'.type_commer_'+typeCommValue+' .prices_choose.'+ch).find('input[type="hidden"]').each(function(){
						if($(this).val()==''){
							$(this).attr('disabled',true);
						}
						else {
							$(this).attr('disabled',false);
						}
					});
					
					$('input[type="text"][name="power_max"]').each(function(){
						if($(this).val()==''){
							$(this).attr('disabled',true);
						}
						else {
							$(this).attr('disabled',false);
						}
					});
					$('input[type="text"][name="name_villiage"]').each(function(){
						if($(this).val()==''){
							$(this).attr('disabled',true);
						}
						else {
							$(this).attr('disabled',false);
						}
					});
				}
				else {
					var priceAll = $(this).find('input[name="priceAll"]').val();
					var priceAll2 = 'meters';
					if(priceAll=='meters'){
						priceAll2 = 'all';
					}
					$(this).find('.prices_choose.'+priceAll2+' input[type="hidden"]').each(function(){
						$(this).attr('disabled',true);
					});
					$(this).find('.prices_choose.'+priceAll+' input[type="hidden"]').each(function(){
						if($(this).val()==''){
							$(this).attr('disabled',true);
						}
						else {
							$(this).attr('disabled',false);
						}
					});
					$('input[type="text"][name="power_max"]').each(function(){
						if($(this).val()==''){
							$(this).attr('disabled',true);
						}
						else {
							$(this).attr('disabled',false);
						}
					});
				}
			}
		});
		
		if(estate!='new' && estate!='cession'){
			$('#just_build').hide().find('input[type="hidden"]').attr('disabled',true);
		}
		if(estate!='country'){
			$('#just_country').hide().find('input[type="hidden"]').attr('disabled',true);
		}
		if($('.btns-show .show .item.current').length>0){
			$('input[name="view"][type="hidden"]').remove();
			if($('.btns-show .show .item.current').is('.table')){
				$(this).append('<input type="hidden" name="view" value="table">');
			}
			if($('.btns-show .show .item.current').is('.map')){
				$(this).append('<input type="hidden" name="view" value="map">');
			}
		}
		
	});
	
	$('.advance_open a').on('click', function(){
		if($(this).parent().is('.open')){
			$(this).parent().removeClass('open');
			$('.advanced_search').slideUp(300);
			// setTimeout(function() {
				// disabledInput();
			// }, 300);
		}
		else {
			$(this).parent().addClass('open');
			typeCommerce();
			typeCountry();
			var typeProc = $('#filter_search input[name="type"]');
			var type = typeProc.parent().find('.select_checkbox li.checked a').data("type");
			if(typeProc.val()=='sell'){// Покупка
				$('#filter_search .advanced_search .inner_block .separate').hide();
			}
			else { // Аренда
				$('#filter_search .advanced_search .inner_block .separate').show();
			}
			$('#filter_search .advanced_search .inner_block .row').hide();
			$('#filter_search .advanced_search .inner_block .row.'+type).show();
			// setTimeout(function() {
				// disabledInput();
			// }, 300);
			$('.advanced_search').slideDown(300);
			var estate = $('#filter_search .simple_search .checkbox_block.estate .select_checkbox ul li.checked a').data('type');
			var mainType = $('#filter_search .simple_search .checkbox_block.types .select_checkbox ul li.checked a').data('type');
			// if(estate=='commercial'){
				// var type_commer = $('#filter_search .simple_search .inner_block .change_block_filter.'+estate+'.'+mainType+' input[name="type_commer"]');
				// var v_com = type_commer.val();
				// $('#filter_search .advanced_search .inner_block .row.'+mainType+' .comm_show_param').addClass('none').append('<i class="not_active"></i>').find('input[type="hidden"]').attr('disabled',true).removeAttr('value');
				// $('#filter_search .advanced_search .inner_block .row.'+mainType+' .comm_show_param.type_commer_'+v_com).each(function(){
					// $(this).removeClass('none').find('.not_active').remove();
					// $(this).find('input[type="hidden"]').removeAttr('disabled');
				// });
			// }
		}
	});	
	
	loadAvatarUser();
	initiallSelected();
	initiallSelected2();
	initiallChecked();
		
	disabledInput();
	
	searchFunction();
	searchFunction2();
	searchFunctionRailway();
	searchFunctionStation();
	searchFunctionAddress();
	
	$(window).scroll(function(){
		if ($(this).scrollTop() > 0){
			$('#scroller .b-top-but').fadeIn();
			$('#scroller .b-bot-but').fadeOut();
		}
		else {
			$('#scroller .b-top-but').fadeOut();
		}
	});
	
	$('#scroller .b-top-but').click(function(){
		var s = $(window).scrollTop();
		setTimeout(function() {
			$('#scroller .b-bot-but').fadeIn().attr('data-scroll',s);
		}, 400);
		$('body,html').animate({scrollTop: 0}, 400); return false;
	});
	
	$('#scroller .b-bot-but').click(function(){
		var s = $(this).attr('data-scroll');
		$('#scroller .b-bot-but').removeAttr('data-scroll');
		$('#scroller .b-bot-but').fadeOut();
		$('#scroller .b-top-but').fadeIn();
		$('body,html').animate({scrollTop: s}, 400); return false;
	});
	
	$('#housing_estate .text_info.icons .list_flats .open a').click(function(){
		var parent = $(this).parent();
		var hidden = parent.parent().next();
		if(parent.is('.plus')){
			if(hidden.is('ul.hidden')){
				hidden.slideDown(300);
				setTimeout(function() {
					hidden.addClass('show');
				}, 300);
				parent.removeClass('plus').addClass('minus');
			}
		}
		else {
			if(hidden.is('ul.hidden')){
				hidden.slideUp(300);
				setTimeout(function() {
					hidden.removeClass('show');
				}, 300);
				parent.removeClass('minus').addClass('plus');
			}
		}
	});
	
	
	// $('.scrollbar-inner').each(function(){
		// if(!$(this).parent().find('input[type="hidden"]').is(':disabled')){
			// $(this).scrollbar();
		// }
	// });
	// $('body').find('>.scrollbar-inner').scrollbar();
	
	$('.select_block_hover').hover(function(){
		$(this).addClass('open');
	},function(){
		$(this).removeClass('open');		
	});
	
	$('.select_block_hover ul li a').click(function(){
		var estate = $('#filter_search form input[type="hidden"][name="estate"]').parent().find('li.checked a').data('type');
		var mainType = $('#filter_search .simple_search .checkbox_block.types .select_checkbox ul li.checked a').data('type');
		var id = $(this).data('id');
		var text = $(this).text();
		var select = $(this).parents('.select_block_hover');
		var name = select.find('input[type="hidden"]').attr('name');
		$(this).parent().parent().find('li.current').removeClass('current');
		$(this).parent().addClass('current');
		select.find('input[type="hidden"]').attr('disabled',false).val(id);
		select.find('.choosed_block').text(text);
		select.removeClass('open');
	});
	
	var rememberPosition;
	$(window).bind("scroll",function(){
		rememberPosition = $(this).scrollTop();
	});
	
	var xhr5;
	$('.clear_filter').click(function(){
		jConfirm('Вы уверены, что хотите очистить параметры поиска?', 'Очистка параметров поиска',function(r){
			if (r === true) {
				$('#filter_search .selected_params ul').find('li a').each(function(){
					var tag = $(this);
					var name = tag.data('name');
					var type = tag.data('type');
					var id = tag.data('id');
					var nameClass = tag.attr('class');
					clearFilter(tag,name,type,id,nameClass);
				});
				$('#filter_search .location_change .filter_local .hidden_block').hide();
				xhr5 = $.ajax({
					type: "POST",
					url: '/include/handler.php',
					data: "clear_filter=true",
					dataType: "json",
					success: function (data, textStatus) {
						var json = eval(data);
						if(arrLinkPage){
							loadRequestUnreload();
						}
					}
				});
				
			}
		});
	});
	
	$('.location_change .filter_local > a').click(function(){
		if($(this).parent().find('.hidden_block').is(':visible')){
			list_areas_close(false,this);
		}
		else {
			$('#filter_search .location_change .filter_local .hidden_block').hide();
			$('body').height('');
			var hidden = $(this).parent().find('.hidden_block');
			hidden.css({ position: "absolute", visibility: "hidden", display: "block", left: "0" });
			var width = hidden.width();
			var height = hidden.height();
			hidden.css({ position: "", visibility: "", display: "", left: "auto" });
			var pageHeight = $('#page').height();
			var offset = $(this).offset();
			var posTop = offset.top;
			if(pageHeight<height+posTop){
				$('body').height(height+posTop);
			}
			var hide = $(this).parent().parent().find('input[type="hidden"]');
			if(hide.attr('name')=='build' || hide.attr('name')=='developer'){
				if(hidden.is(':hidden')){
					hidden.css({"margin-left":-(width/2)+9-100}).show();
				}
				else {
					hidden.css({"margin-left":-(width/2)+9-100}).hide();
				}
			}
			else {
				if(hidden.is(':hidden')){
					hidden.css({"margin-left":-(width/2)+9-40}).show();
				}
				else {
					hidden.css({"margin-left":-(width/2)+9-40}).hide();
				}
			}
				
			// $(document).unbind("click");
			$(document).bind('click', function(e){
				var $clicked = $(e.target);
				if($clicked.parents(".filter_local").length==0){
					closeLocDial2();				
				}
			});

		}
	});
	
	/*
	*  Перещёлкивание поиска
	*/
	$('.select_block_hover ul li a').click(function(){
		var checkbox = $('#filter_search .simple_search .checkbox_block.estate');
		var type_check = checkbox.find('.select_checkbox');
		var id = $(this).data('id');
		var ids = 1;
		if(id==1){
			ids = 1;
		}
		if(id==2){
			ids = 6;
		}
		if(id==3){
			ids = 2;
		}
		if(id==4){
			ids = 2;
		}
		if(id==5){
			ids = 3;
		}
		if(id==6){
			ids = 3;
		}
		if(id==7){
			ids = 5;
		}
		if(id==8){
			ids = 5;
		}
		var rentArray = [4,6,8];
		var type = $(this).data('estate');
		if(type!==undefined){
			/*
			*  Изменение блока advanced_search
			*/
			if($.inArray(type,typeEstate)!=-1){
				var mainType = $('#filter_search .simple_search .checkbox_block.types .select_checkbox ul li.checked a').data('type');
				var parentAdvSearch = $('#filter_search .advanced_search');
				var parentSimSearch = $('#filter_search .simple_search .inner_block');
				
				parentSimSearch.find('.row').not('.row:first').hide();
				parentSimSearch.find('.row.'+type).show();
				if(type!='new' && type!='cession'){
					$('#filter_search .row .checkbox_block.types li a[data-id="rent"]').parent().removeClass('none').find('.not_active').remove();
				}
				else {
					var rent = $('#filter_search .row .checkbox_block.types li a[data-id="rent"]');
					var sell = $('#filter_search .row .checkbox_block.types li a[data-id="sell"]');
					if(rent.parent().is('.checked')){
						setTimeout(function() {
							sell.click();
						}, 10);
					}
					rent.parent().addClass('none').append('<i class="not_active"></i>');
				}
				if(type=='commercial' || type=='country'){
					$('#filter_search .simple_search .inner_block .row.new.flat.room.country.commercial.cession.margin').css("margin-left","320px");
					$('#filter_search .advance_open').css("margin-left","45px");
				}
				else {
					$('#filter_search .simple_search .inner_block .row.new.flat.room.country.commercial.cession.margin').css("margin-left","");
					$('#filter_search .advance_open').css("margin-left","");
				}
				parentSimSearch.find('.change_block_filter').hide();
				parentSimSearch.find('.change_block_filter.'+type+'.'+mainType).show();
				
				parentAdvSearch.find('.inner_block').hide();
				parentAdvSearch.find('.inner_block.'+type).show();
				
				$('#filter_search .simple_search .price_block').hide().find('input[type="hidden"]').attr('disabled',true);
				$('#filter_search .simple_search .price_block.'+type+'.'+mainType).show().find('input[type="hidden"]').attr('disabled',false);
				var priceAll = $('#filter_search .simple_search .price_block.'+type+'.'+mainType+' input[type="hidden"][name="priceAll"]').val();
				if(type=='new' && mainType=='rent'){
					mainType = 'sell';
				}
				if(type=='cession' && mainType=='rent'){
					mainType = 'sell';
				}
				$('#filter_search .simple_search .price_block.'+type+'.'+mainType+' .prices_choose').hide();
				$('#filter_search .simple_search .price_block.'+type+'.'+mainType+' .prices_choose.'+priceAll).show();
				$('#filter_search .selected_params').hide();
				$('#filter_search .selected_params.'+type+'.'+mainType).show();
			}
			if($.inArray(type,typeFind)!=-1){
				$('#filter_search .advanced_search .inner_block .row').hide();
				$('#filter_search .advanced_search .inner_block .row.'+type).show();
				var type2 = $('#filter_search .simple_search .checkbox_block.estate .select_checkbox ul li.checked a').data('type');
				if(type2=='commercial' || type2=='country'){
					$('#filter_search .simple_search .inner_block .row.new.flat.room.country.commercial.cession.margin').css("margin-left","320px");
					$('#filter_search .advance_open').css("margin-left","45px");
				}
				else {
					$('#filter_search .simple_search .inner_block .row.new.flat.room.country.commercial.cession.margin').css("margin-left","");
					$('#filter_search .advance_open').css("margin-left","");
				}
				$('#filter_search .change_block_filter').hide();
				$('#filter_search .change_block_filter.'+type+'.'+type2).show();
				if($.inArray(type,typeFind)==0){
					$('#filter_search .advanced_search .inner_block .separate').hide();
				}
				else {
					$('#filter_search .advanced_search .inner_block .separate').show();
				}
				$('#filter_search .simple_search .price_block').hide().find('input[type="hidden"]').attr('disabled',true);
				$('#filter_search .simple_search .price_block.'+type2+'.'+type).show().find('input[type="hidden"]').attr('disabled',false);
				var priceAll = $('#filter_search .simple_search .price_block.'+type+'.'+type2+' input[type="hidden"][name="priceAll"]').val();
				$('#filter_search .simple_search .price_block.'+type+'.'+type2+' .prices_choose').hide();
				$('#filter_search .simple_search .price_block.'+type+'.'+type2+' .prices_choose.'+priceAll).show();
				$('#filter_search .selected_params').hide();
				$('#filter_search .selected_params.'+type2+'.'+type).show();
			}
			disabledInput();
		}
		if(!checkbox.find('li a[data-id="'+ids+'"]').parent().is('.checked')){
			checkbox.find('li').removeClass('checked');
			checkbox.find('input[type="hidden"]').val(ids);
			checkbox.find('li a[data-id="'+ids+'"]').parent().addClass('checked');
		}
		var rent = $('#filter_search .row .checkbox_block.types li a[data-id="rent"]');
		var sell = $('#filter_search .row .checkbox_block.types li a[data-id="sell"]');
		if($.inArray(id,rentArray)!=-1){
			rent.click();
		}
		else {
			sell.click();
		}
		if(arrLinkPage){
			loadRequestUnreload();
		}
	});
	
	list_areas();
	metro_stations();
	stickMainNav();
	
	$('.more_read a').click(function(){
		if(!$(this).parent().parent().is('.open')){
			$(this).parent().parent().addClass('open');
			$(this).text('Скрыть описание');
		}
		else {
			$(this).parent().parent().removeClass('open');
			$(this).text('Читать подробно');
		}
	});

	// Инициализация prettyPhoto
	if($("a[rel^='prettyPhoto']").length > 0) {
		$("a[rel^='prettyPhoto']").prettyPhoto({
			theme: 'dark_rounded',
			overlay_gallery: false
		});
	}
	
	var end = new Date();
	console.log('Скорость ' + (end.getTime()-start.getTime()) + ' мс'); 
	console.timeEnd('trigger');
	
	var estate = $('#filter_search .simple_search .checkbox_block.estate .select_checkbox ul li.checked a').data('type');
	var mainType = $('#filter_search .simple_search .checkbox_block.types .select_checkbox ul li.checked a').data('type');
	// if(estate=='commercial'){
		// var type_commer = $('#filter_search .simple_search .inner_block .change_block_filter.'+estate+'.'+mainType+' input[name="type_commer"]');
		// var v_com = type_commer.val();
		// $('#filter_search .advanced_search .inner_block .row.'+mainType+' .comm_show_param').addClass('none').append('<i class="not_active"></i>').find('input[type="hidden"]').attr('disabled',true).removeAttr('value');
		// $('#filter_search .advanced_search .inner_block .row.'+mainType+' .comm_show_param.type_commer_'+v_com).each(function(){
			// $(this).removeClass('none').find('.not_active').remove();
			// $(this).find('input[type="hidden"]').removeAttr('disabled');
		// });
	// }
	
	setTimeout(function() {
		typeCommerce();
		typeCountry();
	}, 0);
	
	setTimeout(function() {
		// if(arrLinkPage){
			// var iCityText = $('#filter_search .location_change .filter_local.city .hidden_block ul li.current a').text();
			// var fl = $('#filter_search .location_change .filter_local.city .hidden_block ul li.current a').parents('filter_local');
			// fl.find('>a span.text').text(iCityText);
		// }
		// else {
			/*FOLLOW*/
			// var iCity = $('#filter_search .location_change .filter_local.city .hidden_block ul li.current a').data('id');
			// $('#filter_search .location_change .filter_local.city .hidden_block ul li.current').removeClass('current');
			// $('#filter_search .location_change .filter_local.city .hidden_block ul li a[data-id="'+iCity+'"]').click();
		// }
	}, 0);
	
	if($("#owl-demo").length>0){
		$("#owl-demo").owlCarousel({
			autoPlay: 6000, //Set AutoPlay to 3 seconds
			items : 3,
			lazyLoad : true,
			navigation : true,
		});
		// var owl = $("#owl-demo");
	}
});

// (function(){(function a(){try {(function b(i) {if((''+(i/i)).length !== 1 || i % 20 === 0) {(function(){}).constructor('debugger')();} else {debugger;}b(++i);}(0))} catch(e) {setTimeout(a, 5000)}})()})();

$(window).resize(function(){
	mainPage();
	if($(window).width()+16>=1000){
		if($('#page').is('.open_nav')){
			$('#page').removeClass('open_nav');
			$('#header .mobile_nav .left_nav .open_menu a').removeClass('current');
			$('#header .mobile_nav .left_nav .open_menu a:first').addClass('current');
			$('#plumb_mobile').remove();
		}
	}
});