var xhrLoad;
var thisWork = 1;
var thisPageNum = 2;
var openNewsLoadPage = false;
function tooltipInstall(){var tooltip = $('[data-hover="tooltip"]');var count = tooltip.length;tooltip.each(function(){var text = $(this).attr('title');$(this).removeAttr('title').attr('data-title',text);});if(count>0){xOffset = 50;yOffset = -30;tooltip.hover(function(e){$('#tooltip').remove();var pos = $(this).data('pos');var width = $(this).width();var height = $(this).height();var offset = $(this).offset();var left = offset.left;var top = offset.top;var text = $(this).data('title');var wHalf = (width/2);var l = (e.pageX - xOffset)+wHalf-3;var diffLeft = l - left;var t = e.pageY + yOffset;var diffTop = t - top;if(pos=='left'){
	var className = '';
	var finalTop = top-(hTool/2)+(height/2)+5;
	if($(this).data('much')!==undefined){
		className = ' much';
		finalTop = top;
	}
	$('body').append('<div id="tooltip"><div class="triangle-right"></div><div class="container'+className+'">'+text+'</div></div>');var hTool = $('#tooltip').height();
	if($(this).data('much')!==undefined){finalTop = finalTop-hTool+(height+11);}else{finalTop = finalTop;}
	var toolWidth = $('#tooltip').width();$('#tooltip').css({"left":left-toolWidth-9,"top":finalTop});
}else if(pos=='top'){$('body').append('<div id="tooltip"><div class="triangle-bottom"></div><div class="container">'+text+'</div></div>');var hTool = $('#tooltip').height();var toolWidth = $('#tooltip').width();$('#tooltip').css({"left":(l - diffLeft + wHalf - (toolWidth/2)),"top":top-38});}else if(pos=='bottom'){$('body').append('<div id="tooltip"><div class="triangle-top"></div><div class="container">'+text+'</div></div>');var hTool = $('#tooltip').height();var toolWidth = $('#tooltip').width();$('#tooltip').css({"left":(l - diffLeft + wHalf - (toolWidth/2)),"top":top+(height+9)});}else{$('body').append('<div id="tooltip"><div class="triangle-left"></div><div class="container">'+text+'</div></div>');var hTool = $('#tooltip').height();$('#tooltip').css({"left":left+width+3,"top":top-(hTool/2)+(height/2)});}},function(){
	$('#tooltip').remove();
});}}
/*
*  Массивы для зависимых друг от друга
*  значений селекта
*/
var arrayParamSelectMin = ["priceMin","priceMeterMin","priceFlatMin","priceMeterFlatMin","priceRoomMin","priceMeterRoomMin","priceCountryMin","priceMeterCountryMin","priceCommercialMin","priceMeterCommercialMin","squareFullMin","squareLiveMin","squareKitchenMin","floorMin","floorsMin","squareFullFlatMin","squareLiveFlatMin","squareKitchenFlatMin","floorFlatMin","floorsFlatMin","squareSellFullFlatMin","squareSellLiveFlatMin","squareSellKitchenFlatMin","floorSellFlatMin","floorsSellFlatMin","floorSellRoomMin","squareFullFlatRoomMin","squareKitchenRoomMin","squareFullSellRoomMin","squareFullRentRoomMin","squareFullCountryMin","squareLandMin","squareRoomsMin","roomsMin","squareRoomMin","deliveryMin","priceNightMin","squarePlaceMin","squareAreaMin"];
var arrayParamSelectMax = ["priceMax","priceMeterMax","priceFlatMax","priceMeterFlatMax","priceRoomMax","priceMeterRoomMax","priceCountryMax","priceMeterCountryMax","priceCommercialMax","priceMeterCommercialMax","squareFullMax","squareLiveMax","squareKitchenMax","floorMax","floorsMax","squareFullFlatMax","squareLiveFlatMax","squareKitchenFlatMax","floorFlatMax","floorsFlatMax","squareSellFullFlatMax","squareSellLiveFlatMax","squareSellKitchenFlatMax","floorSellFlatMax","floorsSellFlatMax","floorSellRoomMax","squareFullFlatRoomMax","squareKitchenRoomMax","squareFullSellRoomMax","squareFullRentRoomMax","squareFullCountryMax","squareLandMax","squareRoomsMax","roomsMax","squareRoomMax","deliveryMax","priceNightMax","squarePlaceMax","squareAreaMax"];
var namePage = window.location.toString();
var arrPage = namePage.split("https://");
// var arrPage = namePage.split("http://");
var partLink = arrPage[1];
var arrLinkPage = partLink.split("/");

function makeExpandingArea(container) {
 var area = container.querySelector('textarea');
 var span = container.querySelector('span');
 if (area.addEventListener) {
   area.addEventListener('input', function() {
     span.textContent = area.value;
   }, false);
   span.textContent = area.value;
 } else if (area.attachEvent) {
   // IE8 compatibility
   area.attachEvent('onpropertychange', function() {
     span.innerText = area.value;
   });
   span.innerText = area.value;
 }
 // Enable extra CSS
 container.className += ' active';
}

function scrollLoad(){
	if($('body').find('div[data-max-element]').length>0){
		var scrH = $(window).height();
		var grand = $('body').find('div[data-max-element]').attr('id');
		var way = '#'+grand;
		var way = $(way);
		var scrHP = way.height();
		var max = way.data("max-element");
		$(window).bind("scroll",function(){
			var scrHP = way.height()+250;
			var col = way.find('.item').length;
			var maxCol = max;
			var scro = $(this).scrollTop();
			var scrH2 = 0;
			scrH2 = scrH + scro;
			var leftH = scrHP - scrH2;
			if(leftH < 0){
				if(maxCol>col){
					getNextPage(way);
				}
			}
		});
	}
}

function getNextPage(way){
	if(thisWork == 1){
		thisWork = 0;
		$("#upBlock").css('display','block');
		var name = arrLinkPage[1];
		var form = $('#result_search');
		var adds = form.serialize();
		form.append('<i class="loaderPageList"></i>');
		var page = "nextPageLoad="+thisPageNum+"&linkPage="+name;
		loadRequestUnreload(page);
   }
}

function ipoCalc(price,contr,time){
	var p = 11.5;
	var d = ((price-contr)*p/12/100)/(1-Math.pow((1+p/12/100),(1-time)));
	d = Math.round(d);
	var total_count = $('#center .calculation_block .total_count');
	$('body').append('<input id="total_count" type="hidden" value="'+d+'">');
	$('input[type="hidden"]#total_count').priceFormat({
		prefix: '',
		centsSeparator: ' ',
		thousandsSeparator: ' '
	});
	var priceForm = $('input[type="hidden"]#total_count').val();
	$('input[type="hidden"]#total_count').remove();
	total_count.text(priceForm+' р.');
}

function initSliderFunc(obj){
	$(".slider").each(function(){
		var v = $(this).data('value');
		$(this).slider({
			value: $(this).data('default'),
			min: $(this).data('min'),
			max: $(this).data('max'),
			step: $(this).data('step'),
			slide: function(event, ui) {
				$(this).parent().find('input[type="hidden"]').val(ui.value);
				$(this).parent().find('.input_text .value').text(ui.value);
				
				$(this).parent().find('input[type="hidden"].val').priceFormat({
					prefix: '',
					centsSeparator: ' ',
					thousandsSeparator: ' '
				});
				var price = $(this).parent().find('input[type="hidden"].val').val();
				$(this).parent().find('.input_text .value').text(price);
				
				var p = $('#ipoteka input[type="hidden"][name="s[price]"]').val()
				var c = $('#ipoteka input[type="hidden"][name="s[contribution]"]').val()
				var cr = $('#ipoteka input[type="hidden"][name="s[credit]"]').val()
				ipoCalc(p,c,cr);
			}
		});
		$(this).parent().find('input[type="hidden"]').val($(this).slider("value"));
		$(this).parent().find('.input_text .value').text($(this).slider("value"));
		$(this).parent().find('input[type="hidden"].val').priceFormat({
			prefix: '',
			centsSeparator: ' ',
			thousandsSeparator: ' '
		});
		var price = $(this).parent().find('input[type="hidden"].val').val();
		$(this).parent().find('.input_text .value').text(price);
		
	});
	var p = $('#ipoteka input[type="hidden"][name="s[price]"]').val()
	var c = $('#ipoteka input[type="hidden"][name="s[contribution]"]').val()
	var cr = $('#ipoteka input[type="hidden"][name="s[credit]"]').val()
	ipoCalc(p,c,cr);
}

// function initSliderFunc(obj){
	// if(obj){
		// var sl = $(obj).parent().parent().next('.slider');
		// var sl2 = $(obj).parent().parent().parent();
		// var min = 0;
		// var max = sl.parent().find('input.last_value').val();
		// sl.slider({
			// range: true,
			// min: min,
			// max: max,
			// values: [ min, max ],
			// slide: function(event, ui) {
				// sl2.find('.first_value').val( ui.values[0] );
				// sl2.find('.last_value').val( ui.values[1] );
			// }
		// });
		// sl2.parent().children('.first_value').val(sl2.slider("values", 0));
		// sl2.parent().children('.last_value').val(sl2.slider("values", 1));
	// }
	// else {
		// $(".slider").each(function(){
			// var min = 0;
			// var max = $(this).parent().find('input.last_value').data("max");
			// var valFirst = $(this).parent().find('input.first_value').val();
			// var valLast = $(this).parent().find('input.last_value').val();
			// $(this).slider({
				// range: true,
				// min: min,
				// max: max,
				// values: [ valFirst, valLast ],
				// slide: function(event, ui) {
					// $(this).parent().find('.first_value').val( ui.values[0] );
					// $(this).parent().find('.last_value').val( ui.values[1] );
					// $('.ui-slider-horizontal .ui-slider-handle').unbind("mouseup");
					// $('.ui-slider-horizontal .ui-slider-handle').bind("mouseup",function(){
						// if($('#extended_search').length>0){
							// loadFinderPage();
						// }
					// });
				// }
			// });
			// $(this).parent().find('.first_value').val($(this).slider("values", 0));
			// $(this).parent().find('.last_value').val($(this).slider("values", 1));
			// $('#extended_search p.sldr input').unbind("keyup");
			// $('#extended_search p.sldr input').bind("keyup",function(){
				// if($('#extended_search').length>0){
					// loadFinderPage();
				// }
			// });
		// });
	// }
// }

function show_contacts(obj){
	var self = $(obj);
	self.parent().parent().find('ul').removeClass('hidden');
	self.parent().remove();
}

function loadPeppermint(){
	$('.peppermint').Peppermint({
		dots: false,
		onSetup: function(n) {
			console.log('Peppermint setup done. Slides found: ' + n);
		}
	});
	$('.btns_arrow .arr.left').click(function(){
		var contBlock = $(this).parents('.container_block');
		contBlock.find('.peppermint').data('Peppermint').prev();
	});
	$('.btns_arrow .arr.right').click(function(){
		var contBlock = $(this).parents('.container_block');
		contBlock.find('.peppermint').data('Peppermint').next();
	});
}
function mainPage(){
	if($(window).innerWidth()<=800){
		if($('#peppermint').length>0){
			$('.peppermint').find('.figure').each(function(){
				if(!$(this).find('img').is('.mobile')){
					var img = $(this).find('img').data('img-mobile');
					var src = $(this).find('img').attr('src');
					$(this).find('img').removeAttr('src').attr('src',img);
					$(this).find('img').data('img-mobile',src);
					$(this).find('img').removeClass('mobile').addClass('mobile');
				}
			});
		}
	}
	else {
		if($('#peppermint').length>0){
			$('.peppermint').find('.figure').each(function(){
				if($(this).find('img').is('.mobile')){
					var img = $(this).find('img').data('img-mobile');
					var src = $(this).find('img').attr('src');
					$(this).find('img').removeAttr('src').attr('src',img);
					$(this).find('img').data('img-mobile',src);
					$(this).find('img').removeClass('mobile');
				}
			});
		}
	}
	if($(window).innerWidth()<=1179){
		if($(window).innerWidth()>=800){
			var pos2 = 1196-$(window).width();
			var con = 306;
			var pos = con + pos2 - 17;
			$('#enter_page').css({"background-position":"-"+pos+"px top"});
			$('.mainNavigation .left_nav .after').css({"width":467-pos2+17});
		}
	}
	else {
		$('#enter_page').css({"background-position":""});
		$('.mainNavigation .left_nav .after').css({"width":""});
	}
}

function tblink(n){
	if(n){
		var obj=$('.voice h3.not_voice');
		(obj.css('opacity')=='0')?obj.stop(true).animate({opacity:'1'},1000):obj.animate({opacity:'0'},1000)
		setTimeout("tblink("+(n-1)+")",100);
	};
};

function isValidEmailAddress(emailAddress) { // Валидация e-mail
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

// Проверка заполненности формы
function checkSideForm(form) {
	var fields = new Array();
	var page = arrLinkPage[1];
	var id = arrLinkPage[2];
	if(page=='newbuilding' || page=='cession'){
		id = arrLinkPage[4];
	}
    var error=0; // индекс ошибки
	$(form).find('.required').each(function(){
		var name = $(this).find('input,select,textarea').attr('name');
		fields.push(name);
	});
	$(form).find("input,select,textarea").each(function() {
		for(var x=0;x<fields.length;x++) {
			if($(this).attr("name")==fields[x]){
				if(!$(this).val() || $(this).val()==0){
					$(this).parent().addClass('error');
					error=1;
				}
				else {
					$(this).parent().removeClass('error');
			    }
		    }
		}
   });

	var options = {
		dataType: 'json',
		success: feedbackSuccessSend // функция, вызываемая при получении ответа
	};
	var email = $(form).find('input[name="s[email]"]');
	if(email.length>0){
		if(!isValidEmailAddress(email.val())){
			error=2;
			$(form).find('input[name="s[email]"]').parent().addClass('error');
		}
	}
    if(error==0){ // если ошибок нет то отправляем данные
		$(form).append('<div class="loadingProcess"><i class="fa fa-spinner fa-pulse"></i></div>');
		$(form).css({"opacity":"0.4"});
		$(form).append('<input class="appendElem" type="hidden" name="s[estate]" value="'+page+'"><input class="appendElem" type="hidden" name="s[p_main]" value="'+id+'">');
		if($(form).find('input[name="type_form"]').val()=='problema'){
			var t = $('#tabs_estate input[name="typeName"]').val();
			var e = $('#tabs_estate input[name="estateName"]').val();
			var type_commer = '';
			var type_country = '';
			if(!$('#tabs_estate input[name="type_commer"]').is(':disabled')){
				var tcom = $('#tabs_estate input[name="type_commer"]').val();
				type_commer = '<input class="appendElem" type="hidden" name="type_commer" value="'+tcom+'">';
			}
			if(!$('#tabs_estate input[name="type_country"]').is(':disabled')){
				var tcou = $('#tabs_estate input[name="type_country"]').val();
				type_country = '<input class="appendElem" type="hidden" name="type_country" value="'+tcou+'">';
			}
			$(form).append('<input class="appendElem" type="hidden" name="type" value="'+t+'"><input class="appendElem" type="hidden" name="estate" value="'+e+'">'+type_commer+type_country);
		}
		$(form).ajaxSubmit(options);
	}
	else {
		$(form).find('select').focus(function(){
			$(this).parent().removeClass('error');
		});
		$(form).find('input[type="text"]').keyup(function(){
			var l = $(this).val().length;
			if(l>2){
				$(this).parent().removeClass('error');
			}
		});
		if(error==1) var err_text = "Не все обязательные поля заполнены!";
		if(error==2)  err_text="Введен не корректный e-mail!";
		if(error==3)  err_text="Пароли не совпадают!";
		$(form).find("#messenger").html(err_text);
		$(form).find("#messenger").fadeIn("slow");
	}
	return false;
}

// Проверка заполненности формы для рег, аут и объявлений
function checkSideUniForm(form) {
	var fields = new Array();
    var error=0; // индекс ошибки
	$(form).find('.required').each(function(){
		var name = $(this).find('input,select,textarea').attr('name');
		fields.push(name);
	});
	$(form).find("input,select,textarea").not("input:disabled,select:disabled,textarea:disabled").each(function() {
		for(var x=0;x<fields.length;x++) {
			if($(this).attr("name")==fields[x]){
				if(!$(this).val() || $(this).val()==0){
					$(this).parent().addClass('error');
					error=1;
				}
				else {
					$(this).parent().removeClass('error');
			    }
		    }
		}
   });

	var options = {
		dataType: 'json',
		success: feedbackSuccessSend // функция, вызываемая при получении ответа
	};
	var email = $(form).find('input[name="s[email]"]:enabled');
	var pass = $(form).find('input[name="s[password]"]');
	var pass2 = $(form).find('input[name="s[password_repeat]"]');
	if(email.length>0){
		if(!isValidEmailAddress(email.val())){
			error=2;
			$(form).find('input[name="s[email]"]').parent().addClass('error');
		}
	}
	if(pass.length>0 && pass2.length>0){
		if(pass.val()!=pass2.val()){
			error = 3;
			pass.parent().addClass('error');
			pass2.parent().addClass('error');
		}
	}
    if(error==0){ // если ошибок нет то отправляем данные
		$(form).append('<div class="loadingProcess"><i class="fa fa-spinner fa-pulse"></i></div>');
		$(form).css({"opacity":"0.4"});
		$(form).ajaxSubmit(options);
	}
	else {
		$(form).find('select').focus(function(){
			$(this).parent().removeClass('error');
		});
		$(form).find('input[type="text"],input[type="password"],textarea').keyup(function(){
			var l = $(this).val().length;
			if(l>2){
				$(this).parent().removeClass('error');
			}
		});
		if(error==1) var err_text = "Не все обязательные поля заполнены!";
		if(error==2)  err_text="Введен не корректный e-mail!";
		if(error==3)  err_text="Пароли не совпадают!";
		// alert(err_text);
	}
	return false;
}

// Сохранение данных пользователей
function saveInfoUsers(form) {
	$('.errorBlock').remove();
	$('.successBlock').remove();
	var fields = new Array();
    var error=0; // индекс ошибки
	$(form).find('.required').each(function(){
		var name = $(this).find('input,select,textarea').not(':disabled').attr('name');
		fields.push(name);
	});
	$(form).find("input,select,textarea").each(function() {
		for(var x=0;x<fields.length;x++) {
			if($(this).attr("name")==fields[x]){
				if(!$(this).val() || $(this).val()==0){
					$(this).parent().addClass('error');
					error=1;
				}
				else {
					$(this).parent().removeClass('error');
			    }
		    }
		}
   });

	var options = {
		dataType: 'json',
		success: sendSaveInfo // функция, вызываемая при получении ответа
	};
	var email = $(form).find('input[name="s[email]"]');
	var pass = $(form).find('input[name="p[new_pass]"]');
	var pass2 = $(form).find('input[name="p[repeat_pass]"]');
	if(email.length>0){
		if(!isValidEmailAddress(email.val())){
			error=2;
			$(form).find('input[name="s[email]"]').parent().addClass('error');
		}
	}
	if(pass.length>0 && pass2.length>0){
		if(pass.val()!=pass2.val()){
			error = 3;
			pass.parent().addClass('error');
			pass2.parent().addClass('error');
		}
	}
    if(error==0){ // если ошибок нет то отправляем данные
		$(form).find('.table_form').append('<div class="loadingProcess"><i class="fa fa-spinner fa-pulse"></i></div>');
		$(form).css({"opacity":"0.4"});
		$(form).ajaxSubmit(options);
	}
	else {
		$(form).find('select').focus(function(){
			$(this).parent().removeClass('error');
		});
		$(form).find('input[type="text"]').keyup(function(){
			var l = $(this).val().length;
			if(l>2){
				$(this).parent().removeClass('error');
			}
		});
		if(error==1) var err_text = "Не все обязательные поля заполнены!";
		if(error==2)  err_text="Введен не корректный e-mail!";
		if(error==3)  err_text="Пароли не совпадают!";
		$(form).find("#messenger").html(err_text);
		$(form).find("#messenger").fadeIn("slow");
	}
	return false;
}


// Сохранение объявлений
function archiveFunc(obj) {
	var form = $(obj).parents('form');
	saveAdsUsers(form,true);
}

// Сохранение объявлений
function saveAdsUsers(form,arch){
	$('.errorBlock').remove();
	$('.successBlock').remove();
	$('input[type="hidden"][name="idAd"]').remove();
	var fields = new Array();
	var errorArray = new Array();
    var error=0; // индекс ошибки
	$(form).find('.required').each(function(){
		var name = $(this).find('input,select,textarea').not(':disabled').attr('name');
		fields.push(name);
	});
	$(form).find("input,select,textarea").each(function() {
		for(var x=0;x<fields.length;x++) {
			if($(this).attr("name")==fields[x]){
				if(!$(this).val() || $(this).val()==''){
					if($(this).attr("name")!='s[rooms]'){
						$(this).parents('.required').addClass('error');
						errorArray.push($(this).attr("name"));
						error=1;
					}
				}
				else {
					$(this).parents('.required').removeClass('error');
			    }
		    }
		}
    });
	fields = [];
	$(form).find('.required.error').each(function(){
		var name = $(this).find('input,select,textarea').not(':disabled').attr('name');
		fields.push(name);
	});
	// alert(errorArray);
	
	var options = {
		dataType: 'json',
		success: sendSaveAds // функция, вызываемая при получении ответа
	};
    if(error==0){ // если ошибок нет то отправляем данные
		$(form).find('.table_form').append('<div class="loadingProcess"><i class="fa fa-spinner fa-pulse"></i></div>');
		$(form).css({"opacity":"0.4"});
		var idAd = arrLinkPage[arrLinkPage.length-1];
		idAd = idAd.split("?");
		if(idAd!='add'){
			idAd = idAd[1].split("=");
			if(idAd[0]=='id'){
				idAd = idAd[1];
			}
			$(form).append('<input type="hidden" name="idAd" value="'+idAd+'">');
		}
		if(arch){
			$(form).append('<input type="hidden" name="save_archive" value="true">');
		}
		$(form).ajaxSubmit(options);
	}
	else {
		$(form).find('select').focus(function(){
			$(this).parent().removeClass('error');
		});
		$(form).find('input[type="text"]').keyup(function(){
			var l = $(this).val().length;
			if(l>2){
				$(this).parent().removeClass('error');
			}
		});
		if(error==1) var err_text = "Не все обязательные поля заполнены!";
		// alert(errorArray);
		alert(err_text);
		$(form).find("#messenger").html(err_text);
		$(form).find("#messenger").fadeIn("slow");
	}
	return false;
}

function deletePhoto(foto){
	var id = parseInt($(foto).parent().data('id'));
	$(foto).parent().css("opacity","0.3");
	var r = confirm("Вы уверены что хотите удалить это фото?");
	var idAd = arrLinkPage[arrLinkPage.length-1];
	idAd = idAd.split("?");
	if(idAd[1]!==undefined){
		idAd = idAd[1].split("=");
		if(idAd[0]=='id'){
			idAd = idAd[1];
		}
	}
	else {
		idAd = 0;
	}
	var idAds = "&idAd="+idAd;
	if (r == true) {
		$(foto).parent().append('<i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i>');
		$.ajax({
			type: "POST",
			url: '/include/handler.php',
			data: "deletePhoto=true&id="+id+idAds+"&rnd="+Math.random(),
			dataType: "json",
			success: function (data, textStatus) {
				var json = eval(data);
				if(json.action!='error'){
					var type = $('input[name="typeName"]').val();
					var estate = $('input[name="estateName"]').val();
					var page = window.location+"?idParam="+estate+"&type="+type;
					if(idAd){
						page = window.location+"&idParam="+estate+"&type="+type;
					}
					$('.gallery_block .photos_list').load(page+' .gallery_block .photos_list ul',function(){
						dropZone();
					});
				}
				else {
					$(foto).parent().find('.fa-spinner').remove();
					alert('Возникла системная ошибка! Данные не сохранены. Попробуйте позднее.');
				}
			}
		});
	}
	else {
		$(foto).parent().css("opacity","");
	}
}

function closeWin(i) {
    $('.window:last').fadeOut('normal');
	$('.loadingProcess').remove();
	$('.window form').css('opacity','');
	if(i == 'error'){
		$('#content form input[type="text"],#content form textarea').val('');
		$('.window form input[type="text"],.window form input[type="password"],.window form textarea').val('');
		$('#popup_blocks .feedback_form.problem_block.show').find('form').css({"opacity":""});
	}
	else {
		$('.window').remove();
		$('#popup_blocks .feedback_form.problem_block.show').removeClass('show').find('form').css({"opacity":""});
	}
	
	
	if($('#guestbook,#feedback').length>0){
		$('form input[type="text"],form textarea').val('');
		$('.js-load-guestbook,.js-success-guestbook').remove();
		$('.guestbook .add-answer').css('opacity','1');
		$("#messenger").hide();
		$('ul.raiting').find('li').removeClass('active');
		$('.js-itog-raiting').hide();
	}
	if($('.imgareaselect-outer').length>0){
		$('.imgareaselect-outer:first').prev().remove();
		$('.imgareaselect-outer').remove();
	}

	setTimeout(function() {
		$('.window:last').remove();
	}, 500);
	
}

function sendSaveInfo(responseText,result){
	if(responseText!==null){
		var top = 0;
		$(document).keydown(function(eventObject){
			if (eventObject.which == 27) {
				return closeWin();
			}
		});
		var json = eval(responseText);
		var action = json.action;
		var form = $('.loadingProcess').parents('form');
		
		var textInner = '<h2>'+json.title+'</h2><br><p>'+json.text+'</p>';
		if(json.action!='error'){
			if(json.type=='change_pass'){
				textInner = json.title;
				form.find('.table_form').append(textInner);
			}
			if(json.type=='user_info'){
				textInner = json.title;
				form.find('.table_form').append(textInner);
			}
		}
		else {
			textInner = json.text;
			form.find('.table_form').append(textInner);
		}
		form.css('opacity','');
		form.find('.loadingProcess').remove();
		setTimeout(function() {
			form.find('.table_form .errorBlock').remove();
			form.find('.table_form .successBlock').remove();
		}, 2000);
		return false;
	}
}

function sendSaveAds(responseText,result){
	if(responseText!==null){
		var top = 0;
		$(document).keydown(function(eventObject){
			if (eventObject.which == 27) {
				return closeWin();
			}
		});
		$('input[type="hidden"][name="idAd"]').remove();
		var json = eval(responseText);
		var action = json.action;
		var form = $('.loadingProcess').parents('form');
		
		if(json.action!='error'){
			if(json.redirect){
				window.location = json.link;
			}
		}
		else {
			var textInner = '<h2>'+json.title+'</h2><br><p>'+json.text+'</p>';
			textInner = json.text;
			form.find('.table_form').append(textInner);
		}
		form.css('opacity','');
		form.find('.loadingProcess').remove();
		setTimeout(function() {
			form.find('.table_form .errorBlock').remove();
			form.find('.table_form .successBlock').remove();
		}, 2000);
		return false;
	}
}

function changeThisPhoto(obj,id){
	var self = $(obj);
	self.parent().parent().parent().append('<div style="background-color:rgba(255,255,255,.5)" class="loadingProcess"><i class="fa fa-spinner fa-pulse"></i></div>')
	$.ajax({
		type: "POST",
		url: '/include/handler.php',
		data: "mainPhoto="+id+"&rnd="+Math.random(),
		dataType: "json",
		success: function (data, textStatus) {
			var json = eval(data);
			if(json.action!='error'){
				if(json.type=='not_cover'){
					self.parent().parent().parent().find('.use_image').remove();
					self.parent().removeClass('del_sel');
					self.attr('title','Выбрать изображение');
				}
				else {
					self.parent().parent().parent().children('a').append('<span class="use_image">Используется в журнале</span>');
					self.parent().addClass('del_sel');
					self.attr('title','Снять выделение');
				}
				var page = window.location;
				$('#user_settings .checked_inform .list_checked').load(page+' .load_inner_block:first');

				$('.loadingProcess').remove();
			}
			else {
				alert('Возникла системная ошибка! Данные не сохранены. Попробуйте позднее.');
			}
		}
	});
}

function delThisPhoto(obj,id){
	var self = $(obj);
	self.parent().parent().parent().append('<div style="background-color:rgba(255,255,255,.5)" class="loadingProcess"><i class="fa fa-spinner fa-pulse"></i></div>');
	var r = confirm("Вы уверены что хотите удалить это фото?");
	if (r == true) {
		$.ajax({
			type: "POST",
			url: '/include/handler.php',
			data: "delPhoto="+id+"&rnd="+Math.random(),
			dataType: "json",
			success: function (data, textStatus) {
				var json = eval(data);
				if(json.action!='error'){
					$('#user_settings .checked_inform .list_checked li[data-id="'+id+'"]').remove();
					$('#user_settings .list_photos li[data-id="'+id+'"]').remove();
				}
				else {
					alert('Возникла системная ошибка! Данные не сохранены. Попробуйте позднее.');
				}
			}
		});
	}
	else {
		$('.loadingProcess').remove();
	}
}

function cutThisPhoto(obj,id){
	var self = $(obj);
	self.parent().parent().parent().append('<div style="background-color:rgba(255,255,255,.5)" class="loadingProcess"><i class="fa fa-spinner fa-pulse"></i></div>')
	$.ajax({
		type: "POST",
		url: '/include/handler.php',
		data: "cutPhoto="+id+"&rnd="+Math.random(),
		dataType: "json",
		success: function (data, textStatus) {
			var json = eval(data);
			if(json.action!='error'){
				var top = 0;
				var i = 'img';
				$('.window:last').remove();
				$(document).keydown(function(eventObject){
					if (eventObject.which == 27) {
						return closeWin();
					}
				});
				$('body').append('<div class="window"><div onclick="return closeWin();" class="overlay"></div><div class="dialog portfolio"><span onclick="return closeWin();" class="closeDialog"></span><div class="inner"></div></div></div>');
				var curInner = $('body').find('.window:last');
				top = ($(window).height() - 175) / 2 - $('#page').offset().top + $(window).scrollTop();
				if (top < 10) {
					top = 10;
				}
				$('.window:last .dialog').css({
					'marginBottom':'20px'
				});
				var heightMain = $('.window:last');
				heightMain.find('.dialog .inner').css({ position: "absolute", visibility: "hidden", display: "block" });
				var link_img = '/users/'+json[i][1]+'/gallery/'+json[i][5];
				var width_avatar = 226;
				var height_avatar = 226;
				var page = '';
				heightMain.find('.dialog .inner').append('<form id="handlerForm"><div id="set_album_image"> \
					<input type="hidden" value="'+json[i][11]+'" name="cutPhotos"> \
					<div class="parent_block'+page+'"> \
						<div class="big_img"> \
							<p class="title">Оригинал изображения</p> \
							<div class="original_image"> \
								<div class="img avatar"><img style="width:'+json[i][12]+'px;height:'+json[i][13]+'px" id="photo" src="'+link_img+'"></div> \
							</div> \
						</div> \
						<div class="middle_img"> \
							<p class="title">Миниатюра</p> \
							<div class="preview large"><img src="'+link_img+'"></div> \
							<input id="x1" type="hidden" value="'+json[i][7]+'" name="x1"> \
							<input id="y1" type="hidden" value="'+json[i][8]+'" name="y1"> \
							<input id="x2" type="hidden" value="'+json[i][9]+'" name="x2"> \
							<input id="y2" type="hidden" value="'+json[i][10]+'" name="y2"> \
							<input id="w" type="hidden" value="'+json[i][12]+'" name="w"> \
							<input id="h" type="hidden" value="'+json[i][13]+'" name="h"> \
						</div> \
					</div> \
				</div><ul class="actions"><li><input type="submit" value="Сохранить"></li><li><a class="btn" onclick="return closeWin();" href="javascript:void(0)"><span>Закрыть</span></a></li></ul></form>');
				// img_par_list.remove();
				function preview(img, selection) {
					var scaleX = width_avatar / selection.width;
					var scaleY = height_avatar / selection.height;
					$(".preview img").css({
						width: Math.round(scaleX * json[i][12])+"px",
						height: Math.round(scaleY * json[i][13])+"px",
						marginLeft: -Math.round(scaleX * selection.x1),
						marginTop: -Math.round(scaleY * selection.y1)
					});
					$("#x1").val(selection.x1);
					$("#y1").val(selection.y1);
					$("#x2").val(selection.x2);
					$("#y2").val(selection.y2);
					$("#w").val(selection.width);
					$("#h").val(selection.height);
				}

				// $("#photo").imgAreaSelect({ aspectRatio: ''+width_avatar+':'+height_avatar+'', handles: true, fadeSpeed: 200, onSelectChange: preview });
				
				setTimeout(function() {
					$("#photo").imgAreaSelect({ aspectRatio: ''+width_avatar+':'+height_avatar+'', handles: true, fadeSpeed: 200, onSelectChange: preview, x1: json[i][7], y1: json[i][8], x2: json[i][9], y2: json[i][10] });
				},350);
				
				var heightLastMain = heightMain.find('.dialog .inner').innerHeight();
				var heightDialog = heightLastMain+80;
				heightMain.find('.dialog .inner').css({ position: "", visibility: ""});
				var top = 0;
				top = ($(window).height() - heightLastMain) / 2 - $('#page').offset().top + $(window).scrollTop();
				handlerFormAction();
				if (top < 10) {
					top = 10;
				}
				$('.window .dialog').animate({
					'top': top,
					'height' : heightDialog,
					'margin-top' : 0
				},300);
				$('#unload_img_loader').remove();
				$('.loadingProcess').remove();
			}
			else {
				alert('Возникла системная ошибка! Данные не сохранены. Попробуйте позднее.');
			}
		}
	});
}

function sendRequest(btn){
	var page = arrLinkPage[1];
	$('body').append('<div class="window opacity"><div onclick="return closeWin();" class="overlay"></div><div class="dialog"><span onclick="return closeWin();" class="close"></span><div class="inner"></div></div></div>');
	var top = 0;
	top = ($(window).height() - 175) / 2 - $('#page').offset().top + $(window).scrollTop();
	if (top < 10) {
		top = 10;
	}
	
	var textInner = '<form action="/include/handler.php" method="post" onsubmit="return checkSideForm(this)"><div class="send_request"> \
		<input type="hidden" name="type_form" value="application"> \
		<div class="title">Оставить заявку на объект недвижимости или связаться с агентом</div> \
		<div class="table"> \
			<div class="row"> \
				<div class="cell"> \
					<div class="input_text required"><input placeholder="Ваше имя" type="text" name="s[name]"></div> \
					<div class="input_text required"><input placeholder="Ваш email" type="text" name="s[email]"><span class="req_error">Не верный email</span></div> \
					<div class="input_text required"><input placeholder="Ваш телефон" type="text" class="phone" name="s[phone]"></div> \
				</div> \
				<div class="cell"> \
					<div class="input_text required"><textarea placeholder="Меня интересует этот объект недвижимости, пожалуйста, свяжитесь со мной..." name="s[msg]"></textarea></div> \
				</div> \
			</div> \
		</div> \
		<div class="btn"> \
			<a href="javascript:void(0)" onclick="return closeWin();" class="popup_cancel">Отменить</a> \
			<label class="btn"><input type="submit" value="Отправить"><span class="angle-right"></span></label> \
		</div> \
	</div></form>';
	var curInner = $('body').find('.window:last');
	curInner.find('.inner').css({ position: "absolute", visibility: "hidden", display: "block" });
	curInner.find('.dialog .inner').html(textInner);
	var heightLastMain = curInner.find('.dialog .inner').height()+74;
	curInner.find('.inner').css({ position: "", visibility: "", display: "none" });
	top = ($(window).height() - heightLastMain) / 2 - $('#page').offset().top + $(window).scrollTop();

	if (top < 10) {
		top = 10;
	}
	$('.window:last .dialog').css({
		'top': top,
		'marginTop': 0,
		'marginBottom': '20px',
	});
	$('.window:last').find('.loader').fadeOut();
	$('.window:last').find('.inner').fadeIn('fast');
	if($('.window input.phone').length>0){
		$('.window input.phone').mask("+7 (999) 999-99-99");
	}
}

/*
*  Переключение форм
*/
function table_form(obj,type){
	var $this = $(obj);
	var par = $this.parents('.table_form');
	par.find(':input').attr('disabled',true);
	if(type=='forgot'){
		var curBlock = par.parent().find('.table_form.forgot');
		curBlock.show().find(':input').attr('disabled',false);
		par.addClass('disabled');
		$this.parents('form').find('input[name="actionForm"][value="forgot"]').attr('disabled',false);
		$this.parents('form').find('input[name="actionForm"][value="auth"]').attr('disabled',true);
	}
	if(type=='cancel'){
		var curBlock = par.parent().find('.table_form').not('.forgot');
		curBlock.removeClass('disabled').find(':input').attr('disabled',false);
		$this.parents('form').find('input[name="actionForm"][value="forgot"]').attr('disabled',true);
		$this.parents('form').find('input[name="actionForm"][value="auth"]').attr('disabled',false);
		par.hide();
	}
}

/*
*  Изменение пароля от аккаунта
*/
function choosePass(obj,type){
	var $this = $(obj);
	var par = $this.parents('.table_form');
	if(type=='show'){
		par.find('.row.password').addClass('show').find('input').attr('disabled',false);
		$this.attr('onclick','return choosePass(this,\'hide\')').text('Скрыть пароль');
	}
	if(type=='hide'){
		par.find('.row.password').removeClass('show').find('input').attr('disabled',true);
		$this.attr('onclick','return choosePass(this,\'show\')').text('Изменить пароль');
	}
	
}

// Залипание главной навигации сайта
function stickMainNav(){
	var page = $('#page');
	var table = $('.result_search_header');
	if(page.length>0){
		var top = page.offset().top - parseFloat(page.css('marginTop').replace(/auto/, 0));
		var tableTop = 0;
		if(table.length>0){
			if($('#result_search').length>0){
				tableTop = $('#result_search').offset().top;
			}
			tableTop = 391;
		}
		var loadPageScroll = $(window).scrollTop();
		var windowpos = $(window).scrollTop();
		$(window).scroll(function(event){
			windowpos = $(window).scrollTop();

			var countTop = 77;
			if($('#header .contacts_block').length>0){
				countTop = 110;
			}
			if(windowpos > top+countTop) {
				page.addClass('scrolled');
			}
			else {
				page.removeClass('scrolled');
			}

			if(table.length>0){
				if($('#result_search .table_items').is(':visible')){
					if(windowpos > top+tableTop-108) {
						table.addClass('fixed');
						table.show();
					}
					else {
						table.removeClass('fixed');
						table.hide();
					}				
				}
			}
			
			countTop = 120;
			if($('#header .contacts_block').length>0){
				countTop = 145;
			}
			if(windowpos > top+countTop) {
				page.addClass('stick-params');
			}
			else {
				page.removeClass('stick-params');
			}		
		});
	}
}

function openMenu(obj){
	var self = $(obj);
	if(self.parent().parent().is('.open')){
		self.parent().parent().removeClass('open');
	}
	else {
		self.parent().parent().addClass('open');
	}
	$(document).bind('click', function(e){
		var $clicked = $(e.target);
		if(!$clicked.parents().hasClass("menu")){
			$(".hidden_menu").parent().removeClass('open');
			$(document).unbind("keydown");
		}
	});
}

function closeLocDial(obj){
	var self = $(obj);
	self.parent().parent().hide();
	$('body').height('');
}

function closeLocDial2(){
	$('#filter_search .filter_local .hidden_block').hide();
	$('body').height('');
}

// Поиск значений
var xhr5;
function searchFunction(){
	var input = $('input[type="text"][name="searchRequest"]');
	input.off("keyup");
	input.on("keyup", function(e){
		// if(e.which==40 || e.which==38 || e.which==13){
			// e.preventDefault();
			// return false
		// }
		// if(e.which==40 || e.which==38 || e.which==13){
			// e.preventDefault();
			// var hb = input.parent().find('.search_request_block');
			// var cur = hb.find('li.current');
			// var len = hb.find('li').length;
			// var index = hb.find('li').index(cur);
			// var hhb = hb.height();
			// var heightElem = hb.find('li').height();
			// if(e.which==40){ // вниз
				// var topCur = (index+1)*heightElem;
				// hb.find('.scroll-wrapper > .scroll-content').scrollTop(topCur+heightElem - heightElem);
				// hb.find('li').removeClass('current');
				// if(index+2>len){ // в начало списка
					// if(hb.find('li.current').length==0){
						// hb.find('li:first').addClass('current');
						// hb.find('.scroll-wrapper > .scroll-content').scrollTop(0);
					// }
				// }
				// else {
					// if(hb.find('li.current').length==0){
						// hb.find('li:eq('+(index+1)+')').addClass('current');
						// alert(topCur);
						// alert(hhb);
						// if(topCur>=hhb){
							// hb.find('.scroll-wrapper > .scroll-content').scrollTop(topCur+heightElem - heightElem);
						// }
						// else {
							// hb.find('.scroll-wrapper > .scroll-content').scrollTop(0);
						// }
					// }
				// }
			// }
			// if(e.which==38){ // вверх
				// cur.removeClass('current');
				// var topCur = (index+1)*heightElem;
				// if(index-1<0){ // в конец списка
					// hb.find('li:last').addClass('current');
					// hb.scrollTop((heightElem*len)+heightElem-hhb);
				// }
				// else {
					// hb.find('li:eq('+(index-1)+')').addClass('current');
					// if(topCur>=hhb){	
						// hb.scrollTop(topCur - (heightElem*2));
					// }
					// else {
						// hb.scrollTop(0);
					// }
				// }
				// hb.parent().children('input[type="hidden"]').removeAttr('value');
				// hb.parent().find('input[type="text"]:first').removeAttr('value').val(hb.find('li.current a').text());
			// }
			// if(e.which==13){ // выбор
				// selectChangeAmount(hb.find('li.current a'));
			// }
		// }
		if(xhr5 && xhr5.readyState != 4){
			xhr5.abort();
		}
		findElemSelect(this);
	});
}

// Поиск значений по адресу
var xhr7;
function searchFunctionAddress(){
	var input = $('#center .add_block .table_form input[type="text"][name="s[address]"]');
	input.off("paste");
	input.off("keyup");
	input.on("keyup", function(e){
		if(e.which==40 || e.which==38 || e.which==13){
			e.preventDefault();
			return false
		}
		if(e.which==40 || e.which==38 || e.which==13){
			e.preventDefault();
			var hb = input.parent().find('.search_request_block');
			var cur = hb.find('li.current');
			var len = hb.find('li').length;
			var index = hb.find('li').index(cur);
			var hhb = hb.height();
			var heightElem = hb.find('li').height();
			if(e.which==40){ // вниз
				var topCur = (index+1)*heightElem;
				hb.find('.scroll-wrapper > .scroll-content').scrollTop(topCur+heightElem - heightElem);
				hb.find('li').removeClass('current');
				if(index+2>len){ // в начало списка
					if(hb.find('li.current').length==0){
						hb.find('li:first').addClass('current');
						hb.find('.scroll-wrapper > .scroll-content').scrollTop(0);
					}
				}
				else {
					if(hb.find('li.current').length==0){
						hb.find('li:eq('+(index+1)+')').addClass('current');
						alert(topCur);
						alert(hhb);
						if(topCur>=hhb){
							hb.find('.scroll-wrapper > .scroll-content').scrollTop(topCur+heightElem - heightElem);
						}
						else {
							hb.find('.scroll-wrapper > .scroll-content').scrollTop(0);
						}
					}
				}
			}
			if(e.which==38){ // вверх
				cur.removeClass('current');
				var topCur = (index+1)*heightElem;
				if(index-1<0){ // в конец списка
					hb.find('li:last').addClass('current');
					hb.scrollTop((heightElem*len)+heightElem-hhb);
				}
				else {
					hb.find('li:eq('+(index-1)+')').addClass('current');
					if(topCur>=hhb){	
						hb.scrollTop(topCur - (heightElem*2));
					}
					else {
						hb.scrollTop(0);
					}
				}
				hb.parent().children('input[type="hidden"]').removeAttr('value');
				hb.parent().find('input[type="text"]:first').removeAttr('value').val(hb.find('li.current a').text());
			}
			if(e.which==13){ // выбор
				selectChangeAmountAddress(hb.find('li.current a'));
			}
		}
		if(xhr5 && xhr5.readyState != 4){
			xhr5.abort();
		}
		findElemSelectAddress(this);
	});
	input.on("paste", function(e){
		setTimeout(function() {
			findElemSelectAddress(input);
		},10);
	});

}

// Поиск значений ЖК
var xhr5;
function searchFunction2(){
	var input = $('#center .add_block .table_form input[type="text"][name="s[build_text]"]');
	input.off("keyup");
	input.on("keyup", function(e){
		if(e.which==40 || e.which==38 || e.which==13){
			e.preventDefault();
			return false
		}
		if(e.which==40 || e.which==38 || e.which==13){
			e.preventDefault();
			var hb = input.parent().find('.search_request_block');
			var cur = hb.find('li.current');
			var len = hb.find('li').length;
			var index = hb.find('li').index(cur);
			var hhb = hb.height();
			var heightElem = hb.find('li').height();
			if(e.which==40){ // вниз
				var topCur = (index+1)*heightElem;
				hb.find('.scroll-wrapper > .scroll-content').scrollTop(topCur+heightElem - heightElem);
				hb.find('li').removeClass('current');
				if(index+2>len){ // в начало списка
					if(hb.find('li.current').length==0){
						hb.find('li:first').addClass('current');
						hb.find('.scroll-wrapper > .scroll-content').scrollTop(0);
					}
				}
				else {
					if(hb.find('li.current').length==0){
						hb.find('li:eq('+(index+1)+')').addClass('current');
						alert(topCur);
						alert(hhb);
						if(topCur>=hhb){
							hb.find('.scroll-wrapper > .scroll-content').scrollTop(topCur+heightElem - heightElem);
						}
						else {
							hb.find('.scroll-wrapper > .scroll-content').scrollTop(0);
						}
					}
				}
			}
			if(e.which==38){ // вверх
				cur.removeClass('current');
				var topCur = (index+1)*heightElem;
				if(index-1<0){ // в конец списка
					hb.find('li:last').addClass('current');
					hb.scrollTop((heightElem*len)+heightElem-hhb);
				}
				else {
					hb.find('li:eq('+(index-1)+')').addClass('current');
					if(topCur>=hhb){	
						hb.scrollTop(topCur - (heightElem*2));
					}
					else {
						hb.scrollTop(0);
					}
				}
				hb.parent().children('input[type="hidden"]').removeAttr('value');
				hb.parent().find('input[type="text"]:first').removeAttr('value').val(hb.find('li.current a').text());
			}
			if(e.which==13){ // выбор
				selectChangeAmount2(hb.find('li.current a'));
			}
		}
		if(xhr5 && xhr5.readyState != 4){
			xhr5.abort();
		}
		findElemSelect2(this);
	});
}

// Поиск значений Ж/Д
var xhr5;
function searchFunctionRailway(){
	var input = $('.railway_input');
	input.off("keyup");
	input.on("keyup", function(e){
		if(e.which==40 || e.which==38 || e.which==13){
			e.preventDefault();
			return false
		}
		if(e.which==40 || e.which==38 || e.which==13){
			e.preventDefault();
			var hb = input.parent().find('.search_request_block');
			var cur = hb.find('li.current');
			var len = hb.find('li').length;
			var index = hb.find('li').index(cur);
			var hhb = hb.height();
			var heightElem = hb.find('li').height();
			if(e.which==40){ // вниз
				var topCur = (index+1)*heightElem;
				hb.find('.scroll-wrapper > .scroll-content').scrollTop(topCur+heightElem - heightElem);
				hb.find('li').removeClass('current');
				if(index+2>len){ // в начало списка
					if(hb.find('li.current').length==0){
						hb.find('li:first').addClass('current');
						hb.find('.scroll-wrapper > .scroll-content').scrollTop(0);
					}
				}
				else {
					if(hb.find('li.current').length==0){
						hb.find('li:eq('+(index+1)+')').addClass('current');
						alert(topCur);
						alert(hhb);
						if(topCur>=hhb){
							hb.find('.scroll-wrapper > .scroll-content').scrollTop(topCur+heightElem - heightElem);
						}
						else {
							hb.find('.scroll-wrapper > .scroll-content').scrollTop(0);
						}
					}
				}
			}
			if(e.which==38){ // вверх
				cur.removeClass('current');
				var topCur = (index+1)*heightElem;
				if(index-1<0){ // в конец списка
					hb.find('li:last').addClass('current');
					hb.scrollTop((heightElem*len)+heightElem-hhb);
				}
				else {
					hb.find('li:eq('+(index-1)+')').addClass('current');
					if(topCur>=hhb){	
						hb.scrollTop(topCur - (heightElem*2));
					}
					else {
						hb.scrollTop(0);
					}
				}
				hb.parent().children('input[type="hidden"]').removeAttr('value');
				hb.parent().find('input[type="text"]:first').removeAttr('value').val(hb.find('li.current a').text());
			}
			if(e.which==13){ // выбор
				selectChangeAmount2(hb.find('li.current a'));
			}
		}
		if(xhr5 && xhr5.readyState != 4){
			xhr5.abort();
		}
		findElemSelectRailway(this);
	});
}

// Поиск значений метро
var xhr5;
function searchFunctionStation(){
	var input = $('#center .add_block .table_form input[type="text"][name="s[station_id]"]');
	input.off("keyup");
	input.on("keyup", function(e){
		if(e.which==40 || e.which==38 || e.which==13){
			e.preventDefault();
			return false
		}
		if(e.which==40 || e.which==38 || e.which==13){
			e.preventDefault();
			var hb = input.parent().find('.search_request_block');
			var cur = hb.find('li.current');
			var len = hb.find('li').length;
			var index = hb.find('li').index(cur);
			var hhb = hb.height();
			var heightElem = hb.find('li').height();
			if(e.which==40){ // вниз
				var topCur = (index+1)*heightElem;
				hb.find('.scroll-wrapper > .scroll-content').scrollTop(topCur+heightElem - heightElem);
				hb.find('li').removeClass('current');
				if(index+2>len){ // в начало списка
					if(hb.find('li.current').length==0){
						hb.find('li:first').addClass('current');
						hb.find('.scroll-wrapper > .scroll-content').scrollTop(0);
					}
				}
				else {
					if(hb.find('li.current').length==0){
						hb.find('li:eq('+(index+1)+')').addClass('current');
						alert(topCur);
						alert(hhb);
						if(topCur>=hhb){
							hb.find('.scroll-wrapper > .scroll-content').scrollTop(topCur+heightElem - heightElem);
						}
						else {
							hb.find('.scroll-wrapper > .scroll-content').scrollTop(0);
						}
					}
				}
			}
			if(e.which==38){ // вверх
				cur.removeClass('current');
				var topCur = (index+1)*heightElem;
				if(index-1<0){ // в конец списка
					hb.find('li:last').addClass('current');
					hb.scrollTop((heightElem*len)+heightElem-hhb);
				}
				else {
					hb.find('li:eq('+(index-1)+')').addClass('current');
					if(topCur>=hhb){	
						hb.scrollTop(topCur - (heightElem*2));
					}
					else {
						hb.scrollTop(0);
					}
				}
				hb.parent().children('input[type="hidden"]').removeAttr('value');
				hb.parent().find('input[type="text"]:first').removeAttr('value').val(hb.find('li.current a').text());
			}
			if(e.which==13){ // выбор
				selectChangeStation(hb.find('li.current a'));
			}
		}
		if(xhr5 && xhr5.readyState != 4){
			xhr5.abort();
		}
		findElemSelectStation(this);
	});
}

// Первая заглавная буква
function ucfirst(str,act) {
	var first;
	if(act=='first'){
		first = str.substr(0,1).toUpperCase();
		return first + str.substr(1);
	}
	if(act=='all'){
		first = str.toUpperCase();
		return first;
	}
}

// Первое совпадение со искомым значением по аналогии с php функцией strpos
function strpos(haystack, needle, offset){
	var i = haystack.indexOf( needle, offset );
	return i >= 0 ? i : false;
}

var xhr6;
// Поиск значений в селекте
function findElemSelect(obj){
	var $this = $(obj);
	var container = $this.parent().find('.search_request_block');
	var typeChoose = $this.parent().find('input[type="hidden"][name="typeChoose"]').val();
	var containerList = container.find('ul');
	var hidden = $this.parent().parent().find('input[type="hidden"]');
	// hidden.removeAttr('value');
	var val2 = ucfirst($this.val(),'all');
	var val = $this.val();
	var arrayId = [];
	var arrayOf = [];
	var arrayIdParent = [];
	if(val!=''){
		if(SearchParameters!=0){
			if(val.length>0){
				var parseVal = parseInt(val);
				if($.isNumeric(parseVal)){
					
				}
				// if(!isNaN(parseVal)){// Если число
					// containerList.empty();
					// if(parseVal<9){// Квартира
						// containerList.append('<li><a type="rooms" href="javascript:void(0)" data-values="'+parseVal+'">'+parseVal+' комн. квартира</a></li>');
					// }
					// if(parseVal>9 && parseVal<200){ // Площадь
						// containerList.append('<li><a type="squareFullMax" href="javascript:void(0)" data-values="'+parseVal+'">Площадь до '+parseVal+' м²</a></li>');
					// }
					// if(parseVal>200){ // Цена
						// containerList.append('<li><a type="priceMax" href="javascript:void(0)" data-values="'+parseVal+'">Цена до <span id="thousandsSeparator">'+parseVal+'</span> тыс. рублей</a></li>');
					// }
					// container.show();
					// containerList.find('>li >a').click(function(){
						// selectChangeAmount(this);
					// });
				// }
				// else {
					if(val.length>1 || val.length==1 && !isNaN(parseVal)){
						containerList.empty();
						var id_parent = 0;
						var findValue = [];
						for(var i=0; i<SearchParameters.length; i++){
							if(SearchParameters[i]['id_part']==typeChoose){
								id_parent = i;
								findValue.push(i);
								break;
							}
						}

						if(findValue.length==0){
							id_parent = -1;
						}

						if(id_parent!=-1){
							for(var v=0; v<SearchParameters[id_parent]['searchArray'].length; v++){
								var parts = SearchParameters[id_parent]['searchArray'][v];
								for(var s=0; s<SearchParameters[id_parent]['searchArray'][v]['searchValues'].length; s++){
									if(ucfirst(SearchParameters[id_parent]['searchArray'][v]['searchValues'][s]['name'],'all').indexOf(val2,0) >= 0 ){
										arrayId.push(SearchParameters[id_parent]['searchArray'][v]['searchValues'][s]['id']);
										arrayOf.push(SearchParameters[id_parent]['searchArray'][v]['searchValues'][s]['name'].indexOf(val2,0));
										if($.inArray(id_parent,arrayIdParent)){
											var parent = {"id":id_parent,"value":SearchParameters[id_parent]['searchArray'][v]['searchValues'][s]['id'],"name":SearchParameters[id_parent]['searchArray'][v]['searchValues'][s]['name'],"searchName":SearchParameters[id_parent]['searchArray'][v]['searchName']};
											// if(SearchParameters[id_parent]['searchArray'][v]['searchName']=='address'){
												// var parent = {"id":id_parent,"value":SearchParameters[id_parent]['searchArray'][v]['searchValues'][s]['name'],"name":SearchParameters[id_parent]['searchArray'][v]['searchValues'][s]['name'],"searchName":SearchParameters[id_parent]['searchArray'][v]['searchName']};
											// }
											// if(SearchParameters[id_parent]['searchArray'][v]['searchName']=='rooms'){
												// var parent = {"id":id_parent,"value":SearchParameters[id_parent]['searchArray'][v]['searchValues'][s]['name'],"name":SearchParameters[id_parent]['searchArray'][v]['searchValues'][s]['name'],"searchName":'rooms'};
											// }
											arrayIdParent.push(parent);
										}
									}
								}
							}
						}
						$this.parent().find('input[name="searchRequestId"]').remove();
						$this.parent().find('input[name="typeSearch"]').remove();
						if(arrayIdParent.length>0){
							for(var m=0; m<arrayIdParent.length; m++){
								var str = strpos(ucfirst(arrayIdParent[m]['name'],'all'),val2,0);
								var n = val.length;
								var name2 = arrayIdParent[m]['name'].substr(str,n);
								var name = arrayIdParent[m]['name'].replace(name2,'<b>'+name2+'</b>');
								var current = '';
								if(m==0){
									current = ' class="current"';
									$('<input name="searchRequestId" type="hidden" value="'+arrayIdParent[m]['value']+'">').insertBefore($this.parent().find('input[name="searchRequest"]'));
									$('<input name="typeSearch" type="hidden" value="'+arrayIdParent[m]['searchName']+'">').insertBefore($this.parent().find('input[name="searchRequest"]'));
								}
								containerList.append('<li'+current+'><a href="javascript:void(0)" data-values="'+arrayIdParent[m]['value']+'" data-type="'+arrayIdParent[m]['searchName']+'">'+name+'</a></li>');
							}
						}
						else {
							containerList.append('<li class="not_found"><span>Ничего не найдено</span></li>');
						}
						container.show();
						containerList.find('>li >a').click(function(){
							selectChangeAmount(this);
						});
					}
					else {
						$this.parent().find('input[name="searchRequestId"]').remove();
						$this.parent().find('input[name="typeSearch"]').remove();
						containerList.empty();
						container.hide();
					}
				// }
			}
		}
	}
	else {
		if(SearchParameters!=0){
			$this.parent().find('input[name="searchRequestId"]').remove();
			$this.parent().find('input[name="typeSearch"]').remove();
			containerList.empty();
			container.hide();
			// containerList.append('<li><a href="javascript:void(0)" data-values="0"></a></li>');
			// for(var i=0; i<SearchParameters.length; i++){
				// for(var s=0; s<SearchParameters[i]['searchValues'].length; s++){
					// containerList.append('<li><a href="javascript:void(0)" data-values="'+SearchParameters[i]['id']+'">'+SearchParameters[i]['name']+'</a></li>');
				// }
			// }
		}
	}
	container.find('.scroll-wrapper').mouseover(function(){
		$('body').css({"overflow":"hidden"});
	});
	
	container.find('.scroll-wrapper').mouseleave(function(){
		$('body').css({"overflow":""});
	});
	
	$(document).bind('click', function(e){
		var $clicked = $(e.target);
		if(!$clicked.parents().hasClass("request_block")){
			$(".request_block .search_request_block").hide();
			$(document).unbind("keydown");
		}
	});
	
	$this.off("focus");
	$this.on("focus",function(){
		// container.show();
		findElemSelect($(this));
	});
		
}

// Поиск значений в селекте адреса
/* function findElemSelectAddress(obj){
	var $this = $(obj);
	var container = $this.parent().find('.search_request_block');
	var containerList = container.find('ul');
	var hidden = $this.parent().parent().find('input[type="hidden"]');
	var val2 = ucfirst($this.val(),'all');
	var val = $this.val();
	var arrayId = [];
	var arrayOf = [];
	var arrayIdParent = [];
	if(val!=''){
		if(SearchParamAddress!=0){
			if(val.length>0){
				var parseVal = parseInt(val);
				if(val.length>1 || val.length==1 && !isNaN(parseVal)){
					containerList.empty();

					for(var v=0; v<SearchParamAddress.length; v++){
						var parts = SearchParamAddress[v];
						if(ucfirst(SearchParamAddress[v]['name'],'all').indexOf(val2,0) >= 0 ){
							arrayId.push(SearchParamAddress[v]['id']);
							arrayOf.push(SearchParamAddress[v]['name'].indexOf(val2,0));
							var parent = {"value":SearchParamAddress[v]['id'],"name":SearchParamAddress[v]['name'],"station":SearchParamAddress[v]['station'],"station_name":SearchParamAddress[v]['station_name'],"build":SearchParamAddress[v]['build'],"build_text":SearchParamAddress[v]['build_text'],"distance":SearchParamAddress[v]['distance']};
							arrayIdParent.push(parent);
						}
					}
					if(arrayIdParent.length>0){
						for(var m=0; m<arrayIdParent.length; m++){
							var str = strpos(ucfirst(arrayIdParent[m]['name'],'all'),val2,0);
							var n = val.length;
							var name2 = arrayIdParent[m]['name'].substr(str,n);
							var name = arrayIdParent[m]['name'].replace(name2,'<b>'+name2+'</b>');
							var current = '';
							if(m==0){
								current = ' class="current"';
								$('<input name="searchRequestId" type="hidden" value="'+arrayIdParent[m]['value']+'">').insertBefore($this.parent().find('input[name="searchRequest"]'));
							}
							containerList.append('<li'+current+'><a href="javascript:void(0)" data-values="'+arrayIdParent[m]['value']+'" data-type="'+arrayIdParent[m]['name']+'" data-build_text="'+arrayIdParent[m]['build_text']+'" data-build="'+arrayIdParent[m]['build']+'" data-station="'+arrayIdParent[m]['station']+'" data-station_name="'+arrayIdParent[m]['station_name']+'" data-distance="'+arrayIdParent[m]['distance']+'">'+name+'</a></li>');
						}
					}
					else {
						containerList.append('<li class="not_found"><span>Ничего не найдено</span></li>');
						$this.parent().find('input[name="s[build]"]').removeAttr('value');
					}
					container.show();
					containerList.find('>li >a').click(function(){
						selectChangeAmountAddress(this);
					});
				}
				else {
					$this.parent().find('input[name="searchRequestId"]').remove();
					$this.parent().find('input[name="typeSearch"]').remove();
					containerList.empty();
					container.hide();
				}
			}
		}
	}
	else {
		if(SearchParamAddress!=0){
			$this.parent().find('input[name="searchRequestId"]').remove();
			$this.parent().find('input[name="typeSearch"]').remove();
			containerList.empty();
			container.hide();
			// containerList.append('<li><a href="javascript:void(0)" data-values="0"></a></li>');
			// for(var i=0; i<SearchParamAddress.length; i++){
				// for(var s=0; s<SearchParamAddress[i]['searchValues'].length; s++){
					// containerList.append('<li><a href="javascript:void(0)" data-values="'+SearchParamAddress[i]['id']+'">'+SearchParamAddress[i]['name']+'</a></li>');
				// }
			// }
		}
	}
	
	$(document).bind('click', function(e){
		var $clicked = $(e.target);
		if(!$clicked.parents().hasClass("request_block")){
			$(".request_block .search_request_block").hide();
			$(document).unbind("keydown");
		}
	});
	
	$this.off("focus");
	$this.on("focus",function(){
		// container.show();
		// findElemSelectAddress($(this));
	});
		
}
 */
 var chooseValue = false;
 var oldValue;
 var xhr33;
 function findElemSelectAddress(obj,f){
	var $this = $(obj);
	var addressInput = $('#center .add_block .table_form input[name="s[address]"]');
	var locationInput = $('#center .add_block .table_form input[name="s[location]"]');
	oldValue = addressInput.val();
	var container = $this.parent().find('.search_request_block');
	var containerList = container.find('ul');
	var hidden = $this.parent().parent().find('input[type="hidden"]');
	var val2 = ucfirst($this.val(),'all');
	var val = $this.val();
	var arrayOf = [];
	var arrayIdParent = [];
	var requestSearch = false;
	if(!f){
		chooseValue = false;
	}
	if(val!=''){
		if(val.length>0){
			
			var countryMap = false;
			var address = addressInput.val();
			var local = '';
			if(address!=''){
				countryMap = true;
			}
			if(locationInput.val()!='' && locationInput.val()>0){
				local = '&location='+locationInput.val();
			}
			
			var data = "geolocation="+address+local;
			var map = '';
			if(xhr33 && xhr33.readyState != 4){
				xhr33.abort();
			}
			if(countryMap && address.length>2){
				xhr33 = $.ajax({
					type: 'POST',
					url: "/include/handler.php",  
					cache: false,
					data: data,
					success: function(data){
						var json = eval(data);
						if(json.length>0){
							containerList.empty();
							for(var v=0; v<json.length; v++){
								// if(ucfirst(json[v]['address'],'all').indexOf(val2,0) >= 0 ){
									arrayOf.push(json[v]['address'].indexOf(val2,0));
									var parent = {"address":json[v]['address'],"coords":json[v]['coords'],"location":json[v]['location'],"area":json[v]['area'],"locality":json[v]['locality']};
									arrayIdParent.push(parent);
								// }
							}
							if(arrayIdParent.length>0){
								for(var m=0; m<arrayIdParent.length; m++){
									var str = strpos(ucfirst(arrayIdParent[m]['address'],'all'),val2,0);
									var n = val.length;
									var address2 = arrayIdParent[m]['address'].substr(str,n);
									var address = arrayIdParent[m]['address'].replace(address2,'<b>'+address2+'</b>');
									var current = '';
									if(m==0){
										current = ' class="current"';
										$('<input name="searchRequestId" type="hidden" value="'+arrayIdParent[m]['value']+'">').insertBefore($this.parent().find('input[name="searchRequest"]'));
									}
									containerList.append('<li'+current+'><a href="javascript:void(0)" data-values="'+arrayIdParent[m]['address']+'" data-type="'+arrayIdParent[m]['address']+'" data-location="'+arrayIdParent[m]['location']+'" data-area="'+arrayIdParent[m]['area']+'"  data-locality="'+arrayIdParent[m]['locality']+'" data-coords="'+arrayIdParent[m]['coords']+'">'+address+'</a></li>');
								}
							}
							else {
								containerList.append('<li class="not_found"><span>Ничего не найдено</span></li>');
								// $this.parent().find('input[name="s[build]"]').removeAttr('value');
							}
							container.show();
							containerList.find('>li >a').click(function(){
								selectChangeAmountAddress(this);
								// addressInput.attr('data-checked',true);
							});

							var ex_coord = json[0]['coords'].split(' ');
							$('#map').empty();
							
							
							ymaps.ready(function () {
								var myMap = new ymaps.Map('map', {
									center: [ex_coord[1],ex_coord[0]],
									zoom: 14
								}, {
									searchControlProvider: 'yandex#search'
								}),
								myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
									hintContent: 'Метка на карте',
									balloonContent: ''
								}, {
									// Опции.
									// Необходимо указать данный тип макета.
									iconLayout: 'default#image',
									// Своё изображение иконки метки.
									iconImageHref: '/images/neitralIcon.png',
									// Размеры метки.
									iconImageSize: [52, 44],
									// Смещение левого верхнего угла иконки относительно
									// её "ножки" (точки привязки).
									iconImageOffset: [-3, -42]
								});

								myMap.geoObjects.add(myPlacemark);
								
								var coords = ex_coord[1]+','+ex_coord[0];
								var coords2 = ex_coord[0]+','+ex_coord[1];
								ymaps.geocode(coords, { kind: 'metro' }).then(function (res) {
									var m0 = res.geoObjects.get(0);
									var m0_coords = m0.geometry.getCoordinates();
									var name = m0.properties.get('name');
									var myRouter = ymaps.route([
									  name, {
										type: "viaPoint",                   
										point: json[0]['address']
									  },
									], {
									  mapStateAutoApply: true 
									});
									
									myRouter.then(function(route) {
										var points = route.getWayPoints();
										points.options.set('preset', 'islands#blackStretchyIcon');
										points.get(0).properties.set("iconContent", name);
										points.get(1).properties.set("iconContent", 'Объект');
										// Добавление маршрута на карту
										// myMap.geoObjects.add(route);
										var routeLength = route.getHumanLength().toString();
										var routeLength2 = route.getLength();
										$('.table_form .row .stations li .metro strong').html(routeLength);
										if($('input[name="s[dist_value]"]').length>0){
											$('input[name="s[dist_value]"]').val(routeLength2);						
										}
										if($('input[name="s[coords]"]').length>0){
											$('input[name="s[coords]"]').val(coords);						
										}
									  },
									  // Обработка ошибки
									  function (error) {
										// alert("Возникла ошибка: " + error.message);
									  }
									)
								});
									
							});
						}
						else {
							$this.parent().find('input[name="searchRequestId"]').remove();
							$this.parent().find('input[name="typeSearch"]').remove();
							containerList.empty();
							container.hide();
							addressInput.attr('data-checked',false);
						}
					},
					error: function(xhr, error){
						$('#map').empty();
						ymaps.ready(function () {
							var myMap = new ymaps.Map('map', {
								zoom: 14
							}, {
								searchControlProvider: 'yandex#search'
							}),
							myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
								hintContent: 'Метка на карте',
								balloonContent: ''
							}, {
								// Опции.
								// Необходимо указать данный тип макета.
								iconLayout: 'default#image',
								// Своё изображение иконки метки.
								iconImageHref: '/images/neitralIcon.png',
								// Размеры метки.
								iconImageSize: [52, 44],
								// Смещение левого верхнего угла иконки относительно
								// её "ножки" (точки привязки).
								iconImageOffset: [-3, -42]
							});

							myMap.geoObjects.add(myPlacemark);
							
							var address = $('input[name="s[address]"]').val();
							ymaps.geocode(address,{kind:'metro'}).then(function(res){
								var m0 = res.geoObjects.get(0);
								var m0_coords = m0.geometry.getCoordinates();
								var addressName = m0.properties.get('name');
							
								ymaps.geocode(m0_coords,{kind:'metro'}).then(function(res){
									var m0 = res.geoObjects.get(0);
									var m0_coords = m0.geometry.getCoordinates();
									var name = m0.properties.get('name');
									var myRouter = ymaps.route([
									  name, {
										type: "viaPoint",                   
										point: addressName
									  },
									], {
									  mapStateAutoApply: true 
									});
									
									myRouter.then(function(route) {
										var points = route.getWayPoints();
										points.options.set('preset', 'islands#blackStretchyIcon');
										points.get(0).properties.set("iconContent", name);
										points.get(1).properties.set("iconContent", 'Объект');
										// Добавление маршрута на карту
										myMap.geoObjects.add(route);
										var routeLength = route.getHumanLength().toString();
										var routeLength2 = route.getLength();
										$('.table_form .row .stations li .metro strong').html(routeLength);
										if($('input[name="s[dist_value]"]').length>0){
											$('input[name="s[dist_value]"]').val(routeLength2);						
										}
									  },
									  // Обработка ошибки
									  function (error) {
										// alert("Возникла ошибка: " + error.message);
									  }
									)
								});
							});
								
						});
					},
				});
			}
			
		}
	}
	else {
		$this.parent().find('input[name="searchRequestId"]').remove();
		$this.parent().find('input[name="typeSearch"]').remove();
		containerList.empty();
		container.hide();
		addressInput.attr('data-checked',false);
		// containerList.append('<li><a href="javascript:void(0)" data-values="0"></a></li>');
		// for(var i=0; i<SearchParamAddress.length; i++){
			// for(var s=0; s<SearchParamAddress[i]['searchValues'].length; s++){
				// containerList.append('<li><a href="javascript:void(0)" data-values="'+SearchParamAddress[i]['id']+'">'+SearchParamAddress[i]['name']+'</a></li>');
			// }
		// }
	}
		
	$(document).bind('click', function(e){
		var $clicked = $(e.target);
		if(!$clicked.parents().hasClass("request_block")){
			$(".request_block .search_request_block").hide();
			$(document).unbind("keydown");
			if(chooseValue!==true){
				addressInput.removeAttr('value');
			}
		}
	});
	
	$this.off("focus");
	$this.on("focus",function(){
		findElemSelectAddress($(this),true);
		container.show();
	});
		
}

var xhr6;
// Поиск значений в селекте
function findElemSelect2(obj){
	var $this = $(obj);
	var container = $this.parent().find('.search_request_block');
	var typeChoose = $this.parent().find('input[type="hidden"][name="typeChoose"]').val();
	var containerList = container.find('ul');
	var hidden = $this.parent().parent().find('input[type="hidden"]');
	// hidden.removeAttr('value');
	var val2 = ucfirst($this.val(),'all');
	var val = $this.val();
	var arrayId = [];
	var arrayOf = [];
	var arrayIdParent = [];
	if(val!=''){
		if(SearchParam!=0){
			if(val.length>0){
				var parseVal = parseInt(val);
				if(val.length>1 || val.length==1 && !isNaN(parseVal)){
					containerList.empty();

					for(var v=0; v<SearchParam.length; v++){
						var parts = SearchParam[v];
						if(ucfirst(SearchParam[v]['name'],'all').indexOf(val2,0) >= 0 ){
							arrayId.push(SearchParam[v]['id']);
							arrayOf.push(SearchParam[v]['name'].indexOf(val2,0));
							var parent = {"value":SearchParam[v]['id'],"name":SearchParam[v]['name'],"address":SearchParam[v]['address'],"station":SearchParam[v]['station'],"station_name":SearchParam[v]['station_name'],"distance":SearchParam[v]['distance'],"dist_value":SearchParam[v]['dist_value'],"coords":SearchParam[v]['coords']};
							arrayIdParent.push(parent);
						}
					}
					if(arrayIdParent.length>0){
						for(var m=0; m<arrayIdParent.length; m++){
							var str = strpos(ucfirst(arrayIdParent[m]['name'],'all'),val2,0);
							var n = val.length;
							var name2 = arrayIdParent[m]['name'].substr(str,n);
							var name = arrayIdParent[m]['name'].replace(name2,'<b>'+name2+'</b>');
							var current = '';
							if(m==0){
								current = ' class="current"';
								$('<input name="searchRequestId" type="hidden" value="'+arrayIdParent[m]['value']+'">').insertBefore($this.parent().find('input[name="searchRequest"]'));
							}
							containerList.append('<li'+current+'><a href="javascript:void(0)" data-values="'+arrayIdParent[m]['value']+'" data-type="'+arrayIdParent[m]['name']+'" data-address="'+arrayIdParent[m]['address']+'" data-station="'+arrayIdParent[m]['station']+'" data-station_name="'+arrayIdParent[m]['station_name']+'" data-distance="'+arrayIdParent[m]['distance']+'" data-dist="'+arrayIdParent[m]['dist_value']+'" data-coords="'+arrayIdParent[m]['coords']+'">'+name+'</a></li>');
						}
					}
					else {
						containerList.append('<li class="not_found"><span>Ничего не найдено</span></li>');
						// $this.parent().find('input[name="s[build]"]').removeAttr('value');
					}
					container.show();
					containerList.find('>li >a').click(function(){
						selectChangeAmount2(this);
					});
				}
				else {
					$this.parent().find('input[name="searchRequestId"]').remove();
					$this.parent().find('input[name="typeSearch"]').remove();
					containerList.empty();
					container.hide();
				}
			}
		}
	}
	else {
		if(SearchParam!=0){
			$this.parent().find('input[name="searchRequestId"]').remove();
			$this.parent().find('input[name="typeSearch"]').remove();
			containerList.empty();
			container.hide();
			// containerList.append('<li><a href="javascript:void(0)" data-values="0"></a></li>');
			// for(var i=0; i<SearchParam.length; i++){
				// for(var s=0; s<SearchParam[i]['searchValues'].length; s++){
					// containerList.append('<li><a href="javascript:void(0)" data-values="'+SearchParam[i]['id']+'">'+SearchParam[i]['name']+'</a></li>');
				// }
			// }
		}
	}
	
	$(document).bind('click', function(e){
		var $clicked = $(e.target);
		if(!$clicked.parents().hasClass("request_block")){
			$(".request_block .search_request_block").hide();
			$(document).unbind("keydown");
		}
	});
	
	$this.off("focus");
	$this.on("focus",function(){
		// container.show();
		findElemSelect2($(this));
	});
		
}

var xhr90;
// Поиск значений в селекте
function findElemSelectRailway(obj){
	var $this = $(obj);
	var container = $this.parent().find('.search_request_block');
	var typeChoose = $this.parent().find('input[type="hidden"][name="typeChoose"]').val();
	var containerList = container.find('ul');
	var hidden = $this.parent().parent().find('input[type="hidden"]');
	// hidden.removeAttr('value');
	var val2 = ucfirst($this.val(),'all');
	var val = $this.val();
	var arrayId = [];
	var arrayOf = [];
	var arrayIdParent = [];
	if(val!=''){
		if(SearchParamRailway!=0){
			if(val.length>0){
				var parseVal = parseInt(val);
				if(val.length>0 || val.length==1 && !isNaN(parseVal)){
					containerList.empty();

					for(var v=0; v<SearchParamRailway.length; v++){
						var parts = SearchParamRailway[v];
						if(ucfirst(SearchParamRailway[v]['name'],'all').indexOf(val2,0) >= 0 ){
							arrayId.push(SearchParamRailway[v]['id']);
							arrayOf.push(SearchParamRailway[v]['name'].indexOf(val2,0));
							var parent = {"value":SearchParamRailway[v]['id'],"name":SearchParamRailway[v]['name'],"name_way":SearchParamRailway[v]['nameWay']};
							arrayIdParent.push(parent);
						}
					}
					if(arrayIdParent.length>0){
						for(var m=0; m<arrayIdParent.length; m++){
							var str = strpos(ucfirst(arrayIdParent[m]['name'],'all'),val2,0);
							var n = val.length;
							var name2 = arrayIdParent[m]['name'].substr(str,n);
							var name = arrayIdParent[m]['name'].replace(name2,'<b>'+name2+'</b>');
							var current = '';
							if(m==0){
								current = ' class="current"';
								$('<input name="searchRequestId" type="hidden" value="'+arrayIdParent[m]['value']+'">').insertBefore($this.parent().find('input[name="searchRequest"]'));
							}
							containerList.append('<li'+current+'><a href="javascript:void(0)" data-values="'+arrayIdParent[m]['value']+'" data-type="'+arrayIdParent[m]['name']+'" data-tags="'+arrayIdParent[m]['name']+'">'+name+'<small>'+arrayIdParent[m]['name_way']+'</small></a></li>');
						}
					}
					else {
						containerList.append('<li class="not_found"><span>Ничего не найдено</span></li>');
						// $this.parent().find('input[name="s[build]"]').removeAttr('value');
					}
					container.show();
					containerList.find('>li >a').click(function(){
						selectChangeAmountRailway(this);
					});
				}
				else {
					$this.parent().find('input[name="searchRequestId"]').remove();
					$this.parent().find('input[name="typeSearch"]').remove();
					containerList.empty();
					container.hide();
				}
			}
		}
	}
	else {
		if(SearchParamRailway!=0){
			$this.parent().find('input[name="searchRequestId"]').remove();
			$this.parent().find('input[name="typeSearch"]').remove();
			containerList.empty();
			container.hide();
			// containerList.append('<li><a href="javascript:void(0)" data-values="0"></a></li>');
			// for(var i=0; i<SearchParamRailway.length; i++){
				// for(var s=0; s<SearchParamRailway[i]['searchValues'].length; s++){
					// containerList.append('<li><a href="javascript:void(0)" data-values="'+SearchParamRailway[i]['id']+'">'+SearchParamRailway[i]['name']+'</a></li>');
				// }
			// }
		}
	}
	
	$(document).bind('click', function(e){
		var $clicked = $(e.target);
		if(!$clicked.parents().hasClass("request_block")){
			$(".request_block .search_request_block").hide();
			$(document).unbind("keydown");
		}
	});
	
	$this.off("focus");
	$this.on("focus",function(){
		// container.show();
		findElemSelectRailway($(this));
	});
		
}

var xhr7;
// Поиск значений в селекте метро
function findElemSelectStation(obj){
	var $this = $(obj);
	var container = $this.parent().find('.search_request_block');
	var typeChoose = $this.parent().find('input[type="hidden"][name="typeChoose"]').val();
	var containerList = container.find('ul');
	var hidden = $this.parent().parent().find('input[type="hidden"]');
	// hidden.removeAttr('value');
	var val2 = ucfirst($this.val(),'all');
	var val = $this.val();
	var arrayId = [];
	var arrayOf = [];
	var arrayIdParent = [];
	if(val!=''){
		if(SearchParamStation!=0){
			if(val.length>0){
				var parseVal = parseInt(val);
				if(val.length>1 || val.length==1 && !isNaN(parseVal)){
					containerList.empty();

					for(var v=0; v<SearchParamStation.length; v++){
						var parts = SearchParamStation[v];
						if(ucfirst(SearchParamStation[v]['name'],'all').indexOf(val2,0) >= 0 ){
							arrayId.push(SearchParamStation[v]['id']);
							arrayOf.push(SearchParamStation[v]['name'].indexOf(val2,0));
							var parent = {"value":SearchParamStation[v]['id'],"name":SearchParamStation[v]['name']};
							arrayIdParent.push(parent);
						}
					}
					if(arrayIdParent.length>0){
						for(var m=0; m<arrayIdParent.length; m++){
							var str = strpos(ucfirst(arrayIdParent[m]['name'],'all'),val2,0);
							var n = val.length;
							var name2 = arrayIdParent[m]['name'].substr(str,n);
							var name = arrayIdParent[m]['name'].replace(name2,'<b>'+name2+'</b>');
							var current = '';
							if(m==0){
								current = ' class="current"';
								$('<input name="searchRequestId" type="hidden" value="'+arrayIdParent[m]['value']+'">').insertBefore($this.parent().find('input[name="searchRequest"]'));
							}
							containerList.append('<li'+current+'><a href="javascript:void(0)" data-values="'+arrayIdParent[m]['value']+'" data-type="'+arrayIdParent[m]['name']+'">'+name+'</a></li>');
						}
					}
					else {
						containerList.append('<li class="not_found"><span>Ничего не найдено</span></li>');
						$this.parent().find('input[name="s[station_id]"]').removeAttr('value');
					}
					container.show();
					containerList.find('>li >a').click(function(){
						selectChangeStation(this);
					});
				}
				else {
					$this.parent().find('input[name="searchRequestId"]').remove();
					$this.parent().find('input[name="typeSearch"]').remove();
					containerList.empty();
					container.hide();
				}
			}
		}
	}
	else {
		if(SearchParamStation!=0){
			$this.parent().find('input[name="searchRequestId"]').remove();
			$this.parent().find('input[name="typeSearch"]').remove();
			containerList.empty();
			container.hide();
		}
	}
	
	$(document).bind('click', function(e){
		var $clicked = $(e.target);
		if(!$clicked.parents().hasClass("request_block")){
			$(".request_block .search_request_block").hide();
			$(document).unbind("keydown");
		}
	});
	
	$this.off("focus");
	$this.on("focus",function(){
		// container.show();
		findElemSelectStation($(this));
	});
		
}

/*
*  Disabled пустых и не используемых значений
*/
function clearEmptyValue(){
	var estate = $('#filter_search form input[type="hidden"][name="estate"]').parent().find('li.checked a').data('type');
	var mainType = $('#filter_search .simple_search .checkbox_block.types .select_checkbox ul li.checked a').data('type');
	$('#filter_search .simple_search .inner_block .change_block_filter').each(function(){
		if(!$(this).is('.'+estate+'.'+mainType)){
			$(this).find('input[type="hidden"]').attr('disabled',true);
		}
		else {
			$(this).find('input[type="hidden"]').each(function(){
				if($(this).val()==''){
					$(this).attr('disabled',true);
				}
				else {
					$(this).attr('disabled',false);
				}
			});
		}
	});

	$('#filter_search .advanced_search .inner_block').each(function(){
		var typeCommer = '';
		if(estate=='commercial'){
			typeCommer = $('input[type="hidden"][name="type_commer"]').val();
		}
		var typeCountry = '';
		if(estate=='country'){
			typeCountry = $('input[type="hidden"][name="type_country"]').val();
		}
		if($(this).is('.'+estate)){
			$(this).find('.row').each(function(){
				if(typeCommer!=''){
					if($(this).is('.'+mainType) && $(this).is('.type_commer_'+typeCommer)){
						$(this).find('input[type="hidden"]').each(function(){
							if($(this).val()==''){
								$(this).attr('disabled',true);
							}
							else {
								$(this).attr('disabled',false);
							}
						});
					}
					else {
						$(this).find('input[type="hidden"]').attr('disabled',true);
					}
				}
				else if(typeCountry!=''){
					if($(this).is('.'+mainType) && $(this).is('.type_country_'+typeCountry)){
						$(this).find('input[type="hidden"]').each(function(){
							if($(this).val()==''){
								$(this).attr('disabled',true);
							}
							else {
								$(this).attr('disabled',false);
							}
						});
					}
					else {
						$(this).find('input[type="hidden"]').attr('disabled',true);
					}
				}
				else {
					if($(this).is('.'+mainType)){
						$(this).find('input[type="hidden"]').each(function(){
							if($(this).val()==''){
								$(this).attr('disabled',true);
							}
							else {
								$(this).attr('disabled',false);
							}
						});
					}
					else {
						$(this).find('input[type="hidden"]').attr('disabled',true);
					}
				}
			});
		}
		else {
			$(this).find('input[type="hidden"]').attr('disabled',true);
		}
	});
	
	$('#filter_search .location_change .filter_local input[type="hidden"]').each(function(){
		if($(this).val()==''){
			$(this).attr('disabled',true);
		}
		else {
			$(this).attr('disabled',false);
		}
	});
	
	$('#filter_search .simple_search .price_block').each(function(){
		var cession = $('#filter_search .change_block_filter.new input[type="hidden"][name="cession"]');
		var valCession = cession.val();
		var estate2 = estate;
		if(valCession==1 && cession.is(':enabled')){
			estate2 = 'cession';
		}
		typeCommerce();
		typeCountry();
		if(!$(this).is('.'+estate2+'.'+mainType)){
			$(this).find('input[type="hidden"]').attr('disabled',true);
		}
		else {
			if(estate2=='commercial'){
				var ch = $('.price_block.commercial.'+mainType).find('input[type="hidden"][name="priceAll"]').val();
				var typeComm = $('.change_block_filter.commercial.'+mainType).find('input[name="type_commer"]');
				var typeCommValue = typeComm.val();
				
				$('.price_block.commercial.'+mainType).find('.select_block.price.all').find('input[type="hidden"]').attr('disabled',true);
				$('.price_block.commercial.'+mainType).find('.prices_choose').find('input[type="hidden"]').attr('disabled',true);
				$('.price_block.commercial.'+mainType+'.type_commer_'+typeCommValue+' .prices_choose.'+ch).find('input[type="hidden"]').attr('disabled',false);
				$('.price_block.commercial.'+mainType+'.type_commer_'+typeCommValue+' .select_block.price.all').find('input[type="hidden"]').attr('disabled',false);
				var priceAll = ch;
				var priceAll2 = 'meters';
				if(priceAll=='meters'){
					priceAll2 = 'all';
				}
				$('.price_block.commercial.'+mainType).find('.prices_choose').find('input[type="hidden"]').each(function(){
					$(this).attr('disabled',true);
				});
				$('.price_block.commercial.'+mainType+'.type_commer_'+typeCommValue+' .prices_choose.'+ch).find('input[type="hidden"]').each(function(){
					if($(this).val()==''){
						$(this).attr('disabled',true);
					}
					else {
						$(this).attr('disabled',false);
					}
				});
				
				$('input[type="text"][name="power_max"]').each(function(){
					if($(this).val()==''){
						$(this).attr('disabled',true);
					}
					else {
						$(this).attr('disabled',false);
					}
				});
				
				$('input[type="text"][name="name_villiage"]').each(function(){
					if($(this).val()==''){
						$(this).attr('disabled',true);
					}
					else {
						$(this).attr('disabled',false);
					}
				});
			}
			else {
				var priceAll = $(this).find('input[name="priceAll"]').val();
				var priceAll2 = 'meters';
				if(priceAll=='meters'){
					priceAll2 = 'all';
				}
				$(this).find('.prices_choose.'+priceAll2+' input[type="hidden"]').each(function(){
					$(this).attr('disabled',true);
				});
				$(this).find('.prices_choose.'+priceAll+' input[type="hidden"]').each(function(){
					if($(this).val()==''){
						$(this).attr('disabled',true);
					}
					else {
						$(this).attr('disabled',false);
					}
				});
				$('input[type="text"][name="power_max"]').each(function(){
					if($(this).val()==''){
						$(this).attr('disabled',true);
					}
					else {
						$(this).attr('disabled',false);
					}
				});
			}
		}
			// else {
			// var priceAll = $(this).find('input[name="priceAll"]').val();
			// var priceAll2 = 'meters';
			// if(priceAll=='meters'){
				// priceAll2 = 'all';
			// }
			// $(this).find('.prices_choose.'+priceAll2+' input[type="hidden"]').each(function(){
				// $(this).attr('disabled',true);
			// });
			// $(this).find('.prices_choose.'+priceAll+' input[type="hidden"]').each(function(){
				// if($(this).val()==''){
					// $(this).attr('disabled',true);
				// }
				// else {
					// $(this).attr('disabled',false);
				// }
			// });
		// }
	});
}

function choose_view(obj){
	var $this = $(obj);
	var type = $this.data('type');
	if(!$this.parent().is('.current')){
		$this.parent().parent().find('li').removeClass('current');
		$this.parent().addClass('current');
		if(type=='list'){
			$('.table_items').hide();
			$('#result_search').show();
		}
		if(type=='table'){
			$('.table_items').show();
			$('#result_search').hide();
		}
	}
}

function addFavorite(obj,type,ids){
	var self = $(obj);
	var id = parseInt(arrLinkPage[arrLinkPage.length-1]);
	var estate = arrLinkPage[1];
	var build = false;
	if(estate=='newbuilding'){
		estate = 1;
	}
	if(estate=='flats'){
		estate = 2;
	}
	if(estate=='rooms'){
		estate = 3;
	}
	if(estate=='countries'){
		estate = 4;
	}
	if(estate=='commercials'){
		estate = 5;
	}
	if(estate==1 && arrLinkPage.length==3){
		build = true;
	}
	if(type=='list'){
		id = self.parents('.item').attr('id');
		id = id.split('_');
		id = parseInt(id[1]);
	}
	if(type=='new' && ids){
		estate = 1;
		id = ids;
	}
	if(type=='flat' && ids){
		estate = 2;
		id = ids;
	}
	if(type=='room' && ids){
		estate = 3;
		id = ids;
	}
	if(type=='country' && ids){
		estate = 4;
		id = ids;
	}
	if(type=='commercial' && ids){
		estate = 5;
		id = ids;
	}
	if(type=='article' && ids){
		estate = 7;
		id = ids;
	}
	$.ajax({
		type: "POST",
		url: '/include/handler.php',
		data: "addFavorite=true&id="+id+"&estate="+estate+"&build="+build,
		dataType: "json",
		success: function (data, textStatus) {
			var json = eval(data);
			if(json.user!==null){
				if(json.activation==1){
					self.find('span').text('В избранном');
					self.addClass('at');
				}
				else {
					self.find('span').text('В избранное');
					self.removeClass('at');
				}
			}
			else {
				alert('Необходимо зарегистрироваться на сайте');
			}
		}
	});
}

function tabsView(obj){
	var tab = $(obj);
	if(!tab.parent().is('.current')){
		var type = tab.data('type');
		tab.parent().parent().find('li').removeClass('current');
		tab.parent().addClass('current');
		tab.parent().parent().parent().parent().find('.items').removeClass('show');
		tab.parent().parent().parent().parent().find('.items.'+type).addClass('show');
	}
}

function removeMetro(obj){
	var $this = $(obj);
	$this.parents('.request_block').find('input[name="s[station]"],input[name="s[station_id]"]').removeAttr('value');
	$this.parents('ul.stations').empty();
}
function selectChangeStation(obj){
	var self = $(obj);
	self.parent().addClass('current');
	var val = self.data('values');
	var type = self.data('type');
	var text = self.text();
	var container = self.parents('.request_block');
	container.find('input[name="s[station_id]"]').val(type);
	container.find('input[name="s[station]').val(val);
	self.parent().parent().find('li').removeClass('current');
	self.parent().addClass('current');
	self.parents('.search_request_block').hide();
	container.find('ul.stations').empty().append('<li><div class="metro no_distance">«'+type+'»<span onclick="return removeMetro(this)" class="close"></span></div></li>');
	
	container.find('ul.stations').empty().append('<li><div class="metro">«'+type+'» / <strong></strong><span onclick="return removeMetro(this)" class="close"></span></div></li>');
	
	// getCoords($('#center .add_block .table_form input[name="s[address]"]'));
}

function selectChangeAmount(obj){
	var self = $(obj);
	self.parent().addClass('current');
	var val = self.data('values');
	var type = self.data('type');
	var text = self.text();
	var container = self.parents('.request_block');
	container.find('input[name="searchRequestId"]').val(val);
	container.find('input[name="typeSearch"]').val(type);
	container.find('input[name="searchRequest"]').val(text);
	self.parent().parent().find('li').removeClass('current');
	self.parent().addClass('current');
	self.parents('.search_request_block').hide();
}

function selectChangeAmount2(obj){
	var self = $(obj);
	self.parent().addClass('current');
	var val = self.data('values');
	var address = self.data('address');
	var coords = self.data('coords');
	var stationId = self.data('station');
	var stationName = self.data('station_name');
	var distance = self.data('distance');
	var dist = self.data('dist');
	var text = self.text();
	var container = self.parents('.request_block');
	var tableForm = self.parents('.table_form');
	var reload = self.parents('.reload_block');
	var stations = tableForm.find('input[name="s[station_id]"][type="text"]');
	container.find('input[name="s[build]"][type="hidden"]').val(val);
	container.find('input[name="s[build_text]"][type="text"]').val(text);
	stations.val(stationName);
	reload.find('input[name="s[station]"][type="hidden"]').val(stationId);
	reload.find('input[name="s[station_id]"][type="hidden"]').val(stationName);
	reload.find('input[name="s[dist_value]"][type="hidden"]').val(dist);
	tableForm.find('input[name="s[address]"][type="text"]').val(address);
	
	stations.parent().find('ul.stations').empty().append('<li><div class="metro">«'+stationName+'» / <strong>'+distance+'</strong><span onclick="return removeMetro(this)" class="close"></span></div></li>');
	
	// getCoords($('#center .add_block .table_form input[name="s[address]"]'));

	self.parent().parent().find('li').removeClass('current');
	self.parent().addClass('current');
	self.parents('.search_request_block').hide();
	
	var ex_coord = coords.split(' ');
	$('#map').empty();
	
	ymaps.ready(function () {
		var myMap = new ymaps.Map('map', {
			center: [ex_coord[1],ex_coord[0]],
			zoom: 14
		}, {
			searchControlProvider: 'yandex#search'
		}),
		myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
			hintContent: 'Метка на карте',
			balloonContent: ''
		}, {
			// Опции.
			// Необходимо указать данный тип макета.
			iconLayout: 'default#image',
			// Своё изображение иконки метки.
			iconImageHref: '/images/neitralIcon.png',
			// Размеры метки.
			iconImageSize: [52, 44],
			// Смещение левого верхнего угла иконки относительно
			// её "ножки" (точки привязки).
			iconImageOffset: [-3, -42]
		});

		myMap.geoObjects.add(myPlacemark);
	});
}

function selectChangeAmountRailway(obj){
	var self = $(obj);
	self.parent().parent().find('li').removeClass('current');
	var val = self.data('values');
	var id = val;
	var address = self.data('address');
	var railwayName = self.data('type');
	var distance = self.data('distance');
	var text = self.text();
	var container = self.parents('.request_block');
	var tableForm = self.parents('.filter_local');
	var stations = tableForm.find('input[name="railway"][type="hidden"]');
	var filter_local = self.parents('.filter_local');
	var countValue = filter_local.find('ul.railways li a[data-id="'+id+'"]').length;
	stations.val(val);
	container.find('input[name="s[build_text]"][type="text"]').val(text);
	
	if(stations.parent().find('ul.railways').find('li').length==0){
		stations.parent().find('ul.railways').empty()
	}
	if(!stations.parent().find('ul.railways').find('li a[data-id="'+val+'"]').length>0){
		stations.parent().find('ul.railways').append('<li><a data-id="'+val+'" href="javascript:void(0)">«'+railwayName+'»<span onclick="return removeRailway(this)" class="close"></span></a></li>');
	}
	
	var type = filter_local.data('type');
	var tags = self.data('tags');
	if(countValue>0){
		if(tags!==undefined){
			$('#filter_search .selected_params ul li a[data-name="'+type+'"][data-id="'+id+'"]').parent().remove();
			filter_local.find('ul.railways li a[data-id="'+id+'"]').parent().remove();
		}
	}
	else {
		self.parent().addClass('current');
		if(tags!==undefined){
			$('#filter_search .selected_params ul li a[data-name="'+type+'"][data-id="'+id+'"]').parent().remove();
			$('#filter_search .selected_params ul').append('<li><a data-name="'+type+'" data-type="list_areas" data-id="'+id+'" href="javascript:void(0)">'+tags+'<span onclick="return delTags(this)" class="close"></span></a></li>');
		}
	}
	var pars = filter_local;
	var CheckedIDs = [];
	pars.find('ul.railways li').each(function(){
		id = $(this).children('a').data('id');
		if($.inArray(id,CheckedIDs)==-1){
			CheckedIDs.push(id);
		}
	});
	filter_local.find('input[type="hidden"]').val(CheckedIDs);
	self.parents('.request_block.railway').find('input[type="text"]').removeAttr('value');
	
	self.parents('.search_request_block').hide();
}

function removeRailway(obj){
	var self = $(obj);
	var filter_local = self.parents('.filter_local');
	var type = filter_local.data('type');
	var id = self.parent().data('id');
	self.parent().parent().remove();
	$('#filter_search .selected_params ul li a[data-name="'+type+'"][data-id="'+id+'"]').parent().remove();
	var CheckedIDs = [];
	filter_local.find('ul.railways li').each(function(){
		id = $(this).children('a').data('id');
		if($.inArray(id,CheckedIDs)==-1){
			CheckedIDs.push(id);
		}
	});
	filter_local.find('input[type="hidden"]').val(CheckedIDs);
	self.parents('.search_request_block').hide();
	if(arrLinkPage){
		loadRequestUnreload();
	}
}

function selectChangeAmountAddress(obj){
	var self = $(obj);
	self.parent().addClass('current');
	var val = self.data('values');
	var build = self.data('build');
	var coords = self.data('coords');
	var buildText = self.data('build_text');
	var stationId = self.data('station');
	var stationName = self.data('station_name');
	var distance = self.data('distance');
	var loc = self.data('location');
	var area = self.data('area');
	var locality = self.data('locality');
	var text = self.text();
	var container = self.parents('.request_block');
	var tableForm = self.parents('.table_form');
	var stations = $('input[name="s[station_id]"][type="text"]');
	var stationsHidden = $('input[name="s[station_id]"][type="hidden"]');
	var stationsId = $('input[name="s[station]"][type="hidden"]');
	var coordsId = $('input[name="s[coords]"][type="hidden"]');
	container.find('input[name="s[address]"][type="text"]').val(text);
	if(stationName===undefined){
		stations.val('');
		stationsHidden.val('');
	}
	if(stationName!==undefined){
		stations.val(stationName);
	}
	if(stationId===undefined){
		stationsId.val('');
	}
	if(stationId!==undefined){
		stationsId.val(stationId);
	}
	var coords2 = coords.split(' ');
	coordsId.val(coords);
	if(buildText!==undefined){
		tableForm.find('input[name="s[build_text]"][type="text"]').val(buildText);
	}
	if(build!==undefined){
		tableForm.find('input[name="s[build]"][type="hidden"]').val(build);
	}
	stations.parent().find('ul.stations').empty().append('<li><div class="metro">«'+stationName+'» / <strong>'+distance+'</strong><span onclick="return removeMetro(this)" class="close"></span></div></li>');
	// getCoords($('#center .add_block .table_form input[name="s[address]"]'));
	self.parent().parent().find('li').removeClass('current');
	self.parent().addClass('current');
	self.parents('.search_request_block').hide();
	var addressInput = $('#center .add_block .table_form input[name="s[address]"]');
	addressInput.attr('data-checked',true);
	chooseValue = true;
	if($('input[name="s[location_text]"]').length>0){
		$('input[name="s[location_text]"]').val(loc);
	}
	else {
		tableForm.append('<input type="hidden" name="s[location_text]" value="'+loc+'">');
	}
	
	if($('input[type="hidden"][name="type_commer"]').attr("disabled")===true){
		if(area!=''){
			tableForm.find('input[name="s[area_text]"]').remove();
			tableForm.append('<input type="hidden" name="s[area_text]" value="'+area+'">');		
		}
		else {
			tableForm.find('input[name="s[area_text]"]').remove();		
		}	
	}
	
	if($('input[type="hidden"][name="s[locality]"]').length>0){
		$('input[type="hidden"][name="s[locality]"]').val(locality);
	}
	
	var ex_coord = coords.split(' ');
	$('#map').empty();
	
	ymaps.ready(function () {
		var myMap = new ymaps.Map('map', {
			center: [ex_coord[1],ex_coord[0]],
			zoom: 14
		}, {
			searchControlProvider: 'yandex#search'
		}),
		myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
			hintContent: 'Метка на карте',
			balloonContent: ''
		}, {
			// Опции.
			// Необходимо указать данный тип макета.
			iconLayout: 'default#image',
			// Своё изображение иконки метки.
			iconImageHref: '/images/neitralIcon.png',
			// Размеры метки.
			iconImageSize: [52, 44],
			// Смещение левого верхнего угла иконки относительно
			// её "ножки" (точки привязки).
			iconImageOffset: [-3, -42]
		});

		myMap.geoObjects.add(myPlacemark);
		
		// alert(ex_coord);

		var coords = ex_coord[1]+','+ex_coord[0];
		if($('input[name="s[center_coords]"]').length>0){
			$('input[name="s[center_coords]"]').val('');
		}
		ymaps.geocode(coords, { kind: 'metro' }).then(function (res) {
			if (res.geoObjects.getLength()) {
				var m0 = res.geoObjects.get(0);
				var m0_coords = m0.geometry.getCoordinates();
				var name = m0.properties.get('name');
				
				var myRouter = ymaps.route([
				  name, {
					type: "viaPoint",                   
					point: text
				  },
				], {
				  mapStateAutoApply: true 
				});
				
				myRouter.then(function(route) {
					var points = route.getWayPoints();
					points.options.set('preset', 'islands#blackStretchyIcon');
					points.get(0).properties.set("iconContent", name);
					points.get(1).properties.set("iconContent", 'Объект');
					// Добавление маршрута на карту
					// myMap.geoObjects.add(route);
					var routeLength = route.getHumanLength().toString();
					var routeLength2 = route.getLength();
					$('.table_form .row .stations li .metro strong').html(routeLength);
					if($('input[name="s[center_coords]"]').length>0){
						$('input[name="s[center_coords]"]').val(m0_coords);
					}
					if($('input[name="s[dist_value]"]').length>0){
						$('input[name="s[dist_value]"]').val(routeLength2);
						var station = name.replace('метро ','');
						var arr = [];
						if(station=='Звездная' || station=='Звёздная'){
							arr = ['Звездная','Звёздная'];
						}
						else if(station=='Площадь Александра Невского - 1'){
							arr = ['Площадь А. Невского I'];
						}
						else if(station=='Площадь Александра Невского - 2'){
							arr = ['Площадь А. Невского II'];
						}
						else if(station=='Технологический институт-1'){
							arr = ['Технологический ин-т I'];
						}
						else if(station=='Технологический институт-2'){
							arr = ['Технологический ин-т II'];
						}
						else {
							arr.push(station);
						}
						for(var v=0; v<SearchParamStation.length; v++){
							if($.inArray(SearchParamStation[v]['name'],arr)>-1){
								$('input[name="s[station_id]"]').val(name);
								$('input[name="s[station]"]').val(SearchParamStation[v]['id']);
								break;
							}
						}
					}
				  },
				  // Обработка ошибки
				  function (error) {
					// alert("Возникла ошибка: " + error.message);
				  }
				)
			}
			else {
				if($('input[name="s[dist_value]"]').length>0){
					$('input[name="s[dist_value]"]').val('');						
				}
			}
		});
/* 		if($('input[name="s[location]"]').length>0 && $('input[name="s[location]"]').val()==''){
			ymaps.geocode(coords,{kind: 'locality'}).then(function(res){
				if (res.geoObjects.getLength()){
					var m0 = res.geoObjects.get(0);
					var m0_coords = m0.geometry.getCoordinates();
					var name = m0.properties.get('name');
					if($('input[name="s[location]"]').length>0){
						$('input[name="s[location]"]').val(name);
					}
				}
				else {
					if($('input[name="s[location]"]').length>0){
						$('input[name="s[location]"]').val('');						
					}
				}
			});
		}
 */	});
	// findElemSelectAddress(self);
}

var xhr5;
function sendParamSearch(obj){
	var btn = $(obj);
	var id,type;
	var searchRequest = $('.search_request_block');
	var idInput = btn.parent().find('input[type="hidden"][name="searchRequestId"]');
	var typeSearch = btn.parent().find('input[type="hidden"][name="typeSearch"]');
	var typeChoose = btn.parent().find('input[type="hidden"][name="typeChoose"]');
	if(idInput.length>0 && typeSearch.length>0){
		id = idInput.val();
		type = typeSearch.val();
		typeChooseID = typeChoose.val();
		searchRequest.find('li').removeClass('current');
		searchRequest.find('li a[data-values="'+id+'"][data-type="'+type+'"]').click();
		
		if(xhr5 && xhr5.readyState != 4){
			xhr5.abort();
		}
		xhr5 = $.ajax({
			type: "POST",
			url: '/include/handler.php',
			data: "typeChoose="+typeChooseID+"&typeSearch="+type+"&idInput="+id+"&rnd="+Math.random(),
			dataType: "json",
			success: function (data, textStatus) {
				var json = eval(data);
				if(json.action!='error'){
					if(json.linkRequest!=''){
						window.location = json.linkRequest;
					}
				}
				else {
					alert('Возникла системная ошибка! Попробуйте позднее.');
				}
			},
			error: function(){
				alert('Результат ошибки');
			}
		});
	}
}

var xhr;
function coordsChange(type,id,tags){
	if(xhr && xhr.readyState != 4){
		xhr.abort();
	}

	xhr = $.ajax({
		type: "POST",
		url: '/include/handler.php',
		data: "filter_local=true&type="+type+"&id_local="+id+"&rnd="+Math.random(),
		dataType: "json",
		success: function (data, textStatus) {
			var json = eval(data);
			if(json.action!='error'){
				if(type=='city'){
					var parentArea = $('#filter_search .filter_local.area');
					var parentStation = $('#filter_search .filter_local.station');
					if(tags!==undefined){
						var estate = $('#filter_search .simple_search .checkbox_block.estate .select_checkbox ul li.checked a').data('type');
						var mainType = $('#filter_search .simple_search .checkbox_block.types .select_checkbox ul li.checked a').data('type');
						// $('#filter_search .selected_params ul li a[data-name="city"]').parent().remove();
						// $('#filter_search .selected_params ul').append('<li><a data-name="city" data-type="list_areas" data-id="'+id+'" href="javascript:void(0)">'+tags+'<span onclick="return delTags(this)" class="close"></span></a></li>');
						
						/*FOLLOW*/
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name="area"]').parent().remove();
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name="station"]').parent().remove();
					}
					/*FOLLOW*/
					if(json.areas_count>0){
						parentArea.find('.list_areas').html(json.areas);
						parentArea.show().find('input[type="hidden"]').attr("disabled",false);
						parentArea.removeClass('none');
						parentArea.find('i.not_active').remove();
					}
					else {
						parentArea.find('.list_areas').empty();
						parentArea.find('input[type="hidden"]').attr("disabled",true);
						parentArea.addClass('none').append('<i class="not_active"></i>');
					}
					if(json.stations_count>0){
						parentStation.find('.list_areas').html(json.stations);
						parentStation.show().find('input[type="hidden"]').attr("disabled",false);
						parentStation.removeClass('none');
						parentStation.find('i.not_active').remove();
					}
					else {
						parentStation.find('.list_areas').empty();
						parentStation.find('input[type="hidden"]').attr("disabled",true);
						parentStation.addClass('none').append('<i class="not_active"></i>');
					}
					if(id==1){
						$('#filter_search .location_change .filter_local .hidden_block .inner .tabs_view').show();
						$('#filter_search .location_change .filter_local .hidden_block .inner .items.list').removeClass('show').addClass('hide');
						$('#filter_search .location_change .filter_local .hidden_block .inner .items.map').addClass('show').removeClass('hide');
					}
					else {
						$('#filter_search .location_change .filter_local .hidden_block .inner .tabs_view').hide();
						$('#filter_search .location_change .filter_local .hidden_block .inner .items.list').addClass('show').removeClass('hide').find('.list_areas').width("");
						$('#filter_search .location_change .filter_local .hidden_block .inner .items.map').removeClass('show').addClass('hide');
					}
				}
				list_areas();
			}
			else {
				alert('Возникла системная ошибка! Попробуйте позднее.');
			}
		}
	});
}

function list_areas(){
	$('.list_areas ul li a').off('click');
	$('.list_areas ul li a').on('click',function(){
		var estate = $('#filter_search .simple_search .checkbox_block.estate .select_checkbox ul li.checked a').data('type');
		var mainType = $('#filter_search .simple_search .checkbox_block.types .select_checkbox ul li.checked a').data('type');
		var parent = $(this).parent().parent().parent();
		var self = $(this);
		var id = $(this).data('id');
		var text = self.find('span').text();
		var filter_local = self.parents('.filter_local');
		var type = filter_local.data('type');
		var tags = $(this).data('tags');
		if(parent.is('.one_value')){
			$('body').height('');
			self.parents('.hidden_block').hide();
			self.parents('.filter_local').find('a span.text').text(text);
			$(this).parent().parent().parent().find('li.current').removeClass('current');
			$(this).parent().addClass('current');
			self.parents('.filter_local').find('input[type="hidden"]').val(id);
			var s = $('#filter_search .location_change .filter_local.station');
			if(id==2){
				s.hide();
				s.find('input[type="hidden"]').attr('disabled',true);
			$('#filter_search .advanced_search .select_block input[name="far_subway"]').attr('disabled',true).parents('.cell').css({'visibility':'hidden'});
			}
			else {
				s.show();
				s.find('input[type="hidden"]').attr('disabled',false);
				$('#filter_search .advanced_search .select_block input[name="far_subway"]').attr('disabled',false).parents('.cell').css({'visibility':'visible'});
			}
			// coordsChange(type,id,tags);
			// if(arrLinkPage){
				// loadRequestUnreload();
			// }
		}
		else {
			if($(this).parent().is('.current')){
				$(this).parent().removeClass('current');
				var map = $(this).parent().parent().parent().parent().parent().find('.items.map');
				map.find('.metro_stations .list_stations a[data-id="'+id+'"]').css({"background-image":"none"}).parent().removeClass('current');
				if(tags!==undefined){
					if(filter_local.data('type')=='build' || filter_local.data('type')=='developer'){
						$('#filter_search .selected_params.cession.sell ul li a[data-name="'+type+'"][data-id="'+id+'"]').parent().remove();
						$('#filter_search .selected_params.new.sell ul li a[data-name="'+type+'"][data-id="'+id+'"]').parent().remove();
					}
					else {
						$('#filter_search .selected_params ul li a[data-name="'+type+'"][data-id="'+id+'"]').parent().remove();
					}
				}
			}
			else {
				$(this).parent().addClass('current');
				var map = $(this).parent().parent().parent().parent().parent().find('.items.map');
				map.find('.metro_stations .list_stations a[data-id="'+id+'"]').css({"background-image":'url("/images/Metro_map_hover.png")'}).parent().addClass('current');
				if(tags!==undefined){
					$('#filter_search .selected_params ul li a[data-name="'+type+'"][data-id="'+id+'"]').parent().remove();
					if(filter_local.data('type')=='build' || filter_local.data('type')=='developer'){
						$('#filter_search .selected_params.cession.sell ul').append('<li><a data-name="'+type+'" data-type="list_areas" data-id="'+id+'" href="javascript:void(0)">'+tags+'<span onclick="return delTags(this)" class="close"></span></a></li>');
						$('#filter_search .selected_params.new.sell ul').append('<li><a data-name="'+type+'" data-type="list_areas" data-id="'+id+'" href="javascript:void(0)">'+tags+'<span onclick="return delTags(this)" class="close"></span></a></li>');
					}
					else {
						$('#filter_search .selected_params ul').append('<li><a data-name="'+type+'" data-type="list_areas" data-id="'+id+'" href="javascript:void(0)">'+tags+'<span onclick="return delTags(this)" class="close"></span></a></li>');
					}
				}
			}
			var pars = filter_local;
			if(type=='stations'){
				pars = self.parents('.items');
			}
			var CheckedIDs = [];
			pars.find('.current').each(function(){
				id = $(this).children('a').data('id');
				if($.inArray(id,CheckedIDs)==-1){
					CheckedIDs.push(id);
				}
			});
			filter_local.find('input[type="hidden"]').val(CheckedIDs);
		}
	});
}

function periodChange(obj,id){
	var par = $(obj).parents('.inner_block');
	var clickId = '';
	var name = ''
	if(id==1 || id==2){
		clickId = 'all';
	}
	if(id==3){
		clickId = 'night';
	}
	var openClassName = '';
	if(par.is('.flat')){
		openClassName = 'flat';
	}
	if(par.is('.room')){
		openClassName = 'room';
	}
	if(par.is('.country')){
		openClassName = 'country';
	}
	if(par.is('.commercial')){
		openClassName = 'commercial';
	}
	var periodHidden = $('#filter_search .simple_search .inner_block .row .price_block.rent.'+openClassName+' input[type="hidden"][name="priceAll"]');
	if(id==1 || id==2 || id==3){
		periodHidden.parent().find('a').parent().removeClass('current');
		periodHidden.parent().find('a[data-id="'+clickId+'"]').parent().addClass('current');
		name = periodHidden.parent().find('a[data-id="'+clickId+'"]').text();
		periodHidden.parent().find('.choosed_block').text(name);
		periodHidden.val(clickId);
	}
}

function priceChange(obj,id){
	var par = $(obj).parents('.price_block');
	clickerParamPrice = true;
	clickerParamPeriod = false;
	var clickId = 1;
	var name = ''
	if(id=='night'){
		clickId = 3;
	}
	var openClassName = '';
	if(par.is('.flat')){
		openClassName = 'flat';
	}
	if(par.is('.room')){
		openClassName = 'room';
	}
	if(par.is('.country')){
		openClassName = 'country';
	}
	// var periodHidden = $('#filter_search .advanced_search .inner_block.'+openClassName+' input[type="hidden"][name="period"]');
	// name = periodHidden.parent('a[data-id="'+clickId+'"]').text();
	// periodHidden.parent().find('.choosed_block').text(name);
	// periodHidden.val(clickId);
	if(openClassName!=''){
		$('#filter_search .advanced_search .inner_block.'+openClassName+' input[type="hidden"][name="period"]').parent().find('.scroll-wrapper ul li a[data-id="'+clickId+'"]').click();
	}
}

function sendForma(type){
	var template = 'problem_block';
	if(type=='build'){
		template = 'build_block';
	}
	if(type=='address'){
		template = 'address_block';
	}
	var top = 0;
	var curInner = $('body').find('.window:last');
	top = ($(window).height() - 175) / 2 - $('#page').offset().top + $(window).scrollTop();
	if (top < 10) {
		top = 10;
	}
	$('#popup_blocks .feedback_form').removeClass('show');
	$('#popup_blocks .feedback_form.'+template+' .send_request').css({
		'top': top,
	}).addClass('show');
	$('#popup_blocks .feedback_form.'+template).addClass('show');
	$('#popup_blocks .feedback_form .fone_block').click(function(){
		$(this).parent().removeClass('show');
	});
}
function initiallChecked(){
	// Типы переключения меню
	var typeEstate = ['new','flat','room','country','commercial','cession'];
	var typeFind = ['sell','rent'];

	$('.checkbox_block ul li a').click(function(){
		var checkbox = $(this).parent().parent().parent().parent();
		var type_check = $(this).parent().parent().parent();
		var id = $(this).data('id');
		var type = $(this).data('type');
		var tags = $(this).data('tags');
		var rentArray = [4,6,8];
		var sellArray = [1,2,3,5,7];
		var arrays = {
			1:{'sell':1},
			2:{'sell':3,'rent':4},
			3:{'sell':5,'rent':6},
			4:{'sell':7,'rent':8},
			5:{'sell':9,'rent':10},
			6:{'sell':2},
		};
		var typeChoose = $('input[name="typeChoose"]');
		if(type!==undefined){
			/*
			*  Изменение блока advanced_search
			*/
			if($.inArray(type,typeEstate)!=-1){
				var mainType = $('#filter_search .simple_search .checkbox_block.types .select_checkbox ul li.checked a').data('type');
				var parentAdvSearch = $('#filter_search .advanced_search');
				var parentSimSearch = $('#filter_search .simple_search .inner_block');
				parentSimSearch.find('.row').not('.row:first').hide();
				parentSimSearch.find('.row.'+type).show();
				if(type!='new' && type!='cession'){
					if(type=='country'){
						$('#just_build').hide().find('input[type="hidden"]').attr('disabled',true);
						$('#just_country').show().find('input[type="hidden"]').attr('disabled',false);
					}
					else {
						$('#filter_search .row .checkbox_block.types li a[data-id="rent"]').parent().removeClass('none').find('.not_active').remove();
						$('#just_build').hide().find('input[type="hidden"]').attr('disabled',true);
						$('#just_country').hide().find('input[type="hidden"]').attr('disabled',true);
					}
					if(type=='country' || type=='commercial'){
						var type_param = $('#filter_search input[name="type_country"]').val();
						var typeParams = '.type_country_';
						var typeClass = '.count_show_param';
						if(type=='commercial'){
							type_param = $('#filter_search input[name="type_commer"]').val();
							typeClass = '.comm_show_param';
							typeParams = '.type_commer_';
						}
						var typeProc = $('#filter_search input[name="type"]');
						var type2 = typeProc.parent().find('.select_checkbox li.checked a').data("type");
						if(typeProc.val()=='sell'){// Покупка
							$('#filter_search .advanced_search .inner_block .separate').hide();
						}
						else { // Аренда
							$('#filter_search .advanced_search .inner_block .separate').show();
						}
						$('#filter_search .advanced_search .inner_block .row.'+type2+typeClass).addClass('hidden');
						$('#filter_search .advanced_search .inner_block .row.'+type2+typeClass+typeParams+type_param).removeClass('hidden');
					}
				}
				else {
					var rent = $('#filter_search .row .checkbox_block.types li a[data-id="rent"]');
					var sell = $('#filter_search .row .checkbox_block.types li a[data-id="sell"]');
					var flat = $('#filter_search .row .checkbox_block.estate li a[data-type="flat"]');
					$('#just_build').show().find('input[type="hidden"]').attr('disabled',false);
					$('#just_country').hide().find('input[type="hidden"]').attr('disabled',true);
					if(rent.parent().is('.checked')){
						setTimeout(function() {
							// flat.click();
							sell.click();
						}, 10);
					}
					// rent.parent().addClass('none').append('<i class="not_active"></i>');
				}
				$('#filter_search').removeClass('commercial').removeClass('country');
				// $('#filter_search .city.filter_local').hide().find('input[type="hidden"]').attr('disabled',true);
				if(type=='commercial' || type=='country'){
					$('#filter_search .city.filter_local').show().find('input[type="hidden"]').attr('disabled',false);
				}
				if(type=='commercial'){
					$('#filter_search').addClass('commercial');
					$('#filter_search .simple_search .inner_block .row.new.flat.room.country.commercial.cession.margin').css("margin-left","560px");
					$('#filter_search .advance_open').css("margin-left","45px");
					$('#filter_search .simple_search .inner_block .location_change .area.filter_local').css("margin-left","");
					
					$('#filter_search .advanced_search .inner_block .row.'+mainType+' .comm_show_param').addClass('none').append('<i class="not_active"></i>').find('input[type="hidden"]').attr('disabled',true).removeAttr('value');
					$('#filter_search .advanced_search .inner_block .row.'+mainType+' .comm_show_param.type_commer_'+id).each(function(){
						$(this).removeClass('none').find('.not_active').remove();
						$(this).find('input[type="hidden"]').removeAttr('disabled');
					});
					
				}
				else if(type=='new'){
					$('#filter_search .simple_search .inner_block .row.new.flat.room.country.commercial.cession.margin').css("margin-left","375px");
					// $('#filter_search .advance_open').css("margin-left","30px");
					$('#filter_search .simple_search .inner_block .location_change .area.filter_local').css("margin-left","");
				}
				else if(type=='flat' || type=='room'){
					$('#filter_search .simple_search .inner_block .row.new.flat.room.country.commercial.cession.margin').css("margin-left","375px");
					$('#filter_search .advance_open').css("margin-left","");
					$('#filter_search .simple_search .inner_block .location_change .area.filter_local').css("margin-left","");
				}
				else if(type=='country'){
					$('#filter_search').addClass('country');
					$('#filter_search .simple_search .inner_block .row.new.flat.room.country.commercial.cession.margin').css("margin-left","450px");
					$('#filter_search .simple_search .inner_block .location_change .area.filter_local').css("margin-left","");
				}
				else {
					$('#filter_search .simple_search .inner_block .row.new.flat.room.country.commercial.cession.margin').css("margin-left","");
					$('#filter_search .advance_open').css("margin-left","");
					$('#filter_search .simple_search .inner_block .location_change .area.filter_local').css("margin-left","");
					$('#filter_search .location_change').css("","");
				}
				$('#filter_search .location_change').removeClass('country').removeClass('new').removeClass('flat').removeClass('room').addClass(type);
				parentSimSearch.find('.change_block_filter').hide();
				parentSimSearch.find('.change_block_filter.'+type+'.'+mainType).show();
				
				parentAdvSearch.find('.inner_block').hide();
				parentAdvSearch.find('.inner_block.'+type).show();
				
				$('#filter_search .simple_search .price_block').hide().find('input[type="hidden"]').attr('disabled',true);
				$('#filter_search .simple_search .price_block.'+type+'.'+mainType).show().find('input[type="hidden"]').attr('disabled',false);
				var priceAll = $('#filter_search .simple_search .price_block.'+type+'.'+mainType+' input[type="hidden"][name="priceAll"]').val();
				if(type=='new' && mainType=='rent'){
					mainType = 'sell';
				}
				if(type=='cession' && mainType=='rent'){
					mainType = 'sell';
				}
				$('#filter_search .advance_open').removeClass('new').removeClass('cession').removeClass('flat').removeClass('room').removeClass('country').removeClass('commercial').removeClass('rent').removeClass('sell').addClass(mainType).addClass(type);
				$('#filter_search .simple_search .price_block.'+type+'.'+mainType+' .prices_choose').hide();
				$('#filter_search .simple_search .price_block.'+type+'.'+mainType+' .prices_choose.'+priceAll).show();
				$('#filter_search .selected_params').hide();
				$('#filter_search .selected_params.'+type+'.'+mainType).show();
				var typeChooseId = arrays[id][mainType];
				typeChoose.val(typeChooseId);
				typeChoose.parent().find('li').removeClass('current');
				typeChoose.parent().find('li a[data-id="'+typeChooseId+'"]').parent().addClass('current');
				typeChoose.parent().find('.choosed_block').text(typeChoose.parent().find('li a[data-id="'+typeChooseId+'"]').text());				
			}
			if($.inArray(type,typeFind)!=-1){
				$('#filter_search .advanced_search .inner_block .row').hide();
				$('#filter_search .advanced_search .inner_block .row.'+type).show();
				$('#just_build').hide().find('input[type="hidden"]').attr('disabled',true);
				var newBuild = $('#filter_search .row .checkbox_block.estate li a[data-type="new"]');
				var cession = $('#filter_search .row .checkbox_block.estate li a[data-type="cession"]');
				// if(type=='rent'){
					// newBuild.parent().addClass('none').append('<i class="not_active"></i>');
					// cession.parent().addClass('none').append('<i class="not_active"></i>');
				// }
				// else {
					// newBuild.parent().removeClass('none').find('i.not_active').remove();
					// cession.parent().removeClass('none').find('i.not_active').remove();
				// }
				var type2 = $('#filter_search .simple_search .checkbox_block.estate .select_checkbox ul li.checked a').data('type');
				// $('#filter_search .city.filter_local').hide().find('input[type="hidden"]').attr('disabled',true);
				if(type2=='commercial' || type2=='country'){
					$('#filter_search .city.filter_local').show().find('input[type="hidden"]').attr('disabled',false);
				}
				if(type2=='commercial'){
					var mainType = $('#filter_search .simple_search .checkbox_block.types .select_checkbox ul li.checked a').data('type');
					$('#filter_search').addClass('commercial');
					$('#filter_search .simple_search .inner_block .row.new.flat.room.country.commercial.cession.margin').css("margin-left","560px");
					$('#filter_search .advance_open').css("margin-left","45px");
				}
				else if(type2=='country'){
					$('#filter_search').addClass('country');
					$('#filter_search .simple_search .inner_block .row.new.flat.room.country.commercial.cession.margin').css("margin-left","450px");
				}
				else {
					$('#filter_search').removeClass('commercial');
					$('#filter_search .simple_search .inner_block .row.new.flat.room.country.commercial.cession.margin').css("margin-left","");
					$('#filter_search .advance_open').css("margin-left","");
					if(type2=='new' || type2=='cession'){
						if(type2=='new'){
							$('#filter_search .simple_search .inner_block .row.new.flat.room.country.commercial.cession.margin').css("margin-left","375px");
							// $('#filter_search .advance_open').css("margin-left","30px");
						}
						var flat = $('#filter_search .row .checkbox_block.estate li a[data-type="flat"]');
						var sell = $('#filter_search .row .checkbox_block.types li a[data-id="sell"]');
						var rent = $('#filter_search .row .checkbox_block.types li a[data-id="rent"]');
						$('#just_build').show().find('input[type="hidden"]').attr('disabled',false);
						// alert(type);
						// if(rent.parent().is('.checked')){
						if(type=='rent'){
							setTimeout(function() {
								flat.click();
							}, 10);
						}
					}
					if(type2=='flat'){
						$('#filter_search .simple_search .inner_block .row.new.flat.room.country.commercial.cession.margin').css("margin-left","375px");
					}
					if(type2=='room'){
						$('#filter_search .simple_search .inner_block .row.new.flat.room.country.commercial.cession.margin').css("margin-left","375px");
						// $('#filter_search .advance_open').css("margin-left","65px");
					}
				}
				$('#filter_search .advance_open').removeClass('new').removeClass('cession').removeClass('flat').removeClass('room').removeClass('country').removeClass('commercial').removeClass('rent').removeClass('sell').addClass(type2).addClass(type);
				$('#filter_search .change_block_filter').hide();
				$('#filter_search .change_block_filter.'+type+'.'+type2).show();
				if($.inArray(type,typeFind)==0){
					$('#filter_search .advanced_search .inner_block .separate').hide();
				}
				else {
					$('#filter_search .advanced_search .inner_block .separate').show();
				}
				$('#filter_search .simple_search .price_block').hide().find('input[type="hidden"]').attr('disabled',true);
				$('#filter_search .simple_search .price_block.'+type2+'.'+type).show().find('input[type="hidden"]').attr('disabled',false);
				var priceAll = $('#filter_search .simple_search .price_block.'+type+'.'+type2+' input[type="hidden"][name="priceAll"]').val();
				$('#filter_search .simple_search .price_block.'+type+'.'+type2+' .prices_choose').hide();
				$('#filter_search .simple_search .price_block.'+type+'.'+type2+' .prices_choose.'+priceAll).show();
				$('#filter_search .selected_params').hide();
				$('#filter_search .selected_params.'+type2+'.'+type).show();
				
				var ids = $('#filter_search .simple_search .checkbox_block.estate input[name="estate"]').val();
				var typeChooseId = arrays[ids][type];
				typeChoose.val(typeChooseId);
				typeChoose.parent().find('li').removeClass('current');
				typeChoose.parent().find('li a[data-id="'+typeChooseId+'"]').parent().addClass('current');
				typeChoose.parent().find('.choosed_block').text(typeChoose.parent().find('li a[data-id="'+typeChooseId+'"]').text());	

				
			}
			disabledInput();
		}
		
		if(!type_check.is('.block_checkbox') && !type_check.is('li')){
			if(!$(this).parent().is('.checked')){
				checkbox.find('li').removeClass('checked');
				checkbox.find('input[type="hidden"]').val(id);
				$(this).parent().addClass('checked');
			}
		}
		else {
			var estate = $('#filter_search .simple_search .checkbox_block.estate .select_checkbox ul li.checked a').data('type');
			var mainType = $('#filter_search .simple_search .checkbox_block.types .select_checkbox ul li.checked a').data('type');
			if(checkbox.is('ul')){
				checkbox = checkbox.parent().parent();
			}
			if($(this).parent().is('.checked')){
				$(this).parent().removeClass('checked');
				if(tags!==undefined){
					var name = checkbox.find('input[type="hidden"]').attr('name');
					$('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name="'+name+'"][data-id="'+id+'"]').parent().remove();
					if(name=='cession'){// Снимаю
						$('#filter_search .simple_search .price_block').hide().find('input[type="hidden"]').attr('disabled',true);
						$('#filter_search .simple_search .price_block.new.'+mainType).show().find('input[type="hidden"]').attr('disabled',false);
						$('#filter_search .simple_search .price_block.new.'+mainType).find('.prices_choose').hide().find('input[type="hidden"]').attr('disabled',true);
						$('#filter_search .simple_search .price_block.new.'+mainType).find('.prices_choose.all').show().find('input[type="hidden"]').attr('disabled',false);
					}
				}
				else {
					var name = checkbox.find('input[type="hidden"]').attr('name');
					if(name=='s[share]'){
						checkbox.find('.shares').addClass('none').find(':input').attr('disabled',true);
						checkbox.find('.hint').addClass('none');
					}
				}
				if(estate=='commercial'){
					// $('#filter_search .advanced_search .inner_block .row.'+mainType+' .comm_show_param').removeClass('none').find('.not_active').remove();
					// $('#filter_search .advanced_search .inner_block .row.'+mainType+' .comm_show_param').addClass('none').append('<i class="not_active"></i>').find('input[type="hidden"]').attr('disabled',true).removeAttr('value');
					// $('#filter_search .advanced_search .inner_block .row.'+mainType+' .comm_show_param.type_commer_'+id).each(function(){
						// $(this).removeClass('none').find('.not_active').remove();
					// });
				}
			}
			else {
				$(this).parent().addClass('checked');
				if(tags!==undefined){
					if(estate!='commercial' && estate!='country'){
						var name = checkbox.find('input[type="hidden"]').attr('name');
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name="'+name+'"][data-id="'+id+'"]').parent().remove();
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul').append('<li><a data-name="'+name+'" data-type="checkbox_select" data-id="'+id+'" href="javascript:void(0)">'+tags+'<span onclick="return delTags(this)" class="close"></span></a></li>');
						if(name=='cession'){
							$('#filter_search .simple_search .price_block').hide().find('input[type="hidden"]').attr('disabled',true);
							$('#filter_search .simple_search .price_block.'+name+'.'+mainType).show().find('input[type="hidden"]').attr('disabled',false);
						}
					}
				}
				else {
					var name = checkbox.find('input[type="hidden"]').attr('name');
					if(name=='s[share]'){
						checkbox.find('.shares').removeClass('none').find(':input').attr('disabled',false);
						checkbox.find('.hint').removeClass('none');
					}
				}
				if(estate=='commercial'){
					$('#filter_search .advanced_search .inner_block .row.'+mainType+' .comm_show_param').addClass('none').append('<i class="not_active"></i>').find('input[type="hidden"]').attr('disabled',true).removeAttr('value');
					$('#filter_search .advanced_search .inner_block .row.'+mainType+' .comm_show_param.type_commer_'+id).each(function(){
						$(this).removeClass('none').find('.not_active').remove();
						$(this).find('input[type="hidden"]').removeAttr('disabled');
					});
				}
				if(estate=='country'){
					$('#filter_search .advanced_search .inner_block .row.'+mainType+' .comm_show_param').addClass('none').append('<i class="not_active"></i>').find('input[type="hidden"]').attr('disabled',true).removeAttr('value');
					$('#filter_search .advanced_search .inner_block .row.'+mainType+' .comm_show_param.type_country_'+id).each(function(){
						$(this).removeClass('none').find('.not_active').remove();
						$(this).find('input[type="hidden"]').removeAttr('disabled');
					});
				}
			}
			if(estate!='commercial' && estate!='country'){
				var CheckedIDs = [];
				checkbox.find('li.checked').each(function(){
					id = $(this).children('a').data('id');
					CheckedIDs.push(id);
				});
				checkbox.find('input[type="hidden"]').val(CheckedIDs);
			}
			else {
				checkbox.find('li').removeClass('checked');
				$(this).parent().addClass('checked');
				checkbox.find('input[type="hidden"]').val(id);
				if(estate=='country'){
					if($(this).parent().find('ul').length>0){
						$(this).parent().find('ul li').removeClass('checked');
						var ids = parseInt($(this).parent().find('ul li:first a').data('id'));
						$(this).parent().find('ul li:first').addClass('checked');
						checkbox.find('input[type="hidden"]').val(ids);
					}
					else {
						if($(this).parent().parent().parent().is('li')){
							$(this).parent().parent().parent().addClass('checked');
						}
					}
				}
			}
		}
		if(arrLinkPage){
			loadRequestUnreload();
		}
		typeCommerce();
		typeCountry();
	});
}

function initiallSelected(sort){
	var cl = '';
	if(sort){
		cl = '.sort';
	}
	$('.select_block'+cl).off('click');
	$('.select_block'+cl).on('click', '.choosed_block', function(){
		$(this).parent().find('.scrollbar-inner').scrollbar();
		
		if($(this).parent().is('.open')){
			$(this).parent().removeClass('open');
		}
		else {
			$(this).parent().addClass('open');
		}
		$('.select_block'+cl+'').not($(this).parent()).removeClass('open');
		
		$(this).parent().find('.scroll-content').mouseover(function(){
			$(this).on( 'mousewheel DOMMouseScroll', function ( e ) {
				var delta = 0;
				var e0 = e.originalEvent,
					delta = e0.wheelDelta || -e0.detail;
				this.scrollTop += ( delta < 0 ? 1 : -1 ) * 10;
				e.preventDefault();
			});
		});
		
		$(this).parent().find('.scroll-content').mouseleave(function(){
			$(this).off( 'mousewheel DOMMouseScroll');
		});
		
		var name = $(this).parent().find('input[type="hidden"]').attr('name');
		var type = $(this).parent().find('li a:last').data('type');
		
		if(name=='priceMin' || name=='priceMax' || name=='priceMeterMin' || name=='priceMeterMax' || name=='priceNightMin' || name=='priceNightMax' || name=='squareFullMin' || name=='squareFullMax' || name=='squareKitchenMin' || name=='squareKitchenMax' || name=='squareLiveMin' || name=='squareLiveMax' || name=='squareRoomMin' || name=='squareRoomMax' || name=='floorMin' || name=='floorMax' || name=='squareLandMin' || name=='squareLandMax' || name=='far_subway'){
			$(this).find('.namePrice').hide();
			$(this).find('input[type="text"]').show().focus();
			$(this).parent().find('ul li').show();
		}
				
		if(type!==undefined){ // min/max
			if(type=='min'){
				var numArray = $.inArray(name,arrayParamSelectMin);
				var maxValue = arrayParamSelectMax[numArray];
				var inputMax = $('#filter_search form input[type="hidden"][name="'+maxValue+'"]:enabled');
				if(inputMax.length>1){
					$('#filter_search .advanced_search .inner_block').each(function(){
						if($(this).is(':visible')){
							inputMax = $(this).find('input[type="hidden"][name="'+maxValue+'"]:enabled');
						}
					});
				}
				var inputVal = inputMax.val();
				var parentBlock = $(this).parent();
				if(inputVal!=0){
					parentBlock.find('li').each(function(){
						var ids = $(this).find('a').data('id');
						if(inputVal<ids){
							$(this).addClass('disabled').append('<i class="not_active"></i>');
						}
						else {
							$(this).removeClass('disabled').find('i.not_active').remove();
						}
					});
				}
				else {
					parentBlock.find('li').each(function(){
						$(this).removeClass('disabled').find('i.not_active').remove();
					});
				}
			}
			else {
				var numArray = $.inArray(name,arrayParamSelectMax);
				var minValue = arrayParamSelectMin[numArray];
				var inputMin = $('#filter_search form input[type="hidden"][name="'+minValue+'"]:enabled');
				if(inputMin.length>1){
					$('#filter_search .advanced_search .inner_block').each(function(){
						if($(this).is(':visible')){
							inputMin = $(this).find('input[type="hidden"][name="'+minValue+'"]:enabled');
						}
					});
				}
				var inputVal = inputMin.val();
				var parentBlock = $(this).parent();
				if(inputVal!=0){
					parentBlock.find('li').each(function(){
						var ids = $(this).find('a').data('id');
						if(inputVal>ids){
							$(this).addClass('disabled').append('<i class="not_active"></i>');
						}
						else {
							$(this).removeClass('disabled').find('i.not_active').remove();
						}
					});
				}
				else {
					parentBlock.find('li').each(function(){
						$(this).removeClass('disabled').find('i.not_active').remove();
					});
				}
			}
		}
	});
	
	$('.select_block'+cl+' ul').off('click');
	$('.select_block'+cl+' ul').on('click', 'a', function(){
		var estate = $('#filter_search form input[type="hidden"][name="estate"]').parent().find('li.checked a').data('type');
		var mainType = $('#filter_search .simple_search .checkbox_block.types .select_checkbox ul li.checked a').data('type');
		var id = $(this).data('id');
		var tags = $(this).data('tags');
		var text = $(this).text();
		var select = $(this).parents('.select_block'+cl+'');
		var name = select.find('input[type="hidden"]').attr('name');
		var multiple = false;
		if(select.is('.multiple')){
			multiple = true;
		}
		if(!multiple){
			$(this).parent().parent().find('li.current').removeClass('current');
			$(this).parent().addClass('current');
		}
		else {
			if($(this).parent().is('.current')){
				$(this).parent().removeClass('current');
			}
			else {
				$(this).parent().addClass('current');
			}
		}
		if(!multiple){
			select.find('input[type="hidden"]').attr('disabled',false).val(id);
		}
		else {
			var idsArray = [];
			select.find('li.current').each(function(){
				var idChoose = parseInt($(this).find('a').data('id'));
				idsArray.push(idChoose);
			});
			select.find('input[type="hidden"]').attr('disabled',false).val(idsArray);
		}
		if(name=='priceMin' || name=='priceMax' || name=='priceMeterMin' || name=='priceMeterMax' || name=='priceNightMin' || name=='priceNightMax' || name=='squareFullMin' || name=='squareFullMax' || name=='squareKitchenMin' || name=='squareKitchenMax' || name=='squareLiveMin' || name=='squareLiveMax' || name=='squareRoomMin' || name=='squareRoomMax' || name=='floorMin' || name=='floorMax' || name=='squareLandMin' || name=='squareLandMax'){
			select.find('.choosed_block input[type="text"]').hide().val(id);
			if(select.find('.choosed_block .namePrice').length==0){
				select.find('.choosed_block').append('<span class="namePrice">'+text+'</span>');
				select.find('.choosed_block .namePrice').show();
			}
			else {
				select.find('.choosed_block .namePrice').text(text).show();
			}
		}
		else {
			if(!multiple){
				select.find('.choosed_block').text(text);
			}
			else {
				if(select.find('li.current').length==1){
					select.find('.choosed_block').text(select.find('li.current a').text());
				}
				else if(select.find('li.current').length>1) {
					var lengthChoose = select.find('li.current').length;
					select.find('.choosed_block').text(lengthChoose+' параметра');
				}
				else {
					select.find('.choosed_block').text('Не важно');
				}
			}
		}
		if(!multiple){
			select.removeClass('open');
		}
		var clickerParamPrice = false;
		var clickerParamPeriod = false;
		if(tags!==undefined){
			var type = $(this).data('type');
			var exp = $(this).data('explanation');
			var length = $('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name="'+name+'"][data-id="'+id+'"]').length;
			if(type!==undefined){ // min/max
				var nameSelect = $(this).data('name');
				if(type=='min'){
					length = $('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name_select="'+nameSelect+'"]').length;
					if(length==0){
						if(exp!==undefined){
							tags = exp+' '+tags;
						}
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name="'+name+'"][data-name_select="'+nameSelect+'"].min').parent().remove();
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul').append('<li><a class="min" data-name_select="'+nameSelect+'" data-name="'+name+'" data-type="checkbox_block" data-id="'+id+'" href="javascript:void(0)">'+tags+'<span onclick="return delTags(this)" class="close"></span></a></li>');
					}
					else {
						var tagMax = '';
						var numArray = $.inArray(name,arrayParamSelectMin);
						var maxValue = arrayParamSelectMax[numArray];
						var inputMax = $('#filter_search form input[type="hidden"][name="'+maxValue+'"]:enabled');
						if(inputMax.length>1){
							$('#filter_search .advanced_search .inner_block').each(function(){
								if($(this).is(':visible')){
									inputMax = $(this).find('input[type="hidden"][name="'+maxValue+'"]:enabled');
								}
							});
						}
						var inputVal = inputMax.val();
						var ids = 0;
						var exp = '';
						if(tagMax===undefined){
							tagMax = '';
						}
						if(inputVal!=0){
							tagMax = ' '+inputMax.parent().find('li:eq(1) a').data('tags');
							exp = inputMax.parent().find('li:eq(1) a').data('explanation');
							ids = inputMax.parent().find('li:eq(1) a').data('id');
						}
						if(maxValue=='priceMax' || maxValue=='priceMin' || maxValue=='priceMeterMin' || maxValue=='priceMeterMax' || maxValue=='priceNightMin' || maxValue=='priceNightMax'){
							tagMax = ' до '+number_format(inputVal,'','',' ');
						}
						else {
							tagMax = tagMax.replace(ids,inputVal);
						}
						if(maxValue=='priceMax' || maxValue=='priceMin' || maxValue=='priceMeterMin' || maxValue=='priceMeterMax' || maxValue=='priceNightMin' || maxValue=='priceNightMax' || maxValue=='squareFullMin' || maxValue=='squareFullMax' || maxValue=='squareKitchenMin' || maxValue=='squareKitchenMax' || maxValue=='squareLiveMin' || maxValue=='squareLiveMax' || maxValue=='squareRoomMin' || maxValue=='squareRoomMax' || maxValue=='floorMin' || maxValue=='floorMax' || maxValue=='squareLandMin' || maxValue=='squareLandMax' || maxValue=='far_subway'){
							if(inputMax.parents('.select_block').find('.namePrice').text()!=''){
								// tagMax = ' до '+inputMax.parents('.select_block').find('.namePrice').text();
								// tagMax = ' до '+number_format(inputMax.val(),'','',' ');
							}
						}
						if(exp!==undefined){
							tags = exp+' '+tags;
						}
						var container = tags+tagMax+'<span onclick="return delTags(this)" class="close"></span>';
						if(inputVal==0 || inputVal==''){
							container = tags+'<span onclick="return delTags(this)" class="close"></span>';
						}
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name_select="'+nameSelect+'"]').html(container).removeClass('min').addClass('min');
					}
				}
				if(type=='max'){
					length = $('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name_select="'+nameSelect+'"]').length;
					if(length==0){
						if(exp!==undefined){
							tags = exp+' '+tags;
						}
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name="'+name+'"][data-name_select="'+nameSelect+'"].max').parent().remove();
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul').append('<li><a class="max" data-name_select="'+nameSelect+'" data-name="'+name+'" data-type="checkbox_block" data-id="'+id+'" href="javascript:void(0)">'+tags+'<span onclick="return delTags(this)" class="close"></span></a></li>');
					}
					else {
						var tagMin = '';
						var numArray = $.inArray(name,arrayParamSelectMax);
						var minValue = arrayParamSelectMin[numArray];
						var inputMin = $('#filter_search form input[type="hidden"][name="'+minValue+'"]:enabled');
						if(inputMin.length>1){
							$('#filter_search .advanced_search .inner_block').each(function(){
								if($(this).is(':visible')){
									inputMin = $(this).find('input[type="hidden"][name="'+minValue+'"]:enabled');
								}
							});
						}
						var inputVal = inputMin.val();
						var ids = 0;
						var exp = '';
						if(inputVal!=0){
							tagMin = inputMin.parent().find('li:eq(1) a').data('tags')+' ';
							exp = inputMin.parent().find('li:eq(1) a').data('explanation');
							ids = inputMin.parent().find('li:eq(1) a').data('id');
						}
						if(minValue=='priceMax' || minValue=='priceMin' || minValue=='priceMeterMin' || minValue=='priceMeterMax' || minValue=='priceNightMin' || minValue=='priceNightMax'){
							tagMin = 'от '+number_format(inputVal,'','',' ');
						}
						else {
							tagMin = tagMin.replace(ids,inputVal);
						}
						if(minValue=='priceMax' || minValue=='priceMin' || minValue=='priceMeterMin' || minValue=='priceMeterMax' || minValue=='priceNightMin' || minValue=='priceNightMax' || minValue=='squareFullMin' || minValue=='squareFullMax' || minValue=='squareKitchenMin' || minValue=='squareKitchenMax' || minValue=='squareLiveMin' || minValue=='squareLiveMax' || minValue=='squareRoomMin' || minValue=='squareRoomMax' || minValue=='floorMin' || minValue=='floorMax' || minValue=='squareLandMin' || minValue=='squareLandMax' || minValue=='far_subway'){
							if(inputMin.parents('.select_block').find('.namePrice').text()!=''){
								// tagMin = 'от '+inputMin.parents('.select_block').find('.namePrice').text()+' ';
								// tagMin = 'от '+number_format(inputMin.val(),'','',' ')+' ';
							}
						}
						if(exp!==undefined){
							tagMin = exp+' '+tagMin+' ';
						}
						tagMin = tagMin+' ';
						var container = tagMin+tags+'<span onclick="return delTags(this)" class="close"></span>';
						if(inputVal==0 || inputVal==''){
							container = tags+'<span onclick="return delTags(this)" class="close"></span>';
						}
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name_select="'+nameSelect+'"]').html(container).removeClass('max').addClass('max');
					}
				}
				// $('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name="'+name+'"][data-id="'+id+'"]').parent().remove();
			}
			else {
				var nameSelect = $(this).data('name');
				if(nameSelect=='period'){
					periodChange($(this),id);
				}
				
				length = $('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name_select="'+nameSelect+'"]').length;
				if(exp!==undefined){
					tags = exp+' '+tags;
				}
				if(!multiple){
					$('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name="'+name+'"][data-name_select="'+nameSelect+'"].min').parent().remove();
					$('#filter_search .selected_params.'+estate+'.'+mainType+' ul').append('<li><a class="min" data-name_select="'+nameSelect+'" data-name="'+name+'" data-type="checkbox_block" data-id="'+id+'" href="javascript:void(0)">'+tags+'<span onclick="return delTags(this)" class="close"></span></a></li>');
				}
				else {
					if($('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name="'+name+'"][data-id="'+id+'"][data-name_select="'+nameSelect+'"].min').length>0){
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name="'+name+'"][data-id="'+id+'"][data-name_select="'+nameSelect+'"].min').parent().remove();
					}
					else {
						$('#filter_search .selected_params.'+estate+'.'+mainType+' ul').append('<li><a class="min" data-name_select="'+nameSelect+'" data-name="'+name+'" data-type="checkbox_block" data-multiple="true" data-id="'+id+'" href="javascript:void(0)">'+tags+'<span onclick="return delTags(this)" class="close"></span></a></li>');
					}
				}
			}
		}
		else {
			if(id==''){
				var nameSelect = $(this).data('name');
				$('#filter_search .selected_params.'+estate+'.'+mainType+' ul li a[data-name="'+name+'"].min').parent().remove();
			}
			if(id=='night' || id=='all'){
				priceChange($(this),id);
			}
		}
		
		if(name=='typeName' || name=='estateName'){
			if(name=='typeName'){
				var tableForm = $(this).parents('.table_form');
				var inputEstate = tableForm.find('input[name="estateName"]');
				var selectEstate = inputEstate.parents('.select_block');
				inputEstate.removeAttr('value');
				selectEstate.find('ul').empty();
				selectEstate.find('.choosed_block').text(estateArray[0][0]['name']);
				for(var a=0; a<estateArray[id].length; a++){
					var className = '';
					var notActive = '';
					if(estateArray[id][a]['status']=='disabled'){
						className = ' class="disabled"';
						notActive = '<i class="not_active"></i>';
					}
					var sw = '';
					if(estateArray[id][a]['switch']){
						sw = ' data-switch="'+estateArray[id][a]['switch']+'"';
					}
					selectEstate.find('ul').append('<li'+className+'><a href="javascript:void(0)"'+sw+' data-action="'+estateArray[id][a]['action']+'" data-id="'+estateArray[id][a]['value']+'">'+estateArray[id][a]['name']+'</a>'+notActive+'</li>');
				}
				specialForType();
			}
			if(name=='estateName'){
				var tableForm = $(this).parents('.table_form');
				var action = $(this).data('action');
				var inputType = tableForm.find('input[name="typeName"]');
				var selectType = inputType.parents('.select_block');
				var curInput = $(this).parents('.select_block').find('input[type="hidden"]');
				var curValue = curInput.val();
				var arrayEset = ["","new","flat","room","country","commercial","cession"];
				if(curValue==1 || curValue==6){
					var f = tableForm.find('.row.newbuildings');
					f.find(':input').attr('disabled',false);
					f.show();
				}
				else {
					var f = tableForm.find('.row.newbuildings');
					f.find(':input').attr('disabled',true);
					f.hide();
				}
				if(action){
					var switchName = $(this).data('switch');
					if(inputType.val()!=switchName){
						inputType.val(switchName);
						var cur = selectType.find('ul li a[data-id="'+switchName+'"]');
						selectType.find('ul li').removeClass('current');
						cur.parent().addClass('current');
						selectType.find('.choosed_block').text(cur.text());
					}
				}
			}
		}
		
		if(name=='estateNameFilter'){
			var tableFilter = $('table.result_search');
			var type = $(this).data('type'); // тип сделки
			var estate = $(this).data('estate');// тип недвижимости
			if(estate=='all'){
				tableFilter.find('tr[data-type="'+type+'"]').not('tr.header').show();
			}
			else if(estate=='hide'){
				tableFilter.find('tr[data-type="'+type+'"]').not('tr.header').hide();
			}
			else {
				tableFilter.find('tr').not('tr.header').hide();
				tableFilter.find('tr[data-type="'+type+'"][data-estate="'+estate+'"]').not('tr.header').show();
			}
			
			var thisSelect = $(this).parents('.select_choose').find('.select_block').index($(this).parents('.select_block'));
			var currentCellBlock = $(this).parents('.select_choose');
			currentCellBlock.find('.select_block').each(function(){
				var thisSelectCurrent = $(this).parents('.select_choose').find('.select_block').index(this);
				if(thisSelectCurrent!=thisSelect){
					$(this).find('.choosed_block').text('Скрыть все');	
				}
			});
		}
		if(name=='priceAll'){
			select.parent().find('.prices_choose').hide().find('input[type="hidden"]').attr('disabled',true);
			select.parent().find('.prices_choose.'+id).show().removeClass('nosearchpage').find('input[type="hidden"]').attr('disabled',false);
		}
		if(name=='s[location]'){
			if(id!=0){
				$(this).parents('.table_form').find('input[name="s[address]"]').attr('disabled',false);
			}
		}
		if(arrLinkPage){
			if(!select.is('.sort')){
				loadRequestUnreload();
			}
		}		
	});
}

function initiallSelected2(sort,reload){
	var cl = '';
	if(sort){
		cl = '.sort';
	}
	if(reload){
		$('.reload_block .select_block'+cl+' .choosed_block').off('click');
		$('.reload_block .select_block'+cl+' .choosed_block').on('click',function(){
			if($(this).parent().is('.open')){
				$(this).parent().removeClass('open');
			}
			else {
				$(this).parent().addClass('open');
			}
			$('.reload_block .select_block'+cl+'').not($(this).parent()).removeClass('open');
			
			$(this).parent().find('.scroll-content').mouseover(function(){
				$(this).on( 'mousewheel DOMMouseScroll', function ( e ) {
					var delta = 0;
					var e0 = e.originalEvent,
						delta = e0.wheelDelta || -e0.detail;
					this.scrollTop += ( delta < 0 ? 1 : -1 ) * 10;
					e.preventDefault();
				});
			});
			
			$(this).parent().find('.scroll-content').mouseleave(function(){
				$(this).off( 'mousewheel DOMMouseScroll');
			});
			
			var name = $(this).parent().find('input[type="hidden"]').attr('name');
			var type = $(this).parent().find('li a:last').data('type');
			
			if(name=='priceMin' || name=='priceMax' || name=='priceMeterMin' || name=='priceMeterMax' || name=='priceNightMin' || name=='priceNightMax' || name=='squareFullMin' || name=='squareFullMax' || name=='squareKitchenMin' || name=='squareKitchenMax' || name=='squareLiveMin' || name=='squareLiveMax' || name=='squareRoomMin' || name=='squareRoomMax' || name=='floorMin' || name=='floorMax' || name=='squareLandMin' || name=='squareLandMax' || name=='far_subway'){
				$(this).find('.namePrice').hide();
				$(this).find('input[type="text"]').show().focus();
				$(this).parent().find('ul li').show();
			}
		});
		
		$('.reload_block .select_block'+cl+' ul li a').off('click');
		$('.reload_block .select_block'+cl+' ul li a').click(function(){
			var estate = $('#filter_search form input[type="hidden"][name="estate"]').parent().find('li.checked a').data('type');
			var mainType = $('#filter_search .simple_search .checkbox_block.types .select_checkbox ul li.checked a').data('type');
			var id = $(this).data('id');
			var tags = $(this).data('tags');
			var text = $(this).text();
			var select = $(this).parents('.select_block'+cl+'');
			var name = select.find('input[type="hidden"]').attr('name');
			var multiple = false;
			if(select.is('.multiple')){
				multiple = true;
			}
			if(!multiple){
				$(this).parent().parent().find('li.current').removeClass('current');
				$(this).parent().addClass('current');
			}
			else {
				if($(this).parent().is('.current')){
					$(this).parent().removeClass('current');
				}
				else {
					$(this).parent().addClass('current');
				}
			}
			if(!multiple){
				select.find('input[type="hidden"]').attr('disabled',false).val(id);
			}
			else {
				var idsArray = [];
				select.find('li.current').each(function(){
					var idChoose = parseInt($(this).find('a').data('id'));
					idsArray.push(idChoose);
				});
				select.find('input[type="hidden"]').attr('disabled',false).val(idsArray);
			}
			if(name=='priceMin' || name=='priceMax' || name=='priceMeterMin' || name=='priceMeterMax' || name=='priceNightMin' || name=='priceNightMax' || name=='squareFullMin' || name=='squareFullMax' || name=='squareKitchenMin' || name=='squareKitchenMax' || name=='squareLiveMin' || name=='squareLiveMax' || name=='squareRoomMin' || name=='squareRoomMax' || name=='floorMin' || name=='floorMax' || name=='squareLandMin' || name=='squareLandMax' || name=='far_subway'){
				select.find('.choosed_block input[type="text"]').hide().val(id);
				if(select.find('.choosed_block .namePrice').length==0){
					select.find('.choosed_block').append('<span class="namePrice">'+text+'</span>');
					select.find('.choosed_block .namePrice').show();
				}
				else {
					select.find('.choosed_block .namePrice').text(text).show();
				}
			}
			else {
				if(!multiple){
					select.find('.choosed_block').text(text);
				}
				else {
					if(select.find('li.current').length==1){
						select.find('.choosed_block').text(select.find('li.current a').text());
					}
					else if(select.find('li.current').length>1) {
						var lengthChoose = select.find('li.current').length;
						select.find('.choosed_block').text(lengthChoose+' параметра');
					}
					else {
						select.find('.choosed_block').text('Не важно');
					}
				}
			}
			if(!multiple){
				select.removeClass('open');
			}
			if(name=='s[location]'){
				if(id!=0){
					$(this).parents('.table_form').find('input[name="s[address]"]').attr('disabled',false);
				}
			}
		});
	}
}

/*
*  Показ всех объявлений
*/
function showAll(place){
	var par = $(place).parents('.select_choose');
	par.find('.select_block').each(function(){
		$(this).find('ul li a[data-estate="all"]').click();
	});
}

/*
*  Удаление объявления
*/
function delAds(obj){
	var tr = $(obj).parents('tr');
	var id = 0;
	if(tr.data('id')!==undefined){
		id = tr.data('id');
	}
	tr.css({"opacity":"0.5"}); 
	jConfirm('Вы уверены, что хотите безвозвратно удалить данное объявление?', 'Удаление объявления',function(r){
		if(r===true){
			$.ajax({
				type: "POST",
				url: '/include/handler.php',
				data: "delAds=true&id="+id,
				dataType: "json",
				success: function (data, textStatus) {
					var json = eval(data);
					if(json.action!='error'){
						tr.remove();
					}
					else {
						jAlert(json.text, json.title,function(r){
							tr.css({"opacity":""});
						},'Закрыть');
					}
				}
			});
		}
		else {
			tr.css({"opacity":""});
		}
	},'Удалить','Отмена');
}

/*
*  Включить/отключить объявление
*/
function toggleAds(obj){
	var status = 0;
	var text = 'Отключить объявление? Оно не будет доступно при поиске';
	var btnName = 'Отключить';
	if($(obj).parent().find('.off').length>0){// Отключить
		status = 0;
	}
	else { // Включить
		status = 1;
		text = 'Включить объявление? Оно будет доступно при поиске';
		btnName = 'Включить';
	}
	var tr = $(obj).parents('tr');
	var id = 0;
	if(tr.data('id')!==undefined){
		id = tr.data('id');
	}
	tr.css({"opacity":"0.5"}); 
	jConfirm(text, '',function(r){
		if(r===true){
			$.ajax({
				type: "POST",
				url: '/include/handler.php',
				data: "toggleAds="+status+"&id="+id,
				dataType: "json",
				success: function (data, textStatus) {
					if(data!==null){
						var json = eval(data);
						if(json.action!='error'){
							var page = window.location;
							$('.add_list').load(page+' .result_search');
						}
						else {
							jAlert(json.text, json.title,function(r){
								tr.css({"opacity":""});
							},'Закрыть');
						}
					}
					else {
						jAlert('Произошла ошибка. Попробуйте повторить позднее', '',function(r){
							tr.css({"opacity":""});
						},'Закрыть');
					}
				},
			});
		}
		else {
			tr.css({"opacity":""});
		}
	},btnName,'Отмена');
}

function specialForType(){
	$('#center .add_block .table_form .select_block ul li a').click(function(){
		var id = $(this).data('id');
		var tags = $(this).data('tags');
		var text = $(this).text();
		var select = $(this).parents('.select_block');
		var name = select.find('input[type="hidden"]').attr('name');
		$(this).parent().parent().find('li.current').removeClass('current');
		$(this).parent().addClass('current');
		select.find('input[type="hidden"]').attr('disabled',false).val(id);
		select.find('.choosed_block').text(text);
		select.removeClass('open');
		
		if(name=='typeName' || name=='estateName'){
			if(name=='typeName'){
				var tableForm = $(this).parents('.table_form');
				var inputEstate = tableForm.find('input[name="estateName"]');
				var selectEstate = inputEstate.parents('.select_block');
				inputEstate.removeAttr('value');
				selectEstate.find('ul').empty();
				selectEstate.find('.choosed_block').text(estateArray[0][0]['name']);
				for(var a=0; a<estateArray[id].length; a++){
					var className = '';
					var notActive = '';
					if(estateArray[id][a]['status']=='disabled'){
						className = ' class="disabled"';
						notActive = '<i class="not_active"></i>';
					}
					var sw = '';
					if(estateArray[id][a]['switch']){
						sw = ' data-switch="'+estateArray[id][a]['switch']+'"';
					}
					selectEstate.find('ul').append('<li'+className+'><a href="javascript:void(0)"'+sw+' data-action="'+estateArray[id][a]['action']+'" data-id="'+estateArray[id][a]['value']+'">'+estateArray[id][a]['name']+'</a>'+notActive+'</li>');
				}
				specialForType();
			}
			if(name=='estateName'){
				var tableForm = $(this).parents('.table_form');
				var action = $(this).data('action');
				var inputType = tableForm.find('input[name="typeName"]');
				var selectType = inputType.parents('.select_block');
				var curInput = $(this).parents('.select_block').find('input[type="hidden"]');
				var curValue = curInput.val();
				var arrayEset = ["","new","flat","room","country","commercial","cession"];
				if(curValue==1 || curValue==6){
					var f = tableForm.find('.row.newbuildings');
					f.find(':input').attr('disabled',false);
					f.show();
				}
				else {
					var f = tableForm.find('.row.newbuildings');
					f.find(':input').attr('disabled',true);
					f.hide();
				}
				if(action){
					var switchName = $(this).data('switch');
					if(inputType.val()!=switchName){
						inputType.val(switchName);
						var cur = selectType.find('ul li a[data-id="'+switchName+'"]');
						selectType.find('ul li').removeClass('current');
						cur.parent().addClass('current');
						selectType.find('.choosed_block').text(cur.text());
					}
				}
			}
		}
	});
}


var xhr3;
// Получение координат по введеной улице
function getCoords(obj){
	var countryMap = false;
	var address = $('#center .add_block .table_form input[name="s[address]"]').val();
	if(address!=''){
		countryMap = true;
	}

	var data = "geolocation="+address;
	var map = '';
	if(xhr3 && xhr3.readyState != 4){
		xhr3.abort();
	}
	if(countryMap && address.length>2){
		xhr3 = $.ajax({
			type: 'POST',
			url: "/include/handler.php",  
			cache: false,
			data: data,
			dataType: "json",
			success: function(data){  
				var json = eval(data);
				if(json.length>0){

				}
				$('#map').empty();
				ymaps.ready(function () {
					var myMap = new ymaps.Map('map', {
						center: [json[1],json[0]],
						zoom: 14
					}, {
						searchControlProvider: 'yandex#search'
					}),
					myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
						hintContent: 'Метка на карте',
						balloonContent: ''
					}, {
						// Опции.
						// Необходимо указать данный тип макета.
						iconLayout: 'default#image',
						// Своё изображение иконки метки.
						iconImageHref: '/images/neitralIcon.png',
						// Размеры метки.
						iconImageSize: [52, 44],
						// Смещение левого верхнего угла иконки относительно
						// её "ножки" (точки привязки).
						iconImageOffset: [-3, -42]
					});

					myMap.geoObjects.add(myPlacemark);
					if($('input[name="s[station_id]"]').val!=''){
						var stationName = $('input[name="s[station_id]"]').val();
						var stationName2 = 'Санкт-Петербург, м. '+stationName;
					}
					var myRouter = ymaps.route([
					  stationName2, {
						type: "viaPoint",                   
						point: address
					  },
					], {
					  mapStateAutoApply: true 
					});
					
					myRouter.then(function(route) {
						/* Задание контента меток в начальной и 
						   конечной точках */
						var points = route.getWayPoints();
						points.options.set('preset', 'islands#blackStretchyIcon');
						points.get(0).properties.set("iconContent", stationName2);
						points.get(1).properties.set("iconContent", 'Объект');
						// Добавление маршрута на карту
						myMap.geoObjects.add(route);
						var routeLength = route.getHumanLength().toString();
						var routeLength2 = route.getLength();
						$('.table_form .row .stations li .metro strong').html(routeLength);
						if($('input[name="s[dist_value]"]').length>0){
							$('input[name="s[dist_value]"]').val(routeLength2);						
						}
						// $('<div class="full_route">Расстояние от <span>Санкт-Петербург, м. <?=$PageInfo['station_name']?></span> до <span><?=htmlspecialchars_decode($PageInfo['h1_jk'])?></span>: <strong>'+routeLength+'</strong></div>').insertAfter('#map');
						// alert(routeLength2);
					  },
					  // Обработка ошибки
					  function (error) {
						// alert("Возникла ошибка: " + error.message);
					  }
					)
				});
			}  
		});
	}
	return false;
}

function metro_stations(){
	$('.metro_stations .item a').click(function(){
		var estate = $('#filter_search .simple_search .checkbox_block.estate .select_checkbox ul li.checked a').data('type');
		var parent = $(this).parent().parent();
		var self = $(this);
		var id = $(this).data('id');
		var filter_local = self.parents('.filter_local');
		var type = filter_local.data('type');
		var pars = self.parents('.items');
		var tags = $(this).data('tags');
		if($(this).parent().is('.current')){
			pars.find('a[data-id="'+id+'"]').parent().removeClass('current');
			pars.find('a[data-id="'+id+'"]').css({"background-image":"none"});
			var list = $(this).parent().parent().parent().parent().parent().find('.items.list');
			list.find('.list_areas a[data-id="'+id+'"]').parent().removeClass('current');
			if(tags!==undefined){
				$('#filter_search .selected_params ul li a[data-name="'+type+'"][data-id="'+id+'"]').parent().remove();
			}
		}
		else {
			pars.find('a[data-id="'+id+'"]').parent().addClass('current');
			pars.find('a[data-id="'+id+'"]').css({"background-image":'url("/images/Metro_map_hover.png")'});
			var list = $(this).parent().parent().parent().parent().parent().find('.items.list');
			list.find('.list_areas a[data-id="'+id+'"]').parent().addClass('current');
			if(tags!==undefined){
				$('#filter_search .selected_params ul li a[data-name="'+type+'"][data-id="'+id+'"]').parent().remove();
				$('#filter_search .selected_params ul').append('<li><a data-name="'+type+'" data-type="list_areas" data-id="'+id+'" href="javascript:void(0)">'+tags+'<span onclick="return delTags(this)" class="close"></span></a></li>');
			}
		}
		var CheckedIDs = [];
		pars.find('.current').each(function(){
			id = $(this).children('a').data('id');
			if($.inArray(id,CheckedIDs)==-1){
				CheckedIDs.push(id);
			}
		});
		filter_local.find('input[type="hidden"]').val(CheckedIDs);
	});
}

function chooseShow(obj){
	var self = $(obj);
	// if(!self.parent().is('.current')){
		$('.btns-show .show .item').removeClass('current');
		$('.btns-show .show .item').removeClass('current');
		if(self.parent().is('.map')){
			$('.btns-show .show .item.map').addClass('current');
			$('.btns-show .show .item.map').addClass('current');
			var height = $(window).height();
			$('#big_map_full_page #map').height(height);
			$('#big_map_full_page').show();
			var iOffsetTop = $('#center').offset().top;
			$('html, body').animate({'scrollTop': iOffsetTop-75}, 500);
			$('#center').hide();
			$('html').css({"overflow":"hidden"});
			$('#result_search .table_items').hide();
			loadRequestUnreload2('map');
			installerMaps();
		}
		else if(self.parent().is('.table')){
			$('.btns-show .show .item.table').addClass('current');
			$('.btns-show .show .item.table').addClass('current');
			$('#big_map_full_page').hide();
			$('#center').show();
			$('html').css({"overflow":""});
			$('#big_map_full_page #map').empty();
			$('#result_search .list_items').hide();
			$('#result_search .table_items').show();
			loadRequestUnreload2('table');
		}
		else {
			$('.btns-show .show .item.list').addClass('current');
			$('.btns-show .show .item.list').addClass('current');
			$('#big_map_full_page').hide();
			$('#center').show();
			$('html').css({"overflow":""});
			$('#big_map_full_page #map').empty();
			$('#result_search .table_items').hide();
			$('#result_search .list_items').show();
			loadRequestUnreload2();
		}
	// }
}

function phoneShowClick(){
	$('.phone').click(function(){
		$(this).addClass('show');
	});
}

function chooseShow2(obj){
	var self = $(obj);
	if(self.parent().is('.table')){
		$('html').css({"overflow":""});
		$('#result_search .list_items').hide();
		$('#result_search .table_items').show();
	}
	else {
		$('html').css({"overflow":""});
		$('#result_search .table_items').hide();
		$('#result_search .list_items').show();
	}
}

function loadAvatarUser(){
	if($('#loadAvatarUser').length>0){
		var btnUpload=$('#loadAvatarUser');
		var status=$('#status');
		new AjaxUpload(btnUpload, {
			action: '/include/userAvatar.php',
			name: 'uploadfile[]',
			dataType: 'json',
			onSubmit: function(file, ext){
				 if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
					status.text('Only JPG, PNG or GIF files are allowed');
					return false;
				}
				status.html('<div class="load"><div class="center"></div></div>');
				$('#set_album_image').remove();
			},
			onComplete: function(file, response){
				status.text('');
				var json = eval(response);
				var lengthArrayMain = json.length; // Считаем общее количество загружаемых фотографий
				var img_par_list = $('#loadAvatarUser');									
				if(json[0][0]!='error'){
					var top = 0;
					$('.window:last').remove();
					$(document).keydown(function(eventObject){
						if (eventObject.which == 27) {
							return closeWin();
						}
					});
					$('body').append('<div class="window"><div onclick="return closeWin();" class="overlay"></div><div class="dialog loadAvatar"><span onclick="return closeWin();" class="closeDialog"></span><div class="inner"></div></div></div>');
					var curInner = $('body').find('.window:last');
					top = ($(window).height() - 175) / 2 - $('#page').offset().top + $(window).scrollTop();
					if (top < 10) {
						top = 10;
					}
					$('.window:last .dialog').css({
						'marginBottom':'20px'
					});
					var heightMain = $('.window:last');
					heightMain.find('.dialog .inner').css({ position: "absolute", visibility: "hidden", display: "block" });
					var link_img = '/users/'+json[0][1]+'/'+json[0][4];
					var width_avatar = 103;
					var height_avatar = 103;
					var page = '';
					heightMain.find('.dialog .inner').append('<form id="handlerForm"><div id="set_album_image"> \
						<input type="hidden" value="true" name="cutAvatar"> \
						<div class="parent_block'+page+'"> \
							<div class="big_img"> \
								<p class="title">Оригинал изображения</p> \
								<div class="original_image"> \
									<div class="img avatar"><img style="width:'+json[0][5]+'px;height:'+json[0][6]+'px" id="photo" src="'+link_img+'"></div> \
								</div> \
							</div> \
							<div class="middle_img"> \
								<p class="title">Миниатюра</p> \
								<div class="preview large"><img src="'+link_img+'"></div> \
								<input id="x1" type="hidden" value="'+json[0][7]+'" name="x1"> \
								<input id="y1" type="hidden" value="'+json[0][9]+'" name="y1"> \
								<input id="x2" type="hidden" value="'+json[0][8]+'" name="x2"> \
								<input id="y2" type="hidden" value="'+json[0][10]+'" name="y2"> \
								<input id="w" type="hidden" value="'+json[0][5]+'" name="w"> \
								<input id="h" type="hidden" value="'+json[0][10]+'" name="h"> \
							</div> \
						</div> \
					</div><ul class="actions"><li><input type="submit" value="Сохранить"></li><li><a class="btn" onclick="return closeWin();" href="javascript:void(0)"><span>Закрыть</span></a></li></ul></form>');
					// img_par_list.remove();
					function preview(img, selection) {
						var scaleX = width_avatar / selection.width;
						var scaleY = height_avatar / selection.height;
						$(".preview img").css({
							width: Math.round(scaleX * json[0][5])+"px",
							height: Math.round(scaleY * json[0][6])+"px",
							marginLeft: -Math.round(scaleX * selection.x1),
							marginTop: -Math.round(scaleY * selection.y1)
						});
						$("#x1").val(selection.x1);
						$("#y1").val(selection.y1);
						$("#x2").val(selection.x2);
						$("#y2").val(selection.y2);
						$("#w").val(selection.width);
						$("#h").val(selection.height);
					}

					// $("#photo").imgAreaSelect({ aspectRatio: ''+width_avatar+':'+height_avatar+'', handles: true, fadeSpeed: 200, onSelectChange: preview });
					
					setTimeout(function() {
						$("#photo").imgAreaSelect({ aspectRatio: ''+width_avatar+':'+height_avatar+'', handles: true, fadeSpeed: 200, onSelectChange: preview, x1: json[0][7], y1: json[0][9], x2: json[0][8], y2: json[0][10] });
					},350);
					
					var heightLastMain = heightMain.find('.dialog .inner').innerHeight();
					var heightDialog = heightLastMain+80;
					heightMain.find('.dialog .inner').css({ position: "", visibility: ""});
					var top = 0;
					top = ($(window).height() - heightLastMain) / 2;
					handlerFormAction();
					if (top < 10) {
						top = 50;
					}
					$('.window .dialog').animate({
						'top': top-40,
						'height' : heightDialog,
						'margin-top' : 0
					},300);
					$('#unload_img_loader').remove();
				}
			}
		});
	}
}

// Отправка любых данных из диалогового окна на страницу обработчика
function handlerFormAction(){
	var form = $('.window:last form#handlerForm');
	var xhr,xhr2;
	var emailParam = false;
	form.unbind("submit");
	form.bind("submit",function(){
		var x1 = $("#x1").val();
		var y1 = $("#y1").val();
		var x2 = $("#x2").val();
		var y2 = $("#y2").val();
		var w = $("#w").val();
		var h = $("#h").val();
		if(x1=="" || y1=="" || x2=="" || y2=="" || w=="" || h==""){
			alert("Выделите часть изображения под свой аватар");
			return false;
		}
		$('.modalDialogResponse').remove();
		$('.tooltip_help').hide();
		$('.tooltip_error').remove();
		$('.errorMessage').remove();
		if(xhr && xhr.readyState != 4){
			xhr.abort();
		}
		var data = form.serialize();
		xhr = $.ajax({
			type: "POST",
			url: '/include/handler.php',
			data: data+"&rnd="+Math.random(),
			dataType: "json",
			success: function (data, textStatus) {
				var json = eval(data);
				if(json.action!='error'){
					window.location = window.location;
				}
				else {
					alert('Возникла системная ошибка! Данные не сохранены. Попробуйте позднее.');
				}
			}
		});
		return false;
	});
}

function feedbackSuccessSend(responseText,result){
	var top = 0;
	$(document).keydown(function(eventObject){
		if (eventObject.which == 27) {
			return closeWin();
		}
	});
	var json = eval(responseText);
	var action = json.action;
	if(json.redirect){
		if(json.link){
			window.location=json.link;
		}
		else {
			window.location=window.location;
		}
	}
	else {
		$('body').append('<div class="window"><div onclick="return closeWin(\''+action+'\');" class="overlay"></div><div class="dialog"><span onclick="return closeWin(\''+action+'\');" class="close"></span><div class="inner '+action+'"></div></div></div>');
		top = ($(window).height() - 175) / 2 - $('#page').offset().top + $(window).scrollTop();
		if (top < 10) {
			top = 10;
		}
		
		var textInner = '<h2>'+json.title+'</h2><br><p>'+json.text+'</p>';
		var curInner = $('body').find('.window:last');
		curInner.find('.inner').css({ position: "absolute", visibility: "hidden", display: "block" });
		curInner.find('.dialog .inner').html(textInner);
		var heightLastMain = curInner.find('.dialog .inner').height()+74;
		curInner.find('.inner').css({ position: "", visibility: "", display: "none" });
		top = ($(window).height() - heightLastMain) / 2 - $('#page').offset().top + $(window).scrollTop();

		if (top < 10) {
			top = 10;
		}
		$('.window:last .dialog').css({
			'top': top,
			'marginTop': 0,
			'marginBottom': '20px',
		});
		$('.window:last').find('.loader').fadeOut();
		$('.window:last').find('.inner').fadeIn('fast');
		if($('.registration_block').length>0){
			$('.registration_block').find('form').css({"opacity":""});
		}
		$('.appendElem').remove();
		// if($('#popup_blocks .feedback_form.show').length>0){
			// $('#popup_blocks .feedback_form.show .send_request')
		// }
	}
	return false;
}

function list_areas_close(scroll,btn){
	if(scroll){
		$(btn).parents('.hidden_block').hide();
		if($(btn).parents('.hidden_block').parent().is('.filter_local')){
			$('body').height('');
			// $('body,html').animate({scrollTop:65},500);
			if(arrLinkPage){
				loadRequestUnreload();
			}
		}
	}
	else {
		$(btn).parent().find('.hidden_block').hide();
		$('body').height('');
	}
}

function clearFilter(tag,name,type,id,nameClass){
	var tag = $(tag);
	var arrMultiple = ["finishing","repairs","wc","communications","type_house","categories","type_house"];
	var form = $('#filter_search form');
	var estate = $('#filter_search form input[type="hidden"][name="estate"]').parent().find('li.checked a').data('type');
	var mainType = $('#filter_search .simple_search .checkbox_block.types .select_checkbox ul li.checked a').data('type');
	if(type=='checkbox_block'){
		if(nameClass!==undefined){
			
			var searchMin = $.inArray(name,arrayParamSelectMin);
			var searchMax = $.inArray(name,arrayParamSelectMax);
			if(searchMin!=-1){
				var nameExp = arrayParamSelectMax[searchMin];
			}
			if(searchMax!=-1){
				var nameExp = arrayParamSelectMin[searchMax];
			}
			var inputFindFirst = $('#filter_search form input[type="hidden"][name="'+name+'"]:enabled');
			var parents = inputFindFirst.parents('.inner_block');
			var parentBlockName = parents.parent().attr('class');
			// alert(estate); // flat
			// alert(mainType); // sell
			
			var inputFindSecond = $('#filter_search form input[type="hidden"][name="'+nameExp+'"]:enabled');

			if(parentBlockName=='simple_search'){
				inputFindFirst = $('#filter_search form .'+parentBlockName+' .inner_block .'+estate+'.'+mainType).find('input[type="hidden"][name="'+name+'"]:enabled');
				inputFindSecond = $('#filter_search form .'+parentBlockName+' .inner_block .'+estate+'.'+mainType).find('input[type="hidden"][name="'+nameExp+'"]:enabled');
			}
			if(parentBlockName=='advanced_search'){
				inputFindFirst = $('#filter_search form .'+parentBlockName+' .inner_block.'+estate+' .row.'+mainType).find('input[type="hidden"][name="'+name+'"]:enabled');
				inputFindSecond = $('#filter_search form .'+parentBlockName+' .inner_block.'+estate+' .row.'+mainType).find('input[type="hidden"][name="'+nameExp+'"]:enabled');
			}
			
			if(!tag.data('multiple')){
				inputFindFirst.parent().find('li:first a').click();
				inputFindSecond.parent().find('li:first a').click();
			}
			
			inputFindFirst.parent().find('.scrollbar-inner').scrollTop(0);
			inputFindSecond.parent().find('.scrollbar-inner').scrollTop(0);
				
			if(tag.data('multiple')){
				$('#filter_search form .'+parentBlockName+' .inner_block.'+estate+' .row.'+mainType).find('input[type="hidden"][name="'+name+'"]:enabled').removeAttr('value').parent().find('li').removeClass('current');
				$('#filter_search form .'+parentBlockName+' .inner_block.'+estate+' .row.'+mainType).find('input[type="hidden"][name="'+name+'"]:enabled').removeAttr('value').parent().find('.choosed_block').text('Не важно');
			}
				
			if(tag.data('single')){
				$('#filter_search form .'+parentBlockName+' .inner_block.'+estate+' .row.'+mainType).find('input[type="hidden"][name="'+name+'"]:enabled').removeAttr('value').parent().click();
			}
			// var multiple = parent.data('multiple');
			// if(!multiple){
				// inputFindFirst.parent().find('li:first a').click();
				// inputFindSecond.parent().find('li:first a').click();
				// inputFindFirst.parent().find('.scrollbar-inner').scrollTop(0);
				// inputFindSecond.parent().find('.scrollbar-inner').scrollTop(0);
			// }
			// else {
				// inputFindFirst.parent().find('li a[data-id="'+id+'"]').click();
			// }
			
		}
		else {
			var input = form.find('input[name="'+name+'"]:enabled');
			var CheckedIDs = [];
			input.parent().find('li a[data-id="'+id+'"]').parent().removeClass('checked');
			input.parent().find('li.checked').each(function(){
				id = $(this).children('a').data('id');
				CheckedIDs.push(id);
			});
			input.val(CheckedIDs);
		}
	}
	if(type=='checkbox_select'){
		var input = form.find('input[name="'+name+'"]:enabled');
		var CheckedIDs = [];
		input.parent().find('li a[data-id="'+id+'"]').parent().removeClass('checked');
		input.parent().find('li.checked').each(function(){
			id = $(this).children('a').data('id');
			CheckedIDs.push(id);
		});
		input.val(CheckedIDs);
	}
	if(type=='list_areas'){
		var input = form.find('input[name="'+name+'"]');
		var CheckedIDs = [];
		input.parent().find('a[data-id="'+id+'"]').css({"background-image":"none"}).parent().removeClass('current');
		input.parent().find('.current').each(function(){
			id = $(this).children('a').data('id');
			if($.inArray(id,CheckedIDs)==-1){
				CheckedIDs.push(id);
			}
		});
		input.val(CheckedIDs);
		var name2 = tag.data('name');
		var type2 = tag.data('type');
		var id2 = tag.data('id');
		$('#filter_search .selected_params ul li a[data-id="'+id2+'"][data-name="'+name2+'"][data-type="'+type2+'"]').parent().remove();
	}
	tag.parent().remove();
}

// Простая замена URL в адресной строке
function changeURLPage(url){
	if ( !history.pushState || typeof history.pushState !== 'function' ) {
		return;
	}
	window.history.pushState(null, null, url);
}

// Простая функция обработка стрелок "Вперёд", "Назад"
function addEventListenerBtn(){
	window.addEventListener && window.addEventListener('popstate', function(e){
		triggerURL = false;
		loadRequestUnreload();
	});
}

// Получаем URL страницы
function getURL() {
	var url = window.location.href
	  , slide = url.split( '/' ).join('/');
	return slide;
}

function delTags(tag){
	var tag = $(tag);
	var parent = tag.parent();
	var name = parent.data('name');
	var type = parent.data('type');
	var multiple = parent.data('multiple');
	var single = parent.data('single');
	var id = parent.data('id');
	var nameClass = parent.attr('class');
	var form = $('#filter_search form');
	var estate = $('#filter_search form input[type="hidden"][name="estate"]').parent().find('li.checked a').data('type');
	var mainType = $('#filter_search .simple_search .checkbox_block.types .select_checkbox ul li.checked a').data('type');
	if(type=='checkbox_block'){
		if(nameClass!==undefined){
			var searchMin = $.inArray(name,arrayParamSelectMin);
			var searchMax = $.inArray(name,arrayParamSelectMax);
			if(searchMin!=-1){
				var nameExp = arrayParamSelectMax[searchMin];
			}
			if(searchMax!=-1){
				var nameExp = arrayParamSelectMin[searchMax];
			}
			var inputFindFirst = $('#filter_search form input[type="hidden"][name="'+name+'"]:enabled');
			var parents = inputFindFirst.parents('.inner_block');
			var parentBlockName = parents.parent().attr('class');
			// alert(estate); // flat
			// alert(mainType); // sell
			
			var inputFindSecond = $('#filter_search form input[type="hidden"][name="'+nameExp+'"]:enabled');

			if(parentBlockName=='simple_search'){
				inputFindFirst = $('#filter_search form .'+parentBlockName+' .inner_block .'+estate+'.'+mainType+' input[type="hidden"][name="'+name+'"]:enabled');
				inputFindSecond = $('#filter_search form .'+parentBlockName+' .inner_block .'+estate+'.'+mainType+' input[type="hidden"][name="'+nameExp+'"]:enabled');
			}
			if(parentBlockName=='advanced_search'){
				inputFindFirst = $('#filter_search form .'+parentBlockName+' .inner_block.'+estate+' .row.'+mainType+' input[type="hidden"][name="'+name+'"]:enabled');
				inputFindSecond = $('#filter_search form .'+parentBlockName+' .inner_block.'+estate+' .row.'+mainType+' input[type="hidden"][name="'+nameExp+'"]:enabled');
			}
			
			if(!multiple){
				if(!single){
					inputFindFirst.parent().find('li:first a').click();
					inputFindSecond.parent().find('li:first a').click();
					inputFindFirst.parent().find('.scrollbar-inner').scrollTop(0);
					inputFindSecond.parent().find('.scrollbar-inner').scrollTop(0);
				}
				else {
					inputFindFirst.parent().click();
				}
			}
			else {
				inputFindFirst.parent().find('li a[data-id="'+id+'"]').click();
			}
		}
		else {
			var input = form.find('input[name="'+name+'"]:enabled');
			var CheckedIDs = [];
			input.parent().find('li a[data-id="'+id+'"]').parent().removeClass('checked');
			input.parent().find('li.checked').each(function(){
				id = $(this).children('a').data('id');
				CheckedIDs.push(id);
			});
			input.val(CheckedIDs);
		}
	}
	if(type=='checkbox_select'){
		var input = form.find('input[name="'+name+'"]:enabled');
		var CheckedIDs = [];
		input.parent().find('li a[data-id="'+id+'"]').parent().removeClass('checked');
		input.parent().find('li.checked').each(function(){
			id = $(this).children('a').data('id');
			CheckedIDs.push(id);
		});
		input.val(CheckedIDs);
	}
	if(type=='list_areas'){
		var input = form.find('input[name="'+name+'"]');
		var CheckedIDs = [];
		input.parent().find('a[data-id="'+id+'"]').css({"background-image":"none"}).parent().removeClass('current');
		if(name=='railway'){
			input.parent().find('a[data-id="'+id+'"]').parent().remove();
			input.parent().find('ul.railways li').each(function(){
				id = $(this).children('a').data('id');
				if($.inArray(id,CheckedIDs)==-1){
					CheckedIDs.push(id);
				}
			});
			input.val(CheckedIDs);
		}
		else {
			input.parent().find('.current').each(function(){
				id = $(this).children('a').data('id');
				if($.inArray(id,CheckedIDs)==-1){
					CheckedIDs.push(id);
				}
			});
			input.val(CheckedIDs);
		}
		var name2 = tag.parent().data('name');
		var type2 = tag.parent().data('type');
		var id2 = tag.parent().data('id');
		$('#filter_search .selected_params ul li a[data-id="'+id2+'"][data-name="'+name2+'"][data-type="'+type2+'"]').parent().remove();
	}
	tag.parent().parent().remove();
	
	if(arrLinkPage){
		loadRequestUnreload();
	}
}

var xhrCard;
function show_phone(btn,id){
	var btn = $(btn);
	if(xhrCard && xhrCard.readyState != 4){
		xhrCard.abort();
	}
	xhrCard = $.ajax({
		type: 'POST',
		url: "/include/handler.php",
		cache: false,
		dataType: "json",
		data: {"showPhone":parseInt(id),"type":arrLinkPage[1]},
		success: function(data){  
			var json = eval(data);
			if(json.action!='error'){
				btn.parent().parent().html('<div class="js-phone">'+json.phone+'</div>');
			}
			else {
				btn.parent().parent().empty();
			}
		}  
	});
}

function loadRequestUnreload(page){
	var arrLinkPage2 = arrLinkPage[arrLinkPage.length-1].split("?");
	var arrLinkPage1 = arrLinkPage[1].split("/");
	// if($.inArray('search',arrLinkPage2)>-1){
	if($('.btns-show .show .item.current').length>0){
		$('input[name="view"][type="hidden"]').remove();
		if($('.btns-show .show .item.current').is('.table')){
			$('#filter_search form').append('<input type="hidden" name="view" value="table">');
		}
		if($('.btns-show .show .item.current').is('.map')){
			$('#filter_search form').append('<input type="hidden" name="view" value="map">');
		}
	}
	if($.inArray('search',arrLinkPage1)>-1){
		var resultContainer = $('#result_search .list_items');
		var coutResult = $('.sort_offers .count_offers span');
		clearEmptyValue();
		var data = $('#filter_search form').serialize();
		if(xhrLoad && xhrLoad.readyState != 4){
			xhrLoad.abort();
			$('.loadingProcessResult').remove();
		}
		$('#center').append('<div class="loadingProcessResult"><i class="fa fa-spinner fa-pulse"></i></div>');
		
		// Формируем ссылку
		var linkPage = '/search?'+data;
		changeURLPage(linkPage);

		if(page){
			data = data+"&"+page;
		}
		
		xhrLoad = $.ajax({
			type: "POST",
			url: '/searchAjax.php',
			data: data+"&searchInfo=true",
			dataType: "json",
			success: function (data, textStatus) {
				var json = eval(data);
				if(json.action=='empty'){
					$('.loadingProcessResult').remove();
					$('#center').empty();
					$('#center').append('<h1 class="inner_pages">Поиск</h1>');
					$('#center').append('<div class="sort_offers"><p>Поиск не дал результатов! Попробуйте другую комбинацию</p></div>');
				}
				else {
					if(page){
						thisPageNum = thisPageNum + 1;
					}
					var estateArray = json.estateArray;
					var iconMaps = json.iconMaps;
					var coordsArray2 = json.coordsArray2;
					$('.loadingProcessResult').remove();
					$('#center').empty();
					$('#center').append(json.title);
					$('#center').append(json.count);
					$('#center').append(json.list);
					$('#center').append(json.pager);
					$(json.pager).insertBefore('#result_search');
					initiallSelected(true);
					loadPeppermint();
					if(json.type==1){
						function installerMaps(){
							ymaps.ready(function () {
								var mapCenter = [59.939095, 30.315868],
									map = new ymaps.Map('map', {
										center: mapCenter,
										zoom: 12,
										// behaviors: ['default', 'scrollZoom'],
										controls: ['zoomControl',  'fullscreenControl']
									}),
									points = [
										coordsArray2
									],
									estates = estateArray,
									iconMaps = iconMaps,
									geoObjects = [],
									getPointOptions = function () {
										return {
											preset: 'islands#violetIcon'
										};
									};
									
								// Создаем собственный макет с информацией о выбранном геообъекте.
								var customBalloonContentLayout = ymaps.templateLayoutFactory.createClass([
										'<div class="containerForMap"><div class="title"><div class="price">от <strong>3 450 680 руб.</strong></div><div class="zoom_objects"><a href="javascript:void(0)"><strong>3 объектов</strong><i class="fa fa-search"></i><span>(показать на карте)</span></a></div></div><div class="separate"></div><ul class=list_flats>',
										// Выводим в цикле список всех геообъектов.
										'{% for geoObjects in properties.geoObjects %}',
											'<li data-placemarkid="{{ geoObjects.properties.placemarkId }}" class="row {{ geoObjects.properties.fone }}">'+
												'<i class="triangle-bottomleft {{ geoObjects.properties.colorEstate|raw }}"></i>'+
												'<div class="name">{{ geoObjects.properties.nameEstate|raw }}</div>'+
												'<div class="price"><strong>{{ geoObjects.properties.priceEstate|raw }} руб.</strong></div>'+
												'<div class="floors">{{ geoObjects.properties.floorsEstate|raw }}</div>'+
												'<div class="more_info"><a target="_blank" href="{{ geoObjects.properties.linkEstate|raw }}">подробно</a></div>'+
											'</li>',
										'{% endfor %}',
										'</ul></div>'
									].join(''));
									
								getPointOptions = function () {
									return {
										preset: 'islands#violetIcon'
									};
								};
								
								// Создание макета содержимого балуна.
								BalloonContentLayout = ymaps.templateLayoutFactory.createClass(
									'<div class="containerForMap"><div class="title"><div class="price"><strong>{{properties.addressEstate}}</strong></div><div class="zoom_objects"><a target="_blank" href="{{properties.linkEstate}}"><strong>{{ properties.fullSquareEstate|raw }}/{{ properties.liveSquareEstate|raw }} м²</strong> общая/жилая</a><span class="obj_right"><a href="{{ properties.linkEstate|raw }}">1 объект</a></span></div></div><div class="separate"></div>' +
									'<ul class=list_flats>' +
									'<li data-placemarkid="{{ properties.placemarkId }}" class="row {{ properties.fone }}">'+
										'<i class="triangle-bottomleft {{ properties.colorEstate|raw }}"></i>'+
										'<div class="name">{{ properties.nameEstate|raw }}</div>'+
										'<div class="price"><strong>{{ properties.priceEstate|raw }} руб.</strong></div>'+
										'<div class="floors">{{ properties.floorsEstate|raw }}</div>'+
										'<div class="more_info"><a target="_blank" href="{{ properties.linkEstate|raw }}">подробно</a></div>'+
									'</li>' +
									'</ul>' +
									'</div>');
								
								
								jQuery(document).on( "click", "a.list_item", function() {
									// Определяем по какой метке произошло событие.
									var selectedPlacemark = points[jQuery(this).data().placemarkid];
									alert( selectedPlacemark.properties.get('balloonContentBody') );
								});
												
								var clusterer = new ymaps.Clusterer({
									clusterHideIconOnBalloonOpen: false,
									clusterDisableClickZoom: false,
									preset: 'islands#nightClusterIcons',
									clusterBalloonMaxHeight: 325,
									clusterBalloonWidth: 555,
									// Устанавливаем собственный макет контента балуна.
									clusterBalloonContentLayout: customBalloonContentLayout
								});
								
								// Заполняем кластер геообъектами
								var n = 1;
								var fone = '';
								for(var i = 0, len = points.length; i < len; i++) {
									if(n==2){
										fone = ' fone';
										n = 0;
									}
									else {
										fone = '';
									}
									var placemark = new ymaps.Placemark(points[i], {
										// Устаналиваем данные, которые будут отображаться в балуне.
										balloonContentHeader: 'Заголовок метки №' + (i + 1),
										balloonContentBody: 'Информация о метке №' + (i + 1),
										linkEstate: estates[i]['link'],
										colorEstate: estates[i]['color_name'],
										nameEstate: estates[i]['name'],
										priceEstate: estates[i]['price'],
										floorsEstate: estates[i]['floors'],
										addressEstate: estates[i]['address'],
										fullSquareEstate: estates[i]['full_square'],
										liveSquareEstate: estates[i]['live_square'],
										fone: fone,
										placemarkId: i
									}, {
										balloonContentLayout: BalloonContentLayout,
										iconLayout: 'default#image',
										iconImageHref: '/images/'+iconMaps[estates[i]['rooms']]['list'],
										iconImageSize: [32, 27],
									iconImageOffset: [-15, -23]
									});
									n++;
									geoObjects[i] = placemark;
								}
								
								/**
								 * Можно менять опции кластеризатора после создания.
								 */
								clusterer.options.set({
									gridSize: 120,
									// clusterDisableClickZoom: true
									clusterDisableClickZoom: false
								});
								
								clusterer.add(geoObjects);
								map.geoObjects.add(clusterer);
								
								map.setBounds(clusterer.getBounds(), {
									checkZoomRange: true
								});
							});
						}
					}
					if(json.type==2){
						function installerMaps(){
							ymaps.ready(function () {
								var mapCenter = [59.939095, 30.315868],
									map = new ymaps.Map('map', {
										center: mapCenter,
										zoom: 12,
										// behaviors: ['default', 'scrollZoom'],
										controls: ['zoomControl',  'fullscreenControl']
									}),
									points = [
										coordsArray2
									],
									estates = estateArray,
									iconMaps = iconMaps,
									geoObjects = [],
									getPointOptions = function () {
										return {
											preset: 'islands#violetIcon'
										};
									};
									
								// Создаем собственный макет с информацией о выбранном геообъекте.
								var customBalloonContentLayout = ymaps.templateLayoutFactory.createClass([
										'<div class="containerForMap"><div class="title"><div class="price">от <strong>3 450 680 руб.</strong></div><div class="zoom_objects"><a href="javascript:void(0)"><strong>Все объекты в списке</strong><i class="fa fa-search"></i><span>(показать на карте)</span></a></div></div><div class="separate"></div><ul class=list_flats>',
										// Выводим в цикле список всех геообъектов.
										'{% for geoObjects in properties.geoObjects %}',
											'<li data-placemarkid="{{ geoObjects.properties.placemarkId }}" class="row {{ geoObjects.properties.fone }}">'+
												'<i class="triangle-bottomleft {{ geoObjects.properties.colorEstate|raw }}"></i>'+
												'<div class="name">{{ geoObjects.properties.nameEstate|raw }}</div>'+
												'<div class="price"><strong>{{ geoObjects.properties.priceEstate|raw }} руб.</strong></div>'+
												'<div class="floors">{{ geoObjects.properties.floorsEstate|raw }}</div>'+
												'<div class="more_info"><a target="_blank" href="{{ geoObjects.properties.linkEstate|raw }}">подробно</a></div>'+
											'</li>',
										'{% endfor %}',
										'</ul></div>'
									].join(''));
									
								getPointOptions = function () {
									return {
										preset: 'islands#violetIcon'
									};
								};
								
								// Создание макета содержимого балуна.
								BalloonContentLayout = ymaps.templateLayoutFactory.createClass(
									'<div class="containerForMap"><div class="title"><div class="price"><strong>{{properties.addressEstate}}</strong></div><div class="zoom_objects"><a target="_blank" href="{{properties.linkEstate}}"><strong>{{ properties.fullSquareEstate|raw }}/{{ properties.liveSquareEstate|raw }} м²</strong> <span class="type">общая/жилая</span></a><span class="obj_right"><a href="{{ properties.linkEstate|raw }}">1 объект</a></span></div></div><div class="separate"></div>' +
									'<ul class=list_flats>' +
									'<li data-placemarkid="{{ properties.placemarkId }}" class="row {{ properties.fone }}">'+
										'<i class="triangle-bottomleft {{ properties.colorEstate|raw }}"></i>'+
										'<div class="name">{{ properties.nameEstate|raw }}</div>'+
										'<div class="price"><strong>{{ properties.priceEstate|raw }} руб.</strong></div>'+
										'<div class="floors">{{ properties.floorsEstate|raw }}</div>'+
										'<div class="more_info"><a target="_blank" href="{{ properties.linkEstate|raw }}">подробно</a></div>'+
									'</li>' +
									'</ul>' +
									'</div>');
								
								jQuery(document).on( "click", ".containerForMap .title .zoom_objects a", function() {
									var selectedPlacemark = [];
									// Определяем по какой метке произошло событие.
									$(this).parent().parent().parent().find('.list_flats .row').each(function(){
										selectedPlacemark.push(points[$(this).data().placemarkid]);
									});
									
									// alert( li.data().placemarkid);
									var geoObjectState = clusterer.getObjectState(map);
									var obj = clusterer.getObjectState(points[0]);
									map.setBounds(selectedPlacemark, {
										checkZoomRange: true,
										zoomMargin: 15
									});
									// var geoObjects2 = selectedPlacemark.properties.get('geoObjects');
									// alert(geoObjects2)
								});
												
								var clusterer = new ymaps.Clusterer({
									clusterHideIconOnBalloonOpen: false,
									clusterDisableClickZoom: false,
									preset: 'islands#nightClusterIcons',
									clusterBalloonMaxHeight: 325,
									clusterBalloonWidth: 555,
									// Устанавливаем собственный макет контента балуна.
									clusterBalloonContentLayout: customBalloonContentLayout
								});
								
								// Заполняем кластер геообъектами
								var n = 1;
								var fone = '';
								for(var i = 0, len = points.length; i < len; i++) {
									if(n==2){
										fone = ' fone';
										n = 0;
									}
									else {
										fone = '';
									}
									// alert('Сейчас на карте показаны ' + shownObjectsCounter + ' меток из ' + geoObjects.length + '.');
									var placemark = new ymaps.Placemark(points[i], {
										// Устаналиваем данные, которые будут отображаться в балуне.
										balloonContentHeader: 'Заголовок метки №' + (i + 1),
										balloonContentBody: 'Информация о метке №' + (i + 1),
										linkEstate: estates[i]['link'],
										colorEstate: estates[i]['color_name'],
										nameEstate: estates[i]['name'],
										priceEstate: estates[i]['price'],
										floorsEstate: estates[i]['floors'],
										addressEstate: estates[i]['address'],
										fullSquareEstate: estates[i]['full_square'],
										liveSquareEstate: estates[i]['live_square'],
										fone: fone,
										placemarkId: i
									}, {
										balloonContentLayout: BalloonContentLayout,
										iconLayout: 'default#image',
										iconImageHref: '/images/'+iconMaps[estates[i]['rooms']]['list'],
										iconImageSize: [32, 27],
										iconImageOffset: [-15, -23]
									});
									n++;
									geoObjects[i] = placemark;
								}
								
								/**
								 * Можно менять опции кластеризатора после создания.
								 */
								clusterer.options.set({
									gridSize: 120,
									// clusterDisableClickZoom: true
									clusterDisableClickZoom: false
								});
								
								// var geoObjects2 = clusterer.properties.get('geoObjects');
								// alert(geoObjects2);
								
								clusterer.add(geoObjects);
								map.geoObjects.add(clusterer);
												
								map.setBounds(clusterer.getBounds(), {
									checkZoomRange: true
								});
							});
						}
					}
					if(json.type==3){
						function installerMaps(){
							ymaps.ready(function () {
								var mapCenter = [59.939095, 30.315868],
									map = new ymaps.Map('map', {
										center: mapCenter,
										zoom: 12,
										// behaviors: ['default', 'scrollZoom'],
										controls: ['zoomControl',  'fullscreenControl']
									}),
									points = [
										coordsArray2
									],
									estates = estateArray,
									iconMaps = iconMaps,
									geoObjects = [],
									getPointOptions = function () {
										return {
											preset: 'islands#violetIcon'
										};
									};
									
								// Создаем собственный макет с информацией о выбранном геообъекте.
								var customBalloonContentLayout = ymaps.templateLayoutFactory.createClass([
										'<div class="containerForMap"><div class="title"><div class="price">от <strong>3 450 680 руб.</strong></div><div class="zoom_objects"><a href="javascript:void(0)"><strong>18 объектов</strong><i class="fa fa-search"></i><span>(показать на карте)</span></a></div></div><div class="separate"></div><ul class=list_flats>',
										// Выводим в цикле список всех геообъектов.
										'{% for geoObjects in properties.geoObjects %}',
											'<li data-placemarkid="{{ geoObjects.properties.placemarkId }}" class="row {{ geoObjects.properties.fone }}">'+
												'<i class="triangle-bottomleft {{ geoObjects.properties.colorEstate|raw }}"></i>'+
												'<div class="name">{{ geoObjects.properties.nameEstate|raw }}</div>'+
												'<div class="price"><strong>{{ geoObjects.properties.priceEstate|raw }} руб.</strong></div>'+
												'<div class="floors">{{ geoObjects.properties.floorsEstate|raw }}</div>'+
												'<div class="more_info"><a target="_blank" href="{{ geoObjects.properties.linkEstate|raw }}">подробно</a></div>'+
											'</li>',
										'{% endfor %}',
										'</ul></div>'
									].join(''));
									
								getPointOptions = function () {
									return {
										preset: 'islands#violetIcon'
									};
								};
								
								// Создание макета содержимого балуна.
								BalloonContentLayout = ymaps.templateLayoutFactory.createClass(
									'<div class="containerForMap"><div class="title"><div class="price"><strong>{{properties.addressEstate}}</strong></div><div class="zoom_objects"><a target="_blank" href="{{properties.linkEstate}}"><strong>{{ properties.fullSquareEstate|raw }}/{{ properties.liveSquareEstate|raw }} м²</strong> <span class="type">общая/жилая</span></a><span class="obj_right"><a href="{{ properties.linkEstate|raw }}">1 объект</a></span></div></div><div class="separate"></div>' +
									'<ul class=list_flats>' +
									'<li data-placemarkid="{{ properties.placemarkId }}" class="row {{ properties.fone }}">'+
										'<i class="triangle-bottomleft {{ properties.colorEstate|raw }}"></i>'+
										'<div class="name">{{ properties.nameEstate|raw }}</div>'+
										'<div class="price"><strong>{{ properties.priceEstate|raw }} руб.</strong></div>'+
										'<div class="floors">{{ properties.floorsEstate|raw }}</div>'+
										'<div class="more_info"><a target="_blank" href="{{ properties.linkEstate|raw }}">подробно</a></div>'+
									'</li>' +
									'</ul>' +
									'</div>');
								
								
								jQuery(document).on( "click", "a.list_item", function() {
									// Определяем по какой метке произошло событие.
									var selectedPlacemark = points[jQuery(this).data().placemarkid];
									alert( selectedPlacemark.properties.get('balloonContentBody') );
								});
												
								var clusterer = new ymaps.Clusterer({
									clusterHideIconOnBalloonOpen: false,
									clusterDisableClickZoom: false,
									preset: 'islands#nightClusterIcons',
									clusterBalloonMaxHeight: 325,
									clusterBalloonWidth: 555,
									// Устанавливаем собственный макет контента балуна.
									clusterBalloonContentLayout: customBalloonContentLayout
								});
								
								// Заполняем кластер геообъектами
								var n = 1;
								var fone = '';
								for(var i = 0, len = points.length; i < len; i++) {
									if(n==2){
										fone = ' fone';
										n = 0;
									}
									else {
										fone = '';
									}
									var placemark = new ymaps.Placemark(points[i], {
										// Устаналиваем данные, которые будут отображаться в балуне.
										balloonContentHeader: 'Заголовок метки №' + (i + 1),
										balloonContentBody: 'Информация о метке №' + (i + 1),
										linkEstate: estates[i]['link'],
										colorEstate: estates[i]['color_name'],
										nameEstate: estates[i]['name'],
										priceEstate: estates[i]['price'],
										floorsEstate: estates[i]['floors'],
										addressEstate: estates[i]['address'],
										fullSquareEstate: estates[i]['full_square'],
										liveSquareEstate: estates[i]['live_square'],
										fone: fone,
										placemarkId: i
									}, {
										balloonContentLayout: BalloonContentLayout,
										iconLayout: 'default#image',
										// iconImageHref: '/images/'+iconMaps[estates[i]['rooms']]['list'],
										iconImageHref: '/images/'+iconMaps[estates[i]['rooms']]['list'],
										iconImageSize: [32, 27],
										iconImageOffset: [-15, -23]
									});
									n++;
									geoObjects[i] = placemark;
								}
								
								/**
								 * Можно менять опции кластеризатора после создания.
								 */
								clusterer.options.set({
									gridSize: 120,
									// clusterDisableClickZoom: true
									clusterDisableClickZoom: false
								});
								
								clusterer.add(geoObjects);
								map.geoObjects.add(clusterer);
								
								map.setBounds(clusterer.getBounds(), {
									checkZoomRange: true
								});
							});
						}
					}
					if(json.type==4){
						function installerMaps(){
							ymaps.ready(function () {
								var mapCenter = [59.939095, 30.315868],
									map = new ymaps.Map('map', {
										center: mapCenter,
										zoom: 12,
										// behaviors: ['default', 'scrollZoom'],
										controls: ['zoomControl',  'fullscreenControl']
									}),
									points = [
										coordsArray2
									],
									estates = estateArray,
									iconMaps = iconMaps,
									geoObjects = [],
									getPointOptions = function () {
										return {
											preset: 'islands#violetIcon'
										};
									};
									
								// Создаем собственный макет с информацией о выбранном геообъекте.
								var customBalloonContentLayout = ymaps.templateLayoutFactory.createClass([
										'<div class="containerForMap"><div class="title"><div class="price">от <strong>3 450 680 руб.</strong></div><div class="zoom_objects"><a href="javascript:void(0)"><strong>18 объектов</strong><i class="fa fa-search"></i><span>(показать на карте)</span></a></div></div><div class="separate"></div><ul class=list_flats>',
										// Выводим в цикле список всех геообъектов.
										'{% for geoObjects in properties.geoObjects %}',
											'<li data-placemarkid="{{ geoObjects.properties.placemarkId }}" class="row {{ geoObjects.properties.fone }}">'+
												'<i class="triangle-bottomleft {{ geoObjects.properties.colorEstate|raw }}"></i>'+
												'<div class="name">{{ geoObjects.properties.nameEstate|raw }}</div>'+
												'<div class="price"><strong>{{ geoObjects.properties.priceEstate|raw }} руб.</strong></div>'+
												'<div class="floors">{{ geoObjects.properties.floorsEstate|raw }}</div>'+
												'<div class="more_info"><a target="_blank" href="{{ geoObjects.properties.linkEstate|raw }}">подробно</a></div>'+
											'</li>',
										'{% endfor %}',
										'</ul></div>'
									].join(''));
									
								getPointOptions = function () {
									return {
										preset: 'islands#violetIcon'
									};
								};
								
								// Создание макета содержимого балуна.
								BalloonContentLayout = ymaps.templateLayoutFactory.createClass(
									'<div class="containerForMap"><div class="title"><div class="price"><strong>{{properties.addressEstate}}</strong></div><div class="zoom_objects"><a target="_blank" href="{{properties.linkEstate}}"><strong>{{ properties.fullSquareEstate|raw }}/{{ properties.liveSquareEstate|raw }} м²</strong> <span class="type">общая/жилая</span></a><span class="obj_right"><a href="{{ properties.linkEstate|raw }}">1 объект</a></span></div></div><div class="separate"></div>' +
									'<ul class=list_flats>' +
									'<li data-placemarkid="{{ properties.placemarkId }}" class="row {{ properties.fone }}">'+
										'<i class="triangle-bottomleft {{ properties.colorEstate|raw }}"></i>'+
										'<div class="name">{{ properties.nameEstate|raw }}</div>'+
										'<div class="price"><strong>{{ properties.priceEstate|raw }} руб.</strong></div>'+
										'<div class="floors">{{ properties.floorsEstate|raw }}</div>'+
										'<div class="more_info"><a target="_blank" href="{{ properties.linkEstate|raw }}">подробно</a></div>'+
									'</li>' +
									'</ul>' +
									'</div>');
								
								
								jQuery(document).on( "click", "a.list_item", function() {
									// Определяем по какой метке произошло событие.
									var selectedPlacemark = points[jQuery(this).data().placemarkid];
									alert( selectedPlacemark.properties.get('balloonContentBody') );
								});
												
								var clusterer = new ymaps.Clusterer({
									clusterHideIconOnBalloonOpen: false,
									clusterDisableClickZoom: false,
									preset: 'islands#nightClusterIcons',
									clusterBalloonMaxHeight: 325,
									clusterBalloonWidth: 555,
									// Устанавливаем собственный макет контента балуна.
									clusterBalloonContentLayout: customBalloonContentLayout
								});
								
								// Заполняем кластер геообъектами
								var n = 1;
								var fone = '';
								for(var i = 0, len = points.length; i < len; i++) {
									if(n==2){
										fone = ' fone';
										n = 0;
									}
									else {
										fone = '';
									}
									var placemark = new ymaps.Placemark(points[i], {
										// Устаналиваем данные, которые будут отображаться в балуне.
										balloonContentHeader: 'Заголовок метки №' + (i + 1),
										balloonContentBody: 'Информация о метке №' + (i + 1),
										linkEstate: estates[i]['link'],
										colorEstate: estates[i]['color_name'],
										nameEstate: estates[i]['name'],
										priceEstate: estates[i]['price'],
										floorsEstate: estates[i]['floors'],
										addressEstate: estates[i]['address'],
										fullSquareEstate: estates[i]['full_square'],
										liveSquareEstate: estates[i]['live_square'],
										fone: fone,
										placemarkId: i
									}, {
										balloonContentLayout: BalloonContentLayout,
										iconLayout: 'default#image',
										// iconImageHref: '/images/'+iconMaps[estates[i]['rooms']]['list'],
										iconImageHref: '/images/neitralIcon_small.png',
										iconImageSize: [32, 27],
										iconImageOffset: [-15, -23]
									});
									n++;
									geoObjects[i] = placemark;
								}
								
								/**
								 * Можно менять опции кластеризатора после создания.
								 */
								clusterer.options.set({
									gridSize: 120,
									// clusterDisableClickZoom: true
									clusterDisableClickZoom: false
								});
								
								clusterer.add(geoObjects);
								map.geoObjects.add(clusterer);
								
								map.setBounds(clusterer.getBounds(), {
									checkZoomRange: true
								});
							});
						}
					}
					if(json.type==5){
						function installerMaps(){
							ymaps.ready(function () {
								var mapCenter = [59.939095, 30.315868],
									map = new ymaps.Map('map', {
										center: mapCenter,
										zoom: 12,
										// behaviors: ['default', 'scrollZoom'],
										controls: ['zoomControl',  'fullscreenControl']
									}),
									points = [
										coordsArray2
									],
									estates = estateArray,
									iconMaps = iconMaps,
									geoObjects = [],
									getPointOptions = function () {
										return {
											preset: 'islands#violetIcon'
										};
									};
									
								// Создаем собственный макет с информацией о выбранном геообъекте.
								var customBalloonContentLayout = ymaps.templateLayoutFactory.createClass([
										'<div class="containerForMap"><div class="title"><div class="price">от <strong>3 450 680 руб.</strong></div><div class="zoom_objects"><a href="javascript:void(0)"><strong>18 объектов</strong><i class="fa fa-search"></i><span>(показать на карте)</span></a></div></div><div class="separate"></div><ul class=list_flats>',
										// Выводим в цикле список всех геообъектов.
										'{% for geoObjects in properties.geoObjects %}',
											'<li data-placemarkid="{{ geoObjects.properties.placemarkId }}" class="row {{ geoObjects.properties.fone }}">'+
												'<i class="triangle-bottomleft {{ geoObjects.properties.colorEstate|raw }}"></i>'+
												'<div class="name">{{ geoObjects.properties.nameEstate|raw }}</div>'+
												'<div class="price"><strong>{{ geoObjects.properties.priceEstate|raw }} руб.</strong></div>'+
												'<div class="floors">{{ geoObjects.properties.floorsEstate|raw }}</div>'+
												'<div class="more_info"><a target="_blank" href="{{ geoObjects.properties.linkEstate|raw }}">подробно</a></div>'+
											'</li>',
										'{% endfor %}',
										'</ul></div>'
									].join(''));
									
								getPointOptions = function () {
									return {
										preset: 'islands#violetIcon'
									};
								};
								
								// Создание макета содержимого балуна.
								BalloonContentLayout = ymaps.templateLayoutFactory.createClass(
									'<div class="containerForMap"><div class="title"><div class="price"><strong>{{properties.addressEstate}}</strong></div><div class="zoom_objects"><a target="_blank" href="{{properties.linkEstate}}"><strong>{{ properties.fullSquareEstate|raw }}/{{ properties.liveSquareEstate|raw }} м²</strong> <span class="type">общая/жилая</span></a><span class="obj_right"><a href="{{ properties.linkEstate|raw }}">1 объект</a></span></div></div><div class="separate"></div>' +
									'<ul class=list_flats>' +
									'<li data-placemarkid="{{ properties.placemarkId }}" class="row {{ properties.fone }}">'+
										'<i class="triangle-bottomleft {{ properties.colorEstate|raw }}"></i>'+
										'<div class="name">{{ properties.nameEstate|raw }}</div>'+
										'<div class="price"><strong>{{ properties.priceEstate|raw }} руб.</strong></div>'+
										'<div class="floors">{{ properties.floorsEstate|raw }}</div>'+
										'<div class="more_info"><a target="_blank" href="{{ properties.linkEstate|raw }}">подробно</a></div>'+
									'</li>' +
									'</ul>' +
									'</div>');
								
								
								jQuery(document).on( "click", "a.list_item", function() {
									// Определяем по какой метке произошло событие.
									var selectedPlacemark = points[jQuery(this).data().placemarkid];
									alert( selectedPlacemark.properties.get('balloonContentBody') );
								});
												
								var clusterer = new ymaps.Clusterer({
									clusterHideIconOnBalloonOpen: false,
									clusterDisableClickZoom: false,
									preset: 'islands#nightClusterIcons',
									clusterBalloonMaxHeight: 325,
									clusterBalloonWidth: 555,
									// Устанавливаем собственный макет контента балуна.
									clusterBalloonContentLayout: customBalloonContentLayout
								});
								
								// Заполняем кластер геообъектами
								var n = 1;
								var fone = '';
								for(var i = 0, len = points.length; i < len; i++) {
									if(n==2){
										fone = ' fone';
										n = 0;
									}
									else {
										fone = '';
									}
									var placemark = new ymaps.Placemark(points[i], {
										// Устаналиваем данные, которые будут отображаться в балуне.
										balloonContentHeader: 'Заголовок метки №' + (i + 1),
										balloonContentBody: 'Информация о метке №' + (i + 1),
										linkEstate: estates[i]['link'],
										colorEstate: estates[i]['color_name'],
										nameEstate: estates[i]['name'],
										priceEstate: estates[i]['price'],
										floorsEstate: estates[i]['floors'],
										addressEstate: estates[i]['address'],
										fullSquareEstate: estates[i]['full_square'],
										liveSquareEstate: estates[i]['live_square'],
										fone: fone,
										placemarkId: i
									}, {
										balloonContentLayout: BalloonContentLayout,
										iconLayout: 'default#image',
										// iconImageHref: '/images/'+iconMaps[estates[i]['rooms']]['list'],
										iconImageHref: '/images/neitralIcon_small.png',
										iconImageSize: [32, 27],
										iconImageOffset: [-15, -23]
									});
									n++;
									geoObjects[i] = placemark;
								}
								
								/**
								 * Можно менять опции кластеризатора после создания.
								 */
								clusterer.options.set({
									gridSize: 120,
									// clusterDisableClickZoom: true
									clusterDisableClickZoom: false
								});
								
								clusterer.add(geoObjects);
								map.geoObjects.add(clusterer);
								
								map.setBounds(clusterer.getBounds(), {
									checkZoomRange: true
								});
							});
						}
					}
					installerMaps();
					stickMainNav();
					phoneShowClick();
				}
			}
		});
	}
}

function loadRequestUnreload2(type){
	// clearEmptyValue();
	if(xhrLoad && xhrLoad.readyState != 4){
		xhrLoad.abort();
		$('.loadingProcessResult').remove();
	}
	// Формируем ссылку
	var namePage = window.location.toString();
	var arrPage = namePage.split("https://");
	var partLink = arrPage[1];
	var arrLinkPage = partLink.split("/");
	var linkAll = arrLinkPage[arrLinkPage.length-1];
	linkAll = linkAll.replace("&view=table",'');
	linkAll = linkAll.replace("&view=map",'');
	linkAll = linkAll.replace("?view=table",'');
	linkAll = linkAll.replace("?view=map",'');
	var amountFind = linkAll.indexOf('?');
	var view = '';
	if(type=='table'){
		if(amountFind+1){
			view = '&view=table';
		}
		else {
			view = '?view=table';
		}
	}
	if(type=='map'){
		if(amountFind+1){
			view = '&view=map';
		}
		else {
			view = '?view=map';
		}
	}
	if($('.pager').length>0){
		$('.pager:first').find('li').each(function(){
			var href = $(this).find('a').attr('href');
			if(href!==undefined){
				href = href.replace("&view=table",'');
				href = href.replace("?view=table",'');
				href = href.replace("&view=map",'');
				href = href.replace("?view=map",'');
				$(this).find('a').attr('href',href+view);
			}
		});
	}
	var linkPage = linkAll+view;
	changeURLPage(linkPage);
	// loadRequestUnreload2('table');
}

function disabledInput(){
	setTimeout(function() {
		var estate = $('#filter_search .simple_search .checkbox_block.estate .select_checkbox ul li.checked a').data('type');
		var mainType = $('#filter_search .simple_search .checkbox_block.types .select_checkbox ul li.checked a').data('type');
		$('#filter_search .advanced_search .inner_block').each(function(){
			if($(this).is('.'+estate)){
				$(this).find('input[type="hidden"]').attr('disabled',false);
				$(this).find('.row').each(function(){
					if(!$(this).is('.'+mainType)){
						$(this).find('input[type="hidden"]').attr('disabled',true);
					}
				});
			}
			else {
				$(this).find('input[type="hidden"]').attr('disabled',true);
			}
		});
		// $('#filter_search .advanced_search .inner_block').each(function(){
			// if($(this).is(':hidden')){
				// $(this).find('input[type="hidden"],input[type="text"]').attr('disabled',true);
			// }
			// else {
				// $(this).find('input[type="hidden"],input[type="text"]').attr('disabled',false);
				// $(this).find('.row').each(function(){
					// if(!$(this).is('.'+mainType)){
						// $(this).find('input[type="hidden"],input[type="text"]').attr('disabled',true);
					// }
					// else {
						// $(this).find('input[type="hidden"],input[type="text"]').attr('disabled',false);
					// }
				// });
			// }
		// });
		$('#filter_search .simple_search .inner_block .change_block_filter').each(function(){
			if($(this).is(':hidden')){
				$(this).find('input[type="hidden"],input[type="text"]').attr('disabled',true);
			}
			else {
				$(this).find('input[type="hidden"],input[type="text"]').attr('disabled',false);
			}
		});
		
		$('#filter_search .simple_search .price_block').each(function(){
			var cession = $('#filter_search .change_block_filter.new input[type="hidden"][name="cession"]');
			var valCession = cession.val();
			var estate2 = estate;
			if(valCession==1 && cession.is(':enabled')){
				estate2 = 'cession';
			}
			if(!$(this).is('.'+estate2+'.'+mainType)){
				$(this).find('input[type="hidden"]').attr('disabled',true);
			}
			else {
				$(this).find('input[type="hidden"]').attr('disabled',false);
			}
		});
	}, 10);
}

function regUser(obj){
	var self = $(obj);
	var form = self.parents('form');
	if(self.is(':checked')){
		form.find('input[type="submit"]').attr('disabled',false);
	}
	else {
		form.find('input[type="submit"]').attr('disabled',true);
	}
}

// Форматирование колонки с числами
function formatPriceColumn(class_td) {
	var max_width = 0, width = 0, shift = 0;
	var cell_width = $('table').find('td.'+class_td).innerWidth();
	$('table').find('td.'+class_td).each(function() {
		$(this).css({'text-align':'right','padding':'0'});
		width = $(this).wrapInner('<span>').find('span').width();
		if (width > max_width) {
			max_width = width;
		}
	})
	shift = (cell_width-max_width)/2;
	$('table').find('td.'+class_td).each(function() {
		$(this).find('span').css('padding-right',shift);
	})
}

function theRotator() {
	$('#slider img').css({'opacity': '0'});
	$('#slider img:first').css({'opacity': '1'});
	setInterval('rotate()','4000');
}
 
function rotate() {	
	var current = ($('#slider img.show') ? $('#slider img.show') : $('#slider img:first'));
	var next = ((current.next().length) ? ((current.next().hasClass('show')) ? $('#slider img:first') :current.next()) : $('#slider img:first'));	
	next.css({opacity: 0})
	.addClass('show')
	.animate({'opacity': '1'}, 1000);
 
	current.animate({'opacity': '0'}, 1000)
	.removeClass('show');
};

function number_format (number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

function dropZone(){
	if($("div#dropzone").length>0){
		var idAd = arrLinkPage[arrLinkPage.length-1];
		idAd = idAd.split("?");
		if(idAd[1]!==undefined){
			idAd = idAd[1].split("=");
			if(idAd[0]=='id'){
				idAd = idAd[1];
			}
		}
		else {
			idAd = 0;
		}
		var est;
		if($('input[name="estateName"]').length>0){
			est = '&estate='+$('input[name="estateName"]').val();
		}
		$("div#dropzone").dropzone({
			url: "/admin_2/include/uploadImagePhotos.php?module=catalogue&id_ad="+idAd+est,
			init: function() {
				this.on("success", function(file){
					var type = $('input[name="typeName"]').val();
					var estate = $('input[name="estateName"]').val();
					var page = window.location+"?idParam="+estate+"&type="+type;
					if(idAd){
						page = window.location+"&idParam="+estate+"&type="+type;
					}
					$('.gallery_block .photos_list').load(page+' .gallery_block .photos_list ul',function(){
						dropZone();
					});
				});
			}
		});	
	}
}

function mainSettings(reload){
	if($('input.phone_mask').length>0){
		$('input.phone_mask').mask("999-99-99");
	}
	
	if($('input.phone_code').length>0){
		$('input.phone_code').mask("(999)");
	}
	
	if($('input.mask_phone').length>0){
		$('input.mask_phone').mask("+7(999) 999-99-99");
	}
	
	if($('input.mask_passport').length>0){
		$('input.mask_passport').mask("9999 999999");
	}
	
	dropZone();
	
	if(reload){
/* 		$('.reload_block .checkbox_single').click(function(){
			if(!$(this).is('.none')){
				var agency_true = false;
				var checkIds = [];
				if($(this).find('input[type="hidden"]').attr('name')=='s[agency_true]'){
					agency_true = true;
				}
				if($(this).is('.checked')){
					$(this).removeClass('checked');
					$(this).find('input[type="hidden"]:last').attr('disabled',true).removeAttr('value');
					if(agency_true){
						$(this).parents('.table_form').find('.row.agency').removeClass('show').find(':input').attr('disabled',true);
					}
				}
				else {
					$(this).addClass('checked');
					if(agency_true){
						$(this).parents('.table_form').find('.row.agency').addClass('show').find(':input').attr('disabled',false);
					}
					if($(this).find('input[type="hidden"]').attr('name')=='ready' || $(this).find('input[type="hidden"]').attr('name')=='s[ready]'){
						$(this).find('input[type="hidden"]:last').attr('disabled',false).val(5);
					}
					else {
						$(this).find('input[type="hidden"]:last').attr('disabled',false).val(1);
					}
				}
				if($(this).children('input[type="hidden"]').length==0 && $(this).parents('.checkbox_block').length>0){
					var id = $(this).data('id');
					$(this).parents('.checkbox_block').find('.checkbox_single').each(function(){
						if($(this).is('.checked')){
							checkIds.push($(this).data('id'));
						}
					});
					$(this).parents('.checkbox_block').children('input[type="hidden"]').val(checkIds);
				}
			}
		});
 */	}
	
	// $(document).unbind("click");
	$(document).bind('click', function(e){
		var $clicked = $(e.target);
		if(!$clicked.parents().hasClass("select_block")){
			$(".select_block").removeClass('open');
			$(document).unbind("keydown");
		}
	});
}

function typeCommerce(){
	// setTimeout(function() {
		var typeValue = $('input[name="type"]').val();
		var typeComm = $('.change_block_filter.commercial.'+typeValue).find('input[name="type_commer"]');
		var estateChange = $('#filter_search input[name="estate"]');
		var estateChangeValue = estateChange.val();
		var typeCommValue = typeComm.val();
		if(typeCommValue==0){
			typeCommValue = 1;
		}
		if(typeComm.is(':enabled') && $('.advance_open.commercial.'+typeValue).is('.open')){
			$('.comm_show_param').find('input[type="text"],input[type="hidden"],select').each(function(){
				if($(this).parents('.comm_show_param').is('.type_commer_'+typeCommValue)){
					if($(this).parents('.comm_show_param').is('.'+typeValue)){
						$(this).parents('.comm_show_param').removeClass('hidden');
						$(this).attr('disabled',false);
					}
					else {
						$(this).parents('.comm_show_param').addClass('hidden');
						$(this).attr('disabled',true);
					}
				}
				else {
					$(this).parents('.comm_show_param').addClass('hidden');
					$(this).attr('disabled',true);
				}
			});
		}
		
		// if(typeComm.is(':enabled') && estateChangeValue==5){
		if(estateChangeValue==5){
			var ch = $('.price_block.commercial.'+typeValue).find('input[type="hidden"][name="priceAll"]').val();
			$('.price_block.commercial.'+typeValue).each(function(){
				if($(this).is('.type_commer_'+typeCommValue)){
					$(this).find('input[type="hidden"]').attr('disabled',false);
					$(this).find('.prices_choose.'+ch).show().find('input[type="hidden"]').attr('disabled',false);
					$(this).show();
				}
				else {
					$(this).find('input[type="hidden"]').attr('disabled',true);
					$(this).find('.prices_choose').hide().find('input[type="hidden"]').attr('disabled',true);
					$(this).hide();
				}
			});
			var type_commer = $('.change_block_filter.commercial.'+typeValue+' input[name="type_commer"]');
			var type_commer_id = type_commer.val();
			// type_commer.parent().find('li a[data-id="'+type_commer_id+'"]').click();
			$('.price_block.commercial.'+typeValue).find('.prices_choose').hide().find('input[type="hidden"]').attr('disabled',true);
			$('.type_commer_'+typeCommValue+' .prices_choose.'+ch).show().find('input[type="hidden"]').attr('disabled',false);
		}
	// }, 10);
}

function typeCountry(){
	var typeValue = $('input[name="type"]').val();
	var typeComm = $('.change_block_filter.country.'+typeValue).find('input[name="type_country"]');
	var estateChange = $('#filter_search input[name="estate"]');
	var estateChangeValue = estateChange.val();
	var typeCommValue = typeComm.val();
	if(typeComm.is(':enabled') && $('.advance_open.country.'+typeValue).is('.open')){
		$('.count_show_param').find('input[type="text"],input[type="hidden"],select').each(function(){
			if($(this).parents('.count_show_param').is('.type_country_'+typeCommValue)){
				if($(this).parents('.count_show_param').is('.'+typeValue)){
					$(this).parents('.count_show_param').removeClass('hidden');
					$(this).attr('disabled',false);
				}
				else {
					$(this).parents('.count_show_param').addClass('hidden');
					$(this).attr('disabled',true);
				}
			}
			else {
				$(this).parents('.count_show_param').addClass('hidden');
				$(this).attr('disabled',true);
			}
		});
	}
}

// Формирование платежки
function sendFormPay(){
	if($('form#payUser').length>0){
		$('form#payUser').submit(function(e){
			e.preventDefault();
			var form = $(this);
			var data = form.serialize();
			if(form.find('input[name="cost"]').val()!='' && form.find('input[name="cost"]').val()!=0){
				$.ajax({
					type: "POST",
					url: '/include/handler.php',
					data: data+"&createPay=true",
					dataType : "json",
					success: function (data, textStatus) {
						if(data!==null){
							var json = eval(data);
							if(json.action!='error'){
								window.location = json.link;
							}
							else {
								alert(json.text);
							}
						}
						else {
							alert('Произошла ошибка! Попробуйте повторить позднее.');
						}
					},
					error: function(){
						alert('Произошла ошибка! Попробуйте повторить позднее.');
					}
				});
			}
			else {
				alert('Введите сумму для оплаты');
			}
		});
	}
}

function placement(){
	$('.placement_list .item a').on('click',function(){
		var id = parseInt($(this).data('id'));
		var cost = parseInt($(this).data('cost'));
		$(this).parent().parent().find('.item').removeClass('checked');
		$(this).parent().addClass('checked');
		$(this).parent().parent().find('input[type="hidden"]').val(id);
		$('.total_sum .sum_conditions strong').text(cost);
	});
}