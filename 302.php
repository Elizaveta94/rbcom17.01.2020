<?php
/*
*  Настройки для парсера из файла Excel
*/
$file = '302.xlsx';

$res = parse_excel_file($file);
for($c=1; $c<count($res); $c++){
	$link = trim($res[$c][1]);
	$linkRedirect = trim($res[$c][3]);
	$linkPage = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	if(is_url($link)){
		$linkPage = str_replace('&amp;','&',$linkPage);
		$link = str_replace('&amp;','&',$link);
		// echo $linkPage.'<br>';
		// echo $link.'<br><br>';
		if($linkPage==$link){
			header('Location: '.$linkRedirect, true, 301);
			exit;
		}
	}
}
?>