<?
ini_set('display_errors',1);
ini_set('error_reporting',2047);
ini_set('default_charset', 'utf-8');
mb_internal_encoding('utf-8');
mb_detect_order('utf-8');
include('../admin_2/include/bd.php');
include('../admin_2/include/functions.php');
include_once 'config.php';
include_once 'RoomFinderServiceHelper.php';

/*
*  Если цена в копейках, то true
*/
$sberbank = true;
$amount = 100; // цена в копейках
/*
*  Цена на оплату
*/
$payTotal = 100;
$ex_price = array($payTotal);
$phoneClientHotelFull = '';
$emailClientHotelFull = '';
if(isset($_REQUEST['emailClientHotelFull']) && !empty($_REQUEST['emailClientHotelFull'])){
	$emailClientHotelFull = $_REQUEST['emailClientHotelFull'];
}
if(isset($_REQUEST['phoneClientHotelFull']) && !empty($_REQUEST['phoneClientHotelFull'])){
	$phoneClientHotelFull = $_REQUEST['phoneClientHotelFull'];
}
if(isset($_REQUEST['payDefaultFullPrice']) && !empty($_REQUEST['payDefaultFullPrice'])){
	$payDefaultFullPrice = $_REQUEST['payDefaultFullPrice'];
}
if(isset($_REQUEST['payDefaultFullPriceAll']) && !empty($_REQUEST['payDefaultFullPriceAll'])){
	$payDefaultFullPriceAll = $_REQUEST['payDefaultFullPriceAll'];
}
if(isset($_REQUEST['payTotal']) && !empty($_REQUEST['payTotal'])){
	// $payTotal = preg_replace(",", '.', $_REQUEST['payTotal']);
	$payTotal = $_REQUEST['payTotal'];
	$ex_price = explode('.',$payTotal);
	$amount = $_REQUEST['payTotal'];
}
$priceDefault = $ex_price[0];
if($sberbank){
	$priceDefault = $priceDefault;
	$amount = $amount*100;
}
$numberAsFloat = (float)$priceDefault;
$PAYMENT_AMOUNT = number_format($numberAsFloat, 2, '.', '');

/*
*  Не изменные параметры
*/
$COMPANY_ID = $g_CompanyId;
$PAYMENT_GATEWAY_ID = (int)8;
$SecretKey = "kZ429p912R523t18Z46yJ7";

/*
*  Полная цена
*/
// $payTotalFull = preg_replace(",", '.', $_REQUEST['payTotalFull']);
$payTotalFull = $_REQUEST['payTotalFull'];
$ex_price_full = explode('.',$payTotalFull);
$priceFull = $ex_price_full[0];

/*
*  Номер бронирования
*/
$RESERVATION_ID = (string)$_REQUEST['ReservationId'];

/*
*  ФИО платящего
*/
$ClientFIO = "Тестов Тест Тестович";
if (isset($_REQUEST['ClientFIO']) && !empty($_REQUEST['ClientFIO'])) $ClientFIO = $_REQUEST['ClientFIO'];

/*
*  true - полная оплата, false - оплата за сутки
*/
$FULL_PREPAYMENT = "true";
$fullPrepayment = 1;
if($payDefaultFullPriceAll-$payDefaultFullPrice>0){ // если будет больше нуля, то false
	$FULL_PREPAYMENT = "false";
	$fullPrepayment = 0;
}

/*
*  Номер бронирования в двойном формате
*/
$CreateHashCode=(string)$_REQUEST['ReservationNumber'];

$reservationKey = $_REQUEST['ReservationKey'];

$HASH = md5($COMPANY_ID."|".$PAYMENT_GATEWAY_ID."|".$RESERVATION_ID."|".$SecretKey."|".$PAYMENT_AMOUNT."|".$ClientFIO);

$lang = "ru"; // язык
$orderNumber = $_REQUEST['ReservationNumber'];
$userName = 'kamarooms-api';
$password = 'jyMW5xJeeu9uCrPK';
// $password = 'kamarooms';
$returnUrl = urlencode('http://'.$_SERVER['HTTP_HOST'].'/finish.php');
$postdata = "amount=".$amount."&language=".$lang."&orderNumber=".$CreateHashCode."&password=".$password."&returnUrl=".$returnUrl."&userName=".$userName;
// Do POST
$ch = curl_init();
curl_setopt ($ch, CURLOPT_URL, "https://securepayments.sberbank.ru/payment/rest/register.do");
// curl_setopt ($ch, CURLOPT_URL, "https://3dsec.sberbank.ru/payment/rest/register.do");
curl_setopt ($ch, CURLOPT_POST, 1 );
curl_setopt ($ch, CURLOPT_POSTFIELDS, $postdata );
curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
$result = curl_exec ($ch);
curl_close($ch);
 
$json = json_decode($result);
$orderId = $json->orderId; // id транзакции
$formUrl = $json->formUrl; // ссылка на форму оплаты
$res = mysql_query("
	INSERT INTO payment (order_id,ReservationNumber,ReservationID,ReservationKey,payDefault,payTotal,payment,hash,orderId,PAYMENT_AMOUNT,ClientFIO,fullPrepayment,email,phone) 
	VALUES('".$GATEWAY_TRANSACTION_ID."','".$CreateHashCode."','".$RESERVATION_ID."','".$reservationKey."','".$priceDefault."','".$priceFull."','0','".$HASH."','".$orderId."','".$PAYMENT_AMOUNT."','".$ClientFIO."','".$fullPrepayment."','".$emailClientHotelFull."','".$phoneClientHotelFull."')
");
$payTotal = number_format($payTotal, 2, '.', '');
?>
<p>Внимание! После нажатия на кнопку <strong>"Перейти к оплате"</strong> Вы будете перемещены на сайт платежной системы.</p>
<div class="paysClass">
	<div class="TotalClass">Итого к оплате: <strong><?=$payTotal?> р.</strong></div>
	<a href="<?=$formUrl?>" style="width:180px!important;margin-top:20px" id="payBooking">Перейти к оплате</a>
</div>