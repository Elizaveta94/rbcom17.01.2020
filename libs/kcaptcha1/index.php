<?php
	/**
	 * Convert a hexa decimal color code to its RGB equivalent
	 *
	 * @param string $hexStr (hexadecimal color value)
	 * @param boolean $returnAsString (if set true, returns the value separated by the separator character. Otherwise returns associative array)
	 * @param string $seperator (to separate RGB values. Applicable only if second parameter is true.)
	 * @return array or string (depending on second parameter. Returns False if invalid hex color value)
	 */                                                                                                
	function hex2RGB($hexStr, $returnAsString = false, $seperator = ',') {
		$hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr); // Gets a proper hex string
		$rgbArray = array();
		if (strlen($hexStr) == 6) { //If a proper hex code, convert using bitwise operation. No overhead... faster
			$colorVal = hexdec($hexStr);
			$rgbArray[0] = 0xFF & ($colorVal >> 0x10);
			$rgbArray[1] = 0xFF & ($colorVal >> 0x8);
			$rgbArray[2] = 0xFF & $colorVal;
		} elseif (strlen($hexStr) == 3) { //if shorthand notation, need some string manipulations
			$rgbArray[0] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
			$rgbArray[1] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
			$rgbArray[2] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
		} else {
			return false; //Invalid hex color code
		}
		return $returnAsString ? implode($seperator, $rgbArray) : $rgbArray; // returns the rgb string or the associative array
	}

	//include('../../include/setup.php');
	include('kcaptcha.php');

	//session_name($_F_PREFIX);
	//session_save_path('../session');

	if(isset($_REQUEST[session_name()])){
		session_start();
	}

	$captcha = new KCAPTCHA();

	if($_REQUEST[session_name()]){
		$_SESSION['captcha_keystring'] = $captcha->getKeyString();
	}
?>