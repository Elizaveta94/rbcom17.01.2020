(function() {

    var triggerChangeURL = true
      , fotoramaInitialized, $fotorama, $toggleFullscreenText, $fullscreenDescription;


    init();


    function init() {
        updateVars();
        bindEvents();
    }

    function updateVars() {
        $fotorama              = $( '.fotorama' );
        $toggleFullscreenText  = $( '.js-fotorama-rsp-fullscreen__button' );
        $fullscreenDescription = $( '.fotorama-rsp-fullscreen-description' );
    }

    function bindEvents() {
        $toggleFullscreenText.on( 'click', toggleFullscreenText );

        $fotorama.on( 'fotorama:ready', fotoramaReady ).fotorama();
        $fotorama.on( 'fotorama:show fotorama:fullscreenenter', function( e, fotorama ) {
            redrawDescriptions( e, fotorama );
            changeURL( fotorama );
        });
        $fotorama.on( 'fotorama:fullscreenexit', function( e, fotorama ) {
            removeFullscreenDescription();
            changeURL( fotorama );
        });

        window.addEventListener && window.addEventListener( 'popstate', function( e ) {
            triggerChangeURL = false;
            setInitialSlideByURL( fotoramaInitialized );
        });
    }


    function toggleFullscreenText( event ) {
        var $button      = $( this )
          , $description = $( '.js-fotorama-rsp-fullscreen__description' );

        var status = $( this ).attr( 'data-status' )
          , attr   = 'short';

        if ( status === 'short' ) {
            var attr = 'full';
        }

        $button.attr( 'data-status', attr );
        $button.text( unescape($button.attr('data-'+attr)) );
        $description.html( unescape($description.attr('data-'+attr)) );

        return false;
    };

    function removeFullscreenDescription() {
        updateVars();
        $fullscreenDescription.remove();
    };

    function fotoramaReady( e, fotorama ) {
        fotoramaInitialized = fotorama;

        triggerChangeURL = false;
        setInitialSlideByURL( fotorama );
        $( e.target ).removeClass( 'fotorama-skin-rsp_hidden' );
    }

    function getSlideNumFromURL() {
        var url   = window.location.href
          , slide = url.split( '/' )
          , slide = slide[ slide.length-1 ].split( '.' )[ 0 ] / 1;  // convert to number
        return slide;
    }

    function setInitialSlideByURL( fotorama ) {
        var slide = getSlideNumFromURL() - 1;  // we need 0-index

        if ( fotorama.size >= slide ) {
            fotorama.show( slide );
        } else {
            fotorama.show( 0 );
        }
    }

    function changeURL( fotorama ) {
        var slide = getSlideNumFromURL() - 1;

        if ( triggerChangeURL === false ) {
            triggerChangeURL = true;
            return;
        }

        // ignore browsers that don't support history API
        if ( !history.pushState || typeof history.pushState !== 'function' ) {
            return;
        }

        // ignore when in fullscreen
        if ( $('.fotorama--fullscreen:eq(0)').length ) {
            return;
        }

        var url = window.location.href
          , url = url.split( '/' );

        // assume it's the ID of a slide
		
        if ( fotorama.size >= slide ) {
            url.pop();

        // assume it's news' id
        } else {
            var newsID = url.pop().split( '.' )[ 0 ];
            url.push( newsID );
        }

        url.push( (fotorama.activeIndex+1) );
        window.history.pushState( null, null, url.join('/') );
    }


    function redrawDescriptions( e, fotorama ) {
        removeFullscreenDescription();

        var description   = fotorama.activeFrame.rspdescription

          , $description = $( '.js-description-text' );

        $description.stop().fadeOut(function() {
            $description.html( description ).fadeIn();
        });


        if ( $('.fotorama--fullscreen:eq(0)').length ) {
            var $container = $( 'body' )
                data       = {
                    description     : description
                  , descriptionShort: ( description.length > 195 ) ? description.slice( 0, 195 ) + '...' : description
                  , buttonFull      : 'Скрыть описание'
                  , buttonShort     : 'Показать описание'
                };

            $container.append( tmpl('000001', data) );
        }
    }

    // Simple JavaScript Templating
    // John Resig - http://ejohn.org/ - MIT Licensed
    var cache={};var tmpl=function tmpl(c,b){var a=!/\W/.test(c)?cache[c]=cache[c]||tmpl(document.getElementById(c).innerHTML):new Function("obj","var p=[],print=function(){p.push.apply(p,arguments);};with(obj){p.push('"+c.replace(/[\r\t\n]/g," ").split("<%").join("\t").replace(/((^|%>)[^\t]*)'/g,"$1\r").replace(/\t=(.*?)%>/g,"',$1,'").split("\t").join("');").split("%>").join("p.push('").split("\r").join("\\'")+"');}return p.join('');");return b?a(b):a};
})();