<div class="modals" id="contacts">
<?
	echo '<h1>'.$row['name'].'</h1>';
	echo '
	<div id="feedback" class="module form">
		<div class="information">
			<div class="left">
				<p class="title">Наши телефоны:</p>
				<p><small>+7('.substr($_PHONE,1,3).')</small> '.substr($_PHONE,5,3).'-'.substr($_PHONE,8,2).'-'.substr($_PHONE,10,2).'</p>
				<!--<p><small>+7(812)</small> 123-76-76</p>-->
				<p class="email">E-mail: <a href="mailto:'.$email_info.'">'.$email_info.'</a></p>
			</div>
			<div class="center"><a href="/DV-Studio(brif).doc">Скачать бриф</a> на разработку сайта</div>
			<div class="right">
				<p class="graph"><span> График работы: <br>пн - пт <br><small>с</small> 10:00 <small>по</small> 19:00</span></p>
			</div>
		</div>
		<div class="line-on-the-page"></div>
		<h3 id="payments">Вы можете оплатить услуги любым удобным способом из нижепредставленных:</h3>
		<div id="pay">
			<ul>
				<li class="kiwi"><p>Киви кошелек</p></li>
				<li class="webmoney"><p>Web Money</p></li>
				<li class="yamoney"><p>Яндекс Деньги</p></li>
				<li class="visa"><p>Карта Visa</p></li>
			</ul>
		</div>
		<div class="line-on-the-page"></div>
		<h3>Задайте нам вопрос или сделайте online заказ:</h3>
		<form onsubmit="return sendOrderForm(this);" action="/include/handler.php" method="post">
			<input type="hidden" name="feedbackForm" value="1">
			<input type="hidden" name="placeForm" value="0">
			<input type="hidden" name="validEmail" value="0">
			<table id="formContacts" style="width:95%;margin: 0 auto;">
				<col style="width:235px">
				<col>
				<tr>
					<td><span>ФИО</span><b class="req">*</b>:</td>
					<td><input class="text" type="text" name="b_name" style="width:100%" value=""></td>
				</tr>
				<tr>
					<td><span>E-mail</span><b class="req">*</b>:</td>
					<td><input class="text" id="validate" type="text" name="b_email" style="width:50%" value=""></td>
				</tr>
				<tr>
					<td><span>Телефон</span><b class="req">*</b>:</td>
					<td><input class="text" id="phone_cont2" type="text" name="b_phone" style="width:142px" value=""></td>
				</tr>
				<tr>
					<td style="vertical-align:top;"><span style="position:relative;top:5px;">Комментарий:</span></td>
					<td><textarea rows="5" name="b_msg" style="width:100%"></textarea></td>
				</tr>
				<tr>
					<td colspan="2" style="text-align:center;"><input type="submit" style="width:150px" value="Отправить"></td>
				</tr>
			</table>
		</form>
		<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
		<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir"></div> 
	</div>';
?>
</div>