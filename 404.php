<? session_start();
include('admin_2/include/bd.php');
// include('admin_2/include/functions.php');
include('include/searchParams.php');

$ex_http_host = explode('.',$_SERVER['HTTP_HOST']);
if(!in_array('www',$ex_http_host) && count($ex_http_host)==2) {
	header("Status: 301 Moved Permanently");
	header(sprintf(
		'Location: https://www.%s%s',
		$_SERVER['HTTP_HOST'],
		$_SERVER['REQUEST_URI']
	));
}

if(in_array('www',$ex_http_host) && count($ex_http_host)==4) {
	$HTTP_HOST = str_replace('www.','',$_SERVER['HTTP_HOST']);
	header("Status: 301 Moved Permanently");
	header(sprintf(
		'Location: https://%s%s',
		$HTTP_HOST,
		$_SERVER['REQUEST_URI']
	));
}

if(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] !== 'on') {
    if(!headers_sent()) {
        header("Status: 301 Moved Permanently");
        header(sprintf(
            'Location: https://%s%s',
            $_SERVER['HTTP_HOST'],
            $_SERVER['REQUEST_URI']
        ));
        exit();
    }
}

$costAccount = 0;
$_costAccount = 0;
if(isset($_SESSION['idAuto']) || isset($_COOKIE['idAuto'])){ // Авторизованный пользователь
	if(isset($_COOKIE['idAuto'])){
		$SessionId = (int)$_COOKIE['idAuto'];
	}
	else {
		$SessionId = (int)$_SESSION['idAuto'];
	}
	$res = mysql_query("
		SELECT s.*,
		(SELECT cost FROM personal_account WHERE user_id='".$SessionId."') AS costAccount,
		(SELECT coords FROM ".$template."_location WHERE id=s.area_id) AS coords
		FROM ".$template."_site_users AS s
		WHERE s.id='".$SessionId."' && s.activation='1'
	") or mysql_error();
	if(mysql_num_rows($res)>0){
		$row = mysql_fetch_assoc($res);
		$costAccount = $row['costAccount'];
		$_costAccount = json_encode($costAccount);
		// $_costAccount = normJsonStr($_costAccount);
		$usersInfo = $row;
		$_SESSION['coords'] = $row['coords'];
		$_SESSION['idAuto'] = $row['id'];
		$activity = 1;
		lastOnSite($_SESSION['idAuto'],$activity);
	}
	else {
		unset($SessionId);
		unset($_SESSION['coords']);
		unset($_SESSION['idAuto']);
		unset($_COOKIE['idAuto']);
	}
}

if(isset($_GET['lang'])){
	$lang = $_GET['lang'];
	$lang == 'ru' ? $lang = 0 : $lang = 1;
	$lang == 'ru' ? $pref = '' : $pref = 'en/';
}
else {
	$lang = 0;
	$pref = '';
}

$ex_code = false;
$LEFT_JOIN = '';

$_CAB = false;
$_CAB_PAGES = array("","cabinet","edit_photo","add_journal");
if(in_array($linkOfPage,$_CAB_PAGES)){
	$_CAB = array_search($linkOfPage,$_CAB_PAGES);
}

// echo $linkOfPage;
// echo $linkLastStruct;
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>404 ошибка. Страница не найдена</title>
<meta name="yandex-verification" content="487ff2f43f216e6b" />
<meta property="og:title" content="404 ошибка. Страница не найдена">
<meta property="og:image" content="http://<?=$_SERVER['HTTP_HOST']?>/images/logo.png">
<? include("include/structure/head.php"); ?>
<script type="text/javascript">estateArray = <?=$estateArray?>;costAccount = <?=$_costAccount?>;<?isset($_SESSION['coords']) && !empty($_SESSION['coords'])?print "coordsPlace = [".$coordsParam.']':print"";?></script>
</head>

<body>
<div id="page">
	<div id="header">
		<? include("include/structure/header.php"); ?>
	</div>
	<div id="header_scroll">
		<div class="inner">
			<div class="menu">
				<div class="btn"><a onclick="return openMenu(this)" href="javascript:void(0)"></a></div>
				<div class="hidden_menu">
					<div class="angles">
						<div class="angle-top-border"></div>
						<div class="angle-top-white"></div>
					</div>
					<?initialMenu('','','top');?>
				</div>
			</div>
			<div class="logo-mini">
				<a href="/"></a>
			</div>
		</div>
	</div>
	<?include("include/structure/filter_search.php");?>
	<div id="error_page">
		<h1>Страница не найдена</h1>
		<p>Такой страницы у нас нет</p>
	</div>
	<div id="footer-cleaner"></div>
</div>
<div id="footer">
	<? include("include/structure/footer.php"); ?>
</div>
<div id="popup_blocks">
	<div class="feedback_form problem_block">
		<div class="fone_block"></div>
		<form action="/include/handler.php" method="post" onsubmit="return checkSideForm(this)">
			<div class="send_request">
				<input type="hidden" name="type_form" value="problema">
				<div class="title">Возникла проблема?</div>
				<div class="table">
					<div class="row">
						<div class="cell">
							<div class="label">Ваше имя<b>*</b></div>
						</div>
						<div class="cell">
							<div class="input_text required">
								<input type="text" name="s[name]">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="cell">
							<div class="label">Ваш e-mail<b>*</b></div>
						</div>
						<div class="cell">
							<div class="input_text required">
								<input type="text" name="s[email]">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="cell">
							<div class="label">Тема сообщения</div>
						</div>
						<div class="cell">
							<div class="input_text">
								<input type="text" name="s[theme]">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="cell">
							<div class="label">Сообщение</div>
						</div>
						<div class="cell">
							<div class="input_text"><textarea placeholder="Опишите Вашу проблему" name="s[comment]"></textarea></div>
						</div>
					</div>
					<div class="row">
						<div class="cell full comment center">Пожалуйста, введите символы с картинки</div>
					</div>
					<div class="row">
						<div class="cell captcha">
							<img width="150" height="50" id="kaptcha" src="/libs/kaptcha/index.php?<?=session_name().'='.session_id()?>">
						</div>
						<div style="margin-top:11px" class="cell">
							<div class="input_text required">
								<input style="width:220px;margin-left:20px" type="text" name="s[code]">
							</div>
						</div>
					</div>
				</div>
				<div class="btn">
					<label class="btn"><input type="submit" value="Отправить"><span class="angle-right"></span></label>
				</div>
				<div class="table">
					<div class="row">
						<div class="cell full comment">
							<div class="agreement">Отправляя это сообщение, Вы принимаете условия <a href="#">Пользовательского соглашения</a></div>
							<div class="req"><b>*</b> - поля обязательные для заполнения</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<div id="scroller" class="b-top"><span class="b-top-but"><i>Вверх</i></span><span class="b-bot-but"><i>Вниз</i></span></div>
<?if(count($params)==4 && $params[0]!='search' || (count($params)==2 && $params[0]=='flats') || (count($params)==2 && $params[0]=='newbuilding') || (count($params)==2 && $params[0]=='commercials') || (count($params)==2 && $params[0]=='countries') || (count($params)==2 && $params[0]=='rooms') || $_SERVER['REQUEST_URI']=='/card_new.php'){?>
<script src="https://code.jquery.com/jquery-1.8.3.min.js"></script>
<?}else{?>
<script type="text/javascript" src="/libs/jquery-1.7.1.min.js"></script>	
<?}?>
<!--<script type="text/javascript">p_main=<?=$p_main?></script>-->
<link type="text/css" rel="stylesheet" href="/libs/prettyPhoto/css/prettyPhoto.css">
<link rel="stylesheet" type="text/css" href="/libs/datepicer_redmond/jquery-ui-1.8.7.custom.css">
<link type="text/css" href="/libs/peppermint/css/peppermint.required.css" rel="stylesheet" />
<link type="text/css" href="/libs/peppermint/css/peppermint.suggested.css" rel="stylesheet" />
<link type="text/css" rel="stylesheet" href="/css/font-awesome.min.css" />
<link href="/libs/confirm/css/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/libs/fotorama/css/fotorama.css" rel="stylesheet" type="text/css">
<link href="/libs/fotorama/css/fotorama-skin.css" rel="stylesheet" type="text/css">
<!--<link href="/libs/jquery.imgareaselect-0.9.10/css/imgareaselect-animated.css" rel="stylesheet" type="text/css">
<link href="/libs/jquery.imgareaselect-0.9.10/css/imgareaselect-default.css" rel="stylesheet" type="text/css">
<link href="/libs/jquery.imgareaselect-0.9.10/css/imgareaselect-deprecated.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/libs/jquery.imgareaselect-0.9.10/js/jquery.imgareaselect.pack.js" ></script>-->
<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/x-icon" href="/images/favicon.png">
<!--<script type="text/javascript" src="/libs/jquery.price_format.js"></script>-->
<script type="text/javascript" src="/js/SearchParam.php"></script>


<script type="text/javascript" src="/js/f.js?ver=<?=$client_version;?>"></script>
<script type="text/javascript" src="/js/init.js?ver=<?=$client_version;?>"></script>
<script type="text/javascript" src="/libs/prettyPhoto/js/jquery.prettyPhoto.js?ver=<?=$client_version;?>"></script>
<script type="text/javascript" src="/libs/datepicer_redmond/jquery-ui-1.8.7.custom.min.js?ver=<?=$client_version;?>"></script>
<script type="text/javascript" src="/libs/datepicer_redmond/jquery.ui.datepicker-ru.js?ver=<?=$client_version;?>"></script>
<script type="text/javascript" src="/libs/ajaxForm/js/jquery.form.js?ver=<?=$client_version;?>"></script>
<script src="/libs/peppermint/js/peppermint.min.js?ver=<?=$client_version;?>" type="text/javascript"></script>
<script type="text/javascript" src="/libs/masonry/js/masonry.min.js"></script>
<script type="text/javascript" src="/libs/ajaxupload.3.5.js" ></script>
<script type="text/javascript" src="/libs/jquery.scrollbar/jquery.scrollbar.min.js" ></script>
<script src="/libs/confirm/js/jquery.alerts.js" type="text/javascript"></script>
<script  type="text/javascript" src="/libs/jquery.price_format.js"></script>
<script src="/libs/mask/jquery.maskedinput-1.4.0.js?ver=<?=$client_version;?>" type="text/javascript"></script>
<script type="text/javascript" src="/libs/dropzone/dropzone.js" ></script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44308839 = new Ya.Metrika({
                    id:44308839,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44308839" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>