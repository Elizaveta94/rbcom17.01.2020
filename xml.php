<?
header("content-type:text/html;charset=UTF-8");
include('admin_2/include/bd.php');
include('admin_2/include/functions.php');
class XmlToJson {
	public function Parse ($url) {
		$fileContents= @file_get_contents($url);
		$fileContents = str_replace(array("\n", "\r", "\t"), '', $fileContents);
		$fileContents = trim(str_replace('"', "'", $fileContents));
		$simpleXml = @simplexml_load_string($fileContents);
		$json = json_encode($simpleXml);
		return $simpleXml;
	}
}

$url = 'http://zemli78.ru/yandexfeed_no_duplicate.xml';
$json = @XmlToJson::Parse($url);
// echo '<pre>';
// print_r($json);
// echo '</pre>';
// return false;

$categoryArray = array("Дом"=>3,"Участок"=>4);
$lot_type = array("ИЖС"=>1,"ДНП"=>3,"Садоводство"=>4,"ДСК"=>6,"ЛПХ"=>6);
$loc_names = array(
	"Всеволожский район"=>26,
	"Ломоносовский район"=>33,
	"Гатчинский район"=>28,
	"Кировский район"=>31,
	"Выборгский район"=>27,
	"Тосненский район"=>39,
	"Приозерский район"=>36,
	"Лужский район"=>34,
	"Волховский район"=>25,
	"Волосовский район"=>24,
	"Красносельский"=>8,
    "Пушкинский"=>16,
    "Выборгский"=>3,
    "Петродворцовый"=>14,
    "Приморский"=>15,
    "Калининский"=>4,
);
$metro_names = array(
	"Удельная"=>61,
	"Гражданский Проспект"=>12
);
$locationArray = array();
if(!empty($json)){
	$offer = $json->offer;
	for($j=0; $j<count($offer); $j++){
		$arrayValues = array('activation=1');
		$imagesArray = array();
		$offers = json_encode($offer[$j]);
		$offers = str_replace('-','_',$offers);
		$offers = str_replace('@','',$offers);
		$offers = json_decode($offers);
		
		$category = $offers->category;
		if($category!='квартира'){
			array_push($arrayValues,"type_country='".$categoryArray[$category]."'");
			$load_id = $offers->attributes;
			$load_id = $load_id->internal_id;
			array_push($arrayValues,"load_id='".$load_id."'");

			$type = $offers->type;
			if($type=='продажа'){
				$type = 'sell';
			}
			else {
				$type = 'rent';
			}
			array_push($arrayValues,"type='".$type."'");
			
			$locations = $offers->location;
			// $location = $locations->country;
			$location = $locations->region;	
			$location2 = 1;
			if($location=='Ленинградская область'){
				$location2 = 2;
			}
			array_push($arrayValues,"location='".$location2."'");
			
			$region = $locations->district;
			if(!empty($region)){
				array_push($arrayValues,"area='".$loc_names[$region]."'");
			}
			else {
				$region = $locations->sub_locality_name;
				array_push($arrayValues,"area='".$loc_names[$region]."'");
			}
			
			$locality_name = $locations->locality_name;/**/
			$address = $locations->address;
			array_push($arrayValues,"address='".$address."'");
			
			$latitude = $locations->latitude;
			$longitude = $locations->longitude;
			$metro = $locations->metro;
			if(!empty($metro)){
				$metro = $metro->name;
				array_push($arrayValues,"station='".$metro_names[$metro]."'");
			}
			
			// if(!in_array($category,$categoryArray)){
				// array_push($categoryArray,$category);
			// }
			
			$coords = $longitude.' '.$latitude;
			array_push($arrayValues,"coords='".$coords."'");
			
			$prices = $offers->price;
			$price = $prices->value;
			array_push($arrayValues,"price='".$price."'");
			
			$url = $offers->url;
			array_push($arrayValues,"url='".$url."'");
			
			$mortgages = $offers->mortgage;// ипотека
			array_push($arrayValues,"mortgages='".$mortgages."'");
			
			$short_text = clearValueText($offers->description);
			array_push($arrayValues,"short_text='".$short_text."'");
			
			$areas = $offers->area;
			$lot_areas = $offers->lot_area;
			
			$full_square = $areas->value;
			array_push($arrayValues,"full_square='".$full_square."'");
			
			$area_square = $lot_areas->value;
			array_push($arrayValues,"area_square='".$area_square."'");
			
			$type_use = $offers->lot_type;
			array_push($arrayValues,"type_use='".$lot_type[$type_use]."'");
			
			$communications = $offers->electricity_supply;
			$communications2 = 0;
			if($communications){
				$communications2 = 3;
			}
			array_push($arrayValues,"communications='".$communications2."'");
			
			$images = $offers->image;
			if(!empty($images)){
				if(count($images)>1){
					for($i=0; $i<count($images); $i++){
						array_push($imagesArray,$images[$i]);
					}
				}
				else {
					array_push($imagesArray,$images);
				}
			}
			
			$plumbing = $offers->water_supply;
			$plumbing2 = 0;
			if($plumbing){
				$plumbing2 = 4;
			}
			array_push($arrayValues,"plumbing='".$plumbing2."'");
			
			$heating = $offers->heating_supply;
			$heating2 = 0;
			if($heating){
				$heating2 = 2;
			}
			array_push($arrayValues,"heating='".$heating2."'");
			
			$sewerage = $offers->sewerage_supply;
			$sewerage2 = 0;
			if($sewerage){
				$sewerage2 = 1;
			}
			array_push($arrayValues,"sewerage='".$sewerage2."'");
			
			$gas = $offers->gas_supply;
			$gas2 = 0;
			if($gas){
				$gas2 = 3;
			}
			array_push($arrayValues,"gas_supply='".$gas2."'");
			
			if(count($arrayValues)>0){
				$res = mysql_query("
					SELECT *
					FROM ".$template."_countries
					WHERE load_id='".$load_id."'
				");
				if(mysql_num_rows($res)>0){
					mysql_query("
						UPDATE ".$template."_countries
						SET ".implode(',',$arrayValues)."
						WHERE load_id='".$load_id."'
					");
				}
				else {
					mysql_query("
						INSERT INTO ".$template."_countries
						SET ".implode(',',$arrayValues)."
					") or die(mysql_error());
				}
				// if(count($imagesArray)>0){
					// for($i=0; $i<count($imagesArray); $i++){
						// mysql_query("
							// INSERT images_parser
							// SET load_id='".$load_id."',images='".$imagesArray[$i]."'
						// ");
					// }
				// }
			}
			
			echo '<pre>';
			print_r ( $arrayValues );
			echo '</pre>';
		}
		
		// echo '<pre>';
		// print_r($offers->manually_added);
		// echo '</pre>';
	}
}
