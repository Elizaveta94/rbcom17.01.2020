<div id="center">
	<h1><?=$titlePage?></h1>
	<h3>Пополнение счёта с помощью сервиса ROBOKASSA</h3>
	<form id="payUser" action="http://<?=$_SERVER['HTTP_HOST']?>/create_payment.php" method="post">
		<input name="cost" type="number" min="1" value="100">
		<label class="btn"><input type="submit" value="Пополнить"><span class="angle-right"></span></label>
	</form>
	<p style="font-size:16px" class="careful">Обратите внимание, что после нажатия на кнопку <strong>"Пополнить"</strong> вы перейдете на сайт платёжной системы.</p>
</div>