<div id="center">
<?
	echo '<h1>'.$titlePage.'</h1>';
	echo '<div style="margin:21px 0 -8px" class="separate"></div>';
	echo $textPage;
	
	$region = '<div class="select_block count">';
	$nameParams = 'Выберите регион';
	$regionsArray = array();
	$res = mysql_query("
		SELECT *
		FROM ".$template."_location
		WHERE activation='1'
		ORDER BY name
	");
	if(mysql_num_rows($res)>0){
		while($row = mysql_fetch_assoc($res)){
			$reg = '<li><a href="javascript:void(0)" data-id="'.$row['id'].'">'.$row['name'].'</a></li>';
			array_push($regionsArray,$reg);
		}
	}
	$region .= '<input type="hidden" name="s[region]">';
	$region .= '<div class="choosed_block">'.$nameParams.'</div>';
	$region .= '<div class="scrollbar-inner">';
	$region .= '<ul>';
	if(count($regionsArray)>0){
		$region .= implode('',$regionsArray);
	}
	$region .= '</ul>';
	$region .= '</div>';
	$region .= '</div>';
?>
	<div class="feedback_block">
		<div class="container_block">
			<div class="left_block">
				<form onsubmit="return checkSideUniForm(this)" action="/include/handler.php" method="POST">
					<input type="hidden" name="type_form" value="feedback">
					<div class="table_form">
						<div class="row">
							<div class="cell label right one_second">
								<label>Ваше Имя, Фамилия<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="text" name="s[name]">
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Ваш телефон</label>
							</div>
							<div class="cell one_second last_col">
								<input type="text" class="mask_phone" name="s[phone]">
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Ваш e-mail<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="text" name="s[email]">
							</div>
						</div>
						<div class="row">
							<div class="cell label one_second">&nbsp;</div>
							<div class="cell one_second last_col">
								<div class="checkbox_single">
									<input disabled="disabled" type="hidden" name="s[agency_true]">
									<div class="container">
										<span class="check"></span><span class="label">Я представляю агентство</span>
									</div>
								</div>
							</div>
						</div>
						<div class="row agency">
							<div class="cell label right one_second">
								<label>Агентство</label>
							</div>
							<div class="cell one_second last_col">
								<input disabled="disabled" type="text" name="s[agency]">
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Регион<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<?=$region?>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Тема сообщения</label>
							</div>
							<div class="cell one_second last_col">
								<input type="text" name="s[theme]">
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Сообщение<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<textarea name="s[message]"></textarea>
							</div>
						</div>
						<div class="row">
							<div class="cell label one_second">&nbsp;</div>
							<div class="cell one_second last_col">
								<div class="checkbox_single">
									<input disabled="disabled" type="hidden" name="s[delivery]">
									<div class="container">
										<span class="check"></span><span class="label">Я хочу получать новостную рассылку</span>
									</div>
								</div>
							</div>
						</div>
						<div style="margin-top:15px" class="row">
							<div class="cell label one_second">&nbsp;</div>
							<div class="cell one_second last_col">
								<script src='https://www.google.com/recaptcha/api.js'></script>
									<div class="g-recaptcha" data-sitekey="6Lc25CkUAAAAAGSUoAgMgNQkIdiYVivlI5xP0cPI"></div>
							</div>
						</div>
						<div class="row">
							<div class="cell label one_second">&nbsp;</div>
							<div class="cell one_second last_col">
								<div class="checkbox_single">
									<input disabled="disabled" type="hidden" name="agree">
									<div class="container">
										<span class="check"></span><span class="label">Нажимая кнопку «Отправить» я выражаю согласие на правила обработки персональных данных согласно <a href="/user-agreement" target="_blank">"Политике защиты персональной информации пользователей сайта"</a></span>
									</div>
								</div>
							</div>
						</div>
						<div class="row btn_row">
							<div class="cell label one_second">&nbsp;</div>
							<div class="cell one_second last_col">
								<label class="btn disabled"><input disabled="disabled" type="submit" value="Отправить"><span class="angle-right"></span></label>
							</div>
						</div>
						<div class="row">
							<div class="cell full">
								<div class="req"><b>*</b> - поля обязательные для заполнения</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="right_block">
				<h3>Или Вы можете связаться с нами напрямую</h3>
				<div class="cities">
					<div class="city">
						<table>
							<tr>
								<td>
									<!--<div class="city_name">Санкт-Петербург</div>
									<div class="phone"><?=$_MAINSET['phone']?></div>-->
									<div class="address"><?=$_MAINSET['address']?></div>
									<div class="mode">с Пн по Пт, с <span><?=$_MAINSET['mode_from']?></span> до <span><?=$_MAINSET['mode_to']?></span></div>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>