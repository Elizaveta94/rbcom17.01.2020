<?
// Хлебные крошки (эталон)
echo '<div id="center">
	<h1 class="inner_pages">'.$titlePage.'</h1>';
	// echo '<pre>';
	// print_r($PageInfoAd);
	// echo '</pre>';
	if($PageInfoAd['type']){
		$type = '<div class="select_block count disabled">';
		$not_active = '<i class="not_active"></i>';		
	}
	else {
		$type = '<div class="select_block count">';
		$not_active = '';
	}
	$types = '';
	$nameParams = 'Не выбран';
	$valueInput = '';
	$disabledType = '';
	for($t=0; $t<count($_TYPE_MAIN); $t++){
		$current = "";
		if(empty($PageInfoAd['type']) && $t==0){
			$current = ' class="current"';
			$nameParams = $_TYPE_MAIN[$t]['name'];
		}
		if($PageInfoAd['type']==$_TYPE_MAIN[$t]['value']){
			$current = ' class="current"';
			$nameParams = $_TYPE_MAIN[$t]['name'];
			$valueInput = ' value="'.$_TYPE_MAIN[$t]['value'].'"';
			$typeIsset = true;
			$disabledType = ' disabled="disabled"';
		}
		$types .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$_TYPE_MAIN[$t]['value'].'">'.$_TYPE_MAIN[$t]['name'].'</a></li>';
	}

	if(!$PageInfoAd['type']){
		$type .= '<input type="hidden" name="typeName"'.$valueInput.$disabledType.'>';
	}
	$type .= '<div class="choosed_block">'.$nameParams.'</div>';
	$type .= '<div class="scrollbar-inner">';
	$type .= '<ul>';
	if(!$PageInfoAd['type']){
		$type .= $types;
	}
	$type .= '</ul>';
	$type .= '</div>';
	$type .= $not_active;
	$type .= '</div>';
	
	if($PageInfoAd['estate']){
		$estate = '<div class="select_block count disabled">';
		$not_active = '<i class="not_active"></i>';		
	}
	else {
		$estate = '<div class="select_block count">';
		$not_active = '';
	}
	$estates = '';
	$nameParams = 'Не выбран';
	$typeId = 0;
	$valueInput = '';
	$disabledEstate = '';
	if($PageInfoAd['type']){
		$typeId = $PageInfoAd['type'];
	}
	for($t=0; $t<count($_ESTATE_MAIN[$typeId]); $t++){
		$current = "";
		if(empty($PageInfoAd['estate']) && $t==0){
			$current = ' class="current"';
			$nameParams = $_ESTATE_MAIN[$typeId][$t]['name'];
		}
		if($PageInfoAd['estate']==$_ESTATE_MAIN[$typeId][$t]['value']){
			$current = ' class="current"';
			$nameParams = $_ESTATE_MAIN[$typeId][$t]['name'];
			$valueInput = ' value="'.$_ESTATE_MAIN[$typeId][$t]['value'].'"';
			$disabledEstate = ' disabled="disabled"';
		}
		$estates .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$_ESTATE_MAIN[$typeId][$t]['value'].'">'.$_ESTATE_MAIN[$typeId][$t]['name'].'</a></li>';		
	}

	if(!$PageInfoAd['estate']){
		$estate .= '<input type="hidden" name="estateName"'.$valueInput.$disabledEstate.'>';
	}
	$estate .= '<div class="choosed_block">'.$nameParams.'</div>';
	$estate .= '<div class="scrollbar-inner">';
	$estate .= '<ul>';
	if(!$PageInfoAd['estate']){
		$estate .= $estates;
	}
	$estate .= '</ul>';
	$estate .= '</div>';
	$estate .= $not_active;
	$estate .= '</div>';
	
	/*
	*  Текст ЖК
	*/
	$build_text = '';
	if(!empty($PageInfoAd['build_text'])){
		$build_text = ' value="'.$PageInfoAd['build_text'].'"';
	}
	
	/*
	*  ID ЖК
	*/
	$build = '';
	if(!empty($PageInfoAd['p_main'])){
		$build = ' value="'.$PageInfoAd['p_main'].'"';
	}
	
	/*
	*  Наименование метро
	*/
	$station_id = '';
	if(!empty($PageInfoAd['station_id'])){
		$station_id = ' value="'.$PageInfoAd['station_id'].'"';
	}
	
	/*
	*  ID метро
	*/
	$station = '';
	if(!empty($PageInfoAd['station'])){
		$station = ' value="'.$PageInfoAd['station'].'"';
	}
	
	echo '<div class="text_block">
		'.$textPage.'
	</div>';
	
	$newbuildingRow = '<div class="row newbuildings new cession">
		<div class="cell label right one_second">
			<label>ЖК</label>
		</div>
		<div class="cell one_second last_col">
			<div class="request_block">
				<input autocomplete="off" type="text" class="text" name="s[build_text]"'.$build_text.'>
				<input type="hidden" class="text" name="s[build]"'.$build.'>
				<div class="search_request_block">
					<div class="scrollbar-inner">
						<ul></ul>
					</div>
				</div>
			</div>
		</div>
	</div>';
	
	if(isset($PageInfoAd) && ($PageInfoAd['estate']==2 || $PageInfoAd['estate']==3 || $PageInfoAd['estate']==4 || $PageInfoAd['estate']==5)){
		$newbuildingRow = '';
	}
	
	/*
	*  Шаги страницы
	*/
	$step1 = '<li class="current"><a href="/add?id='.$idAd.'">Расположение</a></li>';
	$step2 = '<li><span>Параметры</span></li>';
	$step3 = '<li><span>Стоимость</span></li>';
	$step4 = '<li><span>Условия размещения</span></li>';
	$step5 = '<li><span>Ваше объявление</span></li>';
	$btnPage = '<label class="btn"><input type="submit" value="Продолжить"><span class="angle-right"></span></label>';
	if($PageInfoAd['all_save']){
		$step1 = '<li class="current"><a href="/add?id='.$idAd.'">Расположение</a></li>';
		$step2 = '<li><a href="/add2?id='.$idAd.'">Параметры</a></li>';
		$step3 = '<li><a href="/add3?id='.$idAd.'">Стоимость</a></li>';
		$step4 = '<li><a href="/add4?id='.$idAd.'">Условия размещения</a></li>';
		$step5 = '<li><a href="/add5?id='.$idAd.'">Ваше объявление</a></li>';
		// $btnPage = '<label class="btn"><input type="submit" value="Сохранить изменения"><span class="angle-right"></span></label>';
	}
	
	// echo '<pre>';
	// print_r($PageInfoAd);
	// echo '</pre>';
	
	$stations = '';
	if(!empty($PageInfoAd['station_id'])){
		$distance = '';
		if(isset($PageInfoAd['dist_value']) && !empty($PageInfoAd['dist_value'])){
			$dist = floor($PageInfoAd['dist_value']).' м';
			if($dist/1000>=1){
				$dist = price_cell($dist/1000,2).' км';
			}
			$distance = '/ <strong>'.$dist.'</strong>';
		}
		$stations = '<li><div class="metro">«'.$PageInfoAd['station_id'].'» '.$distance.'<span onclick="return removeMetro(this)" class="close"></span></div></li>';
	}
		
	echo '<div class="add_block">
		<ul class="steps">'.$step1.$step2.$step3.$step4.$step5.'</ul>
		<div class="container_block ads">
			<div class="left_block">
				<form onsubmit="return saveAdsUsers(this)" action="/include/handler.php" method="POST">
					<input type="hidden" name="ads_save" value="true"> 
					<input type="hidden" name="step" value="1">
					<input type="hidden" name="s[dist_value]" value="'.$PageInfoAd['dist_value'].'">
					<div class="table_form">
						<div class="row">
							<div class="cell label right one_second">
								<label>Тип сделки<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								'.$type.'
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Тип недвижимости<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								'.$estate.'
							</div>
						</div>
						<div class="row">
							<div class="cell full separate"></div>
						</div>
						<div class="row">
							<div class="cell info full">
								<p>Размещение объявлений доступно только на территории Российской Федерации.</p>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Адрес<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<div class="request_block">
									<input type="text" class="text" name="s[address]" value="'.$PageInfoAd['address'].'">
									<div class="search_request_block">
										<div class="scrollbar-inner">
											<ul></ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						'.$newbuildingRow.'
						<div class="row">
							<div class="cell label right one_second">
								<label>Ближайшая станция метро</label>
							</div>
							<div class="cell one_second last_col">
								<div class="request_block">
									<input autocomplete="off" type="text" class="text" name="s[station_id]"'.$station_id.'>
									<input type="hidden" class="text" name="s[station]"'.$station.'>
									<div class="search_request_block">
										<div class="scrollbar-inner">
											<ul></ul>
										</div>
									</div>
									<ul class="stations">
										'.$stations.'
									</ul>
								</div>
							</div>
						</div>
						<div class="row btn_row">
							<div class="cell label one_second">&nbsp;</div>
							<div class="cell one_second last_col">
								'.$btnPage.'
							</div>
						</div>
						<div class="row">
							<div class="cell full">
								<div class="req"><b>*</b> - поля обязательные для заполнения</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="right_block">
				<div class="text_info">	
					<p>Убедитесь, что соблюдаете <a class="dash" href="javascript:void(0)">правила размещения</a> объявления!</p>
				</div>
				<div class="centers_block">';?>
					<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
					<style>
						#map {
							width:502px;height:435px;padding:0;margin:0;border:1px solid #ffffff;
						}
					</style>
					<?
					$coordsYandex = '';
					$ex_coords = array('30.315868','59.939095');
					if($PageInfoAd && empty($PageInfoAd['coords'])){
						$params2 = array(
							'geocode' => 'Санкт-Петербург, '.$PageInfoAd['address'],
							'format'  => 'json',
							'results' => 1,
							'key'     => 'AKDIIFYBAAAAqIYtRgIAZYb8P32hIxnbRRSe9CpggONv4PEAAAAAAAAAAADu6wMZJIEJ0SDd0mRe33ag1JRdNA==',
						);
						$response = json_decode(@file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params2)));
						
						if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0){
							$coordsYandex = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
						}
					}
					else {
						$coordsYandex = '30.315868 59.939095';
					}
					if(!empty($coordsYandex)){
						$ex_coords = explode(' ',$coordsYandex);
					}
					?>
					
					<script>
						ymaps.ready(function () {
							var myMap = new ymaps.Map('map', {
								center: [<?=$ex_coords[1]?>,<?=$ex_coords[0]?>],
								zoom: 13
							}, {
								searchControlProvider: 'yandex#search'
							}),
							myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
								hintContent: '<?=$PageInfoAd['name']?>',
								balloonContent: '<?=$PageInfoAd['address']?>'
							}, {
								// Опции.
								// Необходимо указать данный тип макета.
								iconLayout: 'default#image',
								// Своё изображение иконки метки.
								iconImageHref: '/images/neitralIcon.png',
								// Размеры метки.
								iconImageSize: [52, 44],
								// Смещение левого верхнего угла иконки относительно
								// её "ножки" (точки привязки).
								iconImageOffset: [-3, -42]
							});

							myMap.geoObjects.add(myPlacemark);
							var myRouter = ymaps.route([
							  'м. <?=$PageInfoAd['station_id']?>', {
								type: "viaPoint",                   
								point: "<?=$PageInfoAd['address']?>"
							  },
							], {
							  mapStateAutoApply: true 
							});
							
							myRouter.then(function(route) {
								/* Задание контента меток в начальной и 
								   конечной точках */
								var points = route.getWayPoints();
								points.options.set('preset', 'islands#blackStretchyIcon');
								points.get(0).properties.set("iconContent", 'м. <?=$PageInfoAd['station_id']?>');
								points.get(1).properties.set("iconContent", 'Объект');
								// Добавление маршрута на карту
								myMap.geoObjects.add(route);
								var routeLength = route.getHumanLength();
								var routeLength2 = route.getLength();
								// alert(routeLength2);
							  },
							  // Обработка ошибки
							  function (error) {
								// alert("Возникла ошибка: " + error.message);
							  }
							)
						});
						$(document).ready(function () {
							$('#center .add_block .table_form input[name="s[address]"]').keyup(function(){
								getCoords($(this));
							});
						});
					</script>
					<div class="map"><div id="map"></div></div>
					<?
				echo '</div>
			</div>
		</div>
	</div>
</div>';
?>