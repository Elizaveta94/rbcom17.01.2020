<?
$add_params = '';
$orderByFlats = 'num';
$LOAD_PAGE = 12;

$coordsArray = array();
$coordsArray2 = array();

// $res = mysql_query("
	// SELECT *
	// FROM ".$template."_second_flats
// ");
// if(mysql_num_rows($res)>0){
	// while($row = mysql_fetch_assoc($res)){
		// if($row['num_phone']){
			// mysql_query("
				// UPDATE ".$template."_second_flats
				// SET activation='0'
				// WHERE id='".$row['id']."'
			// ");
		// }
	// }
// }
if(count($params)==1){
	$res = mysql_query("
		SELECT SQL_CALC_FOUND_ROWS s.*,
		(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=s.id && estate='flats') AS images,
		(SELECT title FROM ".$template."_stations WHERE activation='1' && id=s.station) AS station_name,
		(SELECT name FROM ".$template."_location WHERE activation='1' && id=s.location) AS location_name
		FROM ".$template."_second_flats AS s
		WHERE s.activation='1'".$add_params."
		GROUP BY s.id
		ORDER BY ".$orderByFlats."
		LIMIT 0,".$LOAD_PAGE."
	");
	$requestSearch = '';
	if(mysql_num_rows($res)>0){
		$res2 = mysql_query("SELECT FOUND_ROWS()", $db);
		$max_rows = mysql_result($res2, 0);
		while($row = mysql_fetch_assoc($res)){
			$link = '/flats/'.$row['id'];
			$image = '<span>Нет фото</span>';
			if($row['images'] && !empty($row['images'])){
				$ex_image = explode(',',$row['images']);
				$image = '<img src="/admin_2/uploads/'.$ex_image[2].'">';
			}
			
			$metro = '';
			if(!empty($row['station'])){
				$metro = '<div class="metro">&laquo;'.$row['station_name'].'&raquo; / <strong>'.$row['distance'].'</strong></div>';
			}
			$name_title = $row['rooms'].'-комн. квартира';
			if($row['rooms']==0){
				$name_title = 'Квартира студия';
			}
			
			$square_meter = ceil($row['price']/$row['full_square']);
			$settings = '';
			$lift = 'нет';
			$wc = 'нет';
			$balcony = 'нет';
			$phone = 'нет';
			if(!empty($row['lift'])){
				$lift = 'есть';
			}
			if(!empty($row['wc'])){
				$wc = $_TYPE_WC[$row['wc']];
			}
			if(!empty($row['balcony'])){
				$balcony = $_TYPE_BALCONY[$row['balcony']];
			}
			if(!empty($row['phone'])){
				$phone = 'есть';
			}
			$floors_type = '&nbsp;'; //этаж 4/б
			if(!empty($row['floor'])){
				$floors_type = 'этаж '.$row['floor'];
			}
			if(!empty($row['floors'])){
				$floors_type .= '/'.$row['floors'];
			}
			$settings = '<span>Лифт: <i>'.$lift.'</i>,</span><span>Санузел: <i>'.$wc.'</i>,</span><span>Балкон: <i>'.$balcony.'</i>,</span><span>Телефон: <i>'.$phone.'</i>,</span>';
			$requestSearch .= '<div id="id_'.$row['id'].'" class="item">
				<div class="left_block">
					<div class="title"><a href="'.$link.'">'.$name_title.' <span><strong>'.str_replace(".", ",", price_cell($row['full_square'],1)).'/</strong>'.str_replace(".", ",", price_cell($row['live_square'],1)).' м<sup>2</sup></span></a></div>
					<div class="address">'.$row['address'].'</div>
					'.$metro.'
					<div class="image">
						<a href="'.$link.'">'.$image.'</a>
					</div>
				</div>
				<div class="right_block">
					<div class="row">
						<div class="ceil">
							<div class="full_price"><strong>'.price_cell($row['price'],0).'</strong> руб.</div>
							<div class="meter_price">'.price_cell($square_meter,0).' руб/м<sup>2</sup></div>
						</div>
						<div class="ceil">
							<div class="floor">'.$floors_type.'</div>
							<div class="type">Вторичка</div>
						</div>
					</div>
					<div class="row fone">
						<div class="footage">
							<div class="full">Общая <strong>'.str_replace(".", ",", price_cell($row['full_square'],1)).' м<sup>2</sup></strong></div>
							<div class="area">Жилая <strong>'.str_replace(".", ",", price_cell($row['live_square'],1)).' м<sup>2</sup></strong></div>
							<div class="rooms">Комнаты <strong>'.$row['rooms_square'].' м<sup>2</sup></strong></div>
							<div class="kitchen">Кухня <strong>'.$row['kitchen_square'].' м<sup>2</sup></strong></div>
						</div>
						<div class="settings">
							'.$settings.'
							<div class="more"><a href="'.$link.'">подробно</a></div>
						</div>
					</div>
				</div>
			</div>';
			
			// $params = array(
				// 'geocode' => $row['address'], // адрес
				// 'format'  => 'json',                          // формат ответа
				// 'results' => 1,                               // количество выводимых результатов
				// 'key'     => 'AKDIIFYBAAAAqIYtRgIAZYb8P32hIxnbRRSe9CpggONv4PEAAAAAAAAAAADu6wMZJIEJ0SDd0mRe33ag1JRdNA==',                           // ваш api key
			// );
			// $response = json_decode(@file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params)));
			
			// $coordsYandex = '';
			// if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0){
				// $coordsYandex = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
			// }
			// if(!empty($coordsYandex)){
				// $ex_coords = explode(' ',$coordsYandex);
				// $ex_coords2 = "[".$ex_coords[1].",".$ex_coords[0]."]";
				// array_push($coordsArray,$ex_coords);
				// array_push($coordsArray2,$ex_coords2);
			// }
			$title_search_page = 'Квартиры в г. '.$row['location_name'];
			if(isset($rooms_flat_sell_var) || isset($rooms_flat_rent_var)){
				$title_search_page = '';
				if(isset($rooms_flat_sell_var)){
					$roomsArray = $rooms_flat_sell_var;
				}
				if(isset($rooms_flat_rent_var)){
					$roomsArray = $rooms_flat_rent_var;
				}
				$exRoomsArray = explode(',',$roomsArray);
				$roomsArray = array();
				for($c=0; $c<count($exRoomsArray); $c++){
					$roomsName = 'Студии';
					if(!empty($exRoomsArray[$c])){
						$roomsName = $exRoomsArray[$c].'-комн.';
					}
					array_push($roomsArray,$roomsName);
				}
				$title_search_page = implode(', ',$roomsArray).' квартиры в г. '.$row['location_name'];
			}
		}
	}
	?>
	<div id="center">
		<h1 class="inner_pages">Вторичное жильё</h1>
		<div class="sort_offers">
			<div class="count_offers">Подходящих предложений:<span><?=$max_rows?></span></div>
			<div class="sort_block">
				<div class="label_select">Сортировать:</div>
				<div class="select_block price">
					<input type="hidden" name="sort">
					<div class="choosed_block">По умолчанию</div>
					<div class="scrollbar-inner">
						<ul>
							<li class="current"><a href="javascript:void(0)" data-id="1">По умолчанию</a></li>
							<li><a href="javascript:void(0)" data-id="1">По цене</a></li>
							<li><a href="javascript:void(0)" data-id="1">По метражу</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div id="result_search">
			<div class="list_items">
				<?=$requestSearch?>
			</div>
		</div>
		<!--<div id="pager"><ul><li class="prev"><a title="Предыдущая" href="#"><i class="fa fa-angle-left"></i><span>Предыдущая</span></a></li><li><a href="#">1</a></li><li><a href="#">2</a></li><li class=current><span>3</span></li><li><a href="#">4</a></li><li><a href="#">5</a></li><li class="next"><a title="Следующая" href="#"><span>Следующая</span><i class="fa fa-angle-right"></i></a></li></ul></div>-->
	</div>
<?}
if(count($params)==2){?>
<?
// echo '<pre>';
// print_r($PageInfo);
// echo '</pre>';
?>

<div id="center">
	<div class="title_page new">
		<div class="location_card">
			<h1 class="inner_pages"><?=$PageInfo['h1']?></h1>
			<?
			$address = $PageInfo['address'];
			$addressArray = array();
			if(!empty($PageInfo['short_address'])){
				$address = $PageInfo['short_address'];
			}
			$ex_address = explode(',',$address);
			$addss = '';
			for($a=0; $a<count($ex_address); $a++){
				$m = 0;
				if(count($ex_address)==1){
					$m = -1;
				}
				if(count($ex_address)==2){
					$m = -1;
				}
				if(count($ex_address)==4){
					$m = 0;
				}
				if(count($ex_address)==6){
					$m = 2;
				}
				if($a>$m){
					array_push($addressArray,$ex_address[$a]);
				}
			}
	
			$location_name = '';
			if(!empty($PageInfo['location_name'])){
				$l_name = $PageInfo['location_name'];
				$location_name = $l_name;
				if($l_name == 'Санкт-Петербург'){
					$location_name = 'СПб';
				}
				if($l_name == 'Ленинградская область' || $l_name == 'Ленинградская обл.'){
					$location_name = 'ЛО';
				}
				if($l_name == 'Московская область' || $l_name == 'Московская обл.'){
					$location_name = 'МО';
				}
				if($l_name == 'Москва'){
					$location_name = 'МСК';
				}
			}

			$area_name = '';
			$clearAreaName = str_replace('р-н','',$PageInfo['area_name']);
			$clearAreaName = str_replace('г.','',$clearAreaName);
			$clearAreaName = trim($clearAreaName);
			$pos = strripos($PageInfo['area_name'], 'р-н');
			if($pos===true){
				$area_name = $PageInfo['area_name'].' р-н';
			}
			else {
				$pos = strripos($PageInfo['area_name'], 'г.');
				$area_name = $PageInfo['area_name'];
			}
			
			if(count($addressArray)>0){
				$addss = implode(', ',$addressArray);
				$addss = str_replace('улица','ул.',$addss);
				$addss = str_replace('проспект','пр-т',$addss);
				$addss = str_replace('переулок','пер.',$addss);
				$addss = str_replace($clearAreaName,'',$addss);
				$addss = str_replace($locationName,'',$addss);
			}
			$address2 = '<div class="address">'.$location_name.', '.$area_name.', '.$addss.'</div>';
			echo $address2;


			if(!empty($PageInfo['station'])){
				$v = $PageInfo['dist_value'];
				if($v/1000>=1){
					$v = price_cell($v/1000,2).' км';
				}
				else {
					$v = price_cell($v,0).' м';
				}
				$coordsWay = $PageInfo['coords_area'];
				if(!empty($PageInfo['coords_station'])){
					$coordsWay = $PageInfo['coords_station'];
				}
				$coords = $PageInfo['coords'];
				$ex_coords = explode(',',$coords);
				if(count($ex_coords)>1){
					$coords = $ex_coords[1].' '.$ex_coords[0];
				}
				/*
				*  Расстояние по прямой
				*/
				$dist2 = streightDistance($coords,$coordsWay);
				if(!empty($dist2)){
					$v = $dist2;
				}
				echo '<div class="metro"><a href="/search?type=sell&estate=2&priceAll=all&station='.$PageInfo['station'].'">'.$PageInfo['station_name'].'</a><strong>'.$v.'</strong> | <a class="map" data-link="#map" href="#map">Показать на карте</a></div>';
				// echo '<div class="metro">«'.$PageInfo['station_name'].'» / <strong>'.$PageInfo['distance'].'</strong></div>';
			}
			else {
				$coordsWay = $PageInfo['coords_area'];
				if(!empty($PageInfo['coords_station'])){
					$coordsWay = $PageInfo['coords_station'];
				}
				if(!empty($PageInfo['coords']) && !empty($coordsWay)){
					$coords = $PageInfo['coords'];
					$ex_coords = explode(',',$coords);
					if(count($ex_coords)>1){
						$coords = $ex_coords[1].' '.$ex_coords[0];
					}
					/*
					*  Расстояние по прямой
					*/
					$dist2 = streightDistance($coords,$coordsWay);
					if(!empty($dist2)){
						$dist = $dist2;
					}
					
					$placeCoords = '';
					
					$typeCoords = '';
					$types = mysql_query("
						SELECT type
						FROM ".$template."_areas
						WHERE activation='1' && id='".$PageInfo['area']."' && coords!='' 
						LIMIT 1
					");
					if(mysql_num_rows($types)>0){
						$ts = mysql_fetch_assoc($types);
						$typeCoords = $ts['type'];
					}
					if(!empty($typeCoords)){
						$placeCoords = $typeCoords.' / ';
					}
					echo '<div class="place">'.$placeCoords.'<strong>'.$dist.'</strong> | <a class="map" data-link="#map" href="#map">Показать на карте</a></div>';
				}
			}
			$square_meter = ceil($PageInfo['price']/$PageInfo['full_square']);
			
			$rentText = '';
			$rentArray = array();
			if(!empty($PageInfo['communal'])){
				array_push($rentArray,'включая коммунальные платежи');
			}
			if(!empty($PageInfo['prepayment'])){
				if($PageInfo['type']=='rent'){
					if($PageInfo['prepayment']!=1){
						if($PageInfo['prepayment']<=100){
							array_push($rentArray,'Комиссия '.$PageInfo['prepayment'].'%');
						}
						else {
							array_push($rentArray,'Комиссия '.$PageInfo['prepayment'].' <span class="rub">Р</span>');
						}
					}
				}
			}
			if(!empty($PageInfo['deposit'])){
				if($PageInfo['type']=='rent'){
					array_push($rentArray,'Залог '.$PageInfo['deposit'].' <span class="rub">Р</span>');
				}
			}
			if(!empty($PageInfo['rental_holidays'])){
				array_push($rentArray,'арендные каникулы');
			}
			if(!empty($PageInfo['no_agent'])){
				array_push($rentArray,'агентам не звонить');
			}
			if(count($rentArray)>0){
				$rentText = '<div class="communal_block">'.implode('<br>',$rentArray).'</div>';
			}

			$meter_price = '';
			if(!empty($square_meter)){
				$meter_price = '<div class="meter_price">'.price_cell($square_meter,0).' <span class="rub">Р</span>/м<sup>2</sup></div>';
				if($PageInfo['type']=='rent'){
					$meter_price = '<div class="meter_price">'.price_cell($square_meter,0).' <span class="rub">Р</span>/м<sup>2</sup> в мес.</div>';
				}
			}
			
			$full_price = '<div class="full_price"><strong>'.price_cell($PageInfo['price'],0).'</strong> <span class="rub">Р</span></div>';
			if($PageInfo['type']=='rent'){
				$full_price = '<div class="full_price"><strong>'.price_cell($PageInfo['price'],0).'</strong> <span class="rub">Р</span>/мес.</div>';
			}
			?>
		</div>
		<div class="price_card">
			<div class="prices">
				<?=$full_price?>
				<?=$meter_price?>
				<?=$rentText?>
			</div>
		</div>
	</div>
	<div class="separate"></div>
	<div class="card_estate new_card" id="housing_estate">
		<?
			$res = mysql_query("
				SELECT *
				FROM ".$template."_photo_catalogue
				WHERE estate='flats' && p_main='".$PageInfo['id']."'
				ORDER BY cover DESC, num
			");
			if(mysql_num_rows($res)>0){
				echo '<div class="fotogallery">';
				echo '<div class="fotorama fotorama-skin-rsp fotorama-skin-rsp_hidden"
					 data-nav="thumbs"
					 data-height="421"
					 data-keyboard="true"
					 data-navposition="bottom"
					 data-loop="true"
					 data-allowfullscreen="native"
					 data-fit="contain">';
				while($row = mysql_fetch_assoc($res)){
					$ex_images = explode(',',$row['images']);
					if($row['cover']==1){
						$linkImgMid = '/admin_2/uploads/'.$ex_images[3];
						$linkImgMini = '/admin_2/uploads/'.$ex_images[4];
						if($row['user_id']!=0){
							$linkImgMid = '/users/'.$row['user_id'].'/'.$ex_images[3];
							$linkImgMini = '/users/'.$row['user_id'].'/'.$ex_images[4];
						}
						echo '<a href="'.$linkImgMini.'" data-thumb="'.$linkImgMid.'" data-full="'.$linkImgMini.'">
							<img src="'.$linkImgMini.'" />
						</a>';
					}
					else {
						$linkImgMid = '/admin_2/uploads/'.$ex_images[3];
						$linkImgMini = '/admin_2/uploads/'.$ex_images[4];
						if($row['user_id']!=0){
							$linkImgMid = '/users/'.$row['user_id'].'/'.$ex_images[3];
							$linkImgMini = '/users/'.$row['user_id'].'/'.$ex_images[4];
						}
						echo '<a href="'.$linkImgMini.'" data-thumb="'.$linkImgMid.'" data-full="'.$linkImgMini.'">
							<img src="'.$linkImgMini.'" />
						</a>';
					}
				}
				echo '</div>';
				echo '</div>';
				echo '<script type="text/javascript" src="/libs/fotorama/js/fotorama.js"></script>';
				echo '<script type="text/javascript" src="/libs/fotorama/js/_fotorama_2.js"></script>';
			}
			else {
				echo '<div class="fotogallery">';
				echo '<div class="fotorama fotorama-skin-rsp fotorama-skin-rsp_hidden"
					 data-nav="thumbs"
					 data-height="420"
					 data-keyboard="true"
					 data-navposition="bottom"
					 data-loop="true"
					 data-allowfullscreen="native"
					 data-fit="scaledown">
						<img src="/images/no_photo_650x420.png"/></div>';
				echo '</div>';
				echo '<script type="text/javascript" src="/libs/fotorama/js/fotorama.js"></script>';
				echo '<script type="text/javascript" src="/libs/fotorama/js/_fotorama_2.js"></script>';
			}
			
			/*
			*  ЖК
			*/
			$build_text = '';
			if(!empty($PageInfo['build_text'])){
				if(!empty($PageInfo['show_build'])){
					$build_text = '<div class="row">
						<div class="cell label">Название комплекса:</div>
						<div class="cell">'.$PageInfo['build_text'].'</div>
					</div>';
				}
			}
			
			/*
			*  Количество комнат
			*/
			$rooms = '';
			if(isset($PageInfo['rooms'])){
				$room = 'Студия';
				if($PageInfo['rooms']>0){
					$room = $PageInfo['rooms'];
				}
				$rooms = '<div class="row">
					<div class="cell label">Количество комнат:</div>
					<div class="cell">'.$room.'</div>
				</div>';
			}
			
			/*
			*  Тип дома
			*/
			$type_house = '';
			if(!empty($PageInfo['type_house'])){
				$r = mysql_query("
					SELECT name
					FROM ".$template."_type_house
					WHERE id='".$PageInfo['type_house']."'
				");
				if(mysql_num_rows($r)>0){
					$rw = mysql_fetch_assoc($r);
					$type_house = '<div class="row">
						<div class="cell label">Тип дома:</div>
						<div class="cell">'.$rw['name'].'</div>
					</div>';
				}
			}
			
			/*
			*  Кол-во этажей
			*/
			$floors = '';
			if(!empty($PageInfo['floor'])){
				if(!empty($PageInfo['floors'])){
					$floors = '<div class="row">
						<div class="cell label">Этаж:</div>
						<div class="cell">'.$PageInfo['floor'].' (из '.$PageInfo['floors'].')</div>
					</div>';
				}
				else {
					$floors = '<div class="row">
						<div class="cell label">Этаж:</div>
						<div class="cell">'.$PageInfo['floor'].'</div>
					</div>';
				}
			}
			
			/*
			*  Высота потолков
			*/
			$ceiling_height = '';
			if(!empty($PageInfo['ceiling_height']) && $PageInfo['type']=='rent'){
				$ceiling_height = '<div class="row">
					<div class="cell label">Высота потолков:</div>
					<div class="cell">'.str_replace(".", ",", $PageInfo['ceiling_height']).' м</div>
				</div>';
			}
			
			/*
			*  Отделка
			*/
			$finishing_value = '<span class="no">нет</span>';
			$finishing = '';
			if(!empty($PageInfo['finishing'])){
				$finishing_value = '<span class="ok">да</span>';
				$finishing = '<div class="row">
					<div class="cell label">Отделка:</div>
					<div class="cell places">'.$finishing_value.'</div>
				</div>';
			} 
			
			/*
			*  Год подстройки
			*/
			$year = '';
			if(!empty($PageInfo['year'])){
				$year = '<div class="row">
					<div class="cell label">Год подстройки:</div>
					<div class="cell places">'.$PageInfo['year'].'</div>
				</div>';
			} 
		?>
		<div class="card_info">
			<div class="info_housing">
				<div class="row first">
					<div class="cell label">ID: <?=$PageInfo['id']?></div>
					<div class="cell"><a class="add_favorite" href="javascript:void(0)"><span>В избранное</span></a></div>
				</div>
				<div class="row">
					<div class="cell label">Жилая недвижимость:</div>
					<div class="cell">Вторичка</div>
				</div>
				<?=$build_text?>
				<?=$rooms?>
				<?=$type_house?>
				<?=$floors?>
				<?=$ceiling_height?>
				<?=$finishing?>
				<?=$year?>
			</div>
			<?
				/*
				*  Общая площадь
				*/
				$full_square = '';
				if(isset($PageInfo['full_square']) && !empty($PageInfo['full_square'])){
					$full_square = '<div class="row">
						<div class="cell label">Общая площадь:</div>
						<div class="cell">'.$PageInfo['full_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Жилая площадь
				*/
				$live_square = '';
				if(isset($PageInfo['live_square']) && !empty($PageInfo['live_square'])){
					$live_square = '<div class="row">
						<div class="cell label">Жилая площадь:</div>
						<div class="cell">'.$PageInfo['live_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Площадь комнат
				*/
				$rooms_square = '';
				if(isset($PageInfo['rooms_square']) && !empty($PageInfo['rooms_square'])){
					$rooms_square = '<div class="row">
						<div class="cell label">Площадь комнат:</div>
						<div class="cell">'.$PageInfo['rooms_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Площадь кухни
				*/
				$kitchen_square = '';
				if(isset($PageInfo['kitchen_square']) && !empty($PageInfo['kitchen_square'])){
					$kitchen_square = '<div class="row">
						<div class="cell label">Площадь кухни:</div>
						<div class="cell">'.$PageInfo['kitchen_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Площадь кухни
				*/
				$wc = '';
				if(isset($PageInfo['wc']) && !empty($PageInfo['wc'])){
					$wc = '<div class="row">
						<div class="cell label">Санузел:</div>
						<div class="cell">'.$_TYPE_WC_ADMIN[$PageInfo['wc']].'</div>
					</div>';
				}
				
				/*
				*  Санузел
				*/
				$wc = '';
				if(isset($PageInfo['wc']) && !empty($PageInfo['wc'])){
					$wc = '<div class="row">
						<div class="cell label">Санузел:</div>
						<div class="cell">'.$_TYPE_WC_CAB[$PageInfo['wc']].'</div>
					</div>';
				}
				
				/*
				*  Лоджия
				*/
				$loggia = '';
				if(isset($PageInfo['loggia']) && !empty($PageInfo['loggia'])){
					$loggia = '<div class="row">
						<div class="cell label">Лоджия:</div>
						<div class="cell">'.$_TYPE_LOGGIA_CAB[$PageInfo['loggia']].'</div>
					</div>';
				}
				
				/*
				*  Балкон
				*/
				$balcony = '';
				if(isset($PageInfo['balcony']) && !empty($PageInfo['balcony'])){
					$balcony = '<div class="row">
						<div class="cell label">Балкон:</div>
						<div class="cell">'.$_TYPE_BALCONY_CAB[$PageInfo['balcony']].'</div>
					</div>';
				}
				if(isset($PageInfo['balcony_flat']) && !empty($PageInfo['balcony_flat'])){
					$value_balcony = $_TYPE_BALCONY_CAB[$PageInfo['balcony_flat']];
					if($PageInfo['balcony_flat']==1){
						$value_balcony = 'Есть';
					}
					$balcony = '<div class="row">
						<div class="cell label">Балкон:</div>
						<div class="cell">'.$value_balcony.'</div>
					</div>';
				}
				
				/*
				*  Лифт
				*/
				$liftArrays = array();
				$lifts = array();
				$liftServices = array();
				$lift_value = '<span class="no">нет</span>';
				$lift = '<div class="row">
					<div class="cell label">Лифт:</div>
					<div class="cell">'.$lift_value.'</div>
				</div>';
				$lift = '';
				$lift_service = '';
				
				/*
				*  Грузовой лифт
				*/
				if(isset($PageInfo['service_lift']) && !empty($PageInfo['service_lift'])){
					array_push($liftArrays,$_TYPE_LIFT[$PageInfo['service_lift']].' груз.');
				}
				/*
				*  Пассажирский лифт
				*/
				if(isset($PageInfo['lift']) && !empty($PageInfo['lift'])){
					$countLift = $_TYPE_LIFT[$PageInfo['lift']].' ';
					if($_TYPE_LIFT[$PageInfo['lift']]==1){
						$countLift = '';
					}
					array_push($liftArrays,$countLift.'пасс.');
				}
				
				
				if(count($liftArrays)>0){
					$lift = '<div class="row">
						<div class="cell label">Лифт:</div>
						<div class="cell"><span class="ok">'.implode(' + ',$liftArrays).'</span></div>
					</div>';
				}
				
				/*
				*  Паркинг
				*/
				$parking = '';
				$parking_value = '';
				if(isset($PageInfo['parking']) && !empty($PageInfo['parking'])){
					$parking_value = '<span class="ok">Есть</span>';
					$parking = '<div class="row">
						<div class="cell label">Паркинг:</div>
						<div class="cell">'.$parking_value.'</div>
					</div>';
				}
				
				/*
				*  Мебель
				*/
				$furniture = '';
				$furniture_value = '';
				if(isset($PageInfo['furniture']) && !empty($PageInfo['furniture'])){
					$furniture_value = '<span class="ok">Есть</span>';
					$furniture = '<div class="row">
						<div class="cell label">Мебель:</div>
						<div class="cell">'.$furniture_value.'</div>
					</div>';
				}
				
				/*
				*  Кухонная мебель
				*/
				$furniture_kitchen = '';
				$furniture_kitchen_value = '';
				if(isset($PageInfo['furniture_kitchen']) && !empty($PageInfo['furniture_kitchen'])){
					$furniture_kitchen_value = '<span class="ok">Есть</span>';
					$furniture_kitchen = '<div class="row">
						<div class="cell label">Кухонная мебель:</div>
						<div class="cell">'.$furniture_kitchen_value.'</div>
					</div>';
				}
				
				/*
				*  Можно с животными
				*/
				$animal = '';
				$animal_value = '';
				if(isset($PageInfo['animal']) && !empty($PageInfo['animal'])){
					$animal_value = '<span class="ok">Есть</span>';
					$animal = '<div class="row">
						<div class="cell label">Можно с животными:</div>
						<div class="cell">'.$animal_value.'</div>
					</div>';
				}
				
				/*
				*  Можно с детьми
				*/
				$children = '';
				$children_value = '';
				if(isset($PageInfo['children']) && !empty($PageInfo['children'])){
					$children_value = '<span class="ok">Есть</span>';
					$children = '<div class="row">
						<div class="cell label">Можно с детьми:</div>
						<div class="cell">'.$children_value.'</div>
					</div>';
				}
				
				/*
				*  Охраняемая территория
				*/
				$protected_area = '';
				$protected_area_value = '';
				if(isset($PageInfo['protected_area']) && !empty($PageInfo['protected_area'])){
					$protected_area_value = '<span class="ok">Есть</span>';
					$protected_area = '<div class="row">
						<div class="cell label">Охраняемая территория:</div>
						<div class="cell">'.$protected_area_value.'</div>
					</div>';
				}
				
				/*
				*  Консьерж
				*/
				$concierge = '';
				$concierge_value = '';
				if(isset($PageInfo['concierge']) && !empty($PageInfo['concierge'])){
					$concierge_value = '<span class="ok">Есть</span>';
					$concierge = '<div class="row">
						<div class="cell label">Консьерж:</div>
						<div class="cell">'.$concierge_value.'</div>
					</div>';
				}
				
				/*
				*  Телевизор
				*/
				$tv = '';
				$tv_value = '';
				if(isset($PageInfo['tv']) && !empty($PageInfo['tv'])){
					$tv_value = '<span class="ok">Есть</span>';
					$tv = '<div class="row">
						<div class="cell label">Телевизор:</div>
						<div class="cell">'.$tv_value.'</div>
					</div>';
				}
				
				/*
				*  Холодильник
				*/
				$fridge = '';
				$fridge_value = '';
				if(isset($PageInfo['fridge']) && !empty($PageInfo['fridge'])){
					$fridge_value = '<span class="ok">Есть</span>';
					$fridge = '<div class="row">
						<div class="cell label">Холодильник:</div>
						<div class="cell">'.$fridge_value.'</div>
					</div>';
				}
				
				/*
				*  Стиральная машина
				*/
				$washer = '';
				$washer_value = '';
				if(isset($PageInfo['washer']) && !empty($PageInfo['washer'])){
					$washer_value = '<span class="ok">Есть</span>';
					$washer = '<div class="row">
						<div class="cell label">Стиральная машина:</div>
						<div class="cell">'.$washer_value.'</div>
					</div>';
				}
				
				/*
				*  Интернет
				*/
				$internet = '';
				$internet_value = '';
				if(isset($PageInfo['internet']) && !empty($PageInfo['internet'])){
					$internet_value = '<span class="ok">Есть</span>';
					$internet = '<div class="row">
						<div class="cell label">Интернет:</div>
						<div class="cell">'.$internet_value.'</div>
					</div>';
				}
				
				/*
				*  Телефон
				*/
				$phone = '';
				$phone_value = '';
				if(isset($PageInfo['phone']) && !empty($PageInfo['phone'])){
					$phone_value = '<span class="ok">Есть</span>';
					$phone = '<div class="row">
						<div class="cell label">Телефон:</div>
						<div class="cell">'.$phone_value.'</div>
					</div>';
				}
				
				/*
				*  Мусоропровод
				*/
				$rubbish = '';
				// $name_value = '<span class="no">нет</span>';
				$name_value = '';
				if(isset($PageInfo['rubbish']) && !empty($PageInfo['rubbish'])){
					$name_value = '<span class="ok">Есть</span>';
					$rubbish = '<div class="row">
						<div class="cell label">Мусоропровод:</div>
						<div class="cell">'.$name_value.'</div>
					</div>';
				}
				
				/*
				*  Охраняемая территория
				*/
				$protected_area = '';
				$name_value = '';
				if(isset($PageInfo['protected_area']) && !empty($PageInfo['protected_area'])){
					$name_value = '<span class="ok">Есть</span>';
					$protected_area = '<div class="row">
						<div class="cell label">Охраняемая территория:</div>
						<div class="cell">'.$name_value.'</div>
					</div>';
				}
				
				/*
				*  Детская площадка
				*/
				$playground = '';
				$name_value = '';
				if(isset($PageInfo['playground']) && !empty($PageInfo['playground'])){
					$name_value = '<span class="ok">Есть</span>';
					$playground = '<div class="row">
						<div class="cell label">Детская площадка:</div>
						<div class="cell">'.$name_value.'</div>
					</div>';
				}
			?>
			<div class="info_housing">
				<?=$full_square?>
				<?=$live_square?>
				<?=$rooms_square?>
				<?=$kitchen_square?>
				<?=$wc?>
				<?=$balcony?>
				<?=$loggia?>
				<?=$lift?>
				<?=$lift_service?>
				<?
				if(!empty($deadline)){
					echo '<div class="row">
					<div class="cell label">Сдача ГК:</div>
					<div class="cell">'.$deadline.'</div>
				</div>';
				}
				?>
			</div>
			<div class="info_housing">
				<?=$furniture?>
				<?=$furniture_kitchen?>
				<?=$animal?>
				<?=$children?>
				<?=$parking?>
				<?=$concierge?>
				<?=$protected_area?>
				<?=$playground?>
				<?=$tv?>
				<?=$fridge?>
				<?=$washer?>
				<?=$internet?>
				<?=$phone?>
				<?
				$countTextSymbols = iconv_strlen($PageInfo['short_text'],'UTF-8');
				if(!empty($PageInfo['short_text']) && $countTextSymbols<=200){
					echo '<div class="row add">
						<div class="cell label">Дополнительно:</div>
						<div class="cell">'.nl2br($PageInfo['short_text']).'</div>
					</div>';
				}
				?>
			</div>
			<?
			$arrayBuy = array();
			if(isset($PageInfo['mortgages']) && !empty($PageInfo['mortgages'])){
				array_push($arrayBuy,'<li>Ипотека</li>');
			}
			if(isset($PageInfo['subsidies']) && !empty($PageInfo['subsidies'])){
				array_push($arrayBuy,'<li>Субсидии</li>');
			}
			if(isset($PageInfo['installment']) && !empty($PageInfo['installment'])){
				array_push($arrayBuy,'<li>Рассрочка</li>');
			}
			if(count($arrayBuy)>0){
				echo '<div class="ways_buy">
					<ul>
						'.implode('',$arrayBuy).'
					</ul>
				</div>';
				
			}

			$linkThisPage = urlencode('http://'.$_SERVER['HTTP_HOST'].'/'.$_SERVER['REQUEST_URI']);
			?>
			
			<div class="share_block">
				<ul>
					<li>Поделиться:</li>
					<li><a onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="http://vk.com/share.php?url=<?=$linkThisPage?>"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
					<li><a onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://www.facebook.com/sharer.php?src=sp&u=<?=$linkThisPage?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://plus.google.com/share?url=<?=$linkThisPage?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
					<li><a onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://twitter.com/intent/tweet?url=<?=$linkThisPage?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl=<?=$linkThisPage?>"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="clear"></div>
		<div class="text_info">
			<h2><?=$PageInfo['h1']?></h2>
			<?
			
			// echo '<pre>';
			// print_r($PageInfo);
			// echo '</pre>';
			if(!empty($PageInfo['short_text']) && $countTextSymbols>200){
					echo '<div class="text"><p>'.nl2br($PageInfo['short_text']).'</p></div>';
					echo '<div class="more_read"><a href="javascript:void(0)">Читать всё</a></div>';
			}
			else {
				if(!empty($PageInfo['text'])){
					echo '<div class="text"><p>'.strip_tags(nl2br($textPage)).'</p></div>';
					echo '<div class="more_read"><a href="javascript:void(0)">Читать всё</a></div>';
				}
			}
			?>
		</div>
		<!--<div class="more_info">
			<div class="near_objects">
				<div class="infrastructure_block">
					<div class="title">Инфраструктура</div>
					<ul>
						<li>
							<div class="item"><span class="school icon"></span>Школа имени Римского-Корсакого</div>
						</li>
						<li>
							<div class="item"><span class="kindergarten icon"></span>Детский сад №56</div>
						</li>
						<li>
							<div class="item"><span class="clinic icon"></span>Поликлиника им. Св. Марии</div>
						</li>
					</ul>
				</div>
				<div class="safety_block">
					<div class="title">Безопасность</div>
					<ul>
						<li>Огороженный периметр</li>
						<li>Видеонаблюдение</li>
						<li>Пропускная система</li>
						<li>Консьерж</li>
						<li>Охраняемая парковка</li>
						<li>Сигнализация</li>
					</ul>
				</div>
			</div>
			<div class="other_offers_flats">
				<div class="list_flats">
					<div class="row">
						<div class="name">Студия. от <strong>27,5 м<sup>2</sup></strong></div>
						<div class="price">от <strong><a href="#">995 000 руб.</a></strong></div>
					</div>
					<div class="row fone">
						<div class="name">1-комн. от <strong>36,4 м<sup>2</sup></strong></div>
						<div class="price">от <strong><a href="#">1 638 000 руб.</a></strong></div>
					</div>
					<div class="row">
						<div class="name">2-комн. от <strong>44,7 м<sup>2</sup></strong></div>
						<div class="price">от <strong><a href="#">1 922 100 руб.</a></strong></div>
					</div>
				</div>
			</div>
		</div>-->
		<div class="bottom_block">
			<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
			<style>#map {width:650px;height:780px;padding:0;margin:0}</style>
			<?
			// echo '<pre>';
			// print_r($PageInfo);
			// echo '</pre>';
			$coordsYandex = '';
			$ex_coords = array('30.315868','59.939095');
			if(empty($PageInfo['coords'])){
				$params2 = array(
					'geocode' => 'Санкт-Петербург, '.$PageInfo['address'],
					'format'  => 'json',
					'results' => 1,
					// 'key'     => 'AKDIIFYBAAAAqIYtRgIAZYb8P32hIxnbRRSe9CpggONv4PEAAAAAAAAAAADu6wMZJIEJ0SDd0mRe33ag1JRdNA==',
				);
				$response = json_decode(@file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params2)));
				
				if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0){
					$coordsYandex = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
				}
			}
			else {
				$coordsYandex = $PageInfo['coords'];
			}
			$coordsYandex = trim($coordsYandex);
			if(!empty($coordsYandex)){
				$ex_coords = explode(' ',$coordsYandex);
			}
					// echo $coordsYandex;
			
			?>
			
			
			<?if(empty($coordsYandex)){?>
				<script>
					ymaps.ready(function () {
						var myMap = new ymaps.Map('map', {
							center: [<?=$ex_coords[1]?>,<?=$ex_coords[0]?>],
							zoom: 13
						}, {
							searchControlProvider: 'yandex#search'
						}),
						myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
							hintContent: 'Метка на карте',
							balloonContent: 'г. <?=$PageInfo['city_name']?>, <?=$PageInfo['address']?>'
						}, {
							// Опции.
							// Необходимо указать данный тип макета.
							iconLayout: 'default#image',
							// Своё изображение иконки метки.
							iconImageHref: '/images/neitralIcon.png',
							// Размеры метки.
							iconImageSize: [52, 44],
							// Смещение левого верхнего угла иконки относительно
							// её "ножки" (точки привязки).
							iconImageOffset: [-3, -42]
						});

						myMap.geoObjects.add(myPlacemark);
						
						var address = "г. <?=$PageInfo['city_name']?>, <?=$PageInfo['address']?>";
						/* ymaps.geocode(address,{kind:'metro'}).then(function(res){
							var m0 = res.geoObjects.get(0);
							var m0_coords = m0.geometry.getCoordinates();
							var addressName = m0.properties.get('name');
						
							ymaps.geocode(m0_coords,{kind:'metro'}).then(function(res){
								var m0 = res.geoObjects.get(0);
								var m0_coords = m0.geometry.getCoordinates();
								var name = m0.properties.get('name');
								<?if(!empty($PageInfo['station_name'])){?>
									var myRouter = ymaps.route([
									  "г. <?=$PageInfo['city_name']?>, <?=$PageInfo['station_name']?>", {
										type: "viaPoint",                   
										point: addressName
									  },
									], {
									  mapStateAutoApply: true 
									});
								<?}else{?>
									var myRouter = ymaps.route([
									  name, {
										type: "viaPoint",                   
										point: addressName
									  },
									], {
									  mapStateAutoApply: true 
									});
								<?}?>
								
								myRouter.then(function(route) {
									var points = route.getWayPoints();
									points.options.set('preset', 'islands#blackStretchyIcon');
									points.get(0).properties.set("iconContent", 'Санкт-Петербург, м. <?=$PageInfo['station_name']?>');
									points.get(1).properties.set("iconContent", '<?=htmlspecialchars_decode($PageInfo['name'])?>');
									// Добавление маршрута на карту
									myMap.geoObjects.add(route);
									var routeLength = route.getHumanLength().toString();
									var routeLength2 = route.getLength();
									$('<div class="full_route">Расстояние от <span>Санкт-Петербург, м. <?=$PageInfo['station_name']?></span> до <span><?=htmlspecialchars_decode($PageInfo['address'])?></span>: <strong>'+routeLength+'</strong></div>').insertAfter('#map');
									var parent = arrLinkPage[1];
									var id = arrLinkPage[2];
									var data = "buildDist="+routeLength2+"&parent="+parent+"&id="+id;
									xhr = $.ajax({
										type: "POST",
										url: '/include/handler.php',
										data: data+"&rnd="+Math.random(),
										dataType: "json",
										success: function (data, textStatus) {
											var json = eval(data);
											if(json.action!='error'){
												// window.location = window.location;
											}
											else {
												alert('Возникла системная ошибка! Данные не сохранены. Попробуйте позднее.');
											}
										}
									});
								  },
								  // Обработка ошибки
								  function (error) {
									// alert("Возникла ошибка: " + error.message);
								  }
								)
							});
						}); */
							
					});
				</script>
			<?}else{?>
				<script>
				
					ymaps.ready(function(){
						var myMap = new ymaps.Map('map', {
								center: [<?=$ex_coords[1]?>,<?=$ex_coords[0]?>],
								zoom: 13
							}, {
								searchControlProvider: 'yandex#search'
							}),
							myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
								hintContent: '<?=$PageInfo['name']?>',
								balloonContent: '<?=$PageInfo['city_name']?>, <?=$PageInfo['data_address']?>'
							}, {
								// Опции.
								// Необходимо указать данный тип макета.
								iconLayout: 'default#image',
								// Своё изображение иконки метки.
								iconImageHref: '/images/<?=$_ICONS_MAPS[$PageInfo['rooms']]['card']?>',
								// Размеры метки.
								iconImageSize: [52, 44],
								// Смещение левого верхнего угла иконки относительно
								// её "ножки" (точки привязки).
								iconImageOffset: [-3, -42]
							});

						myMap.geoObjects.add(myPlacemark);
						// myMap.behaviors.disable('scrollZoom');
						
						/* var myRouter = ymaps.route([
						  'Санкт-Петербург, м. <?=$PageInfo['station_name']?>', {
							type: "viaPoint",                   
							point: [<?=$ex_coords[1]?>,<?=$ex_coords[0]?>]
						  },
						], {
						  mapStateAutoApply: true 
						});
						
						myRouter.then(function(route) {
							var points = route.getWayPoints();
							points.options.set('preset', 'islands#blackStretchyIcon');
							points.get(0).properties.set("iconContent", 'Санкт-Петербург, м. <?=$PageInfo['station_name']?>');
							points.get(1).properties.set("iconContent", '<?=htmlspecialchars_decode($PageInfo['name'])?>');
							// Добавление маршрута на карту
							myMap.geoObjects.add(route);
							var routeLength = route.getHumanLength();
							var routeLength2 = route.getLength();
							$('<div class="full_route">Расстояние от <span>Санкт-Петербург, м. <?=$PageInfo['station_name']?></span> до <span><?=htmlspecialchars_decode($PageInfo['name'])?></span>: <strong>'+routeLength+'</strong></div>').insertAfter('#map');
							var parent = arrLinkPage[1];
							var id = arrLinkPage[2];
							var data = "buildDist="+routeLength2+"&parent="+parent+"&id="+id;
							xhr = $.ajax({
								type: "POST",
								url: '/include/handler.php',
								data: data+"&rnd="+Math.random(),
								dataType: "json",
								success: function (data, textStatus) {
									var json = eval(data);
									if(json.action!='error'){
										// window.location = window.location;
									}
									else {
										alert('Возникла системная ошибка! Данные не сохранены. Попробуйте позднее.');
									}
								}
							});
							// alert(routeLength2);
						  },
						  // Обработка ошибки
						  function (error) {
							// alert("Возникла ошибка: " + error.message);
						  }
						)		 */			
						// var myGeocoder = ymaps.geocode(<?=$ex_coords[1]?>,<?=$ex_coords[0]?>, {kind: 'metro'}).then(function(res) {var nearest = res.geoObjects.get(0); name = nearest.properties.get('name');});

					});
				</script>
			<?}?>
			<?
			$verified = '<i class="verified"></i>';
			$verified = '';
			$userBlock = '';
			$styleForm = ' style="display:block;text-align:center;width:100%"';
			if(!empty($PageInfo['user_id'])){
				$userBlock = '<span class="id_block">ID: '.$PageInfo['user_id'].$verified.'</span>';
				$styleForm = '';
			}
			?>
			<div class="map"><div id="map"></div></div>
			<div class="feedback_block">
				<div class="representative">
					<div class="user_info">
						<span<?=$styleForm?> class="form">Представитель</span>
						<?=$userBlock?>
					</div>
					<div class="show_phone">
						<div class="btn_save top">
							<label class="btn"><input onclick="return show_phone(this,<?=$PageInfo['id']?>)" type="button" value="Показать ТЕЛЕФОН"><span class="angle-right"></span></label>
						</div>
					</div>
				</div>
				
				<div class="feedback_form">
					<form action="/include/handler.php" method="post" onsubmit="return checkSideForm(this)">
						<div class="send_request">
							<input type="hidden" name="type_form" value="application">
							<div class="title">Запросить информацию по объекту</div>
							<div class="table">
								<div class="row">
									<div class="cell">
										<div class="label">Ваше имя<b>*</b></div>
									</div>
									<div class="cell">
										<div class="input_text required">
											<input type="text" name="s[name]">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="cell">
										<div class="label">Ваш телефон<b>*</b></div>
									</div>
									<div class="cell">
										<div class="input_text required">
											<input class="phone" type="text" name="s[phone]">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="cell">
										<div class="label">Ваш e-mail<b>*</b></div>
									</div>
									<div class="cell">
										<div class="input_text required">
											<input type="text" name="s[email]">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="cell">
										<div class="label">Сообщение<b>*</b></div>
									</div>
									<div class="cell">
										<div class="input_text required"><textarea placeholder="Меня интересует этот объект недвижимости, пожалуйста, свяжитесь со мной..." name="s[msg]"></textarea></div>
									</div>
								</div>
								<div class="row">
									<div class="cell">
										<div class="label">&nbsp;</div>
									</div>
									<div class="cell" style="margin-right:-35px">
										<script src='https://www.google.com/recaptcha/api.js'></script>
										<div style="transform:scale(0.9);-webkit-transform:scale(0.9);transform-origin:0 0;-webkit-transform-origin:0 0;" class="g-recaptcha" data-sitekey="6Lc25CkUAAAAAGSUoAgMgNQkIdiYVivlI5xP0cPI"></div>
									</div>
								</div>
								<div class="row">
									<div class="cell">
										<div class="label">&nbsp;</div>
									</div>
									<div class="cell">
										<div class="checkbox_single">
											<input disabled="disabled" type="hidden" name="agree">
											<div class="container">
												<span class="check"></span><span class="label">Нажимая кнопку «Отправить» я выражаю согласие на правила обработки персональных данных согласно <a href="/user-agreement" target="_blank">"Политике защиты персональной информации пользователей сайта"</a></span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="btn">
								<label class="btn disabled"><input disabled="disabled" type="submit" value="Отправить"><span class="angle-right"></span></label>
							</div>
							<div class="table">
								<div class="row">
									<div class="cell full comment">
										<div class="req"><b>*</b> - поля обязательные для заполнения</div>
									</div>
								</div>
							</div>
							
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<?
		$res = mysql_query("
			SELECT *
			FROM ".$template."_articles
			WHERE activation=1
			ORDER BY date DESC,id DESC
			LIMIT 10
		");
		if(mysql_num_rows($res)>0){
			echo '<div class="news_events">';
			echo '<h3>Новости и статьи</h3>';
			echo '<div id="owl-demo">';
			while($row=mysql_fetch_assoc($res)){
				$images = '';
				$link = '/articles/'.$row['link'];
				if(!empty($row['images'])){
					$ex_avatar = explode(',',$row['images']);
					$images = '<div class="avatar"><img width="125" src="/admin_2/uploads/'.$ex_avatar[1].'"></div>';
				}
				echo '<div class="item">
					<div class="date">'.date_rus($row['date']).'</div>
					<div class="title"><a target="_blank" href="'.$link.'"><span>'.$row['name'].'</span></a></div>
				</div>';
			}
			echo '</div>';
			echo '</div><script src="/libs/owl-carousel/js/owl.carousel.js?ver=<'.time().'"></script>';
		}
		?>
		</div>
		
		<!--<div id="comments_block">
			<h3>Комментарии пользователей</h3>
			<div class="container_block">
				<div class="scrollbar-inner">
					<ul>
						<li>
							<div class="image"><a href="#"><img src="/images/avatar.jpg"></a></div>
							<div class="body">
								<div class="title">
									<div class="info">
										<a href="#">Вера Пчёлкина</a><strong>19:35</strong> | <span>25 Окт. 2016</span>
									</div>
									<div class="id_comment">#24590</div>
								</div>
								<div class="message_block">
									<div class="msg">Очевидно, что марксизм формирует прагматический культ личности. Механизм власти, как бы это ни казалось парадоксальным, теоретически возможен. Авторитаризм, короче говоря, верифицирует системный бихевиоризм.</div>
									<div class="btns">
										<div class="answer"><a href="#">Ответить</a></div>
										<div class="complain"><a href="#">Пожаловаться</a></div>
									</div>
								</div>
							</div>
							<ul>
								<li>
									<div class="image"><a href="#"><img src="/images/avatar.jpg"></a></div>
									<div class="body">
										<div class="title">
											<div class="info">
												<a href="#">Вера Пчёлкина</a><strong>19:35</strong> | <span>25 Окт. 2016</span>
											</div>
											<div class="id_comment">#24590</div>
										</div>
										<div class="message_block">
											<div class="msg">Очевидно, что марксизм формирует прагматический культ личности. Механизм власти, как бы это ни казалось парадоксальным, теоретически возможен. Авторитаризм, короче говоря, верифицирует системный бихевиоризм.</div>
											<div class="btns">
												<div class="answer"><a href="#">Ответить</a></div>
												<div class="complain"><a href="#">Пожаловаться</a></div>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</li>
						<li>
							<div class="image"><a href="#"><img src="/images/avatar.jpg"></a></div>
							<div class="body">
								<div class="title">
									<div class="info">
										<a href="#">Вера Пчёлкина</a><strong>19:35</strong> | <span>25 Окт. 2016</span>
									</div>
									<div class="id_comment">#24590</div>
								</div>
								<div class="message_block">
									<div class="msg">Очевидно, что марксизм формирует прагматический культ личности. Механизм власти, как бы это ни казалось парадоксальным, теоретически возможен. Авторитаризм, короче говоря, верифицирует системный бихевиоризм.</div>
									<div class="btns">
										<div class="answer"><a href="#">Ответить</a></div>
										<div class="complain"><a href="#">Пожаловаться</a></div>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="btn_info cost">
					<div class="table_form">
						<div class="row btn_row">
							<div class="cell center full last_col">
								<label class="btn link"><a class="back_page" href="javascript:void(0)"><span>Оставить сообщение</span></a></label>
								<label class="btn"><input type="submit" value="Перейти к обсуждению темы"><span class="angle-right"></span></label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>-->
		<?
		$res = mysql_query("
			SELECT c.*,
			(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=c.id && estate='flats' LIMIT 1) AS images,
			(SELECT title FROM ".$template."_stations WHERE activation='1' && id=c.station) AS station_name
			FROM ".$template."_second_flats AS c
			WHERE c.activation=1 && c.location=".$PageInfo['location']." && c.type='".$PageInfo['type']."' && c.rooms='".$PageInfo['rooms']."' && c.price>='".($PageInfo['price']-300000)."' && c.price<='".($PageInfo['price']+300000)."' && c.id!='".$PageInfo['id']."'
			ORDER BY RAND()
			LIMIT 4
		") or die(mysql_error());
		if(mysql_num_rows($res)>0){
			echo '<div id="related_offers">';
			echo '<h3>Похожие предложения</h3>';
			echo '<div class="container_block">';
			

			while($row = mysql_fetch_assoc($res)){
				$image = '<span><img src="/images/no_photo_238x156.png"/></span>';
				if(!empty($row['images'])){
					$ex_image = explode(',',$row['images']);
					$size = @getimagesize($_SERVER['DOCUMENTS_ROOT'].'/admin_2/uploads/'.$ex_image[3]);
					if(!empty($size[0])){
						$image = '<img src="/admin_2/uploads/'.$ex_image[3].'">';
					}
				}
				$photosList = '';
				$photos = mysql_query("
					SELECT *
					FROM ".$template."_photo_catalogue
					WHERE estate='flats' && p_main='".$row['id']."'
					LIMIT 1
				");
				if(mysql_num_rows($photos)>0){
					$ph = mysql_fetch_assoc($photos);
					$ex_images = explode(',',$ph['images']);
					if($ph['user_id']!=0){
						$photosList = '/users/'.$ph['user_id'].'/'.$ex_images[3];
					}
					else {
						$photosList = '/admin_2/uploads/'.$ex_images[3];
					}
					$image = '<span class="img" style="background-image:url('.$photosList.')"></span>';
				}
				
				$linkBuild = '/flats/'.$row['p_main'];
				$link = '/flats/'.$row['id'];
				$exploitation = explode('_',$row['exploitation']);
				$deadline = '';
				if(count($exploitation)>0){
					if(count($exploitation)>1){
						$deadline = '<span>'.$exploitation[1].' кв. '.$exploitation[0].'</span>';
					}
					else {
						$deadline = '<span>'.$exploitation[0].'</span>';
					}
				}
				
				$station_name = '';
				if(!empty($row['station_id'])){
					$r = mysql_query("
						SELECT title
						FROM ".$template."_stations
						WHERE id='".$row['station_id']."' && activation=1
					");
					if(mysql_num_rows($r)>0){
						$rw = mysql_fetch_assoc($r);
						$stat_name = $rw['title'];
						$link_station = '/search?type=sell&estate=2&priceAll=all&station='.$row['station'];
						$station_name = '<div class="station"><a href="'.$link_station.'">'.$stat_name.'</a><span class="dist">'.$row['dist_value'].' км</span></div>';
					}
				}
				
				echo '<div class="item">
					<div class="title_block">
						<a href="'.$linkBuild.'">'.$row['nameBuilds'].'</a>
						<div class="info_item">
							<div class="price">'.price_cell($row['price'],0).' <span class="rub">Р</span></div>
							<!--<div class="deadline">Срок сдачи: <span class="queue">'.$deadline.'</span></div>-->
						</div>
						<div class="image">
							<a href="'.$link.'">'.$image.'</a>
						</div>
						<div class="address">'.$row['address'].'</div>
						'.$station_name.'
					</div>
				</div>';
			}
			echo '</div>';
			echo '</div>';
		}
		?>
	</div>
	</div>
</div>	
<?}
if(count($params)==3){
	if($params[2]=='housing'){
		
	}
	if($params[2]=='flats'){
		
	}

?>
<?}
// if(count($params)==4){
	// include("include/mods/flat_open_new.php");
// }
?>