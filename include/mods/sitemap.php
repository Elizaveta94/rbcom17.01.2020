<div id="center">
	<div class="title_pages"><?=$titlePage?></div>
	<div id="sitemap">
		<table>
			<col style="width:33%">
			<col style="width:33%">
			<col style="width:33%">
			<tr>
				<td>
					<div class="item">
						<div class="title"><a href="/">Главная</a></div>
						<div class="title"><a href="/search/kupit/novostroyka/all">Купить квартиру в новостройках, Санкт-Петербург и Ленинградская обл.</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/kupit/novostroyka/all/studiya">студии</a></li>
								<li><a href="/search/kupit/novostroyka/all/1-komnata">1-комнатные</a></li>
								<li><a href="/search/kupit/novostroyka/all/2-komnata">2-комнатные</a></li>
								<li><a href="/search/kupit/novostroyka/all/3-komnata">3-комнатные</a></li>
								<li><a href="/search/kupit/novostroyka/all/4-komnata">4 и более комнат</a></li>
								<!--<li><a href="/search/kupit/novostroyka/all?cession=1">по переуступке</a></li>-->
							</ul>
						</div>
					</div>
					<div class="item">
						<div class="title"><a href="/search/kupit/novostroyka/all/leningradskaya-oblast">Квартиры в новостройке в ЛО</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/kupit/novostroyka/all/studiya/leningradskaya-oblast">студии</a></li>
								<li><a href="/search/kupit/novostroyka/all/1-komnata/leningradskaya-oblast">1-комнатные</a></li>
								<li><a href="/search/kupit/novostroyka/all/2-komnata/leningradskaya-oblast">2-комнатные</a></li>
								<li><a href="/search/kupit/novostroyka/all/3-komnata/leningradskaya-oblast">3-комнатные</a></li>
								<li><a href="/search/kupit/novostroyka/all/4-komnata/leningradskaya-oblast">4 и более комнат</a></li>
							</ul>
						</div>
					</div>
					<div class="item">
						<div class="title"><a href="/search/kupit/novostroyka/all/sankt-peterburg">Квартиры в новостройке, СПб</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/kupit/novostroyka/all/studiya/sankt-peterburg">студии</a></li>
								<li><a href="/search/kupit/novostroyka/all/1-komnata/sankt-peterburg">1-комнатные</a></li>
								<li><a href="/search/kupit/novostroyka/all/2-komnata/sankt-peterburg">2-комнатные</a></li>
								<li><a href="/search/kupit/novostroyka/all/3-komnata/sankt-peterburg">3-комнатные</a></li>
								<li><a href="/search/kupit/novostroyka/all/4-komnata/sankt-peterburg">4 и более комнат</a></li>
							</ul>
						</div>
					</div>
					<div class="item">
						<div class="title"><a href="/search/kupit/zagorodnaya/all">Купить загородную недвижимость</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/kupit/zagorodnaya/all/areas">Земельный участок в коттеджном поселке</a></li>
								<li><a href="/search/kupit/zagorodnaya/all/cottage">Коттедж в коттеджном поселке</a></li>
								<li><a href="/search/kupit/zagorodnaya/all/townhouse">Таунхаус</a></li>
								<li><a href="/search/kupit/zagorodnaya/all/house">Дом</a></li>
								<li><a href="/search/kupit/zagorodnaya/all/land-plot">Земельный участок</a></li>
							</ul>
						</div>
					</div>
					<div class="item">
						<div class="title"><a href="/search/snyat/komnata/all">Снять комнату</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/snyat/komnata/all/2-komnaty">в 2-комнатной квартире</a></li>
								<li><a href="/search/snyat/komnata/all/3-komnaty">в 3-х комнатной квартире</a></li>
								<li><a href="/search/snyat/komnata/all/4-komnaty">в 4-х комнатной квартире</a></li>
							</ul>
						</div>
					</div>
					<div class="item">
						<div class="title"><a href="/search/snyat/komnata/all/sankt-peterburg">Снять комнату в СПб</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/snyat/komnata/all/2-komnaty/sankt-peterburg">комната в 2-комнатной квартире</a></li>
								<li><a href="/search/snyat/komnata/all/3-komnaty/sankt-peterburg">комната в 3-х комнатной квартире</a></li>
								<li><a href="/search/snyat/komnata/all/4-komnaty/sankt-peterburg">комната в 4 и более-комнатной квартире</a></li>
							</ul>
						</div>
					</div>
					<div class="item">
						<div class="title"><a href="/search/snyat/komnata/all/leningradskaya-oblast">Снять комнату в ЛО</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/snyat/komnata/all/2-komnaty/leningradskaya-oblast">комната в 2-комнатной квартире</a></li>
								<li><a href="/search/snyat/komnata/all/3-komnaty/leningradskaya-oblast">комната в 3-х комнатной квартире</a></li>
								<li><a href="/search/snyat/komnata/all/4-komnaty/leningradskaya-oblast">комната в 4 и более-комнатной квартире</a></li>
							</ul>
						</div>
					</div>
					<div class="item">
						<div class="title"><a href="/search/snyat/kommercheskaya/all">Снять коммерческую недвижимость</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/snyat/kommercheskaya/all/office">офис</a></li>
								<li><a href="/search/snyat/kommercheskaya/all/premise">торговля, общепит, услуги</a></li>
								<li><a href="/search/snyat/kommercheskaya/all/auto">Мойка, автосервис</a></li>
								<li><a href="/search/snyat/kommercheskaya/all/building">здание (ОСЗ, БЦ, ТРК)</a></li>
								<li><a href="/search/snyat/kommercheskaya/all/factory">производство, склад</a></li>
								<li><a href="/search/snyat/kommercheskaya/all/areas">земельный участок</a></li>
								<li><a href="/search/snyat/kommercheskaya/all/business">готовый бизнес</a></li>
								<li><a href="/search/snyat/kommercheskaya/all/garage">Гараж, машиноместо</a></li>
							</ul>
						</div>
					</div>
					<div class="item">
						<div class="title"><a href="/search/snyat/kommercheskaya/all/sankt-peterburg">Снять коммерческую недвижимость в Санкт-Петербурге</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/snyat/kommercheskaya/all/office/sankt-peterburg">офис</a></li>
								<li><a href="/search/snyat/kommercheskaya/all/premise/sankt-peterburg">торговля, общепит, услуги</a></li>
								<li><a href="/search/snyat/kommercheskaya/all/auto/sankt-peterburg">Мойка, автосервис</a></li>
								<li><a href="/search/snyat/kommercheskaya/all/factory/sankt-peterburg">производство, склад</a></li>
								<li><a href="/search/snyat/kommercheskaya/all/areas/sankt-peterburg">земельный участок</a></li>
								<li><a href="/search/snyat/kommercheskaya/all/garage/sankt-peterburg">Гараж, машиноместо</a></li>
							</ul>
						</div>
					</div>
					<div class="item">
						<div class="title"><a href="/search/snyat/kommercheskaya/all/leningradskaya-oblast">Снять коммерческую недвижимость в Ленинградской обл.</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/snyat/kommercheskaya/all/office/leningradskaya-oblast">офис</a></li>
								<li><a href="/search/snyat/kommercheskaya/all/premise/leningradskaya-oblast">торговля, общепит, услуги</a></li>
								<li><a href="/search/snyat/kommercheskaya/all/auto/leningradskaya-oblast">Мойка, автосервис</a></li>
								<li><a href="/search/snyat/kommercheskaya/all/factory/leningradskaya-oblast">производство, склад</a></li>
								<li><a href="/search/snyat/kommercheskaya/all/areas/leningradskaya-oblast">земельный участок</a></li>
								<li><a href="/search/snyat/kommercheskaya/all/garage/leningradskaya-oblast">Гараж, машиноместо</a></li>
							</ul>
						</div>
					</div>
				</td>
				<td>
					<div class="item">
						<div class="title"><a href="/enter">Вход</a></div>
						<div class="title"><a href="/search/kupit/kvartira/all">Продажа квартир, комнат, домов, участков в СПб и ЛО</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/kupit/kvartira/all/studiya">студии</a></li>
								<li><a href="/search/kupit/komnata/all">комнаты</a></li>
								<li><a href="/search/kupit/kvartira/all/1-komnata">1-комнатные</a></li>
								<li><a href="/search/kupit/kvartira/all/2-komnata">2-комнатные</a></li>
								<li><a href="/search/kupit/kvartira/all/3-komnata">3-комнатные</a></li>
								<li><a href="/search/kupit/kvartira/all/4-komnata">4-комнатные</a></li>
								<li><a href="/search/kupit/kvartira/all/5-komnata">5 и более комнат</a></li>
								<!--<li><a href="/search/kupit/kvartira/all/sankt-peterburg">купить квартиру в СПб</a></li>
								<li><a href="/search/kupit/komnata/all/sankt-peterburg">купить комнату в СПб</a></li>
								<li><a href="/search/kupit/kvartira/all/leningradskaya-oblast">купить квартиру в ЛО</a></li>
								<li><a href="/search/kupit/komnata/all/leningradskaya-oblast">купить комнату в ЛО</a></li>
								<li><a href="/search/kupit/kvartira/all?share=1">долю в квартире</a></li>-->
							</ul>
						</div>
					</div>
					<div class="item">
						<div class="title"><a href="/search/kupit/kvartira/all/sankt-peterburg">Купить квартиру в СПБ</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/kupit/kvartira/all/studiya/sankt-peterburg">квартира-студия</a></li>
								<li><a href="/search/kupit/kvartira/all/1-komnata/sankt-peterburg">1-комнатную</a></li>
								<li><a href="/search/kupit/kvartira/all/2-komnata/sankt-peterburg">2-комнатную</a></li>
								<li><a href="/search/kupit/kvartira/all/3-komnata/sankt-peterburg">3-комнатную</a></li>
								<li><a href="/search/kupit/kvartira/all/4-komnata/sankt-peterburg">4-комнатную</a></li>
								<li><a href="/search/kupit/kvartira/all/5-komnata/sankt-peterburg">5 и более комнат</a></li>
							</ul>
						</div>
					</div>
					<div class="item">
						<div class="title"><a href="/search/kupit/kvartira/all/leningradskaya-oblast">Купить квартиру в ЛО</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/kupit/kvartira/all/studiya/leningradskaya-oblast">квартира-студия</a></li>
								<li><a href="/search/kupit/kvartira/all/1-komnata/leningradskaya-oblast">1-комнатную</a></li>
								<li><a href="/search/kupit/kvartira/all/2-komnata/leningradskaya-oblast">2-комнатную</a></li>
								<li><a href="/search/kupit/kvartira/all/3-komnata/leningradskaya-oblast">3-комнатную</a></li>
								<li><a href="/search/kupit/kvartira/all/4-komnata/leningradskaya-oblast">4-комнатную</a></li>
								<li><a href="/search/kupit/kvartira/all/5-komnata/leningradskaya-oblast">5 и более комнат</a></li>
							</ul>
						</div>
					</div>
					<div class="item">
						<div class="title"><a href="/search/kupit/kommercheskaya/all">Купить коммерческую недвижимость</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/kupit/kommercheskaya/all/office">офис</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/premise">торговля, общепит, услуги</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/auto">мойка, автосервис</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/building">здание (ОСЗ, БЦ, ТРК)</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/factory">производство, склад</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/areas">земельный участок</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/business">готовый бизнес</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/garage">гараж, машиноместо</a></li>
							</ul>
						</div>
					</div>
					<div class="item">
						<div class="title"><a href="/search/kupit/kommercheskaya/all/sankt-peterburg">Купить коммерческую недвижимость в СПб</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/kupit/kommercheskaya/all/office/sankt-peterburg">офис</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/premise/sankt-peterburg">торговля, общепит, услуги</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/auto/sankt-peterburg">мойка, автосервис</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/building/sankt-peterburg">здание (ОСЗ, БЦ, ТРК)</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/factory/sankt-peterburg">производство, склад</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/areas/sankt-peterburg">земельный участок</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/business/sankt-peterburg">готовый бизнес</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/garage/sankt-peterburg">гараж, машиноместо</a></li>
							</ul>
						</div>
					</div>
					<div class="item">
						<div class="title"><a href="/search/kupit/kommercheskaya/all/leningradskaya-oblast">Купить коммерческую недвижимость в ЛО</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/kupit/kommercheskaya/all/office/leningradskaya-oblast">офис</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/premise/leningradskaya-oblast">торговля, общепит, услуги</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/auto/leningradskaya-oblast">мойка, автосервис</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/building/leningradskaya-oblast">здание (ОСЗ, БЦ, ТРК)</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/factory/leningradskaya-oblast">производство, склад</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/areas/leningradskaya-oblast">земельный участок</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/business/leningradskaya-oblast">готовый бизнес</a></li>
								<li><a href="/search/kupit/kommercheskaya/all/garage/leningradskaya-oblast">гараж, машиноместо</a></li>
							</ul>
						</div>
					</div>
					<!--<div class="item">
						<div class="title"><a href="/realtors">Риелторы</a></div>
					</div>
					<div class="item">
						<div class="title"><a href="/developers">Застройщики</a></div>
					</div>
					<div class="item">
						<div class="title"><a href="/news">Новости</a></div>
					</div>
					<div class="item">
						<div class="title"><a href="/feedback">Обратная связь</a></div>
					</div>-->
					<div class="item">
						<div class="title"><a href="/news">Новости и статьи</a></div>
						<div class="title"><a href="/sitemap">Карта сайта</a></div>
						<div class="title"><a href="/contacts">Контакты</a></div>
						<div class="title"><a href="/reklama">Реклама на сайте</a></div>
						<div class="title"><a href="/feedback">Обратная связь</a></div>
						<div class="title"><a href="/dogovor">Договор оферты</a></div>
					</div>
				</td>
				<td>
					<div class="item">
						<div class="title"><a href="/reg">Регистрация</a></div>
						<div class="title"><a href="/search/kupit/komnata/all">Купить комнату на вторичном рынке</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/kupit/komnata/all/2-komnata">в 2-комнатной квартире</a></li>
								<li><a href="/search/kupit/komnata/all/3-komnata">в 3-х комнатной квартире</a></li>
								<li><a href="/search/kupit/komnata/all/4-komnata">в 4-х комнатной квартире</a></li>
								<li><a href="/search/kupit/komnata/all/5-komnata">в 5-х комнатной квартире</a></li>
							</ul>
						</div>
					</div>
					<div class="item">
						<div class="title"><a href="/search/kupit/komnata/all/sankt-peterburg">Купить комнату в СПБ</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/kupit/komnata/all/2-komnata/sankt-peterburg">комната в 2-комнатной квартире</a></li>
								<li><a href="/search/kupit/komnata/all/3-komnata/sankt-peterburg">комната в 3-х комнатной квартире</a></li>
								<li><a href="/search/kupit/komnata/all/4-komnata/sankt-peterburg">комната в 4-х комнатной квартире</a></li>
								<li><a href="/search/kupit/komnata/all/5-komnata/sankt-peterburg">комната в 5-х комнатной квартире</a></li>
								<li><a href="/search/kupit/komnata/all/6-komnata/sankt-peterburg">комната в 6 и более-комнатной квартире</a></li>
							</ul>
						</div>
					</div>
					<div class="item">
						<div class="title"><a href="/search/kupit/komnata/all/leningradskaya-oblast">Купить комнату в ЛО</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/kupit/komnata/all/2-komnata/leningradskaya-oblast">комната в 2-комнатной квартире</a></li>
								<li><a href="/search/kupit/komnata/all/3-komnata/leningradskaya-oblast">комната в 3-х комнатной квартире</a></li>
								<li><a href="/search/kupit/komnata/all/4-komnata/leningradskaya-oblast">комната в 4-х комнатной квартире</a></li>
								<li><a href="/search/kupit/komnata/all/5-komnata/leningradskaya-oblast">комната в 5-х комнатной квартире</a></li>
								<li><a href="/search/kupit/komnata/all/6-komnata/leningradskaya-oblast">комната в 6 и более-комнатной квартире</a></li>
							</ul>
						</div>
					</div>
					<div class="item">
						<div class="title"><a href="/search/snyat/kvartira/all">Аренда в СПб и ЛО</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/snyat/kvartira/all/studiya">студию</a></li>
								<li><a href="/search/snyat/komnata/all">комнаты</a></li>
								<li><a href="/search/snyat/kvartira/all/1-komnata">1-комнатные</a></li>
								<li><a href="/search/snyat/kvartira/all/2-komnata">2-комнатные</a></li>
								<li><a href="/search/snyat/kvartira/all/3-komnata">3-комнатные</a></li>
								<li><a href="/search/snyat/kvartira/all/4-komnata">4-комнатные</a></li>
								<li><a href="/search/snyat/zagorodnaya/all/house/townhouse/cottage">дома, коттеджи, таунхаусы</a></li>
							</ul>
						</div>
					</div>
					<div class="item">
						<div class="title"><a href="/search/snyat/kvartira/all/sankt-peterburg">Снять квартиру в Санкт-Петербурге</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/snyat/kvartira/all/studiya/sankt-peterburg">квартиру-студию</a></li>
								<li><a href="/search/snyat/kvartira/all/1-komnata/sankt-peterburg">1-комнатную</a></li>
								<li><a href="/search/snyat/kvartira/all/2-komnata/sankt-peterburg">2-комнатную</a></li>
								<li><a href="/search/snyat/kvartira/all/3-komnata/sankt-peterburg">3-комнатную</a></li>
								<li><a href="/search/snyat/kvartira/all/4-komnata/sankt-peterburg">4 и более комнат</a></li>
							</ul>
						</div>
					</div>
					<div class="item">
						<div class="title"><a href="/search/snyat/kvartira/all/leningradskaya-oblast">Снять квартиру в Ленинградской обл.</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/snyat/kvartira/all/studiya/leningradskaya-oblast">квартиру-студию</a></li>
								<li><a href="/search/snyat/kvartira/all/1-komnata/leningradskaya-oblast">1-комнатную</a></li>
								<li><a href="/search/snyat/kvartira/all/2-komnata/leningradskaya-oblast">2-комнатную</a></li>
								<li><a href="/search/snyat/kvartira/all/3-komnata/leningradskaya-oblast">3-комнатную</a></li>
								<li><a href="/search/snyat/kvartira/all/4-komnata/leningradskaya-oblast">4 и более комнат</a></li>
							</ul>
						</div>
					</div>
					<div class="item">
						<div class="title"><a href="/search/snyat/zagorodnaya/all">Снять загородную недвижимость</a></div>
						<div class="submenu">
							<ul>
								<li><a href="/search/snyat/zagorodnaya/all/cottage">Коттедж в коттеджном поселке</a></li>
								<li><a href="/search/snyat/zagorodnaya/all/cottage">Таунхаус</a></li>
								<li><a href="/search/snyat/zagorodnaya/all/house">Дом</a></li>
								<li><a href="/search/snyat/zagorodnaya/all/land-plot">Земельный участок</a></li>
							</ul>
						</div>
					</div>
					<!--<div class="item">
						<div class="title"><a href="/agencies">Агентства</a></div>
					</div>
					<div class="item">
						<div class="title"><a href="/company">Компании</a></div>
					</div>
					<div class="item">
						<div class="title"><a href="/reklama">Реклама на сайте</a></div>
					</div>
					<div class="item">
						<div class="title"><a href="/ipoteka">Ипотека</a></div>
					</div>
					<div class="item">
						<div class="title"><a href="/documents">Образцы документов</a></div>
					</div>-->
				</td>
			</tr>
		</table>
	</div>
</div>