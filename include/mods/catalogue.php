<?
	echo '<div id="breadcrumbs"><ul><li><a href="/">Главная</a></li>'.$parentBreadcrumb.'<li>'.$breadcrumbs.'</li></ul></div>';
	echo '<h1>'.$titlePage.'</h1>';	
	//Вывод списка товаров
	if(count($params)==1){
		echo $textPage;
		$link = $_GET['link'];
		$inner = $_GET['id'];
		$page_name = $linkOfPage.'/';
		$named_module = $template.'_m_catalogue_left';		
		$num = 15; // !!!!!Значение будет регулироваться админкой!!!!!
	
		// Инициализация pager
		@$pages = $_GET['pages'];
		$result00 = mysql_query("
			SELECT COUNT(*) 
			FROM ".$named_module." 
			WHERE link='".$link."' && activation='1'
		");
		$temp = mysql_fetch_array($result00);
		$posts = $temp[0];
		if($posts > 0) {
			$total = (($posts - 1) / $num) + 1;
			$total =  intval($total);
			$pages = intval($pages);
			if(empty($pages) or $pages < 0) $pages = 1;
			  if($pages > $total) $pages = $total;
			$start = $pages * $num - $num;
		}
		
		$p_main = " && p_main='0'";
		
		//Вывод списка товаров на странице
		$res = mysql_query("
			SELECT * 
			FROM ".$named_module."
			WHERE activation='1'".$p_main." && lang='".$lang."'
		",$db) or die(mysql_error());
		if (mysql_num_rows($res) > 0){
			echo '<div class="module catalogue">';
			while($row = mysql_fetch_assoc($res)){
				$link = '/'.$linkOfPage.'/'.$row["link"];
				$metka = $row["metka"];
				$price = '';
				if(empty($row['hide_price'])){
					$price = '<div class="price"><span>'.price_cell($row["price"],0).' '.$_MCUR[$row['currency']].'</span></div>';
				}
				$flag = '';
				$img = '';
				$no_foto = '<span>Нет фото</span>';
				if(!empty($row['images'])){
					$images = explode(',',$row['images']);
					$img = '<div class="img"><span class="margin"><a style="background: url(/admin_2/uploads/'.$images[0].') no-repeat center center;" href="'.$link.'" class="img"></a></span></div>';
					$no_foto = '';
				}

				if(!empty($metka)){
					$flag = '<span>'.$_MCAT[$metka].'</span>';
				}
				echo '<div class="block">
					'.$img.'
					<div class="description">
						<h3><a href="'.$link.'">'.$row["name"].'</a>'.$flag.'</h3>
							'.$price.'									
						<p>'.$row['short_text'].'</p>
						<p class="next"><a href="'.$link.'">Подробнее...</a></p>
					</div>    
				</div>';
			}
			echo '</div>';
		}
		else {
			echo '<h5>На данной странице товары отсутствуют</h5>';
		}

		// pager
		if ($page != 1) $pervpage = '<li class=first><a title="Первая" href="/'.$page_name.'&page=1">Первая</a></li><li class="prev"><a title="Предыдущая" href="/'.$page_name.'&page='.($page - 1).'">&laquo;</a></li> ';
		if ($page != $total) $nextpage = ' <li class=next><a title="Следующая" href="/'.$page_name.'&page='. ($page + 1) .'">&raquo;</a></li><li class="last"><a title="Последняя" href="/'.$page_name.'&page='.$total. '">Последняя</a></li>';
		if($page - 2 > 0) $page2left = ' <li><span><a href="/'.$page_name.'&page='. ($page - 2) .'">'. ($page - 2) .'</a></span></li> ';
		if($page - 1 > 0) $page1left = ' <li><span><a href="/'.$page_name.'&page='. ($page - 1) .'">'. ($page - 1) .'</a></span></li> ';
		if($page + 2 <= $total) $page2right = ' <li><span><a href="/'.$page_name.'&page='. ($page + 2) .'">'. ($page + 2) .'</a></span></li>';
		if($page + 1 <= $total) $page1right = ' <li><span><a href="/'.$page_name.'&page='. ($page + 1) .'">'. ($page + 1) .'</a></span></li>';
		if ($total > 1)
			{
				Error_Reporting(E_ALL & ~E_NOTICE);
				echo "<div id=\"pager\"><ul>";
				echo $pervpage.$page5left.$page4left.$page3left.$page2left.$page1left.'<li class=current><span>'.$page.'</span></li>'.$page1right.$page2right.$page3right.$page4right.$page5right.$nextpage;
				echo "</ul></div>";
			}
	}
	
	// Карточка товара
	if(count($params)==2){
		$res = mysql_query("
			SELECT * 
			FROM ".$template."_m_catalogue_left 
			WHERE link='".$linkLastStruct."' 
			ORDER BY num
		",$db);
		$row = mysql_fetch_assoc($res);
		$id = $row['id'];
		
		echo '<div class="module article catalogue">';
		
		//Цена если есть
		if(empty($row['hide_price'])){
			echo '<div class="price">
				<span class="pricer">'.price_cell($row["price"],0).'<span> '.$_MCUR[$row['currency']].'</span></span>
			</div>';
		}
			  
		//Если есть картинка и она не скрыта выводим её
		if(!empty($row['images'])){
			$images = explode(',',$row['images']);
			echo '<p><a title="'.$row["name"].'" rel="prettyPhoto" href="/admin_2/uploads/'.$images[3].'"><img alt="" src="/admin_2/uploads/'.$images[2].'" class="left_float"></a></p>';
		}
		
		//Полный текст товара
		echo $textPage;
		
		//Если есть дополнительные картинки выводим их тут
		$res = mysql_query("
			SELECT * 
			FROM ".$template."_photo_catalogue 
			WHERE activation='1' && p_main='".$id."' 
			ORDER BY id DESC 
		",$db);
		if (mysql_num_rows($res) > 0) {
			$row = mysql_fetch_assoc($res);
			echo '<div class="addon_img">';
			while($row = mysql_fetch_assoc($res)){
				$images = explode(',',$row['images']);
				echo '<a rel="prettyPhoto" title="" href="/admin_2/uploads/'.$images[3].'"><img class="no_float" src="admin_2/uploads/'.$images[1].'" alt=""></a>';
			}
			echo '</div>';
		}
		
		//Переход к каталогу товаров
		echo '<p class="both"><a href="/'.$linkOfPage.'">Назад...</a></p>';
		echo '</div>';
	}
?>