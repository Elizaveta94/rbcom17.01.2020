<div id="center">
	<div class="title_page new">
		<div class="location_card">
			<h1 class="inner_pages"><?=$PageInfo['h1']?> </h1>
			<div class="area"><a href="#"><?=$PageInfo['region']?></a></div>
			<div class="address"><?=$PageInfo['data_address']?></div>
			<?
			if(!empty($PageInfo['station'])){
				$v = $PageInfo['dist_value'];
				if(empty($PageInfo['dist_value'])){
					$v = $PageInfo['dValue'];
				}
				if(!empty($PageInfo['rid'])){
					$v = $v.' км';
				}
				else {
					if($v/1000>=1){
						$v = price_cell($v/1000,2).' км';
					}
					else {
						$v = price_cell($v,0).' м';
					}
				}
				echo '<div class="metro"><a href="#">'.$PageInfo['station_name'].'</a><strong>'.$v.'</strong> | <a class="map" data-link="#map" href="#map">Показать на карте</a></div>';
			}
			$exploitation = explode('_',$PageInfo['exploitation']);
			$deadline = '';
			if(!empty($PageInfo['exploitation'])){
				if(count($exploitation)>0){
					if(count($exploitation)>1){
						$deadline = '<span>'.$exploitation[1].' кв. '.$exploitation[0].'</span>';
					}
					else {
						$deadline = '<span>'.$exploitation[0].'</span>';
					}
				}
			}
			else {
				$deadline = '';
				$queues = mysql_query("
					SELECT commissioning,queue
					FROM ".$template."_queues
					WHERE p_main='".$PageInfo['id']."' && type_name='new'
					GROUP BY queue
				");
				if(mysql_num_rows($queues)>0){
					$deadline .= '<span>';
					$deadlineArray = array();
					while($queue = mysql_fetch_assoc($queues)){
						array_push($deadlineArray,$queue['queue'].' очередь - '.$queue['commissioning']);
					}
					for($q=0; $q<count($deadlineArray); $q++){
						$deadline .= implode(', ',$deadlineArray);
					}
					$deadline .= '</span>';
				}
			}
			
			$type_developer = '';
			$type_deadline = '';
			if(!empty($PageInfo['name_developer'])){
				$type_developer = '<div class="type">Застройщик: <a href="'.$link_developer.'">'.$PageInfo['name_developer'].'</a></div>';
			}
			if(!empty($deadline)){
				$type_deadline = '<div class="type">Срок сдачи: '.$deadline.'</div>';
			}
			
			$square_meter = ceil($PageInfo['price']/$PageInfo['full_square']);
			$link_developer = '/search?type=sell&estate=1&priceAll=all&developer='.$PageInfo['id_developer'];
			?>
		</div>
		<div class="price_card">
			<div class="prices">
				<div class="full_price"><strong><?=price_cell($PageInfo['price'],0)?></strong> <span class="rub">Р</span></div>
				<div class="meter_price"><?=price_cell($square_meter,0)?> <span class="rub">Р</span>/м<sup>2</sup></div>
				<div class="offer_price"><a href="#">Предложить свою цену</a></div>
			</div>
			<div class="declaration"><a href="#">Декларации</a></div>
		</div>
	</div>
	<div class="separate"></div>
	<div class="card_estate new_card" id="housing_estate">
		<?
			$res = mysql_query("
				SELECT *,
				(SELECT COUNT(id) FROM ".$template."_photo_catalogue WHERE estate='new_flats' && p_main='".$PageInfo['id']."') AS count_photo
				FROM ".$template."_photo_catalogue
				WHERE estate='new_flats' && p_main='".$PageInfo['id']."'
				ORDER BY cover DESC, num
			");
			if(mysql_num_rows($res)>0){
				echo '<div class="fotogallery">';
				echo '<div class="fotorama fotorama-skin-rsp fotorama-skin-rsp_hidden"
					 data-nav="thumbs"
					 data-height="421"
					 data-keyboard="true"
					 data-navposition="bottom"
					 data-loop="true"
					 data-allowfullscreen="native"
					 data-fit="scaledown">';
				while($row = mysql_fetch_assoc($res)){
					$ex_images = explode(',',$row['images']);
					if($row['cover']==1){
						$linkImgMid = '/admin_2/uploads/'.$ex_images[3];
						$linkImgMini = '/admin_2/uploads/'.$ex_images[5];
						if($row['user_id']!=0){
							$linkImgMid = '/users/'.$row['user_id'].'/'.$ex_images[3];
							$linkImgMini = '/users/'.$row['user_id'].'/'.$ex_images[5];
						}
						$big_image = '<a href="'.$linkImgMini.'" data-thumb="'.$linkImgMid.'" data-full="'.$linkImgMini.'">
							<img src="'.$linkImgMini.'" />
						</a>';
					}
				}
				echo '</div>';
				echo '</div>';
				echo '<script type="text/javascript" src="/libs/fotorama/js/fotorama.js"></script>';
				echo '<script type="text/javascript" src="/libs/fotorama/js/_fotorama_2.js"></script>';
			}
			
			/*
			*  Количество комнат
			*/
			$rooms = '';
			if(isset($PageInfo['rooms'])){
				$room = 'Студия';
				if($PageInfo['rooms']>0){
					$room = $PageInfo['rooms'];
				}
				$rooms = '<div class="row">
					<div class="cell label">Количество комнат:</div>
					<div class="cell">'.$room.'</div>
				</div>';
			}
			
			/*
			*  Тип дома
			*/
			if(!empty($PageInfo['type_house'])){
				$r = mysql_query("
					SELECT name
					FROM ".$template."_type_house
					WHERE id='".$PageInfo['type_house']."'
				");
				if(mysql_num_rows($r)>0){
					$rw = mysql_fetch_assoc($r);
					$type_house = '<div class="row">
						<div class="cell label">Тип дома:</div>
						<div class="cell">'.$rw['name'].'</div>
					</div>';
				}
			}
			
			/*
			*  Кол-во этажей
			*/
			$floors = '';
			if(!empty($PageInfo['floor'])){
				if(!empty($PageInfo['floors'])){
					$floors = '<div class="row">
						<div class="cell label">Этаж:</div>
						<div class="cell">'.$PageInfo['floor'].' (из '.$PageInfo['floors'].')</div>
					</div>';
				}
				else {
					$floors = '<div class="row">
						<div class="cell label">Этаж:</div>
						<div class="cell">'.$PageInfo['floor'].'</div>
					</div>';
				}
			}
			
			/*
			*  Высота потолков
			*/
			$ceiling_height = '';
			if(!empty($PageInfo['ceiling_height'])){
				$ceiling_height = '<div class="row">
					<div class="cell label">Высота потолков:</div>
					<div class="cell">'.str_replace(".", ",", $PageInfo['ceiling_height']).' м</div>
				</div>';
			}
			
			/*
			*  Отделка
			*/
			$finishing_value = '<span class="no">нет</span>';
			$finishing = '';
			if(!empty($PageInfo['finishing'])){
				$finishing_value = '<span class="ok">да</span>';
				$finishing = '<div class="row">
					<div class="cell label">Отделка:</div>
					<div class="cell places">'.$finishing_value.'</div>
				</div>';
			} 
		?>

		<div class="card_info">
			<div class="info_housing">
				<div class="row first">
					<div class="cell label">ID: <?=$PageInfo['id']?></div>
					<div class="cell"><a class="add_favorite" href="javascript:void(0)"><span>В избранное</span></a></div>
				</div>
				<div class="row">
					<div class="cell label">Жилая недвижимость:</div>
					<div class="cell">Новостройка</div>
				</div>
				<?=$rooms?>
				<?=$type_house?>
				<?=$floors?>
				<?=$ceiling_height?>
				<?=$finishing?>
			</div>
			<?
				/*
				*  Общая площадь
				*/
				$full_square = '';
				if(isset($PageInfo['full_square']) && !empty($PageInfo['full_square'])){
					$full_square = '<div class="row">
						<div class="cell label">Общая площадь:</div>
						<div class="cell">'.$PageInfo['full_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Жилая площадь
				*/
				$live_square = '';
				if(isset($PageInfo['live_square']) && !empty($PageInfo['live_square'])){
					$live_square = '<div class="row">
						<div class="cell label">Жилая площадь:</div>
						<div class="cell">'.$PageInfo['live_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Площадь комнат
				*/
				$rooms_square = '';
				if(isset($PageInfo['rooms_square']) && !empty($PageInfo['rooms_square'])){
					$rooms_square = '<div class="row">
						<div class="cell label">Площадь комнат:</div>
						<div class="cell">'.$PageInfo['rooms_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Площадь кухни
				*/
				$kitchen_square = '';
				if(isset($PageInfo['kitchen_square']) && !empty($PageInfo['kitchen_square'])){
					$kitchen_square = '<div class="row">
						<div class="cell label">Площадь кухни:</div>
						<div class="cell">'.$PageInfo['kitchen_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Площадь кухни
				*/
				$wc = '';
				if(isset($PageInfo['wc']) && !empty($PageInfo['wc'])){
					$wc = '<div class="row">
						<div class="cell label">Санузел:</div>
						<div class="cell">'.$_TYPE_WC_ADMIN[$PageInfo['wc']].'</div>
					</div>';
				}
				
				/*
				*  Балкон
				*/
				$balcony = '';
				if(isset($PageInfo['balcony']) && !empty($PageInfo['balcony'])){
					$balcony = '<div class="row">
						<div class="cell label">Балкон:</div>
						<div class="cell">'.$_TYPE_BALCONY_ADMIN[$PageInfo['balcony']].'</div>
					</div>';
				}
				
				/*
				*  Лифт
				*/
				$lift = '';
				$lift_value = '<span class="no">нет</span>';
				if(isset($PageInfo['lift']) && !empty($PageInfo['lift'])){
					$lift_value = '<span class="ok">да</span>';
					$lift = '<div class="row">
						<div class="cell label">Лифт:</div>
						<div class="cell">'.$lift_value.'</div>
					</div>';
				}
				
				/*
				*  Паркинг
				*/
				$parking = '';
				$parking_value = '<span class="no">нет</span>';
				if(isset($PageInfo['parking']) && !empty($PageInfo['parking'])){
					$parking_value = '<span class="ok">да</span>';
					$parking = '<div class="row">
						<div class="cell label">Паркинг:</div>
						<div class="cell">'.$parking_value.'</div>
					</div>';
				}
			?>
			<div class="info_housing">
				<?=$full_square?>
				<?=$live_square?>
				<?=$rooms_square?>
				<?=$kitchen_square?>
				<?=$wc?>
				<?=$balcony?>
				<?=$lift?>

				<div class="row">
					<div class="cell label">Сдача ГК:</div>
					<div class="cell"><?=$deadline?></div>
				</div>
				
				<?=$parking?>

				<div class="row add">
					<div class="cell label">Дополнительно:</div>
					<div class="cell"><?=$PageInfo['text']?></div>
				</div>
			</div>
			
			<div class="ways_buy">
				<ul>
					<li>Ипотека</li>
					<li>Субсидии</li>
					<li>Рассрочка</li>
				</ul>
			</div>
			
			<?
			$linkThisPage = urlencode('http://'.$_SERVER['HTTP_HOST'].'/'.$_SERVER['REQUEST_URI']);
			?>
			
			<div class="share_block">
				<ul>
					<li>Поделиться:</li>
					<li><a onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="http://vk.com/share.php?url=<?=$linkThisPage?>"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
					<li><a onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://www.facebook.com/sharer.php?src=sp&u=<?=$linkThisPage?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://plus.google.com/share?url=<?=$linkThisPage?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
					<li><a onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://twitter.com/intent/tweet?url=<?=$linkThisPage?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl=<?=$linkThisPage?>"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="clear"></div>
		<div class="text_info">
			<h2><a href="#">ЖК &laquo;Я - Романтик&raquo; на Невской губе</a></h2>
			<?
			if(!empty($PageInfo['text'])){
				echo '<div class="text">'.$textPage.'</div>';
				echo '<div class="more_read"><a href="javascript:void(0)">Читать всё</a></div>';
			}
			?>
		</div>
		<!--<div class="more_info">
			<div class="near_objects">
				<div class="infrastructure_block">
					<div class="title">Инфраструктура</div>
					<ul>
						<li>
							<div class="item"><span class="school icon"></span>Школа имени Римского-Корсакого</div>
						</li>
						<li>
							<div class="item"><span class="kindergarten icon"></span>Детский сад №56</div>
						</li>
						<li>
							<div class="item"><span class="clinic icon"></span>Поликлиника им. Св. Марии</div>
						</li>
					</ul>
				</div>
				<div class="safety_block">
					<div class="title">Безопасность</div>
					<ul>
						<li>Огороженный периметр</li>
						<li>Видеонаблюдение</li>
						<li>Пропускная система</li>
						<li>Консьерж</li>
						<li>Охраняемая парковка</li>
						<li>Сигнализация</li>
					</ul>
				</div>
			</div>
			<div class="other_offers_flats">
				<div class="list_flats">
					<div class="row">
						<div class="name">Студия. от <strong>27,5 м<sup>2</sup></strong></div>
						<div class="price">от <strong><a href="#">995 000 руб.</a></strong></div>
					</div>
					<div class="row fone">
						<div class="name">1-комн. от <strong>36,4 м<sup>2</sup></strong></div>
						<div class="price">от <strong><a href="#">1 638 000 руб.</a></strong></div>
					</div>
					<div class="row">
						<div class="name">2-комн. от <strong>44,7 м<sup>2</sup></strong></div>
						<div class="price">от <strong><a href="#">1 922 100 руб.</a></strong></div>
					</div>
				</div>
			</div>
		</div>-->
		<div class="bottom_block">
			<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
			<style>#map {width:650px;height:780px;padding:0;margin:0}</style>
			<?
			$coordsYandex = '';
			$ex_coords = array('30.315868','59.939095');
			if(empty($PageInfo['coords'])){
				$params2 = array(
					'geocode' => 'Санкт-Петербург, '.$PageInfo['address'],
					'format'  => 'json',
					'results' => 1,
					'key'     => 'AKDIIFYBAAAAqIYtRgIAZYb8P32hIxnbRRSe9CpggONv4PEAAAAAAAAAAADu6wMZJIEJ0SDd0mRe33ag1JRdNA==',
				);
				$response = json_decode(@file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params2)));
				
				if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0){
					$coordsYandex = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
				}
			}
			else {
				$coordsYandex = $PageInfo['coords'];
			}
			if(!empty($coordsYandex)){
				$ex_coords = explode(' ',$coordsYandex);
			}
			?>
			
			<script>
				ymaps.ready(function(){
					var myMap = new ymaps.Map('map', {
							center: [<?=$ex_coords[1]?>,<?=$ex_coords[0]?>],
							zoom: 13
						}, {
							searchControlProvider: 'yandex#search'
						}),
						myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
							hintContent: '<?=$PageInfo['name']?>',
							balloonContent: '<?=$PageInfo['data_address']?>'
						}, {
							// Опции.
							// Необходимо указать данный тип макета.
							iconLayout: 'default#image',
							// Своё изображение иконки метки.
							iconImageHref: '/images/<?=$_ICONS_MAPS[$PageInfo['rooms']]['card']?>',
							// Размеры метки.
							iconImageSize: [52, 44],
							// Смещение левого верхнего угла иконки относительно
							// её "ножки" (точки привязки).
							iconImageOffset: [-3, -42]
						});

					myMap.geoObjects.add(myPlacemark);
					myMap.behaviors.disable('scrollZoom');
				});
			</script>
			<div class="map"><div id="map"></div></div>
			<div class="feedback_block">
				<div class="representative">
					<div class="user_info">
						<span class="form">Представитель</span>
						<span class="id_block">ID: 10615662<i class="verified"></i></span>
					</div>
					<div class="show_phone">
						<div class="btn_save top">
							<label class="btn"><input onclick="return show_phone(5)" type="button" value="Показать ТЕЛЕФОН"><span class="angle-right"></span></label>
						</div>
					</div>
				</div>
				
				<div class="feedback_form">
					<form action="/include/handler.php" method="post" onsubmit="return checkSideForm(this)">
						<div class="send_request">
							<input type="hidden" name="type_form" value="application">
							<div class="title">Запросить информацию по объекту</div>
							<div class="table">
								<div class="row">
									<div class="cell">
										<div class="label">Ваше имя<b>*</b></div>
									</div>
									<div class="cell">
										<div class="input_text required">
											<input type="text" name="s[name]">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="cell">
										<div class="label">Ваш телефон<b>*</b></div>
									</div>
									<div class="cell">
										<div class="input_text required">
											<input class="phone" type="text" name="s[phone]">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="cell">
										<div class="label">Ваш e-mail<b>*</b></div>
									</div>
									<div class="cell">
										<div class="input_text required">
											<input type="text" name="s[email]">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="cell">
										<div class="label">Сообщение</div>
									</div>
									<div class="cell">
										<div class="input_text required"><textarea placeholder="Меня интересует этот объект недвижимости, пожалуйста, свяжитесь со мной..." name="s[msg]"></textarea></div>
									</div>
								</div>
								<div class="row">
									<div class="cell full comment center">Пожалуйста, введите символы с картинки</div>
								</div>
								<div class="row">
									<div class="cell captcha">
										<img width="150" height="50" id="kaptcha" src="/libs/kaptcha/index.php?<?=session_name().'='.session_id()?>">
									</div>
									<div style="margin-top:11px" class="cell">
										<div class="input_text required">
											<input style="width:220px;margin-left:20px" type="text" name="s[code]">
										</div>
									</div>
								</div>
							</div>
							<div class="btn">
								<label class="btn"><input type="submit" value="Отправить"><span class="angle-right"></span></label>
							</div>
							<div class="table">
								<div class="row">
									<div class="cell full comment">
										<div class="agreement">Отправляя это сообщение, Вы принимаете условия <a href="#">Пользовательского соглашения</a></div>
										<div class="req"><b>*</b> - поля обязательные для заполнения</div>
									</div>
								</div>
							</div>
							
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<?
		$res = mysql_query("
			SELECT *
			FROM ".$template."_articles
			WHERE activation=1
			ORDER BY date DESC,id DESC
			LIMIT 10
		");
		if(mysql_num_rows($res)>0){
			echo '<div class="news_events">';
			echo '<h3>Новости и статьи</h3>';
			echo '<div id="owl-demo">';
			while($row=mysql_fetch_assoc($res)){
				$images = '';
				$link = '/articles/'.$row['link'];
				if(!empty($row['images'])){
					$ex_avatar = explode(',',$row['images']);
					$images = '<div class="avatar"><img width="125" src="/admin_2/uploads/'.$ex_avatar[1].'"></div>';
				}
				echo '<div class="item">
					<div class="date">'.date_rus($row['date']).'</div>
					<div class="title"><a target="_blank" href="'.$link.'"><span>'.$row['name'].'</span></a></div>
				</div>';
			}
			echo '</div>';
			echo '</div><script src="/libs/owl-carousel/js/owl.carousel.js?ver=<'.time().'"></script>';
		}
		?>
		</div>
		
		<div id="comments_block">
			<h3>Комментарии пользователей</h3>
			<div class="container_block">
				<div class="scrollbar-inner">
					<ul>
						<li>
							<div class="image"><a href="#"><img src="/images/avatar.jpg"></a></div>
							<div class="body">
								<div class="title">
									<div class="info">
										<a href="#">Вера Пчёлкина</a><strong>19:35</strong> | <span>25 Окт. 2016</span>
									</div>
									<div class="id_comment">#24590</div>
								</div>
								<div class="message_block">
									<div class="msg">Очевидно, что марксизм формирует прагматический культ личности. Механизм власти, как бы это ни казалось парадоксальным, теоретически возможен. Авторитаризм, короче говоря, верифицирует системный бихевиоризм.</div>
									<div class="btns">
										<div class="answer"><a href="#">Ответить</a></div>
										<div class="complain"><a href="#">Пожаловаться</a></div>
									</div>
								</div>
							</div>
							<ul>
								<li>
									<div class="image"><a href="#"><img src="/images/avatar.jpg"></a></div>
									<div class="body">
										<div class="title">
											<div class="info">
												<a href="#">Вера Пчёлкина</a><strong>19:35</strong> | <span>25 Окт. 2016</span>
											</div>
											<div class="id_comment">#24590</div>
										</div>
										<div class="message_block">
											<div class="msg">Очевидно, что марксизм формирует прагматический культ личности. Механизм власти, как бы это ни казалось парадоксальным, теоретически возможен. Авторитаризм, короче говоря, верифицирует системный бихевиоризм.</div>
											<div class="btns">
												<div class="answer"><a href="#">Ответить</a></div>
												<div class="complain"><a href="#">Пожаловаться</a></div>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</li>
						<li>
							<div class="image"><a href="#"><img src="/images/avatar.jpg"></a></div>
							<div class="body">
								<div class="title">
									<div class="info">
										<a href="#">Вера Пчёлкина</a><strong>19:35</strong> | <span>25 Окт. 2016</span>
									</div>
									<div class="id_comment">#24590</div>
								</div>
								<div class="message_block">
									<div class="msg">Очевидно, что марксизм формирует прагматический культ личности. Механизм власти, как бы это ни казалось парадоксальным, теоретически возможен. Авторитаризм, короче говоря, верифицирует системный бихевиоризм.</div>
									<div class="btns">
										<div class="answer"><a href="#">Ответить</a></div>
										<div class="complain"><a href="#">Пожаловаться</a></div>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="btn_info cost">
					<div class="table_form">
						<div class="row btn_row">
							<div class="cell center full last_col">
								<label class="btn link"><a class="back_page" href="javascript:void(0)"><span>Оставить сообщение</span></a></label>
								<label class="btn"><input type="submit" value="Перейти к обсуждению темы"><span class="angle-right"></span></label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?
		$res = mysql_query("
			SELECT c.*,
			(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=c.id && estate='new_flats' LIMIT 1) AS images,
			(SELECT title FROM ".$template."_stations WHERE activation='1' && id=c.station) AS station_name,
			(SELECT station FROM ".$template."_m_catalogue_left WHERE activation=1 && id=c.p_main) AS station_id,
			(SELECT name FROM ".$template."_m_catalogue_left WHERE activation=1 && id=c.p_main) AS nameBuilds
			FROM ".$template."_new_flats AS c
			WHERE c.activation=1
			ORDER BY RAND()
			LIMIT 4
		") or die(mysql_error());
		if(mysql_num_rows($res)>0){
			echo '<div id="related_offers">';
			echo '<h3>Похожие предложения</h3>';
			echo '<div class="container_block">';
			

			while($row = mysql_fetch_assoc($res)){
				$image = '<span>Нет фото</span>';
				if(!empty($row['images'])){
					$ex_image = explode(',',$row['images']);
					$size = @getimagesize($_SERVER['DOCUMENTS_ROOT'].'/admin_2/uploads/'.$ex_image[3]);
					if(!empty($size[0])){
						$image = '<img src="/admin_2/uploads/'.$ex_image[3].'">';
					}
				}
				$linkBuild = '/newbuilding/'.$row['p_main'];
				$link = '/newbuilding/'.$row['p_main'].'/flat/'.$row['id'];
				$exploitation = explode('_',$row['exploitation']);
				$deadline = '';
				if(count($exploitation)>0){
					if(count($exploitation)>1){
						$deadline = '<span>'.$exploitation[1].' кв. '.$exploitation[0].'</span>';
					}
					else {
						$deadline = '<span>'.$exploitation[0].'</span>';
					}
				}
				
				$station_name = '';
				if(!empty($row['station_id'])){
					$r = mysql_query("
						SELECT title
						FROM ".$template."_stations
						WHERE id='".$row['station_id']."' && activation=1
					");
					if(mysql_num_rows($r)>0){
						$rw = mysql_fetch_assoc($r);
						$stat_name = $rw['title'];
						$link_station = '/search?type=sell&estate=1&priceAll=all&station='.$row['station'];
						$station_name = '<div class="station"><a href="'.$link_station.'">'.$stat_name.'</a><span class="dist">'.$row['dist_value'].' км</span></div>';
					}
				}
				
				echo '<div class="item">
					<div class="title_block">
						<a href="'.$linkBuild.'">'.$row['nameBuilds'].'</a>
						<div class="info_item">
							<div class="price">'.price_cell($row['price'],0).' <span class="rub">Р</span></div>
							<!--<div class="deadline">Срок сдачи: <span class="queue">'.$deadline.'</span></div>-->
						</div>
						<div class="image">
							<a href="'.$link.'">'.$image.'</a>
						</div>
						<div class="address">'.$row['address'].'</div>
						'.$station_name.'
					</div>
				</div>';
			}
			echo '</div>';
			echo '</div>';
		}
		?>
	</div>
	<?
	$item_little = '';
	$item_big = '';
	$res = mysql_query("
		SELECT n.*,c.name AS name_jk,c.address,
		(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=n.id && estate='new_flats') AS images,
		(SELECT name FROM ".$template."_developers WHERE activation='1' && id=c.developer) AS name_developer
		FROM ".$template."_m_catalogue_left AS c
		LEFT JOIN ".$template."_new_flats AS n
		ON n.p_main=c.id && n.activation='1'
		WHERE c.activation='1' && n.id!='".$PageInfo['id']."' && n.price<'".$PageInfo['price']."' && c.id='".$PageInfo['id_jk']."' && n.rooms=IF((SELECT COUNT(*) FROM ".$template."_new_flats WHERE activation='1' && p_main='".$PageInfo['id_jk']."' && rooms='".$PageInfo['rooms']."' && price<'".$PageInfo['price']."')>0, '".$PageInfo['rooms']."', '".($PageInfo['rooms']-1)."')
		ORDER BY RAND()
		LIMIT 1
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$row = mysql_fetch_assoc($res);
		$link = '/newbuilding/'.$PageInfo['id_jk'].'/flat/'.$row['id'];
		$image = '<span>Нет фото</span>';
		if(!empty($row['images'])){
			$ex_image = explode(',',$row['images']);
			$image = '<img src="/admin_2/uploads/'.$ex_image[0].'">';
			if($row['user_id']!=0){
				$image = '<img src="/users/'.$row['user_id'].'/'.$ex_image[0].'">';			
			}
		}
		$square_meter = ceil($row['price']/$row['full_square']);
		$name_title = $row['rooms'].'-комн. кв';
		if($row['rooms']==0){
			$name_title = 'Студия';
		}
		$floor_name = '';
		if(!empty($row['floor'])){
			$floor_name = '<div class="type">этаж '.$row['floor'].'</div>';
		}
		$item_little = '<div class="item little">
			<div class="image">
				<a href="'.$link.'">'.$image.'</a>
			</div>
			<div class="body">
				<div class="location">
					<div class="title"><a href="'.$link.'">'.$name_title.' <strong>'.str_replace(".", ",", price_cell($row['full_square'],1)).' м<sup>2</sup></strong></a></div>
					<div class="comm">'.$row['name_jk'].'</div>
					<div class="price"><strong>'.price_cell($row['price'],0).' руб.</strong></div>
					<div class="square">'.price_cell($square_meter,0).' руб/м<sup>2</sup></div>
					'.$floor_name.'
				</div>
			</div>
		</div>';
	}
	$res = mysql_query("
		SELECT n.*,c.name AS name_jk,c.address,
		(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=n.id && estate='new_flats') AS images,
		(SELECT name FROM ".$template."_developers WHERE activation='1' && id=c.developer) AS name_developer
		FROM ".$template."_m_catalogue_left AS c
		LEFT JOIN ".$template."_new_flats AS n
		ON n.p_main=c.id && n.activation='1'
		WHERE c.activation='1' && c.id='".$PageInfo['id_jk']."' && n.id!='".$PageInfo['id']."' && n.rooms='".$PageInfo['rooms']."' && n.full_square>'".$PageInfo['full_square']."' && n.id!='".$PageInfo['id']."'
		ORDER BY full_square
		LIMIT 1
	") or die(mysql_error());
	$meterBig = '';
	if(mysql_num_rows($res)>0){
		$row = mysql_fetch_assoc($res);
		$link = '/newbuilding/'.$PageInfo['id_jk'].'/flat/'.$row['id'];
		$image = '<span>Нет фото</span>';
		if(!empty($row['images'])){
			$ex_image = explode(',',$row['images']);
			$image = '<img src="/admin_2/uploads/'.$ex_image[0].'">';
			if($row['user_id']!=0){
				$image = '<img src="/users/'.$row['user_id'].'/'.$ex_image[0].'">';			
			}
		}
		$meterBig = price_cell($row['full_square'] - $PageInfo['full_square'],1);
		$square_meter = ceil($row['price']/$row['full_square']);
		$name_title = $row['rooms'].'-комн. квартира';
		if($row['rooms']==0){
			$name_title = 'Студия';
		}
		$floor_name = '';
		if(!empty($row['floor'])){
			$floor_name = '<div class="count_offers">этаж '.$row['floor'].'</div>';
		}
		$item_big = '<div class="item big">
			<div class="image">
				<a href="'.$link.'">'.$image.'</a>
			</div>
			<div class="body">
				<div class="location">
					<div class="title"><a href="'.$link.'">'.$name_title.' <strong>'.str_replace(".", ",", price_cell($row['full_square'],1)).' м<sup>2</sup></strong></a></div>
					<div class="comm">'.$row['name_jk'].'</div>
					<div class="street">'.$row['address'].'</div>
					<div class="type_name">'.$row['name_developer'].'</div>
				</div>
				<div class="cost_block">
					<div class="price"><strong>'.price_cell($row['price'],0).' руб.</strong></div>
					<div class="square">'.price_cell($square_meter,0).' руб/м<sup>2</sup></div>
					'.$floor_name.'
				</div>
			</div>
		</div>';
	}
	echo '<div class="our_offers">';
	echo '<div class="second_title">';
			if(!empty($item_little)){
				echo '<h3 class="little">Ищите дешевле?</h3>';
			}
			if(!empty($meterBig)){
				echo '<h3><span class="baloon"></span>На '.$meterBig.' м<sup>2</sup> больше</h3>';
			}
		echo '</div>';
		if(!empty($item_little) || !empty($item_big)){
			echo '<div class="offers_list">';
			echo $item_little;
			echo $item_big;
			echo '</div>';
		}
	echo '</div>';
	?>
</div>	