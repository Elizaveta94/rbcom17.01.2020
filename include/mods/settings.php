<?
$preview = 'Введите сюда кратко о себе';
if(!empty($usersInfo['preview'])){
	$preview = $usersInfo['preview'];
}
$usersCost = $usersInfo['cost'];
if($usersInfo['cost']==0){
	$usersCost = '';
}
$usersHours = $usersInfo['hours'];
if($usersInfo['hours']==0){
	$usersHours = '';
}
$avatar_user = '';
if(!empty($usersInfo['avatar'])){
	$ex_avatar = explode(',',$usersInfo['avatar']);
	$avatar_user = '/users/'.$usersInfo['id'].'/'.$ex_avatar[1];
}
?>
<div id="breadcrumbs"><ul><li><a href="/">Главная</a></li><li>Редактирование личных данных</li></ul></div>
<div id="right">
	<div id="content">
		<div id="user_settings">
			<form onsubmit="return saveInfoUsers(this)" method="post" action="/include/handler.php" id="user_pass_save">
				<input type="hidden" name="userInfo" value="change_pass">
				<div class="inner_block">
					<table class="user_pass">
						<tr>
							<td colspan="2"><div class="title">Изменить пароль</div></td>
						</tr>
						<tr>
							<td><label for="old_pass">Старый пароль</label></td>
							<td class="required"><input id="old_pass" type="text" name="s[old_pass]"></td>
						</tr>
						<tr>
							<td><label for="new_pass">Новый пароль</label></td>
							<td class="required"><input id="new_pass" type="text" name="s[new_pass]"></td>
						</tr>
						<tr>
							<td><label for="repeat_pass">Повторите пароль</label></td>
							<td class="required"><input id="repeat_pass" type="text" name="s[repeat_pass]"></td>
						</tr>
						<tr>
							<td></td>
							<td><input type="submit" value="Изменить"></td>
						</tr>
					</table>
				</div>
			</form>
			<form onsubmit="return saveInfoUsers(this)" method="post" action="/include/handler.php" id="user_info_save">
				<input type="hidden" name="userInfo" value="user_info">
				<div class="top_info">
					<div class="container_user">
						<div class="user_right">
							<div class="padding">
								<div class="short_text">
									<div class="margin">
										<div class="textarea_block">
											<div class="expandingArea active">
												<pre><span></span><br></pre>
												<textarea name="s[preview]"><?=$preview?></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="user_left">
							<div class="avatar"><a href="javascript:void(0)"><img src="<?=$avatar_user?>"><span id="loadAvatarUser" class="upload_avatar"></span></a></div>
							<div class="angle-left"></div>
						</div>
					</div>
				</div>
				<div class="information_user">
					<div class="inner_block">
						<table class="user_info">
							<tr>
								<td><label for="name_user">Имя</label></td>
								<td class="required"><input id="name_user" value="<?=$usersInfo['name']?>" type="text" name="s[name]"></td>
							</tr>
							<tr>
								<td><label for="surname_user">Фамилия</label></td>
								<td class="required"><input id="surname_user" value="<?=$usersInfo['surname']?>" type="text" name="s[surname]"></td>
							</tr>
							<tr>
								<td><label for="city_user">Город</label></td>
								<td><select id="city_user" name="s[city]"><option value="113">Петропавловск-Камчатский (Россия)</option></select></td>
							</tr>
							<tr>
								<td><label for="cost_user">Стоимость час работы</label></td>
								<td><div class="input_text"><input id="cost_user" type="text" name="s[cost]" value="<?=$usersCost?>"><div class="first_comm">от</div><div class="last_comm">руб.</div></div></td>
							</tr>
							<tr>
								<td><label for="hour_user">Минимальное кол-во часов</label></td>
								<td><div class="input_text"><input id="hour_user" type="text" name="s[hours]" value="<?=$usersHours?>"><div class="first_comm">от</div></div></td>
							</tr>
						</table>
						<table class="about_user">
							<tr>
								<td><label for="about_user">О себе</label></td>
								<td><textarea rows="8" id="about_user" name="s[about]"><?=$usersInfo['about']?></textarea></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" value="Сохранить изменения"></td>
							</tr>
						</table>
					</div>
				</div>
			</form>
			<form onsubmit="return saveInfoUsers(this)" method="post" action="/include/handler.php" id="user_social_save">
				<input type="hidden" name="userInfo" value="user_info">
				<div class="information_user">
					<div style="float:left;width:52%" class="inner_block">
						<table style="float:none;width:100%" class="user_info">
							<tr>
								<td>
									<div class="title">Данные для связи</div>
								</td>
								<td></td>
							</tr>
							<tr>
								<td><label for="site_user">Сайт</label></td>
								<td><input id="site_user" type="text" name="s[site]" value="<?=$usersInfo['site']?>"></td>
							</tr>
							<tr>
								<td><label for="phone_user">Телефон</label></td>
								<td><input id="phone_user" type="text" name="s[phone]" value="<?=$usersInfo['phone']?>"></td>
							</tr>
							<tr>
								<td><label for="email_user">E-mail для связи</label></td>
								<td><input id="email_user" type="text" name="s[work_email]" value="<?=$usersInfo['work_email']?>"></td>
							</tr>
							<tr>
								<td colspan="2"></td>
							</tr>
							<tr>
								<td><label for="vk_user">Вконтакте</label></td>
								<td><div class="input_text"><div class="first_comm"><i class="fa fa-vk"></i></div><input id="vk_user" type="text" name="s[vk]" value="<?=$usersInfo['vk']?>"></div></td>
							</tr>
							<tr>
								<td><label for="fb_user">Facebook</label></td>
								<td><div class="input_text"><div class="first_comm"><i class="fa fa-facebook"></i></div><input id="fb_user" type="text" name="s[fb]" value="<?=$usersInfo['fb']?>"></div></td>
							</tr>
							<tr>
								<td><label for="ok_user">Одноклассники</label></td>
								<td><div class="input_text"><div class="first_comm"><i class="fa fa-odnoklassniki"></i></div><input id="ok_user" type="text" name="s[ok]" value="<?=$usersInfo['ok']?>"></div></td>
							</tr>
							<tr>
								<td><label for="tw_user">Twitter</label></td>
								<td><div class="input_text"><div class="first_comm"><i class="fa fa-twitter"></i></div><input id="tw_user" type="text" name="s[tw]" value="<?=$usersInfo['tw']?>"></div></td>
							</tr>
							<tr>
								<td><label for="gp_user">Google plus</label></td>
								<td><div class="input_text"><div class="first_comm"><i class="fa fa-google-plus"></i></div><input id="gp_user" type="text" name="s[gp]" value="<?=$usersInfo['gp']?>"></div></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" value="Сохранить изменения"></td>
							</tr>
						</table>
					</div>
					<div class="portfolio_user">
						<div class="title">Портфолио</div>
						<div class="text">Мы сделали специальный раздел для более удобного редактирования Вашего портфолио! Чтобы приступить к заполнению или редактированию портфолио нажмите на кнопку:</div>
						<div class="button_portfolio"><a href="/edit_photo">Портфолио</a></div>
						<div class="last_photos">
							<?
							$last_photos = '';
							$last_date = '';
							$res = mysql_query("
								SELECT *
								FROM ".$template."_photo_catalogue
								WHERE activation='1' && user_id='".$_SESSION['idAuto']."'
								ORDER BY date DESC, id DESC
								LIMIT 8
							");
							if(mysql_num_rows($res)>0){
								$last_photos .= '<ul>';
								while($row = mysql_fetch_assoc($res)){
									$ex_images = explode(',',$row['images']);
									$last_photos .= '<li><img width="82" src="/users/'.$_SESSION['idAuto'].'/gallery/'.$ex_images[0].'"></li>';
									$last_date = date("d.m.Y",$row['date']).' '.date("H:i",$row['date']);
								}
								$last_photos .= '</ul>';
							}
							?>
							<div class="date_info">Последние добавленные фото<span class="date"><?=$last_date?></div>
							<div class="photos"><?=$last_photos?></div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>