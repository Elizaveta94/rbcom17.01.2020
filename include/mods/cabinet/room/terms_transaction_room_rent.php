<?
/*
*  Коммунальные платежи
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['communal'])){
	if($PageInfoAd['communal']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['communal'].'"';
	}
}
$communal = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[communal]" value="0">
	<input type="hidden"'.$valueParams.' name="s[communal]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span style="display:block;margin-top:2px" class="label">В стоимость <br>аренды включены коммунальные платежи <i>(кроме счетчиков)</i></span>
	</div>
</div>';

echo '<div id="step-3" class="container_block ads">
	<div class="cost_block">
		<div class="left_block">
			<div class="table_form">
				<h2>Условия сделки</h2>
				<div style="position:relative" class="row">
					<div class="cell label right one_second">
						<label style="margin-top:-2px">Стоимость аренды<b>*</b></label>
					</div>
					<div style="width:235px" class="cell required one_second">
						<input style="width:162px;float:left" type="text" class="text" name="s[price]"'.$price.'>
						<div class="com">руб./мес.</div>
					</div>
					<div style="width:175px;position:absolute;right:0;margin-top:8px" class="cell one_second last_col">
						'.$communal.'
					</div>
				</div>
				<div class="row">
					<div class="cell label right one_second">
						<label>Залог</label>
					</div>
					<div style="width:235px" class="cell one_second last_col">
						<input style="width:162px;float:left" type="text" class="text" name="s[deposit]"'.$deposit.'>
						<div class="com">руб.</div>
					</div>
				</div>
				<div class="row">
					<div class="cell label right one_second">
						<label>Комиссия %</label>
					</div>
					<div style="width:235px" class="cell one_second last_col">
						<input style="width:60px;float:left" type="text" class="text" name="s[prepayment]"'.$prepayment.'>
					</div>
				</div>
			</div>
		</div>
		<div class="right_block">
			<div class="table_form">
				<h2>Телефоны для связи</h2>
				<div class="row">
					<div class="cell info full">
						<p>В объявлении будет указан введённый ранее телефон</p>
					</div>
				</div>
				<div class="row">
					<div class="cell full">
						'.$reg_phone.'
					</div>
				</div>
				<div class="row">
					<div class="cell info full">
						<p>Дополнительно в этом объявлении можно указать ещё один или два номера</p>
					</div>
				</div>
				<div class="row">
					<div style="width:auto;margin:8px 10px 0 0" class="cell code">+7</div>
					<div class="cell code">
						<input maxlength="5" placeholder="код" type="text" class="code phone_code" name="phones[code][1]"'.$codePhone1.'>
					</div>
					<div style="width:204px" class="cell one_second last_col">
						<input type="text" class="phone_type phone_mask" name="phones[number][1]"'.$codeNumber1.'>
					</div>
				</div>
				<div class="row">
					<div style="width:auto;margin:8px 10px 0 0" class="cell code">+7</div>
					<div class="cell code">
						<input maxlength="5" placeholder="код" type="text" class="code phone_code" name="phones[code][2]"'.$codePhone2.'>
					</div>
					<div style="width:204px" class="cell one_second last_col">
						<input type="text" class="phone_type phone_mask" name="phones[number][2]"'.$codeNumber2.'>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="btn_info cost"></div>
</div>';
?>