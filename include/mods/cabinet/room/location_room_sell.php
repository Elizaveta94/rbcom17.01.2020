<?
/*
* Локация
*/
$location = '<div class="select_block count required">';
$location2 = '';
$nameParams = 'Не выбран';
$valueInput = '';
$r = mysql_query("
	SELECT *
	FROM ".$template."_location
	WHERE activation=1
	ORDER BY num
");
if(mysql_num_rows($r)>0){
	while($rw = mysql_fetch_assoc($r)){
		$current = '';
		if(isset($PageInfoAd['location']) && $PageInfoAd['location']==$rw['id']){
			$current = ' class="current"';
			$valueInput = ' value="'.$rw['id'].'"';
			$nameParams = $rw['name'];
		}
		$location2 .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$rw['id'].'">'.$rw['name'].'</a></li>';
	}
}
$location .= '<input type="hidden" name="s[location]"'.$valueInput.'>';
$location .= '<div style="width:282px" class="choosed_block">'.$nameParams.'</div>';
$location .= '<div class="scrollbar-inner">';
$location .= '<ul>';
$location .= $location2;
$location .= '</ul>';
$location .= '</div>';
$location .= '</div>';
		
$disabledAddress = '';
if(!isset($PageInfoAd) && empty($PageInfoAd['location'])){
	$disabledAddress = ' disabled="disabled"';
}
$coords = $PageInfoAd['coords'];
$ex_coords = explode(',',$coords);
if(count($ex_coords)>1){
	$coords = $ex_coords[1].' '.$ex_coords[0];
}

echo '<div style="border:none" class="left_block">
	<input type="hidden" name="ads_save" value="true"> 
	<input type="hidden" name="step" value="1">
	<input type="hidden" name="s[dist_value]" value="'.$PageInfoAd['dist_value'].'">
	<input type="hidden" name="s[station_id]" value="'.$PageInfoAd['station_id'].'">
	<input type="hidden" name="s[station]" value="'.$PageInfoAd['station'].'">
	<input type="hidden" name="s[coords]" value="'.$coords.'">
	<input type="hidden" name="s[center_coords]" value="'.$PageInfoAd['center_coords'].'">
	<input type="hidden" name="s[locality]" value="'.$PageInfoAd['locality'].'">
	<div class="table_form">
		<div class="row">
			<div class="cell info full">
				<p>Размещение объявлений доступно только на территории Российской Федерации.</p>
			</div>
		</div>
		<div class="row newbuildings flat">
			<div style="width:131px;" class="cell label right one_second">
				<label>Регион РФ<b>*</b></label>
			</div>
			<div class="cell one_second last_col required">
				'.$location.'
			</div>
		</div>
		<div class="row newbuildings flat">
			<div class="cell label right one_second">
				<label>Название ЖК</label>
			</div>
			<div class="cell one_second last_col">
				<div class="request_block">
					<input autocomplete="off" type="text" class="text" name="s[build_text]"'.$build_text.'>
					<input type="hidden" class="text" name="s[build]"'.$build.'>
					<div class="search_request_block">
						<div class="scrollbar-inner">
							<ul></ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="cell label right one_second">
				<label>Адрес<b>*</b></label>
			</div>
			<div class="cell required one_second last_col">
				<div style="width:365px" class="request_block">
					<input'.$disabledAddress.' type="text" class="text" name="s[address]" value="'.$PageInfoAd['address'].'">
					<div class="search_request_block">
						<div class="scrollbar-inner">
							<ul></ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row newbuildings flat">
			<div class="cell label right one_second">
				<label>&nbsp;</label>
			</div>
			<div class="cell one_second last_col">
				<div class="request_block">
					<a onclick="return sendForma(this,\'problema\')" class="problem" href="javascript:void(0)">Проблемы с вводом адреса?</a>
				</div>
			</div>
		</div>
		'.$stationRow.'
	</div>
</div>';
?>