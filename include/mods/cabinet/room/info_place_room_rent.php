<?
/*
*  Консьерж
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['concierge'])){
	if($PageInfoAd['concierge']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['concierge'].'"';
	}
}
$concierge = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[concierge]" value="0">
	<input type="hidden"'.$valueParams.' name="s[concierge]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Консьерж</span>
	</div>
</div>';

$liftServiceValue = ' value="0"';
if(isset($PageInfoAd['service_lift'])){
	$liftServiceValue = ' value="'.$PageInfoAd['service_lift'].'"';
}

$liftValue = ' value="0"';
if(isset($PageInfoAd['lift'])){
	$liftValue = ' value="'.$PageInfoAd['lift'].'"';
}

$rightBlock = '<div class="right_block">
	<div class="table_form">
		<h2>Информация о доме</h2>
		<div class="line">
			<div style="width:296px" class="block">
				<div class="row">
					<div class="cell label right one_second">
						<label style="white-space:nowrap;margin-left:-3px">Год подстройки</label>
					</div>
					<div class="cell one_second last_col">
						<input type="text" class="text" name="s[year]"'.$year.'>
					</div>
				</div>
				<div class="row">
					<div class="cell label right one_second">
						<label>Тип дома</label>
					</div>
					<div class="cell one_second last_col">
						'.$type_house.'
					</div>
				</div>
				<div class="row">
					<div class="cell label right one_second"><label></label></div>
					<div class="cell one_second last_col"></div>
				</div>
				<div class="row">
					<div style="text-align:right;margin-top:-4px" class="cell label one_second">Лифт <br>грузовой</div>
					<div class="cell auto last_col">
						<div class="checkbox_block">
							<input type="hidden" name="s[service_lift]"'.$liftServiceValue.'>
							<div class="select_checkbox">
								<ul style="margin-right:-20px;">
									<li'.($PageInfoAd['service_lift']==0?' class="checked"':"").'><a style="padding:4px 7px" href="javascript:void(0)" data-id="0">Нет</a></li>
									<li'.($PageInfoAd['service_lift']==1?' class="checked"':"").'><a style="padding:4px 7px" href="javascript:void(0)" data-id="1">1</a></li>
									<li'.($PageInfoAd['service_lift']==2?' class="checked"':"").'><a style="padding:4px 7px" href="javascript:void(0)" data-id="2">2</a></li>
									<li'.($PageInfoAd['service_lift']==3?' class="checked"':"").'><a style="padding:4px 7px" href="javascript:void(0)" data-id="3">2+</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div style="text-align:right;margin-top:-4px" class="cell label one_second">Лифт пассажирский</div>
					<div class="cell auto last_col">
						<div class="checkbox_block">
							<input type="hidden" name="s[lift]"'.$liftValue.'>
							<div class="select_checkbox">
								<ul style="margin-right:-20px;">
									<li'.($PageInfoAd['lift']==0?' class="checked"':"").'><a style="padding:4px 7px" href="javascript:void(0)" data-id="0">Нет</a></li>
									<li'.($PageInfoAd['lift']==1?' class="checked"':"").'><a style="padding:4px 7px" href="javascript:void(0)" data-id="1">1</a></li>
									<li'.($PageInfoAd['lift']==2?' class="checked"':"").'><a style="padding:4px 7px" href="javascript:void(0)" data-id="2">2</a></li>
									<li'.($PageInfoAd['lift']==3?' class="checked"':"").'><a style="padding:4px 7px" href="javascript:void(0)" data-id="3">2+</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div style="margin-top:87px" class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$parking.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$concierge.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$protected_area.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$playground.'
					</div>
				</div>
			</div>
		</div>
		<div class="line">
			<div style="margin-left:50px;width:246px" class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$furniture.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$animal.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$children.'
					</div>
				</div>
			</div>
			<div class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$tv.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$fridge.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$washer.'
					</div>
				</div>
			</div>
		</div>
	</div>
</div>';

/*
*  Интернет
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['internet'])){
	if($PageInfoAd['internet']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['internet'].'"';
	}
}
$internet = '<div style="float:left;margin-right:40px" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[internet]" value="0">
	<input type="hidden"'.$valueParams.' name="s[internet]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Интернет</span>
	</div>
</div>';

/*
*  Телефон
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['phone'])){
	if($PageInfoAd['phone']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['phone'].'"';
	}
}
$phone = '<div style="float:left" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[phone]" value="0">
	<input type="hidden"'.$valueParams.' name="s[phone]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Телефон</span>
	</div>
</div>';

/*
*  Высота потолков
*/
/* $ceiling_height = '<div class="select_block count">';
$ceiling_heights = '';
$nameParams = 'Не важно';
$valueInput = '';
$devs = mysql_query("
	SELECT *
	FROM ".$template."_type_house
	WHERE activation='1'
	ORDER BY num
");
if(mysql_num_rows($devs)>0){
	$n = 0;
	while($dev = mysql_fetch_assoc($devs)){
		$current = "";
		
		if($n==0 && !isset($PageInfoAd['ceiling_height'])){
			$ceiling_heights .= '<li class="current"><a href="javascript:void(0)" data-id="">Не важно</a></li>';
		}
		if($PageInfoAd['ceiling_height']==$dev['id']){
			$current = ' class="current"';
			$nameParams = $dev['name'];
			$valueInput = ' value="'.$dev['id'].'"';
		}
		$ceiling_heights .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$dev['id'].'">'.$dev['name'].'</a></li>';
		$n++;
	}
}
$ceiling_height .= '<input type="hidden" name="s[ceiling_height]"'.$valueInput.'>';
$ceiling_height .= '<div style="width:162px" class="choosed_block">'.$nameParams.'</div>';
$ceiling_height .= '<div class="scrollbar-inner">';
$ceiling_height .= '<ul>';
$ceiling_height .= $ceiling_heights;
$ceiling_height .= '</ul>';
$ceiling_height .= '</div>';
$ceiling_height .= '</div>';
 */
$roomsValue = ' value="1"';
if(isset($PageInfoAd['rooms'])){
	$roomsValue = ' value="'.$PageInfoAd['rooms'].'"';
}

$roomsFlatValue = ' value="1"';
$roomsFlatValue = '';
if(isset($PageInfoAd['rooms_flat'])){
	$roomsFlatValue = ' value="'.$PageInfoAd['rooms_flat'].'"';
}

echo '<div class="params_block">
		<div class="left_block">
			<div class="table_form">
				<h2>Информация о квартире</h2>
				<div class="row">
					<div class="cell label right one_second">
						<label style="margin-top:-2px">Кол-во<br> комнат<b style="margin-top:-15px">*</b></label>
					</div>
					<div class="cell required one_second last_col">
						<div class="checkbox_block">
							<input type="hidden" name="s[rooms]"'.$roomsValue.'>
							<div class="select_checkbox">
								<ul>
									<li'.($PageInfoAd['rooms']==1? ' class="checked"': "").'><a href="javascript:void(0)" data-id="1"><i class="triangle-bottomleft one"></i>1</a></li>
									<li'.($PageInfoAd['rooms']==2? ' class="checked"': "").'><a href="javascript:void(0)" data-id="2"><i class="triangle-bottomleft two"></i>2</a></li>
									<li'.($PageInfoAd['rooms']==3? ' class="checked"': "").'><a href="javascript:void(0)" data-id="3"><i class="triangle-bottomleft three"></i>3</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="cell label right one_second">
						<label style="margin-top:-2px">Комнат <br>всего</label>
					</div>
					<div class="cell one_second last_col">
						<div class="checkbox_block">
							<input type="hidden" name="s[rooms_flat]"'.$roomsFlatValue.'>
							<div class="select_checkbox">
								<ul>
									<li'.($PageInfoAd['rooms_flat']==1? ' class="checked"': "").'><a href="javascript:void(0)" data-id="1"><i class="triangle-bottomleft one"></i>1</a></li>
									<li'.($PageInfoAd['rooms_flat']==2? ' class="checked"': "").'><a href="javascript:void(0)" data-id="2"><i class="triangle-bottomleft two"></i>2</a></li>
									<li'.($PageInfoAd['rooms_flat']==3? ' class="checked"': "").'><a href="javascript:void(0)" data-id="3"><i class="triangle-bottomleft three"></i>3</a></li>
									<li'.($PageInfoAd['rooms_flat']==4? ' class="checked"': "").'><a href="javascript:void(0)" data-id="4"><i class="triangle-bottomleft more"></i>4</a></li>
									<li'.($PageInfoAd['rooms_flat']==5? ' class="checked"': "").'><a href="javascript:void(0)" data-id="5"><i class="triangle-bottomleft five"></i>5</a></li>
									<li'.($PageInfoAd['rooms_flat']==6? ' class="checked"': "").'><a href="javascript:void(0)" data-id="6"><i class="triangle-bottomleft five"></i>6+</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="line">
					<div class="block">
						<div class="row">
							<div class="cell label right one_second">
								<label>Этаж<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="text" class="text" name="s[floor]"'.$floor.'>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label style="margin-top:0">Общая площадь</label>
							</div>
							<div class="cell one_second last_col">
								<input type="text" class="text" name="s[full_square]"'.$full_square.'>
								<div class="com">м<sup>2</sup></div>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Кухня<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="text" class="text" name="s[kitchen_square]"'.$kitchen_square.'">
								<div class="com">м<sup>2</sup></div>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label style="margin-top:-2px">Высота потолков</label>
							</div>
							<div class="cell one_second last_col">
								<input type="text" class="text" name="s[ceiling_height]"'.$ceiling_height.'>
							</div>
						</div>
					</div>
					<div class="block">
						<div class="row">
							<div class="cell label right one_second">
								<label style="margin-top:0">Этажей в доме<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="text" class="text" name="s[floors]"'.$floors.'>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label style="margin-top:0">Жилая площадь<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="text" class="text" name="s[live_square]"'.$live_square.'>
								<div class="com">м<sup>2</sup></div>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label style="margin-top:0">Площадь комнат</label>
							</div>
							<div class="cell one_second last_col">
								<input placeholder="Пример: 20+15-10" type="text" class="text" name="s[rooms_square]"'.$rooms_square.'>
								<div class="com">м<sup>2</sup></div>
							</div>
						</div>
					</div>
				</div>
										
				<div class="row">
					<div class="cell info full">
						<p>В поле "Площадь комнат" используйте знак + для обозначения смежных комнат и знак - для раздельных комнат</p>
					</div>
				</div>
				<div class="row">
					<div style="margin-top:5px" class="cell right label one_second">Балкон</div>
					<div class="cell auto last_col">
						'.$balcony.'
					</div>
				</div>
				<div class="row">
					<div style="margin-top:5px" class="cell right label one_second">Лоджия</div>
					<div class="cell auto last_col">
						'.$loggia.'
					</div>
				</div>
				<div class="row">
					<div style="margin-top:5px" class="cell right label one_second">Сан.узел</div>
					<div class="cell auto last_col">
						'.$wc.'
					</div>
				</div>
				<div class="row">
					<div style="margin-top:5px" class="cell right label one_second">&nbsp;</div>
					<div class="cell auto last_col">
						'.$internet.'
						'.$phone.'
					</div>
				</div>
			</div>
		</div>
		'.$rightBlock.'
	</div>';
	?>