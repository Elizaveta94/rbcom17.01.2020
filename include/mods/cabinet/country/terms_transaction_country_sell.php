<?
echo '<div id="step-3" class="container_block ads">
	<div class="cost_block">
		<div class="left_block">
			<div class="table_form">
				<h2>Условия сделки</h2>
				<div class="row">
					<div class="cell label right one_second">
						<label>Стоимость<b>*</b></label>
					</div>
					<div style="width:202px" class="cell required one_second last_col">
						<input style="width:162px;float:left" type="text" class="text" name="s[price]"'.$price.'>
						<div class="com">руб.</div>
					</div>
				</div>
				<div class="row">
					<div class="cell label right one_second">
						<label style="margin-top:-4px">Комиссия от продавца клиентам<b>*</b></label>
					</div>
					<div style="width:202px" class="cell required one_second last_col">
						<input style="width:52px;float:left" type="text" class="text" name="s[prepayment]"'.$prepayment.'>
						<div class="com">%</div>
					</div>
				</div>
				<div class="line">
					<div style="margin:4px 0 0 115px" class="block">
						<div class="row">
							<div class="cell full">
								'.$ipoteka.'
							</div>
						</div>
					</div>
					<div style="margin:4px 0 0 50px" class="block">
						<div class="row">
							<div class="cell full">
								'.$subsidies.'
							</div>
						</div>
					</div>
					<div style="margin:4px 0 0 34px" class="block">
						<div class="row">
							<div class="cell full">
								'.$installment.'
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="right_block">
			<div class="table_form">
				<h2>Телефоны для связи</h2>
				<div class="row">
					<div class="cell info full">
						<p>В объявлении будет указан введённый ранее телефон</p>
					</div>
				</div>
				<div class="row">
					<div class="cell full">
						'.$reg_phone.'
					</div>
				</div>
				<div class="row">
					<div class="cell info full">
						<p>Дополнительно в этом объявлении можно указать ещё один или два номера</p>
					</div>
				</div>
				<div class="row">
					<div style="width:auto;margin:8px 10px 0 0" class="cell code">+7</div>
					<div class="cell code">
						<input maxlength="5" placeholder="код" type="text" class="code phone_code" name="phones[code][1]"'.$codePhone1.'>
					</div>
					<div style="width:204px" class="cell one_second last_col">
						<input type="text" class="phone_type phone_mask" name="phones[number][1]"'.$codeNumber1.'>
					</div>
				</div>
				<div class="row">
					<div style="width:auto;margin:8px 10px 0 0" class="cell code">+7</div>
					<div class="cell code">
						<input maxlength="5" placeholder="код" type="text" class="code phone_code" name="phones[code][2]"'.$codePhone2.'>
					</div>
					<div style="width:204px" class="cell one_second last_col">
						<input type="text" class="phone_type phone_mask" name="phones[number][2]"'.$codeNumber2.'>
					</div>
				</div>
				<div class="row">
					<div class="cell info full">
						<p>Почта для получения запросов от покупателей:</p>
					</div>
				</div>
				<div class="row">
					<div style="width:300px" class="cell one_second last_col">
						<input style="width:282px" type="text" name="s[email]"'.$emailValue.'>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="btn_info cost"></div>
</div>';
?>