<?
/*
*  Коммуникации
*/
$communications = '';
$communications2 = '';
$valueInput = '';
for($c=0; $c<count($_COMMUNICATIONS); $c++){
	$checked = '';
	if(isset($PageInfoAd['communications']) && $PageInfoAd['communications']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	$communications2 .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_COMMUNICATIONS[$c].'</a></li>';
}
$communications = '<div class="checkbox_block">';
$communications .= '<input type="hidden" name="s[communications]"'.$valueInput.'>';
$communications .= '<div class="select_checkbox">';
$communications .= '<ul>';
$communications .= $communications2;
$communications .= '</ul>';
$communications .= '</div>';
$communications .= '</div>';

/*
*  Водоснабжение
*/
$plumbing = '';
$plumbing2 = '';
$valueInput = '';
for($c=0; $c<count($_PLUMBING); $c++){
	$checked = '';
	if(isset($PageInfoAd['plumbing']) && $PageInfoAd['plumbing']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	$plumbing2 .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_PLUMBING[$c].'</a></li>';
}
$plumbing = '<div class="checkbox_block">';
$plumbing .= '<input type="hidden" name="s[plumbing]"'.$valueInput.'>';
$plumbing .= '<div class="select_checkbox">';
$plumbing .= '<ul>';
$plumbing .= $plumbing2;
$plumbing .= '</ul>';
$plumbing .= '</div>';
$plumbing .= '</div>';

/*
*  Канализация
*/
$sewerage = '';
$sewerage2 = '';
$valueInput = '';
for($c=0; $c<count($_SEWERAGE); $c++){
	$checked = '';
	if(isset($PageInfoAd['sewerage']) && $PageInfoAd['sewerage']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	$sewerage2 .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_SEWERAGE[$c].'</a></li>';
}
$sewerage = '<div class="checkbox_block">';
$sewerage .= '<input type="hidden" name="s[sewerage]"'.$valueInput.'>';
$sewerage .= '<div class="select_checkbox">';
$sewerage .= '<ul>';
$sewerage .= $sewerage2;
$sewerage .= '</ul>';
$sewerage .= '</div>';
$sewerage .= '</div>';

/*
*  Интернет
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['internet'])){
	if($PageInfoAd['internet']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['internet'].'"';
	}
}
$internet = '<div style="float:left;margin-right:40px" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[internet]" value="0">
	<input type="hidden"'.$valueParams.' name="s[internet]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Интернет</span>
	</div>
</div>';

/*
*  Телефон
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['phone'])){
	if($PageInfoAd['phone']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['phone'].'"';
	}
}
$phone = '<div style="float:left" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[phone]" value="0">
	<input type="hidden"'.$valueParams.' name="s[phone]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Телефон</span>
	</div>
</div>';

/*
*  Ремонт
*/
$finish = '';
$finishes = '';
$valueInput = '';
for($c=1; $c<count($_REPAIRS); $c++){
	$checked = '';
	if(isset($PageInfoAd['repairs']) && $PageInfoAd['repairs']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	if(empty($PageInfoAd['repairs'])){
		if($c==1){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';				
		}
	}
	$finishes .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_REPAIRS[$c].'</a></li>';
}
$finish = '<div class="checkbox_block">';
$finish .= '<input type="hidden" name="s[repairs]"'.$valueInput.'>';
$finish .= '<div class="select_checkbox">';
$finish .= '<ul>';
$finish .= $finishes;
$finish .= '</ul>';
$finish .= '</div>';
$finish .= '</div>';

/*
*  Круглосуточная охрана территории
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['security'])){
	if($PageInfoAd['security']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['security'].'"';
	}
}
$security = '<div style="float:left" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[security]" value="0">
	<input type="hidden"'.$valueParams.' name="s[security]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Круглосуточная охрана территории</span>
	</div>
</div>';

/*
*  Ограждение территории поселка
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['fencing'])){
	if($PageInfoAd['fencing']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['fencing'].'"';
	}
}
$fencing = '<div style="float:left" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[fencing]" value="0">
	<input type="hidden"'.$valueParams.' name="s[fencing]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Ограждение территории поселка</span>
	</div>
</div>';

/*
*  Служба эксплуатации в поселке	
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['maintenance'])){
	if($PageInfoAd['maintenance']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['maintenance'].'"';
	}
}
$maintenance = '<div style="float:left" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[maintenance]" value="0">
	<input type="hidden"'.$valueParams.' name="s[maintenance]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Служба эксплуатации в поселка</span>
	</div>
</div>';

/*
*  Кафе	
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['cafe'])){
	if($PageInfoAd['cafe']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['cafe'].'"';
	}
}
$cafe = '<div style="float:left" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[cafe]" value="0">
	<input type="hidden"'.$valueParams.' name="s[cafe]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Кафе</span>
	</div>
</div>';

/*
*  Ресторан	
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['restaurant'])){
	if($PageInfoAd['restaurant']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['restaurant'].'"';
	}
}
$restaurant = '<div style="float:left" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[restaurant]" value="0">
	<input type="hidden"'.$valueParams.' name="s[restaurant]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Ресторан</span>
	</div>
</div>';

/*
*  Спортивный зал
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['gym'])){
	if($PageInfoAd['gym']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['gym'].'"';
	}
}
$gym = '<div style="float:left" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[gym]" value="0">
	<input type="hidden"'.$valueParams.' name="s[gym]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Спортивный зал</span>
	</div>
</div>';

/*
*  Медпункт
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['first_post'])){
	if($PageInfoAd['first_post']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['first_post'].'"';
	}
}
$first_post = '<div style="float:left" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[first_post]" value="0">
	<input type="hidden"'.$valueParams.' name="s[first_post]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Медпункт</span>
	</div>
</div>';

/*
*  Детская площадка
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['playground'])){
	if($PageInfoAd['playground']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['playground'].'"';
	}
}
$playground = '<div style="float:left" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[playground]" value="0">
	<input type="hidden"'.$valueParams.' name="s[playground]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Детская площадка</span>
	</div>
</div>';

/*
*  Гостевая парковка
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['guest_parking'])){
	if($PageInfoAd['guest_parking']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['guest_parking'].'"';
	}
}
$guest_parking = '<div style="float:left" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[guest_parking]" value="0">
	<input type="hidden"'.$valueParams.' name="s[guest_parking]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Гостевая парковка</span>
	</div>
</div>';

/*
*  Гостевая парковка
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['polyclinic'])){
	if($PageInfoAd['polyclinic']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['polyclinic'].'"';
	}
}
$polyclinic = '<div style="float:left" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[polyclinic]" value="0">
	<input type="hidden"'.$valueParams.' name="s[polyclinic]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Поликлинника</span>
	</div>
</div>';

/*
*  Детский сад
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['kindergarten'])){
	if($PageInfoAd['kindergarten']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['kindergarten'].'"';
	}
}
$kindergarten = '<div style="float:left" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[kindergarten]" value="0">
	<input type="hidden"'.$valueParams.' name="s[kindergarten]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Детский сад</span>
	</div>
</div>';

/*
*  Пирс
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['pier'])){
	if($PageInfoAd['pier']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['pier'].'"';
	}
}
$pier = '<div style="float:left" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[pier]" value="0">
	<input type="hidden"'.$valueParams.' name="s[pier]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Пирс</span>
	</div>
</div>';

/*
*  Школа
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['school'])){
	if($PageInfoAd['school']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['school'].'"';
	}
}
$school = '<div style="float:left" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[school]" value="0">
	<input type="hidden"'.$valueParams.' name="s[school]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Школа</span>
	</div>
</div>';

$rightBlock = '<div style="border-left:none" class="right_block">
	<div class="table_form">
		<h2>Информация о поселке</h2>
		<div class="line">
			<div style="width:100%" class="block">
				<div class="row">
					<div style="width:140px" class="cell label right one_second">
						<label style="white-space:nowrap;margin-left:-3px;margin-top:-2px">Название<br> поселка</label>
					</div>
					<div class="cell one_second last_col">
						<input style="width:340px" type="text" class="text" name="s[name_villiage]"'.$name_villiage.'>
					</div>
				</div>
				<div style="margin-left:30px" class="line">
					<div class="row">
						<div style="width:260px" class="cell auto last_col">
							'.$internet.'
							'.$cab_tv.'
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="line">
			<div style="width:246px;margin-left:30px" class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$security.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$fencing.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$maintenance.'
					</div>
				</div>
			</div>
		</div>
		<div class="line">
			<div style="width:246px;margin-left:30px" class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$shop.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$cafe.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$playground.'
					</div>
				</div>
			</div>
			<div class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$sport_playground.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$first_post.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$guest_parking.'
					</div>
				</div>
			</div>
		</div>
		<div class="line">
			<div style="width:180px;margin-left:30px" class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$water.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$polyclinic.'
					</div>
				</div>
			</div>
			<div style="width:130px" class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:130px" class="cell one_second last_col">
						'.$pier.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:140px" class="cell one_second last_col">
						'.$school.'
					</div>
				</div>
			</div>
			<div style="width:150px" class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$forest.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$kindergarten.'
					</div>
				</div>
			</div>
		</div>
	</div>
</div>';

echo '<div class="params_block">
		<div style="border-right:1px solid #b5babe" class="left_block">
			<div class="table_form">
				<h2 style="margin-top:30px">Информация об участке</h2>
				<div class="line">
					<div style="width:336px" class="block">
						<div class="row">
							<div style="width:102px!important" class="cell label right one_second">
								<label style="white-space:nowrap;margin-left:-3px;margin-top:-2px">Площадь<br> участка<b>*</b></label>
							</div>
							<div class="cell one_second last_col required">
								<input style="width:82px" type="text" class="text" name="s[area_square]"'.$area_square.'>
								<div style="margin-top:8px" class="com">соток</div>
							</div>
						</div>
						<div class="row">
							<div style="width:102px!important" class="cell label right one_second">
								<label style="white-space:nowrap;margin-left:-3px;margin-top:-2px">Кадастровый<br> номер</label>
							</div>
							<div class="cell one_second last_col">
								<input type="text" class="text" name="s[cadastr]"'.$cadastr.'>
							</div>
						</div>
					</div>
				</div>
				<div class="line">
					<div style="width:100%" class="block">
						<div class="row">
							<div style="width:102px!important" class="cell label right one_second">
								<label style="margin-top:-2px">Разрешенное использования</label>
							</div>
							<div class="cell one_second last_col">
								'.$type_use.'
							</div>
						</div>
						<div class="row">
							<div style="width:102px!important" class="cell label right one_second">
								<label>Категория ЗУ</label>
							</div>
							<div class="cell one_second last_col">
								'.$category.'
							</div>
						</div>
						<div class="row">
							<div style="margin-top:5px;width:102px!important" class="cell right label one_second">Право на ЗУ</div>
							<div style="width:310px" class="cell auto last_col">
								'.$rights.'
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="table_form">
				<h2 style="margin-top:30px">Коммуникации на участке</h2>
				<div class="line">
					<div style="width:100%" class="block">
						<div class="row">
							<div style="width:102px!important" class="cell label right one_second">
								<label>Электричество</label>
							</div>
							<div style="width:auto" class="cell one_second last_col">
								<input style="width:85px" type="text" class="text" name="s[power_max]"'.$power_max.'>
								<div style="margin-top:8px" class="com">кВт</div>
							</div>
						</div>
						<div class="row">
							<div style="width:102px!important" class="cell label right one_second">
								<label></label>
							</div>
							<div style="width:auto" class="cell one_second last_col">
								'.$communications.'
							</div>
						</div>
						<div class="row">
							<div style="width:102px!important" class="cell label right one_second">
								<label style="margin-top:5px">Водоснабжение</label>
							</div>
							<div style="width:auto" class="cell one_second last_col">
								'.$plumbing.'
							</div>
						</div>
						<div class="row">
							<div style="width:102px!important" class="cell label right one_second">
								<label style="margin-top:5px">Канализация</label>
							</div>
							<div style="width:auto" class="cell one_second last_col">
								'.$sewerage.'
							</div>
						</div>
						<div class="row">
							<div style="width:102px!important" class="cell label right one_second">
								<label style="margin-top:5px">Газоснабжение</label>
							</div>
							<div style="width:auto" class="cell one_second last_col">
								'.$gassupply.'
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		'.$rightBlock.'
	</div>';
?>