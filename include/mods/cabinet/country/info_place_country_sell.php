<?
/*
*  Тип дома
*/
$type_house = '<div class="select_block count">';
$type_houses = '';
$nameParams = 'Не важно';
$valueInput = '';
for($c=0; $c<count($_TYPE_HOUSE_COUNTRY); $c++){
	$current = "";	
	if($PageInfoAd['type_house']==$c){
		$current = ' class="current"';
		$nameParams = $_TYPE_HOUSE_COUNTRY[$c];
		$valueInput = ' value="'.$c.'"';
	}
	$type_houses .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_HOUSE_COUNTRY[$c].'</a></li>';
}
$type_house .= '<input type="hidden" name="s[type_house]"'.$valueInput.'>';
$type_house .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$type_house .= '<div class="scrollbar-inner">';
$type_house .= '<ul>';
$type_house .= $type_houses;
$type_house .= '</ul>';
$type_house .= '</div>';
$type_house .= '</div>';

/*
*  Застройщик
*/
$developer = '';
if(isset($PageInfoAd['developer']) && !empty($PageInfoAd['developer'])){
	$developer = ' value="'.$PageInfoAd['developer'].'"';
}
	
/*
*  Площадь посёлка
*/
$area_village = '';
if(isset($PageInfoAd['area_village']) && !empty($PageInfoAd['area_village'])){
	$area_village = ' value="'.$PageInfoAd['area_village'].'"';
}
	
/*
*  Количество участков
*/
$count_plots = '';
if(isset($PageInfoAd['count_plots']) && !empty($PageInfoAd['count_plots'])){
	$count_plots = ' value="'.$PageInfoAd['count_plots'].'"';
}

/*
*  Водоснабжение
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['plumbing'])){
	if($PageInfoAd['plumbing']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['plumbing'].'"';
	}
}
$plumbing = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[plumbing]" value="0">
	<input type="hidden"'.$valueParams.' name="s[plumbing]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Водоснабжение</span>
	</div>
</div>';

/*
*  Канализация
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['sewerage'])){
	if($PageInfoAd['sewerage']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['sewerage'].'"';
	}
}
$sewerage = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[sewerage]" value="0">
	<input type="hidden"'.$valueParams.' name="s[sewerage]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Канализация</span>
	</div>
</div>';

/*
*  Теплоснабжение
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['heating'])){
	if($PageInfoAd['heating']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['heating'].'"';
	}
}
$heating = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[heating]" value="0">
	<input type="hidden"'.$valueParams.' name="s[heating]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Теплоснабжение</span>
	</div>
</div>';

/*
*  Газоснабжение
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['gas'])){
	if($PageInfoAd['gas']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['gas'].'"';
	}
}
$gas = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[gas]" value="0">
	<input type="hidden"'.$valueParams.' name="s[gas]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Газоснабжение</span>
	</div>
</div>';

/*
*  Баня
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['sauna'])){
	if($PageInfoAd['sauna']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['sauna'].'"';
	}
}
$sauna = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[sauna]" value="0">
	<input type="hidden"'.$valueParams.' name="s[sauna]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Баня</span>
	</div>
</div>';

/*
*  Бассейн
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['pool'])){
	if($PageInfoAd['pool']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['pool'].'"';
	}
}
$pool = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[pool]" value="0">
	<input type="hidden"'.$valueParams.' name="s[pool]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Бассейн</span>
	</div>
</div>';

/*
*  Гараж
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['garage'])){
	if($PageInfoAd['garage']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['garage'].'"';
	}
}
$garage = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[garage]" value="0">
	<input type="hidden"'.$valueParams.' name="s[garage]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Гараж</span>
	</div>
</div>';

/*
*  Рядом лес
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['forest'])){
	if($PageInfoAd['forest']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['forest'].'"';
	}
}
$forest = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[forest]" value="0">
	<input type="hidden"'.$valueParams.' name="s[forest]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Рядом лес</span>
	</div>
</div>';

/*
*  Водоем
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['water'])){
	if($PageInfoAd['water']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['water'].'"';
	}
}
$water = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[water]" value="0">
	<input type="hidden"'.$valueParams.' name="s[water]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Водоем</span>
	</div>
</div>';
	
/*
*  Право на ЗУ
*/
$rights = '';
$rights2 = '';
$valueInput = '';
for($c=1; $c<count($_RIGHTS); $c++){
	$checked = '';
	if(isset($PageInfoAd['rights']) && $PageInfoAd['rights']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	$rights2 .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_RIGHTS[$c].'</a></li>';
}
$rights = '<div class="checkbox_block">';
$rights .= '<input type="hidden" name="s[rights]"'.$valueInput.'>';
$rights .= '<div class="select_checkbox">';
$rights .= '<ul>';
$rights .= $rights2;
$rights .= '</ul>';
$rights .= '</div>';
$rights .= '</div>';

/*
*  Категория
*/
$category = '<div class="select_block count">';
$categories = '';
$nameParams = 'Не важно';
$valueInput = '';
for($c=0; $c<count($_CATEGORY_COUNTRY); $c++){
	$current = '';
	if(isset($PageInfoAd['category']) && $PageInfoAd['category']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
		$nameParams = $_CATEGORY_COUNTRY[$c];
	}
	$categories .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_CATEGORY_COUNTRY[$c].'</a></li>';
}
$category .= '<input type="hidden" name="s[category]"'.$valueInput.'>';
$category .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$category .= '<div class="scrollbar-inner">';
$category .= '<ul>';
$category .= $categories;
$category .= '</ul>';
$category .= '</div>';
$category .= '</div>';

/*
*  Газоснабжение
*/
$gassupply = '<div class="select_block count">';
$gassupply2 = '';
$nameParams = 'Не важно';
$valueInput = '';
for($c=0; $c<count($_GASSUPPLY); $c++){
	$current = '';
	if(isset($PageInfoAd['gas_supply']) && $PageInfoAd['gas_supply']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
		$nameParams = $_GASSUPPLY[$c];
	}
	$gassupply2 .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_GASSUPPLY[$c].'</a></li>';
}
$gassupply .= '<input type="hidden" name="s[gas_supply]"'.$valueInput.'>';
$gassupply .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$gassupply .= '<div class="scrollbar-inner">';
$gassupply .= '<ul>';
$gassupply .= $gassupply2;
$gassupply .= '</ul>';
$gassupply .= '</div>';
$gassupply .= '</div>';

/*
*  Вид использования
*/
$type_use = '<div class="select_block count">';
$types_use = '';
$nameParams = 'Не важно';
$valueInput = '';
for($c=0; $c<count($_TYPES_USE); $c++){
	$current = '';
	if(isset($PageInfoAd['type_use']) && $PageInfoAd['type_use']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
		$nameParams = $_TYPES_USE[$c];
	}
	$types_use .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPES_USE[$c].'</a></li>';
}
$type_use .= '<input type="hidden" name="s[type_use]"'.$valueInput.'>';
$type_use .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$type_use .= '<div class="scrollbar-inner">';
$type_use .= '<ul>';
$type_use .= $types_use;
$type_use .= '</ul>';
$type_use .= '</div>';
$type_use .= '</div>';


/*
*  Круглосуточная охрана территории
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['security'])){
	if($PageInfoAd['security']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['security'].'"';
	}
}
$security = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[security]" value="0">
	<input type="hidden"'.$valueParams.' name="s[security]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Круглосуточная охрана территории</span>
	</div>
</div>';

/*
*  Ограждение территории поселка
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['fencing'])){
	if($PageInfoAd['fencing']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['fencing'].'"';
	}
}
$fencing = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[fencing]" value="0">
	<input type="hidden"'.$valueParams.' name="s[fencing]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Ограждение территории поселка</span>
	</div>
</div>';

/*
*  Служба эксплуатации в поселке
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['maintenance'])){
	if($PageInfoAd['maintenance']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['maintenance'].'"';
	}
}
$maintenance = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[maintenance]" value="0">
	<input type="hidden"'.$valueParams.' name="s[maintenance]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Служба эксплуатации в поселке</span>
	</div>
</div>';

/*
*  Магазин
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['shop'])){
	if($PageInfoAd['shop']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['shop'].'"';
	}
}
$shop = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[shop]" value="0">
	<input type="hidden"'.$valueParams.' name="s[shop]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Магазин</span>
	</div>
</div>';

/*
*  Медпункт
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['first_post'])){
	if($PageInfoAd['first_post']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['first_post'].'"';
	}
}
$first_post = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[first_post]" value="0">
	<input type="hidden"'.$valueParams.' name="s[first_post]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Медпункт</span>
	</div>
</div>';

/*
*  Кафе
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['cafe'])){
	if($PageInfoAd['cafe']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['cafe'].'"';
	}
}
$cafe = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[cafe]" value="0">
	<input type="hidden"'.$valueParams.' name="s[cafe]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Кафе</span>
	</div>
</div>';

/*
*  Ресторан
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['restaurant'])){
	if($PageInfoAd['restaurant']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['restaurant'].'"';
	}
}
$restaurant = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[restaurant]" value="0">
	<input type="hidden"'.$valueParams.' name="s[restaurant]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Ресторан</span>
	</div>
</div>';

/*
*  Спортивная площадка
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['sport_playground'])){
	if($PageInfoAd['sport_playground']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['sport_playground'].'"';
	}
}
$sport_playground = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[sport_playground]" value="0">
	<input type="hidden"'.$valueParams.' name="s[sport_playground]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Спортивная площадка</span>
	</div>
</div>';

/*
*  Тренажерный зал
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['gym'])){
	if($PageInfoAd['gym']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['gym'].'"';
	}
}
$gym = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[gym]" value="0">
	<input type="hidden"'.$valueParams.' name="s[gym]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Тренажерный зал</span>
	</div>
</div>';

/*
*  Детская площадка
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['playground'])){
	if($PageInfoAd['playground']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['playground'].'"';
	}
}
$playground = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[playground]" value="0">
	<input type="hidden"'.$valueParams.' name="s[playground]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Детская площадка</span>
	</div>
</div>';


/*
*  Электричество (загородная)
*/
$electricity = '';
if(isset($PageInfoAd['electricity']) && !empty($PageInfoAd['electricity'])){
	$electricity = ' value="'.$PageInfoAd['electricity'].'"';
}

/*
*  Коттеджный поселок
*/
if(isset($type_country) && $type_country==1){
	require_once("include/mods/cabinet/country/info/temp1_country_sell.php");
}

/*
*  Таунхаус
*/
if(isset($type_country) && $type_country==2){
	require_once("include/mods/cabinet/country/info/temp2_country_sell.php");
}

/*
*  Дом
*/
if(isset($type_country) && $type_country==3){
	require_once("include/mods/cabinet/country/info/temp3_country_sell.php");
}

/*
*  Земельный участок
*/
if(isset($type_country) && $type_country==4){
	require_once("include/mods/cabinet/country/info/temp4_country_sell.php");
}

/*
*  Коттедж
*/
if(isset($type_country) && $type_country==5){
	require_once("include/mods/cabinet/country/info/temp5_country_sell.php");
}

/*
*  Земельный участок
*/
if(isset($type_country) && $type_country==6){
	require_once("include/mods/cabinet/country/info/temp6_country_sell.php");
}

?>