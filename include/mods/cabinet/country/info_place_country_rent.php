<?
/*
*  Водоснабжение
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['plumbing'])){
	if($PageInfoAd['plumbing']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['plumbing'].'"';
	}
}
$plumbing = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[plumbing]" value="0">
	<input type="hidden"'.$valueParams.' name="s[plumbing]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Водоснабжение</span>
	</div>
</div>';

/*
*  Канализация
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['sewerage'])){
	if($PageInfoAd['sewerage']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['sewerage'].'"';
	}
}
$sewerage = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[sewerage]" value="0">
	<input type="hidden"'.$valueParams.' name="s[sewerage]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Канализация</span>
	</div>
</div>';

/*
*  Теплоснабжение
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['heating'])){
	if($PageInfoAd['heating']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['heating'].'"';
	}
}
$heating = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[heating]" value="0">
	<input type="hidden"'.$valueParams.' name="s[heating]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Теплоснабжение</span>
	</div>
</div>';

/*
*  Газоснабжение
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['gas'])){
	if($PageInfoAd['gas']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['gas'].'"';
	}
}
$gas = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[gas]" value="0">
	<input type="hidden"'.$valueParams.' name="s[gas]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Газоснабжение</span>
	</div>
</div>';

/*
*  Баня
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['sauna'])){
	if($PageInfoAd['sauna']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['sauna'].'"';
	}
}
$sauna = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[sauna]" value="0">
	<input type="hidden"'.$valueParams.' name="s[sauna]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Баня</span>
	</div>
</div>';

/*
*  Гараж
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['garage'])){
	if($PageInfoAd['garage']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['garage'].'"';
	}
}
$garage = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[garage]" value="0">
	<input type="hidden"'.$valueParams.' name="s[garage]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Гараж</span>
	</div>
</div>';

/*
*  Рядом лес
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['forest'])){
	if($PageInfoAd['forest']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['forest'].'"';
	}
}
$forest = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[forest]" value="0">
	<input type="hidden"'.$valueParams.' name="s[forest]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Рядом лес</span>
	</div>
</div>';

/*
*  Водоем
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['water'])){
	if($PageInfoAd['water']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['water'].'"';
	}
}
$water = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[water]" value="0">
	<input type="hidden"'.$valueParams.' name="s[water]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Водоем</span>
	</div>
</div>';

$rightBlock = '<div style="border-left:1px solid #b5babe" class="right_block">
	<div class="table_form">
		<h2>Информация об участке</h2>
		<div class="line">
			<div style="width:296px" class="block">
				<div class="row">
					<div class="cell label right one_second">
						<label style="white-space:nowrap;margin-left:-3px;margin-top:-2px">Площадь<br> участка</label>
					</div>
					<div class="cell one_second last_col">
						<input type="text" class="text" name="s[area_square]">
					</div>
				</div>
<!--			<div class="row">
					<div class="cell label right one_second"><label></label></div>
					<div class="cell one_second last_col"></div>
				</div>-->
			</div>
			<div style="width:200px" class="block">
				<div style="margin-right:-10px" class="row">
					<div class="cell label right one_second">
						<label>Электричество</label>
					</div>
					<div style="width:90px" class="cell one_second last_col">
						<input style="width:50px" type="text" class="text" name="s[electricity]">
						<div style="margin-top:8px" class="com">кВт</div>
					</div>
				</div>
			</div>
		</div>
		<div class="line">
			<div style="width:246px;margin-left:50px" class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$plumbing.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$sewerage.'
					</div>
				</div>
			</div>
			<div class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$heating.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$gas.'
					</div>
				</div>
			</div>
		</div>
		<div class="line">
			<div style="width:246px;margin-left:50px" class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$protected_area.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$playground.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$sport_playground.'
					</div>
				</div>
			</div>
			<div class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$sauna.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$garage.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$forest.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$water.'
					</div>
				</div>
			</div>
		</div>
		<div class="line">
			<h3 class="terms">Условия сдачи</h3>
			<div style="margin-left:50px;width:246px" class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$furniture.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$animal.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$children.'
					</div>
				</div>
			</div>
			<div class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$tv.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$fridge.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$washer.'
					</div>
				</div>
			</div>
		</div>
	</div>
</div>';

/*
*  Интернет
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['internet'])){
	if($PageInfoAd['internet']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['internet'].'"';
	}
}
$internet = '<div style="float:left;margin-right:40px" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[internet]" value="0">
	<input type="hidden"'.$valueParams.' name="s[internet]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Интернет</span>
	</div>
</div>';

/*
*  Телефон
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['phone'])){
	if($PageInfoAd['phone']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['phone'].'"';
	}
}
$phone = '<div style="float:left" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[phone]" value="0">
	<input type="hidden"'.$valueParams.' name="s[phone]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Телефон</span>
	</div>
</div>';

/*
*  Высота потолков
*/
$height_ceiling = '<div class="select_block count">';
$height_ceilings = '';
$nameParams = 'Не важно';
$valueInput = '';
$devs = mysql_query("
	SELECT *
	FROM ".$template."_type_house
	WHERE activation='1'
	ORDER BY num
");
if(mysql_num_rows($devs)>0){
	$n = 0;
	while($dev = mysql_fetch_assoc($devs)){
		$current = "";
		
		if($n==0 && !isset($PageInfoAd['ceiling_height'])){
			$height_ceilings .= '<li class="current"><a href="javascript:void(0)" data-id="">Не важно</a></li>';
		}
		if($PageInfoAd['ceiling_height']==$dev['id']){
			$current = ' class="current"';
			$nameParams = $dev['name'];
			$valueInput = ' value="'.$dev['id'].'"';
		}
		$height_ceilings .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$dev['id'].'">'.$dev['name'].'</a></li>';
		$n++;
	}
}
$height_ceiling .= '<input type="hidden" name="s[ceiling_height]"'.$valueInput.'>';
$height_ceiling .= '<div style="width:162px" class="choosed_block">'.$nameParams.'</div>';
$height_ceiling .= '<div class="scrollbar-inner">';
$height_ceiling .= '<ul>';
$height_ceiling .= $height_ceilings;
$height_ceiling .= '</ul>';
$height_ceiling .= '</div>';
$height_ceiling .= '</div>';

/*
*  Ремонт
*/
$finish = '';
$finishes = '';
$valueInput = '';
for($c=1; $c<count($_REPAIRS); $c++){
	$checked = '';
	if(isset($PageInfoAd['repairs']) && $PageInfoAd['repairs']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	if(empty($PageInfoAd['repairs'])){
		if($c==1){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';				
		}
	}
	$finishes .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_REPAIRS[$c].'</a></li>';
}
$finish = '<div class="checkbox_block">';
$finish .= '<input type="hidden" name="s[repairs]"'.$valueInput.'>';
$finish .= '<div class="select_checkbox">';
$finish .= '<ul>';
$finish .= $finishes;
$finish .= '</ul>';
$finish .= '</div>';
$finish .= '</div>';

echo '<div class="params_block">
		<div style="border-right:none" class="left_block">
			<div class="table_form">
				<h2>Информация о строении</h2>
				<div class="line">
					<div class="block">
						<div class="row">
							<div class="cell label right one_second">
								<label style="margin-top:-2px">Тип дома</label>
							</div>
							<div class="cell one_second last_col">
								'.$type_house.'
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label style="margin-top:0">Этажей в доме</label>
							</div>
							<div class="cell one_second last_col">
								<input type="text" class="text" name="s[floors]"'.$floors.'>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label style="margin-top:0">Общая площадь<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="text" class="text" name="s[full_square]"'.$full_square.'>
								<div class="com">м<sup>2</sup></div>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Кухня</label>
							</div>
							<div class="cell one_second last_col">
								<input type="text" class="text" name="s[kitchen_square]"'.$kitchen_square.'">
								<div class="com">м<sup>2</sup></div>
							</div>
						</div>
					</div>
					<div class="block">
						<div class="row">
							<div class="cell label right one_second">
								<label style="white-space:nowrap;margin-left:-3px;margin-top:-2px">Год<br> постройки</label>
							</div>
							<div class="cell one_second last_col">
								<input type="text" class="text" name="s[year]"'.$year.'>
							</div>
						</div>
					</div>
				</div>										
				<div class="row">
					<div style="margin-top:5px" class="cell right label one_second">Сан.узел</div>
					<div class="cell auto last_col">
						'.$wc.'
					</div>
				</div>
				<div class="row">
					<div style="margin-top:5px" class="cell right label one_second">Ремонт</div>
					<div class="cell auto last_col">
						'.$finish.'
					</div>
				</div>
				<div class="row">
					<div style="margin-top:5px" class="cell right label one_second">&nbsp;</div>
					<div class="cell auto last_col">
						'.$internet.'
						'.$phone.'
					</div>
				</div>
			</div>
		</div>
		'.$rightBlock.'
	</div>';
?>