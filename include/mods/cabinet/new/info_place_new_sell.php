<?
$rightBlock = '<div class="right_block">
	<div class="table_form">
		<h2>Информация о доме</h2>
		<!--<div class="row">
			<div style="width:74px;margin-right:15px;" class="cell label right one_second">
				<label>Название</label>
			</div>
			<div style="width:396px" class="cell one_second last_col">
				<input type="text" class="text" name="s[name]">
			</div>
		</div>-->
		<div class="line">
			<div class="block">
				<!--<div class="row">
					<div class="cell label right one_second">
						<label>Очередь</label>
					</div>
					<div class="cell one_second last_col">
						<input type="text" class="text" name="queues[queue]"'.$queues.'>
					</div>
				</div>-->
				<div class="row">
					<div class="cell label right one_second">
						<label>Срок сдачи</label>
					</div>
					<div class="cell one_second last_col">
						'.$delivery.'
					</div>
				</div>
				<div class="row">
					<div class="cell label right one_second">
						<label>Тип дома</label>
					</div>
					<div class="cell one_second last_col">
						'.$type_house.'
					</div>
				</div>
				<div class="row">
					<div class="cell label right one_second"><label></label></div>
					<div class="cell one_second last_col"></div>
				</div>
				<div class="row">
					<div style="text-align:right" class="cell label one_second">Лифт грузовой</div>
					<div class="cell auto last_col">
						'.$lift_service.'
					</div>
				</div>
				<div class="row">
					<div style="text-align:right" class="cell label one_second">Лифт пассажирский</div>
					<div class="cell auto last_col">
						'.$lift.'
					</div>
				</div>
			</div>
			<div class="block">
				<!--<div class="row">
					<div class="cell label right one_second">
						<label>Корпус</label>
					</div>
					<div class="cell one_second last_col">
						<input type="text" class="text" name="queues[building]"'.$building.'>
					</div>
				</div>
				<div class="row">
					<div class="cell label right one_second">
						<label>Серия дома</label>
					</div>
					<div class="cell one_second last_col">
						<input placeholder="П-404" type="text" class="text" name="">
					</div>
				</div>-->
				<div class="row">
					<div class="cell one_second last_col">
						'.$ready.'
					</div>
				</div>
				<div class="row">
					<div class="cell label right one_second">
						<label></label>
					</div>
					<div class="cell one_second last_col"></div>
				</div>
			</div>
		</div>
		<div class="line">
			<div class="block">
				<div class="row">
					<div class="cell label right one_second"><label></label></div>
					<div class="cell one_second last_col"></div>
				</div>
				<div class="row">
					<div class="cell label right one_second">&nbsp;</div>
					<div style="width:200px" class="cell one_second last_col">
						'.$parking.'
					</div>
				</div>
				<div class="row">
					<div class="cell label right one_second">&nbsp;</div>
					<div style="width:200px" class="cell one_second last_col">
						'.$rubbish.'
					</div>
				</div>
				<div class="row">
					<div class="cell label right one_second">&nbsp;</div>
					<div style="width:200px" class="cell one_second last_col">
						'.$protected_area.'
					</div>
				</div>
				<div class="row">
					<div class="cell label right one_second">&nbsp;</div>
					<div style="width:200px" class="cell one_second last_col">
						'.$playground.'
					</div>
				</div>
			</div>
		</div>
	</div>
</div>';

echo '<div class="params_block">
		<div class="left_block">
			<div class="table_form">
				<h2>Информация о квартире</h2>
				'.$roomsParams.'
				<div class="line">
					<div class="block">
						<div class="row">
							<div class="cell label right one_second">
								<label>Этаж<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="text" class="text" name="s[floor]"'.$floor.'>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label style="margin-top:0">Общая площадь<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="text" class="text" name="s[full_square]"'.$full_square.'>
								<div class="com">м<sup>2</sup></div>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Кухня</label>
							</div>
							<div class="cell one_second last_col">
								<input type="text" class="text" name="s[kitchen_square]"'.$kitchen_square.'">
								<div class="com">м<sup>2</sup></div>
							</div>
						</div>
					</div>
					<div style="margin-top:-52px" class="block">
						<div class="row">
							<div class="cell label right one_second">
								<label style="margin-top:0">Высота потолков</label>
							</div>
							<div class="cell one_second last_col">
								<input type="text" class="text" name="s[ceiling_height]"'.$ceiling_height.'>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label style="margin-top:0">Этажей в доме<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="text" class="text" name="s[floors]"'.$floors.'>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label style="margin-top:0">Жилая площадь<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="text" class="text" name="s[live_square]"'.$live_square.'>
								<div class="com">м<sup>2</sup></div>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label style="margin-top:0">Площадь комнат<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input placeholder="Пример: 20+15-10" type="text" class="text" name="s[rooms_square]"'.$rooms_square.'>
								<div class="com">м<sup>2</sup></div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="cell info full">
						<p>В поле "Площадь комнат" используйте знак + для обозначения смежных комнат и знак - для раздельных комнат</p>
					</div>
				</div>
				<div class="row">
					<div style="margin-top:5px" class="cell right label one_second">Балкон</div>
					<div class="cell auto last_col">
						'.$balcony.'
					</div>
				</div>
				<div class="row">
					<div style="margin-top:5px" class="cell right label one_second">Лоджия</div>
					<div class="cell auto last_col">
						'.$loggia.'
					</div>
				</div>
				<div class="row">
					<div style="margin-top:5px" class="cell right label one_second">Сан.узел</div>
					<div class="cell auto last_col">
						'.$wc.'
					</div>
				</div>
				<div class="row">
					<div style="margin-top:5px" class="cell right label one_second">Отделка</div>
					<div class="cell auto last_col">
						'.$finish.'
					</div>
				</div>
			</div>
		</div>
		'.$rightBlock.'
	</div>';
?>