<?
/*
*  Коммунальные платежи
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['communal'])){
	if($PageInfoAd['communal']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['communal'].'"';
	}
}
$communal = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[communal]" value="0">
	<input type="hidden"'.$valueParams.' name="s[communal]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span style="display:block;margin-top:2px" class="label">Включая коммунальные платежи</span>
	</div>
</div>';

/*
*  Арендные каникулы
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['rental_holidays'])){
	if($PageInfoAd['rental_holidays']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['rental_holidays'].'"';
	}
}
$rental_holidays = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[rental_holidays]" value="0">
	<input type="hidden"'.$valueParams.' name="s[rental_holidays]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span style="display:block;margin-top:2px" class="label">Арендные каникулы</span>
	</div>
</div>';

/*
*  Агентам не звонить
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['no_agent'])){
	if($PageInfoAd['no_agent']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['no_agent'].'"';
	}
}
$no_agent = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[no_agent]" value="0">
	<input type="hidden"'.$valueParams.' name="s[no_agent]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span style="display:block;margin-top:2px" class="label">Агентам не звонить</span>
	</div>
</div>';

$communalRow = '<div style="width:auto;margin-top:15px;margin-left:130px;" class="cell one_second last_col">
	'.$communal.'
</div>';
if(isset($type_commer) && ($type_commer==5 || $type_commer==6)){
	$communalRow = '';
}

echo '<div id="step-3" class="container_block ads">
	<div class="cost_block">
		<div class="left_block">
			<div class="table_form">
				<h2>Условия сделки</h2>
				<div style="position:relative" class="row">
					<div style="width:105px!important" class="cell label right one_second">
						<label style="margin-top:-1px">Стоимость аренды<b>*</b></label>
					</div>
					<div style="width:235px" class="cell required one_second">
						<input style="width:162px;float:left" type="text" class="text" name="s[price]"'.$price.'>
						<div class="com">руб./мес.</div>
					</div>
					'.$communalRow.'
					
					<div style="width:auto;margin-top:47px;position:absolute;right:6px;" class="cell one_second last_col">
						'.$rental_holidays.'
					</div>
					<div style="width:auto;margin-top:93px;position:absolute;right:10px;" class="cell one_second last_col">
						'.$no_agent.'
					</div>
				</div>
				<div class="row">
					<div style="width:105px!important" class="cell label right one_second">
						<label>Комиссия %</label>
					</div>
					<div style="width:235px" class="cell one_second last_col">
						<input style="width:60px;float:left" type="text" class="text" name="s[prepayment]"'.$prepayment.'>
					</div>
				</div>
			</div>
		</div>
		<div class="right_block">
			<div class="table_form">
				<h2>Телефоны для связи</h2>
				<div class="row">
					<div class="cell info full">
						<p>В объявлении будет указан введённый ранее телефон</p>
					</div>
				</div>
				<div class="row">
					<div class="cell full">
						'.$reg_phone.'
					</div>
				</div>
				<div class="row">
					<div class="cell info full">
						<p>Дополнительно в этом объявлении можно указать ещё один или два номера</p>
					</div>
				</div>
				<div class="row">
					<div style="width:auto;margin:8px 10px 0 0" class="cell code">+7</div>
					<div class="cell code">
						<input maxlength="5" placeholder="код" type="text" class="code phone_code" name="phones[code][1]"'.$codePhone1.'>
					</div>
					<div style="width:204px" class="cell one_second last_col">
						<input type="text" class="phone_type phone_mask" name="phones[number][1]"'.$codeNumber1.'>
					</div>
				</div>
				<div class="row">
					<div style="width:auto;margin:8px 10px 0 0" class="cell code">+7</div>
					<div class="cell code">
						<input maxlength="5" placeholder="код" type="text" class="code phone_code" name="phones[code][2]"'.$codePhone2.'>
					</div>
					<div style="width:204px" class="cell one_second last_col">
						<input type="text" class="phone_type phone_mask" name="phones[number][2]"'.$codeNumber2.'>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="btn_info cost"></div>
</div>';
?>