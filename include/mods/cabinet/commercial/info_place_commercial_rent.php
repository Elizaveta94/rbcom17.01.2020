<?
/*
*  Водоснабжение
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['plumbing'])){
	if($PageInfoAd['plumbing']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['plumbing'].'"';
	}
}
$plumbing = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[plumbing]" value="0">
	<input type="hidden"'.$valueParams.' name="s[plumbing]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Водоснабжение</span>
	</div>
</div>';

/*
*  Канализация
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['sewerage'])){
	if($PageInfoAd['sewerage']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['sewerage'].'"';
	}
}
$sewerage = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[sewerage]" value="0">
	<input type="hidden"'.$valueParams.' name="s[sewerage]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Канализация</span>
	</div>
</div>';

/*
*  Наличие Ж/д тупика
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['deadlock'])){
	if($PageInfoAd['deadlock']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['deadlock'].'"';
	}
}
$deadlock = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[deadlock]" value="0">
	<input type="hidden"'.$valueParams.' name="s[deadlock]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Наличие Ж/д тупика</span>
	</div>
</div>';

/*
*  Банкоматы
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['atms'])){
	if($PageInfoAd['atms']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['atms'].'"';
	}
}
$atms = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[atms]" value="0">
	<input type="hidden"'.$valueParams.' name="s[atms]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Банкоматы</span>
	</div>
</div>';

/*
*  Конференц-залы
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['confer'])){
	if($PageInfoAd['confer']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['confer'].'"';
	}
}
$confer = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[confer]" value="0">
	<input type="hidden"'.$valueParams.' name="s[confer]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Конференц-залы</span>
	</div>
</div>';

/*
*  Кафе
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['cafe'])){
	if($PageInfoAd['cafe']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['cafe'].'"';
	}
}
$cafe = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[cafe]" value="0">
	<input type="hidden"'.$valueParams.' name="s[cafe]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Кафе</span>
	</div>
</div>';

/*
*  Столовая
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['canteen'])){
	if($PageInfoAd['canteen']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['canteen'].'"';
	}
}
$canteen = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[canteen]" value="0">
	<input type="hidden"'.$valueParams.' name="s[canteen]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Столовая</span>
	</div>
</div>';

/*
*  Гостиница
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['hotel'])){
	if($PageInfoAd['hotel']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['hotel'].'"';
	}
}
$hotel = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[hotel]" value="0">
	<input type="hidden"'.$valueParams.' name="s[hotel]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Гостиница</span>
	</div>
</div>';

/*
*  Офисные помещения
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['office_rooms'])){
	if($PageInfoAd['office_rooms']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['office_rooms'].'"';
	}
}
$office_rooms = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[office_rooms]" value="0">
	<input type="hidden"'.$valueParams.' name="s[office_rooms]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Офисные помещения</span>
	</div>
</div>';

/*
*  АЗС
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['azs'])){
	if($PageInfoAd['azs']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['azs'].'"';
	}
}
$azs = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[azs]" value="0">
	<input type="hidden"'.$valueParams.' name="s[azs]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">АЗС</span>
	</div>
</div>';

/*
*  Автомойка
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['wash'])){
	if($PageInfoAd['wash']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['wash'].'"';
	}
}
$wash = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[wash]" value="0">
	<input type="hidden"'.$valueParams.' name="s[wash]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Автомойка</span>
	</div>
</div>';

/*
*  Автосервис
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['autoservice'])){
	if($PageInfoAd['autoservice']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['autoservice'].'"';
	}
}
$autoservice = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[autoservice]" value="0">
	<input type="hidden"'.$valueParams.' name="s[autoservice]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Автосервис</span>
	</div>
</div>';

/*
*  Автостоянка
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['autostop'])){
	if($PageInfoAd['autostop']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['autostop'].'"';
	}
}
$autostop = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[autostop]" value="0">
	<input type="hidden"'.$valueParams.' name="s[autostop]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Автостоянка</span>
	</div>
</div>';

/*
*  Машиноместо
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['car_place'])){
	if($PageInfoAd['car_place']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['car_place'].'"';
	}
}
$car_place = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[car_place]" value="0">
	<input type="hidden"'.$valueParams.' name="s[car_place]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Машиноместо</span>
	</div>
</div>';

/*
*  Теплоснабжение
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['heating'])){
	if($PageInfoAd['heating']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['heating'].'"';
	}
}
$heating = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[heating]" value="0">
	<input type="hidden"'.$valueParams.' name="s[heating]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Теплоснабжение</span>
	</div>
</div>';

/*
*  Газоснабжение
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['gas'])){
	if($PageInfoAd['gas']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['gas'].'"';
	}
}
$gas = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[gas]" value="0">
	<input type="hidden"'.$valueParams.' name="s[gas]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Газоснабжение</span>
	</div>
</div>';

/*
*  Баня
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['sauna'])){
	if($PageInfoAd['sauna']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['sauna'].'"';
	}
}
$sauna = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[sauna]" value="0">
	<input type="hidden"'.$valueParams.' name="s[sauna]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Баня</span>
	</div>
</div>';

/*
*  Гараж
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['garage'])){
	if($PageInfoAd['garage']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['garage'].'"';
	}
}
$garage = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[garage]" value="0">
	<input type="hidden"'.$valueParams.' name="s[garage]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Гараж</span>
	</div>
</div>';

/*
*  Рядом лес
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['forest'])){
	if($PageInfoAd['forest']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['forest'].'"';
	}
}
$forest = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[forest]" value="0">
	<input type="hidden"'.$valueParams.' name="s[forest]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Рядом лес</span>
	</div>
</div>';

/*
*  Водоем
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['water'])){
	if($PageInfoAd['water']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['water'].'"';
	}
}
$water = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[water]" value="0">
	<input type="hidden"'.$valueParams.' name="s[water]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Водоем</span>
	</div>
</div>';

	
/*
*  Право на ЗУ
*/
$rights = '';
$rights2 = '';
$valueInput = '';
for($c=1; $c<count($_RIGHTS); $c++){
	$checked = '';
	if(isset($PageInfoAd['rights']) && $PageInfoAd['rights']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	$rights2 .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_RIGHTS[$c].'</a></li>';
}
$rights = '<div class="checkbox_block">';
$rights .= '<input type="hidden" name="s[rights]"'.$valueInput.'>';
$rights .= '<div class="select_checkbox">';
$rights .= '<ul>';
$rights .= $rights2;
$rights .= '</ul>';
$rights .= '</div>';
$rights .= '</div>';

/*
*  Категория
*/
$category = '<div class="select_block count">';
$categories = '';
$nameParams = 'Не важно';
$valueInput = '';
for($c=0; $c<count($_CATEGORY_COUNTRY); $c++){
	$current = '';
	if(isset($PageInfoAd['category']) && $PageInfoAd['category']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
	}
	$categories .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_CATEGORY_COUNTRY[$c].'</a></li>';
}
$category .= '<input type="hidden" name="s[category]"'.$valueInput.'>';
$category .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$category .= '<div class="scrollbar-inner">';
$category .= '<ul>';
$category .= $categories;
$category .= '</ul>';
$category .= '</div>';
$category .= '</div>';

/*
*  Вид использования
*/
$type_use = '<div class="select_block count">';
$types_use = '';
$nameParams = 'Не важно';
$valueInput = '';
for($c=0; $c<count($_TYPES_USE); $c++){
	$current = '';
	if(isset($PageInfoAd['type_use']) && $PageInfoAd['type_use']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
	}
	$types_use .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPES_USE[$c].'</a></li>';
}
$type_use .= '<input type="hidden" name="s[type_use]"'.$valueInput.'>';
$type_use .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$type_use .= '<div class="scrollbar-inner">';
$type_use .= '<ul>';
$type_use .= $types_use;
$type_use .= '</ul>';
$type_use .= '</div>';
$type_use .= '</div>';

/*
*  Первая линия домов
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['first_line'])){
	if($PageInfoAd['first_line']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['first_line'].'"';
	}
}
$first_line = '<div style="float:left;" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[first_line]" value="0">
	<input type="hidden"'.$valueParams.' name="s[first_line]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Первая линия домов</span>
	</div>
</div>';


/*
*  Интернет
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['internet'])){
	if($PageInfoAd['internet']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['internet'].'"';
	}
}
$internet = '<div style="float:left;margin-right:40px" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[internet]" value="0">
	<input type="hidden"'.$valueParams.' name="s[internet]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Интернет</span>
	</div>
</div>';

/*
*  Телефон
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['phone'])){
	if($PageInfoAd['phone']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['phone'].'"';
	}
}
$phone = '<div style="float:left" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[phone]" value="0">
	<input type="hidden"'.$valueParams.' name="s[phone]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Телефон</span>
	</div>
</div>';

/*
*  Ремонт
*/
$finish = '';
$finishes = '';
$valueInput = '';
for($c=1; $c<count($_REPAIRS); $c++){
	$checked = '';
	if(isset($PageInfoAd['repairs']) && $PageInfoAd['repairs']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	if(empty($PageInfoAd['repairs'])){
		if($c==1){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';				
		}
	}
	$finishes .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_REPAIRS[$c].'</a></li>';
}
$finish = '<div class="checkbox_block">';
$finish .= '<input type="hidden" name="s[repairs]"'.$valueInput.'>';
$finish .= '<div class="select_checkbox">';
$finish .= '<ul>';
$finish .= $finishes;
$finish .= '</ul>';
$finish .= '</div>';
$finish .= '</div>';

/*
*  Офис
*/
if(isset($type_commer) && $type_commer==1){
	require_once("include/mods/cabinet/commercial/info/temp4_commercial_sell.php");
}

/*
*  Торговля, сфера услуг, общепит
*/
if(isset($type_commer) && $type_commer==2){ //($type_commer==4 || $type_commer==6 || $type_commer==8)
	require_once("include/mods/cabinet/commercial/info/temp9_commercial_sell.php");
}

/*
*  АЗС, Мойка, Автосервис, Гараж
*/
if(isset($type_commer) && $type_commer==3){ //($type_commer==7)
	require_once("include/mods/cabinet/commercial/info/temp3_commercial_sell.php");
}

/*
*  ОСЗ, БЦ, ТРК
*/
if(isset($type_commer) && $type_commer==4){ // ($type_commer==10 || $type_commer==11)
	require_once("include/mods/cabinet/commercial/info/temp2_commercial_sell.php");
}

/*
*  Производство, склад
*/
if(isset($type_commer) && $type_commer==5){// ($type_commer==2 || $type_commer==3)
	require_once("include/mods/cabinet/commercial/info/temp5_commercial_sell.php");
}

/*
*  Земельный участок
*/
if(isset($type_commer) && $type_commer==6){ //$type_commer==5
	require_once("include/mods/cabinet/commercial/info/temp1_commercial_sell.php");
}

/*
*  Готовый бизнес
*/
if(isset($type_commer) && $type_commer==7){ //$type_commer==9
	if(!isset($type_business)){
		$type_business = 1;
	}
	require_once("include/mods/cabinet/commercial/info/temp10_commercial_sell.php");
	// if($type_business==15){
		// require_once("include/mods/cabinet/commercial/info/temp7_commercial_sell.php");
	// }
	// else if($type_business==16){
		// require_once("include/mods/cabinet/commercial/info/temp8_commercial_sell.php");
	// }
	// else {
		// require_once("include/mods/cabinet/commercial/info/temp6_commercial_sell.php");
	// }
}

/*
*  Гараж, Машиноместо
*/
if(isset($type_commer) && $type_commer==8){
	require_once("include/mods/cabinet/commercial/info/temp11_commercial_sell.php");
}
?>