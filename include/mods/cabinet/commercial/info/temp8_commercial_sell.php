<?

/*
*  Состояние
*/
$condition = '';
$conditions = '';
$valueInput = '';
for($c=0; $c<count($_CONDITIONS_TRK); $c++){
	$checked = '';
	if(isset($PageInfoAd['conditions']) && $PageInfoAd['conditions']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	$conditions .= '<li'.$checked.'><a style="padding:4px 7px;font-size:14px" href="javascript:void(0)" data-id="'.$c.'">'.$_CONDITIONS_TRK[$c].'</a></li>';
}
$condition = '<div class="checkbox_block">';
$condition .= '<input type="hidden" name="s[conditions]"'.$valueInput.'>';
$condition .= '<div class="select_checkbox">';
$condition .= '<ul>';
$condition .= $conditions;
$condition .= '</ul>';
$condition .= '</div>';
$condition .= '</div>';

/*
*  Право на строение
*/
$rights = '<div class="select_block count">';
$rights2 = '';
$nameParams = 'Не важно';
$valueInput = '';
for($c=0; $c<count($_RIGHTS); $c++){
	$current = '';
	if(isset($PageInfoAd['right_build']) && $PageInfoAd['right_build']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
	}
	$rights2 .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_RIGHTS[$c].'</a></li>';
}
$rights .= '<input type="hidden" name="s[right_build]"'.$valueInput.'>';
$rights .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$rights .= '<div class="scrollbar-inner">';
$rights .= '<ul>';
$rights .= $rights2;
$rights .= '</ul>';
$rights .= '</div>';
$rights .= '</div>';

/*
*  Право на ЗУ
*/
$right_land = '<div class="select_block count">';
$right_land2 = '';
$nameParams = 'Не важно';
$valueInput = '';
for($c=0; $c<count($_RIGHTS); $c++){
	$current = '';
	if(isset($PageInfoAd['rights']) && $PageInfoAd['rights']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
	}
	$right_land2 .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_RIGHTS[$c].'</a></li>';
}
$right_land .= '<input type="hidden" name="s[rights]"'.$valueInput.'>';
$right_land .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$right_land .= '<div class="scrollbar-inner">';
$right_land .= '<ul>';
$right_land .= $right_land2;
$right_land .= '</ul>';
$right_land .= '</div>';
$right_land .= '</div>';

/*
*  Категория ЗУ
*/
$category = '<div class="select_block count">';
$categories = '';
$nameParams = 'Не важно';
$valueInput = '';
for($c=0; $c<count($_CATEGORY_COUNTRY); $c++){
	$current = '';
	if(isset($PageInfoAd['category']) && $PageInfoAd['category']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
	}
	$categories .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_CATEGORY_COUNTRY[$c].'</a></li>';
}
$category .= '<input type="hidden" name="s[category]"'.$valueInput.'>';
$category .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$category .= '<div class="scrollbar-inner">';
$category .= '<ul>';
$category .= $categories;
$category .= '</ul>';
$category .= '</div>';
$category .= '</div>';


/*
*  Высота потолков
*/
$height_ceiling = '<div class="select_block count">';
$height_ceilings = '';
$nameParams = 'Не важно';
$valueInput = '';
$devs = mysql_query("
	SELECT *
	FROM ".$template."_type_house
	WHERE activation='1'
	ORDER BY num
");
if(mysql_num_rows($devs)>0){
	$n = 0;
	while($dev = mysql_fetch_assoc($devs)){
		$current = "";
		
		if($n==0 && !isset($PageInfoAd['ceiling_height'])){
			$height_ceilings .= '<li class="current"><a href="javascript:void(0)" data-id="">Не важно</a></li>';
		}
		if($PageInfoAd['ceiling_height']==$dev['id']){
			$current = ' class="current"';
			$nameParams = $dev['name'];
			$valueInput = ' value="'.$dev['id'].'"';
		}
		$height_ceilings .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$dev['id'].'">'.$dev['name'].'</a></li>';
		$n++;
	}
}
$height_ceiling .= '<input type="hidden" name="s[ceiling_height]"'.$valueInput.'>';
$height_ceiling .= '<div style="width:145px" class="choosed_block">'.$nameParams.'</div>';
$height_ceiling .= '<div class="scrollbar-inner">';
$height_ceiling .= '<ul>';
$height_ceiling .= $height_ceilings;
$height_ceiling .= '</ul>';
$height_ceiling .= '</div>';
$height_ceiling .= '</div>';

$rightBlock = '<div style="width:488px" class="right_block">
	<div class="table_form">
		<h2>Необходимые помещения для ведения бизнеса</h2>
		<div class="line">
			<div style="width:100%" class="block">
				<div class="row">
					<div style="width:148px!important;margin-right:15px" class="cell label right one_second">
						<label>Общая площадь<b>*</b></label>
					</div>
					<div style="width:180px" class="cell required one_second last_col">
						<input style="width:145px" type="text" class="text" name="s[full_square]">
						<div class="com">м<sup>2</sup></div>
					</div>
				</div>
				<div class="row">
					<div style="width:148px!important" class="cell label right one_second">
						<label style="margin-top:5px">Высота потолков</label>
					</div>
					<div style="width:160px" class="cell one_second last_col">
						'.$height_ceiling.'
					</div>
				</div>
				<div class="row">
					<div style="width:148px" class="cell label right one_second">
						<label>Электропотребление</label>
					</div>
					<div style="width:125px" class="cell one_second last_col">
						<input style="width:85px" type="text" class="text" name="s[power_max]">
						<div style="margin-top:8px" class="com">кВт</div>
					</div>
				</div>
				<div class="row">
					<div style="margin-right:15px;width:148px!important" class="cell label right one_second">
						<label style="margin-top:-2px">Обязательные<br> требования к<br> помещениям</label>
					</div>
					<div style="width:130px" class="cell one_second last_col">
						<textarea style="width:330px;height:140px" name="s[requirements]"></textarea>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>';

echo '<div class="params_block">
	<div style="width:567px" class="left_block">
		<div class="table_form">
			<h2>Информация о проекте</h2>
			<div class="line">
				<div style="width:100%;float:none" class="block">
					<div class="row">
						<div style="margin-right:15px;width:132px!important" class="cell label right one_second">
							<label>Сфера</label>
						</div>
						<div class="cell one_second last_col">
							<input style="width:377px" type="text" class="text" name="s[business_area]">
						</div>
					</div>
					<div class="row">
						<div style="margin-right:15px;width:132px!important" class="cell label right one_second">
							<label style="margin-top:-2px">Наименование<br> продукта<br><i style="color:#4c606f;font-style:normal;font-size:13px">(тип сервис-продукта)</i></label>
						</div>
						<div style="width:130px" class="cell one_second last_col">
							<textarea style="width:377px;height:100px" name="s[name_product]"></textarea>
						</div>
					</div>
					<div class="row">
						<div style="margin-right:15px;width:132px!important" class="cell label right one_second">
							<label style="margin-top:-2px">Страна<br> происхождение<br> бренда</label>
						</div>
						<div style="width:130px" class="cell one_second last_col">
							<input style="width:137px" type="text" class="text" name="s[country_brand]">
						</div>
					</div>
					<div class="row">
						<div style="margin-right:15px;width:132px!important" class="cell label right one_second">
							<label style="margin-top:-2px">Год создания<br> бренда</label>
						</div>
						<div style="width:130px" class="cell one_second last_col">
							<input style="width:137px" type="text" class="text" name="s[year]">
						</div>
					</div>
					<div class="row">
						<div style="margin-right:15px;width:132px!important" class="cell label right one_second">
							<label style="margin-top:-2px">Кол-во<br> франчайзинговых<br> предприятий</label>
						</div>
						<div style="width:130px" class="cell one_second last_col">
							<input style="width:137px" type="text" class="text" name="s[franch]">
						</div>
					</div>
				</div>
			</div>
			<h2 style="color:#4c606f;clear:both;margin-top:18px;margin-bottom:0">Экономические показатели</h2>
			<div class="line">
				<div class="block">
					<div class="row">
						<div style="margin-right:15px;width:132px!important" class="cell label right one_second">
							<label style="margin-top:-2px">Инвестиции от,<br> <div class="rub">Р</div></label>
						</div>
						<div style="width:130px" class="cell one_second last_col">
							<input style="width:137px" type="text" class="text" name="s[investments]">
						</div>
					</div>
					<div class="row">
						<div style="margin-right:15px;width:132px!important" class="cell label right one_second">
							<label style="margin-top:-2px">Кол-во персонала<br> чел</label>
						</div>
						<div style="width:130px" class="cell one_second last_col">
							<input style="width:137px" type="text" class="text" name="s[staffs]">
						</div>
					</div>
				</div>
				<div class="block">
					<div class="row">
						<div style="margin-right:15px;width:132px!important" class="cell label right one_second">
							<label style="margin-top:-2px">Срок<br> окупаемости</label>
						</div>
						<div style="width:130px" class="cell one_second last_col">
							<input style="width:137px" type="text" class="text" name="s[payback]">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	'.$rightBlock.'
</div>';
?>