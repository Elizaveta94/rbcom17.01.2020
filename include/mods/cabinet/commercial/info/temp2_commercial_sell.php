<?

/*
*  Состояние
*/
$conditionTRK = '';
$conditionsTRK = '';
$valueInput = '';
for($c=0; $c<count($_CONDITIONS_TRK); $c++){
	$checked = '';
	if(isset($PageInfoAd['conditions']) && $PageInfoAd['conditions']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	$conditionsTRK .= '<li'.$checked.'><a style="padding:4px 7px;font-size:14px" href="javascript:void(0)" data-id="'.$c.'">'.$_CONDITIONS_TRK[$c].'</a></li>';
}
$conditionTRK = '<div class="checkbox_block">';
$conditionTRK .= '<input type="hidden" name="s[conditions]"'.$valueInput.'>';
$conditionTRK .= '<div class="select_checkbox">';
$conditionTRK .= '<ul>';
$conditionTRK .= $conditionsTRK;
$conditionTRK .= '</ul>';
$conditionTRK .= '</div>';
$conditionTRK .= '</div>';

/*
*  Право на ЗУ
*/
$rights = '';
$rights2 = '';
$valueInput = '';
for($c=1; $c<count($_RIGHTS); $c++){
	$checked = '';
	if(isset($PageInfoAd['rights']) && $PageInfoAd['rights']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	$rights2 .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_RIGHTS[$c].'</a></li>';
}
$rights = '<div class="checkbox_block">';
$rights .= '<input type="hidden" name="s[rights]"'.$valueInput.'>';
$rights .= '<div class="select_checkbox">';
$rights .= '<ul>';
$rights .= $rights2;
$rights .= '</ul>';
$rights .= '</div>';
$rights .= '</div>';


/*
*  Категория ЗУ
*/
$category = '<div class="select_block count">';
$categories = '';
$nameParams = 'Не важно';
$valueInput = '';
for($c=0; $c<count($_CATEGORY_COUNTRY); $c++){
	$current = '';
	if(isset($PageInfoAd['category']) && $PageInfoAd['category']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
	}
	$categories .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_CATEGORY_COUNTRY[$c].'</a></li>';
}
$category .= '<input type="hidden" name="s[category]"'.$valueInput.'>';
$category .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$category .= '<div class="scrollbar-inner">';
$category .= '<ul>';
$category .= $categories;
$category .= '</ul>';
$category .= '</div>';
$category .= '</div>';

$rightBlock = '<div class="right_block">
	<div class="table_form">
		<h2>Информация о земельном участке</h2>
		<div class="line">
			<div style="width:100%" class="block">
				<div class="row">
					<div style="width:130px" class="cell label right one_second">
						<label style="margin-top:0">Площадь<br> зем. участка</label>
					</div>
					<div style="width:185px" class="cell one_second last_col">
						<input style="width:120px" type="text" class="text" name="s[area_square]"'.$area_square.'>
						<div class="com">м<sup>2</sup></div>
					</div>
				</div>
				<div class="row">
					<div style="width:130px!important;margin-right:15px" class="cell label right one_second">
						<label style="white-space:nowrap;margin-top:-2px">Кадастровый<br> номер</label>
					</div>
					<div class="cell one_second last_col">
						<input style="width:230px" type="text" class="text" name="s[cadastr]"'.$cadastr.'>
					</div>
				</div>
				<!--<div class="row">
					<div style="width:132px!important;margin-right:15px" class="cell label right one_second">
						<label>Категория ЗУ</label>
					</div>
					<div class="cell one_second last_col">
						'.$category.'
					</div>
				</div>
				<div class="row">
					<div style="width:130px!important;margin-right:15px" class="cell label right one_second">
						<label style="white-space:nowrap;margin-top:-2px">Разрешенное<br> использование<br> (из ПЗЗ)</label>
					</div>
					<div class="cell one_second last_col">
						<input style="width:230px" type="text" class="text" name="s[pzz]"'.$pzz.'>
					</div>
				</div>-->
				<div class="row">
					<div style="width:132px!important;margin-right:15px" class="cell right label one_second">
						<label style="margin-top:5px">Право на ЗУ</label>
					</div>
					<div style="width:310px" class="cell auto last_col">
						'.$rights.'
					</div>
				</div>
				<div class="row">
					<div style="width:130px" class="cell label right one_second">
						<label style="margin-top:7px">Электроснабжение</label>
					</div>
					<div style="width:125px" class="cell one_second last_col">
						<input style="width:85px" type="text" class="text" name="s[power_max]"'.$power_max.'>
						<div style="margin-top:8px" class="com">кВт</div>
					</div>
				</div>
			</div>
		</div>
		<div style="margin-top:5px" class="line">
			<div style="width:246px;margin-left:145px" class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$plumbing.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$sewerage.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$deadlock.'
					</div>
				</div>
			</div>
			<div class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$heating.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$gas.'
					</div>
				</div>
			</div>
		</div>
	</div>
</div>';

	
/*
*  Тип здания
*/
$typeBuildings = '';
$typeBuildings2 = '';
$valueInput = '';
for($c=0; $c<count($_TYPE_BUILD_TRK); $c++){
	$checked = '';
	if(isset($PageInfoAd['type_build']) && $PageInfoAd['type_build']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}	
	if(empty($PageInfoAd['type_build'])){
		if($c==0){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';				
		}
	}
	$typeBuildings2 .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_BUILD_TRK[$c].'</a></li>';
}
$typeBuildings = '<div class="checkbox_block">';
$typeBuildings .= '<input type="hidden" name="s[type_build]"'.$valueInput.'>';
$typeBuildings .= '<div class="select_checkbox">';
$typeBuildings .= '<ul>';
$typeBuildings .= $typeBuildings2;
$typeBuildings .= '</ul>';
$typeBuildings .= '</div>';
$typeBuildings .= '</div>';

/*
*  Тип здания
*/
$typeBuildings = '<div class="select_block count">';
$typeBuildings2 = '';
$nameParams = 'Не выбран';
$valueInput = '';
for($c=0; $c<count($_TYPE_BUILD_FRESH); $c++){
	$current = "";
	// if($c==0 && !isset($PageInfoAd['type_build'])){
		// $typeBuildings2 .= '<li class="current"><a href="javascript:void(0)" data-id="">Не выбран</a></li>';
	// }
	if(isset($PageInfoAd['type_build']) && $PageInfoAd['type_build']==$c){
		$current = ' class="current"';
		$nameParams = $_TYPE_BUILD_FRESH[$c];
		$valueInput = ' value="'.$c.'"';
	}
	$typeBuildings2 .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_BUILD_FRESH[$c].'</a></li>';
}
$typeBuildings .= '<input type="hidden" name="s[type_build]"'.$valueInput.'>';
$typeBuildings .= '<div style="width:162px" class="choosed_block">'.$nameParams.'</div>';
$typeBuildings .= '<div class="scrollbar-inner">';
$typeBuildings .= '<ul>';
$typeBuildings .= $typeBuildings2;
$typeBuildings .= '</ul>';
$typeBuildings .= '</div>';
$typeBuildings .= '</div>';
	
/*
*  Класс объекта
*/
$classObject = '';
$classObjects = '';
$valueInput = '';
for($c=1; $c<=count($_CLASS_OBJECTS); $c++){
	$checked = '';
	if(isset($PageInfoAd['class_object']) && $PageInfoAd['class_object']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}	
	if(empty($PageInfoAd['class_object'])){
		if($c==0){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';				
		}
	}
	$classObjects .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_CLASS_OBJECTS[$c].'</a></li>';
}
$classObject = '<div class="checkbox_block">';
$classObject .= '<input type="hidden" name="s[class_object]"'.$valueInput.'>';
$classObject .= '<div class="select_checkbox">';
$classObject .= '<ul>';
$classObject .= $classObjects;
$classObject .= '</ul>';
$classObject .= '</div>';
$classObject .= '</div>';


/*
*  Состояние
*/
$condition_select = '<div class="select_block count">';
$condition_selects = '';
$nameParams = 'Не выбран';
$valueInput = '';
for($c=0; $c<count($_CONDITIONS_FRESH); $c++){
	$current = '';
	if(isset($PageInfoAd['conditions']) && $PageInfoAd['conditions']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
	}
	$condition_selects .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_CONDITIONS_FRESH[$c].'</a></li>';
}
$condition_select .= '<input type="hidden" name="s[conditions]"'.$valueInput.'>';
$condition_select .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$condition_select .= '<div class="scrollbar-inner">';
$condition_select .= '<ul>';
$condition_select .= $condition_selects;
$condition_select .= '</ul>';
$condition_select .= '</div>';
$condition_select .= '</div>';

echo '<div class="params_block">
	<div style="border-right:1px solid #b5babe;width:505px" class="left_block">
		<div class="table_form">
			<h2>Информация об объекте</h2>
			<div class="line">
				<div style="width:100%;" class="block">
					<div class="row">
						<div style="width:132px!important;margin-right:15px" class="cell label right one_second">
							<label style="white-space:nowrap;margin-left:-3px;margin-top:-2px">Кадастровый<br> номер</label>
						</div>
						<div class="cell one_second last_col">
							<input style="width:230px" type="text" class="text" name="s[cadastr]"'.$cadastr.'>
						</div>
					</div>
					<div class="row commercial flat">
						<div style="width:132px!important;margin-right:15px;" class="cell label right one_second">
							<label>Название комплекса</label>
						</div>
						<div class="cell one_second last_col">
							<div class="request_block">
								<input style="width:290px" autocomplete="off" type="text" class="text" name="s[name_build]" value="'.$PageInfoAd['name_build'].'">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="line">
				<div style="width:310px" class="block">
					<div class="row">
						<div style="width:132px!important;margin-top:7px;margin-right:15px;" class="cell right label one_second">
							<label style="margin-top:-2px">Тип здания<b>*</b></label>
						</div>
						<div style="width:160px" class="cell one_second last_col required">
							'.$typeBuildings.'
						</div>
					</div>
				</div>
				<div style="width:200px" class="block">
					<div style="margin-top:-2px;margin-left:5px;" class="row">
						<div style="margin-right:15px;width:105px!important" class="cell label right one_second">
							<label style="margin-top:7px;width:105px!important">Год постройки</label>
						</div>
						<div style="width:70px" class="cell one_second last_col">
							<input style="width:62px" type="text" class="text" name="s[year]"'.$year.'>
						</div>
					</div>
				</div>
			</div>
			<div class="line">
				<div style="width:100%;" class="block">
					<div class="row">
						<div style="width:160px;margin-left:147px;margin-top:-12px" class="cell one_second last_col">
							'.$first_line.'
						</div>
					</div>
					<div class="row">
						<div style="width:132px!important;margin-right:15px" class="cell right label one_second">
							<label>Класс объекта</label>
						</div>
						<div style="width:310px" class="cell one_second last_col">
							'.$classObject.'
						</div>
					</div>
					<div class="row">
						<div style="width:132px!important;margin-right:15px" class="cell label right one_second">
							<label>Общая площадь<b>*</b></label>
						</div>
						<div class="cell required one_second last_col">
							<input style="width:137px" type="text" class="text" name="s[full_square]"'.$full_square.'>
							<div class="com">м<sup>2</sup></div>
						</div>
					</div>
					<div class="row">
						<div style="width:132px!important;margin-right:15px" class="cell label right one_second">
							<label>Этажей в здании</label>
						</div>
						<div class="cell one_second last_col">
							<input style="width:92px" type="text" class="text" name="s[floors]"'.$floors.'>
						</div>
					</div>
					<div class="row">
						<div style="width:132px!important;margin-right:15px" class="cell right label one_second">
							<label style="margin-top:5px">Состояние</label>
						</div>
						<div style="width:370px" class="cell auto last_col">
							'.$condition_select.'
						</div>
					</div>
					<div class="row">
						<div style="width:132px!important;margin-right:15px" class="cell label right one_second">
							<label>Мест на парковке</label>
						</div>
						<div class="cell one_second last_col">
							<input style="width:92px" type="text" class="text" name="s[places]"'.$places.'>
						</div>
					</div>
				</div>
			</div>										
		</div>
	</div>
	'.$rightBlock.'
</div>';
?>