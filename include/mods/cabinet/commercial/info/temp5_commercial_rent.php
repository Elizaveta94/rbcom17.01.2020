<?

/*
*  Состояние
*/
$condition = '';
$conditions = '';
$valueInput = '';
for($c=0; $c<count($_CONDITIONS_TRK); $c++){
	$checked = '';
	if(isset($PageInfoAd['conditions']) && $PageInfoAd['conditions']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	$conditions .= '<li'.$checked.'><a style="padding:4px 7px;font-size:14px" href="javascript:void(0)" data-id="'.$c.'">'.$_CONDITIONS_TRK[$c].'</a></li>';
}
$condition = '<div class="checkbox_block">';
$condition .= '<input type="hidden" name="s[conditions]"'.$valueInput.'>';
$condition .= '<div class="select_checkbox">';
$condition .= '<ul>';
$condition .= $conditions;
$condition .= '</ul>';
$condition .= '</div>';
$condition .= '</div>';

/*
*  Право на ЗУ
*/
$rights = '';
$rights2 = '';
$valueInput = '';
for($c=1; $c<count($_RIGHTS); $c++){
	$checked = '';
	if(isset($PageInfoAd['rights']) && $PageInfoAd['rights']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	$rights2 .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_RIGHTS[$c].'</a></li>';
}
$rights = '<div class="checkbox_block">';
$rights .= '<input type="hidden" name="s[rights]"'.$valueInput.'>';
$rights .= '<div class="select_checkbox">';
$rights .= '<ul>';
$rights .= $rights2;
$rights .= '</ul>';
$rights .= '</div>';
$rights .= '</div>';


/*
*  Категория ЗУ
*/
$category = '<div class="select_block count">';
$categories = '';
$nameParams = 'Не важно';
$valueInput = '';
for($c=0; $c<count($_CATEGORY_COUNTRY); $c++){
	$current = '';
	if(isset($PageInfoAd['category']) && $PageInfoAd['category']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
	}
	$categories .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_CATEGORY_COUNTRY[$c].'</a></li>';
}
$category .= '<input type="hidden" name="s[category]"'.$valueInput.'>';
$category .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$category .= '<div class="scrollbar-inner">';
$category .= '<ul>';
$category .= $categories;
$category .= '</ul>';
$category .= '</div>';
$category .= '</div>';

$rightBlock = '<div style="border-left:1px solid #b5babe;width:490px" class="right_block">
	<div class="table_form">
		<h2>Информация о земельном участке</h2>
		<div class="line">
			<div style="width:100%" class="block">
				<div class="row">
					<div style="width:130px!important;margin-right:15px" class="cell label right one_second">
						<label style="white-space:nowrap;margin-top:-2px">Кадастровый<br> номер</label>
					</div>
					<div class="cell one_second last_col">
						<input style="width:230px" type="text" class="text" name="s[cadastr]">
					</div>
				</div>
				<div class="row">
					<div style="width:130px" class="cell label right one_second">
						<label style="margin-top:0">Площадь<br> зем. участка</label>
					</div>
					<div style="width:185px" class="cell one_second last_col">
						<input style="width:120px" type="text" class="text" name="s[area_square]">
						<div class="com">м<sup>2</sup></div>
					</div>
				</div>
				<div class="row">
					<div style="width:132px!important;margin-right:15px" class="cell label right one_second">
						<label>Категория ЗУ</label>
					</div>
					<div class="cell one_second last_col">
						'.$category.'
					</div>
				</div>
				<div class="row">
					<div style="width:130px!important;margin-right:15px" class="cell label right one_second">
						<label style="white-space:nowrap;margin-top:-2px">Разрешенное<br> использование<br> (из ПЗЗ)</label>
					</div>
					<div class="cell one_second last_col">
						<input style="width:230px" type="text" class="text" name="s[pzz]">
					</div>
				</div>
				<div class="row">
					<div style="width:132px!important;margin-right:15px" class="cell right label one_second">
						<label style="margin-top:5px">Право на ЗУ</label>
					</div>
					<div style="width:310px" class="cell auto last_col">
						'.$rights.'
					</div>
				</div>
				<div class="row">
					<div style="width:130px" class="cell label right one_second">
						<label style="margin-top:0">Максимальная<br> мощность</label>
					</div>
					<div style="width:125px" class="cell one_second last_col">
						<input style="width:85px" type="text" class="text" name="s[power_max]">
						<div style="margin-top:8px" class="com">кВт</div>
					</div>
				</div>
			</div>
		</div>
		<div style="margin-top:5px" class="line">
			<div style="width:200px;margin-left:145px" class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$plumbing.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$sewerage.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$deadlock.'
					</div>
				</div>
			</div>
			<div class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$heating.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$gas.'
					</div>
				</div>
			</div>
		</div>
	</div>
</div>';
	
/*
*  Класс объекта
*/
$classObject = '';
$classObjects = '';
$valueInput = '';
for($c=0; $c<count($_CLASS_OBJECTS); $c++){
	$checked = '';
	if(isset($PageInfoAd['class_object']) && $PageInfoAd['class_object']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}	
	if(empty($PageInfoAd['class_object'])){
		if($c==0){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';				
		}
	}
	$classObjects .= '<li'.$checked.'><a style="padding:4px 7px;font-size:14px" href="javascript:void(0)" data-id="'.$c.'">'.$_CLASS_OBJECTS[$c].'</a></li>';
}
$classObject = '<div class="checkbox_block">';
$classObject .= '<input type="hidden" name="s[class_object]"'.$valueInput.'>';
$classObject .= '<div class="select_checkbox">';
$classObject .= '<ul>';
$classObject .= $classObjects;
$classObject .= '</ul>';
$classObject .= '</div>';
$classObject .= '</div>';

	
/*
*  Тип здания
*/
$typeBuildings = '';
$typeBuildings2 = '';
$valueInput = '';
for($c=0; $c<count($_TYPE_BUILD_TRK); $c++){
	$checked = '';
	if(isset($PageInfoAd['type_build']) && $PageInfoAd['type_build']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}	
	if(empty($PageInfoAd['type_build'])){
		if($c==0){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';				
		}
	}
	$typeBuildings2 .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_BUILD_TRK[$c].'</a></li>';
}
$typeBuildings = '<div class="checkbox_block">';
$typeBuildings .= '<input type="hidden" name="s[type_build]"'.$valueInput.'>';
$typeBuildings .= '<div class="select_checkbox">';
$typeBuildings .= '<ul>';
$typeBuildings .= $typeBuildings2;
$typeBuildings .= '</ul>';
$typeBuildings .= '</div>';
$typeBuildings .= '</div>';

/*
*  Тип здания
*/
$typeBuildings = '<div class="select_block count">';
$typeBuildings2 = '';
$nameParams = 'Не выбран';
$valueInput = '';
for($c=0; $c<count($_TYPE_BUILD_TRK); $c++){
	$current = "";
	if($c==0 && !isset($PageInfoAd['type_build'])){
		$typeBuildings2 .= '<li class="current"><a href="javascript:void(0)" data-id="">Не выбран</a></li>';
	}
	if(isset($PageInfoAd['type_build']) && $PageInfoAd['type_build']==$с){
		$current = ' class="current"';
		$nameParams = $_TYPE_BUILD_TRK[$c];
		$valueInput = ' value="'.$с.'"';
	}
	$typeBuildings2 .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$с.'">'.$_TYPE_BUILD_TRK[$c].'</a></li>';
}
$typeBuildings .= '<input type="hidden" name="s[type_build]"'.$valueInput.'>';
$typeBuildings .= '<div style="width:162px" class="choosed_block">'.$nameParams.'</div>';
$typeBuildings .= '<div class="scrollbar-inner">';
$typeBuildings .= '<ul>';
$typeBuildings .= $typeBuildings2;
$typeBuildings .= '</ul>';
$typeBuildings .= '</div>';
$typeBuildings .= '</div>';

/*
*  Высота потолков
*/
$height_ceiling = '<div class="select_block count">';
$height_ceilings = '';
$nameParams = 'Не важно';
$valueInput = '';
$devs = mysql_query("
	SELECT *
	FROM ".$template."_type_house
	WHERE activation='1'
	ORDER BY num
");
if(mysql_num_rows($devs)>0){
	$n = 0;
	while($dev = mysql_fetch_assoc($devs)){
		$current = "";
		
		if($n==0 && !isset($PageInfoAd['ceiling_height'])){
			$height_ceilings .= '<li class="current"><a href="javascript:void(0)" data-id="">Не важно</a></li>';
		}
		if($PageInfoAd['ceiling_height']==$dev['id']){
			$current = ' class="current"';
			$nameParams = $dev['name'];
			$valueInput = ' value="'.$dev['id'].'"';
		}
		$height_ceilings .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$dev['id'].'">'.$dev['name'].'</a></li>';
		$n++;
	}
}
$height_ceiling .= '<input type="hidden" name="s[ceiling_height]"'.$valueInput.'>';
$height_ceiling .= '<div style="width:162px" class="choosed_block">'.$nameParams.'</div>';
$height_ceiling .= '<div class="scrollbar-inner">';
$height_ceiling .= '<ul>';
$height_ceiling .= $height_ceilings;
$height_ceiling .= '</ul>';
$height_ceiling .= '</div>';
$height_ceiling .= '</div>';

/*
*  Шаг колонн
*/
$column_space = '<div class="select_block count">';
$column_spaces = '';
$nameParams = 'Не важно';
$valueInput = '';
$devs = mysql_query("
	SELECT *
	FROM ".$template."_type_house
	WHERE activation='1'
	ORDER BY num
");
if(mysql_num_rows($devs)>0){
	$n = 0;
	while($dev = mysql_fetch_assoc($devs)){
		$current = "";
		
		if($n==0 && !isset($PageInfoAd['column_space'])){
			$column_spaces .= '<li class="current"><a href="javascript:void(0)" data-id="">Не важно</a></li>';
		}
		if($PageInfoAd['column_space']==$dev['id']){
			$current = ' class="current"';
			$nameParams = $dev['name'];
			$valueInput = ' value="'.$dev['id'].'"';
		}
		$column_spaces .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$dev['id'].'">'.$dev['name'].'</a></li>';
		$n++;
	}
}
$column_space .= '<input type="hidden" name="s[column_space]"'.$valueInput.'>';
$column_space .= '<div style="width:137px" class="choosed_block">'.$nameParams.'</div>';
$column_space .= '<div class="scrollbar-inner">';
$column_space .= '<ul>';
$column_space .= $column_spaces;
$column_space .= '</ul>';
$column_space .= '</div>';
$column_space .= '</div>';

/*
*  Количество ворот
*/
$gates = '<div class="select_block count">';
$gates2 = '';
$nameParams = 'Не важно';
$valueInput = '';
$devs = mysql_query("
	SELECT *
	FROM ".$template."_type_house
	WHERE activation='1'
	ORDER BY num
");
if(mysql_num_rows($devs)>0){
	$n = 0;
	while($dev = mysql_fetch_assoc($devs)){
		$current = "";
		
		if($n==0 && !isset($PageInfoAd['gates'])){
			$gates2 .= '<li class="current"><a href="javascript:void(0)" data-id="">Не важно</a></li>';
		}
		if($PageInfoAd['gates']==$dev['id']){
			$current = ' class="current"';
			$nameParams = $dev['name'];
			$valueInput = ' value="'.$dev['id'].'"';
		}
		$gates2 .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$dev['id'].'">'.$dev['name'].'</a></li>';
		$n++;
	}
}
$gates .= '<input type="hidden" name="s[gates]"'.$valueInput.'>';
$gates .= '<div style="width:137px" class="choosed_block">'.$nameParams.'</div>';
$gates .= '<div class="scrollbar-inner">';
$gates .= '<ul>';
$gates .= $gates2;
$gates .= '</ul>';
$gates .= '</div>';
$gates .= '</div>';

/*
*  Кран-балка
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['balka'])){
	if($PageInfoAd['balka']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['balka'].'"';
	}
}
$balka = '<div style="float:left;margin-right:40px" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[balka]" value="0">
	<input type="hidden"'.$valueParams.' name="s[balka]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Кран-балка</span>
	</div>
</div>';

/*
*  Наличие пандуса
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['rampant'])){
	if($PageInfoAd['rampant']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['rampant'].'"';
	}
}
$rampant = '<div style="float:left;margin-right:40px" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[rampant]" value="0">
	<input type="hidden"'.$valueParams.' name="s[rampant]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Наличие пандуса</span>
	</div>
</div>';


echo '<div class="params_block">
	<div style="border-right:none;width:565px" class="left_block">
		<div class="table_form">
			<h2>Информация об объекте</h2>
			<div class="line">
				<div style="width:315px" class="block">
					<div class="row">
						<div style="width:92px!important" class="cell label right one_second">
							<label style="white-space:nowrap;margin-left:-3px;margin-top:-2px">Кадастровый<br> номер</label>
						</div>
						<div style="width:160px" class="cell one_second last_col">
							<input type="text" class="text" name="s[cadastr]">
						</div>
					</div>
					<div class="row">
						<div style="width:92px!important" class="cell right label one_second">
							<label style="margin-top:-2px">Тип здания</label>
						</div>
						<div style="width:160px" class="cell one_second last_col">
							'.$typeBuildings.'
						</div>
					</div>
					<div class="row">
						<div style="width:92px!important" class="cell label right one_second">
							<label style="margin-top:-2px">Общая площадь<b>*</b></label>
						</div>
						<div style="width:190px" class="cell required one_second last_col">
							<input type="text" class="text" name="s[full_square]">
							<div class="com">м<sup>2</sup></div>
						</div>
					</div>
					<div class="row">
						<div style="width:92px!important" class="cell label right one_second">
							<label>Этаж<b>*</b></label>
						</div>
						<div style="width:160px" class="cell required one_second last_col">
							<input type="text" class="text" name="s[floor]">
						</div>
					</div>
					<div class="row">
						<div style="width:92px!important" class="cell label right one_second">
							<label style="margin-top:-2px">Лифты/<br> подъемники</label>
						</div>
						<div style="width:160px" class="cell one_second last_col">
							<input type="text" class="text" name="s[lifts]">
						</div>
					</div>
					<div class="row">
						<div style="width:92px!important" class="cell label right one_second">
							<label style="margin-top:-2px">Высота потолков</label>
						</div>
						<div style="width:160px" class="cell one_second last_col">
							'.$height_ceiling.'
						</div>
					</div>
				</div>
				<div style="width:235px;margin-left:20px" class="block">
					<div class="row">
						<div style="width:72px!important;margin-right:15px" class="cell right label one_second">
							<label style="margin-top:0px">Класс объекта</label>
						</div>
						<div style="width:145px" class="cell one_second last_col">
							'.$classObject.'
						</div>
					</div>
					<div class="row">
						<div style="margin-right:15px;width:72px!important" class="cell label right one_second">
							<label style="margin-top:-2px">Год<br> постройки</label>
						</div>
						<div style="width:130px" class="cell one_second last_col">
							<input style="width:122px" type="text" class="text" name="s[year]">
						</div>
					</div>
					<div class="row">
						<div style="margin-right:15px;width:72px!important" class="cell label right one_second">
							<label style="margin-top:5px;white-space:nowrap">Шаг колонн</label>
						</div>
						<div style="width:135px" class="cell one_second last_col">
							'.$column_space.'
						</div>
					</div>
					<div class="row">
						<div style="margin-right:15px;width:72px!important" class="cell label right one_second">
							<label style="margin-top:-2px">Этажей<br> в здании</label>
						</div>
						<div style="width:130px" class="cell one_second last_col">
							<input style="width:122px" type="text" class="text" name="s[floors]">
						</div>
					</div>
					<div class="row">
						<div style="margin-right:15px;width:72px!important" class="cell label right one_second">
							<label style="margin-top:-2px">Количество<br> ворот</label>
						</div>
						<div style="width:135px" class="cell one_second last_col">
							'.$gates.'
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div style="width:92px!important" class="cell right label one_second">
					<label style="margin-top:5px">Состояние</label>
				</div>
				<div style="width:370px" class="cell auto last_col">
					'.$condition.'
				</div>
			</div>
			<div class="row">
				<div style="margin-top:5px;width:92px!important" class="cell right label one_second">&nbsp;</div>
				<div class="cell auto last_col">
					'.$balka.'
					'.$rampant.'
				</div>
			</div>
		</div>
	</div>
	'.$rightBlock.'
</div>';
?>