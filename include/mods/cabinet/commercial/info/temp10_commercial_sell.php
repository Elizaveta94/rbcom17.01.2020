<?

/*
*  Состояние
*/
$condition = '';
$conditions = '';
$valueInput = '';
for($c=0; $c<count($_CONDITIONS_TRK); $c++){
	$checked = '';
	if(isset($PageInfoAd['conditions']) && $PageInfoAd['conditions']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
		$nameParams = $_CONDITIONS_TRK[$c];
	}
	$conditions .= '<li'.$checked.'><a style="padding:4px 7px;font-size:14px" href="javascript:void(0)" data-id="'.$c.'">'.$_CONDITIONS_TRK[$c].'</a></li>';
}
$condition = '<div class="checkbox_block">';
$condition .= '<input type="hidden" name="s[conditions]"'.$valueInput.'>';
$condition .= '<div class="select_checkbox">';
$condition .= '<ul>';
$condition .= $conditions;
$condition .= '</ul>';
$condition .= '</div>';
$condition .= '</div>';

/*
*  Тип бизнеса
*/
$business = '<div style="width:auto" class="select_block count">';
$business2 = '';
$nameParams = 'Не выбран';
$valueInput = '';
for($c=0; $c<count($_READY_BUSINESS); $c++){
	$current = '';
	if(isset($PageInfoAd['type_business']) && $PageInfoAd['type_business']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
		$nameParams = $_READY_BUSINESS[$c];
	}
	$business2 .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_READY_BUSINESS[$c].'</a></li>';
}
$business .= '<input type="hidden" name="s[type_business]"'.$valueInput.'>';
$business .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$business .= '<div class="scrollbar-inner">';
$business .= '<ul>';
$business .= $business2;
$business .= '</ul>';
$business .= '</div>';
$business .= '</div>';

/*
*  Право на помещение
*/
$rights = '<div class="select_block count">';
$rights2 = '';
$nameParams = 'Не важно';
$valueInput = '';
for($c=0; $c<count($_RIGHTS); $c++){
	$current = '';
	if(isset($PageInfoAd['right_build']) && $PageInfoAd['right_build']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
		$nameParams = $_RIGHTS[$c];
	}
	$rights2 .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_RIGHTS[$c].'</a></li>';
}
$rights .= '<input type="hidden" name="s[right_build]"'.$valueInput.'>';
$rights .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$rights .= '<div class="scrollbar-inner">';
$rights .= '<ul>';
$rights .= $rights2;
$rights .= '</ul>';
$rights .= '</div>';
$rights .= '</div>';

/*
*  Право на ЗУ
*/
$right_land = '';
$right_land2 = '';
$valueInput = '';
for($c=1; $c<count($_RIGHTS); $c++){
	$checked = '';
	if(isset($PageInfoAd['right_land']) && $PageInfoAd['right_land']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
		$nameParams = $_RIGHTS[$c];
	}
	$right_land2 .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_RIGHTS[$c].'</a></li>';
}
$right_land = '<div class="checkbox_block">';
$right_land .= '<input type="hidden" name="s[rights]"'.$valueInput.'>';
$right_land .= '<div class="select_checkbox">';
$right_land .= '<ul>';
$right_land .= $right_land2;
$right_land .= '</ul>';
$right_land .= '</div>';
$right_land .= '</div>';

/*
*  Категория ЗУ
*/
$category = '<div class="select_block count">';
$categories = '';
$nameParams = 'Не важно';
$valueInput = '';
for($c=0; $c<count($_CATEGORY_COUNTRY); $c++){
	$current = '';
	if(isset($PageInfoAd['category']) && $PageInfoAd['category']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
		$nameParams = $_CATEGORY_COUNTRY[$c];
	}
	$categories .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_CATEGORY_COUNTRY[$c].'</a></li>';
}
$category .= '<input type="hidden" name="s[category]"'.$valueInput.'>';
$category .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$category .= '<div class="scrollbar-inner">';
$category .= '<ul>';
$category .= $categories;
$category .= '</ul>';
$category .= '</div>';
$category .= '</div>';

$rightBlock = '<div style="border-left:1px solid #b5babe;width:520px" class="right_block">
	<div class="table_form">
		<h2>Информация об активах, входящих в состав объекта</h2>
		<div class="line">
			<div style="width:100%" class="block">
				<div class="row">
					<div style="width:148px" class="cell label right one_second">
						<label style="margin-top:0">Площадь земельного<br> участка</label>
					</div>
					<div style="width:185px" class="cell one_second last_col">
						<input style="width:145px" type="text" class="text" name="s[area_square]">
						<div class="com">м<sup>2</sup></div>
					</div>
				</div>
				<div class="row">
					<div style="width:148px!important;margin-right:15px" class="cell label right one_second">
						<label>Кадастровый номер</label>
					</div>
					<div class="cell one_second last_col">
						<input style="width:145px" type="text" class="text" name="s[cadastr]">
					</div>
				</div>
				<div class="row">
					<div style="width:148px!important;margin-right:15px" class="cell label right one_second">
						<label>Категория ЗУ</label>
					</div>
					<div class="cell one_second last_col">
						'.$category.'
					</div>
				</div>
				<div class="row">
					<div style="width:148px!important;margin-right:15px" class="cell label right one_second">
						<label style="white-space:nowrap;margin-top:-2px">Разрешенное<br> использование</label>
					</div>
					<div class="cell one_second last_col">
						<input style="width:145px" type="text" class="text" name="s[used]">
					</div>
				</div>
				<div class="row">
					<div style="width:148px!important;margin-right:15px" class="cell right label one_second">
						<label style="margin-top:5px">Право на ЗУ</label>
					</div>
					<div style="width:310px" class="cell auto last_col">
						'.$right_land.'
					</div>
				</div>
			</div>
		</div>
	</div>
</div>';
	
/*
*  Франчайзинг
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['franchising'])){
	if($PageInfoAd['franchising']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['franchising'].'"';
	}
}
$franchising = '<div style="float:left;margin:5px 0 0 25px" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[franchising]" value="0">
	<input type="hidden"'.$valueParams.' name="s[franchising]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Франчайзинг</span>
	</div>
</div>';
	
/*
*  Подвал/полуподвал
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['basement'])){
	if($PageInfoAd['basement']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['basement'].'"';
	}
}
$basement = '<div style="float:left;" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[basement]" value="0">
	<input type="hidden"'.$valueParams.' name="s[basement]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">подвал/полуподвал</span>
	</div>
</div>';

/*
*  Состояние
*/
$condition_select = '<div class="select_block count">';
$condition_selects = '';
$nameParams = 'Не выбран';
$valueInput = '';
for($c=0; $c<count($_CONDITIONS_FRESH); $c++){
	$current = '';
	if(isset($PageInfoAd['conditions']) && $PageInfoAd['conditions']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
		$nameParams = $_CONDITIONS_FRESH[$c];
	}
	$condition_selects .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_CONDITIONS_FRESH[$c].'</a></li>';
}
$condition_select .= '<input type="hidden" name="s[conditions]"'.$valueInput.'>';
$condition_select .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$condition_select .= '<div class="scrollbar-inner">';
$condition_select .= '<ul>';
$condition_select .= $condition_selects;
$condition_select .= '</ul>';
$condition_select .= '</div>';
$condition_select .= '</div>';

echo '<div class="params_block">
	<div style="border-right:none;width:535px" class="left_block">
		<div class="table_form">
			<h2>Информация об объекте</h2>
			<div class="line">
				<div style="width:100%;float:none" class="block">
					<div class="row">
						<div style="width:148px!important;margin-right:15px" class="cell right label one_second">
							<label style="margin-top:5px">Тип бизнеса<b>*</b></label>
						</div>
						<div style="width:310px" class="cell required auto last_col">
							'.$business.'
							'.$franchising.'
						</div>
					</div>
					<div class="row">
						<div style="width:148px!important;margin-right:15px" class="cell label right one_second">
							<label style="margin-top:7px">Общая площадь<b>*</b></label>
						</div>
						<div style="width:190px" class="cell required one_second last_col">
							<input style="width:146px;" type="text" class="text" name="s[full_square]"'.$full_square.'>
							<div class="com">м<sup>2</sup></div>
						</div>
					</div>
					<div class="row">
						<div style="width:148px!important;margin-right:15px" class="cell label right one_second">
							<label>Этаж</label>
						</div>
						<div style="width:auto" class="cell required one_second last_col">
							<input style="width:146px;" type="text" class="text" name="s[floor]"'.$floor.'>
						</div>
						<div style="width:auto;margin-left:25px;margin-top:8px;float:left;" class="cell one_second last_col">
							'.$basement.'
						</div>
					</div>
					<div class="row">
						<div style="width:148px!important;margin-right:15px" class="cell right label one_second">
							<label style="margin-top:5px">Право на помещение</label>
						</div>
						<div style="width:310px" class="cell auto last_col">
							'.$rights.'
						</div>
					</div>
					<div class="row">
						<div style="width:148px!important;margin-right:15px;" class="cell right label one_second">
							<label style="margin-top:5px">Состояние</label>
						</div>
						<div style="width:370px" class="cell auto last_col">
							'.$condition_select.'
						</div>
					</div>
					<div class="row">
						<div style="width:148px!important;margin-right:15px;" class="cell label right one_second">
							<label>Электроснабжение</label>
						</div>
						<div style="width:200px" class="cell one_second last_col">
							<input style="width:148px" type="text" class="text" name="s[power_max]"'.$power_max.'>
							<div style="margin-top:8px" class="com">кВт</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	'.$rightBlock.'
</div>';
?>