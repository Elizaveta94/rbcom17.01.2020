<?

/*
*  Состояние
*/
$condition = '';
$conditions = '';
$valueInput = '';
for($c=0; $c<count($_CONDITIONS_TRK); $c++){
	$checked = '';
	if(isset($PageInfoAd['conditions']) && $PageInfoAd['conditions']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	$conditions .= '<li'.$checked.'><a style="padding:4px 7px;font-size:14px" href="javascript:void(0)" data-id="'.$c.'">'.$_CONDITIONS_TRK[$c].'</a></li>';
}
$condition = '<div class="checkbox_block">';
$condition .= '<input type="hidden" name="s[conditions]"'.$valueInput.'>';
$condition .= '<div class="select_checkbox">';
$condition .= '<ul>';
$condition .= $conditions;
$condition .= '</ul>';
$condition .= '</div>';
$condition .= '</div>';

/*
*  Право на строение
*/
$rights = '<div class="select_block count">';
$rights2 = '';
$nameParams = 'Не важно';
$valueInput = '';
for($c=0; $c<count($_RIGHTS); $c++){
	$current = '';
	if(isset($PageInfoAd['right_build']) && $PageInfoAd['right_build']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
	}
	$rights2 .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_RIGHTS[$c].'</a></li>';
}
$rights .= '<input type="hidden" name="s[right_build]"'.$valueInput.'>';
$rights .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$rights .= '<div class="scrollbar-inner">';
$rights .= '<ul>';
$rights .= $rights2;
$rights .= '</ul>';
$rights .= '</div>';
$rights .= '</div>';

/*
*  Право на ЗУ
*/
$right_land = '<div class="select_block count">';
$right_land2 = '';
$nameParams = 'Не важно';
$valueInput = '';
for($c=0; $c<count($_RIGHTS); $c++){
	$current = '';
	if(isset($PageInfoAd['rights']) && $PageInfoAd['rights']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
	}
	$right_land2 .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_RIGHTS[$c].'</a></li>';
}
$right_land .= '<input type="hidden" name="s[rights]"'.$valueInput.'>';
$right_land .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$right_land .= '<div class="scrollbar-inner">';
$right_land .= '<ul>';
$right_land .= $right_land2;
$right_land .= '</ul>';
$right_land .= '</div>';
$right_land .= '</div>';

/*
*  Категория ЗУ
*/
$category = '<div class="select_block count">';
$categories = '';
$nameParams = 'Не важно';
$valueInput = '';
for($c=0; $c<count($_CATEGORY_COUNTRY); $c++){
	$current = '';
	if(isset($PageInfoAd['category']) && $PageInfoAd['category']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
	}
	$categories .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_CATEGORY_COUNTRY[$c].'</a></li>';
}
$category .= '<input type="hidden" name="s[category]"'.$valueInput.'>';
$category .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$category .= '<div class="scrollbar-inner">';
$category .= '<ul>';
$category .= $categories;
$category .= '</ul>';
$category .= '</div>';
$category .= '</div>';

$rightBlock = '<div style="border-left:1px solid #b5babe;width:520px" class="right_block">
	<div class="table_form">
		<h2>Информация об активах, входящих в состав объекта</h2>
		<div class="line">
			<div style="width:100%" class="block">
				<div class="row">
					<div style="width:148px!important;margin-right:15px" class="cell label right one_second">
						<label style="margin-top:-2px">Общая площадь<br> строения<b>*</b></label>
					</div>
					<div style="width:180px" class="cell required one_second last_col">
						<input style="width:145px" type="text" class="text" name="s[full_square]">
						<div class="com">м<sup>2</sup></div>
					</div>
				</div>
				<div class="row">
					<div style="width:148px!important;margin-right:15px" class="cell label right one_second">
						<label>Год постройки</label>
					</div>
					<div class="cell one_second last_col">
						<input style="width:84px" type="text" class="text" name="s[year]">
					</div>
				</div>
				<div class="row">
					<div style="width:148px!important;margin-right:15px" class="cell right label one_second">
						<label style="margin-top:5px">Право на строение</label>
					</div>
					<div style="width:310px" class="cell auto last_col">
						'.$rights.'
					</div>
				</div>
				<div class="row">
					<div style="width:148px!important;margin-right:15px" class="cell right label one_second">
						<label style="margin-top:5px">Состояние</label>
					</div>
					<div style="width:370px" class="cell auto last_col">
						'.$condition.'
					</div>
				</div>
				<div class="row">
					<div style="width:148px" class="cell label right one_second">
						<label style="margin-top:0">Площадь земельного<br> участка</label>
					</div>
					<div style="width:185px" class="cell one_second last_col">
						<input style="width:86px" type="text" class="text" name="s[area_square]">
						<div class="com">м<sup>2</sup></div>
					</div>
				</div>
				<div class="row">
					<div style="width:148px!important;margin-right:15px" class="cell label right one_second">
						<label>Кадастровый номер</label>
					</div>
					<div class="cell one_second last_col">
						<input style="width:145px" type="text" class="text" name="s[cadastr]">
					</div>
				</div>
				<div class="row">
					<div style="width:148px!important;margin-right:15px" class="cell label right one_second">
						<label>Категория ЗУ</label>
					</div>
					<div class="cell one_second last_col">
						'.$category.'
					</div>
				</div>
				<div class="row">
					<div style="width:148px!important;margin-right:15px" class="cell label right one_second">
						<label style="white-space:nowrap;margin-top:-2px">Разрешенное<br> использование</label>
					</div>
					<div class="cell one_second last_col">
						<input style="width:145px" type="text" class="text" name="s[used]">
					</div>
				</div>
				<div class="row">
					<div style="width:148px!important;margin-right:15px" class="cell right label one_second">
						<label style="margin-top:5px">Право на ЗУ</label>
					</div>
					<div style="width:310px" class="cell auto last_col">
						'.$right_land.'
					</div>
				</div>
			</div>
		</div>
	</div>
</div>';

echo '<div class="params_block">
	<div style="border-right:none;width:535px" class="left_block">
		<div class="table_form">
			<h2>Информация об объекте</h2>
			<div class="line">
				<div style="width:100%;float:none" class="block">
					<div class="row">
						<div style="margin-right:15px;width:132px!important" class="cell label right one_second">
							<label style="white-space:nowrap;margin-left:-3px;margin-top:-2px">Наименование<br> бизнеса</label>
						</div>
						<div class="cell one_second last_col">
							<input style="width:377px" type="text" class="text" name="s[name_busines]">
						</div>
					</div>
					<div class="row">
						<div style="margin-right:15px;width:132px!important" class="cell label right one_second">
							<label>Год основания</label>
						</div>
						<div style="width:130px" class="cell one_second last_col">
							<input style="width:137px" type="text" class="text" name="s[year_created]">
						</div>
					</div>
					<div class="row">
						<div style="margin-right:15px;width:132px!important" class="cell label right one_second">
							<label>Обороты за год</label>
						</div>
						<div style="width:130px" class="cell one_second last_col">
							<input style="width:137px" type="text" class="text" name="s[budget]">
						</div>
					</div>
					<div class="row">
						<div style="margin-right:15px;width:132px!important" class="cell label right one_second">
							<label style="margin-top:-2px">Кол-во персонала<br> чел</label>
						</div>
						<div style="width:130px" class="cell one_second last_col">
							<input style="width:137px" type="text" class="text" name="s[staffs]">
						</div>
					</div>
					<div class="row">
						<div style="margin-right:15px;width:132px!important" class="cell label right one_second">
							<label>Срок окупаемости</label>
						</div>
						<div style="width:130px" class="cell one_second last_col">
							<input style="width:137px" type="text" class="text" name="s[payback]">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	'.$rightBlock.'
</div>';
?>