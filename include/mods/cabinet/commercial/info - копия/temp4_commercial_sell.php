<?

/*
*  Отделка
*/
$decorations = '';
$decoration = '';
$valueInput = '';
for($c=0; $c<count($_DECORATIONS); $c++){
	$checked = '';
	if(isset($PageInfoAd['decoration']) && $PageInfoAd['decoration']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	$decoration .= '<li'.$checked.'><a style="padding:4px 7px;font-size:14px" href="javascript:void(0)" data-id="'.$c.'">'.$_DECORATIONS[$c].'</a></li>';
}
$decorations = '<div class="checkbox_block">';
$decorations .= '<input type="hidden" name="s[decoration]"'.$valueInput.'>';
$decorations .= '<div class="select_checkbox">';
$decorations .= '<ul>';
$decorations .= $decoration;
$decorations .= '</ul>';
$decorations .= '</div>';
$decorations .= '</div>';

/*
*  Мебель
*/
$furnitures = '';
$furniture = '';
$valueInput = '';
for($c=0; $c<count($_FURNITURE); $c++){
	$checked = '';
	if(isset($PageInfoAd['furniture']) && $PageInfoAd['furniture']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	$furniture .= '<li'.$checked.'><a style="padding:4px 7px;font-size:14px" href="javascript:void(0)" data-id="'.$c.'">'.$_FURNITURE[$c].'</a></li>';
}
$furnitures = '<div class="checkbox_block">';
$furnitures .= '<input type="hidden" name="s[furniture]"'.$valueInput.'>';
$furnitures .= '<div class="select_checkbox">';
$furnitures .= '<ul>';
$furnitures .= $furniture;
$furnitures .= '</ul>';
$furnitures .= '</div>';
$furnitures .= '</div>';

/*
*  Парковка
*/
$parking_values = '';
$parking_value = '';
$valueInput = '';
for($c=0; $c<count($_PARKING); $c++){
	$checked = '';
	if(isset($PageInfoAd['parking_value']) && $PageInfoAd['parking_value']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	$parking_value .= '<li'.$checked.'><a style="padding:4px 7px;font-size:14px" href="javascript:void(0)" data-id="'.$c.'">'.$_PARKING[$c].'</a></li>';
}
$parking_values = '<div class="checkbox_block">';
$parking_values .= '<input type="hidden" name="s[parking_value]"'.$valueInput.'>';
$parking_values .= '<div class="select_checkbox">';
$parking_values .= '<ul>';
$parking_values .= $parking_value;
$parking_values .= '</ul>';
$parking_values .= '</div>';
$parking_values .= '</div>';

/*
*  Готовность
*/
$readis = '';
$readiss = '';
$valueInput = '';
for($c=0; $c<count($_READIS); $c++){
	$checked = '';
	if(isset($PageInfoAd['readis']) && $PageInfoAd['readis']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	$readiss .= '<li'.$checked.'><a style="padding:4px 7px;font-size:14px" href="javascript:void(0)" data-id="'.$c.'">'.$_READIS[$c].'</a></li>';
}
$readis = '<div class="checkbox_block">';
$readis .= '<input type="hidden" name="s[readis]"'.$valueInput.'>';
$readis .= '<div class="select_checkbox">';
$readis .= '<ul>';
$readis .= $readiss;
$readis .= '</ul>';
$readis .= '</div>';
$readis .= '</div>';

/*
*  Вход
*/
$entrances = '';
$entrance = '';
$valueInput = '';
for($c=0; $c<count($_ENTRANCES); $c++){
	$checked = '';
	if(isset($PageInfoAd['entrance']) && $PageInfoAd['entrance']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	$entrance .= '<li'.$checked.'><a style="padding:4px 7px;font-size:14px" href="javascript:void(0)" data-id="'.$c.'">'.$_ENTRANCES[$c].'</a></li>';
}
$entrances = '<div class="checkbox_block">';
$entrances .= '<input type="hidden" name="s[entrance]"'.$valueInput.'>';
$entrances .= '<div class="select_checkbox">';
$entrances .= '<ul>';
$entrances .= $entrance;
$entrances .= '</ul>';
$entrances .= '</div>';
$entrances .= '</div>';

/*
*  Право на ЗУ
*/
$rights = '';
$rights2 = '';
$valueInput = '';
for($c=1; $c<count($_RIGHTS); $c++){
	$checked = '';
	if(isset($PageInfoAd['right']) && $PageInfoAd['right']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	$rights2 .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_RIGHTS[$c].'</a></li>';
}
$rights = '<div class="checkbox_block">';
$rights .= '<input type="hidden" name="s[right]"'.$valueInput.'>';
$rights .= '<div class="select_checkbox">';
$rights .= '<ul>';
$rights .= $rights2;
$rights .= '</ul>';
$rights .= '</div>';
$rights .= '</div>';


/*
*  Категория ЗУ
*/
$category = '<div class="select_block count">';
$categories = '';
$nameParams = 'Не важно';
$valueInput = '';
for($c=0; $c<count($_CATEGORY_COUNTRY); $c++){
	$current = '';
	if(isset($PageInfoAd['category']) && $PageInfoAd['category']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
	}
	$categories .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_CATEGORY_COUNTRY[$c].'</a></li>';
}
$category .= '<input type="hidden" name="s[category]"'.$valueInput.'>';
$category .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$category .= '<div class="scrollbar-inner">';
$category .= '<ul>';
$category .= $categories;
$category .= '</ul>';
$category .= '</div>';
$category .= '</div>';
	
/*
*  Класс объекта
*/
$classObject = '';
$classObjects = '';
$valueInput = '';
for($c=0; $c<count($_CLASS_OBJECTS); $c++){
	$checked = '';
	if(isset($PageInfoAd['class_object']) && $PageInfoAd['class_object']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}	
	if(empty($PageInfoAd['class_object'])){
		if($c==0){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';				
		}
	}
	$classObjects .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_CLASS_OBJECTS[$c].'</a></li>';
}
$classObject = '<div class="checkbox_block">';
$classObject .= '<input type="hidden" name="s[class_object]"'.$valueInput.'>';
$classObject .= '<div class="select_checkbox">';
$classObject .= '<ul>';
$classObject .= $classObjects;
$classObject .= '</ul>';
$classObject .= '</div>';
$classObject .= '</div>';

/*
*  Тип здания
*/
$type_build = '<div class="select_block count required">';
$type_build2 = '';
$nameParams = 'Не выбран';
$valueInput = '';
for($c=0; $c<count($_TYPE_BUILD_OFFICE); $c++){
	$current = '';
	if(isset($PageInfoAd['type_build']) && $PageInfoAd['type_build']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
	}
	$type_build2 .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_BUILD_OFFICE[$c].'</a></li>';
}
$type_build .= '<input type="hidden" name="s[type_build]"'.$valueInput.'>';
$type_build .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$type_build .= '<div class="scrollbar-inner">';
$type_build .= '<ul>';
$type_build .= $type_build2;
$type_build .= '</ul>';
$type_build .= '</div>';
$type_build .= '</div>';


$rightBlock = '<div style="border-left:1px solid #b5babe;width:500px" class="right_block">
	<div class="table_form">
		<h2>Информация о земельном участке</h2>
		<div class="line">
			<div style="width:100%" class="block">
				<div class="row">
					<div style="width:130px!important;margin-right:15px" class="cell label right one_second">
						<label style="white-space:nowrap">Название комплекса</label>
					</div>
					<div class="cell one_second last_col">
						<input style="width:230px" type="text" class="text" name="s[name_build]" value="'.$PageInfoAd['name_build'].'">
					</div>
				</div>
				<!--<div class="row">
					<div style="width:130px!important;margin-right:15px" class="cell label right one_second">
						<label style="white-space:nowrap">Год постройки</label>
					</div>
					<div class="cell one_second last_col">
						<input style="width:90px" type="text" class="text" name="s[year]"'.$year.'>
					</div>
				</div>-->
				<div class="row">
					<div style="width:130px!important;margin-right:15px" class="cell label right one_second">
						<label style="margin-top:5px">Тип здания<b>*</b></label>
					</div>
					<div class="cell one_second last_col">
						'.$type_build.'
					</div>
				</div>
				<div class="row">
					<div style="width:130px!important;margin-right:15px" class="cell right label one_second">
						<label style="margin-top:5px">Класс объекта</label>
					</div>
					<div style="width:310px" class="cell one_second last_col">
						'.$classObject.'
					</div>
				</div>
				<div class="row">
					<div style="width:130px!important;margin-right:15px" class="cell right label one_second">
						<label style="margin-top:5px">Готовность</label>
					</div>
					<div style="width:310px" class="cell one_second last_col">
						'.$readis.'
					</div>
				</div>
				<div class="row">
					<div style="width:130px!important;margin-right:15px" class="cell right label one_second">
						<label style="margin-top:5px">Вход</label>
					</div>
					<div style="width:310px" class="cell one_second last_col">
						'.$entrances.'
					</div>
				</div>
			</div>
		</div>
		<div style="margin-top:30px" class="line">
			<div style="width:200px;margin-left:145px" class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$atms.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$confer.'
					</div>
				</div>
			</div>
			<div class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$cafe.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$canteen.'
					</div>
				</div>
			</div>
		</div>
	</div>
</div>';

/*
*  Тип здания
*/
$typeBuildings = '<div class="select_block count">';
$typeBuildings2 = '';
$nameParams = 'Не выбран';
$valueInput = '';
for($c=0; $c<count($_TYPE_BUILD_TRK); $c++){
	$current = "";
	if($c==0 && !isset($PageInfoAd['type_build'])){
		$typeBuildings2 .= '<li class="current"><a href="javascript:void(0)" data-id="">Не выбран</a></li>';
	}
	if(isset($PageInfoAd['type_build']) && $PageInfoAd['type_build']==$с){
		$current = ' class="current"';
		$nameParams = $_TYPE_BUILD_TRK[$c];
		$valueInput = ' value="'.$с.'"';
	}
	$typeBuildings2 .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$с.'">'.$_TYPE_BUILD_TRK[$c].'</a></li>';
}
$typeBuildings .= '<input type="hidden" name="s[type_build]"'.$valueInput.'>';
$typeBuildings .= '<div style="width:162px" class="choosed_block">'.$nameParams.'</div>';
$typeBuildings .= '<div class="scrollbar-inner">';
$typeBuildings .= '<ul>';
$typeBuildings .= $typeBuildings2;
$typeBuildings .= '</ul>';
$typeBuildings .= '</div>';
$typeBuildings .= '</div>';

/*
*  Высота потолков
*/
$height_ceiling = '<div class="select_block count">';
$height_ceilings = '';
$nameParams = 'Не важно';
$valueInput = '';
$devs = mysql_query("
	SELECT *
	FROM ".$template."_type_house
	WHERE activation='1'
	ORDER BY num
");
if(mysql_num_rows($devs)>0){
	$n = 0;
	while($dev = mysql_fetch_assoc($devs)){
		$current = "";
		
		if($n==0 && !isset($PageInfoAd['ceiling_height'])){
			$height_ceilings .= '<li class="current"><a href="javascript:void(0)" data-id="">Не важно</a></li>';
		}
		if($PageInfoAd['ceiling_height']==$dev['id']){
			$current = ' class="current"';
			$nameParams = $dev['name'];
			$valueInput = ' value="'.$dev['id'].'"';
		}
		$height_ceilings .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$dev['id'].'">'.$dev['name'].'</a></li>';
		$n++;
	}
}
$height_ceiling .= '<input type="hidden" name="s[ceiling_height]"'.$valueInput.'>';
$height_ceiling .= '<div style="width:162px" class="choosed_block">'.$nameParams.'</div>';
$height_ceiling .= '<div class="scrollbar-inner">';
$height_ceiling .= '<ul>';
$height_ceiling .= $height_ceilings;
$height_ceiling .= '</ul>';
$height_ceiling .= '</div>';
$height_ceiling .= '</div>';

/*
*  Место на парковке
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['parking'])){
	if($PageInfoAd['parking']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['parking'].'"';
	}
}
$parking = '<div style="float:left;margin-right:40px" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[parking]" value="0">
	<input type="hidden"'.$valueParams.' name="s[parking]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Место на парковке</span>
	</div>
</div>';


/*
*  Подвал
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['basement'])){
	if($PageInfoAd['basement']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['basement'].'"';
	}
}
$basement = '<div style="float:left;margin-right:40px" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[basement]" value="0">
	<input type="hidden"'.$valueParams.' name="s[basement]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Подвал</span>
	</div>
</div>';

/*
*  Полуподвал
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['semibasement'])){
	if($PageInfoAd['semibasement']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['semibasement'].'"';
	}
}
$semibasement = '<div style="float:left;margin-right:40px" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[semibasement]" value="0">
	<input type="hidden"'.$valueParams.' name="s[semibasement]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Полуподвал</span>
	</div>
</div>';

/*
*  Свободно
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['freeplace'])){
	if($PageInfoAd['freeplace']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['freeplace'].'"';
	}
}
$freeplace = '<div style="float:left;margin-right:40px" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[freeplace]" value="0">
	<input type="hidden"'.$valueParams.' name="s[freeplace]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Свободно</span>
	</div>
</div>';
echo '<div class="params_block">
	<div style="border-right:none;width:555px" class="left_block">
		<div class="table_form">
			<h2>Информация об объекте</h2>
			<div class="line">
				<div style="width:315px" class="block">
					<div class="row">
						<div style="width:92px!important" class="cell label right one_second">
							<label style="white-space:nowrap;margin-left:-3px;margin-top:-2px">Кадастровый<br> номер</label>
						</div>
						<div style="width:190px" class="cell one_second last_col">
							<input style="width:162px" type="text" class="text" name="s[cadastr]">
						</div>
					</div>
					<div class="row">
						<div style="width:92px!important" class="cell label right one_second">
							<label style="margin-top:-2px">Общая площадь<b>*</b></label>
						</div>
						<div style="width:190px" class="cell required one_second last_col">
							<input style="width:162px" type="text" class="text" name="s[full_square]">
							<div class="com">м<sup>2</sup></div>
						</div>
					</div>
					<div class="row">
						<div style="width:92px!important" class="cell label right one_second">
							<label>Этаж<b>*</b></label>
						</div>
						<div style="width:190px" class="cell required one_second last_col">
							<input style="width:162px" type="text" class="text" name="s[floor]">
						</div>
					</div>
					<!--<div class="row">
						<div style="width:92px!important" class="cell label right one_second">
							<label style="margin-top:-2px">Высота потолков</label>
						</div>
						<div style="width:160px" class="cell one_second last_col">
							'.$height_ceiling.'
						</div>
					</div>-->
				</div>
				<div style="width:245px;margin-left:10px;margin-top:52px" class="block">
					<div class="row">
						<div style="margin-right:15px;width:72px!important" class="cell label right one_second">
							<label style="margin-top:-2px">Площадь помещений</label>
						</div>
						<div style="width:155px" class="cell one_second last_col">
							<input style="width:122px" placeholder="Пример: 20+15-10" type="text" class="text" name="s[rooms_square]">
							<div class="com">м<sup>2</sup></div>
						</div>
					</div>
					<div class="row">
						<div style="margin-right:15px;width:72px!important" class="cell label right one_second">
							<label style="margin-top:-2px">Этажей<br> в здании</label>
						</div>
						<div style="width:130px" class="cell one_second last_col">
							<input style="width:122px" type="text" class="text" name="s[floors]">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div style="margin-top:5px;width:93px!important;" class="cell right label one_second">&nbsp;</div>
				<div class="cell auto last_col">
					'.$basement.'
					'.$semibasement.'
				</div>
			</div>

			<div class="row">
				<div style="width:92px!important" class="cell right label one_second">
					<label style="margin-top:5px">Отделка</label>
				</div>
				<div style="width:370px" class="cell auto last_col">
					'.$decorations.'
				</div>
			</div>
			<div class="row">
				<div style="width:92px!important" class="cell right label one_second">
					<label style="margin-top:5px">Мебель</label>
				</div>
				<div style="width:370px" class="cell auto last_col">
					'.$furnitures.'
				</div>
			</div>
			<div class="row">
				<div style="margin-top:5px;width:93px!important;" class="cell right label one_second">&nbsp;</div>
				<div class="cell auto last_col">
					'.$freeplace.'
				</div>
			</div>
			<div class="row">
				<div style="width:92px!important" class="cell right label one_second">
					<label style="margin-top:5px">Парковка</label>
				</div>
				<div style="width:370px" class="cell auto last_col">
					'.$parking_values.'
				</div>
			</div>
			<div class="row">
				<div style="width:92px!important" class="cell label right one_second">
					<label style="white-space:nowrap;margin-left:-3px;margin-top:-2px">Мест<br> в паркинге</label>
				</div>
				<div style="width:190px" class="cell one_second last_col">
					<input style="width:60px" type="text" class="text" name="s[parking_place]">
				</div>
			</div>
			<!--<div class="row">
				<div style="width:92px!important;margin-top:5px" class="cell right label one_second">&nbsp;</div>
				<div class="cell auto last_col">
					'.$parking.'
				</div>
			</div>-->
		</div>
	</div>
	'.$rightBlock.'
</div>';
?>