<?

/*
*  Состояние
*/
$condition = '';
$conditions = '';
$valueInput = '';
for($c=0; $c<count($_CONDITIONS); $c++){
	$checked = '';
	if(isset($PageInfoAd['conditions']) && $PageInfoAd['condition']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	if(empty($PageInfoAd['conditions'])){
		if($c==0){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';				
		}
	}
	$conditions .= '<li'.$checked.'><a style="padding:4px 7px;font-size:14px" href="javascript:void(0)" data-id="'.$c.'">'.$_CONDITIONS[$c].'</a></li>';
}
$condition = '<div class="checkbox_block">';
$condition .= '<input type="hidden" name="s[conditions]"'.$valueInput.'>';
$condition .= '<div class="select_checkbox">';
$condition .= '<ul>';
$condition .= $conditions;
$condition .= '</ul>';
$condition .= '</div>';
$condition .= '</div>';

/*
*  Авто подъезд
*/
$auto_staircase = '';
$auto_staircases = '';
$valueInput = '';
for($c=0; $c<count($_AUTO_STAIRCASE); $c++){
	$checked = '';
	if(isset($PageInfoAd['auto_staircase']) && $PageInfoAd['auto_staircase']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	if(empty($PageInfoAd['auto_staircase'])){
		if($c==0){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';				
		}
	}
	$auto_staircases .= '<li'.$checked.'><a style="padding:4px 7px;font-size:14px" href="javascript:void(0)" data-id="'.$c.'">'.$_AUTO_STAIRCASE[$c].'</a></li>';
}
$auto_staircase = '<div class="checkbox_block">';
$auto_staircase .= '<input type="hidden" name="s[auto_staircase]"'.$valueInput.'>';
$auto_staircase .= '<div class="select_checkbox">';
$auto_staircase .= '<ul>';
$auto_staircase .= $auto_staircases;
$auto_staircase .= '</ul>';
$auto_staircase .= '</div>';
$auto_staircase .= '</div>';

/*
*  ЖД подъезд
*/
$station_staircase = '';
$station_staircases = '';
$valueInput = '';
for($c=0; $c<count($_STATION_STAIRCASE); $c++){
	$checked = '';
	if(isset($PageInfoAd['station_staircase']) && $PageInfoAd['station_staircase']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	if(empty($PageInfoAd['station_staircase'])){
		if($c==0){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';				
		}
	}
	$station_staircases .= '<li'.$checked.'><a style="padding:4px 7px;font-size:14px" href="javascript:void(0)" data-id="'.$c.'">'.$_STATION_STAIRCASE[$c].'</a></li>';
}
$station_staircase = '<div class="checkbox_block">';
$station_staircase .= '<input type="hidden" name="s[station_staircase]"'.$valueInput.'>';
$station_staircase .= '<div class="select_checkbox">';
$station_staircase .= '<ul>';
$station_staircase .= $station_staircases;
$station_staircase .= '</ul>';
$station_staircase .= '</div>';
$station_staircase .= '</div>';

/*
*  Ограждение
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['fencing'])){
	if($PageInfoAd['fencing']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['fencing'].'"';
	}
}
$fencing = '<div class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[fencing]" value="0">
	<input type="hidden"'.$valueParams.' name="s[fencing]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Ограждение</span>
	</div>
</div>';

/*
*  Электроснабжение
*/
$electricity = '';
$station_staircases = '';
$valueInput = '';
for($c=0; $c<count($_ELECTRICITY); $c++){
	$checked = '';
	if(isset($PageInfoAd['electricity']) && $PageInfoAd['electricity']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	if(empty($PageInfoAd['electricity'])){
		if($c==0){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';				
		}
	}
	$station_staircases .= '<li'.$checked.'><a style="padding:4px 7px;font-size:14px" href="javascript:void(0)" data-id="'.$c.'">'.$_ELECTRICITY[$c].'</a></li>';
}
$electricity = '<div class="checkbox_block">';
$electricity .= '<input type="hidden" name="s[electricity]"'.$valueInput.'>';
$electricity .= '<div class="select_checkbox">';
$electricity .= '<ul>';
$electricity .= $station_staircases;
$electricity .= '</ul>';
$electricity .= '</div>';
$electricity .= '</div>';

/*
*  Наличие согласований для застройки
*/
$matching = '<div class="select_block count">';
$matchings = '';
$nameParams = 'Не важно';
$valueInput = '';
for($c=0; $c<count($_MATCHINGS); $c++){
	$current = '';
	if(isset($PageInfoAd['matching']) && $PageInfoAd['matching']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
	}
	$matchings .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_MATCHINGS[$c].'</a></li>';
}
$matching .= '<input type="hidden" name="s[matching]"'.$valueInput.'>';
$matching .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$matching .= '<div class="scrollbar-inner">';
$matching .= '<ul>';
$matching .= $matchings;
$matching .= '</ul>';
$matching .= '</div>';
$matching .= '</div>';

$rightBlock = '<div style="border-left:1px solid #b5babe" class="right_block">
	<div class="table_form">
		<h2>Улучшение на земельном участке</h2>
		<div class="line">
			<div style="width:100%" class="block">
				<div class="row">
					<div style="margin-top:5px;width:130px" class="cell right label one_second">Состояние</div>
					<div style="width:400px" class="cell one_second last_col">
						'.$condition.'
					</div>
				</div>
				<div class="row">
					<div style="margin-top:5px;width:130px" class="cell right label one_second">Авто подъезд</div>
					<div style="width:400px" class="cell one_second last_col">
						'.$auto_staircase.'
					</div>
				</div>
				<div class="row">
					<div style="margin-top:5px;width:130px" class="cell right label one_second">ЖД подъезд</div>
					<div style="width:400px" class="cell one_second last_col">
						'.$station_staircase.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:130px" class="cell right label one_second">&nbsp;</div>
					<div style="width:200px" class="cell one_second last_col">
						'.$fencing.'
					</div>
				</div>
				<div class="row">
					<div style="margin-top:5px;width:130px" class="cell right label one_second">Электроснабжение</div>
					<div style="width:400px" class="cell one_second last_col">
						'.$electricity.'
					</div>
				</div>
				<div class="row">
					<div style="width:130px" class="cell label right one_second">
						<label>Максимальная мощность</label>
					</div>
					<div style="width:125px" class="cell one_second last_col">
						<input style="width:85px" type="text" class="text" name="s[power_max]"'.$power_max.'>
						<div style="margin-top:8px" class="com">кВт</div>
					</div>
				</div>
			</div>
		</div>
		<div style="margin-top:5px" class="line">
			<div style="width:246px;margin-left:145px" class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$plumbing.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$sewerage.'
					</div>
				</div>
			</div>
			<div class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$heating.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$gas.'
					</div>
				</div>
			</div>
		</div>
		<div class="line">
			<div class="block">
				<!--<div class="row">
					<div style="width:130px!important" class="cell label right one_second">
						<label style="margin-top:-1px">Наличие<br> согласований<br> для застройки</label>
					</div>
					<div class="cell one_second last_col">
						'.$matching.'
					</div>
				</div>-->
				<div class="row">
					<div style="width:130px!important" class="cell label right one_second">
						<label style="white-space:nowrap;margin-left:-3px;margin-top:-2px">Площадь<br> строений на ЗУ</label>
					</div>
					<div style="width:180px" class="cell one_second last_col">
						<input style="width:146px" type="text" class="text" name="s[building_area]"'.$building_area.'>
						<div class="com">м<sup>2</sup></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>';

	
/*
*  Право на ЗУ
*/
$rights = '';
$rights2 = '';
$valueInput = '';
for($c=1; $c<count($_RIGHTS); $c++){
	$checked = '';
	if(isset($PageInfoAd['rights']) && $PageInfoAd['rights']==$c){
		$checked = ' class="checked"';
		$valueInput = ' value="'.$c.'"';
	}
	$rights2 .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_RIGHTS[$c].'</a></li>';
}
$rights = '<div class="checkbox_block">';
$rights .= '<input type="hidden" name="s[rights]"'.$valueInput.'>';
$rights .= '<div class="select_checkbox">';
$rights .= '<ul>';
$rights .= $rights2;
$rights .= '</ul>';
$rights .= '</div>';
$rights .= '</div>';


/*
*  Категория ЗУ
*/
$category = '<div class="select_block count">';
$categories = '';
$nameParams = 'Не важно';
$valueInput = '';
for($c=0; $c<count($_CATEGORY_COMM_COUNTRY); $c++){
	$current = '';
	if(isset($PageInfoAd['category']) && $PageInfoAd['category']==$c){
		$current = ' class="current"';
		$valueInput = ' value="'.$c.'"';
	}
	$categories .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_CATEGORY_COMM_COUNTRY[$c].'</a></li>';
}
$category .= '<input type="hidden" name="s[category]"'.$valueInput.'>';
$category .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
$category .= '<div class="scrollbar-inner">';
$category .= '<ul>';
$category .= $categories;
$category .= '</ul>';
$category .= '</div>';
$category .= '</div>';


/*
*  Инвестиционный проект
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['investment_value'])){
	if($PageInfoAd['investment_value']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['investment_value'].'"';
	}
}
$investment_value = '<div style="float:left;margin-right:40px" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[investment_value]" value="0">
	<input type="hidden"'.$valueParams.' name="s[investment_value]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Инвестиционный проект</span>
	</div>
</div>';

/*
*  Наличие обременений
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['encumbrances'])){
	if($PageInfoAd['encumbrances']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['encumbrances'].'"';
	}
}
$encumbrances = '<div style="float:left;margin-right:40px" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[encumbrances]" value="0">
	<input type="hidden"'.$valueParams.' name="s[encumbrances]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Наличие обременений</span>
	</div>
</div>';


echo '<div class="params_block">
	<div style="border-right:none;width:505px" class="left_block">
		<div class="table_form">
			<h2>Информация о земельном участке</h2>
			<div class="line">
				<div style="width:100%;" class="block">
					<div class="row">
						<div style="width:122px!important" class="cell label right one_second">
							<label style="white-space:nowrap;margin-left:-3px;margin-top:-2px">Кадастровый<br> номер</label>
						</div>
						<div class="cell one_second last_col">
							<input style="width:230px" type="text" class="text" name="s[cadastr]">
						</div>
					</div>
					<div class="row">
						<div style="width:122px!important" class="cell label right one_second">
							<label style="white-space:nowrap;margin-left:-3px;margin-top:-2px">Площадь<br> зем. участка<b>*</b></label>
						</div>
						<div class="cell required one_second last_col">
							<input style="width:137px" type="text" class="text" name="s[area_square]">
							<div class="com">м<sup>2</sup></div>
						</div>
					</div>
					<div class="row">
						<div style="width:122px!important" class="cell right label one_second">
							<label style="margin-top:5px">Право на ЗУ</label>
						</div>
						<div style="width:310px" class="cell auto last_col">
							'.$rights.'
						</div>
					</div>
					<div style="margin-top:16px" class="row">
						<div style="width:122px!important" class="cell right label one_second">&nbsp;</div>
						<div style="width:300px" class="cell one_second last_col">
							'.$investment_value.'
						</div>
					</div>
					<div class="row">
						<div style="width:122px!important" class="cell label right one_second">
							<label style="margin-top:5px">Категория ЗУ</label>
						</div>
						<div class="cell one_second last_col">
							'.$category.'
						</div>
					</div>
					<div class="row">
						<div style="width:122px!important" class="cell label right one_second">
							<label style="white-space:nowrap;margin-left:-3px;margin-top:-2px">Зона согласно<br> ППЗ</label>
						</div>
						<div style="width:350px" class="cell one_second last_col">
							<input style="width:100%" type="text" class="text" name="s[ppz]">
						</div>
					</div>
					<div class="row">
						<div style="width:122px!important" class="cell label right one_second">
							<label style="white-space:nowrap;margin-left:-3px;margin-top:-2px">Разрешенное<br> использование</label>
						</div>
						<div style="width:350px" class="cell one_second last_col">
							<input style="width:100%" type="text" class="text" name="s[used]">
						</div>
					</div>
					<div style="margin-top:16px" class="row">
						<div style="width:122px!important" class="cell right label one_second">&nbsp;</div>
						<div style="width:300px" class="cell one_second last_col">
							'.$encumbrances.'
						</div>
					</div>
				</div>
			</div>										
		</div>
	</div>
	'.$rightBlock.'
</div>';
?>