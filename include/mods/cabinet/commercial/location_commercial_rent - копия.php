<?
echo '<div style="border:none" class="left_block">
	<input type="hidden" name="ads_save" value="true"> 
	<input type="hidden" name="step" value="1">
	<input type="hidden" name="s[dist_value]" value="'.$PageInfoAd['dist_value'].'">
	<input type="hidden" name="s[station_id]" value="'.$PageInfoAd['station_id'].'">
	<input type="hidden" name="s[station]" value="'.$PageInfoAd['station'].'">
	<div class="table_form">
		<div class="row">
			<div class="cell info full">
				<p>Размещение объявлений доступно только на территории Российской Федерации.</p>
			</div>
		</div>';
		
		if(isset($type_commer) && ($type_commer==9 || $type_commer==11)){
			echo '<div class="row newbuildings flat">
				<div class="cell label right one_second">
					<label style="margin-top:-2px">Название<br> комплекса</label>
				</div>
				<div class="cell one_second last_col">
					<div class="request_block">
						<input autocomplete="off" type="text" class="text" name="s[name]"'.$name.'>
					</div>
				</div>
			</div>';
			echo '<div class="row">
				<div class="cell label right one_second">
					<label>Адрес<b>*</b></label>
				</div>
				<div class="cell required one_second last_col">
					<div style="width:365px" class="request_block">
						<input type="text" class="text" name="s[address]" value="'.$PageInfoAd['address'].'">
						<div class="search_request_block">
							<div class="scrollbar-inner">
								<ul></ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row newbuildings flat">
				<div class="cell label right one_second">
					<label>&nbsp;</label>
				</div>
				<div class="cell one_second last_col">
					<div class="request_block">
						<a class="problem" href="javascript:void(0)">Проблемы с вводом адреса?</a>
					</div>
				</div>
			</div>';
		}
		else if(isset($type_commer) && $type_commer==5){
			echo '<div class="row newbuildings flat">
				<div style="width:185px" class="cell label right one_second">
					<label style="margin-top:-2px">Название<br> пром. зоны</label>
				</div>
				<div class="cell one_second last_col">
					<div class="request_block">
						<input autocomplete="off" type="text" class="text" name="s[name]"'.$name.'>
					</div>
				</div>
			</div>';
			echo '<div class="row">
				<div style="width:185px" class="cell label right one_second">
					<label>Адрес<b>*</b></label>
				</div>
				<div class="cell required one_second last_col">
					<div style="width:325px" class="request_block">
						<input type="text" class="text" name="s[address]" value="'.$PageInfoAd['address'].'">
						<div class="search_request_block">
							<div class="scrollbar-inner">
								<ul></ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row newbuildings flat">
				<div style="width:185px" class="cell label right one_second">
					<label>&nbsp;</label>
				</div>
				<div class="cell one_second last_col">
					<div class="request_block">
						<a class="problem" href="javascript:void(0)">Проблемы с вводом адреса?</a>
					</div>
				</div>
			</div>
			<div class="row">
				<div style="width:185px" class="cell label right one_second">
					<label style="margin-top:-2px">Ближайшая станция метро, Ж/д станция, остановка</label>
				</div>
				<div class="cell one_second last_col">
					<div class="request_block">
						<input type="text" class="text" name="s[station]" value="'.$PageInfoAd['station_name'].'">
					</div>
				</div>
			</div>
			<div class="row">
				<div style="width:185px" class="cell label right one_second">
					<label style="margin-top:-2px">Удаленность от метро<br>(остановки)</label>
				</div>
				<div class="cell one_second last_col">
					<div class="request_block">
						<input type="text" class="text" name="s[dist_value]" value="'.$PageInfoAd['dist_value'].'">
					</div>
				</div>
			</div>';
		}
		else if(isset($type_commer) && ($type_commer==3 || $type_commer==7)){
			echo '<div class="row newbuildings flat">
				<div style="width:185px" class="cell label right one_second">
					<label>Название комплекса</label>
				</div>
				<div class="cell one_second last_col">
					<div class="request_block">
						<input autocomplete="off" type="text" class="text" name="s[name]"'.$name.'>
					</div>
				</div>
			</div>';
			echo '<div class="row">
				<div style="width:185px" class="cell label right one_second">
					<label>Адрес<b>*</b></label>
				</div>
				<div class="cell required one_second last_col">
					<div style="width:325px" class="request_block">
						<input type="text" class="text" name="s[address]" value="'.$PageInfoAd['address'].'">
						<div class="search_request_block">
							<div class="scrollbar-inner">
								<ul></ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div style="width:185px" class="cell label right one_second">
					<label style="margin-top:-2px">Ближайшая станция метро, Ж/д станция, остановка</label>
				</div>
				<div class="cell one_second last_col">
					<div class="request_block">
						<input type="text" class="text" name="s[station]" value="'.$PageInfoAd['station_name'].'">
					</div>
				</div>
			</div>
			<div class="row">
				<div style="width:185px" class="cell label right one_second">
					<label style="margin-top:-2px">Удаленность от метро<br>(остановки)</label>
				</div>
				<div class="cell one_second last_col">
					<div class="request_block">
						<input type="text" class="text" name="s[dist_value]" value="'.$PageInfoAd['dist_value'].'">
					</div>
				</div>
			</div>
			<div class="row newbuildings flat">
				<div style="width:185px" class="cell label right one_second">
					<label>&nbsp;</label>
				</div>
				<div class="cell one_second last_col">
					<div class="request_block">
						<a class="problem" href="javascript:void(0)">Проблемы с вводом адреса?</a>
					</div>
				</div>
			</div>';
		}
		else if(isset($type_commer) && ($type_commer==2 || $type_commer==4 || $type_commer==6 || $type_commer==8)){
			echo '<div class="row newbuildings flat">
				<div style="width:185px" class="cell label right one_second">
					<label>Название комплекса</label>
				</div>
				<div class="cell one_second last_col">
					<div class="request_block">
						<input autocomplete="off" type="text" class="text" name="s[name]"'.$name.'>
					</div>
				</div>
			</div>';
			echo '<div class="row">
				<div style="width:185px" class="cell label right one_second">
					<label>Адрес<b>*</b></label>
				</div>
				<div class="cell required one_second last_col">
					<div style="width:325px" class="request_block">
						<input type="text" class="text" name="s[address]" value="'.$PageInfoAd['address'].'">
						<div class="search_request_block">
							<div class="scrollbar-inner">
								<ul></ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div style="width:185px" class="cell label right one_second">
					<label style="margin-top:-2px">Ближайшая станция метро, Ж/д станция, остановка</label>
				</div>
				<div class="cell one_second last_col">
					<div class="request_block">
						<input type="text" class="text" name="s[station]" value="'.$PageInfoAd['station_name'].'">
					</div>
				</div>
			</div>
			<div class="row">
				<div style="width:185px" class="cell label right one_second">
					<label style="margin-top:-2px">Удаленность от метро<br>(остановки)</label>
				</div>
				<div class="cell one_second last_col">
					<div class="request_block">
						<input type="text" class="text" name="s[dist_value]" value="'.$PageInfoAd['dist_value'].'">
					</div>
				</div>
			</div>
			<div class="row newbuildings flat">
				<div style="width:185px" class="cell label right one_second">
					<label>&nbsp;</label>
				</div>
				<div class="cell one_second last_col">
					<div class="request_block">
						<a class="problem" href="javascript:void(0)">Проблемы с вводом адреса?</a>
					</div>
				</div>
			</div>';
		}
		else {
			echo '<div class="row newbuildings flat">
				<div class="cell label right one_second">
					<label style="margin-top:-2px">Название<br> комплекса</label>
				</div>
				<div class="cell one_second last_col">
					<div class="request_block">
						<input autocomplete="off" type="text" class="text" name="s[name]"'.$name.'>
					</div>
				</div>
			</div>';
			echo '<div class="row">
				<div class="cell label right one_second">
					<label>Адрес<b>*</b></label>
				</div>
				<div class="cell required one_second last_col">
					<div style="width:365px" class="request_block">
						<input type="text" class="text" name="s[address]" value="'.$PageInfoAd['address'].'">
						<div class="search_request_block">
							<div class="scrollbar-inner">
								<ul></ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row newbuildings flat">
				<div class="cell label right one_second">
					<label>&nbsp;</label>
				</div>
				<div class="cell one_second last_col">
					<div class="request_block">
						<a class="problem" href="javascript:void(0)">Проблемы с вводом адреса?</a>
					</div>
				</div>
			</div>';
		}
		
		echo $stationRow.'
	</div>
</div>';
?>