<?

$liftServiceValue = ' value="0"';
if(isset($PageInfoAd['lift_service'])){
	$liftServiceValue = ' value="'.$PageInfoAd['lift_service'].'"';
}

$liftValue = ' value="0"';
if(isset($PageInfoAd['lift'])){
	$liftValue = ' value="'.$PageInfoAd['lift'].'"';
}

$rightBlock = '<div class="right_block">
	<div class="table_form">
		<h2>Информация о доме</h2>
		<div class="line">
			<div style="width:296px" class="block">
				<div class="row">
					<div class="cell label right one_second">
						<label style="white-space:nowrap;margin-left:-3px">Год подстройки</label>
					</div>
					<div class="cell one_second last_col">
						<input type="text" class="text" name="s[year]"'.$year.'>
					</div>
				</div>
				<div class="row">
					<div class="cell label right one_second">
						<label>Тип дома</label>
					</div>
					<div class="cell one_second last_col">
						'.$type_house.'
					</div>
				</div>
				<div class="row">
					<div class="cell label right one_second"><label></label></div>
					<div class="cell one_second last_col"></div>
				</div>
				<div class="row">
					<div style="text-align:right;margin-top:-4px" class="cell label one_second">Лифт <br>грузовой</div>
					<div class="cell auto last_col">
						<div class="checkbox_block">
							<input type="hidden" name="s[lift_service]"'.$liftServiceValue.'>
							<div class="select_checkbox">
								<ul style="margin-right:-20px;">
									<li'.($PageInfoAd['lift_service']==0?' class="checked"':"").'><a style="padding:4px 7px" href="javascript:void(0)" data-id="0">Нет</a></li>
									<li'.($PageInfoAd['lift_service']==1?' class="checked"':"").'><a style="padding:4px 7px" href="javascript:void(0)" data-id="1">1</a></li>
									<li'.($PageInfoAd['lift_service']==2?' class="checked"':"").'><a style="padding:4px 7px" href="javascript:void(0)" data-id="2">2</a></li>
									<li'.($PageInfoAd['lift_service']==3?' class="checked"':"").'><a style="padding:4px 7px" href="javascript:void(0)" data-id="3">3</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div style="text-align:right;margin-top:-4px" class="cell label one_second">Лифт пассажирский</div>
					<div class="cell auto last_col">
						<div class="checkbox_block">
							<input type="hidden" name="s[lift]"'.$liftValue.'>
							<div class="select_checkbox">
								<ul style="margin-right:-20px;">
									<li'.($PageInfoAd['lift']==0?' class="checked"':"").'><a style="padding:4px 7px" href="javascript:void(0)" data-id="0">Нет</a></li>
									<li'.($PageInfoAd['lift']==1?' class="checked"':"").'><a style="padding:4px 7px" href="javascript:void(0)" data-id="1">1</a></li>
									<li'.($PageInfoAd['lift']==2?' class="checked"':"").'><a style="padding:4px 7px" href="javascript:void(0)" data-id="2">2</a></li>
									<li'.($PageInfoAd['lift']==3?' class="checked"':"").'><a style="padding:4px 7px" href="javascript:void(0)" data-id="3">3</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div style="margin-top:87px" class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$parking.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$rubbish.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$protected_area.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:200px" class="cell one_second last_col">
						'.$playground.'
					</div>
				</div>
			</div>
		</div>
		<div class="line">
			<div style="margin-left:50px;width:246px" class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$furniture.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$furniture_kitchen.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$animal.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$children.'
					</div>
				</div>
			</div>
			<div class="block">
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$tv.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$fridge.'
					</div>
				</div>
				<div style="margin-top:16px" class="row">
					<div style="width:auto" class="cell one_second last_col">
						'.$washer.'
					</div>
				</div>
			</div>
		</div>
	</div>
</div>';

/*
*  Интернет
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['internet'])){
	if($PageInfoAd['internet']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['internet'].'"';
	}
}
$internet = '<div style="float:left;margin-right:40px" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[internet]" value="0">
	<input type="hidden"'.$valueParams.' name="s[internet]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Интернет</span>
	</div>
</div>';

/*
*  Телефон
*/
$checked = '';
$valueParams = '';
$disabled = ' disabled="disabled"';
if(isset($PageInfoAd['phone'])){
	if($PageInfoAd['phone']){
		$disabled = '';
		$checked = ' checked';
		$valueParams = ' value="'.$PageInfoAd['phone'].'"';
	}
}
$phone = '<div style="float:left" class="checkbox_single'.$checked.'">
	<input type="hidden" name="s[phone]" value="0">
	<input type="hidden"'.$valueParams.' name="s[phone]"'.$disabled.'>
	<div class="container">
		<span class="check"></span><span class="label">Телефон</span>
	</div>
</div>';

$roomsValue = ' value="0"';
if(isset($PageInfoAd['rooms'])){
	$roomsValue = ' value="'.$PageInfoAd['rooms'].'"';
}

/*
*  Высота потолков
*/
$height_ceiling = '<div class="select_block count">';
$height_ceilings = '';
$nameParams = 'Не важно';
$valueInput = '';
$devs = mysql_query("
	SELECT *
	FROM ".$template."_type_house
	WHERE activation='1'
	ORDER BY num
");
if(mysql_num_rows($devs)>0){
	$n = 0;
	while($dev = mysql_fetch_assoc($devs)){
		$current = "";
		
		if($n==0 && !isset($PageInfoAd['height_ceiling'])){
			$height_ceilings .= '<li class="current"><a href="javascript:void(0)" data-id="">Не важно</a></li>';
		}
		if($PageInfoAd['height_ceiling']==$dev['id']){
			$current = ' class="current"';
			$nameParams = $dev['name'];
			$valueInput = ' value="'.$dev['id'].'"';
		}
		$height_ceilings .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$dev['id'].'">'.$dev['name'].'</a></li>';
		$n++;
	}
}
$height_ceiling .= '<input type="hidden" name="s[height_ceiling]"'.$valueInput.'>';
$height_ceiling .= '<div style="width:162px" class="choosed_block">'.$nameParams.'</div>';
$height_ceiling .= '<div class="scrollbar-inner">';
$height_ceiling .= '<ul>';
$height_ceiling .= $height_ceilings;
$height_ceiling .= '</ul>';
$height_ceiling .= '</div>';
$height_ceiling .= '</div>';

echo '<div class="params_block">
		<div class="left_block">
			<div class="table_form">
				<h2>Информация о квартире</h2>
				<div class="row">
					<div class="cell label right one_second">
						<label>Комнаты<b>*</b></label>
					</div>
					<div class="cell required one_second last_col">
						<div class="checkbox_block">
							<input type="hidden" name="s[rooms]"'.$roomsValue.'>
							<div class="select_checkbox">
								<ul>
									<li'.($PageInfoAd['rooms']==0? ' class="checked"': "").'><a href="javascript:void(0)" data-id="0"><i class="triangle-bottomleft studio"></i>Студия</a></li>
									<li'.($PageInfoAd['rooms']==1? ' class="checked"': "").'><a href="javascript:void(0)" data-id="1"><i class="triangle-bottomleft one"></i>1</a></li>
									<li'.($PageInfoAd['rooms']==2? ' class="checked"': "").'><a href="javascript:void(0)" data-id="2"><i class="triangle-bottomleft two"></i>2</a></li>
									<li'.($PageInfoAd['rooms']==3? ' class="checked"': "").'><a href="javascript:void(0)" data-id="3"><i class="triangle-bottomleft three"></i>3</a></li>
									<li'.($PageInfoAd['rooms']==4? ' class="checked"': "").'><a href="javascript:void(0)" data-id="4"><i class="triangle-bottomleft more"></i>4</a></li>
									<li'.($PageInfoAd['rooms']==5? ' class="checked"': "").'><a href="javascript:void(0)" data-id="5"><i class="triangle-bottomleft five"></i>5+</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="line">
					<div class="block">
						<div class="row">
							<div class="cell label right one_second">
								<label>Этаж<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="text" class="text" name="s[floor]"'.$floor.'>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label style="margin-top:0">Общая площадь<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="text" class="text" name="s[full_square]"'.$full_square.'>
								<div class="com">м<sup>2</sup></div>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Кухня<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="text" class="text" name="s[kitchen_square]"'.$kitchen_square.'">
								<div class="com">м<sup>2</sup></div>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label style="margin-top:-2px">Высота потолков</label>
							</div>
							<div class="cell one_second last_col">
								<input type="text" class="text" name="s[ceiling_height]"'.$ceiling_height.'>
							</div>
						</div>
					</div>
					<div class="block">
						<div class="row">
							<div class="cell label right one_second">
								<label style="margin-top:0">Этажей в доме<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="text" class="text" name="s[floors]"'.$floors.'>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label style="margin-top:0">Жилая площадь<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="text" class="text" name="s[live_square]"'.$live_square.'>
								<div class="com">м<sup>2</sup></div>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label style="margin-top:0">Площадь комнат</label>
							</div>
							<div class="cell one_second last_col">
								<input placeholder="Пример: 20+15-10" type="text" class="text" name="s[rooms_square]"'.$rooms_square.'>
								<div class="com">м<sup>2</sup></div>
							</div>
						</div>
					</div>
				</div>
										
				<div class="row">
					<div class="cell info full">
						<p>В поле "Площадь комнат" используйте знак + для обозначения смежных комнат и знак - для раздельных комнат</p>
					</div>
				</div>
				<div class="row">
					<div style="margin-top:5px" class="cell right label one_second">Балкон</div>
					<div class="cell auto last_col">
						'.$balcony.'
					</div>
				</div>
				<div class="row">
					<div style="margin-top:5px" class="cell right label one_second">Лоджия</div>
					<div class="cell auto last_col">
						'.$loggia.'
					</div>
				</div>
				<div class="row">
					<div style="margin-top:5px" class="cell right label one_second">Сан.узел</div>
					<div class="cell auto last_col">
						'.$wc.'
					</div>
				</div>
				<div class="row">
					<div style="margin-top:5px" class="cell right label one_second">&nbsp;</div>
					<div class="cell auto last_col">
						'.$internet.'
						'.$phone.'
					</div>
				</div>
			</div>
		</div>
		'.$rightBlock.'
	</div>';
	?>