<div id="center">
	<?
	$companies_block = '';
	$max_rows = 0;
	$titlePage = '';
	$res = mysql_query("
		SELECT h1 AS titlePage
		FROM ".$template."_types_company 
		WHERE id='".$nowCompaniesPage."' && activation=1
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$row = mysql_fetch_assoc($res);
		$titlePage = $row['titlePage'];
	}
	$res = mysql_query("
		SELECT SQL_CALC_FOUND_ROWS *
		FROM ".$template."_companies
		WHERE p_main='".$nowCompaniesPage."'
		ORDER BY num
	");
	if(mysql_num_rows($res)>0){
		$res2 = mysql_query("SELECT FOUND_ROWS()", $db);
		$max_rows = mysql_result($res2, 0);
		while($row = mysql_fetch_assoc($res)){
			$img = '';
			if(!empty($row['images'])){
				$ex_image = explode(',',$row['images']);
				$img = '<div class="image">
					<img src="/admin_2/uploads/'.$ex_image[0].'">
				</div>';
			}
			$phone_one = '';
			$phone_two = '';
			$site = '';
			if(!empty($row['phone_one'])){
				$phone_one = '<div class="row"><span>Телефон:</span><strong>'.$row['phone_one'].'</strong></div>';
			}
			if(!empty($row['phone_two'])){
				$phone_two = '<div class="row"><span>&nbsp;</span><strong>'.$row['phone_two'].'</strong></div>';
			}
			if(!empty($row['site'])){
				$site = '<div class="row"><span>Сайт:</span><a target="_blank" href="http://'.$row['site'].'">'.$row['site'].'</a></div>';
			}
			
			$today = strtotime(date("Y-m-d")) - strtotime($row['date']);
			$days = $today/86400;
			$months = $days/30;
			$months = floor($months);
			$reg_date = '';
			if($months<12){
				if($months>=1){
					if($months==1){
						$reg_date = '<strong>1 месяц</strong> на сайте';
					}
					else if($months==2 || $months==3 || $months==4){
						$reg_date = '<strong>'.$months.' месяца</strong> на сайте';
					}
					else {
						$reg_date = '<strong>'.$months.' месяцев</strong> на сайте';
					}
				}
				else {
					$reg_date = '<strong>меньше месяца</strong> на сайте';
				}
			}
			else if($months==12) {
				$reg_date = 'На сайте 1 год';
			}
			else {
				$year = floor($months/12);
				$months = $months - ($year * 12);
				if($months==0){
					$month = '';
				}
				else if($months==1){
					$month = ' и 1 месяц';
				}
				else if($months==2 || $months==3 || $months==4){
					$month = ' и '.$months.' месяца';
				}
				else {
					$month = ' и '.$months.' месяцев';
				}
				if($year==1){
					$year = '1 год';
				}
				else if($year==2 || $year==3 || $year==4){
					$year = $year.' года';
				}
				else {
					$year = $year.' лет';
				}
				$reg_date = '<strong>'.$year.$month.'</strong> на сайте';
			}
			
			$short_text = '';
			if(!empty($row['short_text'])){
				$short_text = '<div class="short_text">'.$row['short_text'].'</div>';
			}
			
			$full_text = '';
			$text = htmlspecialchars_decode($row['text']);
			if(!empty($text)){
				$full_text = '<div class="text_full">
					<div class="text">'.htmlspecialchars_decode($row['text']).'</div>
					<div class="more_read"><a href="javascript:void(0)">Читать всё</a></div>
				</div>';
			}
			$companies_block .= '<div id="id_'.$row['id'].'" class="item">
				<div class="container_block">
					<div class="main_info_block">
						<div class="name">'.$row['name'].'</div>
						'.$short_text.'
					</div>
					<div class="contacts">
						<div class="reg_time">'.$reg_date.'</div>
						'.$phone_one.'
						'.$phone_two.'
						'.$site.'
					</div>
				</div>
				<div class="inform_block">
					'.$img.'
					'.$full_text.'
				</div>
			</div>';
		}
	}
	else {
		$companies_block = '<h3>Нет компаний в базе</h3>';
	}
	?>
	<div class="title_pages">Компании: <?=$titlePage?></div>
	<div class="sort_offers">
		<div class="count_offers">Всего<span><?=$max_rows?></span> компаний: <a href="#">Санкт-Петербург</a></div>
		<div class="sort_block">
			<div class="label_select">Сортировать:</div>
			<div class="select_block sort">
				<input type="hidden" name="sort">
				<?
				$countOffersValue = ' class="current"';
				$nameValue = '';
				$nameSort = 'По кол-ву объектов';
				if($valueSortName=='count'){
					$nameSort = 'По кол-ву объектов';
					$countOffersValue = ' class="current"';
				}
				if($valueSortName=='name'){
					$nameSort = 'По имени';
					$nameValue = ' class="current"';
					$countOffersValue = '';
				}
				?>
				<div style="width:160px" class="choosed_block"><?=$nameSort?></div>
				<div class="scrollbar-inner">
					<ul>
						<li<?=$countOffersValue?>><a href="<?=$linkSortCounts?>" data-id="1">По кол-ву объектов</a></li>
						<li<?=$nameValue?>><a href="<?=$linkSortNames?>" data-id="2">По имени</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div id="result_search" class="company_block">
		<div class="menu_block">
			<div class="menu_gray">
				<?
				$res = mysql_query("
					SELECT *
					FROM ".$template."_types_company
					WHERE activation=1
					ORDER BY num
				");
				if(mysql_num_rows($res)>0){
					echo '<ul>';
					while($row = mysql_fetch_assoc($res)){
						$current = '';
						if($row['link']==$params[1] && count($params)==2 || count($params)==1 && $row['num']==1){
							$current = ' class="current"';
						}
						if($row['num']==1){
							echo '<li'.$current.'><a href="/company">'.$row['name'].'</a></li>';
						}
						else {
							echo '<li'.$current.'><a href="/company/'.$row['link'].'">'.$row['name'].'</a></li>';
						}
					}
					echo '</ul>';
				}
				?>
			</div>
		<?
		$res = mysql_query("
			SELECT c.*,
			(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=c.id && estate='jk') AS images,
			(SELECT MIN(price) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS min_price,
			(SELECT MAX(price) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS max_price,
			(SELECT MIN(full_square) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS min_square,
			(SELECT MAX(full_square) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS max_square,
			(SELECT COUNT(*) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS count_estate,
			(SELECT name FROM ".$template."_developers WHERE activation='1' && id=c.developer) AS name_developer
			FROM ".$template."_m_catalogue_left AS c
			WHERE c.activation='1'
			ORDER BY RAND()
			LIMIT 2
		") or die(mysql_error());
		if(mysql_num_rows($res)>0){
			echo '<div class="our_offers">';
			echo '<div class="centers_block">';
			echo '<h3>Возможно вам понравится</h3>';
			echo '<div class="offers_list">';
			while($row = mysql_fetch_assoc($res)){
				$ex_image = explode(',',$row['images']);
				$image = '<img src="/admin_2/uploads/'.$ex_image[0].'">';
				$link = '/newbuilding/'.$row['id'];
				$exploitation = explode('_',$row['exploitation']);
				$deadline = '';
				if(empty($row['min_square']) || empty($row['max_square'])){
					$square_meter_min = 0;
					$square_meter_max = 0;
				}
				else {
					$square_meter_min = ceil($row['min_price']/$row['min_square']);
					$square_meter_max = ceil($row['max_price']/$row['max_square']);
				}
				if(count($exploitation)>0){
					if(count($exploitation)>1){
						if(!empty($exploitation[0]) && !empty($exploitation[1])){
							$deadline = '<div class="deadline">Срок сдачи: <span>'.$exploitation[1].' кв. '.$exploitation[0].'</span></div>';
						}
					}
					else {
						if(!empty($exploitation[0])){
							$deadline = '<div class="deadline">Срок сдачи: <span>'.$exploitation[0].'</span></div>';
						}
					}
				}
				echo '<div class="item">
					<div class="image">
						<a target="_blank" href="'.$link.'">'.$image.'</a>
						<div class="type_name">'.$row['name_developer'].'</div>
						'.$deadline.'
					</div>
					<div class="body">
						<div class="location">
							<div class="title"><a target="_blank" href="'.$link.'">'.$row['name'].'</a></div>
							<div class="street">'.$row['address'].'</div>
						</div>
						<div class="cost_block">
							<div class="price">от <strong>'.price_cell($row['min_price'],0).' руб.</strong></div>
							<div class="square">от <strong>'.price_cell($square_meter_min,0).' руб/м<sup>2</sup></strong></div>
							<div class="count_offers"><a target="_blank" href="'.$link.'">'.$row['count_estate'].' в продаже</a></div>
						</div>
					</div>
				</div>';
			}
			echo '</div>';
			echo '</div>';
			echo '</div>';
		}
		?>
		</div>
		<div class="list_users">
			<?=$companies_block?>
		</div>
	</div>
	<!--<div style="line-height:1;margin-bottom:18px" class="pager">
		<ul>
			<li class="current"><span>1</span></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li class="next"><a title="Следующая" href="#">→</a></li>
			<li class="last"><a title="В конец" href="#">В конец</a></li>
		</ul>
	</div>-->
</div>