<div id="center">
	<div class="title_page new">
		<div class="location_card">
			<h1 class="inner_pages"><?=$PageInfo['h1']?></h1>
			<div class="address"><?=$PageInfo['address']?></div>
			<?
			if(!empty($PageInfo['station'])){
				$v = $PageInfo['dist_value'];
				if($v/1000>=1){
					$v = price_cell($v/1000,2).' км';
				}
				else {
					$v = price_cell($v,0).' м';
				}
				echo '<div class="metro">«'.$PageInfo['station_name'].'» / <strong>'.$v.'</strong></div>';
				// echo '<div class="metro">«'.$PageInfo['station_name'].'» / <strong>'.$PageInfo['distance'].'</strong></div>';
			}
			$exploitation = explode('_',$PageInfo['exploitation']);
			$deadline = '';
			if(!empty($PageInfo['exploitation'])){
				if(count($exploitation)>0){
					if(count($exploitation)>1){
						$deadline = '<span>'.$exploitation[1].' кв. '.$exploitation[0].'</span>';
					}
					else {
						$deadline = '<span>'.$exploitation[0].'</span>';
					}
				}
			}
			else {
				$deadline = '';
				$queues = mysql_query("
					SELECT commissioning,queue
					FROM ".$template."_queues
					WHERE p_main='".$PageInfo['id']."' && type_name='new'
					GROUP BY queue
				");
				if(mysql_num_rows($queues)>0){
					$deadline .= '<span>';
					$deadlineArray = array();
					while($queue = mysql_fetch_assoc($queues)){
						array_push($deadlineArray,$queue['queue'].' очередь - '.$queue['commissioning']);
					}
					for($q=0; $q<count($deadlineArray); $q++){
						$deadline .= implode(', ',$deadlineArray);
					}
					$deadline .= '</span>';
				}
			}
			
			$type_developer = '';
			$type_deadline = '';
			if(!empty($PageInfo['name_developer'])){
				$type_developer = '<div class="type">Застройщик: <a href="'.$link_developer.'">'.$PageInfo['name_developer'].'</a></div>';
			}
			if(!empty($deadline)){
				$type_deadline = '<div class="type">Срок сдачи: '.$deadline.'</div>';
			}
			
			$square_meter = ceil($PageInfo['price']/$PageInfo['full_square']);
			$link_developer = '/search?type=sell&estate=1&priceAll=all&developer='.$PageInfo['id_developer'];
			?>
		</div>
		<div class="price_card">
			<div class="prices">
				<div class="full_price"><strong><?=price_cell($PageInfo['price'],0)?></strong> руб.</div>
				<div class="meter_price"><?=price_cell($square_meter,0)?> м<sup>2</sup></div>
			</div>
			<div class="developer">
				<?=$type_developer?>
				<?=$type_deadline?>
			</div>
		</div>
	</div>
	<div class="separate"></div>
	<div class="card_estate" id="housing_estate">
		<div class="graph_info">
			<?

			$res = mysql_query("
				SELECT id
				FROM ".$template."_photo_catalogue
				WHERE type_cession='photoplan' && estate='cession' && p_main='".$PageInfo['id']."'
			");
			if(mysql_num_rows($res)==0){
				/*
				*  Ссылка на картинку
				*/
				$picUrl = $PageInfo['picUrl'];
				$ex_img = explode('/',$picUrl);
				$arrayImg = $picUrl;
				$arrayImgCut = $ex_img[count($ex_img)-1];
				$images = mysql_query("
					SELECT id
					FROM ".$template."_photo_catalogue
					WHERE link_market LIKE '%$arrayImgCut%' && p_main='".$PageInfo['id']."' && estate='cession'
				");
				if(mysql_num_rows($images)==0){
					$pic_weight = 8000;
					$pic_height = 8000;
					$save = true;
					$module = 'cession';
					$estate = 'cession';
					$id2 = $PageInfo['id'];
					$images2 = $arrayImg;
					
					$type = explode('.',$images2);
					$type = $type[count($type)-1];
					$type = 'jpg';
					
					$uploaddir  = $_SERVER['DOCUMENT_ROOT']."/admin_2/uploads/"; // Оригинал
				 
					$apend  = substr(md5(date('YmdHis').rand(100,1000)),0,20).".".$type;
					$apend0 = substr(md5(date('YmdHis').rand(100,1000)),0,20).".".$type;
					$apend1 = substr(md5(date('YmdHis').rand(100,1000)),0,20).".".$type;
					$apend2 = substr(md5(date('YmdHis').rand(100,1000)),0,20).".".$type;
					$apend3 = substr(md5(date('YmdHis').rand(100,1000)),0,20).".".$type;
					$apend4 = substr(md5(date('YmdHis').rand(100,1000)),0,20).".".$type;
					
					$uploadfile = "$uploaddir$apend"; // original
					$bigfile2 = "$uploaddir$apend0";
					$bigfile = "$uploaddir$apend1";
					$largefile = "$uploaddir$apend2";
					$middlefile = "$uploaddir$apend3";
					$minfile = "$uploaddir$apend4";
					
					@file_put_contents($uploadfile, file_get_contents($images2));
					if (file_exists($uploadfile) && ($size = getimagesize($uploadfile))){
						$size = getimagesize($uploadfile);
					
						if ($size[0] < $pic_weight && $size[1] < $pic_height){
							if($size[0] > $size[1]){
								resize($uploadfile,$bigfile2, $_IMG_MOD[$module]['big_redactor']['x'], 0,false);
								resize($uploadfile,$bigfile, $_IMG_MOD[$module]['big']['x'], 0,false);
								
								resize($bigfile,$largefile, 0, $_IMG_MOD[$module]['lar']['x'],false);
								crop($largefile,$largefile, array(0, 0, $_IMG_MOD[$module]['lar']['x'], $_IMG_MOD[$module]['lar']['y']),false,$_IMG_MOD[$module]['lar']['x'],$position = 'center',$type_img = 'H');
								
								resize($bigfile,$middlefile, 0, $_IMG_MOD[$module]['mid']['x'],false);
								crop($middlefile,$middlefile, array(0, 0, $_IMG_MOD[$module]['mid']['x'], $_IMG_MOD[$module]['mid']['y']),false,$_IMG_MOD[$module]['mid']['x'],$position = 'center',$type_img = 'H');
								
								resize($bigfile,$minfile, 0, $_IMG_MOD[$module]['min']['x'],false);
								crop($minfile,$minfile, array(0, 0, $_IMG_MOD[$module]['min']['x'], $_IMG_MOD[$module]['min']['y']),false,$_IMG_MOD[$module]['min']['x'],$position = 'center',$type_img = 'H');				
							}
							else {
								resize($uploadfile,$bigfile2, $_IMG_MOD[$module]['big_redactor']['x'], 0,false);
								resize($uploadfile,$bigfile, $_IMG_MOD[$module]['big']['x'], 0,false);

								resize($bigfile,$largefile, $_IMG_MOD[$module]['lar']['x'], 0,false);
								crop($largefile,$largefile, array(0, 0, $_IMG_MOD[$module]['lar']['x'], $_IMG_MOD[$module]['lar']['y']),false,$_IMG_MOD[$module]['lar']['x'],$position = 'center',$type_img = 'H');
								
								resize($bigfile,$middlefile, $_IMG_MOD[$module]['mid']['x'], 0,false);
								crop($middlefile,$middlefile, array(0, 0, $_IMG_MOD[$module]['mid']['x'], $_IMG_MOD[$module]['mid']['y']),false,$_IMG_MOD[$module]['mid']['x'],$position = 'center',$type_img = 'H');				
								
								resize($bigfile,$minfile, $_IMG_MOD[$module]['min']['x'], 0,false);
								crop($minfile,$minfile, array(0, 0, $_IMG_MOD[$module]['min']['x'], $_IMG_MOD[$module]['min']['y']),false,$_IMG_MOD[$module]['min']['x'],$position = 'center',$type_img = 'H');				
							}
							$action = 'clear';
							$minfile = explode("/",$minfile);
							$middlefile = explode("/",$middlefile);
							$largefile = explode("/",$largefile);
							$bigfile = explode("/",$bigfile);
							$bigfile2 = explode("/",$bigfile2);
							$uploadfile = explode("/",$uploadfile);
							
							$minfile = $minfile[count($minfile)-1];
							$middlefile = $middlefile[count($middlefile)-1];
							$largefile = $largefile[count($largefile)-1];
							$bigfile = $bigfile[count($bigfile)-1];
							$bigfile2 = $bigfile2[count($bigfile2)-1];
							$uploadfile = $uploadfile[count($uploadfile)-1];
							
							$arr2 = $minfile.",".$middlefile.",".$largefile.",".$bigfile.",".$bigfile2.",".$uploadfile;
							$num = 0;

							$coords = $_IMG_MOD[$module]['coords']['x1'].','.$_IMG_MOD[$module]['coords']['y1'].','.$_IMG_MOD[$module]['coords']['x2'].','.$_IMG_MOD[$module]['coords']['y2'].','.$_IMG_MOD[$module]['coords']['width'].','.$_IMG_MOD[$module]['coords']['height'];
							if(!empty($id2)){
								$rows = mysql_query("
									SELECT num 
									FROM ".$template."_photo_catalogue
									WHERE p_main='".$id2."' && estate='".$estate."'
									ORDER BY id DESC LIMIT 1
								") or die(mysql_error());
								if(mysql_num_rows($rows)>0){
									$num = mysql_fetch_assoc($rows);
									$num = $num['num'];
								}
							}
							$res = mysql_query ("
								INSERT INTO ".$template."_photo_catalogue (p_main,estate,images,num,date,activation,coords,link_market,cover) 
								VALUES('".$id2."','".$estate."','".$arr2."','".($num+1)."','".time()."','1','".$coords."','".$arrayImg."','1')
							") or die(mysql_error());
						}
					}
					else {
						unlink($uploadfile);
					}
				}
			}
			
			$res = mysql_query("
				SELECT *,
				(SELECT COUNT(id) FROM ".$template."_photo_catalogue WHERE estate='cession' && p_main='".$PageInfo['id']."') AS count_photo
				FROM ".$template."_photo_catalogue
				WHERE estate='cession' && p_main='".$PageInfo['id']."'
				ORDER BY cover DESC, num
			");
			if(mysql_num_rows($res)>0){
				echo '<div class="images left_mini">';
				$n = 1;
				$big_image = '';
				while($row = mysql_fetch_assoc($res)){
					$ex_images = explode(',',$row['images']);
					if($row['cover']==1){
						$max_rows = $row['count_photo'];
						$see_gallery = '';
						if($max_rows - 4 > 0){
							$max_rows = $max_rows - 4;
							if(substr($max_rows,-1,1)==1 && substr($max_rows,-2,2)!=11){
								$count_photo = '+'.$max_rows.' фотография';
							}
							else if(substr($max_rows,-1,1)==2 && substr($max_rows,-2,2)!=12 || substr($max_rows,-1,1)==3 && substr($max_rows,-2,2)!=13 || substr($max_rows,-1,1)==4 && substr($max_rows,-2,2)!=14){
								$count_photo = '+'.$max_rows.' фотографии';
							}
							else {
								$count_photo = '+'.$max_rows.' фотографий';
							}
							$see_gallery = '<div class="see_gallery">
								<a rel="prettyPhoto[img]" href="/admin_2/uploads/'.$ex_images[5].'"><span>'.$count_photo.'</span><i class="fa fa-search"></i></a><span>(смотреть всю галерею)</span>
							</div>';
						}
						$one = '';
						if(mysql_num_rows($res)==1){
							$one = ' one';
						}
						$big_image = '<div class="big_image">
							<a class="big'.$one.'" style="background-image:url(/admin_2/uploads/'.$ex_images[3].')" rel="prettyPhoto[img]" href="/admin_2/uploads/'.$ex_images[5].'"></a>
							'.$see_gallery.'
						</div>';
					}
					if(mysql_num_rows($res)==1){
						// if($n==2){
							// echo '<div class="mini_images">';
						// }
						// if($n>1){
							// $hidden = '';
							// if($n>4){
								// $hidden = ' hidden';
							// }
							// echo '<div class="img'.$hidden.'"><a rel="prettyPhoto[img]" href="/admin_2/uploads/'.$ex_images[5].'"><img src="/admin_2/uploads/'.$ex_images[1].'"></a></div>';
						// }
						// if(mysql_num_rows($res)==$n){
							// echo '</div>';
						// }
					}
					$n++;
				}
				echo $big_image;
				echo '</div>';
			}
			$floors = '';
			$type_house = '';
			$year_build = '';
			$queue = '';
			$ceiling_height = '';
			$lift = '';
			/*
			*  Кол-во этажей
			*/
			if(!empty($PageInfo['floors'])){
				if(!empty($PageInfo['floor'])){
					$floors = '<div class="row">
						<div class="cell label">Этаж:</div>
						<div class="cell">'.$PageInfo['floor'].' (из '.$PageInfo['floors'].')</div>
					</div>';
				}
			}
			/*
			*  Тип дома
			*/
			if(!empty($PageInfo['type_house'])){
				$r = mysql_query("
					SELECT name
					FROM ".$template."_type_house
					WHERE id='".$PageInfo['type_house']."'
				");
				if(mysql_num_rows($r)>0){
					$rw = mysql_fetch_assoc($r);
					$type_house = '<div class="row">
						<div class="cell label">Тип дома:</div>
						<div class="cell">'.$rw['name'].'</div>
					</div>';
				}
			}
			/*
			*  Этап строительства
			*/
			if(!empty($PageInfo['ready'])){
				$ready = '';
				$devs = mysql_query("
					SELECT *
					FROM ".$template."_ready
					WHERE activation='1' && id='".$PageInfo['ready']."'
				");
				if(mysql_num_rows($devs)>0){
					$dev = mysql_fetch_assoc($devs);
					$ready_build = '<div class="row">
						<div class="cell label">Этап строительства:</div>
						<div class="cell">'.$dev['name'].'</div>
					</div>';
				}
			}
			/*
			*  Корпус
			*/
			if(!empty($PageInfo['queue'])){
				$queue = '<div class="row">
					<div class="cell label">Корпус:</div>
					<div class="cell">'.$PageInfo['queue'].' корпус</div>
				</div>';
			}
			/*
			*  Отделка
			*/
			$finishing_value = '<span class="no">нет</span>';
			$finishing = '';
			if(!empty($PageInfo['finishing'])){
				$finishing_value = '<span class="ok">да</span>';
				$finishing = '<div class="row">
					<div class="cell label">Отделка:</div>
					<div class="cell places">'.$finishing_value.'</div>
				</div>';
			}
			/*
			*  Высота потолков
			*/
			if(!empty($PageInfo['ceiling_height'])){
				$ceiling_height = '<div class="row">
					<div class="cell label">Высота потолков:</div>
					<div class="cell">'.str_replace(".", ",", $PageInfo['ceiling_height']).' м</div>
				</div>';
			}
			/*
			*  Лифт
			*/
			$lift_value = '<span class="no">нет</span>';
			$lift = '';
			if(!empty($PageInfo['lift'])){
				$lift_value = '<span class="ok">да</span>';
				$lift = '<div class="row">
					<div class="cell label">Лифт:</div>
					<div class="cell places">'.$lift_value.'</div>
				</div>';
			}
			
			/*
			*  Агент текущей недвижимости
			*/
			$agent_this_estate = '';
			if(!empty($PageInfo['agent'])){
				$agents = mysql_query("
					SELECT *
					FROM ".$template."_users
					WHERE activation='1' && id='".$PageInfo['agent']."'
				");
				if(mysql_num_rows($agents)>0){
					$agent = mysql_fetch_assoc($agents);
					$skype = '';
					$phones = '';
					if(!empty($agent['skype'])){
						$skype = '<div class="skype"><i class="fa fa-skype"></i>Skype: <a href="skype:'.$agent['skype'].'?call">'.$agent['skype'].'</a></div>';
					}
					$phones .= '<div class="phones">';
					if(!empty($agent['phone'])){
						$phones .= '<div class="main_phone">+7 <strong>'.substr($agent['phone'],4,3).' '.substr($agent['phone'],9,3).'-'.substr($agent['phone'],13,2).''.substr($agent['phone'],16,2).'</strong></div>';
					}
					if(!empty($agent['add_phone'])){
						$phones .= '<div class="add_phone">+7 <strong>'.substr($agent['add_phone'],4,3).' '.substr($agent['add_phone'],9,3).'-'.substr($agent['add_phone'],13,2).''.substr($agent['add_phone'],16,2).'</strong></div>';
					}
					else {
						$phones .= '<div class="add_phone">+7 <strong>'.substr($_MAINSET['phone_clear'],3,3).' '.substr($_MAINSET['phone_clear'],7,3).'-'.substr($_MAINSET['phone_clear'],11,2).substr($_MAINSET['phone_clear'],14,2).'</strong></div>';
					}
					$phones .= '</div>';
					$agent_this_estate = '<div class="agency_block card">
						<div class="title">
							<div class="title_left">Ваш агент:</div>
							<div class="title_right"><strong>'.$agent['name'].'</strong></div>
						</div>
						<div class="info_agent">
							<div class="send_request">
								<a onclick="return sendRequest(this)" href="javascript:void(0)">Оставить заявку</a>
							</div>
							'.$skype.'
							'.$phones.'
						</div>
					</div>';
				}
				else {
					$phones = '<div class="phones"><div class="main_phone">+7 <strong>'.substr($_MAINSET['phone_clear'],3,3).' '.substr($_MAINSET['phone_clear'],7,3).'-'.substr($_MAINSET['phone_clear'],11,2).substr($_MAINSET['phone_clear'],14,2).'</strong></div></div>';
					$agent_this_estate = '<div class="agency_block card">
						<div class="title">
							<div class="title_left">Оставьте заявку и мы с Вами свяжемся</div>
						</div>
						<div class="info_agent">
							<div class="send_request">
								<a onclick="return sendRequest(this)" href="javascript:void(0)">Оставить заявку</a>
							</div>
							'.$phones.'
						</div>
					</div>';
				}
			}
			else {
				$agents2 = mysql_query("
					SELECT u.*
					FROM ".$template."_users AS u
					LEFT JOIN ".$template."_m_catalogue_left AS a
					ON a.id='".$PageInfo['p_main']."'
					WHERE u.activation='1' && u.id=a.agent
				");
				if(mysql_num_rows($agents2)>0){
					$agent2 = mysql_fetch_assoc($agents2);
					$skype = '';
					$phones = '';
					if(!empty($agent2['skype'])){
						$skype = '<div class="skype"><i class="fa fa-skype"></i>Skype: <a href="skype:'.$agent2['skype'].'?call">'.$agent2['skype'].'</a></div>';
					}
					$phones .= '<div class="phones">';
					if(!empty($agent2['phone'])){
						$phones .= '<div class="main_phone">+7 <strong>'.substr($agent2['phone'],4,3).' '.substr($agent2['phone'],9,3).'-'.substr($agent2['phone'],13,2).''.substr($agent2['phone'],16,2).'</strong></div>';
					}
					if(!empty($agent2['add_phone'])){
						$phones .= '<div class="add_phone">+7 <strong>'.substr($agent2['add_phone'],4,3).' '.substr($agent2['add_phone'],9,3).'-'.substr($agent2['add_phone'],13,2).''.substr($agent2['add_phone'],16,2).'</strong></div>';
					}
					else {
						$phones .= '<div class="add_phone">+7 <strong>'.substr($_MAINSET['phone_clear'],3,3).' '.substr($_MAINSET['phone_clear'],7,3).'-'.substr($_MAINSET['phone_clear'],11,2).substr($_MAINSET['phone_clear'],14,2).'</strong></div>';
					}
					$phones .= '</div>';
					$agent_this_estate = '<div class="agency_block card">
						<div class="title">
							<div class="title_left">Ваш агент:</div>
							<div class="title_right"><strong>'.$agent2['name'].'</strong></div>
						</div>
						<div class="info_agent">
							<div class="send_request">
								<a onclick="return sendRequest(this)" href="javascript:void(0)">Оставить заявку</a>
							</div>
							'.$skype.'
							'.$phones.'
						</div>
					</div>';
				}
				else {
					$phones = '<div class="phones"><div class="main_phone">+7 <strong>'.substr($_MAINSET['phone_clear'],3,3).' '.substr($_MAINSET['phone_clear'],7,3).'-'.substr($_MAINSET['phone_clear'],11,2).substr($_MAINSET['phone_clear'],14,2).'</strong></div></div>';
					$agent_this_estate = '<div class="agency_block card">
						<div class="title">
							<div class="title_left">Оставьте заявку и мы с Вами свяжемся</div>
						</div>
						<div class="info_agent">
							<div class="send_request">
								<a onclick="return sendRequest(this)" href="javascript:void(0)">Оставить заявку</a>
							</div>
							'.$phones.'
						</div>
					</div>';
				}
			}
			
			echo '<div class="info_housing">
				'.$floors.'
				'.$type_house.'
				'.$ready_build.'
				'.$queue.'
				'.$ceiling_height.'
				'.$finishing.'
				'.$lift.'
			</div>';
			echo $agent_this_estate;
			?>
		</div>			
		<div class="text_info icons">
			<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
			<style>
				#map {
					width: 572px; height: 295px; padding: 0; margin: 0;
				}
			</style>
			<?
			$coordsYandex = '';
			$ex_coords = array('30.315868','59.939095');
			if(empty($PageInfo['coords'])){
				$params2 = array(
					'geocode' => 'Санкт-Петербург, '.$PageInfo['address'],
					'format'  => 'json',
					'results' => 1,
					'key'     => 'AKDIIFYBAAAAqIYtRgIAZYb8P32hIxnbRRSe9CpggONv4PEAAAAAAAAAAADu6wMZJIEJ0SDd0mRe33ag1JRdNA==',
				);
				$response = json_decode(@file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params2)));
				
				if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0){
					$coordsYandex = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
				}
			}
			else {
				$coordsYandex = $PageInfo['coords'];
			}
			if(!empty($coordsYandex)){
				$ex_coords = explode(' ',$coordsYandex);
			}
			?>
			
			<script>
				ymaps.ready(function () {
					var myMap = new ymaps.Map('map', {
							center: [<?=$ex_coords[1]?>,<?=$ex_coords[0]?>],
							zoom: 13
						}, {
							searchControlProvider: 'yandex#search'
						}),
						myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
							hintContent: '<?=$PageInfo['name']?>',
							balloonContent: '<?=$PageInfo['address']?>'
						}, {
							// Опции.
							// Необходимо указать данный тип макета.
							iconLayout: 'default#image',
							// Своё изображение иконки метки.
							iconImageHref: '/images/<?=$_ICONS_MAPS[$PageInfo['rooms']]['card']?>',
							// Размеры метки.
							iconImageSize: [52, 44],
							// Смещение левого верхнего угла иконки относительно
							// её "ножки" (точки привязки).
							iconImageOffset: [-3, -42]
						});

					myMap.geoObjects.add(myPlacemark);
					var myRouter = ymaps.route([
					  'м. <?=$PageInfo['station_name']?>', {
						type: "viaPoint",                   
						point: "<?=$PageInfo['address']?>"
					  },
					], {
					  mapStateAutoApply: true 
					});
					
					myRouter.then(function(route) {
						/* Задание контента меток в начальной и 
						   конечной точках */
						var points = route.getWayPoints();
						points.options.set('preset', 'islands#blackStretchyIcon');
						points.get(0).properties.set("iconContent", 'м. <?=$PageInfo['station_name']?>');
						points.get(1).properties.set("iconContent", '<?=$PageInfo['h1_jk']?>');
						// Добавление маршрута на карту
						myMap.geoObjects.add(route);
						var routeLength = route.getHumanLength();
						var routeLength2 = route.getLength();
						$('<div class="full_route">Расстояние от <span>Санкт-Петербург, м. <?=$PageInfo['station_name']?></span> до <span><?=htmlspecialchars_decode($PageInfo['h1_jk'])?></span>: <strong>'+routeLength+'</strong></div>').insertAfter('#map');
						// alert(routeLength2);
					  },
					  // Обработка ошибки
					  function (error) {
						alert("Возникла ошибка: " + error.message);
					  }
					)
				});
			</script>
			<div class="map"><div id="map"></div></div>
			<div class="info_housing">
				<div class="row">
					<div class="cell label">Общая площадь:</div>
					<div class="cell"><strong><?=$PageInfo['full_square']?> м<sup>2</sup></strong></div>
				</div>
				<?if(!empty($PageInfo['live_square'])){?>
				<div class="row">
					<div class="cell label">Жилая площадь:</div>
					<div class="cell"><strong><?=$PageInfo['live_square']?> м<sup>2</sup></strong></div>
				</div>
				<?}?>
				<?if(!empty($PageInfo['rooms_square'])){?>
				<div class="row">
					<div class="cell label">Площадь комнат:</div>
					<div class="cell"><?=$PageInfo['rooms_square']?> м<sup>2</sup></div>
				</div>
				<?}?>
				<?if(!empty($PageInfo['kitchen_square'])){?>
				<div class="row">
					<div class="cell label">Площадь кухни:</div>
					<div class="cell"><?=$PageInfo['kitchen_square']?> м<sup>2</sup></div>
				</div>
				<?}?>
				<?if(!empty($PageInfo['wc'])){?>
				<div class="row">
					<div class="cell label">Санузел:</div>
					<div class="cell"><?=$_TYPE_WC_ADMIN[$PageInfo['wc']]?></div>
				</div>
				<?}?>
				<?
					$balcony = '<span class="no">'.$_TYPE_BALCONY_ADMIN[$PageInfo['balcony']].'</span>';
					if(!empty($PageInfo['balcony'])){
						$balcony = '<span class="ok">'.$_TYPE_BALCONY_ADMIN[$PageInfo['balcony']].'</span>';
						echo '<div class="row">
							<div class="cell label">Балкон:</div>
							<div class="cell places">'.$balcony.'</div>
						</div>';
					}
				?>
				<?
				$phone = '<span class="no">нет</span>';
				if(!empty($PageInfo['phone'])){
					$phone = '<span class="ok">есть</span>';
					echo '<div class="row">
						<div class="cell label">Телефон:</div>
						<div class="cell places">'.$phone.'</div>
					</div>';
				}
				?>
				<?
				if(!empty($PageInfo['short_text'])){
					echo '<div class="row">
						<div class="cell label add">Дополнительно:</div>
						<div class="cell">'.nl2br($PageInfo['short_text']).'</div>
					</div>';
				}
				?>
			</div>
			<?
			if(!empty($PageInfo['text'])){
				echo '<div class="text">'.$textPage.'</div>';
				echo '<div class="more_read"><a href="javascript:void(0)">Читать всё</a></div>';
			}
			?>
		</div>
	</div>
	<?
	$item_little = '';
	$item_big = '';
	$res = mysql_query("
		SELECT n.*,c.name AS name_jk,c.address,
		(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=n.id && estate='cession') AS images,
		(SELECT name FROM ".$template."_developers WHERE activation='1' && id=c.developer) AS name_developer
		FROM ".$template."_m_catalogue_left AS c
		LEFT JOIN ".$template."_cessions AS n
		ON n.p_main=c.id && n.activation='1'
		WHERE c.activation='1' && n.id!='".$PageInfo['id']."' && n.price<'".$PageInfo['price']."' && c.id='".$PageInfo['id_jk']."' && n.rooms=IF((SELECT COUNT(*) FROM ".$template."_cessions WHERE activation='1' && p_main='".$PageInfo['id_jk']."' && rooms='".$PageInfo['rooms']."' && price<'".$PageInfo['price']."')>0, '".$PageInfo['rooms']."', '".($PageInfo['rooms']-1)."')
		ORDER BY RAND()
		LIMIT 1
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$row = mysql_fetch_assoc($res);
		$link = '/cession/'.$PageInfo['id_jk'].'/flat/'.$row['id'];
		$image = '<span>Нет фото</span>';
		if(!empty($row['images'])){
			$ex_image = explode(',',$row['images']);
			$image = '<img src="/admin_2/uploads/'.$ex_image[0].'">';
		}
		$square_meter = ceil($row['price']/$row['full_square']);
		$name_title = $row['rooms'].'-комн. кв';
		if($row['rooms']==0){
			$name_title = 'Студия';
		}
		$floor_name = '';
		if(!empty($row['floor'])){
			$floor_name = '<div class="type">этаж '.$row['floor'].'</div>';
		}
		$item_little = '<div class="item little">
			<div class="image">
				<a href="'.$link.'">'.$image.'</a>
			</div>
			<div class="body">
				<div class="location">
					<div class="title"><a href="'.$link.'">'.$name_title.' <strong>'.str_replace(".", ",", price_cell($row['full_square'],1)).' м<sup>2</sup></strong></a></div>
					<div class="comm">'.$row['name_jk'].'</div>
					<div class="price"><strong>'.price_cell($row['price'],0).' руб.</strong></div>
					<div class="square">'.price_cell($square_meter,0).' руб/м<sup>2</sup></div>
					'.$floor_name.'
				</div>
			</div>
		</div>';
	}
	$res = mysql_query("
		SELECT n.*,c.name AS name_jk,c.address,
		(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=n.id && estate='cession') AS images,
		(SELECT name FROM ".$template."_developers WHERE activation='1' && id=c.developer) AS name_developer
		FROM ".$template."_m_catalogue_left AS c
		LEFT JOIN ".$template."_cessions AS n
		ON n.p_main=c.id && n.activation='1'
		WHERE c.activation='1' && c.id='".$PageInfo['id_jk']."' && n.id!='".$PageInfo['id']."' && n.rooms='".$PageInfo['rooms']."' && n.full_square>'".$PageInfo['full_square']."' && n.id!='".$PageInfo['id']."'
		ORDER BY full_square
		LIMIT 1
	") or die(mysql_error());
	$meterBig = '';
	if(mysql_num_rows($res)>0){
		$row = mysql_fetch_assoc($res);
		$link = '/cession/'.$PageInfo['id_jk'].'/flat/'.$row['id'];
		$image = '<span>Нет фото</span>';
		if(!empty($row['images'])){
			$ex_image = explode(',',$row['images']);
			$image = '<img src="/admin_2/uploads/'.$ex_image[0].'">';
		}
		$meterBig = price_cell($row['full_square'] - $PageInfo['full_square'],1);
		$square_meter = ceil($row['price']/$row['full_square']);
		$name_title = $row['rooms'].'-комн. квартира';
		if($row['rooms']==0){
			$name_title = 'Студия';
		}
		$floor_name = '';
		if(!empty($row['floor'])){
			$floor_name = '<div class="count_offers">этаж '.$row['floor'].'</div>';
		}
		$item_big = '<div class="item big">
			<div class="image">
				<a href="'.$link.'">'.$image.'</a>
			</div>
			<div class="body">
				<div class="location">
					<div class="title"><a href="'.$link.'">'.$name_title.' <strong>'.str_replace(".", ",", price_cell($row['full_square'],1)).' м<sup>2</sup></strong></a></div>
					<div class="comm">'.$row['name_jk'].'</div>
					<div class="street">'.$row['address'].'</div>
					<div class="type_name">'.$row['name_developer'].'</div>
				</div>
				<div class="cost_block">
					<div class="price"><strong>'.price_cell($row['price'],0).' руб.</strong></div>
					<div class="square">'.price_cell($square_meter,0).' руб/м<sup>2</sup></div>
					'.$floor_name.'
				</div>
			</div>
		</div>';
	}
	echo '<div class="our_offers">';
	echo '<div class="second_title">';
			if(!empty($item_little)){
				echo '<h3 class="little">Ищите дешевле?</h3>';
			}
			if(!empty($meterBig)){
				echo '<h3><span class="baloon"></span>На '.$meterBig.' м<sup>2</sup> больше</h3>';
			}
		echo '</div>';
		if(!empty($item_little) || !empty($item_big)){
			echo '<div class="offers_list">';
			echo $item_little;
			echo $item_big;
			echo '</div>';
		}
	echo '</div>';
	?>
</div>	