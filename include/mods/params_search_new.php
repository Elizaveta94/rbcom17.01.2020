<?
$title_search_page = '';
$orderByNewBuilding = 'rooms';
$LOAD_PAGE = 12;
// echo '<pre>';
// print_r($searchParams);
// echo '</pre>';
$roomsAddArray = array();
$roomsAddArrayFlats = array();
$queueAddArray = array();
$queueAddArrayFlats = array();
$roomsAddParam = '';
$roomsAddParamFlats = '';
$queueAddParam = '';
$queueAddParamFlats = '';


if(isset($arrayLinkSearch2['rooms'])){
	$ex_roomsParam = explode(',',$arrayLinkSearch2['rooms']);
	for($c=0; $c<count($ex_roomsParam); $c++){
		if($ex_roomsParam[$c]==4){
			array_push($roomsAddArrayFlats,"n.rooms>='".$ex_roomsParam[$c]."'");
			array_push($roomsAddArray,"rooms>='".$ex_roomsParam[$c]."'");
		}
		else {
			array_push($roomsAddArrayFlats,"n.rooms='".$ex_roomsParam[$c]."'");
			array_push($roomsAddArray,"rooms='".$ex_roomsParam[$c]."'");
		}
	}
	if(count($roomsAddArray)>0){
		if(count($roomsAddArray)==1){
			$roomsAddParam = " && ".$roomsAddArray[0];
		}
		else {
			$roomsAddParam = " && (".implode(' || ',$roomsAddArray).")";
		}
	}
	if(count($roomsAddArrayFlats)>0){
		if(count($roomsAddArrayFlats)==1){
			$roomsAddParamFlats = " && ".$roomsAddArrayFlats[0];
		}
		else {
			$roomsAddParamFlats = " && (".implode(' || ',$roomsAddArrayFlats).")";
		}
	}
}
if(isset($arrayLinkSearch2['housing'])){
	$ex_queueParam = explode(',',$arrayLinkSearch2['housing']);
	for($c=0; $c<count($ex_queueParam); $c++){
		array_push($queueAddArrayFlats,"n.queue='".$ex_queueParam[$c]."'");
		array_push($queueAddArray,"queue='".$ex_queueParam[$c]."'");
	}
	if(count($queueAddArray)>0){
		if(count($queueAddArray)==1){
			$queueAddParam = " && ".$queueAddArray[0];
		}
		else {
			$queueAddParam = " && (".implode(' || ',$queueAddArray).")";
		}
	}
	if(count($queueAddArrayFlats)>0){
		if(count($queueAddArrayFlats)==1){
			$queueAddParamFlats = " && ".$queueAddArrayFlats[0];
		}
		else {
			$queueAddParamFlats = " && (".implode(' || ',$queueAddArrayFlats).")";
		}
	}
}
$searchParams = '';
if($arrayLinkSearch2['parent']){ // ЖК
	if(!empty($arrayLinkSearch2['parent'])){
		$rooms_array = array();
		$ex_values = explode(',',$arrayLinkSearch2['parent']);
		if(count($ex_values)>1){
			for($c=0; $c<count($ex_values); $c++){
				array_push($rooms_array,"f.p_main='".$ex_values[$c]."'");
			}
			$searchParams .= " && (".implode(" || ",$rooms_array).")";
		}
		else {
			$searchParams .= " && f.p_main='".$ex_values[0]."'";
		}
	}
}
if($arrayLinkSearch2['housing']){ // Корпус
	if(!empty($arrayLinkSearch2['housing'])){
		$rooms_array = array();
		$ex_values = explode(',',$arrayLinkSearch2['housing']);
		if(count($ex_values)>1){
			for($c=0; $c<count($ex_values); $c++){
				array_push($rooms_array,"f.queue='".$ex_values[$c]."'");
			}
			$searchParams .= " && (".implode(" || ",$rooms_array).")";
		}
		else {
			$searchParams .= " && f.queue='".$ex_values[0]."'";
		}
	}
}

$searchParams = "f.activation='1'".$searchParams;
// echo $searchParams;

$res = mysql_query("
	SELECT SQL_CALC_FOUND_ROWS e.*,
	(SELECT title FROM ".$template."_stations WHERE activation='1' && id=e.station) AS station_name,
	(SELECT name FROM ".$template."_developers WHERE activation='1' && id=e.developer) AS name_developer,
	(SELECT COUNT(*) FROM ".$template."_new_flats WHERE activation='1' && p_main=e.id) AS all_count_flats,
	(SELECT name FROM ".$template."_location WHERE activation='1' && id=e.location) AS location_name
	FROM ".$template."_m_catalogue_left AS e
	LEFT JOIN ".$template."_new_flats AS f
	ON f.p_main=e.id
	WHERE ".$searchParams."
	GROUP BY e.id
	ORDER BY f.".$orderByNewBuilding."
");
$requestSearch = '';
$coordsArray = array();
$coordsArray2 = array();
$estateArray = array();
$station_name = '';
$address_name = '';
if(mysql_num_rows($res)>0){
	$row = mysql_fetch_assoc($res);
	
	$station_name = $row['station_name'];
	$address_name = $row['address'];
	/*
	*  Поиск минимальной общей площади
	*/
	$min_full_square = 1;
	$min_full_squares = mysql_query("
		SELECT MIN(full_square) AS min_square
		FROM ".$template."_new_flats 
		WHERE activation='1' && p_main='".$row['id']."'".$roomsAddParam.$queueAddParam."
	");
	if(mysql_num_rows($min_full_squares)>0){
		$min_full_square = mysql_fetch_assoc($min_full_squares);
		$min_full_square = $min_full_square['min_square'];
	}
	
	/*
	*  Поиск максимальной общей площади
	*/
	$max_full_square = 1;
	$max_full_squares = mysql_query("
		SELECT MAX(full_square) AS max_square
		FROM ".$template."_new_flats 
		WHERE activation='1' && p_main='".$row['id']."'".$roomsAddParam.$queueAddParam."
	");
	if(mysql_num_rows($max_full_squares)>0){
		$max_full_square = mysql_fetch_assoc($max_full_squares);
		$max_full_square = $max_full_square['max_square'];
	}
	
	/*
	*  Поиск минимальной стоимости
	*/
	$min_price = 0;
	$min_prices = mysql_query("
		SELECT MIN(price) AS min_price
		FROM ".$template."_new_flats
		WHERE activation='1' && p_main='".$row['id']."'".$roomsAddParam.$queueAddParam."
	");
	if(mysql_num_rows($min_prices)>0){
		$min_price = mysql_fetch_assoc($min_prices);
		$min_price = $min_price['min_price'];
	}
	
	/*
	*  Поиск максимальной стоимости
	*/
	$max_price = 0;
	$max_prices = mysql_query("
		SELECT MAX(price) AS max_price
		FROM ".$template."_new_flats 
		WHERE activation='1' && p_main='".$row['id']."'".$roomsAddParam.$queueAddParam."
	");
	if(mysql_num_rows($max_prices)>0){
		$max_price = mysql_fetch_assoc($max_prices);
		$max_price = $max_price['max_price'];
	}

	$link = '/newbuilding/'.$row['id'];
	$link_developer = '/search?type=sell&estate=1&priceAll=all&developer='.$row['developer'];
	
	// $total_estate = '<div class="total_estate">Всего <strong>'.$row['name'].'</strong>: <a href="/newbuilding?estate=1&parent='.$row['id'].'">'.$row['all_count_flats'].' квартир в продаже</a></div>';
	
	$total_estate = '<div class="total_estate">Всего <strong>'.htmlspecialchars_decode($row['name']).'</strong>: '.$row['all_count_flats'].' квартир в продаже</div>';

	$deadline = '';
	if(!empty($row['exploitation'])){
		$exploitation = explode('_',$row['exploitation']);
		if(count($exploitation)>0){
			if(count($exploitation)>1){
				$deadline = '<span>'.$exploitation[1].' кв. '.$exploitation[0].'</span>';
			}
			else {
				$deadline = '<span>'.$exploitation[0].'</span>';
			}
		}
	}
	else {
		$queues = mysql_query("
			SELECT commissioning,queue
			FROM ".$template."_queues
			WHERE p_main='".$row['id']."' && type_name='new'
			GROUP BY queue
		");
		if(mysql_num_rows($queues)>0){
			$deadline .= '<span>';
			$deadlineArray = array();
			while($queue = mysql_fetch_assoc($queues)){
				array_push($deadlineArray,$queue['queue'].' очередь - '.$queue['commissioning']);
			}
			for($q=0; $q<count($deadlineArray); $q++){
				$deadline .= implode(', ',$deadlineArray);
			}
			$deadline .= '</span>';
		}
	}
	$ex_image = explode(',',$row['images']);
	$image = '<img src="/admin_2/uploads/'.$ex_image[2].'">';
	
	$metro = '';
	if(!empty($row['station'])){
		$v = $row['dist_value'];
		if($v/1000>=1){
			$v = price_cell($v/1000,2).' км';
		}
		else {
			$v = price_cell($v,0).' м';
		}
		// $metro = '<div class="metro">&laquo;'.$row['station_name'].'&raquo; / <strong>'.$row['distance'].'</strong></div>';
		$metro = '<div class="metro">&laquo;'.$row['station_name'].'&raquo; / <strong>'.$v.'</strong></div>';
	}
	
	$square_meter_min = ceil($min_price/$min_full_square);
	$square_meter_max = ceil($max_price/$max_full_square);
	
	$link = '/newbuilding/'.$row['id'];
	$request = '';
	if($arrayLinkSearch['rooms']){
		$ex_request = explode(',',$arrayLinkSearch['rooms']);
		$ex_request2 = array();
		for($c=0; $c<count($ex_request); $c++){
			$req = $ex_request[$c].'-комн.';
			if($ex_request[$c]==0){
				$req = 'Студии';
			}
			if($ex_request[$c]==4){
				$req = $ex_request[$c].'+ комн.';
			}
			array_push($ex_request2,$req);
		}
		$request = implode(' / ',$ex_request2);
		$request = $request.' квартиры';
	}
	else if($arrayLinkSearch['housing']){
		$ex_request = explode(',',$arrayLinkSearch['housing']);
		$ex_request2 = array();
		for($c=0; $c<count($ex_request); $c++){
			$req = $ex_request[$c].' корпус';
			array_push($ex_request2,$req);
		}
		$request = implode(' / ',$ex_request2);
	}
	else {
		$request = 'все квартиры комплекса';
	}
	
	
	$type_developer = '';
	$type_deadline = '';
	if(!empty($row['name_developer'])){
		$type_developer = '<div class="type">Застройщик: <a href="'.$link_developer.'">'.$row['name_developer'].'</a></div>';
	}
	if(!empty($deadline)){
		$type_deadline = '<div class="type">Срок сдачи: '.$deadline.'</div>';
	}
	$title_card = '<div class="title_page house">
		<div class="location_card">
			<h1 class="inner_pages"><a href="'.$link.'">'.htmlspecialchars_decode($row['h1']).'</a></h1>
			<div class="show">Показано: <strong>'.$request.'</strong></div>
			<div class="address">'.$row['address'].'</div>
			'.$metro.'
		</div>
		<div class="price_card">
			<div class="prices">
				<div class="full_price"><strong>'.price_cell($min_price,0).'</strong><div class="max_price">- '.price_cell($max_price,0).' руб.</div></div>
				<div class="meter_price">'.price_cell($square_meter_min,0).' - '.price_cell($square_meter_max,0).' руб/м<sup>2</sup></div>
			</div>
			<div class="developer">
				'.$type_developer.'
				'.$type_deadline.'
			</div>
		</div>
	</div>';
}	
?>
<div id="center">
	<?=$title_card?>
	<div class="separate"></div>
	<div id="housing_estate">
		<div class="text_info icons">
			<div class="list_flats">
			<?
			$arrayAllFlats = array();
			$flats = mysql_query("
				SELECT n.*
				FROM ".$template."_new_flats AS n
				WHERE n.activation='1' && n.p_main='".$row['id']."'".$roomsAddParamFlats.$queueAddParamFlats."
				ORDER BY n.rooms,n.queue
			");
			if(@mysql_num_rows($flats)>0){
				while($flat = mysql_fetch_assoc($flats)){
					$arr = array(
						"id" => $flat['id'],
						"rooms" => $flat['rooms'],
						"queue" => $flat['queue'],
						"full_square" => $flat['full_square'],
						"price" => $flat['price'],
						"floor" => $flat['floor'],
						"link" => "/newbuilding/".$row['id']."/flat/".$flat['id'],
						"scheme" => $flat['images'],
					);
					array_push($arrayAllFlats,$arr);
				}
			}
			
			$arrayRooms = array();
			$arrayQueue = array();
			$n = 1;
			$num = 1;
			$number = 1;
			$number2 = 0;
			// echo '<pre>';
			// print_r($arrayAllFlats);
			// echo '</pre>';
			for($c=0; $c<count($arrayAllFlats); $c++){
				if(!in_array($arrayAllFlats[$c]['rooms'],$arrayRooms)){
					array_push($arrayRooms,$arrayAllFlats[$c]['rooms']);
					// if(!in_array($arrayAllFlats[$c]['queue'],$arrayQueue)){
					$arrayQueue = array();
					array_push($arrayQueue,$arrayAllFlats[$c]['queue']);
					if($n>1){
						echo '</ul>';
						$n=1;
					}
					$name_rooms = $arrayAllFlats[$c]['rooms'].'-комн.';
					$name_rooms2 = $arrayAllFlats[$c]['rooms'].'-комн. кв';
					$min_square = '';
					$squaresArray = array();
					$pricesArray = array();
					for($f=0; $f<count($arrayAllFlats); $f++){
						if($arrayAllFlats[$f]['rooms']==$arrayAllFlats[$c]['rooms'] && $arrayAllFlats[$f]['queue']==$arrayAllFlats[$c]['queue']){
							array_push($squaresArray,$arrayAllFlats[$f]['full_square']);
							array_push($pricesArray,$arrayAllFlats[$f]['price']);
						}
					}
					$min_square = min($squaresArray);
					$min_price = min($pricesArray);
					if($arrayAllFlats[$c]['rooms']==0){
						$name_rooms = 'Студии';
						$name_rooms2 = 'Студии';
					}
					$show = '';
					$plus_minus = ' plus';
					if($number==1){
						$show = ' show';
						$plus_minus = ' minus';
						$number++;
					}
					echo '<div class="row fone">
						<i class="triangle-bottomleft '.$classRoom[$arrayAllFlats[$c]['rooms']].'"></i>
						<div class="open'.$plus_minus.'"><a href="javascript:void(0)"><span>'.$name_rooms.' от <strong>'.$min_square.' м<sup>2</sup></strong></span></a></div>
						<div class="housing">Корпус '.$arrayAllFlats[$c]['queue'].'</div>
						<div class="price">от <strong>'.price_cell($min_price,0).' руб.</strong></div>
					</div>';
					$scheme = '';
					$icon = '<div class="icon disabled">'.$scheme.'</div>';
					if(!empty($arrayAllFlats[$c]['scheme'])){
						$scheme = '<a rel="prettyPhoto" href="/admin_2/uploads/'.$arrayAllFlats[$c]['scheme'].'"></a>';
						$icon = '<div class="icon '.$classRoom[$arrayAllFlats[$c]['rooms']].'">'.$scheme.'</div>';
					}
					$floorText = '<div class="floor"></div>';
					if(!empty($arrayAllFlats[$c]['floor'])){
						$floorText = '<div class="floor">этаж '.$arrayAllFlats[$c]['floor'].'</div>';
					}
					echo '<ul class="hidden'.$show.'">
						<li class="fone">
							'.$icon.'
							<div class="property">'.$name_rooms2.' <strong>'.str_replace(".", ",", $arrayAllFlats[$c]['full_square']).' м<sup>2</sup></strong></div>
							<div class="price_prop"><strong>'.price_cell($arrayAllFlats[$c]['price'],0).' руб.</strong></div>
							'.$floorText.'
							<div class="more_info"><a target="_blank" href="'.$arrayAllFlats[$c]['link'].'">подробно</a></div>
						</li>';
					$number2++;
					$n++;
					$num++;
					// }
					// else {
						
					// }
				}
				else {
					if(!in_array($arrayAllFlats[$c]['queue'],$arrayQueue)){
						array_push($arrayQueue,$arrayAllFlats[$c]['queue']);
						if($n>1){
							echo '</ul>';
							$n = 1;
						}
						$name_rooms = $arrayAllFlats[$c]['rooms'].'-комн.';
						$name_rooms2 = $arrayAllFlats[$c]['rooms'].'-комн. кв';
						$min_square = '';
						$squaresArray = array();
						$pricesArray = array();
						for($f=0; $f<count($arrayAllFlats); $f++){
							if($arrayAllFlats[$f]['rooms']==$arrayAllFlats[$c]['rooms'] && $arrayAllFlats[$f]['queue']==$arrayAllFlats[$c]['queue']){
								array_push($squaresArray,$arrayAllFlats[$f]['full_square']);
								array_push($pricesArray,$arrayAllFlats[$f]['price']);
							}
						}
						$min_square = min($squaresArray);
						$min_price = min($pricesArray);
						if($arrayAllFlats[$c]['rooms']==0){
							$name_rooms = 'Студии';
							$name_rooms2 = 'Студии';
						}
						$show = '';
						$plus_minus = ' plus';
						if($number==1){
							$show = ' show';
							$plus_minus = ' minus';
							$number++;
						}
						echo '<div class="row fone">
							<i class="triangle-bottomleft '.$classRoom[$arrayAllFlats[$c]['rooms']].'"></i>
							<div class="open'.$plus_minus.'"><a href="javascript:void(0)"><span>'.$name_rooms.' от <strong>'.$min_square.' м<sup>2</sup></strong></span></a></div>
							<div class="housing">Корпус '.$arrayAllFlats[$c]['queue'].'</div>
							<div class="price">от <strong>'.price_cell($min_price,0).' руб.</strong></div>
						</div>';
						$scheme = '';
						$icon = '<div class="icon disabled">'.$scheme.'</div>';
						if(!empty($arrayAllFlats[$c]['scheme'])){
							$scheme = '<a rel="prettyPhoto" href="/admin_2/uploads/'.$arrayAllFlats[$c]['scheme'].'"></a>';
							$icon = '<div class="icon '.$classRoom[$arrayAllFlats[$c]['rooms']].'">'.$scheme.'</div>';
						}
						$floorText = '<div class="floor"></div>';
						if(!empty($arrayAllFlats[$c]['floor'])){
							$floorText = '<div class="floor">этаж '.$arrayAllFlats[$c]['floor'].'</div>';
						}
						echo '<ul class="hidden'.$show.'">
							<li class="fone">
								'.$icon.'
								<div class="property">'.$name_rooms2.' <strong>'.str_replace(".", ",", $arrayAllFlats[$c]['full_square']).' м<sup>2</sup></strong></div>
								<div class="price_prop"><strong>'.price_cell($arrayAllFlats[$c]['price'],0).' руб.</strong></div>
								'.$floorText.'
								<div class="more_info"><a target="_blank" href="'.$arrayAllFlats[$c]['link'].'">подробно</a></div>
							</li>';
						$number2++;
						$n++;
						$num++;
					}
					else {
						$name_rooms = $arrayAllFlats[$c]['rooms'].'-комн.';
						$name_rooms2 = $arrayAllFlats[$c]['rooms'].'-комн. кв';
						if($arrayAllFlats[$c]['rooms']==0){
							$name_rooms = 'Студии';
							$name_rooms2 = 'Студии';
						}
						$fone = '';
						if($num==1){
							$fone = ' class="fone"';
							$num++;
						}
						else {
							$num = 1;
						}
						$scheme = '';
						$icon = '<div class="icon disabled">'.$scheme.'</div>';
						if(!empty($arrayAllFlats[$c]['scheme'])){
							$scheme = '<a rel="prettyPhoto" href="/admin_2/uploads/'.$arrayAllFlats[$c]['scheme'].'"></a>';
							$icon = '<div class="icon '.$classRoom[$arrayAllFlats[$c]['rooms']].'">'.$scheme.'</div>';
						}
						$floorText = '<div class="floor"></div>';
						if(!empty($arrayAllFlats[$c]['floor'])){
							$floorText = '<div class="floor">этаж '.$arrayAllFlats[$c]['floor'].'</div>';
						}
						echo '<li'.$fone.'>
							'.$icon.'
							<div class="property">'.$name_rooms2.' <strong>'.str_replace(".", ",", $arrayAllFlats[$c]['full_square']).' м<sup>2</sup></strong></div>
							<div class="price_prop"><strong>'.price_cell($arrayAllFlats[$c]['price'],0).' руб.</strong></div>
							'.$floorText.'
							<div class="more_info"><a target="_blank" href="'.$arrayAllFlats[$c]['link'].'">подробно</a></div>
						</li>';
						$number2++;
						$n++;
					}
				}
			}
			?>
			</div>
			<?=$total_estate?>
		</div>
		<div class="graph_info">
			<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
			<style>
				#map {
					width: 572px; height: 295px; padding: 0; margin: 0;
				}
			</style>
			<?
			$coordsYandex = '';
			$ex_coords = array('30.315868','59.939095');
			if(empty($row['coords'])){
				$notUpdate = false;
				$params2 = array(
					'geocode' => 'Санкт-Петербург, '.$row['address'],
					'format'  => 'json',
					'results' => 1,
					'key'     => 'AKDIIFYBAAAAqIYtRgIAZYb8P32hIxnbRRSe9CpggONv4PEAAAAAAAAAAADu6wMZJIEJ0SDd0mRe33ag1JRdNA==',
				);
				$response = json_decode(@file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params2)));
				
				if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0){
					$coordsYandex = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
				}
			}
			else {
				$coordsYandex = $row['coords'];
				$notUpdate = true;
			}
			if(!empty($coordsYandex)){
				if(!$notUpdate){
					mysql_query("
						UPDATE ".$template."_m_catalogue_left
						SET coords='".$coordsYandex."'
						WHERE id='".$row['id']."'
					");
				}
			}
			if(!empty($coordsYandex)){
				$ex_coords = explode(' ',$coordsYandex);
			}

			?>
			
			<script>
				ymaps.ready(function () {
					var myMap = new ymaps.Map('map', {
							center: [<?=$ex_coords[1]?>,<?=$ex_coords[0]?>],
							zoom: 13
						}, {
							searchControlProvider: 'yandex#search'
						}),
						myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
							hintContent: '<?=$row['name']?>',
							balloonContent: '<?=$row['address']?>'
						}, {
							// Опции.
							// Необходимо указать данный тип макета.
							iconLayout: 'default#image',
							// Своё изображение иконки метки.
							iconImageHref: '/images/<?=$_ICONS_MAPS[5]['card']?>',
							// Размеры метки.
							iconImageSize: [52, 44],
							// Смещение левого верхнего угла иконки относительно
							// её "ножки" (точки привязки).
							iconImageOffset: [-3, -42]
						});

					myMap.geoObjects.add(myPlacemark);
					var myRouter = ymaps.route([
					  'м. <?=$row['station_name']?>', {
						type: "viaPoint",                   
						point: "<?=$row['address']?>"
					  },
					], {
					  mapStateAutoApply: true 
					});
					
					myRouter.then(function(route) {
						/* Задание контента меток в начальной и 
						   конечной точках */
						var points = route.getWayPoints();
						points.options.set('preset', 'islands#blackStretchyIcon');
						points.get(0).properties.set("iconContent", 'м. <?=$row['station_name']?>');
						points.get(1).properties.set("iconContent", '<?=$row['name']?>');
						// Добавление маршрута на карту
						myMap.geoObjects.add(route);
						var routeLength = route.getHumanLength();
						var routeLength2 = route.getLength();
						$('<div class="full_route">Расстояние от <span>Санкт-Петербург, м. <?=$row['station_name']?></span> до <span><?=htmlspecialchars_decode($row['name'])?></span>: <strong>'+routeLength+'</strong></div>').insertAfter('#map');
						// alert(routeLength2);
					  },
					  // Обработка ошибки
					  function (error) {
						alert("Возникла ошибка: " + error.message);
					  }
					)
				});
			</script>
			<div class="map"><div id="map"></div></div>
			<?
			$floors = '';
			$ready = '';
			$type_house = '';
			$queue = '';
			$ceiling_height = '';
			$parking = '';
			if(!empty($row['floors']) || !empty($row['ready']) || !empty($row['type_house']) || !empty($row['queue']) || !empty($row['ceiling_height']) || !empty($row['parking'])){
				
			}
			if(!empty($row['floors'])){
				if(empty($row['floors_live'])){
					$row['floors_live'] = $row['floors'];
				}
				$floors = '<div class="row">
					<div class="cell label">Количество этажей:</div>
					<div class="cell">'.$row['floors_live'].'-'.$row['floors'].' этажей</div>
				</div>';
			}
			if(!empty($row['ready'])){
				$r = mysql_query("
					SELECT name
					FROM ".$template."_ready
					WHERE id='".$row['ready']."'
				");
				if(mysql_num_rows($r)>0){
					$rw = mysql_fetch_assoc($r);
					$ready = '<div class="row">
						<div class="cell label">Стадия строительства:</div>
						<div class="cell">'.$rw['name'].'</div>
					</div>';
				}
			}
			if(!empty($row['type_house'])){
				$r = mysql_query("
					SELECT name
					FROM ".$template."_type_house
					WHERE id='".$row['type_house']."'
				");
				if(mysql_num_rows($r)>0){
					$rw = mysql_fetch_assoc($r);
					$type_house = '<div class="row">
						<div class="cell label">Тип дома:</div>
						<div class="cell">'.$rw['name'].'</div>
					</div>';
				}
			}
			if(!empty($row['queue_count'])){
				$queue = '<div class="row">
					<div class="cell label">Корпусов:</div>
					<div class="cell">'.$row['queue_count'].'</div>
				</div>';
			}
			if(!empty($row['ceiling_height'])){
				$ceiling_height = '<div class="row">
					<div class="cell label">Высота потолков:</div>
					<div class="cell">'.str_replace(".", ",", $row['ceiling_height']).' м</div>
				</div>';
			}
			if(!empty($row['parking'])){
				$parking = '<div class="row">
					<div class="cell label">Паркинг:</div>
					<div class="cell places"><span class="ok">есть</span>('.$row['parking'].' мест)</div>
				</div>';
			}
			echo '<div class="info_housing">
				'.$floors.'
				'.$ready.'
				'.$type_house.'
				'.$queue.'
				'.$ceiling_height.'
				'.$parking.'
			</div>';			
			?>
<!--		<div class="info_housing">
				<div class="row">
					<div class="cell label">Количество этажей:</div>
					<div class="cell">14-14 этажей</div>
				</div>
				<div class="row">
					<div class="cell label">Стадия строительства:</div>
					<div class="cell">Строиться</div>
				</div>
				<div class="row">
					<div class="cell label">Тип дома:</div>
					<div class="cell">Монолитно-кирпичный</div>
				</div>
				<div class="row">
					<div class="cell label">Корпусов:</div>
					<div class="cell">4 корпуса</div>
				</div>
				<div class="row">
					<div class="cell label">Высота потолков:</div>
					<div class="cell">2,69 м</div>
				</div>
				<div class="row">
					<div class="cell label">Паркинг:</div>
					<div class="cell places"><span class="ok">есть</span>(120 мест)</div>
				</div>
			</div>-->
			<?
			if(isset($parent_new__var2) && (isset($housing_new__var2) || isset($rooms_new__var2))){
				$ex_rooms_new__var2 = explode(',',$rooms_new__var2);
				$e_r_new = array();
				for($e=0; $e<count($ex_rooms_new__var2); $e++){
					array_push($e_r_new,"rooms!='".$ex_rooms_new__var2[$e]."'");
				}
				$arrayRooms = array();
				$r = mysql_query("
					SELECT rooms,
					(SELECT COUNT(*) FROM ".$template."_new_flats WHERE activation='1' && p_main='".$row['id']."' && ".implode(' && ',$e_r_new).") AS count 
					FROM ".$template."_new_flats
					WHERE activation='1' && p_main='".$row['id']."' && ".implode(' && ',$e_r_new)."
				");
				if(mysql_num_rows($r)>0){
					while($rw = mysql_fetch_assoc($r)){
						$rw_rooms = $rw['rooms'];
						if($rw['rooms']>=4){
							$rw_rooms = 4;
						}
						if(!in_array($rw_rooms,$arrayRooms)){
							array_push($arrayRooms,$rw_rooms);
						}
					}
				}
				
				$ex_housing_new__var2 = explode(',',$housing_new__var2);
				$e_h_new = array();
				for($e=0; $e<count($ex_housing_new__var2); $e++){
					array_push($e_h_new,"queue!='".$ex_housing_new__var2[$e]."'");
				}
				$arrayHousing = array();
				$r = mysql_query("
					SELECT queue,
					(SELECT COUNT(*) FROM ".$template."_new_flats WHERE activation='1' && p_main='".$row['id']."' && ".implode(' && ',$e_h_new).") AS count 
					FROM ".$template."_new_flats
					WHERE activation='1' && p_main='".$row['id']."' && ".implode(' && ',$e_h_new)."
				");
				if(mysql_num_rows($r)>0){
					while($rw = mysql_fetch_assoc($r)){
						array_push($arrayHousing,$rw['queue']);
					}
				}
				$roomsParam = '';
				if(count($arrayRooms)>0){
					$roomsParam = '&rooms='.implode(',',$arrayRooms);
				}
				$housingParam = '';
				if(count($arrayHousing)>0 && count($arrayRooms)==0){
					$housingParam = '&housing='.implode(',',$arrayHousing);
				}
				$deferent = $row['all_count_flats'] - $number2;
				if($deferent>0){
					// echo '<div class="agency_block">
						// <div class="title">
							// <div class="title_left">Нравится жилой комплекс?</div>
							// <div class="title_right"><a href="/newbuilding?estate=1&parent='.$row['id'].''.$roomsParam.''.$housingParam.'"><span>Ещё '.$deferent.' объектов</span><i class="fa fa-angle-right"></i></a></div>
						// </div>
						// <div class="list_row">
							// <div class="row fone">
								// <i class="triangle-bottomleft studio"></i>
								// <div class="name">Студия от <strong>29,90 м<sup>2</sup></strong></div>
								// <div class="price">от <strong>1 755 215 руб.</strong></div>
								// <div class="count_offers"><a href="#">3 в продаже</a></div>
							// </div>
							// <div class="row">
								// <i class="triangle-bottomleft one"></i>
								// <div class="name">1-комн. от <strong>34,78 м<sup>2</sup></strong></div>
								// <div class="price">от <strong>1 990 290 руб.</strong></div>
								// <div class="count_offers"><a href="#">10 в продаже</a></div>
							// </div>
							// <div class="row fone">
								// <i class="triangle-bottomleft four"></i>
								// <div class="name">5-комн. от <strong>53,20 м<sup>2</sup></strong></div>
								// <div class="price">от <strong>2 860 000 руб.</strong></div>
								// <div class="count_offers"><a href="#">4 в продаже</a></div>
							// </div>
						// </div>
					// </div>';
				}
			}
			?>
		</div>
	</div>
	<?
	$res = mysql_query("
		SELECT c.*,
		(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=c.id && estate='jk') AS images,
		(SELECT MIN(price) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS min_price,
		(SELECT MAX(price) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS max_price,
		(SELECT MIN(full_square) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS min_square,
		(SELECT MAX(full_square) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS max_square,
		(SELECT COUNT(*) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS count_estate,
		(SELECT name FROM ".$template."_developers WHERE activation='1' && id=c.developer) AS name_developer
		FROM ".$template."_m_catalogue_left AS c
		WHERE c.activation='1' && c.id!='".$PageInfo['id']."'
		ORDER BY RAND()
		LIMIT 2
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		echo '<div class="our_offers">';
		echo '<h3>Похожие жилые комплексы</h3>';
		echo '<div class="offers_list">';
		while($row = mysql_fetch_assoc($res)){
			$ex_image = explode(',',$row['images']);
			$image = '<img src="/admin_2/uploads/'.$ex_image[0].'">';
			$link = '/newbuilding/'.$row['id'];
			$exploitation = explode('_',$row['exploitation']);
			$deadline = '';
			if(empty($row['min_square']) || empty($row['max_square'])){
				$square_meter_min = 0;
				$square_meter_max = 0;
			}
			else {
				$square_meter_min = ceil($row['min_price']/$row['min_square']);
				$square_meter_max = ceil($row['max_price']/$row['max_square']);
			}
			if(!empty($row['exploitation'])){
				if(count($exploitation)>0){
					if(count($exploitation)>1){
						$deadline = '<span>'.$exploitation[1].' кв. '.$exploitation[0].'</span>';
					}
					else {
						$deadline = '<span>'.$exploitation[0].'</span>';
					}
				}
			}
			else {
				$queues = mysql_query("
					SELECT commissioning,queue
					FROM ".$template."_queues
					WHERE p_main='".$row['id']."' && type_name='new'
					GROUP BY queue
				");
				if(mysql_num_rows($queues)>0){
					$deadline .= '<span>';
					$deadlineArray = array();
					
					while($queue = mysql_fetch_assoc($queues)){
						array_push($deadlineArray,$queue['queue'].' очередь - '.$queue['commissioning']);
					}
					// for($q=0; $q<1; $q++){
						$deadline .= $deadlineArray[0];
					// }
					$deadline .= '</span>';
				}
			}
			
			$type_developer = '';
			$type_deadline = '';
			if(!empty($PageInfo['name_developer'])){
				$type_developer = '<div class="type_name">'.$row['name_developer'].'</div>';
			}
			if(!empty($deadline)){
				$type_deadline = '<div class="deadline">Срок сдачи: '.$deadline.'</div>';
			}

			echo '<div class="item">
				<div class="image">
					<a href="'.$link.'">'.$image.'</a>
				</div>
				<div class="body">
					<div class="location">
						<div class="title"><a href="'.$link.'">'.$row['name'].'</a></div>
						<div class="street">'.$row['address'].'</div>
						'.$type_developer.'
						'.$type_deadline.'
					</div>
					<div class="cost_block">
						<div class="price">от <strong>'.price_cell($row['min_price'],0).' руб.</strong></div>
						<div class="square">'.price_cell($square_meter_min,0).' - '.price_cell($square_meter_max,0).' руб/м<sup>2</sup></div>
						<div class="count_offers"><a href="'.$link.'">'.$row['count_estate'].' в продаже</a></div>
					</div>
				</div>
			</div>';
		}
		echo '</div>';
		echo '</div>';
	}
	?>
</div>