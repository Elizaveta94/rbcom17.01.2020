<?
echo '<div id="center">
	<h1 class="inner_pages">'.$titlePage.'</h1>';
	// echo '<pre>';
	// print_r($PageInfoAd);
	// echo '</pre>';
	
	$rooms = '<div style="width:auto" class="select_block count">';
	$rooms_list = '';
	$nameParams = 'Не выбран';
	$valueInput = '';
	for($r=0; $r<=5; $r++){
		$name_param = $r;
		if($r==0){
			$name_param = 'Студия';
		}
		if($r==5){
			$name_param = '5+';
		}
		
		$current = "";
		if($r==0){
			if(!isset($PageInfoAd)){
				$current = ' class="current"';
			}
			$rooms_list .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не выбран</a></li>';
		}
		if($PageInfoAd['rooms']==$r){
			$current = ' class="current"';
			$nameParams = $name_param;
			$valueInput = ' value="'.$r.'"';
			$typeIsset = true;
		}
		$rooms_list .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$r.'">'.$name_param.'</a></li>';
	}

	$rooms .= '<input type="hidden" name="s[rooms]"'.$valueInput.'>';
	$rooms .= '<div style="width:121px" class="choosed_block">'.$nameParams.'</div>';
	$rooms .= '<div class="scrollbar-inner">';
	$rooms .= '<ul>';
	$rooms .= $rooms_list;
	$rooms .= '</ul>';
	$rooms .= '</div>';
	$rooms .= '</div>';

	if($PageInfoAd['estate']==3){
		$rooms = '<div class="checkbox_block rooms">';
		$ex_rooms = array(2);
		$valueInput = ' value="2"';
		if(isset($PageInfoAd['rooms']) && !empty($PageInfoAd['rooms'])){
			$ex_rooms = explode(',',$PageInfoAd['rooms']);
			$valueInput = ' value="'.$PageInfoAd['rooms'].'"';
		}
		$rooms .= '<input type="hidden" name="s[rooms]"'.$valueInput.'>';
		$rooms .= '<div class="select_checkbox">';
		$rooms .= '<ul>';
		$rooms .= '<li '.(in_array(2,$ex_rooms)?'class="checked"':'').'><a href="javascript:void(0)" data-id="2">2</a></li>';
		$rooms .= '<li '.(in_array(3,$ex_rooms)?'class="checked"':'').'><a href="javascript:void(0)" data-id="3">3</a></li>';
		$rooms .= '<li '.(in_array(4,$ex_rooms)?'class="checked"':'').'><a href="javascript:void(0)" data-id="4">4</a></li>';
		$rooms .= '<li '.(in_array(5,$ex_rooms)?'class="checked"':'').'><a href="javascript:void(0)" data-id="5">5+</a></li>';
		// $rooms .= '<li '.(in_array(6,$ex_rooms)?'class="checked"':'').'><a href="javascript:void(0)" data-id="6">6+</a></li>';
		$rooms .= '</ul>';
		$rooms .= '</div>';
		$rooms .= '</div>';
		$roomsParams = '<div class="row">
			<div class="cell label right one_second">
				<label>Комнат в квартире<b>*</b></label>
			</div>
			<div class="cell required one_second last_col">
				'.$rooms.'
			</div>
		</div>';
	}
	else {
		$roomsParams = '<div class="row">
			<div class="cell label right one_second">
				<label>Комнаты<b>*</b></label>
			</div>
			<div class="cell required one_second last_col">
				'.$rooms.'
			</div>
		</div>';
	}
		
	/*
	*  Вывод долей для квартир и комнат
	*/
	$share = '';
	if($PageInfoAd['estate']==2 || $PageInfoAd['estate']==3){
		$checked = '';
		$valueParams = '';
		$disabled = ' disabled="disabled"';
		$disabledParams = ' disabled="disabled"';
		$none = ' none';
		if(isset($PageInfoAd['share'])){
			if($PageInfoAd['share']){
				$disabled = '';
				$disabledParams = '';
				$checked = ' checked';
				$valueParams = ' value="'.$PageInfoAd['share'].'"';
				$none = '';
			}
		}
		$share = '<div class="checkbox_block shares">
			<input type="hidden" name="s[share]" value="0">
			<input'.$disabled.' type="hidden" name="s[share]"'.$valueParams.'>
			<div class="block_checkbox">
				<ul>
					<li class="vert_line"></li>
					<li class="share'.$checked.'"><a href="javascript:void(0)" data-id="1">Доля</a></li>
					<li class="shares'.$none.'"><input'.$disabledParams.' type="text" name="s[count_share]" value="'.$PageInfoAd['count_share'].'"></li>
					<li class="hint'.$none.'">долей из</li>
					<li class="shares'.$none.'"><input'.$disabledParams.' type="text" name="s[all_share]" value="'.$PageInfoAd['all_share'].'"></li>
				</ul>
			</div>
		</div>';
		
		if($PageInfoAd['estate']==2){
			$roomsParams = '<div class="row">
				<div class="cell label right one_second">
					<label>Комнаты<b>*</b></label>
				</div>
				<div style="width:auto" class="cell required one_second last_col">
					'.$rooms.$share.'
				</div>
			</div>';		
		}
		if($PageInfoAd['estate']==3){
			$roomsParams = '<div class="row">
				<div class="cell label right one_second">
					<label>Комнаты<b>*</b></label>
				</div>
				<div style="width:auto" class="cell required one_second last_col">
					'.$rooms.$share.'
				</div>
			</div>';		
		}
	}
	
	/*
	*  Балкон
	*/
	$balcony = '';
	$balconis = '';
	$valueInput = '';
	for($c=0; $c<count($_TYPE_BALCONY_ADMIN); $c++){
		$checked = '';
		if(isset($PageInfoAd['balcony']) && $PageInfoAd['balcony']==$c){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';
		}
		$balconis .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_BALCONY_ADMIN[$c].'</a></li>';
	}
	$balcony = '<div class="checkbox_block">';
	$balcony .= '<input type="hidden" name="s[balcony]"'.$valueInput.'>';
	$balcony .= '<div class="select_checkbox">';
	$balcony .= '<ul>';
	$balcony .= $balconis;
	$balcony .= '</ul>';
	$balcony .= '</div>';
	$balcony .= '</div>';
	
	/*
	*  Санузел
	*/
	$wc = '';
	$wcs = '';
	$valueInput = '';
	for($c=0; $c<count($_TYPE_WC); $c++){
		$checked = '';
		$name_param = $_TYPE_WC[$c];
		if(isset($PageInfoAd['wc']) && $PageInfoAd['wc']==$c){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';
		}
		if($c==0){
			$name_param = 'Нет';
		}
		$wcs .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$name_param.'</a></li>';
	}
	$wc = '<div class="checkbox_block">';
	$wc .= '<input type="hidden" name="s[wc]"'.$valueInput.'>';
	$wc .= '<div class="select_checkbox">';
	$wc .= '<ul>';
	$wc .= $wcs;
	$wc .= '</ul>';
	$wc .= '</div>';
	$wc .= '</div>';
	
	/*
	*  Отделка
	*/
	$finish = '';
	$finishes = '';
	$valueInput = '';
	for($c=1; $c<count($_TYPE_FINISHING_CAB); $c++){
		$checked = '';
		if(isset($PageInfoAd['finishing']) && $PageInfoAd['finishing']==$c){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';
		}
		if(empty($PageInfoAd['finishing'])){
			if($c==1){
				$checked = ' class="checked"';
				$valueInput = ' value="'.$c.'"';				
			}
		}
		$finishes .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_FINISHING_CAB[$c].'</a></li>';
	}
	$finish = '<div class="checkbox_block">';
	$finish .= '<input type="hidden" name="s[finishing]"'.$valueInput.'>';
	$finish .= '<div class="select_checkbox">';
	$finish .= '<ul>';
	$finish .= $finishes;
	$finish .= '</ul>';
	$finish .= '</div>';
	$finish .= '</div>';
	
	/*
	*  Лифт
	*/
	$lift = '';
	$liftes = '';
	for($c=0; $c<count($_TYPE_LIFT); $c++){
		$checked = '';
		$valueName = $_TYPE_LIFT[$c];
		$value = $c;
		if($c==0 && !isset($PageInfoAd['lift'])){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$value.'"';
		}
		if(isset($PageInfoAd['lift']) && $PageInfoAd['lift']==$value){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$value.'"';
		}
		$liftes .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$value.'">'.$valueName.'</a></li>';
	}
	$lift = '<div class="checkbox_block">';
	$lift .= '<input type="hidden" name="s[lift]"'.$valueInput.'>';
	$lift .= '<div class="select_checkbox">';
	$lift .= '<ul style="margin-right:-20px;">';
	$lift .= $liftes;
	$lift .= '</ul>';
	$lift .= '</div>';
	$lift .= '</div>';
	
	/*
	*  Тип дома
	*/
	$type_house = '<div class="select_block count">';
	$type_houses = '';
	$nameParams = 'Не важно';
	$valueInput = '';
	$devs = mysql_query("
		SELECT *
		FROM ".$template."_type_house
		WHERE activation='1'
		ORDER BY num
	");
	if(mysql_num_rows($devs)>0){
		$n = 0;
		while($dev = mysql_fetch_assoc($devs)){
			$current = "";
			
			if($n==0 && !isset($PageInfoAd['type_house'])){
				$type_houses .= '<li class="current"><a href="javascript:void(0)" data-id="">Не важно</a></li>';
			}
			if($PageInfoAd['type_house']==$dev['id']){
				$current = ' class="current"';
				$nameParams = $dev['name'];
				$valueInput = ' value="'.$dev['id'].'"';
			}
			$type_houses .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$dev['id'].'">'.$dev['name'].'</a></li>';
			$n++;
		}
	}
	$type_house .= '<input type="hidden" name="s[type_house]"'.$valueInput.'>';
	$type_house .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
	$type_house .= '<div class="scrollbar-inner">';
	$type_house .= '<ul>';
	$type_house .= $type_houses;
	$type_house .= '</ul>';
	$type_house .= '</div>';
	$type_house .= '</div>';
	
	/*
	*  Сдача
	*/
	$ex_min_value_delivery_new_sell = explode(' ',$min_value_delivery_new_sell);
	$ex_max_value_delivery_new_sell = explode(' ',$max_value_delivery_new_sell);
	$_min_value_delivery_year = (int)$ex_min_value_delivery_new_sell[1];
	$_max_value_delivery_year = (int)$ex_max_value_delivery_new_sell[1];

	$n = 1;
	$deliveryAdd = '';
	$deliveryArrayMin = '';
	$_QUATRE = array("I","II","III","IV");
	$currentParams = '';
	if($PageInfoAd['commissioning']){
		$currentParams = $PageInfoAd['commissioning'];
	}
	for($c=$_min_value_delivery_year; $c<=$_max_value_delivery_year; $c++){
		$min_search = array_search($ex_min_value_delivery_new_sell[0],$_QUATRE);
		$max_search = array_search($ex_max_value_delivery_new_sell[0],$_QUATRE);
		if($c==$_min_value_delivery_year){
			for($q=$min_search; $q<count($_QUATRE); $q++){
				$q_full = $_QUATRE[$q].' '.$c;
				$q_name = $_QUATRE[$q]." кв. ".$c;
				$current = "";
				if($q_full==$currentParams){
					$current = ' class="current"';
					$deliveryArrayMin = $q_name;
				}
				$deliveryAdd .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$q_full.'">'.$q_name.'</a></li>';
				$n++;
			}
		}
		else if($c==$_max_value_delivery_year){
			for($q=0; $q<=$max_search; $q++){
				$q_name = $_QUATRE[$q]." кв. ".$c;
				$q_full = $_QUATRE[$q].' '.$c;
				$current = "";
				if($q_full==$currentParams){
					$current = ' class="current"';
					$deliveryArrayMin = $q_name;
				}
				$deliveryAdd .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$q_full.'">'.$q_name.'</a></li>';
				$n++;
			}
		}
		else {
			for($q=0; $q<count($_QUATRE); $q++){
				$q_name = $_QUATRE[$q]." кв. ".$c;
				$q_full = $_QUATRE[$q].' '.$c;
				$current = "";
				if($n==$currentParams){
					$current = ' class="current"';
					$deliveryArrayMin = $q_name;
				}
				$deliveryAdd .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$q_full.'">'.$q_name.'</a></li>';
				$n++;
			}
		}
	}
	$delivery .= '<div class="select_block count">';
	$nameParams = 'От';
	$valueParams = '';
	if(!empty($PageInfoAd['commissioning'])){
		$nameParams = $PageInfoAd['commissioning'];
		$valueParams = ' value="'.$PageInfoAd['commissioning'].'"';
	}
	$delivery .= '<input type="hidden" name="queues[commissioning]"'.$valueParams.'>';
	$delivery .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
	$delivery .= '<div class="scrollbar-inner">';
	$delivery .= '<ul>';
	$delivery .= $deliveryAdd;
	$delivery .= '</ul>';
	$delivery .= '</div>';
	$delivery .= '</div>';
	
	/*
	*  Дополнительная информация
	*/
	$text = '';
	if(isset($PageInfoAd['text']) && !empty($PageInfoAd['text'])){
		$text = $PageInfoAd['text'];
	}
	
	/*
	*  Этаж
	*/
	$floor = '';
	if(isset($PageInfoAd['floor']) && !empty($PageInfoAd['floor'])){
		$floor = ' value="'.$PageInfoAd['floor'].'"';
	}
	
	/*
	*  Общая площадь
	*/
	$full_square = '';
	if(isset($PageInfoAd['full_square']) && !empty($PageInfoAd['full_square'])){
		$full_square = ' value="'.str_replace(".", ",", $PageInfoAd['full_square']).'"';
	}
	
	/*
	*  Жилая площадь
	*/
	$live_square = '';
	if(isset($PageInfoAd['live_square']) && !empty($PageInfoAd['live_square'])){
		$live_square = ' value="'.str_replace(".", ",", $PageInfoAd['live_square']).'"';
	}
	
	/*
	*  Кухня
	*/
	$kitchen_square = '';
	if(isset($PageInfoAd['kitchen_square']) && !empty($PageInfoAd['kitchen_square'])){
		$kitchen_square = ' value="'.str_replace(".", ",", $PageInfoAd['kitchen_square']).'"';
	}
	
	/*
	*  Этажей в доме
	*/
	$floors = '';
	if(isset($PageInfoAd['floors']) && !empty($PageInfoAd['floors'])){
		$floors = ' value="'.$PageInfoAd['floors'].'"';
	}
	
	/*
	*  Площадь комнат
	*/
	$rooms_square = '';
	if(isset($PageInfoAd['rooms_square']) && !empty($PageInfoAd['rooms_square'])){
		$rooms_square = ' value="'.$PageInfoAd['rooms_square'].'"';
	}
	
	/*
	*  Очередь
	*/
	$queues = '';
	if(isset($PageInfoAd['queue']) && !empty($PageInfoAd['queue'])){
		$queues = ' value="'.$PageInfoAd['queue'].'"';
	}
	
	/*
	*  Корпус
	*/
	$building = '';
	if(isset($PageInfoAd['building']) && !empty($PageInfoAd['building'])){
		$building = ' value="'.$PageInfoAd['building'].'"';
	}
	
	/*
	*  Дом сдан
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['ready'])){
		if($PageInfoAd['ready']==5){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['ready'].'"';
		}
	}
	$ready = '<div class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[ready]" value="0">
		<input type="hidden"'.$valueParams.' name="s[ready]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">дом сдан</span>
		</div>
	</div>';
	
	/*
	*  Паркинг
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['parking'])){
		if($PageInfoAd['parking']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['parking'].'"';
		}
	}
	$parking = '<div class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[parking]" value="0">
		<input type="hidden"'.$valueParams.' name="s[parking]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Паркинг</span>
		</div>
	</div>';
	
	/*
	*  Мусоропровод
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['rubbish'])){
		if($PageInfoAd['rubbish']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['rubbish'].'"';
		}
	}
	$rubbish = '<div class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[rubbish]" value="0">
		<input type="hidden"'.$valueParams.' name="s[rubbish]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Мусоропровод</span>
		</div>
	</div>';
	
	/*
	*  Фотографии
	*/
	$estateName = '';
	if($PageInfoAd['estate']==1){
		$estateName = 'new_flats';
	}
	if($PageInfoAd['estate']==2){
		$estateName = 'flats';
	}
	if($PageInfoAd['estate']==3){
		$estateName = 'rooms';
	}
	if($PageInfoAd['estate']==4){
		$estateName = 'country';
	}
	if($PageInfoAd['estate']==5){
		$estateName = 'commercial';
	}
	if($PageInfoAd['estate']==6){
		$estateName = 'cession';
	}
	$photos_list = '<li class="photo_title"><span>Фотографии объекта</span></li>';
	$photos = mysql_query("
		SELECT *
		FROM ".$template."_photo_catalogue
		WHERE p_main='".$PageInfoAd['id']."' && activation='1' && estate='".$estateName."'
		ORDER BY num
	");
	if(mysql_num_rows($photos)>0){
		while($photo = mysql_fetch_assoc($photos)){
			$img = explode(',',$photo['images']);
			$photos_list .= '<li data-id="'.$photo['id'].'"><img width="170" height="110" src="/users/'.$_SESSION['idAuto'].'/'.$img[1].'"><span onclick="return deletePhoto(this)" class="close"></span></li>';
		}
	}
	
	/*
	*  Шаги страницы
	*/
	$step1 = '<li><a href="/add?id='.$idAd.'">Расположение</a></li>';
	$step2 = '<li class="current"><a href="/add2?id='.$idAd.'">Параметры</a></li>';
	$step3 = '<li><span>Стоимость</span></li>';
	$step4 = '<li><span>Условия размещения</span></li>';
	$step5 = '<li><span>Ваше объявление</span></li>';
	$btnBack = '<label class="btn link"><a class="back_page" href="/add?id='.$idAd.'"><span>Назад</span></a></label>';
	$btnPage = '<label class="btn"><input type="submit" value="Продолжить"><span class="angle-right"></span></label>';
	if($PageInfoAd['all_save']){
		$step1 = '<li><a href="/add?id='.$idAd.'">Расположение</a></li>';
		$step2 = '<li class="current"><a href="/add2?id='.$idAd.'">Параметры</a></li>';
		$step3 = '<li><a href="/add3?id='.$idAd.'">Стоимость</a></li>';
		$step4 = '<li><a href="/add4?id='.$idAd.'">Условия размещения</a></li>';
		$step5 = '<li><a href="/add5?id='.$idAd.'">Ваше объявление</a></li>';
		$btnBack = '<label class="btn link"><a class="back_page" href="/add?id='.$idAd.'"><span>Назад</span></a></label>';
		// $btnPage = '<label class="btn"><input type="submit" value="Сохранить изменения"><span class="angle-right"></span></label>';
	}
	
	/*
	*  Новостройки и переуступки
	*/
	if($PageInfoAd['estate']==1 || $PageInfoAd['estate']==6){
		$rightBlock = '<div class="right_block">
			<div class="table_form">
				<h2>Информация о доме</h2>
				<!--<div class="row">
					<div style="width:74px;margin-right:15px;" class="cell label right one_second">
						<label>Название</label>
					</div>
					<div style="width:396px" class="cell one_second last_col">
						<input type="text" class="text" name="s[name]">
					</div>
				</div>-->
				<div class="line">
					<div class="block">
						<div class="row">
							<div class="cell label right one_second">
								<label>Очередь</label>
							</div>
							<div class="cell one_second last_col">
								<input type="text" class="text" name="queues[queue]"'.$queues.'>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Тип дома</label>
							</div>
							<div class="cell one_second last_col">
								'.$type_house.'
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Срок сдачи</label>
							</div>
							<div class="cell one_second last_col">
								'.$delivery.'
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">&nbsp;</div>
							<div class="cell one_second last_col">
								'.$ready.'
							</div>
						</div>
						<div class="row">
							<div style="text-align:right" class="cell label one_second">Лифт</div>
							<div class="cell auto last_col">
								'.$lift.'
							</div>
						</div>
					</div>
					<div class="block">
						<div class="row">
							<div class="cell label right one_second">
								<label>Корпус</label>
							</div>
							<div class="cell one_second last_col">
								<input type="text" class="text" name="queues[building]"'.$building.'>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Серия дома</label>
							</div>
							<div class="cell one_second last_col">
								<input placeholder="П-404" type="text" class="text" name="">
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label></label>
							</div>
							<div class="cell one_second last_col"></div>
						</div>
						<div class="row">
							<div class="cell label right one_second">&nbsp;</div>
							<div class="cell one_second last_col">
								'.$parking.'
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">&nbsp;</div>
							<div class="cell one_second last_col">
								'.$rubbish.'
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>';
	}
	
	/*
	*  Квартиры и комнаты
	*/
	if($PageInfoAd['estate']==2 || $PageInfoAd['estate']==3){
		$rightBlock = '<div class="right_block">
			<div class="table_form">
				<h2>Информация о доме</h2>
				<div class="line">
					<div class="block">
						<div class="row">
							<div class="cell label right one_second">
								<label>Тип дома</label>
							</div>
							<div class="cell one_second last_col">
								'.$type_house.'
							</div>
						</div>
						<div class="row">
							<div style="text-align:right" class="cell label one_second">Лифт</div>
							<div class="cell auto last_col">
								'.$lift.'
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">&nbsp;</div>
							<div class="cell one_second last_col">
								'.$parking.'
							</div>
						</div>
					</div>
					<div class="block">
						<div class="row">
							<div class="cell label right one_second">
								<label>Серия дома</label>
							</div>
							<div class="cell one_second last_col">
								<input placeholder="П-404" type="text" class="text" name="">
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label></label>
							</div>
							<div class="cell one_second last_col"></div>
						</div>
						<div class="row">
							<div class="cell label right one_second">&nbsp;</div>
							<div class="cell one_second last_col">
								'.$rubbish.'
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>';
	}
	
	echo '<div class="text_block">
		'.$textPage.'
	</div>';
	echo '<div class="add_block">
		<ul class="steps">'.$step1.$step2.$step3.$step4.$step5.'</ul>
		<div class="container_block ads step-2">
			<form onsubmit="return saveAdsUsers(this)" action="/include/handler.php" method="POST">
				<input type="hidden" name="ads_save" value="true"> 
				<input type="hidden" name="step" value="2"> 
				<div class="params_block">
					<div class="left_block">
						<div class="table_form">
							<h2>Информация о квартире</h2>
							'.$roomsParams.'
							
							<div class="line">
								<div class="block">
									<div class="row">
										<div class="cell label right one_second">
											<label>Этаж<b>*</b></label>
										</div>
										<div class="cell required one_second last_col">
											<input type="text" class="text" name="s[floor]"'.$floor.'>
										</div>
									</div>
									<div class="row">
										<div class="cell label right one_second">
											<label>Общая площадь<b>*</b></label>
										</div>
										<div class="cell required one_second last_col">
											<input type="text" class="text" name="s[full_square]"'.$full_square.'>
											<div class="com">м<sup>2</sup></div>
										</div>
									</div>
									<div class="row">
										<div class="cell label right one_second">
											<label>Жилая площадь<b>*</b></label>
										</div>
										<div class="cell required one_second last_col">
											<input type="text" class="text" name="s[live_square]"'.$live_square.'>
											<div class="com">м<sup>2</sup></div>
										</div>
									</div>
								</div>
								<div class="block">
									<div class="row">
										<div class="cell label right one_second">
											<label>Этажей в доме<b>*</b></label>
										</div>
										<div class="cell required one_second last_col">
											<input type="text" class="text" name="s[floors]"'.$floors.'>
										</div>
									</div>
									<div class="row">
										<div class="cell label right one_second">
											<label>Площадь комнат<b>*</b></label>
										</div>
										<div class="cell required one_second last_col">
											<input placeholder="Пример: 20+15-10" type="text" class="text" name="s[rooms_square]"'.$rooms_square.'>
											<div class="com">м<sup>2</sup></div>
										</div>
									</div>
									<div class="row">
										<div class="cell label right one_second">
											<label>Кухня</label>
										</div>
										<div class="cell one_second last_col">
											<input type="text" class="text" name="s[kitchen_square]"'.$kitchen_square.'">
											<div class="com">м<sup>2</sup></div>
										</div>
									</div>
								</div>
							</div>
													
							<div class="row">
								<div class="cell info full">
									<p>В поле "Площадь комнат" используйте знак + для обозначения смежных комнат и знак - для раздельных комнат</p>
								</div>
							</div>
							<div class="row">
								<div class="cell right label one_second">Балкон</div>
								<div class="cell auto last_col">
									'.$balcony.'
								</div>
							</div>
							<div class="row">
								<div class="cell right label one_second">Сан.узел</div>
								<div class="cell auto last_col">
									'.$wc.'
								</div>
							</div>
							<div class="row">
								<div class="cell right label one_second">Отделка</div>
								<div class="cell auto last_col">
									'.$finish.'
								</div>
							</div>
						</div>
					</div>
					'.$rightBlock.'
				</div>
				<div class="add_info">
					<div class="left_block">
						<div class="table_form">
							<h2>Дополнительная информация</h2>
							<textarea placeholder="Расскажите ещё немного о Вашем объекте недвижимости..." name="s[text]">'.$text.'</textarea>
						</div>
					</div>
					<div class="right_block">
						<div class="gallery_block">
							<div class="title">Первая загруженная фотография определяется как главная в галерее</div>
							<div class="photos_list">
								<ul>
									'.$photos_list.'
									<li>
										<div class="drag_drop_container">
											<div id="dropzone" class="dropzone"></div>
											<div class="load_images">
												<div class="load_image" id="uploadsImages"><a href="javascript:void(0)"><span>Загрузить фотографию</span></a></div>
											</div>
										</div>
									</li>
								</ul>
							</div>
							<div class="hint">
								<strong>Не допускаются к размещению!</strong>
								<p>Фотографии чужих объектов и рекламные изображения</p>
							</div>
						</div>
					</div>
				</div>
				<div class="btn_info">
					<div class="table_form">
						<div class="row btn_row">
							<div class="cell center full last_col">
								'.$btnBack.'
								'.$btnPage.'
							</div>
						</div>
						<div class="row">
							<div class="cell full">
								<div class="req"><b>*</b> - поля обязательные для заполнения</div>
							</div>
						</div>
					</div>					
				</div>
			</form>
		</div>
	</div>
</div>';
?>