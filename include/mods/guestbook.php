<?
echo '<div id="breadcrumbs"><ul><li><a href="/">Главная</a></li>'.$parentBreadcrumb.'<li>'.$breadcrumbs.'</li></ul></div>';
echo '<h1>'.$titlePage.'</h1>';
echo $textPage;
?>
<div class="module guestbook" id="guestbook">
	<div class="users-answers">
	<?
		$res = mysql_query("
			SELECT * 
			FROM ".$template."_m_guestbook_left 
			WHERE activation='1' && lang='".$lang."' 
			ORDER BY date DESC, time DESC
		",$db) or die(mysql_error());
		if(mysql_num_rows($res) > 0){
			while($row = mysql_fetch_assoc($res)){
				$answer = "";
				if(!empty($row["answer"])){
					$answer = '<b>Ответ:</b><p>'.$row["answer"].'</p>';
				}

				if(!empty($row['city']) && !empty($row['country'])) {
					$place = '<p class="place">'.$row['country'].', '.$row['city'].'</p>';
				}
				if((empty($row['city']) || empty($row['country'])) && (!empty($row['city']) || !empty($row['country']))){
					$place = '<p class="place">'.$row['country'].$row['city'].'</p>';
				}
				if(empty($row['city']) && empty($row['country'])){
					$place = "";
				}
					
				echo '<div class="block">
					<div class="title">
						<h4>'.$row["name"].'</h4>
						'.$place.'
						<p>'.date_rus($row["date"]).' года в '.$row['time'].'</p>
					</div>
					<div class="body">
						<p>'.$row['msg'].'</p>
					</div>
					<div class="body-answer">
						'.$answer.'
					</div>
				</div>';
			}
		}
	?>
	</div>
	<div class="add-answer">
		<p><i>Обратите внимание на поля со звездочкой(<b>*</b>) они обязательны для заполнения!</i></p>
		<div id="messenger"></div>
		<form onsubmit="return checkSideForm(this);" class="search" action="/include/handler.php" method="post">
			<input type="hidden" name="guestbook" value="1">
			<table style="width:100%">
				<tr>
					<td>
						<label for="name">Имя:<b class="req">*</b></label>
						<input id="name" type="text" class="text" name="s[name]">
					</td>
				</tr>
				<tr>
					<td>
						<div class="cell">
							<label for="country">Страна:</label>
							<input type="text" name="s[country]" class="text" id="country">
						</div>
						<div class="cell">
							<label for="city">Город:</label>
							<input type="text" name="s[city]" class="text" id="city">
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<label for="email">E-mail (будет скрыт):<b class="req">*</b></label>
						<input id="email" type="text" class="text" name="s[email]">
					</td>
				</tr>
				<tr>
					<td>
						<label for="msg">Сообщение:<b class="req">*</b></label>
						<textarea class="text" id="msg" rows="7" name="s[msg]"></textarea>
					</td>
				</tr>
				<tr>
					<td>
						<input type="submit" class="submit" value="Отправить" name="">
					</td>
				</tr>
			</table>				
		</form>
	</div>
</div>