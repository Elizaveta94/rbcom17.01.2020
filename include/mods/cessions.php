<?
$add_params = '';
$orderByCessionBuilding = 'price';
$LOAD_PAGE = 12;

if(count($params)==1 && !$arrayLinkSearch2){
	$res = mysql_query("
		SELECT SQL_CALC_FOUND_ROWS c.*,
		(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=c.id && estate='jk') AS images,
		(SELECT title FROM ".$template."_stations WHERE activation='1' && id=c.station) AS station_name,
		(SELECT MAX(price) FROM ".$template."_cessions WHERE activation='1' && p_main=c.id) AS max_price,
		(SELECT MIN(price) FROM ".$template."_cessions WHERE activation='1' && p_main=c.id) AS min_price,
		(SELECT name FROM ".$template."_developers WHERE activation='1' && id=c.developer) AS name_developer
		FROM ".$template."_m_catalogue_left AS c
		WHERE c.activation='1'".$add_params."
		ORDER BY ".$orderByCessionBuilding."
		LIMIT 0,".$LOAD_PAGE."
	") or die(mysql_error());
	$requestSearch = '';
	if(mysql_num_rows($res)>0){
		$res2 = mysql_query("SELECT FOUND_ROWS()", $db);
		$max_rows = mysql_result($res2, 0);
		while($row = mysql_fetch_assoc($res)){
			$link = $params[0].'/'.$row['id'];
			$flats_list = '';
			
			$flats = mysql_query("
				SELECT SQL_CALC_FOUND_ROWS n.*,
				(SELECT MIN(full_square) FROM ".$template."_cessions WHERE activation='1' && p_main='".$row['id']."' && rooms=n.rooms) AS full_square_min,
				(SELECT MAX(price) FROM ".$template."_cessions WHERE activation='1' && p_main='".$row['id']."' && rooms=n.rooms) AS max_price,
				(SELECT COUNT(id) FROM ".$template."_cessions WHERE p_main='".$row['id']."' && rooms=n.rooms && queue=n.queue) AS count_flats
				FROM ".$template."_cessions AS n
				WHERE n.activation='1' && n.p_main='".$row['id']."'
				GROUP BY rooms,queue
				ORDER BY n.rooms
			");
			if(mysql_num_rows($flats)>0){
				$flats_list = '<div class="row flats">';
				$n = 1;
				while($flat = mysql_fetch_assoc($flats)){
					$fone = '';
					if($n==1){
						$fone = ' fone';
						$n++;
					}
					else {
						$n=1;
					}
					$name_title = $flat['rooms'].'-комн.';
					if($flat['rooms']==0){
						$name_title = 'Студия';
					}
					$flats_list .= '<div class="row_flats'.$fone.'">
						<div class="name">'.$name_title.' от <strong>'.str_replace(".", ",", price_cell($flat['full_square_min'],1)).' м<sup>2</sup></strong></div>
						<div class="housing"><a href="'.$link.'">Корпус '.$flat['queue'].'</a></div>
						<div class="price">от <strong>'.price_cell($flat['max_price'],0).' руб.</strong></div>
						<div class="count_offers"><a href="'.$link.'">'.$flat['count_flats'].' в продаже</a></div>
					</div>';
				}
				$flats_list .= '</div>';
			}
			$exploitation = explode('_',$row['exploitation']);
			$deadline = '';
			if(!empty($row['exploitation'])){
				if(count($exploitation)>0){
					if(count($exploitation)>1){
						$deadline = '<span>'.$exploitation[1].' кв. '.$exploitation[0].'</span>';
					}
					else {
						$deadline = '<span>'.$exploitation[0].'</span>';
					}
				}
			}
			else {
				$queues = mysql_query("
					SELECT commissioning,queue
					FROM ".$template."_queues
					WHERE p_main='".$row['id']."' && type_name='cession'
					GROUP BY queue
				");
				if(mysql_num_rows($queues)>0){
					$deadline .= '<span>';
					$deadlineArray = array();
					while($queue = mysql_fetch_assoc($queues)){
						array_push($deadlineArray,$queue['queue'].' очередь - '.$queue['commissioning']);
					}
					for($q=0; $q<count($deadlineArray); $q++){
						$deadline .= implode(', ',$deadlineArray);
					}
					$deadline .= '</span>';
				}
			}
			
			$ex_image = explode(',',$row['images']);
			$image = '<img src="/admin_2/uploads/'.$ex_image[2].'">';
			
			$metro = '';
			if(!empty($row['station'])){
				$v = $row['dist_value'];
				if($v/1000>=1){
					$v = price_cell($v/1000,2).' км';
				}
				else {
					$v = price_cell($v,0).' м';
				}
				// $metro = '<div class="metro">&laquo;'.$row['station_name'].'&raquo; / <strong>'.$row['distance'].'</strong></div>';
				$metro = '<div class="metro">&laquo;'.$row['station_name'].'&raquo; / <strong>'.$v.'</strong></div>';
			}
			$link_developer = '/search?type=sell&estate=1&priceAll=all&developer='.$row['developer'];
			$type_developer = '';
			$type_deadline = '';
			if(!empty($row['name_developer'])){
				$type_developer = '<div class="type">Застройщик: <a href="'.$link_developer.'">'.$row['name_developer'].'</a></div>';
			}
			if(!empty($deadline)){
				$type_deadline = '<div class="type">Срок сдачи: '.$deadline.'</div>';
			}
			$requestSearch .= '<div id="id_'.$row['id'].'" class="item">
				<div class="left_block">
					<div class="title"><a href="'.$link.'">'.$row['name'].'</a></div>
					<div class="address">'.$row['address'].'</div>
					'.$metro.'
					<div class="image">
						<a href="'.$link.'">'.$image.'</a>
					</div>
				</div>
				<div class="right_block">
					<div class="row">
						<div class="ceil prices">
							<div class="full_price"><strong>'.price_cell($row['min_price'],0).'</strong><div class="max_price">- '.price_cell($row['max_price'],0).' руб.</div></div>
							<div class="meter_price">'.price_cell($row['min_price_meter'],0).' - '.price_cell($row['max_price_meter'],0).' руб/м<sup>2</sup></div>
						</div>
						<div class="ceil prices">
							'.$type_developer.'
							'.$type_deadline.'
						</div>
					</div>
					'.$flats_list.'
				</div>
			</div>';
		}
	}
	?>
	<div id="center">
		<h1 class="inner_pages">Новостройки</h1>
		<div class="sort_offers">
			<div class="count_offers">Подходящих предложений:<span><?=$max_rows?></span></div>
			<div class="sort_block">
				<div class="label_select">Сортировать:</div>
				<div class="select_block price">
					<input type="hidden" name="sort">
					<div class="choosed_block">По умолчанию</div>
					<div class="scrollbar-inner">
						<ul>
							<li class="current"><a href="javascript:void(0)" data-id="1">По умолчанию</a></li>
							<li><a href="javascript:void(0)" data-id="1">По цене</a></li>
							<li><a href="javascript:void(0)" data-id="1">По метражу</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div id="result_search">
			<div class="list_items">
				<?=$requestSearch?>
			</div>
		</div>
		<!--<div id="pager"><ul><li class="prev"><a title="Предыдущая" href="#"><i class="fa fa-angle-left"></i><span>Предыдущая</span></a></li><li><a href="#">1</a></li><li><a href="#">2</a></li><li class=current><span>3</span></li><li><a href="#">4</a></li><li><a href="#">5</a></li><li class="next"><a title="Следующая" href="#"><span>Следующая</span><i class="fa fa-angle-right"></i></a></li></ul></div>-->
	</div>
<?}

/* 
*  Поиск по переуступкам
*/
if(count($params)==1 && $arrayLinkSearch2 && $arrayLinkSearch2['estate']==6){
	$searchRequest = '';
	$searchRequest = array();
	$title_search_page = '';
	$orderByCessionBuilding = 'num';
	include("params_search_cession.php");
}
if(count($params)==2){
?>
<div id="center">
	<div class="title_page">
		<div class="location_card">
			<h1 class="inner_pages"><?=$PageInfo['h1']?></h1>
			<div class="address"><?=$PageInfo['address']?></div>
			<?
			if(!empty($PageInfo['station'])){
				$v = $PageInfo['dist_value'];
				if($v/1000>=1){
					$v = price_cell($v/1000,2).' км';
				}
				else {
					$v = price_cell($v,0).' м';
				}
				echo '<div class="metro">«'.$PageInfo['station_name'].'» / <strong>'.$v.'</strong></div>';
			}
			$exploitation = explode('_',$PageInfo['exploitation']);
			$deadline = '';
			if(!empty($PageInfo['exploitation'])){
				if(count($exploitation)>0){
					if(count($exploitation)>1){
						$deadline = '<span>'.$exploitation[1].' кв. '.$exploitation[0].'</span>';
					}
					else {
						$deadline = '<span>'.$exploitation[0].'</span>';
					}
				}
			}
			else {
				$deadline = '';
				$queues = mysql_query("
					SELECT commissioning,queue,building
					FROM ".$template."_queues
					WHERE p_main='".$PageInfo['id']."' && type_name='cession'
					ORDER BY queue, commissioning
				");
				if(mysql_num_rows($queues)>0){
					$deadline .= '<span>';
					$deadlineArray = array();
					$name_dead = '';
					while($queue = mysql_fetch_assoc($queues)){
						$name_dead = $queue['queue'].' очередь - '.$queue['commissioning'];
						if(!in_array($name_dead,$deadlineArray)){
							array_push($deadlineArray,$name_dead);
						}
					}
					$deadline .= implode(', ',$deadlineArray);
					$deadline .= '</span>';
				}
			}
			
			if(empty($PageInfo['min_square'])){
				$PageInfo['min_square'] = 1;
			}
			if(empty($PageInfo['max_square'])){
				$PageInfo['max_square'] = 1;
			}
			$square_meter_min = ceil($PageInfo['min_price']/$PageInfo['min_square']);
			$square_meter_max = ceil($PageInfo['max_price']/$PageInfo['max_square']);
			$link_developer = '/search?type=sell&estate=1&priceAll=all&developer='.$PageInfo['developer'];
			$type_developer = '';
			$type_deadline = '';
			if(!empty($PageInfo['name_developer'])){
				$type_developer = '<div class="type">Застройщик: <a href="'.$link_developer.'">'.$PageInfo['name_developer'].'</a></div>';
			}
			if(!empty($deadline)){
				$class = '';
				if(!empty($type_developer)){
					$class = ' have';
				}
				$type_deadline = '<div class="type'.$class.'">Срок сдачи: '.$deadline.'</div>';
			}
			?>
		</div>
		<div class="price_card">
			<div class="prices">
				<div class="full_price"><strong><?=price_cell($PageInfo['min_price'],0)?></strong><div class="max_price">- <?=price_cell($PageInfo['max_price'],0)?> руб.</div></div>
				<div class="meter_price"><?=price_cell($square_meter_min,0)?> - <?=price_cell($square_meter_max,0)?> руб/м<sup>2</sup></div>
			</div>
			<div class="developer">
				<?=$type_developer?>
				<?=$type_deadline?>
			</div>
		</div>
	</div>
	<div class="separate"></div>
	<div id="housing_estate">
		<div class="text_info">
			<?		
			/*
			*  Переуступки
			*/
			$res = mysql_query("
				SELECT n.*,
				(SELECT COUNT(id) FROM ".$template."_cessions WHERE p_main='".$PageInfo['id']."' && rooms=n.rooms && queue=n.queue) AS count_photo,
				(SELECT MIN(price) FROM ".$template."_cessions WHERE p_main='".$PageInfo['id']."' && rooms=n.rooms && queue=n.queue) AS min_price,
				(SELECT MIN(full_square) FROM ".$template."_cessions WHERE p_main='".$PageInfo['id']."' && rooms=n.rooms && queue=n.queue) AS min_full_square,
				(SELECT COUNT(id) FROM ".$template."_cessions WHERE p_main='".$PageInfo['id']."' && rooms=n.rooms && queue=n.queue) AS count_flats
				FROM ".$template."_cessions AS n
				WHERE n.activation='1' && n.p_main='".$PageInfo['id']."'
				GROUP BY rooms,queue
				ORDER BY rooms
			");
			if(mysql_num_rows($res)>0){
				echo '<div class="cessions page">';
				echo '<h2>Переуступка:</h2>';
				echo '<div class="list_flats">';
				$n = 1;
				while($row = mysql_fetch_assoc($res)){
					$fone = '';
					if($n==1){
						$fone = ' fone';
						$n++;
					}
					else {
						$n=1;
					}
					$name_title = $row['rooms'].'-комн.';
					if($row['rooms']==0){
						$name_title = 'Студия';
					}
					echo '<div class="row'.$fone.'">
						<i class="triangle-bottomleft '.$classRoom[$row['rooms']].'"></i>
						<div class="name">'.$name_title.'. от <strong>'.str_replace(".", ",", price_cell($row['min_full_square'],1)).' м<sup>2</sup></strong></div>
						<div class="housing"><a href="/cession?housing='.$row['queue'].'&estate=6&parent='.$PageInfo['id'].'">Корпус '.$row['queue'].'</a></div>
						<div class="price">от <strong>'.price_cell($row['min_price'],0).' руб.</strong></div>
						<div class="count_offers"><a href="/cession?housing='.$row['queue'].'&rooms='.$row['rooms'].'&estate=6&parent='.$PageInfo['id'].'">'.$row['count_flats'].' в продаже</a></div>
					</div>';
				}
				echo '</div>';
				echo '</div>';
			}

			$floors = '';
			$ready = '';
			$type_house = '';
			$queue = '';
			$ceiling_height = '';
			$parking = '';
			if(!empty($PageInfo['floors']) || !empty($PageInfo['ready']) || !empty($PageInfo['type_house']) || !empty($PageInfo['queue']) || !empty($PageInfo['ceiling_height']) || !empty($PageInfo['parking'])){
				
			}
			if(!empty($PageInfo['floors'])){
				if(empty($PageInfo['floors_live'])){
					$PageInfo['floors_live'] = $PageInfo['floors'];
				}
				$floors = '<div class="row">
					<div class="cell label">Количество этажей:</div>
					<div class="cell">'.$PageInfo['floors_live'].'-'.$PageInfo['floors'].' этажей</div>
				</div>';
			}
			if(!empty($PageInfo['ready'])){
				$r = mysql_query("
					SELECT name
					FROM ".$template."_ready
					WHERE id='".$PageInfo['ready']."'
				");
				if(mysql_num_rows($r)>0){
					$rw = mysql_fetch_assoc($r);
					$ready = '<div class="row">
						<div class="cell label">Стадия строительства:</div>
						<div class="cell">'.$rw['name'].'</div>
					</div>';
				}
			}
			if(!empty($PageInfo['type_house'])){
				$r = mysql_query("
					SELECT name
					FROM ".$template."_type_house
					WHERE id='".$PageInfo['type_house']."'
				");
				if(mysql_num_rows($r)>0){
					$rw = mysql_fetch_assoc($r);
					$type_house = '<div class="row">
						<div class="cell label">Тип дома:</div>
						<div class="cell">'.$rw['name'].'</div>
					</div>';
				}
			}
			if(!empty($PageInfo['queue_count'])){
				$queue = '<div class="row">
					<div class="cell label">Корпусов:</div>
					<div class="cell">'.$PageInfo['queue_count'].'</div>
				</div>';
			}
			if(!empty($PageInfo['ceiling_height'])){
				$ceiling_height = '<div class="row">
					<div class="cell label">Высота потолков:</div>
					<div class="cell">'.str_replace(".", ",", $PageInfo['ceiling_height']).' м</div>
				</div>';
			}
			if(!empty($PageInfo['parking'])){
				$parking = '<div class="row">
					<div class="cell label">Паркинг:</div>
					<div class="cell places"><span class="ok">есть</span>('.$PageInfo['parking'].' мест)</div>
				</div>';
			}
			echo '<div class="info_housing">
				'.$floors.'
				'.$ready.'
				'.$type_house.'
				'.$queue.'
				'.$ceiling_height.'
				'.$parking.'
			</div>';

			if(!empty($PageInfo['text'])){
				echo '<div class="text">'.$textPage.'</div>';
				echo '<div class="more_read"><a href="javascript:void(0)">Читать всё</a></div>';
			}
			?>
		</div>
		<div class="graph_info">
			<?
			$res = mysql_query("
				SELECT *,
				(SELECT COUNT(id) FROM ".$template."_photo_catalogue WHERE estate='jk' && p_main='".$PageInfo['id']."') AS count_photo
				FROM ".$template."_photo_catalogue
				WHERE estate='jk' && p_main='".$PageInfo['id']."'
				ORDER BY cover DESC, num
			");
			$numImages = 0;
			if(mysql_num_rows($res)>0){
				$numImages = mysql_num_rows($res);
				echo '<div class="images">';
				$n = 1;
				while($row = mysql_fetch_assoc($res)){
					$ex_images = explode(',',$row['images']);
					if($row['cover']==1){
						$max_rows = $row['count_photo'];
						$see_gallery = '';
						if($max_rows - 4 > 0){
							$max_rows = $max_rows - 4;
							if(substr($max_rows,-1,1)==1 && substr($max_rows,-2,2)!=11){
								$count_photo = '+'.$max_rows.' фотография';
							}
							else if(substr($max_rows,-1,1)==2 && substr($max_rows,-2,2)!=12 || substr($max_rows,-1,1)==3 && substr($max_rows,-2,2)!=13 || substr($max_rows,-1,1)==4 && substr($max_rows,-2,2)!=14){
								$count_photo = '+'.$max_rows.' фотографии';
							}
							else {
								$count_photo = '+'.$max_rows.' фотографий';
							}
							$see_gallery = '<div class="see_gallery">
								<a rel="prettyPhoto[img]" href="/admin_2/uploads/'.$ex_images[5].'"><span>'.$count_photo.'</span><i class="fa fa-search"></i></a><span>(смотреть всю галерею)</span>
							</div>';
						}
						echo '<div class="big_image">
							<a rel="prettyPhoto[img]" href="/admin_2/uploads/'.$ex_images[5].'"><img src="/admin_2/uploads/'.$ex_images[3].'"></a>
							'.$see_gallery.'
						</div>';
					}
					if($n==2){
						echo '<div class="mini_images">';
					}
					if($n>1){
						$hidden = '';
						if($n>4){
							$hidden = ' hidden';
						}
						echo '<div class="img'.$hidden.'"><a rel="prettyPhoto[img]" href="/admin_2/uploads/'.$ex_images[5].'"><img src="/admin_2/uploads/'.$ex_images[1].'"></a></div>';
					}
					if(mysql_num_rows($res)==$n){
						if($numImages>1){
							echo '</div>';
						}
					}
					$n++;
				}
				// if($numImages>1){
					echo '</div>';
				// }
			}
			?>
			<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
			<style>
				#map {
					width: 572px; height: 295px; padding: 0; margin: 0;
				}
			</style>
			<?
			$coordsYandex = '';
			$ex_coords = array('30.315868','59.939095');
			if(empty($PageInfo['coords'])){
				$notUpdate = false;
				$params2 = array(
					'geocode' => 'Санкт-Петербург, '.$PageInfo['address'],
					'format'  => 'json',
					'results' => 1,
					'key'     => 'AKDIIFYBAAAAqIYtRgIAZYb8P32hIxnbRRSe9CpggONv4PEAAAAAAAAAAADu6wMZJIEJ0SDd0mRe33ag1JRdNA==',
				);
				$response = json_decode(@file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params2)));
				
				if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0){
					$coordsYandex = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
				}
			}
			else {
				$coordsYandex = $PageInfo['coords'];
				$notUpdate = true;
			}
			if(!empty($coordsYandex)){
				if(!$notUpdate){
					mysql_query("
						UPDATE ".$template."_m_catalogue_left
						SET coords='".$coordsYandex."'
						WHERE id='".$PageInfo['id']."'
					");
				}
			}
			if(!empty($coordsYandex)){
				$ex_coords = explode(' ',$coordsYandex);
			}
			?>
			
			<script>
				ymaps.ready(function () {
					var myMap = new ymaps.Map('map', {
							center: [<?=$ex_coords[1]?>,<?=$ex_coords[0]?>],
							zoom: 13
						}, {
							searchControlProvider: 'yandex#search'
						}),
						myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
							hintContent: '<?=$PageInfo['name']?>',
							balloonContent: '<?=$PageInfo['address']?>'
						}, {
							// Опции.
							// Необходимо указать данный тип макета.
							iconLayout: 'default#image',
							// Своё изображение иконки метки.
							iconImageHref: '/images/<?=$_ICONS_MAPS[5]['card']?>',
							// Размеры метки.
							iconImageSize: [52, 44],
							// Смещение левого верхнего угла иконки относительно
							// её "ножки" (точки привязки).
							iconImageOffset: [-3, -42]
						});

					myMap.geoObjects.add(myPlacemark);
					
					var myRouter = ymaps.route([
					  'Санкт-Петербург, м. <?=$PageInfo['station_name']?>', {
						type: "viaPoint",                   
						point: "Санкт-Петербург, <?=$PageInfo['address']?>"
					  },
					], {
					  mapStateAutoApply: true 
					});
					
					myRouter.then(function(route) {
						/* Задание контента меток в начальной и 
						   конечной точках */
						var points = route.getWayPoints();
						points.options.set('preset', 'islands#blackStretchyIcon');
						points.get(0).properties.set("iconContent", 'Санкт-Петербург, м. <?=$PageInfo['station_name']?>');
						points.get(1).properties.set("iconContent", '<?=htmlspecialchars_decode($PageInfo['name'])?>');
						// Добавление маршрута на карту
						myMap.geoObjects.add(route);
						var routeLength = route.getHumanLength();
						var routeLength2 = route.getLength();
						$('<div class="full_route">Расстояние от <span>Санкт-Петербург, м. <?=$PageInfo['station_name']?></span> до <span><?=htmlspecialchars_decode($PageInfo['name'])?></span>: <strong>'+routeLength+'</strong></div>').insertAfter('#map');
						// alert(routeLength2);
					  },
					  // Обработка ошибки
					  function (error) {
						alert("Возникла ошибка: " + error.message);
					  }
					)
				});
			</script>
			<div class="map"><div id="map"></div></div>
		</div>
	</div>
	<?
	$res = mysql_query("
		SELECT c.*,
		(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=c.id && estate='jk') AS images,
		(SELECT MIN(price) FROM ".$template."_cessions WHERE activation='1' && p_main=c.id) AS min_price,
		(SELECT MAX(price) FROM ".$template."_cessions WHERE activation='1' && p_main=c.id) AS max_price,
		(SELECT MIN(full_square) FROM ".$template."_cessions WHERE activation='1' && p_main=c.id) AS min_square,
		(SELECT MAX(full_square) FROM ".$template."_cessions WHERE activation='1' && p_main=c.id) AS max_square,
		(SELECT COUNT(*) FROM ".$template."_cessions WHERE activation='1' && p_main=c.id) AS count_estate,
		(SELECT name FROM ".$template."_developers WHERE activation='1' && id=c.developer) AS name_developer
		FROM ".$template."_m_catalogue_left AS c
		WHERE c.activation='1' && c.id!='".$PageInfo['id']."'
		ORDER BY RAND()
		LIMIT 2
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		echo '<div class="our_offers">';
		echo '<h3>Похожие</h3>';
		echo '<div class="offers_list">';
		while($row = mysql_fetch_assoc($res)){
			$ex_image = explode(',',$row['images']);
			$image = '<img src="/admin_2/uploads/'.$ex_image[0].'">';
			$link = '/cession/'.$row['id'];
			$exploitation = explode('_',$row['exploitation']);
			$deadline = '';
			if(empty($row['min_square']) || empty($row['max_square'])){
				$square_meter_min = 0;
				$square_meter_max = 0;
			}
			else {
				$square_meter_min = ceil($row['min_price']/$row['min_square']);
				$square_meter_max = ceil($row['max_price']/$row['max_square']);
			}
			if(!empty($row['exploitation'])){
				if(count($exploitation)>0){
					if(count($exploitation)>1){
						$deadline = '<span>'.$exploitation[1].' кв. '.$exploitation[0].'</span>';
					}
					else {
						$deadline = '<span>'.$exploitation[0].'</span>';
					}
				}
			}
			else {
				$queues = mysql_query("
					SELECT commissioning,queue
					FROM ".$template."_queues
					WHERE p_main='".$row['id']."' && type_name='cession'
					GROUP BY queue
				");
				if(mysql_num_rows($queues)>0){
					$deadline .= '<span>';
					$deadlineArray = array();
					
					while($queue = mysql_fetch_assoc($queues)){
						array_push($deadlineArray,$queue['queue'].' очередь - '.$queue['commissioning']);
					}
					// for($q=0; $q<1; $q++){
						$deadline .= $deadlineArray[0];
					// }
					$deadline .= '</span>';
				}
			}
			
			$type_developer = '';
			$type_deadline = '';
			if(!empty($PageInfo['name_developer'])){
				$type_developer = '<div class="type_name">'.$row['name_developer'].'</div>';
			}
			if(!empty($deadline)){
				$type_deadline = '<div class="deadline">Срок сдачи: '.$deadline.'</div>';
			}

			echo '<div class="item">
				<div class="image">
					<a href="'.$link.'">'.$image.'</a>
				</div>
				<div class="body">
					<div class="location">
						<div class="title"><a href="'.$link.'">'.$row['name'].'</a></div>
						<div class="street">'.$row['address'].'</div>
						'.$type_developer.'
						'.$type_deadline.'
					</div>
					<div class="cost_block">
						<div class="price">от <strong>'.price_cell($row['min_price'],0).' руб.</strong></div>
						<div class="square">'.price_cell($square_meter_min,0).' - '.price_cell($square_meter_max,0).' руб/м<sup>2</sup></div>
						<div class="count_offers"><a href="'.$link.'">'.$row['count_estate'].' в продаже</a></div>
					</div>
				</div>
			</div>';
		}
		echo '</div>';
		echo '</div>';
	}
	?>
</div>	
<?
}
if(count($params)==3){
	if($params[2]=='housing'){

	}
	if($params[2]=='flats'){
		
	}

?>
<?}
if(count($params)==4){
	if($params[2]=='housing' && $housingsPage){
		
	}
	if($params[2]=='flat' && $flatsPageCession){
		include("include/mods/flat_open_cession.php");
	}
}
?>