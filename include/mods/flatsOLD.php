<?
$add_params = '';
$orderByFlats = 'num';
$LOAD_PAGE = 12;

$coordsArray = array();
$coordsArray2 = array();
if(count($params)==1){
	$res = mysql_query("
		SELECT SQL_CALC_FOUND_ROWS s.*,
		(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=s.id && estate='flats') AS images,
		(SELECT title FROM ".$template."_stations WHERE activation='1' && id=s.station) AS station_name,
		(SELECT name FROM ".$template."_location WHERE activation='1' && id=s.location) AS location_name
		FROM ".$template."_second_flats AS s
		WHERE s.activation='1'".$add_params."
		GROUP BY s.id
		ORDER BY ".$orderByFlats."
		LIMIT 0,".$LOAD_PAGE."
	");
	$requestSearch = '';
	if(mysql_num_rows($res)>0){
		$res2 = mysql_query("SELECT FOUND_ROWS()", $db);
		$max_rows = mysql_result($res2, 0);
		while($row = mysql_fetch_assoc($res)){
			$link = '/flats/'.$row['id'];
			$image = '<span>Нет фото</span>';
			if($row['images'] && !empty($row['images'])){
				$ex_image = explode(',',$row['images']);
				$image = '<img src="/admin_2/uploads/'.$ex_image[2].'">';
			}
			
			$metro = '';
			if(!empty($row['station'])){
				$metro = '<div class="metro">&laquo;'.$row['station_name'].'&raquo; / <strong>'.$row['distance'].'</strong></div>';
			}
			$name_title = $row['rooms'].'-комн. квартира';
			if($row['rooms']==0){
				$name_title = 'Квартира студия';
			}
			
			$square_meter = ceil($row['price']/$row['full_square']);
			$settings = '';
			$lift = 'нет';
			$wc = 'нет';
			$balcony = 'нет';
			$phone = 'нет';
			if(!empty($row['lift'])){
				$lift = 'есть';
			}
			if(!empty($row['wc'])){
				$wc = $_TYPE_WC[$row['wc']];
			}
			if(!empty($row['balcony'])){
				$balcony = $_TYPE_BALCONY[$row['balcony']];
			}
			if(!empty($row['phone'])){
				$phone = 'есть';
			}
			$floors_type = '&nbsp;'; //этаж 4/б
			if(!empty($row['floor'])){
				$floors_type = 'этаж '.$row['floor'];
			}
			if(!empty($row['floors'])){
				$floors_type .= '/'.$row['floors'];
			}
			$settings = '<span>Лифт: <i>'.$lift.'</i>,</span><span>Санузел: <i>'.$wc.'</i>,</span><span>Балкон: <i>'.$balcony.'</i>,</span><span>Телефон: <i>'.$phone.'</i>,</span>';
			$requestSearch .= '<div id="id_'.$row['id'].'" class="item">
				<div class="left_block">
					<div class="title"><a href="'.$link.'">'.$name_title.' <span><strong>'.str_replace(".", ",", price_cell($row['full_square'],1)).'/</strong>'.str_replace(".", ",", price_cell($row['live_square'],1)).' м<sup>2</sup></span></a></div>
					<div class="address">'.$row['address'].'</div>
					'.$metro.'
					<div class="image">
						<a href="'.$link.'">'.$image.'</a>
					</div>
				</div>
				<div class="right_block">
					<div class="row">
						<div class="ceil">
							<div class="full_price"><strong>'.price_cell($row['price'],0).'</strong> руб.</div>
							<div class="meter_price">'.price_cell($square_meter,0).' руб/м<sup>2</sup></div>
						</div>
						<div class="ceil">
							<div class="floor">'.$floors_type.'</div>
							<div class="type">Вторичка</div>
						</div>
					</div>
					<div class="row fone">
						<div class="footage">
							<div class="full">Общая <strong>'.str_replace(".", ",", price_cell($row['full_square'],1)).' м<sup>2</sup></strong></div>
							<div class="area">Жилая <strong>'.str_replace(".", ",", price_cell($row['live_square'],1)).' м<sup>2</sup></strong></div>
							<div class="rooms">Комнаты <strong>'.$row['rooms_square'].' м<sup>2</sup></strong></div>
							<div class="kitchen">Кухня <strong>'.$row['kitchen_square'].' м<sup>2</sup></strong></div>
						</div>
						<div class="settings">
							'.$settings.'
							<div class="more"><a href="'.$link.'">подробно</a></div>
						</div>
					</div>
				</div>
			</div>';
			
			// $params = array(
				// 'geocode' => $row['address'], // адрес
				// 'format'  => 'json',                          // формат ответа
				// 'results' => 1,                               // количество выводимых результатов
				// 'key'     => 'AKDIIFYBAAAAqIYtRgIAZYb8P32hIxnbRRSe9CpggONv4PEAAAAAAAAAAADu6wMZJIEJ0SDd0mRe33ag1JRdNA==',                           // ваш api key
			// );
			// $response = json_decode(@file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params)));
			
			// $coordsYandex = '';
			// if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0){
				// $coordsYandex = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
			// }
			// if(!empty($coordsYandex)){
				// $ex_coords = explode(' ',$coordsYandex);
				// $ex_coords2 = "[".$ex_coords[1].",".$ex_coords[0]."]";
				// array_push($coordsArray,$ex_coords);
				// array_push($coordsArray2,$ex_coords2);
			// }
			$title_search_page = 'Квартиры в г. '.$row['location_name'];
			if(isset($rooms_flat_sell_var) || isset($rooms_flat_rent_var)){
				$title_search_page = '';
				if(isset($rooms_flat_sell_var)){
					$roomsArray = $rooms_flat_sell_var;
				}
				if(isset($rooms_flat_rent_var)){
					$roomsArray = $rooms_flat_rent_var;
				}
				$exRoomsArray = explode(',',$roomsArray);
				$roomsArray = array();
				for($c=0; $c<count($exRoomsArray); $c++){
					$roomsName = 'Студии';
					if(!empty($exRoomsArray[$c])){
						$roomsName = $exRoomsArray[$c].'-комн.';
					}
					array_push($roomsArray,$roomsName);
				}
				$title_search_page = implode(', ',$roomsArray).' квартиры в г. '.$row['location_name'];
			}
		}
	}
	?>
	<div id="center">
		<h1 class="inner_pages">Вторичное жильё</h1>
		<div class="sort_offers">
			<div class="count_offers">Подходящих предложений:<span><?=$max_rows?></span></div>
			<div class="sort_block">
				<div class="label_select">Сортировать:</div>
				<div class="select_block price">
					<input type="hidden" name="sort">
					<div class="choosed_block">По умолчанию</div>
					<div class="scrollbar-inner">
						<ul>
							<li class="current"><a href="javascript:void(0)" data-id="1">По умолчанию</a></li>
							<li><a href="javascript:void(0)" data-id="1">По цене</a></li>
							<li><a href="javascript:void(0)" data-id="1">По метражу</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div id="result_search">
			<div class="list_items">
				<?=$requestSearch?>
			</div>
		</div>
		<!--<div id="pager"><ul><li class="prev"><a title="Предыдущая" href="#"><i class="fa fa-angle-left"></i><span>Предыдущая</span></a></li><li><a href="#">1</a></li><li><a href="#">2</a></li><li class=current><span>3</span></li><li><a href="#">4</a></li><li><a href="#">5</a></li><li class="next"><a title="Следующая" href="#"><span>Следующая</span><i class="fa fa-angle-right"></i></a></li></ul></div>-->
	</div>
<?}
if(count($params)==2){?>
<div id="center">
	<div class="title_page flat">
		<div class="location_card">
			<h1 class="inner_pages"><?=$PageInfo['h1']?></h1>
			<div class="address"><?=$PageInfo['address']?></div>
			<?
			if(!empty($PageInfo['station'])){
				$v = $PageInfo['dist_value'];
				if($v/1000>=1){
					$v = price_cell($v/1000,2).' км';
				}
				else {
					$v = price_cell($v,0).' м';
				}
				echo '<div class="metro">«'.$PageInfo['station_name'].'» / <strong>'.$v.'</strong></div>';
				// echo '<div class="metro">«'.$PageInfo['station_name'].'» / <strong>'.$PageInfo['distance'].'</strong></div>';
			}
			$square_meter = ceil($PageInfo['price']/$PageInfo['full_square']);
			?>
		</div>
		<div class="price_card">
			<div class="prices">
				<div class="full_price"><strong><?=price_cell($PageInfo['price'],0)?></strong> руб.</div>
				<div class="meter_price"><?=price_cell($square_meter,0)?> м<sup>2</sup></div>
				<div class="type">Вторичка</div>
			</div>
		</div>
	</div>
	<div class="separate"></div>
	<div class="card_estate" id="housing_estate">
		<div class="graph_info">
			<?
			$res = mysql_query("
				SELECT *,
				(SELECT COUNT(id) FROM ".$template."_photo_catalogue WHERE estate='flats' && p_main='".$PageInfo['id']."') AS count_photo
				FROM ".$template."_photo_catalogue
				WHERE estate='flats' && p_main='".$PageInfo['id']."'
				ORDER BY cover DESC, num
			");
			if(mysql_num_rows($res)>0){
				echo '<div class="images left_mini">';
				$n = 1;
				$big_image = '';
				while($row = mysql_fetch_assoc($res)){
					$ex_images = explode(',',$row['images']);
					if($row['cover']==1){
						$max_rows = $row['count_photo'];
						$see_gallery = '';
						if($max_rows - 4 > 0){
							$max_rows = $max_rows - 4;
							if(substr($max_rows,-1,1)==1 && substr($max_rows,-2,2)!=11){
								$count_photo = '+'.$max_rows.' фотография';
							}
							else if(substr($max_rows,-1,1)==2 && substr($max_rows,-2,2)!=12 || substr($max_rows,-1,1)==3 && substr($max_rows,-2,2)!=13 || substr($max_rows,-1,1)==4 && substr($max_rows,-2,2)!=14){
								$count_photo = '+'.$max_rows.' фотографии';
							}
							else {
								$count_photo = '+'.$max_rows.' фотографий';
							}
							if(!empty($row['user_id'])){
								$see_gallery = '<div class="see_gallery">
									<a rel="prettyPhoto[img]" href="/users/'.$row['user_id'].'/'.$ex_images[5].'"><span>'.$count_photo.'</span><i class="fa fa-search"></i></a><span>(смотреть всю галерею)</span>
								</div>';
							}
							else {
								$see_gallery = '<div class="see_gallery">
									<a rel="prettyPhoto[img]" href="/admin_2/uploads/'.$ex_images[5].'"><span>'.$count_photo.'</span><i class="fa fa-search"></i></a><span>(смотреть всю галерею)</span>
								</div>';
							}
						}
						if(!empty($row['user_id'])){
							$big_image = '<div class="big_image">
								<a class="big" style="background-image:url(/users/'.$row['user_id'].'/'.$ex_images[3].')" rel="prettyPhoto[img]" href="/users/'.$row['user_id'].'/'.$ex_images[5].'"></a>
								'.$see_gallery.'
							</div>';
						}
						else {
							$big_image = '<div class="big_image">
								<a class="big" style="background-image:url(/admin_2/uploads/'.$ex_images[3].')" rel="prettyPhoto[img]" href="/admin_2/uploads/'.$ex_images[5].'"></a>
								'.$see_gallery.'
							</div>';
						}
					}
					if($n==2){
						echo '<div class="mini_images">';
					}
					if($n>1){
						$hidden = '';
						if($n>4){
							$hidden = ' hidden';
						}
						if(!empty($row['user_id'])){
							echo '<div class="img'.$hidden.'"><a rel="prettyPhoto[img]" href="/users/'.$row['user_id'].'/'.$ex_images[5].'"><img src="/users/'.$row['user_id'].'/'.$ex_images[1].'"></a></div>';
						}
						else {
							echo '<div class="img'.$hidden.'"><a rel="prettyPhoto[img]" href="/admin_2/uploads/'.$ex_images[5].'"><img src="/admin_2/uploads/'.$ex_images[1].'"></a></div>';
						}
					}
					if(mysql_num_rows($res)==$n && $n>1){
						echo '</div>';
					}
					$n++;
				}
				echo $big_image;
				echo '</div>';
			}
			$floors = '';
			$type_house = '';
			$year_build = '';
			$queue = '';
			$ceiling_height = '';
			$lift = '';
			/*
			*  Кол-во этажей
			*/
			if(!empty($PageInfo['floors'])){
				$floors = '<div class="row">
					<div class="cell label">Этаж:</div>
					<div class="cell">'.$PageInfo['floor'].' (из '.$PageInfo['floors'].')</div>
				</div>';
			}
			/*
			*  Тип дома
			*/
			if(!empty($PageInfo['type_house'])){
				$r = mysql_query("
					SELECT name
					FROM ".$template."_type_build
					WHERE id='".$PageInfo['type_house']."'
				");
				if(mysql_num_rows($r)>0){
					$rw = mysql_fetch_assoc($r);
					$type_house = '<div class="row">
						<div class="cell label">Тип дома:</div>
						<div class="cell">'.$rw['name'].'</div>
					</div>';
				}
			}
			/*
			*  Год постройки
			*/
			if(!empty($PageInfo['year'])){
				$year_build = '<div class="row">
					<div class="cell label">Год постройки:</div>
					<div class="cell">'.$PageInfo['year'].'</div>
				</div>';
			}
			/*
			*  Корпус
			*/
			if(!empty($PageInfo['queue'])){
				$queue = '<div class="row">
					<div class="cell label">Корпус:</div>
					<div class="cell">'.$PageInfo['queue'].' корпус</div>
				</div>';
			}
			/*
			*  Высота потолков
			*/
			if(!empty($PageInfo['ceiling_height'])){
				$ceiling_height = '<div class="row">
					<div class="cell label">Высота потолков:</div>
					<div class="cell">'.str_replace(".", ",", $PageInfo['ceiling_height']).' м</div>
				</div>';
			}
			/*
			*  Лифт
			*/
			$lift_value = '<span class="no">нет</span>';
			if(!empty($PageInfo['lift'])){
				$lift_value = '<span class="ok">да</span>';
			}
			$lift = '<div class="row">
				<div class="cell label">Лифт:</div>
				<div class="cell places">'.$lift_value.'</div>
			</div>';
			
			/*
			*  Агент текущей недвижимости
			*/
			$agent_this_estate = '';
			if(!empty($PageInfo['agent'])){
				$agents = mysql_query("
					SELECT *
					FROM ".$template."_users
					WHERE activation='1' && id='".$PageInfo['agent']."'
				");
				if(mysql_num_rows($agents)>0){
					$agent = mysql_fetch_assoc($agents);
					$skype = '';
					$phones = '';
					if(!empty($agent['skype'])){
						$skype = '<div class="skype"><i class="fa fa-skype"></i>Skype: <a href="skype:'.$agent['skype'].'?call">'.$agent['skype'].'</a></div>';
					}
					$phones .= '<div class="phones">';
					if(!empty($agent['phone'])){
						$phones .= '<div class="main_phone">+7 <strong>'.substr($agent['phone'],4,3).' '.substr($agent['phone'],9,3).'-'.substr($agent['phone'],13,2).''.substr($agent['phone'],16,2).'</strong></div>';
					}
					if(!empty($agent['add_phone'])){
						$phones .= '<div class="add_phone">+7 <strong>'.substr($agent['add_phone'],4,3).' '.substr($agent['add_phone'],9,3).'-'.substr($agent['add_phone'],13,2).''.substr($agent['add_phone'],16,2).'</strong></div>';
					}
					else {
						$phones .= '<div class="add_phone">+7 <strong>'.substr($_MAINSET['phone_clear'],3,3).' '.substr($_MAINSET['phone_clear'],7,3).'-'.substr($_MAINSET['phone_clear'],11,2).substr($_MAINSET['phone_clear'],14,2).'</strong></div>';
					}
					$phones .= '</div>';
					$agent_this_estate = '<div class="agency_block card">
						<div class="title">
							<div class="title_left">Ваш агент:</div>
							<div class="title_right"><strong>'.$agent['name'].'</strong></div>
						</div>
						<div class="info_agent">
							<div class="send_request">
								<a onclick="return sendRequest(this)" href="javascript:void(0)">Оставить заявку</a>
							</div>
							'.$skype.'
							'.$phones.'
						</div>
					</div>';
				}
				else {
					$phones = '<div class="phones"><div class="main_phone">+7 <strong>'.substr($_MAINSET['phone_clear'],3,3).' '.substr($_MAINSET['phone_clear'],7,3).'-'.substr($_MAINSET['phone_clear'],11,2).substr($_MAINSET['phone_clear'],14,2).'</strong></div></div>';
					$agent_this_estate = '<div class="agency_block card">
						<div class="title">
							<div class="title_left">Оставьте заявку и мы с Вами свяжемся</div>
						</div>
						<div class="info_agent">
							<div class="send_request">
								<a onclick="return sendRequest(this)" href="javascript:void(0)">Оставить заявку</a>
							</div>
							'.$phones.'
						</div>
					</div>';
				}
			}
			else {
				$phones = '<div class="phones"><div class="main_phone">+7 <strong>'.substr($_MAINSET['phone_clear'],3,3).' '.substr($_MAINSET['phone_clear'],7,3).'-'.substr($_MAINSET['phone_clear'],11,2).substr($_MAINSET['phone_clear'],14,2).'</strong></div></div>';
				$agent_this_estate = '<div class="agency_block card">
					<div class="title">
						<div class="title_left">Оставьте заявку и мы с Вами свяжемся</div>
					</div>
					<div class="info_agent">
						<div class="send_request">
							<a onclick="return sendRequest(this)" href="javascript:void(0)">Оставить заявку</a>
						</div>
						'.$phones.'
					</div>
				</div>';
			}
			
			echo '<div class="info_housing">
				'.$floors.'
				'.$type_house.'
				'.$year_build.'
				'.$queue.'
				'.$ceiling_height.'
				'.$lift.'
			</div>';
			echo $agent_this_estate;
			?>
		</div>			
		<div class="text_info icons">
			<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
			<style>
				#map {
					width: 572px; height: 295px; padding: 0; margin: 0;
				}
			</style>
			<?
			$coordsYandex = '';
			$ex_coords = array('30.315868','59.939095');
			if(empty($row['coords'])){
				$params2 = array(
					'geocode' => $PageInfo['address'],
					'format'  => 'json',
					'results' => 1,
					'key'     => 'AKDIIFYBAAAAqIYtRgIAZYb8P32hIxnbRRSe9CpggONv4PEAAAAAAAAAAADu6wMZJIEJ0SDd0mRe33ag1JRdNA==',
				);
				$response = json_decode(@file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params2)));
				
				if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0){
					$coordsYandex = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
				}
			}
			else {
				$coordsYandex = $row['coords'];
			}
			if(!empty($coordsYandex)){
				$ex_coords = explode(' ',$coordsYandex);
			}
			
			?>
			
			<script>
				ymaps.ready(function () {
					var myMap = new ymaps.Map('map', {
							center: [<?=$ex_coords[1]?>,<?=$ex_coords[0]?>],
							zoom: 13
						}, {
							searchControlProvider: 'yandex#search'
						}),
						myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
							hintContent: '<?=$PageInfo['name']?>',
							balloonContent: '<?=$PageInfo['address']?>'
						}, {
							// Опции.
							// Необходимо указать данный тип макета.
							iconLayout: 'default#image',
							// Своё изображение иконки метки.
							iconImageHref: '/images/<?=$_ICONS_MAPS[$PageInfo['rooms']]['card']?>',
							// Размеры метки.
							iconImageSize: [52, 44],
							// Смещение левого верхнего угла иконки относительно
							// её "ножки" (точки привязки).
							iconImageOffset: [-3, -42]
						});

					myMap.geoObjects.add(myPlacemark);
					var myRouter = ymaps.route([
					  'Санкт-Петербург, м. <?=$PageInfo['station_name']?>', {
						type: "viaPoint",                   
						point: "Санкт-Петербург, <?=$PageInfo['address']?>"
					  },
					], {
					  mapStateAutoApply: true 
					});
					
					myRouter.then(function(route) {
						/* Задание контента меток в начальной и 
						   конечной точках */
						var points = route.getWayPoints();
						points.options.set('preset', 'islands#blackStretchyIcon');
						points.get(0).properties.set("iconContent", 'Санкт-Петербург, м. <?=$PageInfo['station_name']?>');
						points.get(1).properties.set("iconContent", '<?=htmlspecialchars_decode($PageInfo['name'])?>');
						// Добавление маршрута на карту
						myMap.geoObjects.add(route);
						var routeLength = route.getHumanLength();
						var routeLength2 = route.getLength();
						$('<div class="full_route">Расстояние от <span>Санкт-Петербург, м. <?=$PageInfo['station_name']?></span> до <span><?=htmlspecialchars_decode($PageInfo['name'])?></span>: <strong>'+routeLength+'</strong></div>').insertAfter('#map');
						var parent = arrLinkPage[1];
						var id = arrLinkPage[2];
						var data = "buildDist="+routeLength2+"&parent="+parent+"&id="+id;
						xhr = $.ajax({
							type: "POST",
							url: '/include/handler.php',
							data: data+"&rnd="+Math.random(),
							dataType: "json",
							success: function (data, textStatus) {
								var json = eval(data);
								if(json.action!='error'){
									// window.location = window.location;
								}
								else {
									alert('Возникла системная ошибка! Данные не сохранены. Попробуйте позднее.');
								}
							}
						});
						// alert(routeLength2);
					  },
					  // Обработка ошибки
					  function (error) {
						alert("Возникла ошибка: " + error.message);
					  }
					)
				});
			</script>
			<div class="map"><div id="map"></div></div>
			<div class="info_housing">
				<div class="row">
					<div class="cell label">Общая площадь:</div>
					<div class="cell"><strong><?=$PageInfo['full_square']?> м<sup>2</sup></strong></div>
				</div>
				<div class="row">
					<div class="cell label">Жилая площадь:</div>
					<div class="cell"><strong><?=$PageInfo['live_square']?> м<sup>2</sup></strong></div>
				</div>
				<div class="row">
					<div class="cell label">Площадь комнат:</div>
					<div class="cell"><?=$PageInfo['rooms_square']?> м<sup>2</sup></div>
				</div>
				<div class="row">
					<div class="cell label">Площадь кухни:</div>
					<div class="cell"><?=$PageInfo['kitchen_square']?> м<sup>2</sup></div>
				</div>
				<div class="row">
					<div class="cell label">Санузел:</div>
					<div class="cell"><?=$_TYPE_WC_ADMIN[$PageInfo['wc']]?></div>
				</div>
				<div class="row">
					<div class="cell label">Балкон:</div>
					<?
					$balcony = '<span class="no">'.$_TYPE_BALCONY_ADMIN[$PageInfo['balcony']].'</span>';
					if(!empty($PageInfo['balcony'])){
						$balcony = '<span class="ok">'.$_TYPE_BALCONY_ADMIN[$PageInfo['balcony']].'</span>';
					}
					?>
					<div class="cell places"><?=$balcony?></div>
				</div>
				<?
				$phone = '<span class="no">нет</span>';
				if(!empty($PageInfo['phone'])){
					$phone = '<span class="ok">есть</span>';
				}
				?>
				<div class="row">
					<div class="cell label">Телефон:</div>
					<div class="cell places"><?=$phone?></div>
				</div>
				<?
				if(!empty($PageInfo['short_text'])){
					echo '<div class="row">
						<div class="cell label add">Дополнительно:</div>
						<div class="cell">'.nl2br($PageInfo['short_text']).'</div>
					</div>';
				}
				?>
			</div>
			<?
			if(!empty($PageInfo['text'])){
				echo '<div class="text">'.$textPage.'</div>';
				echo '<div class="more_read"><a href="javascript:void(0)">Читать всё</a></div>';
			}
			?>
		</div>
	</div>
	<?
	$item_little = '';
	$item_big = '';
	$res = mysql_query("
		SELECT s.*,
		(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=s.id && estate='flats') AS images
		FROM ".$template."_second_flats AS s
		WHERE s.activation='1' && s.id!='".$PageInfo['id']."' && s.price<'".$PageInfo['price']."' && s.rooms=IF((SELECT COUNT(*) FROM ".$template."_second_flats WHERE activation='1' && rooms='".$PageInfo['rooms']."' && price<'".$PageInfo['price']."')>0, '".$PageInfo['rooms']."', '".($PageInfo['rooms']-1)."') && s.type='".$PageInfo['type']."'
		ORDER BY RAND()
		LIMIT 1
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$row = mysql_fetch_assoc($res);
		$link = '/flats/'.$row['id'];
		$image = '<span>Нет фото</span>';
		if(!empty($row['images'])){
			$ex_image = explode(',',$row['images']);
			$image = '<img src="/admin_2/uploads/'.$ex_image[0].'">';
		}
		$square_meter = ceil($row['price']/$row['full_square']);
		$name_title = $row['rooms'].'-комн. кв';
		if($row['rooms']==0){
			$name_title = 'Студия';
		}
		$item_little = '<div class="item little">
			<div class="image">
				<a href="'.$link.'">'.$image.'</a>
			</div>
			<div class="body">
				<div class="location">
					<div class="title"><a href="'.$link.'">'.$name_title.' <strong>'.str_replace(".", ",", price_cell($row['full_square'],1)).' м<sup>2</sup></strong></a></div>
					<div class="comm">('.str_replace(".", ",", price_cell($row['live_square'],1)).' м<sup>2</sup> жилая)</div>
					<div class="price"><strong>'.price_cell($row['price'],0).' руб.</strong></div>
					<div class="square">'.price_cell($square_meter,0).' руб/м<sup>2</sup></div>
					<div class="type">этаж '.$row['floor'].'/'.$row['floors'].'</div>
				</div>
			</div>
		</div>';
	}
	$res = mysql_query("
		SELECT s.*,
		(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=s.id && estate='flats') AS images
		FROM ".$template."_second_flats AS s
		WHERE s.activation='1' && s.id!='".$PageInfo['id']."' && s.rooms='".$PageInfo['rooms']."' && s.full_square>'".$PageInfo['full_square']."' && s.id!='".$PageInfo['id']."' && s.type='".$PageInfo['type']."'
		ORDER BY full_square
		LIMIT 1
	") or die(mysql_error());
	$meterBig = '';
	if(mysql_num_rows($res)>0){
		$row = mysql_fetch_assoc($res);
		$link = '/flats/'.$row['id'];
		$image = '<span>Нет фото</span>';
		if(!empty($row['images'])){
			$ex_image = explode(',',$row['images']);
			$image = '<img src="/admin_2/uploads/'.$ex_image[0].'">';
		}
		$meterBig = price_cell($row['full_square'] - $PageInfo['full_square'],1);
		$square_meter = ceil($row['price']/$row['full_square']);
		$name_title = $row['rooms'].'-комн. квартира';
		if($row['rooms']==0){
			$name_title = 'Студия';
		}
		$item_big = '<div class="item big">
			<div class="image">
				<a href="'.$link.'">'.$image.'</a>
			</div>
			<div class="body">
				<div class="location">
					<div class="title"><a href="'.$link.'">'.$name_title.' <strong>'.str_replace(".", ",", price_cell($row['full_square'],1)).' м<sup>2</sup></strong></a></div>
					<div class="comm">('.str_replace(".", ",", price_cell($row['live_square'],1)).' м<sup>2</sup> жилая)</div>
					<div class="street">'.$row['address'].'</div>
					<div class="type_name">'.$row['name_developer'].'</div>
				</div>
				<div class="cost_block">
					<div class="price"><strong>'.price_cell($row['price'],0).' руб.</strong></div>
					<div class="square">'.price_cell($square_meter,0).' руб/м<sup>2</sup></div>
					<div class="count_offers">этаж '.$row['floor'].'/'.$row['floors'].'</div>
				</div>
			</div>
		</div>';
	}
	echo '<div class="our_offers">';
	echo '<div class="second_title">';
			if(!empty($item_little)){
				echo '<h3 class="little">Ищите дешевле?</h3>';
			}
			if(!empty($meterBig)){
				echo '<h3><span class="baloon"></span>На '.$meterBig.' м<sup>2</sup> больше</h3>';
			}
		echo '</div>';
		if(!empty($item_little) || !empty($item_big)){
			echo '<div class="offers_list">';
			echo $item_little;
			echo $item_big;
			echo '</div>';
		}
	echo '</div>';
	?>
</div>	
<?}
if(count($params)==3){
	if($params[2]=='housing'){
		
	}
	if($params[2]=='flats'){
		
	}

?>
<?}
// if(count($params)==4){
	// include("include/mods/flat_open_new.php");
// }
?>