<?
$add_params = '';
$orderByNewBuilding = 'num';
$LOAD_PAGE = 12;

if(count($params)==1 && !$arrayLinkSearch2){
	$res = mysql_query("
		SELECT SQL_CALC_FOUND_ROWS c.*,
		(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=c.id && estate='jk') AS images,
		(SELECT title FROM ".$template."_stations WHERE activation='1' && id=c.station) AS station_name,
		(SELECT MAX(price) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS max_price,
		(SELECT MIN(price) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS min_price,
		(SELECT MIN(price_meter) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS min_price_meter,
		(SELECT MAX(price_meter) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS max_price_meter,
		(SELECT name FROM ".$template."_developers WHERE activation='1' && id=c.developer) AS name_developer
		FROM ".$template."_m_catalogue_left AS c
		WHERE c.activation='1'".$add_params."
		ORDER BY ".$orderByNewBuilding."
		LIMIT 0,".$LOAD_PAGE."
	");
	$requestSearch = '';
	if(mysql_num_rows($res)>0){
		$res2 = mysql_query("SELECT FOUND_ROWS()", $db);
		$max_rows = mysql_result($res2, 0);
		while($row = mysql_fetch_assoc($res)){
			$link = $params[0].'/'.$row['id'];
			$flats_list = '';
			
			$flats = mysql_query("
				SELECT SQL_CALC_FOUND_ROWS n.*,
				(SELECT MIN(full_square) FROM ".$template."_new_flats WHERE activation='1' && p_main='".$row['id']."' && rooms=n.rooms) AS full_square_min,
				(SELECT MAX(price) FROM ".$template."_new_flats WHERE activation='1' && p_main='".$row['id']."' && rooms=n.rooms) AS max_price,
				(SELECT COUNT(id) FROM ".$template."_new_flats WHERE p_main='".$row['id']."' && rooms=n.rooms && queue=n.queue) AS count_flats
				FROM ".$template."_new_flats AS n
				WHERE n.activation='1' && n.p_main='".$row['id']."'
				GROUP BY rooms,queue
				ORDER BY n.rooms
			");
			if(mysql_num_rows($flats)>0){
				$flats_list = '<div class="row flats">';
				$n = 1;
				while($flat = mysql_fetch_assoc($flats)){
					$fone = '';
					if($n==1){
						$fone = ' fone';
						$n++;
					}
					else {
						$n=1;
					}
					$name_title = $flat['rooms'].'-комн.';
					if($flat['rooms']==0){
						$name_title = 'Студия';
					}
					$flats_list .= '<div class="row_flats'.$fone.'">
						<div class="name">'.$name_title.' от <strong>'.str_replace(".", ",", price_cell($flat['full_square_min'],1)).' м<sup>2</sup></strong></div>
						<div class="housing"><a href="'.$link.'">Корпус '.$flat['queue'].'</a></div>
						<div class="price">от <strong>'.price_cell($flat['max_price'],0).' ₽</strong></div>
						<div class="count_offers"><a href="'.$link.'">'.$flat['count_flats'].' в продаже</a></div>
					</div>';
				}
				$flats_list .= '</div>';
			}
			$exploitation = explode('_',$row['exploitation']);
			$deadline = '';
			if(!empty($row['exploitation'])){
				if(count($exploitation)>0){
					if(count($exploitation)>1){
						$deadline = '<span>'.$exploitation[1].' кв. '.$exploitation[0].'</span>';
					}
					else {
						$deadline = '<span>'.$exploitation[0].'</span>';
					}
				}
			}
			else {
				$queues = mysql_query("
					SELECT commissioning,queue
					FROM ".$template."_queues
					WHERE p_main='".$row['id']."' && type_name='new'
					GROUP BY queue
				");
				if(mysql_num_rows($queues)>0){
					$deadline .= '<span>';
					$deadlineArray = array();
					while($queue = mysql_fetch_assoc($queues)){
						array_push($deadlineArray,$queue['queue'].' очередь - '.$queue['commissioning']);
					}
					for($q=0; $q<count($deadlineArray); $q++){
						$deadline .= implode(', ',$deadlineArray);
					}
					$deadline .= '</span>';
				}
			}
			
			$ex_image = explode(',',$row['images']);
			$image = '<img src="/admin_2/uploads/'.$ex_image[2].'">';
			
			$metro = '';
			if(!empty($row['station'])){
				$v = $row['dist_value'];
				if($v/1000>=1){
					$v = price_cell($v/1000,2).' км';
				}
				else {
					$v = price_cell($v,0).' м';
				}
				// $metro = '<div class="metro">&laquo;'.$row['station_name'].'&raquo; / <strong>'.$row['distance'].'</strong></div>';
				$metro = '<div class="metro">&laquo;'.$row['station_name'].'&raquo; / <strong>'.$v.'</strong></div>';
			}
			$link_developer = '/search?type=sell&estate=1&priceAll=all&developer='.$row['developer'];
			$type_developer = '';
			$type_deadline = '';
			if(!empty($row['name_developer'])){
				$type_developer = '<div class="type">Застройщик: <a href="'.$link_developer.'">'.$row['name_developer'].'</a></div>';
			}
			if(!empty($deadline)){
				$type_deadline = '<div class="type">Срок сдачи: '.$deadline.'</div>';
			}
			$requestSearch .= '<div id="id_'.$row['id'].'" class="item">
				<div class="left_block">
					<div class="title"><a href="'.$link.'">'.$row['name'].'</a></div>
					<div class="address">'.$row['address'].'</div>
					'.$metro.'
					<div class="image">
						<a href="'.$link.'">'.$image.'</a>
					</div>
				</div>
				<div class="right_block">
					<div class="row">
						<div class="ceil prices">
							<div class="full_price"><strong>'.price_cell($row['min_price'],0).'</strong><div class="max_price">- '.price_cell($row['max_price'],0).' ₽</div></div>
							<div class="meter_price">'.price_cell($row['min_price_meter'],0).' - '.price_cell($row['max_price_meter'],0).' ₽/м<sup>2</sup></div>
						</div>
						<div class="ceil prices">
							'.$type_developer.'
							'.$type_deadline.'
						</div>
					</div>
					'.$flats_list.'
				</div>
			</div>';
		}
	}
	?>
	<div id="center">
		<h1 class="inner_pages">Новостройки</h1>
		<div class="sort_offers">
			<div class="count_offers">Подходящих предложений:<span><?=$max_rows?></span></div>
			<div class="sort_block">
				<div class="label_select">Сортировать:</div>
				<div class="select_block price">
					<input type="hidden" name="sort">
					<div class="choosed_block">По умолчанию</div>
					<div class="scrollbar-inner">
						<ul>
							<li class="current"><a href="javascript:void(0)" data-id="1">По умолчанию</a></li>
							<li><a href="javascript:void(0)" data-id="1">По цене</a></li>
							<li><a href="javascript:void(0)" data-id="1">По метражу</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div id="result_search">
			<div class="list_items">
				<?=$requestSearch?>
			</div>
		</div>
		<!--<div id="pager"><ul><li class="prev"><a title="Предыдущая" href="#"><i class="fa fa-angle-left"></i><span>Предыдущая</span></a></li><li><a href="#">1</a></li><li><a href="#">2</a></li><li class=current><span>3</span></li><li><a href="#">4</a></li><li><a href="#">5</a></li><li class="next"><a title="Следующая" href="#"><span>Следующая</span><i class="fa fa-angle-right"></i></a></li></ul></div>-->
	</div>
<?}

/* 
*  Поиск по новостройкам
*/
if(count($params)==1 && $arrayLinkSearch2 && $arrayLinkSearch2['estate']==1){
	$searchRequest = '';
	$searchRequest = array();
	$title_search_page = '';
	$orderByNewBuilding = 'num';
	include("params_search_new.php");
}

/* 
*  Поиск по переуступкам
*/
if(count($params)==1 && $arrayLinkSearch2 && $arrayLinkSearch2['estate']==6){
	$searchRequest = '';
	$searchRequest = array();
	$title_search_page = '';
	$orderByNewBuilding = 'num';
	include("params_search_cession.php");
}
if(count($params)==2){
	$metroStation = '';
	if(!empty($PageInfo['station'])){
		$v = $PageInfo['dist_value'];
		if($v/1000>=1){
			$v = price_cell($v/1000,2).' км';
		}
		else {
			$v = price_cell($v,0).' м';
		}
		$metroStation = '<div class="row">
				<div class="cell label">Метро:</div>
				<div class="cell text"><div class="metro">«'.$PageInfo['station_name'].'» / <strong>'.$v.'</strong></div></div>
			</div>';
	}
	
	$areaRow = '';
	if(!empty($PageInfo['area']) && !empty($PageInfo['area_name'])){
		$areaRow = '<div class="row">
			<div class="cell label">Район:</div>
			<div class="cell text"><a href="/search?type=sell&estate=1&priceAll=all&area='.$PageInfo['area'].'">'.$PageInfo['area_name'].'</a></div>
		</div>';
	}
		
	$addressName = '';
	if(!empty($PageInfo['address'])){
		$addressName = '<div class="row">
			<div class="cell label">Адрес:</div>
			<div class="cell text"><div class="address">'.$PageInfo['address'].'</div></div>
		</div>';
	}
	if(!empty($PageInfo['short_address'])){
		$localName = $PageInfo['location_name'];
		if($PageInfo['location_name']=='Санкт-Петербург'){
			$localName = 'СПб';
		}
		if($PageInfo['location_name']=='Ленинградская область'){
			$localName = 'ЛО';
		}
		if($PageInfo['location_name']=='Московская область'){
			$localName = 'МО';
		}
		if($PageInfo['location_name']=='Москва'){
			$localName = 'МСК';
		}
		$arrayAddress = array($localName);
		
		if(!empty($PageInfo['area_name'])){
			$areaName = $PageInfo['area_name'];
			$pos = stripos($PageInfo['area_name'], 'г.');
			if($pos===false){
				$pos = stripos($PageInfo['area_name'], 'р-н');
				if($pos===false){
					$areaName = $PageInfo['area_name'].' р-н, ';
				}
				else {
					$areaName = $PageInfo['area_name'];
				}
			}
			else {
				$areaName = $PageInfo['area_name'];			
			}
			array_push($arrayAddress,$areaName);
		}
		
		array_push($arrayAddress,$PageInfo['short_address']);
		$addressName = '<div class="row">
			<div class="cell label">Адрес:</div>
			<div class="cell text"><div class="address">'.implode(', ',$arrayAddress).'</div></div>
		</div>';
	}
?>
<div id="center">
	<div class="title_page new">
		<div class="location_card">
			<div class="table">
				<div class="row">
					<div class="cell label">
						<strong>ID: <?=$PageInfo['id']?></strong>
					</div>
					<div class="cell text">
						<h1 class="inner_pages"><?=$PageInfo['h1']?></h1>
					</div>
				</div>
				<?=$addressName?>
				<?=$areaRow?>
				<?=$metroStation?>
			</div>
			<?			
			if(empty($PageInfo['min_square'])){
				$PageInfo['min_square'] = 1;
			}
			if(empty($PageInfo['max_square'])){
				$PageInfo['max_square'] = 1;
			}
			$square_meter_min = ceil($PageInfo['min_square']);
			$square_meter_max = ceil($PageInfo['max_square']);
			
			if($PageInfo['min_price']==0 || $PageInfo['max_price']==0){
				$min_full_square = 1;
				$min_price = 1;
				$max_full_square = 1;
				$max_price = 1;
				$min_prices = mysql_query("
					SELECT MIN(price) AS min_price,
					(SELECT COUNT(*) FROM ".$template."_cessions WHERE activation='1' && p_main='".$PageInfo['id']."') AS count
					FROM ".$template."_cessions
					WHERE activation='1' && p_main='".$PageInfo['id']."'
				");
				if(mysql_num_rows($min_prices)>0){
					$min_price2 = mysql_fetch_assoc($min_prices);
					if($min_price2['count']>0){
						$min_price = $min_price2['min_price'];
					}
				}
				$min_full_squares = mysql_query("
					SELECT MIN(full_square) AS min_square,
					(SELECT COUNT(*) FROM ".$template."_cessions WHERE activation='1' && p_main='".$PageInfo['id']."') AS count
					FROM ".$template."_cessions
					WHERE activation='1' && p_main='".$PageInfo['id']."'
				");
				if(mysql_num_rows($min_full_squares)>0){
					$min_full_square2 = mysql_fetch_assoc($min_full_squares);
					if($min_full_square2['count']>0){
						$min_full_square = $min_full_square2['min_square'];
					}
					$square_meter_min = ceil($min_price/$min_full_square);
				}
				
				$max_prices = mysql_query("
					SELECT MAX(price) AS max_price,
					(SELECT COUNT(*) FROM ".$template."_cessions WHERE activation='1' && p_main='".$PageInfo['id']."') AS count
					FROM ".$template."_cessions
					WHERE activation='1' && p_main='".$PageInfo['id']."'
				");
				if(mysql_num_rows($max_prices)>0){
					$min_price2 = mysql_fetch_assoc($max_prices);
					if($min_price2['count']>0){
						$max_price = $min_price2['max_price'];
					}
				}
				$max_full_squares = mysql_query("
					SELECT MAX(price/full_square) AS max_square,
					(SELECT COUNT(*) FROM ".$template."_new_flats WHERE activation='1' && p_main='".$PageInfo['id']."') AS count
					FROM ".$template."_new_flats
					WHERE activation='1' && p_main='".$PageInfo['id']."'
				");
				if(mysql_num_rows($max_full_squares)>0){
					$max_full_square2 = mysql_fetch_assoc($max_full_squares);
					if($max_full_square2['count']>0){
						$max_full_square = $max_full_square2['max_square'];
					}
					if(empty($max_full_square)){
						$max_full_square = 1;
					}
					$square_meter_max = ceil($max_price/$max_full_square);
				}
				$PageInfo['min_price'] = $min_price;
				$PageInfo['max_price'] = $max_price;
			}
			$link_developer = '/search?type=sell&estate=1&priceAll=all&developer='.$PageInfo['developer'];
			$type_developer = '';
			$type_deadline = '';
			if(!empty($PageInfo['name_developer'])){
				$type_developer = '<div class="type">Застройщик: <a href="'.$link_developer.'">'.$PageInfo['name_developer'].'</a></div>';
			}
			if(!empty($deadline)){
				$class = '';
				if(!empty($type_developer)){
					$class = ' have';
				}
				$type_deadline = '<div class="type'.$class.'">Срок сдачи: '.$deadline.'</div>';
			}
			?>
		</div>
		<div class="price_card">
			<div class="prices">
				<div class="full_price"><strong><?=price_cell($PageInfo['min_price'],0)?></strong><div class="max_price">- <?=price_cell($PageInfo['max_price'],0)?> ₽</div></div>
				<div class="meter_price"><?=price_cell($square_meter_min,0)?> - <?=price_cell($square_meter_max,0)?> ₽/м<sup>2</sup></div>
			</div>
		</div>
		<?
		/*
		*  В продаже новостройки
		*/
		$res = mysql_query("
			SELECT n.*,
			(SELECT COUNT(id) FROM ".$template."_new_flats WHERE p_main='".$PageInfo['id']."' && rooms=n.rooms) AS count_photo,
			(SELECT MIN(price) FROM ".$template."_new_flats WHERE p_main='".$PageInfo['id']."' && rooms=n.rooms) AS min_price,
			(SELECT MAX(price) FROM ".$template."_new_flats WHERE p_main='".$PageInfo['id']."' && rooms=n.rooms) AS max_price,
			(SELECT MIN(full_square) FROM ".$template."_new_flats WHERE p_main='".$PageInfo['id']."' && rooms=n.rooms) AS min_full_square,
			(SELECT MAX(full_square) FROM ".$template."_new_flats WHERE p_main='".$PageInfo['id']."' && rooms=n.rooms) AS max_full_square,
			(SELECT COUNT(id) FROM ".$template."_new_flats WHERE p_main='".$PageInfo['id']."' && rooms=n.rooms) AS count_flats
			FROM ".$template."_new_flats AS n
			WHERE n.activation='1' && n.p_main='".$PageInfo['id']."'
			GROUP BY rooms
			ORDER BY rooms
		");
		if(mysql_num_rows($res)>0){
			// echo '<h2>В продаже:</h2>';
			echo '<div class="list_flats">';
			$n = 1;
			while($row = mysql_fetch_assoc($res)){
				$fone = '';
				if($n==1){
					$fone = ' fone';
					$n++;
				}
				else {
					$n=1;
				}
				$name_title = $row['rooms'].'-комн.';
				if($row['rooms']==0){
					$name_title = 'Студия';
				}
				echo '<div class="row'.$fone.'">
					<i class="triangle-bottomleft '.$classRoom[$row['rooms']].'"></i>
					<div style="width:240px" class="name">'.$name_title.' от <strong>'.str_replace(".", ",", price_cell($row['min_full_square'],1)).' м<sup>2</sup></strong> до <strong>'.str_replace(".", ",", price_cell($row['max_full_square'],1)).' м<sup>2</sup></strong></div>
					<!--<div class="housing"><a href="/newbuilding?housing='.$row['queue'].'&estate=1&parent='.$PageInfo['id'].'">Корпус '.$row['queue'].'</a></div>-->
					<div style="width:235px" class="price"><a href="/newbuilding?housing='.$row['queue'].'&rooms='.$row['rooms'].'&estate=1&parent='.$PageInfo['id'].'"><strong>'.price_cell($row['min_price'],0).' ₽</strong> - <strong>'.price_cell($row['max_price'],0).' ₽</strong></a></div>
					<!--<div class="count_offers"><a href="/newbuilding?housing='.$row['queue'].'&rooms='.$row['rooms'].'&estate=1&parent='.$PageInfo['id'].'">'.$row['count_flats'].' в продаже</a></div>-->
				</div>';
			}
			echo '</div>';
		}
		?>
	</div>
	<div class="separate"></div>
	
	
	
	<div class="card_estate new_card newbuilding" id="housing_estate">
		<?
			$res = mysql_query("
				SELECT *
				FROM ".$template."_photo_catalogue
				WHERE estate='jk' && p_main='".$PageInfo['id']."'
				ORDER BY cover DESC, num
			");
			if(mysql_num_rows($res)>0){
				echo '<div class="fotogallery">';
				echo '<div class="fotorama fotorama-skin-rsp fotorama-skin-rsp_hidden"
					 data-nav="thumbs"
					 data-height="421"
					 data-keyboard="true"
					 data-navposition="bottom"
					 data-loop="true"
					 data-allowfullscreen="native"
					 data-fit="contain">';
				while($row = mysql_fetch_assoc($res)){
					$ex_images = explode(',',$row['images']);
					if($row['cover']==1){
						$linkImgMid = '/admin_2/uploads/'.$ex_images[5];
						$linkImgMini = '/admin_2/uploads/'.$ex_images[5];
						if($row['user_id']!=0){
							$linkImgMid = '/users/'.$row['user_id'].'/'.$ex_images[5];
							$linkImgMini = '/users/'.$row['user_id'].'/'.$ex_images[5];
						}
						echo '<a href="'.$linkImgMini.'" data-thumb="'.$linkImgMid.'" data-full="'.$linkImgMini.'">
							<img src="'.$linkImgMini.'" />
						</a>';
					}
					else {
						$linkImgMid = '/admin_2/uploads/'.$ex_images[5];
						$linkImgMini = '/admin_2/uploads/'.$ex_images[5];
						if($row['user_id']!=0){
							$linkImgMid = '/users/'.$row['user_id'].'/'.$ex_images[5];
							$linkImgMini = '/users/'.$row['user_id'].'/'.$ex_images[5];
						}
						echo '<a href="'.$linkImgMini.'" data-thumb="'.$linkImgMid.'" data-full="'.$linkImgMini.'">
							<img src="'.$linkImgMini.'" />
						</a>';
					}
				}
				echo '</div>';
				$linkThisPage = urlencode('https://'.$_SERVER['HTTP_HOST'].'/'.$_SERVER['REQUEST_URI']);
				echo '<div class="share_block">
					<ul style="text-align:left">
						<li>Поделиться:</li>
						<li><a onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;" href="http://vk.com/share.php?url='.$linkThisPage.'"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
						<li><a onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;" href="https://www.facebook.com/sharer.php?src=sp&u='.$linkThisPage.'"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;" href="https://plus.google.com/share?url='.$linkThisPage.'"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
						<li><a onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;" href="https://twitter.com/intent/tweet?url='.$linkThisPage.'"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;" href="https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl='.$linkThisPage.'"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a></li>
					</ul>
				</div>';
				echo '</div>';
				echo '<script type="text/javascript" src="/libs/fotorama/js/fotorama.js"></script>';
				echo '<script type="text/javascript" src="/libs/fotorama/js/_fotorama_2.js"></script>';
			}
			else {
				echo '<div class="fotogallery">';
				echo '<div class="fotorama fotorama-skin-rsp fotorama-skin-rsp_hidden"
					 data-nav="thumbs"
					 data-height="420"
					 data-keyboard="true"
					 data-navposition="bottom"
					 data-loop="true"
					 data-allowfullscreen="native"
					 data-fit="scaledown">
						<img src="/images/no_photo_650x420.png"/></div>';
				echo '</div>';
				echo '<script type="text/javascript" src="/libs/fotorama/js/fotorama.js"></script>';
				echo '<script type="text/javascript" src="/libs/fotorama/js/_fotorama_2.js"></script>';
			}
			
			// echo '<pre>';
			// print_r($PageInfo);
			// echo '</pre>';
			
			/*
			*  Количество квартир
			*/
			$roomsCount = '';
			if(!empty($PageInfo['roomsCount'])){
				$roomsCount = '<div class="row">
					<div class="cell label">Кол-во квартир:</div>
					<div class="cell">'.$PageInfo['roomsCount'].'</div>
				</div>';
			}
			
			/*
			*  Класс жилья
			*/
			$class_object = '';
			if(!empty($PageInfo['class_object'])){
				$class_object = '<div class="row">
					<div class="cell label">Класс жилья:</div>
					<div class="cell"><a href="/search?type=sell&estate=1&priceAll=all&city='.$PageInfo['location'].'&class='.$PageInfo['class_object'].'">'.$class_objects_new[$PageInfo['class_object']].'</a></div>
				</div>';
			}

			$dogovor = '';
			$devs = mysql_query("
				SELECT *
				FROM ".$template."_dogovor
				WHERE activation='1' && id=".$PageInfo['dogovor']."
				ORDER BY num
			");
			if(mysql_num_rows($devs)>0){
				$dev = mysql_fetch_assoc($devs);
				$dogovor = '<div class="row">
						<div class="cell label">Договор:</div>
						<div class="cell">'.$dev['name'].'</div>
					</div>';
			}
			/*
			*  Тип дома
			*/
			$type_house = '';
			if(!empty($PageInfo['type_house'])){
				$r = mysql_query("
					SELECT name
					FROM ".$template."_type_house
					WHERE id='".$PageInfo['type_house']."'
				");
				if(mysql_num_rows($r)>0){
					$rw = mysql_fetch_assoc($r);
					$type_house = '<div class="row">
						<div class="cell label">Тип дома:</div>
						<div class="cell">'.$rw['name'].'</div>
					</div>';
				}
			}
			
			/*
			*  Застройщик
			*/
			$name_developer = '';
			if(!empty($PageInfo['name_developer'])){
				$name_developer = '<div class="row">
					<div class="cell label">Застройщик:</div>
					<div class="cell">'.$PageInfo['name_developer'].'</div>
				</div>';
			}
			
			/*
			*  Кол-во этажей
			*/
			$floors = '';
			$arrayFloors = array();
			if(!empty($PageInfo['floor'])){
				array_push($arrayFloors,$PageInfo['floor']);
			}
			if(!empty($PageInfo['floors_flat'])){
				array_push($arrayFloors,$PageInfo['floors_flat']);
			}
			else {
				if(!empty($PageInfo['floors'])){
					array_push($arrayFloors,$PageInfo['floors']);
				}
			}
			if(count($arrayFloors)>0){
				if(count($arrayFloors)==1){
					$floors = '<div class="row">
						<div class="cell label">Кол-во этажей:</div>
						<div class="cell">'.$arrayFloors[0].'</div>
					</div>';
				}
				else {
					$floors = '<div class="row">
						<div class="cell label">Кол-во этажей:</div>
						<div class="cell">'.$arrayFloors[0].' (из '.$arrayFloors[1].')</div>
					</div>';
				}
			}
			
			/*
			*  Высота потолков
			*/
			$ceiling_height = '';
			if(!empty($PageInfo['ceiling_height'])){
				$ceiling_height = '<div class="row">
					<div class="cell label">Высота потолков:</div>
					<div class="cell">'.str_replace(".", ",", $PageInfo['ceiling_height']).' м</div>
				</div>';
			}
			
			/*
			*  Отделка
			*/
			$finishing = '';
			if(isset($PageInfo['finishing_value'])){
				$finishing = '<div class="row">
					<div class="cell label">Отделка:</div>
					<div class="cell places">'.$_TYPE_FINISHING_CAB[$PageInfo['finishing_value']].'</div>
				</div>';
			} 
			
			$idUser = 0;
			$textFavorite = 'В избранное';
			$classFavorite = '';
			if($_SESSION['idAuto']){
				$idUser = $_SESSION['idAuto'];
				$res = mysql_query("
					SELECT COUNT(*) AS count
					FROM ".$template."_favorites
					WHERE estate=11 && user_id=".$idUser." && float_id=".$PageInfo['id']." && activation=1
				");
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					if($row['count']){
						$textFavorite = 'В избранном';
						$classFavorite = ' at';
					}
				}
			}
			
			$deadline = '';
			$type_name = 'new_flats';
			$e = mysql_query("
				SELECT commissioning
				FROM ".$template."_queues
				WHERE type_name='".$type_name."' && p_main=".$PageInfo['id']."
			");
			if(mysql_num_rows($e)>0){
				$w = mysql_fetch_assoc($e);
				$deadline = $w['commissioning'];
				$ex_deadline = explode(' ',$deadline);
				$deadline = $ex_deadline[0].' кв. '.$ex_deadline[1];
			}
			
			if(empty($deadline)){
				$t = 'new';
				$deadline = '';
				$queues = mysql_query("
					SELECT commissioning,queue,building
					FROM ".$template."_queues
					WHERE p_main='".$PageInfo['p_main']."' && type_name='".$t."'
					ORDER BY queue, commissioning
					LIMIT 1
				");
				if(mysql_num_rows($queues)>0){
					$deadline .= '<span>';
					$deadlineArray = array();
					$name_dead = '';
					while($queue = mysql_fetch_assoc($queues)){
						$name_dead = $queue['commissioning'];
						$ex_dead = explode(' ',$name_dead);
						if(count($ex_dead)==2){
							$name_dead = $ex_dead[0].' кв. '.$ex_dead[1];
						}
						if(!in_array($name_dead,$deadlineArray)){
							array_push($deadlineArray,$name_dead);
						}
					}
					$deadline .= implode(', ',$deadlineArray);
					$deadline .= '</span>';
				}
			}
			
			if($PageInfo['ready']==5){
				$deadline = 'Дом сдан';
			}
			
			$deadline_row = '';
			if(!empty($deadline)){
				$deadline_row = '<div class="row">
					<div class="cell label">Сдача ГК:</div>
					<div class="cell">'.$deadline.'</div>
				</div>';
			}
		?>

		<div class="card_info">
			<div class="info_housing">
				<div class="row first">
					<div class="cell label">ID: <?=$PageInfo['id']?></div>
					<div class="cell"><a class="add_favorite<?=$classFavorite?>" onclick="return addFavorite(this)" href="javascript:void(0)"><span><?=$textFavorite?></span></a></div>
				</div>
				<div class="row">
					<div class="cell label">Жилая недвижимость:</div>
					<div class="cell">Новостройка</div>
				</div>
				<?=$class_object?>
				<?=$dogovor?>
				<?=$name_developer?>
				<?=$deadline_row?>
				<?=$type_house?>
				<?=$floors?>
				<?=$roomsCount?>
				<?=$ceiling_height?>
				<?=$finishing?>
			</div>
			<?
				/*
				*  Общая площадь
				*/
				$full_square = '';
				if(isset($PageInfo['full_square']) && !empty($PageInfo['full_square'])){
					$full_square = '<div class="row">
						<div class="cell label">Общая площадь:</div>
						<div class="cell">'.$PageInfo['full_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Жилая площадь
				*/
				$live_square = '';
				if(isset($PageInfo['live_square']) && !empty($PageInfo['live_square'])){
					$live_square = '<div class="row">
						<div class="cell label">Жилая площадь:</div>
						<div class="cell">'.$PageInfo['live_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Площадь комнат
				*/
				$rooms_square = '';
				if(isset($PageInfo['rooms_square']) && !empty($PageInfo['rooms_square'])){
					$rooms_square = '<div class="row">
						<div class="cell label">Площадь комнат:</div>
						<div class="cell">'.$PageInfo['rooms_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Площадь кухни
				*/
				$kitchen_square = '';
				if(isset($PageInfo['kitchen_square']) && !empty($PageInfo['kitchen_square'])){
					$kitchen_square = '<div class="row">
						<div class="cell label">Площадь кухни:</div>
						<div class="cell">'.$PageInfo['kitchen_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Санузел
				*/
				$wc = '';
				if(isset($PageInfo['wc']) && !empty($PageInfo['wc'])){
					$wc = '<div class="row">
						<div class="cell label">Санузел:</div>
						<div class="cell">'.$_TYPE_WC_CAB[$PageInfo['wc']].'</div>
					</div>';
				}
				
				/*
				*  Балкон
				*/
				$balcony = '';
				if(isset($PageInfo['balcony']) && !empty($PageInfo['balcony'])){
					$balcony = '<div class="row">
						<div class="cell label">Балкон:</div>
						<div class="cell">'.$_TYPE_BALCONY_ADMIN[$PageInfo['balcony']].'</div>
					</div>';
				}
				if(isset($PageInfo['balcony_flat']) && !empty($PageInfo['balcony_flat'])){
					$value_balcony = $_TYPE_BALCONY_CAB[$PageInfo['balcony_flat']];
					if($PageInfo['balcony_flat']==1){
						$value_balcony = 'Есть';
					}
					$balcony = '<div class="row">
						<div class="cell label">Балкон:</div>
						<div class="cell">'.$value_balcony.'</div>
					</div>';
				}
				
				/*
				*  Лифт
				*/
				$liftArrays = array();
				$lifts = array();
				$liftServices = array();
				$lift_value = '<span class="no">нет</span>';
				$lift = '<div class="row">
					<div class="cell label">Лифт:</div>
					<div class="cell">'.$lift_value.'</div>
				</div>';
				$lift = '';
				$lift_service = '';
				/*
				*  Грузовой лифт
				*/
				if(isset($PageInfo['lift_service']) && !empty($PageInfo['lift_service'])){
					array_push($liftArrays,$_TYPE_LIFT[$PageInfo['lift_service']].' груз.');
				}
				/*
				*  Пассажирский лифт
				*/
				if(isset($PageInfo['lift']) && !empty($PageInfo['lift'])){
					array_push($liftArrays,$_TYPE_LIFT[$PageInfo['lift']].' пасс.');
				}
				
				
				if(count($liftArrays)>0){
					$lift = '<div class="row">
						<div class="cell label">Лифт:</div>
						<div class="cell"><span class="ok">'.implode(' + ',$liftArrays).'</span></div>
					</div>';
				}
				
				/*
				*  Паркинг
				*/
				$parking = '';
				$parking_value = '';
				if(isset($PageInfo['parking']) && !empty($PageInfo['parking'])){
					$parking_value = '<span class="ok">Есть</span>';
					$parking = '<div class="row">
						<div class="cell label">Паркинг:</div>
						<div class="cell">'.$parking_value.'</div>
					</div>';
				}
				
				/*
				*  Мусоропровод
				*/
				$rubbish = '';
				// $name_value = '<span class="no">нет</span>';
				$name_value = '';
				if(isset($PageInfo['rubbish']) && !empty($PageInfo['rubbish'])){
					$name_value = '<span class="ok">Есть</span>';
					$rubbish = '<div class="row">
						<div class="cell label">Мусоропровод:</div>
						<div class="cell">'.$name_value.'</div>
					</div>';
				}
				
				/*
				*  Охраняемая территория
				*/
				$protected_area = '';
				$name_value = '';
				if(isset($PageInfo['protected_area']) && !empty($PageInfo['protected_area'])){
					$name_value = '<span class="ok">Есть</span>';
					$protected_area = '<div class="row">
						<div class="cell label">Охраняемая территория:</div>
						<div class="cell">'.$name_value.'</div>
					</div>';
				}
				
				/*
				*  Детская площадка
				*/
				$playground = '';
				$name_value = '';
				if(isset($PageInfo['playground']) && !empty($PageInfo['playground'])){
					$name_value = '<span class="ok">Есть</span>';
					$playground = '<div class="row">
						<div class="cell label">Детская площадка:</div>
						<div class="cell">'.$name_value.'</div>
					</div>';
				}
			?>
			<div class="info_housing">
				<?=$full_square?>
				<?=$live_square?>
				<?=$rooms_square?>
				<?=$kitchen_square?>
				<?=$wc?>
				<?=$balcony?>
				<?=$lift?>
				<?=$lift_service?>
				<?=$parking?>
				<?=$rubbish?>
				<?=$protected_area?>
				<?=$playground?>
				<?
				if(!empty($PageInfo['short_text'])){
					echo '<div class="row add">
						<div class="cell label">Дополнительно:</div>
						<div class="cell">'.$PageInfo['short_text'].'</div>
					</div>';
				}
				// else {
					// if(!empty($PageInfo['text'])){
						// echo '<div class="row add">
							// <div class="cell label">Дополнительно:</div>
							// <div class="cell">'.strip_tags(htmlspecialchars_decode($PageInfo['text'])).'</div>
						// </div>';
					// }
				// }
				?>
			</div>
			<?
			$arrayBuy = array();
			if(isset($PageInfo['mortgages']) && !empty($PageInfo['mortgages'])){
				array_push($arrayBuy,'<li>Ипотека</li>');
			}
			if(isset($PageInfo['subsidies']) && !empty($PageInfo['subsidies'])){
				array_push($arrayBuy,'<li>Субсидии</li>');
			}
			if(isset($PageInfo['installment']) && !empty($PageInfo['installment'])){
				array_push($arrayBuy,'<li>Рассрочка</li>');
			}
			if(count($arrayBuy)>0){
				echo '<div class="ways_buy">
					<ul>
						'.implode('',$arrayBuy).'
					</ul>
				</div>';
				
			}

			?>			
		</div>
		<div class="clear"></div>
		<div class="text_info">
			<h2><?=$PageInfo['h1']?></h2>
			<?
			if(!empty($PageInfo['text'])){
				echo '<div class="text"><p>'.strip_tags(nl2br(htmlspecialchars_decode($PageInfo['text']))).'</p></div>';
				echo '<div class="more_read"><a href="javascript:void(0)">Читать всё</a></div>';
			}
			?>
		</div>
		<?
		$res = mysql_query("
			SELECT SQL_CALC_FOUND_ROWS f.*,e.address,e.short_address,e.dist_value AS dValue,e.station AS stationBuild,e.area AS areaId,e.location AS cityId,e.name AS nameBuild,e.text AS textBuild,e.class_object,e.type_house AS typeBuild,e.coords,
			(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=f.id && estate='new_flats' LIMIT 1) AS images,
			(SELECT title FROM ".$template."_stations WHERE activation='1' && id=f.station) AS station_name,
			(SELECT title FROM ".$template."_stations WHERE activation='1' && id=e.station) AS station_name_build,
			(SELECT name FROM ".$template."_developers WHERE activation='1' && id=e.developer) AS name_developer,
			(SELECT COUNT(*) FROM ".$template."_new_flats WHERE activation='1' && id=e.developer) AS max_rows,
			(SELECT name FROM ".$template."_location WHERE activation='1' && id=e.location) AS location_name,
			(SELECT title FROM ".$template."_areas WHERE activation='1' && id=e.area) AS area_name,
			(SELECT coords FROM ".$template."_areas WHERE activation='1' && id=e.area) AS coords_area,
			(SELECT coords FROM ".$template."_stations WHERE activation='1' && id=e.station) AS coords_station
			FROM ".$template."_m_catalogue_left AS e
			LEFT JOIN ".$template."_new_flats AS f
			ON f.p_main=e.id
			LEFT JOIN ".$template."_user_ads AS ua
			ON ua.float_id=f.id && ua.estate='1' && ua.type='sell'
			WHERE e.id=".$PageInfo['id']."
			GROUP BY f.id
			ORDER BY f.rooms
		");
		
		$requestSearch = '';
		$requestSearchList = '';
		$coordsArray = array();
		$coordsArray2 = array();
		$estateArray = array();
		$arrayBuilds = array();
		if(mysql_num_rows($res)>0){
			$res2 = mysql_query("SELECT FOUND_ROWS()", $db);
			$max_rows = mysql_result($res2, 0);
			$s = 1;
			$n = 1;
			$roomsCounts = 1;
			$typeRoomsArray = array();
			$countRoomsArray = array();
			while($row = mysql_fetch_assoc($res)){
				array_push($arrayBuilds,$row['id']);			
				$link = '/newbuilding/'.$row['p_main'].'/flat/'.$row['id'];
				$link_developer = '/search?type=sell&estate=1&priceAll=all&developer='.$row['developer'];
				$exploitation = explode('_',$row['exploitation']);
				$photosList = '';
				$photos = mysql_query("
					SELECT *
					FROM ".$template."_photo_catalogue
					WHERE estate='new_flats' && p_main='".$row['id']."' || estate='jk' && p_main='".$row['p_main']."'
					ORDER BY cover DESC, num
				");
				if(mysql_num_rows($photos)>0){
					while($ph = mysql_fetch_assoc($photos)){
						$ex_images = explode(',',$ph['images']);
						if($ph['user_id']!=0){
							$photosList .= '<span class="img" style="background-image:url(/users/'.$ph['user_id'].'/'.$ex_images[3].')"></span>';
						}
						else {
							$photosList .= '<span class="img" style="background-image:url(/admin_2/uploads/'.$ex_images[3].')"></span>';
						}
					}
				}
				
				if(!in_array($row['rooms'],$typeRoomsArray)){
					array_push($typeRoomsArray,$row['rooms']);
					$roomsFlat = 'квартир студий';
					if(!empty($row['rooms'])){
						$roomsFlat = $row['rooms'].'-комнатных квартир';
					}
					$requestSearch .= '<div class="titlePart"><h2><a href="/newbuilding?housing='.$row['queue'].'&rooms='.$row['rooms'].'&estate=1&parent='.$PageInfo['id'].'">Предложение '.$roomsFlat.' в '.$row['nameBuild'].'</a></h2><div class="viewer"><ul><li class="current list"><span onclick="return chooseShow2(this)"><i class="fa fa-list" aria-hidden="true"></i></span></li><li class="table"><span onclick="return chooseShow2(this)"><i class="fa fa-table" aria-hidden="true"></i></span></li></ul></div></div>';
					
					if(count($typeRoomsArray)>1){
						$requestSearchList .= '</table><table class="result_search">
						<colgroup>
							<col style="width:55px">
							<col style="width:190px">
							<col style="width:110px">
							<col style="width:125px">
							<col style="width:110px">
							<col style="width:180px">
							<col style="width:350px">
						</colgroup>';
					}
					$requestSearchList .= '<tr><td colspan="7"><div class="titlePart"><h2><a href="/newbuilding?housing='.$row['queue'].'&rooms='.$row['rooms'].'&estate=1&parent='.$PageInfo['id'].'">Предложение '.$roomsFlat.' в '.$row['nameBuild'].'</a></h2><div class="viewer"><ul><li class="list"><span onclick="return chooseShow2(this)"><i class="fa fa-list" aria-hidden="true"></i></span></li><li class="table current"><span onclick="return chooseShow2(this)"><i class="fa fa-table" aria-hidden="true"></i></span></li></ul></div></div></td></tr>';
					$countRoomsArray = array();
				}
				
				array_push($countRoomsArray,$row['id']);				
				if(count($countRoomsArray)<=2){
					$image = '<span><img src="/images/no_photo_230x210.png"></span>';
					$metro = '';
					$metroTable = '';
					if(!empty($row['station'])){
						$metro = '<div class="metro">&laquo;'.$row['station_name'].'&raquo; / <strong>'.$row['distance'].'</strong></div>';
						$metroTable = '<div class="metro">&laquo;'.$row['station_name'].'&raquo; <strong>/ '.$row['distance'].'</strong></div>';
						
						$dist = floor($row['dist_value']).' м';
						if($dist/1000>=1){
							$dist = price_cell($dist/1000,2).' км';
						}
						$metro = '<div class="metro">&laquo;'.$row['station_name'].'&raquo; / <strong>'.$dist.'</strong></div>';
						$metroTable = '<div class="metro">&laquo;'.$row['station_name'].'&raquo; <strong>/ '.$dist.'</strong></div>';
					}
					$name_title = $row['rooms'].'-комн. квартира';
					$name_title2 = $row['rooms'].'-комн. кв.';
					if($row['rooms']==0){
						$name_title = 'Квартира студия';
						$name_title2 = 'Студия';
					}
					$name_title_map = $row['rooms'].'-комн. кв.';
					if($row['rooms']==0){
						$name_title_map = 'Студия';
					}
					
					if($row['full_square']==0){
						$row['full_square'] = 1;
					}
					$square_meter = ceil($row['price']/$row['full_square']);
					$settings = '';
					$settings2 = '';
					$lift = '';
					$wc = '';
					$balcony = '';
					$phone = '';
					$www = '';
					$ceiling_height = '';
					if(!empty($row['ceiling_height'])){
						$ceiling_height = '<span><ins>Потолки:</ins><i>'.str_replace(".", ",", $row['ceiling_height']).' м</i></span>';
					}
					if(!empty($row['lift'])){
						$lift = '<span><ins>Лифт:</ins><i><i class="ok">да</i></i></span>';
					}
					if(!empty($row['wc'])){
						$wc = '<span><ins>Санузел:</ins><i><i class="ok">'.$_TYPE_WC[$row['wc']].'</i></i></span>';
					}
					if(!empty($row['balcony'])){
						$balcony = '<span><ins>Балкон:</ins><i><i class="ok">да</i></i></span>';
					}
					if(!empty($row['phone'])){
						$phone = '<span><ins>Телефон:</ins><i><i class="ok">да</i></i></span>';
					}
					if(!empty($row['internet'])){
						$www = '<span><ins>Интернет:</ins><i><i class="ok">да</i></i>';
					}
					$floors_type = '&nbsp;'; //этаж 4/б
					$floors_type_table = '';
					if(!empty($row['floor'])){
						$floors_type = 'этаж '.$row['floor'];
						$floors_type_table = ''.$row['floor'].'';
					}
					if(!empty($row['floors'])){
						$floors_type .= ' (из '.$row['floors'].')';
						$floors_type_table .= ' (из '.$row['floors'].')';
					}
					$settings = $ceiling_height.$lift.$wc.$balcony;
					$settings2 = $phone.$www;
					
						
					$fullSquare = '';
					if(!empty($row['full_square'])){
						$fullSquare = '<div class="full">Общая '.str_replace(".", ",", price_cell($row['full_square'],1)).' м<sup>2</sup></div>';
					}
					
					$fullKitchen = '';
					if(!empty($row['kitchen_square'])){
						$fullKitchen = '<div class="kitchen">Кухня '.$row['kitchen_square'].' м<sup>2</sup></div>';
					}
					
					$liveSquare = '';
					if(!empty($row['live_square'])){
						$liveSquare = '<div class="area">Жилая '.str_replace(".", ",", price_cell($row['live_square'],1)).' м<sup>2</sup></div>';
					}
					
					$rooms_square = '';
					if(!empty($row['rooms_square'])){
						$rooms_square = '<div class="rooms">Комнаты '.$row['rooms_square'].' м<sup>2</sup></div>';
					}
						
					$floors_type = '&nbsp;'; //этаж 4/б
					$floors_type_table = '';
					if(!empty($row['floor'])){
						$floors_type = '<div class="block">
							<div class="label">Этаж:</div>
							<div class="value">'.$row['floor'].'</div>
						</div>';
						$floors_type_table = '<div class="floors"><span>Этаж:</span>'.$row['floor'].'</div>';
					}
					if(!empty($row['floors'])){
						$floors_type = '<div class="block">
							<div class="label">Этаж:</div>
							<div class="value">'.$row['floor'].' ('.$row['floors'].')</div>
						</div>';
						$floors_type_table = '<div class="floors"><span>Этаж:</span>'.$row['floor'].' ('.$row['floors'].')</div>';
					}
								
					$fullKitchen = '';
					if(!empty($row['kitchen_square'])){
						$fullKitchen = '<div class="block">
							<div class="label">Кухня:</div>
							<div class="value">'.str_replace(".", ",", price_cell($row['kitchen_square'],1)).' м²</div>
						</div>';
					}
					
					$liveSquare = '';
					if(!empty($row['live_square'])){
						$liveSquare = '<div class="block">
							<div class="label">Жилая:</div>
							<div class="value">'.str_replace(".", ",", price_cell($row['live_square'],1)).' м²</div>
						</div>';
					}
					
					$rooms_square = '';
					if(!empty($row['rooms_square'])){
						$rooms_square = '<div class="rooms">Комнаты '.$row['rooms_square'].' м<sup>2</sup></div>';
					}
					// echo '<pre>';
					// print_r($row);
					// echo '</pre>';
					
					$v = $row['dist_value'];
					$v = $row['dValue'];
					if(!empty($row['rid'])){
						if($v/1000>=1){
							$v = price_cell($v/1000,2).' км';
						}
						else {
							$v = price_cell($v,0).' м';
						}
					}
					else {
						if($v/1000>=1){
							$v = price_cell($v/1000,2).' км';
						}
						else {
							$v = price_cell($v,0).' м';
						}
					}
					
					
					$distance = '<div class="distance"><strong>'.$v.'</strong> | <a class="map" data-link="#map" href="'.$link.'#map">Показать на карте</a></div>';
					if(empty($v)){
						$distance = '<div class="distance"><a class="map" data-link="#map" href="'.$link.'#map">Показать на карте</a></div>';
					}
					
					$coordsWay = $row['coords_area'];
					if(!empty($row['coords_station'])){
						$coordsWay = $row['coords_station'];
					}
					
					if(!empty($row['coords']) && !empty($coordsWay)){
						/*
						*  Расстояние по прямой
						*/
						$dist2 = streightDistance($coordsWay,$row['coords'],'new');
						if(!empty($dist2)){
							$dist = $dist2;
						}
						
						$placeCoords = '';
						if(!empty($row['typeCoords'])){
							$placeCoords = '&laquo;'.$row['typeCoords'].'&raquo; / ';
						}
						$distance = '<div class="distance"><strong>'.$dist.'</strong> | <a class="map" data-link="#map" href="'.$link.'#map">Показать на карте</a></div>';
					}

					
					$type_house = '';
					if(!empty($row['type_house'])){
						// $type_house = '<div class="type_house">'.$type_houseArray[$row['type_house']].'</div>';
						
						/*
						*  Тип дома
						*/
						$devs = mysql_query("
							SELECT *
							FROM ".$template."_type_house
							WHERE activation='1' && id=".$row['type_house']."
							ORDER BY num
						");
						if(mysql_num_rows($devs)>0){
							$dev = mysql_fetch_assoc($devs);
							$type_house = '<div class="type_house">'.$dev['name'].'</div>';
						}
					}
					
					$stationName = '<div class="metro"><a href="/search?type=sell&estate=1&priceAll=all&station='.$row['station'].'">'.$row['station_name'].'</a></div>';
					if(!empty($row['station_name_build'])){
						$stationName = '<div class="metro"><a href="/search?type=sell&estate=1&priceAll=all&station='.$row['stationBuild'].'">'.$row['station_name_build'].'</a></div>';
					}
					if(empty($row['station_name_build']) && empty($row['station_name'])){
						$stationName = '';
					}
					
					$fullText = '';
					if(!empty($row['build_text']) || !empty($row['text'])){
						if(!empty($row['text'])){
							$fullText = '<div class="row">
								<div class="cell full">
									'.htmlspecialchars_decode($row['text']).'
								</div>
							</div>';
						}
						else {
							$fullText = '<div class="row">
								<div class="cell full">
									'.htmlspecialchars_decode($row['build_text']).'
								</div>
							</div>';
						}
					}
					
					$fullText = '';
					$txt = strip_tags(htmlspecialchars_decode($row['text']));
					$txt2 = strip_tags(htmlspecialchars_decode($row['textBuild']));
					if(!empty($txt)){
						$fullText = '<div class="row">
							<div class="cell full">
								'.cutString(strip_tags(htmlspecialchars_decode($row['text'])),600).'
							</div>
						</div>';
					}
					else {
						if(!empty($txt2)){
							$fullText = '<div class="row">
								<div class="cell full">
									'.cutString(strip_tags(htmlspecialchars_decode($row['textBuild'])),600).'
								</div>
							</div>';
						}
					}
					
					$reg_phone = '';
					if(!empty($row['reg_phone'])){
						// $reg_phone = '<div class="phone">+7<strong>812 336-00-</strong></div>';
						$users = mysql_query("
							SELECT *
							FROM ".$template."_phone_users
							WHERE user_id=".$row['user_id']." && activation=1 && type_phone=0
							LIMIT 1
						");
						if(mysql_num_rows($users)>0){
							$user = mysql_fetch_assoc($users);
							$reg_phone = '<div class="phone"><strong>+7 '.$user['code_phone'].' '.$user['number_phone'].'</strong></div>';
						}
					}
					if(!empty($row['num_phone'])){
						$reg_phone = '<div class="phone"><strong>'.$row['num_phone'].'</strong></div>';
					}
					$ex_phones = takePhone($row['id']);
					
					if(empty($reg_phone)){
						$reg_phone = '<div class="phone"><strong>'.$ex_phones[0].'</strong></div>';
					}
					
					$btns = '';
					if(!empty($photosList)){
						$image = '';
						$btns = '<div class="btns_arrow"><a class="left arr" href="javascript:void(0)"></a><a class="right arr" href="javascript:void(0)"></a></div>';
					}

			
			$address = $row['address'];
			$addressArray = array();
			if(!empty($row['short_address'])){
				$address = $row['short_address'];
			}
			$ex_address = explode(',',$address);
			$addss = '';
			for($a=0; $a<count($ex_address); $a++){
				$m = 0;
				if(count($ex_address)==2){
					$m = -1;
				}
				if(count($ex_address)==4){
					$m = 1;
				}
				if(count($ex_address)==6){
					$m = 2;
				}
				if($a>$m){
					array_push($addressArray,$ex_address[$a]);
				}
			}
			
			if(count($addressArray)>0){
				$addss = implode(', ',$addressArray);
			}
			
			$location_name = '';
			if(!empty($row['location_name'])){
				$l_name = $row['location_name'];
				$location_name = $l_name;
				if($l_name == 'Санкт-Петербург'){
					$location_name = 'СПб';
				}
				if($l_name == 'Ленинградская область'){
					$location_name = 'ЛО';
				}
				if($l_name == 'Московская область'){
					$location_name = 'МО';
				}
				if($l_name == 'Москва'){
					$location_name = 'МСК';
				}
			}

			$area_name = '';
			$pos = strripos($row['area_name'], 'р-н');
			if($pos===true){
				$area_name = $row['area_name'].' р-н';
			}
			else {
				$pos = strripos($row['area_name'], 'г.');
				$area_name = $row['area_name'];
			}
			
			$address2 = '<div class="address">'.$location_name.', <a href="/search?type=sell&estate=2&priceAll=all&city='.$row['cityId'].'&area='.$row['area'].'">'.$area_name.'</a>, '.$addss.'</div>';
					
					
					$deadline = '';
					$deadline2 = '';
					$type_name = 'new_flats';
					$r = mysql_query("
						SELECT commissioning
						FROM ".$template."_queues
						WHERE type_name='".$type_name."' && p_main='".$row['id']."'
					");
					if(mysql_num_rows($r)>0){
						$w = mysql_fetch_assoc($r);
						$deadline = $w['commissioning'];
						$ex_deadline = explode(' ',$deadline);
						$deadline = $ex_deadline[0].' кв. '.$ex_deadline[1];
						$deadline2 = $ex_deadline[0].' кв. '.$ex_deadline[1];
					}
					
					if(empty($deadline)){
						$t = 'new';
						$deadline = '';
						$deadline2 = '';
						$queues = mysql_query("
							SELECT commissioning,queue,building
							FROM ".$template."_queues
							WHERE p_main='".$row['p_main']."' && type_name='".$t."'
							ORDER BY queue, commissioning
							LIMIT 1
						");
						if(mysql_num_rows($queues)>0){
							$deadline .= '<span>';
							$deadlineArray = array();
							$name_dead = '';
							while($queue = mysql_fetch_assoc($queues)){
								$name_dead = $queue['commissioning'];
								$ex_dead = explode(' ',$name_dead);
								if(count($ex_dead)==2){
									$name_dead = $ex_dead[0].' кв. '.$ex_dead[1];
								}
								if(!in_array($name_dead,$deadlineArray)){
									array_push($deadlineArray,$name_dead);
								}
							}
							$deadline .= implode(', ',$deadlineArray);
							$deadline2 .= implode(', ',$deadlineArray);
							$deadline .= '</span>';
						}
					}
					
					if($row['ready']==5){
						$deadline = 'Дом сдан';
					}
					
					$deadlineTable = '';
					if(!empty($deadline)){
						$deadlineTable = '<div class="comm">
							<span class="name">Сдача:</span><span class="stat"><b>'.$deadline2.'</b></span>
						</div>';
						$deadline = '<div class="block">
							<div class="label">Сдача ГК:</div>
							<div class="value">'.$deadline.'</div>
						</div>';
					}
					
					$class_object = '';
					if(!empty($row['class_object'])){
						$class_object = '<div class="class_object">'.$class_objects_new[$row['class_object']].'</div>';
					}
					
					$idUser = 0;
					$textFavorite = 'В избранное';
					$classFavorite = '';
					if($_SESSION['idAuto']){
						$idUser = $_SESSION['idAuto'];
						$rs = mysql_query("
							SELECT COUNT(*) AS count
							FROM ".$template."_favorites
							WHERE estate=1 && user_id=".$idUser." && float_id='".$row['id']."' && activation=1
						");
						if(mysql_num_rows($rs)>0){
							$rw = mysql_fetch_assoc($rs);
							if($rw['count']){
								$textFavorite = 'В избранном';
								$classFavorite = ' at';
							}
						}
					}
					
					$requestSearch .= '<div id="id_'.$row['id'].'" class="item new_view">
						<div class="left_block">
							<div class="image">
								<div class="container_block">
									'.$btns.'
									<a target="_blank" href="'.$link.'"><span class="peppermint peppermint-inactive">'.$image.$photosList.'</span></a>
								</div>
							</div>
						</div>
						<div class="right_block">
							<div class="row">
								<div class="cell one_third">
									'.$stationName.'
									'.$distance.'
									<div style="margin-top:15px;font-size:17px" class="build_name"><a style="color:#3361eb;text-decoration:none;border-bottom:1px solid #99b0f5" href="/newbuilding/'.$row['p_main'].'">'.htmlspecialchars_decode($row['nameBuild']).'</a></div>
									<!--<div class="address">'.$address.'</div>-->
									'.$address2.'
								</div>
								<div class="cell one_third">
									<div class="place"><a href="'.$link.'">'.$name_title.' <strong>'.str_replace(".", ",", price_cell($row['full_square'],1)).'</strong>м²</a></div>
									<div class="params_place">
										'.$liveSquare.'
										'.$fullKitchen.'
										'.$floors_type.'
										'.$deadline.'
									</div>
								</div>
								<div class="cell one_third last_col">
									<div class="prices">
										<div class="full_price"><strong>'.price_cell($row['price'],0).'</strong> ₽</div>
										<div class="meter_price">'.price_cell($square_meter,0).' ₽/м²</div>
										'.$type_house.'
										'.$class_object.'
									</div>
									'.$reg_phone.'
								</div>
							</div>
							'.$fullText.'
							<div class="row">
								<div class="cell one_third">
									<div class="more"><a target="_blank" href="'.$link.'">К странице объявления</a></div>
								</div>
								<div class="cell one_third">
									<div class="id_block">ID: '.$row['id'].'</div>
								</div>
								<div class="cell one_third last_col">
									<a class="add_favorite'.$classFavorite.'" onclick="return addFavorite(this,\'new\','.$row['id'].')" href="javascript:void(0)"><span>'.$textFavorite.'</span></a>
								</div>
							</div>
						</div>
					</div>';
					

					/**********/
					$rooms_square = '';
					if(!empty($row['rooms_square'])){
						$rooms_square = '<p class="comm center"><span>'.$row['rooms_square'].' м<sup>2</sup></span></p>';
					}
					$img_count = '';
					if(substr($row['images_count'],-1,1)==1 && substr($row['images_count'],-2,2)!=11){
						$img_count = $row['images_count'].' фотография';
					}
					else if(substr($row['images_count'],-1,1)==2 && substr($row['images_count'],-2,2)!=12 || substr($row['images_count'],-1,1)==3 && substr($row['images_count'],-2,2)!=13 || substr($row['images_count'],-1,1)==4 && substr($row['images_count'],-2,2)!=14){
						$img_count = $row['images_count'].' фотографии';
					}
					else {
						$img_count = $row['images_count'].' фотографий';
					}
					
					$text = '<div class="text">&nbsp;</div>';
					$txt = strip_tags(htmlspecialchars_decode($row['text']));
					$txt2 = strip_tags(htmlspecialchars_decode($row['textBuild']));
					if(!empty($txt)){
						$text = '<div class="text">
							'.cutString(strip_tags(htmlspecialchars_decode($row['text'])),600).'
						</div>';
					}
					else {
						if(!empty($txt2)){
							$text = '<div class="text">
								'.cutString(strip_tags(htmlspecialchars_decode($row['textBuild'])),600).'
							</div>';
						}
					}
					
					$paramsFloat = '';
					
					/*
					*  Высота потолков
					*/
					$ceiling_height = '';
					if(!empty($row['ceiling_height'])){
						$ceiling_height = '<div class="comm"><span class="name">Потолки: </span><span class="stat">'.str_replace(".", ",", $row['ceiling_height']).' м</span></div>';
					}
					
					/*
					*  Ремонт
					*/
					$repairs = '';
					if(!empty($row['finishing_value'])){
						$repairs = '<div class="comm"><span class="name">Отделка: </span><span class="stat">'.$_TYPE_FINISHING[$row['finishing_value']].'</span></div>';
					}
					
					/*
					*  Лифт
					*/
					$lift_value = '<span class="no stat">нет</span>';
					if($row['lift']==1){
						$lift_value = '<span class="ok stat">да</span>';
					}
					$lift = '<div class="comm"><span class="name">Лифт: </span>'.$lift_value.'</div>';
					
					/*
					*  Санузел
					*/
					$wc = '<span class="stat no">нет</span>';
					if(!empty($row['wc'])){
						$wc = '<span class="stat ok">'.$_TYPE_WC_ADMIN[$row['wc']].'</span>';
					}
					$wc = '<div class="comm"><span class="name">Санузел: </span>'.$wc.'</div>';
					
					/*
					*  Балкон
					*/
					
					$balcony = '';
					// $balcony = '<span class="stat no">нет</span>';
					// if(!empty($row['balcony'])){
						// $balcony = '<span class="stat ok">'.$_TYPE_BALCONY_ADMIN[$row['balcony']].'</span>';
					// }
					// $balcony = '<div class="comm"><span class="name">Балкон: </span>'.$balcony.'</div>';
					
					/*
					*  Телефон
					*/
					$phone_value = '<span class="no stat">нет</span>';
					if($row['phone']==1){
						$phone_value = '<span class="ok stat">да</span>';
					}
					$phone = '<div class="comm"><span class="name">Телефон: </span>'.$phone_value.'</div>';
					
					$paramsFloat = $ceiling_height.$repairs.$lift.$wc.$balcony.$phone;

					$metroTable = '';
					if(isset($row['station_name']) && !empty($row['station_name'])){
						$metroTable = '<div class="metro">&laquo;'.$row['station_name'].'&raquo; <strong>/ '.$row['distance'].'</strong></div>';
						$dist = floor($row['dist_value']).' м';
						if($dist/1000>=1){
							$dist = price_cell($dist/1000,2).' км';
						}
						$metroTable = '<div class="metro">&laquo;'.$row['station_name'].'&raquo; <strong>/ '.$dist.'</strong></div>';
					}
					
					$square_meter = ceil($row['price']/$row['full_square']);
					
					$type_developer = '';
					if(!empty($row['name_developer'])){
						$type_developer = '<div class="developer">Застройщик: <a href="'.$link_developer.'">'.$row['name_developer'].'</a></div>';
					}
					
					$placeName = '';
					$linkBuild = '/newbuilding/'.$row['p_main'];
					if(!empty($row['nameBuild'])){
						$placeName = '<div class="place"><a href="'.$linkBuild.'">'.$row['nameBuild'].'</a></div>';
					}
					
					$full_square = '';
					if(!empty($row['full_square'])){
						$full_square = '<div class="comm">Общая: <strong>'.str_replace(".", ",", price_cell($row['full_square'],1)).' м<sup>2</sup></strong></div>';
					}
					
					$live_square = '';
					if(!empty($row['live_square'])){
						$live_square = '<div class="comm">Жилая: <strong>'.str_replace(".", ",", price_cell($row['live_square'],1)).' м<sup>2</sup></strong></div>';
					}
					
					$kitchen_square = '';
					if(!empty($row['kitchen_square'])){
						$kitchen_square = '<div class="comm">Кухня: <strong>'.$row['kitchen_square'].' м<sup>2</sup></strong></div>';
					}
					
					$phoneTable = '';
					if(!empty($row['num_phone'])){
						$phoneTable = '<div class="phone"><strong>'.$row['num_phone'].'</strong></div>';
					}

					$address = $row['address'];
					if(!empty($row['short_address'])){
						$address = $row['short_address'];
					}
					
					$requestSearchList .= '<tr>
						<td>'.$n.'</td>
						<td>
							<div class="build_name"><a href="/newbuilding/'.$row['p_main'].'">'.htmlspecialchars_decode($row['nameBuild']).'</a></div>
							'.$stationName.'
							'.$distance.'
							<div class="address">'.$address.'</div><!--'.$metroTable.'-->
							<!--<div style="margin-top:10px" class="build"><a href="'.$link.'">'.htmlspecialchars_decode($row['nameBuild']).'</a></div>
							'.$type_developer.'-->
						</td>
						<td>
							<div class="title"><a target="_blank" href="'.$link.'">'.$name_title.'</a><span class="type">Новостройка</span></div>
							<div class="floors">'.$floors_type_table.'</div>
							'.$type_house.'
						</td>
						<td>
							'.$full_square.'
							'.$live_square.'
							'.$rooms_square.'
							'.$kitchen_square.'
						</td>
						<td>
							<center><div class="full_price"><strong>'.price_cell($row['price'],0).'</strong> ₽</div><div class="price_meter">'.price_cell($square_meter,0).' ₽/м<sup>2</sup></div></center></td>
						<td>
							'.$deadlineTable.'
							'.$paramsFloat.'
						</td>
						<td style="vertical-align:top!important">
							<div class="info">
								<div class="id">ID: '.$row['id'].'</div>
								'.$phoneTable.'
							</div>
							'.$text.'
							<div class="link"><a target="_blank" href="'.$link.'">Подробнее</a></div>
						</td>
					</tr>';
					$n++;
					/**********/
				}
			}
		}
	?>
		<div id="result_search">
			<div style="border:none" class="list_items">
				<?=$requestSearch?>
			</div>
			<?if(!empty($requestSearchList)){?>
				<div style="display:none" class="table_items">
					<table class="result_search">
						<colgroup>
							<col style="width:55px">
							<col style="width:190px">
							<col style="width:110px">
							<col style="width:125px">
							<col style="width:110px">
							<col style="width:180px">
							<col style="width:350px">
						</colgroup>
						<?=$requestSearchList?>
					</table>
				</div>
			<?}?>
		</div>
		<!--<div class="more_info">
			<div class="near_objects">
				<div class="infrastructure_block">
					<div class="title">Инфраструктура</div>
					<ul>
						<li>
							<div class="item"><span class="school icon"></span>Школа имени Римского-Корсакого</div>
						</li>
						<li>
							<div class="item"><span class="kindergarten icon"></span>Детский сад №56</div>
						</li>
						<li>
							<div class="item"><span class="clinic icon"></span>Поликлиника им. Св. Марии</div>
						</li>
					</ul>
				</div>
				<div class="safety_block">
					<div class="title">Безопасность</div>
					<ul>
						<li>Огороженный периметр</li>
						<li>Видеонаблюдение</li>
						<li>Пропускная система</li>
						<li>Консьерж</li>
						<li>Охраняемая парковка</li>
						<li>Сигнализация</li>
					</ul>
				</div>
			</div>
			<div class="other_offers_flats">
				<div class="list_flats">
					<div class="row">
						<div class="name">Студия. от <strong>27,5 м<sup>2</sup></strong></div>
						<div class="price">от <strong><a href="#">995 000 руб.</a></strong></div>
					</div>
					<div class="row fone">
						<div class="name">1-комн. от <strong>36,4 м<sup>2</sup></strong></div>
						<div class="price">от <strong><a href="#">1 638 000 руб.</a></strong></div>
					</div>
					<div class="row">
						<div class="name">2-комн. от <strong>44,7 м<sup>2</sup></strong></div>
						<div class="price">от <strong><a href="#">1 922 100 руб.</a></strong></div>
					</div>
				</div>
			</div>
		</div>-->
		<div class="bottom_block">
			<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
			<style>
				#map {
					width: 572px; height: 295px; padding: 0; margin: 0;
				}
			</style>
			<?
			$coordsYandex = '';
			$ex_coords = array('30.315868','59.939095');
			if(empty($PageInfo['coords'])){
				$notUpdate = false;
				$params2 = array(
					'geocode' => 'Санкт-Петербург, '.$PageInfo['address'],
					'format'  => 'json',
					'results' => 1,
					// 'key'     => 'AKDIIFYBAAAAqIYtRgIAZYb8P32hIxnbRRSe9CpggONv4PEAAAAAAAAAAADu6wMZJIEJ0SDd0mRe33ag1JRdNA==',
				);
				$response = json_decode(@file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params2)));
				
				if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0){
					$coordsYandex = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
				}
			}
			else {
				$coordsYandex = $PageInfo['coords'];
				$notUpdate = true;
			}
			if(!empty($coordsYandex)){
				if(!$notUpdate){
					mysql_query("
						UPDATE ".$template."_m_catalogue_left
						SET coords='".$coordsYandex."'
						WHERE id='".$PageInfo['id']."'
					");
				}
			}
			if(!empty($coordsYandex)){
				$ex_coords = explode(' ',$coordsYandex);
			}
			
			$typeInfos = array(1 => "Школы","Детские сады","Поликлиники","Продуктовые магазины","Кафе/рестораны","Банкоматы");
			$t = 1;
			
			$arrayCoords = array();
			$arrayValuesType = array();
			$arrayCoordsFormat = array();
			$arrayCoordsFormat2 = array();
			$schoolsArray = array();
			$kindersArray = array();
			$medicalsArray = array();
			$shopsArray = array();
			$cafeArray = array();
			$bankArray = array();
			$r = mysql_query("
				SELECT *
				FROM infrastructures
				WHERE activation=1
			");
			if(mysql_num_rows($r)>0){
				while($rw = mysql_fetch_assoc($r)){
					$arr = array(
						"type" => $rw['type'],
						"name" => $rw['name'],
						"address" => $rw['address'],
						"coords" => $rw['coords'],
					);
					$arrayCoordsFormat[$rw['id']] = $arr;
				}
			}
			$r = mysql_query("
				SELECT *
				FROM infrastructures
				WHERE estate='build' && flat_id=".$PageInfo['id']." && activation=1
				ORDER BY type
			");
			if(mysql_num_rows($r)>0){
				while($rw = mysql_fetch_assoc($r)){
					$arr = array(
						"type" => $rw['type'],
						"name" => $rw['name'],
						"address" => $rw['address'],
						"coords" => $rw['coords'],
					);
					$arrayCoords[$rw['id']] = $arr;
					$ex_coord = explode(' ',$rw['coords']);
					$arr2 = array(
						"name" => $rw['name'],
						"type" => $rw['type'],
						"center" => $ex_coord[0].','.$ex_coord[1],
					);
					$arrs = array(
						"name" => $rw['name'],
						"center" => array($ex_coord[1],$ex_coord[0]),
					);
					if($rw['type']==1){
						array_push($schoolsArray,$arrs);
					}
					if($rw['type']==2){
						array_push($kindersArray,$arrs);
					}
					if($rw['type']==3){
						array_push($medicalsArray,$arrs);
					}
					if($rw['type']==4){
						array_push($shopsArray,$arrs);
					}
					if($rw['type']==5){
						array_push($cafeArray,$arrs);
					}
					if($rw['type']==6){
						array_push($bankArray,$arrs);
					}
					$arrayCoordsFormat2[$rw['id']] = $arr2;
				}
			}
			else {
				for($t=1; $t<=count($typeInfos); $t++){
					$params2 = array(
						'text' => $typeInfos[$t],
						'll' => $ex_coords[0].','.$ex_coords[1],
						// 'spn' => '3.552069,2.400552',
						'lang'  => 'ru_RU',
						'results'  => 500,
						'apikey' => 'b30f68b0-2dbf-4176-83e3-380ac1dcdf61',
					);
					$response = json_decode(@file_get_contents('https://search-maps.yandex.ru/v1/?' . http_build_query($params2)));
					
					$response->properties->Attribution->Sources;
					$objects = $response->features;
					if(count($objects)>0){
						for($f=0; $f<count($objects); $f++){
							$name = $objects[$f]->properties->CompanyMetaData->name;
							$address = $objects[$f]->properties->CompanyMetaData->address;
							$coordinates = $objects[$f]->geometry->coordinates;
							$x = $coordinates[0];
							$y = $coordinates[1];
							$coordsObject = array($x,$y);
							$coord = "'".$x." ".$y."'";
							$useCoord = true;
							if(count($arrayCoordsFormat)>0){
								for($c=0; $c<count($arrayCoordsFormat); $c++){
									$coord2 = $arrayCoordsFormat[$c]['coords'];
									$n2 = $arrayCoordsFormat[$c]['name'];
									if($coord2==$coord && $n2==$name){
										$useCoord = false;
										break;
									}
								}
							}
							if($useCoord){
								$arr = array(
									"type=".$t,
									"estate='build'",
									"flat_id=".$PageInfo['id'],
									"name='".$name."'",
									"address='".$address."'",
									"coords=".$coord,
									"activation=1"
								);
								array_push($arrayValuesType,$arr);
							}
						}
						if(count($arrayValuesType)>0){
							for($c=0; $c<count($arrayValuesType); $c++){
								mysql_query("
									INSERT INTO infrastructures
									SET ".implode(',',$arrayValuesType[$c])."
								") or die(mysql_error());
							}
						}
					}
				}
			}
			$schoolsArray = json_encode($schoolsArray);
			$kindersArray = json_encode($kindersArray);
			$medicalsArray = json_encode($medicalsArray);
			$shopsArray = json_encode($shopsArray);
			$cafeArray = json_encode($cafeArray);
			$bankArray = json_encode($bankArray);
			$arrayIcons = array(1 => "schools.png","kinder.png","medical.png","shops.png","cafe.png","bank.png");
			?>
			<script>
				


				
				ymaps.ready(init);

				// Группы объектов
				var groups = [
					{
						name: "Школы",
						type: "schools",
						style: [{
							iconLayout: 'default#image',
							iconImageHref: '/images/<?=$arrayIcons[1]?>',
							iconImageSize: [52, 44],
							iconImageOffset: [-26, -22],
						}],
						items: <?=$schoolsArray?>
					},
					{
						name: "Детские сады",
						type: "kindergarten",
						style: [{
							iconLayout: 'default#image',
							iconImageHref: '/images/<?=$arrayIcons[2]?>',
							iconImageSize: [52, 44],
							iconImageOffset: [-26, -22],
						}],
						items: <?=$kindersArray?>
					},
					{
						name: "Поликлиники",
						type: "medical",
						style: [{
							iconLayout: 'default#image',
							iconImageHref: '/images/<?=$arrayIcons[3]?>',
							iconImageSize: [52, 44],
							iconImageOffset: [-26, -22],
						}],
						items: <?=$medicalsArray?>
					},
					{
						name: "Продуктовые магазины",
						type: "shop",
						style: [{
							iconLayout: 'default#image',
							iconImageHref: '/images/<?=$arrayIcons[4]?>',
							iconImageSize: [52, 44],
							iconImageOffset: [-26, -22],
						}],
						items: <?=$shopsArray?>
					},
					{
						name: "Кафе/рестораны",
						type: "cafe",
						style: [{
							iconLayout: 'default#image',
							iconImageHref: '/images/<?=$arrayIcons[5]?>',
							iconImageSize: [52, 44],
							iconImageOffset: [-26, -22],
						}],
						items: <?=$cafeArray?>
					},
					{
						name: "Банкоматы",
						type: "bank",
						style: [{
							iconLayout: 'default#image',
							iconImageHref: '/images/<?=$arrayIcons[6]?>',
							iconImageSize: [52, 44],
							iconImageOffset: [-26, -22],
						}],
						items: <?=$bankArray?>
					}
				];

				function init() {
					// Создание экземпляра карты.
					var myMap = new ymaps.Map('map', {
							center: [<?=$ex_coords[1]?>,<?=$ex_coords[0]?>],
							zoom: 16
						}, {
							searchControlProvider: 'yandex#search'
						}),
						// Контейнер для меню.
						menu = $('<div class="informs"><span class="title"><i>Инфраструктура</i></span><ul class="menu"></ul></div>'),
						myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
							hintContent: '<?=$PageInfo['name']?>',
							balloonContent: 'Санкт-Петербург, <?=$PageInfo['address']?>'
						}, {
							// Опции.
							// Необходимо указать данный тип макета.
							iconLayout: 'default#image',
							// Своё изображение иконки метки.
							iconImageHref: '/images/<?=$_ICONS_MAPS[5]['card']?>',
							// Размеры метки.
							iconImageSize: [52, 44],
							// Смещение левого верхнего угла иконки относительно
							// её "ножки" (точки привязки).
							iconImageOffset: [-3, -42]
						});
						
					for (var i = 0, l = groups.length; i < l; i++) {
						createMenuGroup(groups[i]);
					}

					function createMenuGroup (group) {
						// Пункт меню.
						var menuItem = $('<li class="checked ' + group.type + '"><a href="javascript:void(0)"><span class="check"></span>' + group.name + '</a></li>'),
						// Коллекция для геообъектов группы.
							collection = new ymaps.GeoObjectCollection(null, { preset: group.style }),
						// Контейнер для подменю.
							submenu = $('<ul class="submenu"></ul>');

						// Добавляем коллекцию на карту.
						myMap.geoObjects.add(collection);
						// Добавляем подменю.
						
						menu.find('ul:first').append(menuItem);
						menuItem
							// .append(submenu)
							// Добавляем пункт в меню.
							// По клику удаляем/добавляем коллекцию на карту и скрываем/отображаем подменю.
							.find('a')
							.bind('click', function () {
								if (collection.getParent()) {
									$(this).parent().removeClass('checked');
									myMap.geoObjects.remove(collection);
									submenu.hide();
								} else {
									$(this).parent().addClass('checked');
									myMap.geoObjects.add(collection);
									submenu.show();
								}
							});
						for (var j = 0, m = group.items.length; j < m; j++) {
							createSubMenu(group.items[j], collection, submenu);
						}
					}

					function createSubMenu (item, collection, submenu) {
						// Пункт подменю.
						var submenuItem = $('<li><a href="javascript:void(0)">' + item.name + '</a></li>'),
						// Создаем метку.
							placemark = new ymaps.Placemark(item.center, { balloonContent: item.name });

						// Добавляем метку в коллекцию.
						collection.add(placemark);
						// Добавляем пункт в подменю.
						submenuItem
							.appendTo(submenu)
							// При клике по пункту подменю открываем/закрываем баллун у метки.
							.find('a')
							.bind('click', function () {
								if (!placemark.balloon.isOpen()) {
									placemark.balloon.open();
								} else {
									placemark.balloon.close();
								}
								return false;
							});
					}

					myMap.geoObjects.add(myPlacemark);
/* 					var myRouter = ymaps.route([
					  'Санкт-Петербург, м. <?=$PageInfo['station_name']?>', {
						type: "viaPoint",                   
						point: [<?=$ex_coords[1]?>,<?=$ex_coords[0]?>]
					  },
					], {
					  mapStateAutoApply: true 
					});
					
					myRouter.then(function(route) {
						var points = route.getWayPoints();
						points.options.set('preset', 'islands#blackStretchyIcon');
						points.get(0).properties.set("iconContent", 'Санкт-Петербург, м. <?=$PageInfo['station_name']?>');
						points.get(1).properties.set("iconContent", '<?=htmlspecialchars_decode($PageInfo['name'])?>');
						// Добавление маршрута на карту
						// myMap.geoObjects.add(route);
						var routeLength = route.getHumanLength();
						var routeLength2 = route.getLength();
						$('<div class="full_route">Расстояние от <span>Санкт-Петербург, м. <?=$PageInfo['station_name']?></span> до <span><?=htmlspecialchars_decode($PageInfo['name'])?></span>: <strong>'+routeLength+'</strong></div>').insertAfter('#map');
						var parent = arrLinkPage[1];
						var id = arrLinkPage[2];
						var data = "buildDist="+routeLength2+"&parent="+parent+"&id="+id;
						xhr = $.ajax({
							type: "POST",
							url: '/include/handler.php',
							data: data+"&rnd="+Math.random(),
							dataType: "json",
							success: function (data, textStatus) {
								var json = eval(data);
								if(json.action!='error'){
									// window.location = window.location;
								}
								else {
									alert('Возникла системная ошибка! Данные не сохранены. Попробуйте позднее.');
								}
							}
						});
						// alert(routeLength2);
					  },
					  // Обработка ошибки
					  function (error) {
						alert("Возникла ошибка: " + error.message);
					  }
					)
 */
					// Добавляем меню.
					$('#map').append(menu);
					// Выставляем масштаб карты чтобы были видны все группы.
					// myMap.setBounds(myMap.geoObjects.getBounds());
					$('#map .informs .title').on('click',function(){
						if($(this).parent().is('.hide')){
							$(this).parent().removeClass('hide');
							$(this).parent().find('ul.menu').show();
						}
						else {
							$(this).parent().addClass('hide');		
							$(this).parent().find('ul.menu').hide();
						}
					});
				}
				
				
			</script>
			<div class="map"><div style="width:1120px;height:600px" id="map"></div></div>
		</div>
		
	</div>	
	
	<?
	$res = mysql_query("
		SELECT c.*,
		(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && p_main=c.id && estate='jk' ORDER BY cover DESC, num LIMIT 1) AS images,
		(SELECT MIN(price) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS min_price,
		(SELECT MAX(price) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS max_price,
		(SELECT MIN(full_square) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS min_square,
		(SELECT MAX(full_square) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS max_square,
		(SELECT COUNT(*) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS count_estate,
		(SELECT name FROM ".$template."_developers WHERE activation='1' && id=c.developer) AS name_developer
		FROM ".$template."_m_catalogue_left AS c
		WHERE c.activation='1' && c.id!='".$PageInfo['id']."'
		ORDER BY RAND()
		LIMIT 10
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		echo '<div class="our_offers">';
		echo '<h3>Похожие</h3>';
		echo '<div class="offers_list">';
		$n = 1;
		while($row = mysql_fetch_assoc($res)){
			$ex_image = explode(',',$row['images']);
			$image = '<img src="/admin_2/uploads/'.$ex_image[0].'">';
			$link = '/newbuilding/'.$row['id'];
			$exploitation = explode('_',$row['exploitation']);
			$deadline = '';
			if(empty($row['min_square']) || empty($row['max_square'])){
				$square_meter_min = 0;
				$square_meter_max = 0;
			}
			else {
				$square_meter_min = ceil($row['min_price']/$row['min_square']);
				$square_meter_max = ceil($row['max_price']/$row['max_square']);
			}
			if(!empty($row['exploitation'])){
				if(count($exploitation)>0){
					if(count($exploitation)>1){
						$deadline = '<span>'.$exploitation[1].' кв. '.$exploitation[0].'</span>';
					}
					else {
						$deadline = '<span>'.$exploitation[0].'</span>';
					}
				}
			}
			else {
				$queues = mysql_query("
					SELECT commissioning,queue
					FROM ".$template."_queues
					WHERE p_main='".$row['id']."' && type_name='new'
					GROUP BY queue
				");
				if(mysql_num_rows($queues)>0){
					$deadline .= '<span>';
					$deadlineArray = array();
					
					while($queue = mysql_fetch_assoc($queues)){
						array_push($deadlineArray,$queue['queue'].' очередь - '.$queue['commissioning']);
					}
					// for($q=0; $q<1; $q++){
						$deadline .= $deadlineArray[0];
					// }
					$deadline .= '</span>';
				}
			}
			
			$type_developer = '';
			$type_deadline = '';
			if(!empty($PageInfo['name_developer'])){
				$type_developer = '<div class="type_name">'.$row['name_developer'].'</div>';
			}
			if(!empty($deadline)){
				$type_deadline = '<div class="deadline">Срок сдачи: '.$deadline.'</div>';
			}

			if($row['count_estate']>0){
				echo '<div class="item">
					<div class="image">
						<a href="'.$link.'">'.$image.'</a>
					</div>
					<div class="body">
						<div class="location">
							<div class="title"><a href="'.$link.'">'.$row['name'].'</a></div>
							<div class="street">'.$row['address'].'</div>
							'.$type_developer.'
							'.$type_deadline.'
						</div>
						<div class="cost_block">
							<div class="price">от <strong>'.price_cell($row['min_price'],0).' ₽</strong></div>
							<div class="square">'.price_cell($square_meter_min,0).' - '.price_cell($square_meter_max,0).' ₽/м<sup>2</sup></div>
							<div class="count_offers"><a href="'.$link.'">'.$row['count_estate'].' в продаже</a></div>
						</div>
					</div>
				</div>';
				if($n==2){
					break;
				}
				else {
					$n++;
				}
			}
		}
		echo '</div>';
		echo '</div>';
	}
	?>
</div>	
<?
}
if(count($params)==3){
	if($params[2]=='housing'){

	}
	if($params[2]=='flats'){
		
	}

?>
<?}
if(count($params)==4){
	if($params[2]=='housing' && $housingsPage){
		
	}
	if($params[2]=='flat' && $flatsPage){
		include("include/mods/flat_open_new.php");
	}
}
?>