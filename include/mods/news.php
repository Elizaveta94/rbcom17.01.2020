<div id="center">
<?
	echo '<h1 class="page_list">'.$titlePage.'</h1>';
	//Вывод списка новостей
	if(count($params)==1){
		echo '<div id="news_list">';
		echo '<div class="article_block">';
		echo '<div class="title">Статьи</div>';
		echo '<div class="articles_list">';
		echo '<div class="scrollbar-inner">';
		echo '<ul>';
		$res = mysql_query("
			SELECT * 
			FROM ".$template."_articles_news
			WHERE activation='1' && lang='".$lang."'
			ORDER BY date DESC,id DESC 
		");
		if(mysql_num_rows($res)>0){
			$n = 1;
			while($row = mysql_fetch_assoc($res)){
				$date = '';
				$link = '/news/'.$row['link'];
				$ex_date = explode('-',$row['date']);
				$ex_date = $ex_date[2].' '.$ex_date[1].' '.$ex_date[0];
				$ex_date = explode(' ',$ex_date);
				for($d=0; $d<count($ex_date); $d++){
					$name_date = ' '.$ex_date[$d];
					if($d==1){
						$m = intval($ex_date[$d]);
						$name_date = ' '.$_SHORT_MONTH[$m];
					}
					$date .= $name_date;
				}
				$current = '';
				$hidden_block = '';
				if($n==1){
					$current = ' class="current"';
					$hidden_block = ' style="display:block"';
				}
				echo '<li'.$current.'>
						<div class="name"><a href="javascript:void(0)"><span>'.$row['name'].'</span></a></div>
						<div'.$hidden_block.' class="hidden_block">
							<div class="text_info">
								<div class="h3">'.$row['name'].'</div>
								<div class="text">
									'.$row['short_news'].'
								</div>
								<div class="more_read">
									<a href="/informs/'.$row['link'].'">Читать статью целиком</a>
								</div>
							</div>
						</div>
					</li>';
				$n++;
			}
		}
		else {
			echo '<li><div class="name"><a href="javascript:void(0)">Статей пока нет</a></div></li>';
		}
		echo '</ul>';
		echo '</div>';
		echo '</div>';
		echo '</div>';

		echo '<div class="news_block">';
		echo '<div class="title">Новости</div>';
		echo '<div class="news_list">';
			
		$start = 0;
		$num = 10;
		$page_name = $linkOfPage.'/page/';
		
		// Инициализация pager
		@$pages = (int)$params[count($params)-1];
		$result00 = mysql_query("
			SELECT COUNT(*) 
			FROM ".$template."_articles_news 
			WHERE activation='1' && lang='".$lang."'
		");
		if(mysql_num_rows($result00)>0){
			$temp = mysql_fetch_array($result00);
			$posts = $temp[0];
			if($posts > 0) {
				$total = (($posts - 1) / $num) + 1;
				$total =  intval($total);
				$pages = intval($pages);
				if(empty($pages) or $pages < 0) $pages = 1;
				  if($pages > $total) $pages = $total;
				$start = $pages * $num - $num;
			}
		}
		$res = mysql_query("
			SELECT * 
			FROM ".$template."_articles_news 
			WHERE activation='1' && lang='".$lang."'
			ORDER BY date DESC,id DESC 
			LIMIT ".$start.",".$num."
		");
		if(mysql_num_rows($res)>0){
			$n = 1;
			while($row = mysql_fetch_assoc($res)){
				$date = '';
				$link = '/informs/'.$row['link'];
				$ex_date = explode('-',$row['date']);
				$ex_date = $ex_date[2].' '.$ex_date[1].' '.$ex_date[0];
				$ex_date = explode(' ',$ex_date);
				for($d=0; $d<count($ex_date); $d++){
					$name_date = ' '.$ex_date[$d];
					if($d==1){
						$m = intval($ex_date[$d]);
						$name_date = ' '.$_SHORT_MONTH[$m];
					}
					$date .= $name_date;
				}
				if($n==1){
					$images = '';
					if(!empty($row['images'])){
						$ex_images = explode(',',$row['images']);
						$images = '<div class="image float_left"><img src="/admin_2/uploads/'.$ex_images[2].'"></div>';
					}
					echo '<div class="big_news">
						<div style="float:none" class="date">'.$date.'</div>
						<div class="title">'.$row['name'].'</div>
						<div class="text_info">
							<div class="text">
								'.$images.'
								'.htmlspecialchars_decode($row['text']).'
							</div>
							<div class="more_read"><a href="'.$link.'">Читать новость целиком</a></div>
						</div>
					</div>';
					echo '<div class="other_news">';
					echo '<ul>';
				}
				else {
					$images = '';
					if(!empty($row['images'])){
						$ex_image = explode(',',$row['images']);
						$images = '<div class="image"><a style="background-image:url(/admin_2/uploads/'.$ex_image[0].')" href="'.$link.'"></a></div>';
					}
					echo '<li>
						<div class="date">'.$date.'</div>
						<div class="name"><a href="'.$link.'">'.$row['name'].'</a></div>
						'.$images.'
						<div class="text">
							'.cutString($row['short_news'],200).'
						</div>
						<div class="more_read">
							<a href="'.$link.'">Читать подробно</a>
						</div>
					</li>';
				}
				$n++;
			}
			echo '</ul>';
			echo '</div>';
		}
		else {
			echo '<h3>Новостей пока нет</h3>';
		}
		echo '</div>';
		echo '</div>';
				
		// pager
		$prev = '';
		if(($pages - 1)>-1){
			if(($pages - 1)==1){
				$prev = '<li class="prev"><a title="Предыдущая" href="/'.$page_name.'"><i class="triangle-left"></i></a></li>';
			}
			else {
				$prev = '<li class="prev"><a title="Предыдущая" href="/'.$page_name.''.($pages - 1).'/"><i class="triangle-left"></i></a></li>';
			}
		}
		if ($pages != 1) $pervpage = $prev;
		if ($pages != $total) $nextpage = '<li class=next><a title="Следующая" href="/'.$page_name.''. ($pages + 1) .'"><i class="triangle-right"></i></a></li>';
		if($pages - 2 > 0) {
			if(($pages - 2)==1){
				$pagesleft = '<li><a href="/'.$page_name.'">'. ($pages - 2) .'</a></li>';
			}
			else {
				$pagesleft = '<li><a href="/'.$page_name.''. ($pages - 2) .'">'. ($pages - 2) .'</a></li>';
			}
		}
		if($pages - 1 > 0){
			if(($pages - 1)==1){
				$page1left = '<li><a href="/'.$page_name.'">'. ($pages - 1) .'</a></li>';
			}
			else {
				$page1left = '<li><a href="/'.$page_name.''. ($pages - 1) .'">'. ($pages - 1) .'</a></li>';
			}
		}
		
		if($pages + 2 <= $total) $pagesright = '<li><a href="/'.$page_name.''. ($pages + 2) .'">'. ($pages + 2) .'</a></li>';
		if($pages + 1 <= $total) $page1right = '<li><a href="/'.$page_name.''. ($pages + 1) .'">'. ($pages + 1) .'</a></li>';
		if ($total > 1){
			Error_Reporting(E_ALL & ~E_NOTICE);
			echo "<div id=\"pager\"><ul>";
			echo $pervpage.$pagesleft.$page1left.'<li class=current><span>'.$pages.'</span></li>'.$page1right.$pagesright.$nextpage.'</ul></div>';
		}
		
		$res = mysql_query("
			SELECT c.*,
			(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=c.id && estate='jk') AS images,
			(SELECT MIN(price) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS min_price,
			(SELECT MAX(price) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS max_price,
			(SELECT MIN(full_square) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS min_square,
			(SELECT MAX(full_square) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS max_square,
			(SELECT COUNT(*) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS count_estate,
			(SELECT name FROM ".$template."_developers WHERE activation='1' && id=c.developer) AS name_developer
			FROM ".$template."_m_catalogue_left AS c
			WHERE c.activation='1' && c.location='".$location_id."'
			ORDER BY RAND()
			LIMIT 10
		") or die(mysql_error());
		if(mysql_num_rows($res)>0){
			echo '<div style="clear:both;overflow:hidden;float:left" class="our_offers">';
			echo '<div class="centers_block">';
			echo '<h3>Возможно вам понравится</h3>';
			echo '<div class="offers_list">';
			$n = 1;
			while($row = mysql_fetch_assoc($res)){
				if(!empty($row['max_price'])){
					$ex_image = explode(',',$row['images']);
					$image = '<img src="/images/no_photo_230x210.png"/>';
					$photosList = '';
					$photos = mysql_query("
						SELECT *
						FROM ".$template."_photo_catalogue
						WHERE estate='jk' && p_main='".$row['id']."'
						LIMIT 1
					");
					if(mysql_num_rows($photos)>0){
						$ph = mysql_fetch_assoc($photos);
						$ex_images = explode(',',$ph['images']);
						if($ph['user_id']!=0){
							$photosList = '/users/'.$ph['user_id'].'/'.$ex_images[3];
						}
						else {
							$photosList = '/admin_2/uploads/'.$ex_images[3];
						}
						$image = '<img src="'.$photosList.'">';
					}

					$link = '/newbuilding/'.$row['id'];
					$exploitation = explode('_',$row['exploitation']);
					$deadline = '';
					if(empty($row['min_square']) || empty($row['max_square'])){
						$square_meter_min = 0;
						$square_meter_max = 0;
					}
					else {
						$square_meter_min = ceil($row['min_price']/$row['min_square']);
						$square_meter_max = ceil($row['max_price']/$row['max_square']);
					}
					if(count($exploitation)>0){
						if(count($exploitation)>1){
							$deadline = '<span>'.$exploitation[1].' кв. '.$exploitation[0].'</span>';
						}
						else {
							$deadline = '<span>'.$exploitation[0].'</span>';
						}
					}
					echo '<div class="item">
						<div class="image">
							<a href="'.$link.'">'.$image.'</a>
						</div>
						<div class="body">
							<div class="location">
								<div class="title"><a href="'.$link.'">'.$row['name'].'</a></div>
								<div class="street">'.$row['address'].'</div>
								<div class="type_name">'.$row['name_developer'].'</div>
								<div class="deadline">Срок сдачи: '.$deadline.'</div>
							</div>
							<div class="cost_block">
								<div class="price">от <strong>'.price_cell($row['min_price'],0).' руб.</strong></div>
								<div class="square">'.price_cell($square_meter_min,0).' - '.price_cell($square_meter_max,0).' руб/м<sup>2</sup></div>
								<div class="count_offers"><a href="'.$link.'">'.$row['count_estate'].' в продаже</a></div>
							</div>
						</div>
					</div>';
					if($n!=2){
						$n++;
					}
					else {
						break;
					}
				}
			}
			echo '</div>';
			echo '</div>';
			echo '</div>';
		}
	}
	else {
		if($newsPage){// пагинация
			echo '<div id="sort_block">
				<div class="search_block">
					<form id="search_name">
						<input type="text" placeholder="Поиск по заголовку">
					</form>
				</div>
			</div>';
			echo $textPage;
			$start = 0;
			$num = 10;
			$page_name = $linkOfPage;
			
			// Инициализация pager
			@$pages = (int)$params[count($params)-1];
			$result00 = mysql_query("
				SELECT COUNT(*) 
				FROM ".$template."_m_news_left 
				WHERE activation='1' && lang='".$lang."'
			");
			if(mysql_num_rows($result00)>0){
				$temp = mysql_fetch_array($result00);
				$posts = $temp[0];
				if($posts > 0) {
					$total = (($posts - 1) / $num) + 1;
					$total =  intval($total);
					$pages = intval($pages);
					if(empty($pages) or $pages < 0) $pages = 1;
					  if($pages > $total) $pages = $total;
					$start = $pages * $num - $num;
				}
			}
			
			$res = mysql_query("
				SELECT * 
				FROM ".$template."_m_news_left 
				WHERE activation='1' && lang='".$lang."'
				ORDER BY date DESC,id DESC 
				LIMIT ".$start.",".$num."
			",$db);
			$num_rows = mysql_num_rows($res);
			if ($num_rows > 0) {
				echo '<div class="module news list">';
				while ($row = mysql_fetch_assoc($res)){			
					$img = '';
					if(!empty($row['images'])){
						$images = explode(',',$row['images']);
						$img = '<div class="image"><a href="/news/'.$row["link"].'" class="img"><img data-img-mobile="/admin_2/uploads/'.$images[1].'" src="/admin_2/uploads/'.$images[0].'"></a></div>';
					}
					
					echo '<div class="block">
						'.$img.'
						<div class="description">
							<div class="date">'.date_rus($row['date']).'</div>
							<div class="title"><a href="/news/'.$row["link"].'">'.$row["name"].'</a></div>
							<div class="text">'.cutString($row["short_news"],380).'</div>
						</div>    
					</div>';
				}
				echo '</div>';
			}
			else {
				echo '<p>Новостей нет</p>';
			}
			
			// pager
			$prev = '';
			if(($pages - 1)>-1){
				if(($pages - 1)==1){
					$prev = '<li class="prev"><a title="Предыдущая" href="/'.$page_name.'"><i class="triangle-left"></i></a></li>';
				}
				else {
					$prev = '<li class="prev"><a title="Предыдущая" href="/'.$page_name.'/page/'.($pages - 1).'/"><i class="triangle-left"></i></a></li>';
				}
			}
			if ($pages != 1) $pervpage = $prev;
			if ($pages != $total) $nextpage = '<li class=next><a title="Следующая" href="/'.$page_name.'/page/'. ($pages + 1) .'"><i class="triangle-right"></i></a></li>';
			if($pages - 2 > 0) {
				if(($pages - 2)==1){
					$pagesleft = '<li><a href="/'.$page_name.'">'. ($pages - 2) .'</a></li>';
				}
				else {
					$pagesleft = '<li><a href="/'.$page_name.'/page/'. ($pages - 2) .'">'. ($pages - 2) .'</a></li>';
				}
			}
			if($pages - 1 > 0){
				if(($pages - 1)==1){
					$page1left = '<li><a href="/'.$page_name.'">'. ($pages - 1) .'</a></li>';
				}
				else {
					$page1left = '<li><a href="/'.$page_name.'/page/'. ($pages - 1) .'">'. ($pages - 1) .'</a></li>';
				}
			}
			
			if($pages + 2 <= $total) $pagesright = '<li><a href="/'.$page_name.'/page/'. ($pages + 2) .'">'. ($pages + 2) .'</a></li>';
			if($pages + 1 <= $total) $page1right = '<li><a href="/'.$page_name.'/page/'. ($pages + 1) .'">'. ($pages + 1) .'</a></li>';
			if ($total > 1){
				Error_Reporting(E_ALL & ~E_NOTICE);
				echo "<div id=\"pager\"><ul>";
				echo $pervpage.$pagesleft.$page1left.'<li class=current><span>'.$pages.'</span></li>'.$page1right.$pagesright.$nextpage.'</ul></div>';
			}
		}
		else {// карточка новости
			echo '<div id="news_list">';
			echo '<div class="article_block">';
			echo '<div class="title">Статьи</div>';
			echo '<div class="articles_list">';
			echo '<div class="scrollbar-inner">';
			echo '<ul>';
			$res = mysql_query("
				SELECT * 
				FROM ".$template."_articles_news
				WHERE activation='1' && lang='".$lang."'
				ORDER BY date DESC,id DESC 
			");
			if(mysql_num_rows($res)>0){
				$n = 1;
				while($row = mysql_fetch_assoc($res)){
					$date = '';
					$link = '/news/'.$row['link'];
					$ex_date = explode('-',$row['date']);
					$ex_date = $ex_date[2].' '.$ex_date[1].' '.$ex_date[0];
					$ex_date = explode(' ',$ex_date);
					for($d=0; $d<count($ex_date); $d++){
						$name_date = ' '.$ex_date[$d];
						if($d==1){
							$m = intval($ex_date[$d]);
							$name_date = ' '.$_SHORT_MONTH[$m];
						}
						$date .= $name_date;
					}
					$current = '';
					$hidden_block = '';
					if($n==1){
						$current = ' class="current"';
						$hidden_block = ' style="display:block"';
					}
					echo '<li'.$current.'>
							<div class="name"><a href="javascript:void(0)"><span>'.$row['name'].'</span></a></div>
							<div'.$hidden_block.' class="hidden_block">
								<div class="text_info">
									<div class="h3">'.$row['name'].'</div>
									<div class="text">
										'.$row['short_news'].'
									</div>
									<div class="more_read">
										<a href="/informs/'.$row['link'].'">Читать статью целиком</a>
									</div>
								</div>
							</div>
						</li>';
					$n++;
				}
			}
			else {
				echo '<li><div class="name"><a href="javascript:void(0)">Статей пока нет</a></div></li>';
			}
			echo '</ul>';
			echo '</div>';
			echo '</div>';
			echo '</div>';
			
			echo '<div class="news_block">';
			echo '<div class="title"><a href="/news">Назад к новостям</a></div>';
			echo '<div class="news_list">';
			$date = '';
			$ex_date = explode('-',$PageInfo['date']);
			$ex_date = $ex_date[2].' '.$ex_date[1].' '.$ex_date[0];
			$ex_date = explode(' ',$ex_date);
			for($d=0; $d<count($ex_date); $d++){
				$name_date = ' '.$ex_date[$d];
				if($d==1){
					$m = intval($ex_date[$d]);
					$name_date = ' '.$_SHORT_MONTH[$m];
				}
				$date .= $name_date;
			}
			echo '<div class="big_news card">
				<div class="date">'.$date.'</div>
				<!--<div class="title">'.$PageInfo['name'].'</div>-->
				<div class="text_info" style="max-height:100%">
					<div class="text">
						'.htmlspecialchars_decode($PageInfo['text']).'
					</div>
				</div>
			</div>';
			echo '</div>';
			$res = mysql_query("
				SELECT * 
				FROM ".$template."_m_news_left 
				WHERE activation='1' && lang='".$lang."' && id!='".$PageInfo['id']."'
				ORDER BY date DESC,id DESC 
				LIMIT 2
			");
			if(mysql_num_rows($res)>0){
				echo '<div class="other_blocks">';
				echo '<div class="title">Другие новости</div>';
				echo '<ul>';
				while($row = mysql_fetch_assoc($res)){
					$date = '';
					$link = '/news/'.$row['link'];
					$ex_date = explode('-',$row['date']);
					$ex_date = $ex_date[2].' '.$ex_date[1].' '.$ex_date[0];
					$ex_date = explode(' ',$ex_date);
					for($d=0; $d<count($ex_date); $d++){
						$name_date = ' '.$ex_date[$d];
						if($d==1){
							$m = intval($ex_date[$d]);
							$name_date = ' '.$_SHORT_MONTH[$m];
						}
						$date .= $name_date;
					}

					echo '<li>
						<div class="date">'.$date.'</div>
						<div class="name"><a href="'.$link.'">'.$row['name'].'</a></div>
					</li>';
				}
				echo '</ul>';
				echo '</div>';
			}
			echo '</div>';
			echo '</div>';
		}
	}	
?>
</div>