<?
$add_params = '';
$orderByFlats = 'num';
$LOAD_PAGE = 12;

$coordsArray = array();
$coordsArray2 = array();
if(count($params)==1){
	$res = mysql_query("
		SELECT SQL_CALC_FOUND_ROWS s.*,
		(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=s.id && estate='country') AS images,
		(SELECT title FROM ".$template."_stations WHERE activation='1' && id=s.station) AS station_name,
		(SELECT name FROM ".$template."_location WHERE activation='1' && id=s.location) AS location_name
		FROM ".$template."_countries AS s
		WHERE s.activation='1'".$add_params."
		GROUP BY s.id
		ORDER BY ".$orderByFlats."
		LIMIT 0,".$LOAD_PAGE."
	");
	$requestSearch = '';
	if(mysql_num_rows($res)>0){
		$res2 = mysql_query("SELECT FOUND_ROWS()", $db);
		$max_rows = mysql_result($res2, 0);
		while($row = mysql_fetch_assoc($res)){
			$link = '/countries/'.$row['id'];
			$image = '<span>Нет фото</span>';
			if($row['images'] && !empty($row['images'])){
				$ex_image = explode(',',$row['images']);
				$image = '<img src="/admin_2/uploads/'.$ex_image[2].'">';
			}
			
			$metro = '';
			if(!empty($row['station'])){
				$metro = '<div class="metro">&laquo;'.$row['station_name'].'&raquo; / <strong>'.$row['distance'].'</strong></div>';
			}
			$name_title = $_TYPE_COUNTRY_ESTATE[$row['type_country']].' ';
			
			$square_meter = ceil($row['price']/$row['full_square']);
			$settings = '';
			$lift = 'нет';
			if(!empty($row['lift'])){
				$lift = 'есть';
			}
			$floors_type = '&nbsp;'; //этаж 4/б
			if(!empty($row['floor'])){
				$floors_type = $_FLOOR_COMMERCIAL[$row['floor']];
			}
			// $settings = '<span>Лифт: <i>'.$lift.'</i>,</span><span>Санузел: <i>'.$wc.'</i>,</span><span>Балкон: <i>'.$balcony.'</i>,</span><span>Телефон: <i>'.$phone.'</i>,</span>';
			$requestSearch .= '<div id="id_'.$row['id'].'" class="item">
				<div class="left_block">
					<div class="title"><a href="'.$link.'">'.$name_title.' <span><strong>'.str_replace(".", ",", price_cell($row['full_square'],1)).'/</strong>'.str_replace(".", ",", price_cell($row['live_square'],1)).' м<sup>2</sup></span></a></div>
					<div class="address">'.$row['address'].'</div>
					'.$metro.'
					<div class="image">
						<a href="'.$link.'">'.$image.'</a>
					</div>
				</div>
				<div class="right_block">
					<div class="row">
						<div class="ceil">
							<div class="full_price"><strong>'.price_cell($row['price'],0).'</strong> руб.</div>
							<div class="meter_price">'.price_cell($square_meter,0).' руб/м<sup>2</sup></div>
						</div>
						<div class="ceil">
							<div class="floor">'.$floors_type.'</div>
							<div class="type">Загородная</div>
						</div>
					</div>
					<div class="row fone">
						<div class="footage">
							<div class="full">Общая <strong>'.str_replace(".", ",", price_cell($row['full_square'],1)).' м<sup>2</sup></strong></div>
						</div>
						<div class="settings">
							'.$settings.'
							<div class="more"><a href="'.$link.'">подробно</a></div>
						</div>
					</div>
				</div>
			</div>';
			
			// $params = array(
				// 'geocode' => $row['address'], // адрес
				// 'format'  => 'json',                          // формат ответа
				// 'results' => 1,                               // количество выводимых результатов
				// 'key'     => 'AKDIIFYBAAAAqIYtRgIAZYb8P32hIxnbRRSe9CpggONv4PEAAAAAAAAAAADu6wMZJIEJ0SDd0mRe33ag1JRdNA==',                           // ваш api key
			// );
			// $response = json_decode(@file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params)));
			
			// $coordsYandex = '';
			// if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0){
				// $coordsYandex = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
			// }
			// if(!empty($coordsYandex)){
				// $ex_coords = explode(' ',$coordsYandex);
				// $ex_coords2 = "[".$ex_coords[1].",".$ex_coords[0]."]";
				// array_push($coordsArray,$ex_coords);
				// array_push($coordsArray2,$ex_coords2);
			// }
			$title_search_page = 'Загородная недвижимость в г. '.$row['location_name'];
			if(isset($rooms_flat_sell_var) || isset($rooms_flat_rent_var)){
				$title_search_page = '';
				if(isset($rooms_flat_sell_var)){
					$roomsArray = $rooms_flat_sell_var;
				}
				if(isset($rooms_flat_rent_var)){
					$roomsArray = $rooms_flat_rent_var;
				}
				$exRoomsArray = explode(',',$roomsArray);
				$roomsArray = array();
				for($c=0; $c<count($exRoomsArray); $c++){
					$roomsName = 'Студии';
					if(!empty($exRoomsArray[$c])){
						$roomsName = $exRoomsArray[$c].'-комн.';
					}
					array_push($roomsArray,$roomsName);
				}
				$title_search_page = implode(', ',$roomsArray).' загородная недвижимость в г. '.$row['location_name'];
			}
		}
	}
	?>
	<div id="center">
		<h1 class="inner_pages">Загородная недвижимость</h1>
		<div class="sort_offers">
			<div class="count_offers">Подходящих предложений:<span><?=$max_rows?></span></div>
			<div class="sort_block">
				<div class="label_select">Сортировать:</div>
				<div class="select_block price">
					<input type="hidden" name="sort">
					<div class="choosed_block">По умолчанию</div>
					<div class="scrollbar-inner">
						<ul>
							<li class="current"><a href="javascript:void(0)" data-id="1">По умолчанию</a></li>
							<li><a href="javascript:void(0)" data-id="1">По цене</a></li>
							<li><a href="javascript:void(0)" data-id="1">По метражу</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div id="result_search">
			<div class="list_items">
				<?=$requestSearch?>
			</div>
		</div>
		<!--<div id="pager"><ul><li class="prev"><a title="Предыдущая" href="#"><i class="fa fa-angle-left"></i><span>Предыдущая</span></a></li><li><a href="#">1</a></li><li><a href="#">2</a></li><li class=current><span>3</span></li><li><a href="#">4</a></li><li><a href="#">5</a></li><li class="next"><a title="Следующая" href="#"><span>Следующая</span><i class="fa fa-angle-right"></i></a></li></ul></div>-->
	</div>
<?}
if(count($params)==2){?>
<div id="center">
	<div class="title_page new">
		<div class="location_card">
			<h1 class="inner_pages"><?=$PageInfo['h1']?></h1>
			<?
			$address = $PageInfo['address'];
			$addressArray = array();
			if(!empty($PageInfo['short_address'])){
				$address = $PageInfo['short_address'];
			}
			$ex_address = explode(',',$address);
			$addss = '';
			for($a=0; $a<count($ex_address); $a++){
				$m = 0;
				if(count($ex_address)==1){
					$m = -1;
				}
				if(count($ex_address)==2){
					$m = -1;
				}
				if(count($ex_address)==4){
					$m = 0;
				}
				if(count($ex_address)==6){
					$m = 2;
				}
				if($a>$m){
					array_push($addressArray,$ex_address[$a]);
				}
			}
	
			$location_name = '';
			if(!empty($PageInfo['location_name'])){
				$l_name = $PageInfo['location_name'];
				$location_name = $l_name;
				if($l_name == 'Санкт-Петербург'){
					$location_name = 'СПб';
				}
				if($l_name == 'Ленинградская область' || $l_name == 'Ленинградская обл.'){
					$location_name = 'ЛО';
				}
				if($l_name == 'Московская область' || $l_name == 'Московская обл.'){
					$location_name = 'МО';
				}
				if($l_name == 'Москва'){
					$location_name = 'МСК';
				}
			}

			$area_name = '';
			$clearAreaName = str_replace('р-н','',$PageInfo['area_name']);
			$clearAreaName = str_replace('г.','',$clearAreaName);
			$clearAreaName = trim($clearAreaName);
			$pos = strripos($PageInfo['area_name'], 'р-н');
			if($pos===true){
				$area_name = $PageInfo['area_name'].' р-н';
			}
			else {
				$pos = strripos($PageInfo['area_name'], 'г.');
				$area_name = $PageInfo['area_name'];
			}
			
			if(count($addressArray)>0){
				$addss = implode(', ',$addressArray);
				$addss = str_replace('улица','ул.',$addss);
				$addss = str_replace('проспект','пр-т',$addss);
				$addss = str_replace('переулок','пер.',$addss);
				$addss = str_replace($clearAreaName,'',$addss);
				$addss = str_replace($locationName,'',$addss);
			}
			$address2 = '<div class="address">'.makeAddress($PageInfo['address'],$PageInfo['short_address'],$PageInfo['location_name'],$PageInfo['area_name'],$PageInfo['location'],$PageInfo['area'],4).'</div>';
			echo $address2;

			$metro = '';
			$metroTable = '';
			if(!empty($PageInfo['station'])){
				$metro = '<div class="metro">&laquo;'.$PageInfo['station_name'].'&raquo; / <strong style="margin-left:0">'.$PageInfo['distance'].'</strong></div>';
				$metroTable = '<div class="metro">&laquo;'.$PageInfo['station_name'].'&raquo; <strong style="margin-left:0">/ '.$PageInfo['distance'].'</strong></div>';
				
				$dist = floor($PageInfo['dist_value']).' м';
				if($dist/1000>=1){
					$dist = price_cell($dist/1000,2).' км';
				}
				$metro = '<div class="metro">&laquo;'.$PageInfo['station_name'].'&raquo; / <strong style="margin-left:0">'.$dist.'</strong></div>';
				$metroTable = '<div class="metro">&laquo;'.$PageInfo['station_name'].'&raquo; <strong style="margin-left:0">/ '.$dist.'</strong></div>';
			}
			else {
				if(!empty($PageInfo['coords']) && !empty($PageInfo['center_coords'])){
					// Расстояние по прямой
					$dist2 = streightDistance($PageInfo['coords'],$PageInfo['center_coords']);
					if(!empty($dist2)){
						$dist = $dist2;
					}
					
					$placeCoords = '';
					if(!empty($PageInfo['typeCoords'])){
						$placeCoords = '&laquo;'.$PageInfo['typeCoords'].'&raquo; / ';
					}
					$metro = '<div class="place">'.$placeCoords.'<strong>'.$dist.'</strong></div>';
					$metroTable = '<div class="place">'.$placeCoords.'<strong>'.$dist.'</strong></div>';
				}
			}

			$v = $PageInfo['dist_value'];
			// $v = $PageInfo['dValue'];
			if(!empty($PageInfo['rid'])){
				if($v/1000>=1){
					$v = price_cell($v/1000,2).' км';
				}
				else {
					$v = price_cell($v,0).' м';
				}
			}
			else {
				if($v/1000>=1){
					$v = price_cell($v/1000,2).' км';
				}
				else {
					$v = price_cell($v,0).' м';
				}
			}
			
			$distance = '<div class="distance"><strong>'.$v.'</strong> | <a class="map" data-link="#map" href="'.$link.'#map">Показать на карте</a></div>';
			if(empty($v)){
				$distance = '<div class="distance"><a class="map" data-link="#map" href="'.$link.'#map">Показать на карте</a></div>';
			}
			
			$distance = '<div class="distance"><strong>'.$v.'</strong> | <a class="map" data-link="#map" href="'.$link.'#map">Показать на карте</a></div>';
			if(empty($v) || $v=='0 м'){
				$distance = '<div class="distance"><a class="map" data-link="#map" href="'.$link.'#map">Показать на карте</a></div>';
			}


			if(!empty($PageInfo['station'])){
				$v = $PageInfo['dist_value'];
				if($v/1000>=1){
					$v = price_cell($v/1000,2).' км';
				}
				else {
					$v = price_cell($v,0).' м';
				}
				$coordsWay = $PageInfo['coords_area'];
				if(!empty($PageInfo['coords_station'])){
					$coordsWay = $PageInfo['coords_station'];
				}
				$coords = $PageInfo['coords'];
				$ex_coords = explode(',',$coords);
				if(count($ex_coords)>1){
					$coords = $ex_coords[1].' '.$ex_coords[0];
				}
				/*
				*  Расстояние по прямой
				*/
				$dist2 = streightDistance($coords,$coordsWay);
				if(!empty($dist2)){
					$v = $dist2;
				}
				
				$distance = '<div class="metro"><a href="/search?type=sell&estate=4&priceAll=all&station='.$PageInfo['station'].'">'.$PageInfo['station_name'].'</a><strong>'.$v.'</strong> | <a class="map" data-link="#map" href="#map">Показать на карте</a></div>';			
			}
			else {
				$coordsWay = $PageInfo['coords_area'];
				if(!empty($PageInfo['coords_station'])){
					$coordsWay = $PageInfo['coords_station'];
				}
				if(!empty($PageInfo['coords']) && !empty($coordsWay)){
					$coords = $PageInfo['coords'];
					$ex_coords = explode(',',$coords);
					if(count($ex_coords)>1){
						$coords = $ex_coords[1].' '.$ex_coords[0];
					}
					/*
					*  Расстояние по прямой
					*/
					$dist2 = streightDistance($coords,$coordsWay);
					if(!empty($dist2)){
						$dist = $dist2;
					}

					$distance = '<div class="place">'.$placeCoords.'<strong>'.$dist.'</strong> | <a class="map" data-link="#map" href="#map">Показать на карте</a></div>';
				}
			}
			echo $distance;
			if($PageInfo['type_country']==6){
				$square_meter = @ceil($PageInfo['price']/$PageInfo['area_square']);
			}
			else {
				if(empty($PageInfo['full_square'])){
					$square_meter = 0;
				}
				else {
					$square_meter = @ceil($PageInfo['price']/$PageInfo['full_square']);
				}
			}
			
			$rentText = '';
			$rentArray = array();
			if(!empty($PageInfo['communal'])){
				array_push($rentArray,'включая коммунальные платежи');
			}
			if(!empty($PageInfo['prepayment'])){
				if($PageInfo['type']=='rent'){
					if($PageInfo['prepayment']!=1){
						if($PageInfo['prepayment']<=100){
							array_push($rentArray,'Комиссия '.$PageInfo['prepayment'].'%');
						}
						else {
							array_push($rentArray,'Комиссия '.$PageInfo['prepayment'].' <span class="rub">Р</span>');
						}
					}
				}
			}
			if(!empty($PageInfo['deposit'])){
				if($PageInfo['type']=='rent'){
					array_push($rentArray,'Залог '.$PageInfo['deposit'].' <span class="rub">Р</span>');
				}
			}
			if(!empty($PageInfo['rental_holidays'])){
				array_push($rentArray,'арендные каникулы');
			}
			if(!empty($PageInfo['no_agent'])){
				array_push($rentArray,'агентам не звонить');
			}
			if(count($rentArray)>0){
				$rentText = '<div class="communal_block">'.implode('<br>',$rentArray).'</div>';
			}
						
			$meter_price = '';
			if(!empty($square_meter)){
				$meter_price = '<div class="meter_price">'.price_cell($square_meter,0).' <span class="rub">Р</span>/м<sup>2</sup></div>';
				if($PageInfo['type']=='rent'){
					$meter_price = '<div class="meter_price">'.price_cell($square_meter,0).' <span class="rub">Р</span>/м<sup>2</sup> в мес.</div>';
				}
			}
			
			$full_price = '<div class="full_price"><strong>'.price_cell($PageInfo['price'],0).'</strong> <span class="rub">Р</span></div>';
			if($PageInfo['type']=='rent'){
				$full_price = '<div class="full_price"><strong>'.price_cell($PageInfo['price'],0).'</strong> <span class="rub">Р</span>/мес.</div>';
			}
			?>
		</div>
		<div class="price_card">
			<div class="prices">
				<?=$full_price?>
				<?=$meter_price?>
				<?=$rentText?>
			</div>
		</div>
	</div>
	<div class="separate"></div>
	<div class="card_estate new_card" id="housing_estate">
		<?
			$res = mysql_query("
				SELECT *
				FROM ".$template."_photo_catalogue
				WHERE estate='country' && p_main='".$PageInfo['id']."'
				ORDER BY cover DESC, num
			");
			if(mysql_num_rows($res)>0){
				echo '<div class="fotogallery">';
				echo '<div class="fotorama fotorama-skin-rsp fotorama-skin-rsp_hidden"
					 data-nav="thumbs"
					 data-height="421"
					 data-keyboard="true"
					 data-navposition="bottom"
					 data-loop="true"
					 data-allowfullscreen="native"
					 data-fit="contain">';
				while($row = mysql_fetch_assoc($res)){
					$ex_images = explode(',',$row['images']);
					if($row['cover']==1){
						$linkImgMid = '/admin_2/uploads/'.$ex_images[3];
						$linkImgMini = '/admin_2/uploads/'.$ex_images[4];
						if($row['user_id']!=0){
							$linkImgMid = '/users/'.$row['user_id'].'/'.$ex_images[3];
							$linkImgMini = '/users/'.$row['user_id'].'/'.$ex_images[4];
						}
						echo '<a href="'.$linkImgMini.'" data-thumb="'.$linkImgMid.'" data-full="'.$linkImgMini.'">
							<img src="'.$linkImgMini.'" />
						</a>';
					}
					else {
						$linkImgMid = '/admin_2/uploads/'.$ex_images[3];
						$linkImgMini = '/admin_2/uploads/'.$ex_images[4];
						if($row['user_id']!=0){
							$linkImgMid = '/users/'.$row['user_id'].'/'.$ex_images[3];
							$linkImgMini = '/users/'.$row['user_id'].'/'.$ex_images[4];
						}
						echo '<a href="'.$linkImgMini.'" data-thumb="'.$linkImgMid.'" data-full="'.$linkImgMini.'">
							<img src="'.$linkImgMini.'" />
						</a>';
					}
				}
				echo '</div>';
				echo '</div>';
				echo '<script type="text/javascript" src="/libs/fotorama/js/fotorama.js"></script>';
				echo '<script type="text/javascript" src="/libs/fotorama/js/_fotorama_2.js"></script>';
			}
			else {
				echo '<div class="fotogallery">';
				echo '<div class="fotorama fotorama-skin-rsp fotorama-skin-rsp_hidden"
					 data-nav="thumbs"
					 data-height="420"
					 data-keyboard="true"
					 data-navposition="bottom"
					 data-loop="true"
					 data-allowfullscreen="native"
					 data-fit="scaledown">
						<img src="/images/no_photo_650x420.png"/></div>';
				echo '</div>';
				echo '<script type="text/javascript" src="/libs/fotorama/js/fotorama.js"></script>';
				echo '<script type="text/javascript" src="/libs/fotorama/js/_fotorama_2.js"></script>';
			}
			
			/*
			*  Количество комнат
			*/
			$rooms = '';
			if(isset($PageInfo['rooms'])){
				$room = 'Студия';
				if($PageInfo['rooms']>0){
					$room = $PageInfo['rooms'];
				}
				$rooms = '<div class="row">
					<div class="cell label">Количество комнат:</div>
					<div class="cell">'.$room.'</div>
				</div>';
			}
			
			/*
			*  Тип дома
			*/
			$type_house = '';
			if(!empty($PageInfo['type_house'])){
				$type_house = '<div class="row">
					<div class="cell label">Тип дома:</div>
					<div class="cell">'.$_TYPE_HOUSE_COUNTRY[$PageInfo['type_house']].'</div>
				</div>';
			}
			
			/*
			*  Кол-во этажей
			*/
			$floors = '';
			if(!empty($PageInfo['floor'])){
				if(!empty($PageInfo['floors'])){
					$floors = '<div class="row">
						<div class="cell label">Этаж:</div>
						<div class="cell">'.$PageInfo['floor'].' (из '.$PageInfo['floors'].')</div>
					</div>';
				}
				else {
					$floors = '<div class="row">
						<div class="cell label">Этаж:</div>
						<div class="cell">'.$PageInfo['floor'].'</div>
					</div>';
				}
			}
			
			$paramsBlock = $rooms.$type_house.$floors.$ceiling_height.$finishing;
			$paramsBlock2 = '';

			/*
			*  Коттеджный поселок
			*/
			if($PageInfo['type_country']==5){
				/*
				*  Название посёлка
				*/
				$name_villiage = '';
				if(!empty($PageInfo['show_build'])){
					if(!empty($PageInfo['name_villiage'])){
						$name_villiage = '<div class="row">
							<div class="cell label">Название посёлка:</div>
							<div class="cell">'.$PageInfo['name_villiage'].'</div>
						</div>';
					}
				}
				
				/*
				*  Площадь посёлка
				*/
				$area_village = '';
				if(!empty($PageInfo['area_village'])){
					$area_village = '<div class="row">
						<div class="cell label">Площадь посёлка:</div>
						<div class="cell">'.$PageInfo['area_village'].'</div>
					</div>';
				}
				
				/*
				*  Количество участков
				*/
				$count_plots = '';
				if(!empty($PageInfo['count_plots'])){
					$count_plots = '<div class="row">
						<div class="cell label">Количество участков:</div>
						<div class="cell">'.$PageInfo['count_plots'].'</div>
					</div>';
				}
				
				/*
				*  Тип дома
				*/
				$type_house = '';
				if(!empty($PageInfo['type_house'])){
					$type_house = '<div class="row">
						<div class="cell label">Тип дома:</div>
						<div class="cell">'.$_TYPE_HOUSE_COUNTRY[$PageInfo['type_house']].'</div>
					</div>';
				}
				
				/*
				*  Год постройки дома
				*/
				$year = '';
				if(!empty($PageInfo['year'])){
					$year = '<div class="row">
						<div class="cell label">Год постройки дома:</div>
						<div class="cell">'.$PageInfo['year'].'</div>
					</div>';
				}
				
				$paramsBlock = $name_villiage.$area_village.$count_plots.$type_house.$year;
				
				/*
				*  Общая площадь
				*/
				$full_square = '';
				if(!empty($PageInfo['full_square'])){
					$full_square = '<div class="row">
						<div class="cell label">Общая площадь:</div>
						<div class="cell">'.$PageInfo['full_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Количество спален
				*/
				$bedrooms = '';
				if(!empty($PageInfo['bedrooms'])){
					$bedrooms = '<div class="row">
						<div class="cell label">Количество спален:</div>
						<div class="cell">'.$PageInfo['bedrooms'].'</div>
					</div>';
				}
				
				/*
				*  Кухня
				*/
				$kitchen_square = '';
				if(!empty($PageInfo['kitchen_square'])){
					$kitchen_square = '<div class="row">
						<div class="cell label">Площадь кухни:</div>
						<div class="cell">'.$PageInfo['kitchen_square'].'</div>
					</div>';
				}
				/*
				*  Площадь помещений
				*/
				$rooms_square = '';
				if(!empty($PageInfo['rooms_square'])){
					$rooms_square = '<div class="row">
						<div class="cell label">Площадь помещений:</div>
						<div class="cell">'.$PageInfo['rooms_square'].' м²</div>
					</div>';
				}
				/*
				*  Высота потолков
				*/
				$ceiling_height = '';
				if(!empty($PageInfo['ceiling_height'])){
					$ceiling_height = '<div class="row">
						<div class="cell label">Высота потолков:</div>
						<div class="cell">'.str_replace(".", ",", $PageInfo['ceiling_height']).' м</div>
					</div>';
				}
				/*
				*  Этажей в доме
				*/
				$floors = '';
				if(!empty($PageInfo['floors'])){
					$floors = '<div class="row">
						<div class="cell label">Этажей в доме:</div>
						<div class="cell">'.str_replace(".", ",", $PageInfo['floors']).' м</div>
					</div>';
				}
				if(!empty($PageInfo['basement']) || !empty($PageInfo['semibasement'])){
					$floorsArray = array();
					if(!empty($PageInfo['basement'])){
						array_push($floorsArray,'Подвал');
					}
					if(!empty($PageInfo['semibasement'])){
						array_push($floorsArray,'Полуподвал');
					}
					$floorsType = '';
					if(count($floorsArray)>0){
						$floorsType = ' + '.implode(', ',$floorsArray);
					} 
					$floors = '<div class="row">
						<div class="cell label">Этажей в доме:</div>
						<div class="cell">'.$PageInfo['floors'].''.$floorsType.'</div>
					</div>';
				}
				
/******************/
				$arrayInfras = array();
				/*
				*  Круглосуточная охрана территории
				*/
				$security = '';
				if(!empty($PageInfo['security'])){
					$security = '<div class="row">
						<div class="cell label">Круглосуточная охрана территории:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Круглосуточная охрана территории</li>');
				}
				
				/*
				*  Ограждение территории поселка
				*/
				$fencing = '';
				if(!empty($PageInfo['fencing'])){
					$fencing = '<div class="row">
						<div class="cell label">Ограждение территории поселка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Ограждение территории поселка</li>');
				}
				
				/*
				*  Ограждение территории поселка
				*/
				$maintenance = '';
				if(!empty($PageInfo['maintenance'])){
					$maintenance = '<div class="row">
						<div class="cell label">Служба эксплуатации в поселка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Служба эксплуатации в поселка</li>');
				}
				
				/*
				*  Кафе
				*/
				$cafe = '';
				if(!empty($PageInfo['cafe'])){
					$cafe = '<div class="row">
						<div class="cell label">Кафе:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Кафе</li>');
				}
				
				/*
				*  Магазин
				*/
				$shop = '';
				if(!empty($PageInfo['shop'])){
					$shop = '<div class="row">
						<div class="cell label">Магазин:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Магазин</li>');
				}
				
				/*
				*  Детская площадка
				*/
				$playground = '';
				if(!empty($PageInfo['playground'])){
					$playground = '<div class="row">
						<div class="cell label">Детская площадка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Детская площадка</li>');
				}
				
				/*
				*  Спортивная площадка
				*/
				$sport_playground = '';
				if(!empty($PageInfo['sport_playground'])){
					$sport_playground = '<div class="row">
						<div class="cell label">Спортивная площадка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Спортивная площадка</li>');
				}
				
				/*
				*  Медпункт
				*/
				$first_post = '';
				if(!empty($PageInfo['first_post'])){
					$first_post = '<div class="row">
						<div class="cell label">Медпункт:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Медпункт</li>');
				}
				
				/*
				*  Гостевая парковка
				*/
				$guest_parking = '';
				if(!empty($PageInfo['guest_parking'])){
					$guest_parking = '<div class="row">
						<div class="cell label">Гостевая парковка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Гостевая парковка</li>');
				}
				
				/*
				*  Водоём
				*/
				$water = '';
				if(!empty($PageInfo['water'])){
					$water = '<div class="row">
						<div class="cell label">Водоём:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Водоём</li>');
				}
				
				/*
				*  Поликлиника
				*/
				$polyclinic = '';
				if(!empty($PageInfo['polyclinic'])){
					$polyclinic = '<div class="row">
						<div class="cell label">Поликлиника:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Поликлиника</li>');
				}
				
				/*
				*  Пирс
				*/
				$pier = '';
				if(!empty($PageInfo['pier'])){
					$pier = '<div class="row">
						<div class="cell label">Пирс:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Пирс</li>');
				}
				
				/*
				*  Школа
				*/
				$school = '';
				if(!empty($PageInfo['school'])){
					$school = '<div class="row">
						<div class="cell label">Школа:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Школа</li>');
				}
				
				/*
				*  Рядом лес
				*/
				$forest = '';
				if(!empty($PageInfo['forest'])){
					$forest = '<div class="row">
						<div class="cell label">Рядом лес:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Рядом лес</li>');
				}
				
				/*
				*  Детский сад
				*/
				$kindergarten = '';
				if(!empty($PageInfo['kindergarten'])){
					$kindergarten = '<div class="row">
						<div class="cell label">Детский сад:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Детский сад</li>');
				}
				
				/*
				*  Ремонт
				*/
				$finishing = '';
				if(!empty($PageInfo['finishing'])){
					$finishing = '<div class="row">
						<div class="cell label">Ремонт:</div>
						<div class="cell">'.$_TYPE_FINISHING_COUNTRY[$PageInfo['finishing']].'</div>
					</div>';
				}
				
				/*
				*  Коммуникации
				*/
				$communications = '';
				if(!empty($PageInfo['communications'])){
					$communications = '<div class="row">
						<div class="cell label">Коммуникации:</div>
						<div class="cell">'.$_COMMUNICATIONS[$PageInfo['communications']].'</div>
					</div>';
				}
				
				/*
				*  Водоснабжение
				*/
				$plumbing = '';
				if(!empty($PageInfo['plumbing'])){
					$plumbing = '<div class="row">
						<div class="cell label">Водоснабжение:</div>
						<div class="cell">'.$_PLUMBING[$PageInfo['plumbing']].'</div>
					</div>';
				}
				
				/*
				*  Газоснабжение
				*/
				$gas_supply = '';
				if(!empty($PageInfo['gas_supply'])){
					$gas_supply = '<div class="row">
						<div class="cell label">Газоснабжение:</div>
						<div class="cell">'.$_GASSUPPLY[$PageInfo['gas_supply']].'</div>
					</div>';
				}
				
				/*
				*  Канализиция
				*/
				$sewerage = '';
				if(!empty($PageInfo['sewerage'])){
					$sewerage = '<div class="row">
						<div class="cell label">Канализация:</div>
						<div class="cell">'.$_SEWERAGE[$PageInfo['sewerage']].'</div>
					</div>';
				}
				
				/*
				*  Электричество
				*/
				$power_max = '';
				if(!empty($PageInfo['power_max'])){
					$power_max = '<div class="row">
						<div class="cell label">Электричество:</div>
						<div class="cell">'.$PageInfo['power_max'].' кВт</div>
					</div>';
				}
				
				/*
				*  Отопление
				*/
				$heating = '';
				if(!empty($PageInfo['heating'])){
					$firePlace = '';
					if(!empty($PageInfo['fireplace'])){
						$firePlace = ' + камин';
					}
					$heating = '<div class="row">
						<div class="cell label">Отопление:</div>
						<div class="cell">'.$_HEATING_COUNTRY[$PageInfo['heating']].$firePlace.'</div>
					</div>';
				}
				
				/*
				*  На участке
				*/
				$have = '';
				if(!empty($PageInfo['internet']) || !empty($PageInfo['cab_tv']) || !empty($PageInfo['garage']) || !empty($PageInfo['sauna']) || !empty($PageInfo['pool'])){
					$arr = '';
					$ar = array();
					if(!empty($PageInfo['internet'])){
						array_push($ar,'Интернет');
					}
					if(!empty($PageInfo['cab_tv'])){
						array_push($ar,'Кабельное ТВ');
					}
					if(!empty($PageInfo['garage'])){
						array_push($ar,'Гараж');
					}
					if(!empty($PageInfo['sauna'])){
						array_push($ar,'Баня');
					}
					if(!empty($PageInfo['pool'])){
						array_push($ar,'Бассейн');
					}
					if(count($ar)>0){
						$arr = implode(', ',$ar);
					}
					$have = '<div class="row">
						<div class="cell label">На участке:</div>
						<div class="cell">'.$arr.'</div>
					</div>';
				}
				
				/*
				*  Площадь земельного участка
				*/
				$area_square = '';
				if(!empty($PageInfo['area_square'])){
					$area_square = '<div class="row">
						<div class="cell label">Площадь земельного участка:</div>
						<div class="cell">'.str_replace(".", ",", $PageInfo['area_square']).' соток</div>
					</div>';
				}
				
				/*
				*  Разрешенное использование
				*/
				$type_use = '';
				if(!empty($PageInfo['type_use'])){
					$type_use = '<div class="row">
						<div class="cell label">Разрешенное использование:</div>
						<div class="cell">'.str_replace(".", ",", $_TYPES_USE[$PageInfo['type_use']]).'</div>
					</div>';
				}
				
				/*
				*  Категория ЗУ
				*/
				$category = '';
				if(!empty($PageInfo['category'])){
					$category = '<div class="row">
						<div class="cell label">Категория ЗУ:</div>
						<div class="cell">'.str_replace(".", ",", $_CATEGORY_COUNTRY[$PageInfo['category']]).'</div>
					</div>';
				}
				
				/*
				*  Право на ЗУ
				*/
				$rights = '';
				if(!empty($PageInfo['rights'])){
					$rights = '<div class="row">
						<div class="cell label">Право на ЗУ:</div>
						<div class="cell">'.str_replace(".", ",", $_RIGHTS[$PageInfo['rights']]).'</div>
					</div>';
				}
								
				$paramsBlock2 = $full_square.$kitchen_square.$rooms_square.$ceiling_height.$floors.$bedrooms.$finishing.$area_square.$type_use.$category.$rights.$heating.$communications.$plumbing.$sewerage.$gas_supply.$power_max.$have;
			}
			
			/*
			*  Земельный участок (посёлок)
			*/
			if($PageInfo['type_country']==6){
				/*
				*  Название коттеджного посёлка
				*/
				$name_villiage = '';
				if(!empty($PageInfo['show_build'])){
					if(!empty($PageInfo['name_villiage'])){
						$name_villiage = '<div class="row">
							<div class="cell label">Название посёлка:</div>
							<div class="cell">'.$PageInfo['name_villiage'].'</div>
						</div>';
					}
				}
				
				/*
				*  Площадь участка
				*/
				$area_square = '';
				if(!empty($PageInfo['area_square'])){
					$area_square = '<div class="row">
						<div class="cell label">Площадь участка:</div>
						<div class="cell">'.$PageInfo['area_square'].' соток</div>
					</div>';
				}
				
				$paramsBlock = $name_villiage.$area_square;
				
/******************/
				$arrayInfras = array();
				/*
				*  Круглосуточная охрана территории
				*/
				$security = '';
				if(!empty($PageInfo['security'])){
					$security = '<div class="row">
						<div class="cell label">Круглосуточная охрана территории:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Круглосуточная охрана территории</li>');
				}
				
				/*
				*  Ограждение территории поселка
				*/
				$fencing = '';
				if(!empty($PageInfo['fencing'])){
					$fencing = '<div class="row">
						<div class="cell label">Ограждение территории поселка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Ограждение территории поселка</li>');
				}
				
				/*
				*  Ограждение территории поселка
				*/
				$maintenance = '';
				if(!empty($PageInfo['maintenance'])){
					$maintenance = '<div class="row">
						<div class="cell label">Служба эксплуатации в поселка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Служба эксплуатации в поселка</li>');
				}
				
				/*
				*  Кафе
				*/
				$cafe = '';
				if(!empty($PageInfo['cafe'])){
					$cafe = '<div class="row">
						<div class="cell label">Кафе:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Кафе</li>');
				}
				
				/*
				*  Магазин
				*/
				$shop = '';
				if(!empty($PageInfo['shop'])){
					$shop = '<div class="row">
						<div class="cell label">Магазин:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Магазин</li>');
				}
				
				/*
				*  Детская площадка
				*/
				$playground = '';
				if(!empty($PageInfo['playground'])){
					$playground = '<div class="row">
						<div class="cell label">Детская площадка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Детская площадка</li>');
				}
				
				/*
				*  Спортивная площадка
				*/
				$sport_playground = '';
				if(!empty($PageInfo['sport_playground'])){
					$sport_playground = '<div class="row">
						<div class="cell label">Спортивная площадка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Спортивная площадка</li>');
				}
				
				/*
				*  Медпункт
				*/
				$first_post = '';
				if(!empty($PageInfo['first_post'])){
					$first_post = '<div class="row">
						<div class="cell label">Медпункт:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Медпункт</li>');
				}
				
				/*
				*  Гостевая парковка
				*/
				$guest_parking = '';
				if(!empty($PageInfo['guest_parking'])){
					$guest_parking = '<div class="row">
						<div class="cell label">Гостевая парковка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Гостевая парковка</li>');
				}
				
				/*
				*  Водоём
				*/
				$water = '';
				if(!empty($PageInfo['water'])){
					$water = '<div class="row">
						<div class="cell label">Водоём:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Водоём</li>');
				}
				
				/*
				*  Поликлиника
				*/
				$polyclinic = '';
				if(!empty($PageInfo['polyclinic'])){
					$polyclinic = '<div class="row">
						<div class="cell label">Поликлиника:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Поликлиника</li>');
				}
				
				/*
				*  Пирс
				*/
				$pier = '';
				if(!empty($PageInfo['pier'])){
					$pier = '<div class="row">
						<div class="cell label">Пирс:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Пирс</li>');
				}
				
				/*
				*  Школа
				*/
				$school = '';
				if(!empty($PageInfo['school'])){
					$school = '<div class="row">
						<div class="cell label">Школа:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Школа</li>');
				}
				
				/*
				*  Рядом лес
				*/
				$forest = '';
				if(!empty($PageInfo['forest'])){
					$forest = '<div class="row">
						<div class="cell label">Рядом лес:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Рядом лес</li>');
				}
				
				/*
				*  Детский сад
				*/
				$kindergarten = '';
				if(!empty($PageInfo['kindergarten'])){
					$kindergarten = '<div class="row">
						<div class="cell label">Детский сад:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Детский сад</li>');
				}
				
				/* 
				*  Коммуникации
				*/
				$communications = '';
				if(!empty($PageInfo['communications'])){
					$communications = '<div class="row">
						<div class="cell label">Коммуникации:</div>
						<div class="cell">'.$_COMMUNICATIONS[$PageInfo['communications']].'</div>
					</div>';
				}
				
				/*
				*  Водоснабжение
				*/
				$plumbing = '';
				if(!empty($PageInfo['plumbing'])){
					$plumbing = '<div class="row">
						<div class="cell label">Водоснабжение:</div>
						<div class="cell">'.$_PLUMBING[$PageInfo['plumbing']].'</div>
					</div>';
				}
				
				/*
				*  Газоснабжение
				*/
				$gas_supply = '';
				if(!empty($PageInfo['gas_supply'])){
					$gas_supply = '<div class="row">
						<div class="cell label">Газоснабжение:</div>
						<div class="cell">'.$_GASSUPPLY[$PageInfo['gas_supply']].'</div>
					</div>';
				}
				
				/*
				*  Канализиция
				*/
				$sewerage = '';
				if(!empty($PageInfo['sewerage'])){
					$sewerage = '<div class="row">
						<div class="cell label">Канализация:</div>
						<div class="cell">'.$_SEWERAGE[$PageInfo['sewerage']].'</div>
					</div>';
				}
				
				/*
				*  Электричество
				*/
				$power_max = '';
				if(!empty($PageInfo['power_max'])){
					$power_max = '<div class="row">
						<div class="cell label">Электричество:</div>
						<div class="cell">'.$PageInfo['power_max'].' кВт</div>
					</div>';
				}
				
				/*
				*  Отопление
				*/
				$heating = '';
				if(!empty($PageInfo['heating'])){
					$firePlace = '';
					if(!empty($PageInfo['fireplace'])){
						$firePlace = ' + камин';
					}
					$heating = '<div class="row">
						<div class="cell label">Отопление:</div>
						<div class="cell">'.$_HEATING_COUNTRY[$PageInfo['heating']].$firePlace.'</div>
					</div>';
				}
				
				/*
				*  На участке
				*/
				$have = '';
				if(!empty($PageInfo['internet']) || !empty($PageInfo['cab_tv']) || !empty($PageInfo['garage']) || !empty($PageInfo['sauna']) || !empty($PageInfo['pool'])){
					$arr = '';
					$ar = array();
					if(!empty($PageInfo['internet'])){
						array_push($ar,'Интернет');
					}
					if(!empty($PageInfo['cab_tv'])){
						array_push($ar,'Кабельное ТВ');
					}
					if(count($ar)>0){
						$arr = implode(', ',$ar);
					}
					$have = '<div class="row">
						<div class="cell label">На участке:</div>
						<div class="cell">'.$arr.'</div>
					</div>';
				}
				
				/*
				*  Разрешенное использование
				*/
				$type_use = '';
				if(!empty($PageInfo['type_use'])){
					$type_use = '<div class="row">
						<div class="cell label">Разрешенное использование:</div>
						<div class="cell">'.str_replace(".", ",", $_TYPES_USE[$PageInfo['type_use']]).'</div>
					</div>';
				}
				
				/*
				*  Категория ЗУ
				*/
				$category = '';
				if(!empty($PageInfo['category'])){
					$category = '<div class="row">
						<div class="cell label">Категория ЗУ:</div>
						<div class="cell">'.str_replace(".", ",", $_CATEGORY_COUNTRY[$PageInfo['category']]).'</div>
					</div>';
				}
				
				/*
				*  Право на ЗУ 
				*/
				$rights = '';
				if(!empty($PageInfo['rights'])){
					$rights = '<div class="row">
						<div class="cell label">Право на ЗУ:</div>
						<div class="cell">'.str_replace(".", ",", $_RIGHTS[$PageInfo['rights']]).'</div>
					</div>';
				}
				
				/*
				*  Кадастровый номер
				*/
				$cadastr = '';
				if(!empty($PageInfo['cadastr'])){
					$cadastr = '<div class="row">
						<div class="cell label">Кадастровый номер:</div>
						<div class="cell">'.$PageInfo['cadastr'].'</div>
					</div>';
				}
				
				/*
				*  Застройщик
				*/
				$developer = '';
				if(!empty($PageInfo['developer'])){
					$developer = '<div class="row">
						<div class="cell label">Застройщик:</div>
						<div class="cell">'.$PageInfo['developer'].'</div>
					</div>';
				}
				
				/*
				*  Площадь посёлка
				*/
				$area_village = '';
				if(!empty($PageInfo['area_village'])){
					$area_village = '<div class="row">
						<div class="cell label">Площадь посёлка:</div>
						<div class="cell">'.$PageInfo['area_village'].'</div>
					</div>';
				}
				
				/*
				*  Количество участков
				*/
				$count_plots = '';
				if(!empty($PageInfo['count_plots'])){
					$count_plots = '<div class="row">
						<div class="cell label">Количество участков:</div>
						<div class="cell">'.$PageInfo['count_plots'].'</div>
					</div>';
				}
								
				$paramsBlock2 = $cadastr.$area_village.$developer.$count_plots.$type_use.$category.$rights.$heating.$communications.$plumbing.$sewerage.$gas_supply.$power_max.$have;
			}
			
			/*
			*  Земельный участок
			*/
			if($PageInfo['type_country']==4){
				/*
				*  Название посёлка
				*/
				$name_villiage = '';
				if(!empty($PageInfo['show_build'])){
					if(!empty($PageInfo['name_villiage'])){
						$name_villiage = '<div class="row">
							<div class="cell label">Название посёлка:</div>
							<div class="cell">'.$PageInfo['name_villiage'].'</div>
						</div>';
					}
				}
				
				/*
				*  Площадь участка
				*/
				$area_square = '';
				if(!empty($PageInfo['area_square'])){
					$area_square = '<div class="row">
						<div class="cell label">Площадь участка:</div>
						<div class="cell">'.$PageInfo['area_square'].' соток</div>
					</div>';
				}
				
				$paramsBlock = $name_villiage.$area_square;
				
/******************/
				$arrayInfras = array();
				/*
				*  Круглосуточная охрана территории
				*/
				$security = '';
				if(!empty($PageInfo['security'])){
					$security = '<div class="row">
						<div class="cell label">Круглосуточная охрана территории:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Круглосуточная охрана территории</li>');
				}
				
				/*
				*  Ограждение территории поселка
				*/
				$fencing = '';
				if(!empty($PageInfo['fencing'])){
					$fencing = '<div class="row">
						<div class="cell label">Ограждение территории поселка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Ограждение территории поселка</li>');
				}
				
				/*
				*  Ограждение территории поселка
				*/
				$maintenance = '';
				if(!empty($PageInfo['maintenance'])){
					$maintenance = '<div class="row">
						<div class="cell label">Служба эксплуатации в поселка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Служба эксплуатации в поселка</li>');
				}
				
				/*
				*  Кафе
				*/
				$cafe = '';
				if(!empty($PageInfo['cafe'])){
					$cafe = '<div class="row">
						<div class="cell label">Кафе:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Кафе</li>');
				}
				
				/*
				*  Магазин
				*/
				$shop = '';
				if(!empty($PageInfo['shop'])){
					$shop = '<div class="row">
						<div class="cell label">Магазин:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Магазин</li>');
				}
				
				/*
				*  Детская площадка
				*/
				$playground = '';
				if(!empty($PageInfo['playground'])){
					$playground = '<div class="row">
						<div class="cell label">Детская площадка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Детская площадка</li>');
				}
				
				/*
				*  Спортивная площадка
				*/
				$sport_playground = '';
				if(!empty($PageInfo['sport_playground'])){
					$sport_playground = '<div class="row">
						<div class="cell label">Спортивная площадка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Спортивная площадка</li>');
				}
				
				/*
				*  Медпункт
				*/
				$first_post = '';
				if(!empty($PageInfo['first_post'])){
					$first_post = '<div class="row">
						<div class="cell label">Медпункт:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Медпункт</li>');
				}
				
				/*
				*  Гостевая парковка
				*/
				$guest_parking = '';
				if(!empty($PageInfo['guest_parking'])){
					$guest_parking = '<div class="row">
						<div class="cell label">Гостевая парковка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Гостевая парковка</li>');
				}
				
				/*
				*  Водоём
				*/
				$water = '';
				if(!empty($PageInfo['water'])){
					$water = '<div class="row">
						<div class="cell label">Водоём:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Водоём</li>');
				}
				
				/*
				*  Поликлиника
				*/
				$polyclinic = '';
				if(!empty($PageInfo['polyclinic'])){
					$polyclinic = '<div class="row">
						<div class="cell label">Поликлиника:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Поликлиника</li>');
				}
				
				/*
				*  Пирс
				*/
				$pier = '';
				if(!empty($PageInfo['pier'])){
					$pier = '<div class="row">
						<div class="cell label">Пирс:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Пирс</li>');
				}
				
				/*
				*  Школа
				*/
				$school = '';
				if(!empty($PageInfo['school'])){
					$school = '<div class="row">
						<div class="cell label">Школа:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Школа</li>');
				}
				
				/*
				*  Рядом лес
				*/
				$forest = '';
				if(!empty($PageInfo['forest'])){
					$forest = '<div class="row">
						<div class="cell label">Рядом лес:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Рядом лес</li>');
				}
				
				/*
				*  Детский сад
				*/
				$kindergarten = '';
				if(!empty($PageInfo['kindergarten'])){
					$kindergarten = '<div class="row">
						<div class="cell label">Детский сад:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Детский сад</li>');
				}
				
				/*
				*  Ремонт
				*/
				$finishing = '';
				if(!empty($PageInfo['finishing'])){
					$finishing = '<div class="row">
						<div class="cell label">Ремонт:</div>
						<div class="cell">'.$_TYPE_FINISHING_COUNTRY[$PageInfo['finishing']].'</div>
					</div>';
				}
				
				/*
				*  Коммуникации
				*/
				$communications = '';
				if(!empty($PageInfo['communications'])){
					$communications = '<div class="row">
						<div class="cell label">Коммуникации:</div>
						<div class="cell">'.$_COMMUNICATIONS[$PageInfo['communications']].'</div>
					</div>';
				}
				
				/*
				*  Водоснабжение
				*/
				$plumbing = '';
				if(!empty($PageInfo['plumbing'])){
					$plumbing = '<div class="row">
						<div class="cell label">Водоснабжение:</div>
						<div class="cell">'.$_PLUMBING[$PageInfo['plumbing']].'</div>
					</div>';
				}
				
				/*
				*  Газоснабжение
				*/
				$gas_supply = '';
				if(!empty($PageInfo['gas_supply'])){
					$gas_supply = '<div class="row">
						<div class="cell label">Газоснабжение:</div>
						<div class="cell">'.$_GASSUPPLY[$PageInfo['gas_supply']].'</div>
					</div>';
				}
				
				/*
				*  Канализиция
				*/
				$sewerage = '';
				if(!empty($PageInfo['sewerage'])){
					$sewerage = '<div class="row">
						<div class="cell label">Канализация:</div>
						<div class="cell">'.$_SEWERAGE[$PageInfo['sewerage']].'</div>
					</div>';
				}
				
				/*
				*  Электричество
				*/
				$power_max = '';
				if(!empty($PageInfo['power_max'])){
					$power_max = '<div class="row">
						<div class="cell label">Электричество:</div>
						<div class="cell">'.$PageInfo['power_max'].' кВт</div>
					</div>';
				}
				
				/*
				*  Отопление
				*/
				$heating = '';
				if(!empty($PageInfo['heating'])){
					$firePlace = '';
					if(!empty($PageInfo['fireplace'])){
						$firePlace = ' + камин';
					}
					$heating = '<div class="row">
						<div class="cell label">Отопление:</div>
						<div class="cell">'.$_HEATING_COUNTRY[$PageInfo['heating']].$firePlace.'</div>
					</div>';
				}
				
				/*
				*  На участке
				*/
				$have = '';
				if(!empty($PageInfo['internet']) || !empty($PageInfo['cab_tv']) || !empty($PageInfo['garage']) || !empty($PageInfo['sauna']) || !empty($PageInfo['pool'])){
					$arr = '';
					$ar = array();
					if(!empty($PageInfo['internet'])){
						array_push($ar,'Интернет');
					}
					if(!empty($PageInfo['cab_tv'])){
						array_push($ar,'Кабельное ТВ');
					}
					if(count($ar)>0){
						$arr = implode(', ',$ar);
					}
					$have = '<div class="row">
						<div class="cell label">На участке:</div>
						<div class="cell">'.$arr.'</div>
					</div>';
				}
				
				/*
				*  Разрешенное использование
				*/
				$type_use = '';
				if(!empty($PageInfo['type_use'])){
					$type_use = '<div class="row">
						<div class="cell label">Разрешенное использование:</div>
						<div class="cell">'.str_replace(".", ",", $_TYPES_USE[$PageInfo['type_use']]).'</div>
					</div>';
				}
				
				/*
				*  Категория ЗУ
				*/
				$category = '';
				if(!empty($PageInfo['category'])){
					$category = '<div class="row">
						<div class="cell label">Категория ЗУ:</div>
						<div class="cell">'.str_replace(".", ",", $_CATEGORY_COUNTRY[$PageInfo['category']]).'</div>
					</div>';
				}
				
				/*
				*  Право на ЗУ 
				*/
				$rights = '';
				if(!empty($PageInfo['rights'])){
					$rights = '<div class="row">
						<div class="cell label">Право на ЗУ:</div>
						<div class="cell">'.str_replace(".", ",", $_RIGHTS[$PageInfo['rights']]).'</div>
					</div>';
				}
				
				/*
				*  Кадастровый номер
				*/
				$cadastr = '';
				if(!empty($PageInfo['cadastr'])){
					$cadastr = '<div class="row">
						<div class="cell label">Кадастровый номер:</div>
						<div class="cell">'.$PageInfo['cadastr'].'</div>
					</div>';
				}
								
				$paramsBlock2 = $cadastr.$kitchen_square.$rooms_square.$ceiling_height.$floors.$bedrooms.$finishing.$full_square.$type_use.$category.$rights.$heating.$communications.$plumbing.$sewerage.$gas_supply.$power_max.$have;
			}
			
			/*
			*  Таунхаус
			*/
			if($PageInfo['type_country']==2){
				/*
				*  Недвижимость
				*/
				$nameEstate = '';
				$floors = '';
				if(!empty($PageInfo['floors'])){
					$basement = '';
					if(!empty($PageInfo['basement'])){
						$basement = ' + подвал';
					}
					$floors = '<div class="row">
						<div class="cell label">Этажей в доме:</div>
						<div class="cell">'.$PageInfo['floors'].$basement.'</div>
					</div>';
				}
				
				/*
				*  Тип дома
				*/
				$type_house = '';
				if(!empty($PageInfo['type_house'])){
					$type_house = '<div class="row">
						<div class="cell label">Тип дома:</div>
						<div class="cell">'.$_TYPE_HOUSE_COUNTRY[$PageInfo['type_house']].'</div>
					</div>';
				}
				
				/*
				*  Год постройки дома
				*/
				$year = '';
				if(!empty($PageInfo['year'])){
					$year = '<div class="row">
						<div class="cell label">Год постройки дома:</div>
						<div class="cell">'.$PageInfo['year'].'</div>
					</div>';
				}

				$paramsBlock = $nameEstate.$type_house.$year;
				
				/*
				*  Площадь дома
				*/
				$full_square = '';
				if(!empty($PageInfo['full_square'])){
					$full_square = '<div class="row">
						<div class="cell label">Площадь дома:</div>
						<div class="cell">'.$PageInfo['full_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Площадь кухни
				*/
				$kitchen_square = '';
				if(!empty($PageInfo['kitchen_square'])){
					$kitchen_square = '<div class="row">
						<div class="cell label">Площадь кухни:</div>
						<div class="cell">'.$PageInfo['kitchen_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Количество спален
				*/
				$bedrooms = '';
				if(!empty($PageInfo['bedrooms'])){
					$bedrooms = '<div class="row">
						<div class="cell label">Количество спален:</div>
						<div class="cell">'.$PageInfo['bedrooms'].'</div>
					</div>';
				}
				
				/*
				*  Высота потолков
				*/
				$ceiling_height = '';
				if(!empty($PageInfo['ceiling_height'])){
					$ceiling_height = '<div class="row">
						<div class="cell label">Высота потолков:</div>
						<div class="cell">'.str_replace(".", ",", $PageInfo['ceiling_height']).' м</div>
					</div>';
				}
				
				/*
				*  Ремонт
				*/
				$finishing = '';
				if(!empty($PageInfo['finishing'])){
					$finishing = '<div class="row">
						<div class="cell label">Ремонт:</div>
						<div class="cell">'.$_TYPE_FINISHING_COUNTRY[$PageInfo['finishing']].'</div>
					</div>';
				}
				
				/*
				*  Отопление
				*/
				$heating = '';
				if(!empty($PageInfo['heating'])){
					$firePlace = '';
					if(!empty($PageInfo['fireplace'])){
						$firePlace = ' + камин';
					}
					$heating = '<div class="row">
						<div class="cell label">Отопление:</div>
						<div class="cell">'.$_HEATING_COUNTRY[$PageInfo['heating']].$firePlace.'</div>
					</div>';
				}
				
				/*
				*  На участке
				*/
				$have = '';
				if(!empty($PageInfo['internet']) || !empty($PageInfo['cab_tv'])){
					$arr = '';
					$ar = array();
					if(!empty($PageInfo['internet'])){
						array_push($ar,'Интернет');
					}
					if(!empty($PageInfo['cab_tv'])){
						array_push($ar,'Кабельное ТВ');
					}
					if(count($ar)>0){
						$arr = implode(', ',$ar);
					}
					$have = '<div class="row">
						<div class="cell label">На участке:</div>
						<div class="cell">'.$arr.'</div>
					</div>';
				}
/******************/
				$arrayInfras = array();
				/*
				*  Круглосуточная охрана территории
				*/
				$security = '';
				if(!empty($PageInfo['security'])){
					$security = '<div class="row">
						<div class="cell label">Круглосуточная охрана территории:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Круглосуточная охрана территории</li>');
				}
				
				/*
				*  Ограждение территории поселка
				*/
				$fencing = '';
				if(!empty($PageInfo['fencing'])){
					$fencing = '<div class="row">
						<div class="cell label">Ограждение территории поселка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Ограждение территории поселка</li>');
				}
				
				/*
				*  Ограждение территории поселка
				*/
				$maintenance = '';
				if(!empty($PageInfo['maintenance'])){
					$maintenance = '<div class="row">
						<div class="cell label">Служба эксплуатации в поселка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Служба эксплуатации в поселка</li>');
				}
				
				/*
				*  Кафе
				*/
				$cafe = '';
				if(!empty($PageInfo['cafe'])){
					$cafe = '<div class="row">
						<div class="cell label">Кафе:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Кафе</li>');
				}
				
				/*
				*  Магазин
				*/
				$shop = '';
				if(!empty($PageInfo['shop'])){
					$shop = '<div class="row">
						<div class="cell label">Магазин:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Магазин</li>');
				}
				
				/*
				*  Детская площадка
				*/
				$playground = '';
				if(!empty($PageInfo['playground'])){
					$playground = '<div class="row">
						<div class="cell label">Детская площадка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Детская площадка</li>');
				}
				
				/*
				*  Спортивная площадка
				*/
				$sport_playground = '';
				if(!empty($PageInfo['sport_playground'])){
					$sport_playground = '<div class="row">
						<div class="cell label">Спортивная площадка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Спортивная площадка</li>');
				}
				
				/*
				*  Медпункт
				*/
				$first_post = '';
				if(!empty($PageInfo['first_post'])){
					$first_post = '<div class="row">
						<div class="cell label">Медпункт:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Медпункт</li>');
				}
				
				/*
				*  Гостевая парковка
				*/
				$guest_parking = '';
				if(!empty($PageInfo['guest_parking'])){
					$guest_parking = '<div class="row">
						<div class="cell label">Гостевая парковка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Гостевая парковка</li>');
				}
				
				/*
				*  Водоём
				*/
				$water = '';
				if(!empty($PageInfo['water'])){
					$water = '<div class="row">
						<div class="cell label">Водоём:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Водоём</li>');
				}
				
				/*
				*  Поликлиника
				*/
				$polyclinic = '';
				if(!empty($PageInfo['polyclinic'])){
					$polyclinic = '<div class="row">
						<div class="cell label">Поликлиника:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Поликлиника</li>');
				}
				
				/*
				*  Пирс
				*/
				$pier = '';
				if(!empty($PageInfo['pier'])){
					$pier = '<div class="row">
						<div class="cell label">Пирс:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Пирс</li>');
				}
				
				/*
				*  Школа
				*/
				$school = '';
				if(!empty($PageInfo['school'])){
					$school = '<div class="row">
						<div class="cell label">Школа:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Школа</li>');
				}
				
				/*
				*  Рядом лес
				*/
				$forest = '';
				if(!empty($PageInfo['forest'])){
					$forest = '<div class="row">
						<div class="cell label">Рядом лес:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Рядом лес</li>');
				}
				
				/*
				*  Детский сад
				*/
				$kindergarten = '';
				if(!empty($PageInfo['kindergarten'])){
					$kindergarten = '<div class="row">
						<div class="cell label">Детский сад:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Детский сад</li>');
				}
/******************/
				
				/*
				*  Площадь земельного участка
				*/
				$area_square = '';
				if(!empty($PageInfo['area_square'])){
					$area_square = '<div class="row">
						<div class="cell label">Площадь земельного участка:</div>
						<div class="cell">'.str_replace(".", ",", $PageInfo['area_square']).' соток</div>
					</div>';
				}
				
				/*
				*  Разрешенное использование
				*/
				$type_use = '';
				if(!empty($PageInfo['type_use'])){
					$type_use = '<div class="row">
						<div class="cell label">Разрешенное использование:</div>
						<div class="cell">'.str_replace(".", ",", $_TYPES_USE[$PageInfo['type_use']]).'</div>
					</div>';
				}
				
				/*
				*  Категория ЗУ
				*/
				$category = '';
				if(!empty($PageInfo['category'])){
					$category = '<div class="row">
						<div class="cell label">Категория ЗУ:</div>
						<div class="cell">'.str_replace(".", ",", $_CATEGORY_COUNTRY[$PageInfo['category']]).'</div>
					</div>';
				}
				
				/*
				*  Право на ЗУ
				*/
				$rights = '';
				if(!empty($PageInfo['rights'])){
					$rights = '<div class="row">
						<div class="cell label">Право на ЗУ:</div>
						<div class="cell">'.str_replace(".", ",", $_RIGHTS[$PageInfo['rights']]).'</div>
					</div>';
				}
				
				/*
				*  Кадастровый номер
				*/
				$cadastr = '';
				if(!empty($PageInfo['cadastr'])){
					$cadastr = '<div class="row">
						<div class="cell label">Кадастровый номер:</div>
						<div class="cell">'.$PageInfo['cadastr'].'</div>
					</div>';
				}
				
				/*
				*  Застройщик
				*/
				$developer = '';
				if(!empty($PageInfo['developer'])){
					$developer = '<div class="row">
						<div class="cell label">Застройщик:</div>
						<div class="cell">'.$PageInfo['developer'].'</div>
					</div>';
				}
				
				/*
				*  Площадь посёлка
				*/
				$area_village = '';
				if(!empty($PageInfo['area_village'])){
					$area_village = '<div class="row">
						<div class="cell label">Площадь посёлка:</div>
						<div class="cell">'.$PageInfo['area_village'].'</div>
					</div>';
				}
				
				/*
				*  Количество участков
				*/
				$count_plots = '';
				if(!empty($PageInfo['count_plots'])){
					$count_plots = '<div class="row">
						<div class="cell label">Количество участков:</div>
						<div class="cell">'.$PageInfo['count_plots'].'</div>
					</div>';
				}
								
				$paramsBlock2 = $area_village.$developer.$count_plots.$floors.$full_square.$bedrooms.$ceiling_height.$finishing.$heating.$area_square.$type_use.$category.$rights.$have;
			}
			
			/*
			*  Дом
			*/
			if($PageInfo['type_country']==3){
				/*
				*  Название посёлка
				*/
				$name_villiage = '';
				if(!empty($PageInfo['show_build'])){
					if(!empty($PageInfo['name_villiage'])){
						$name_villiage = '<div class="row">
							<div class="cell label">Название посёлка:</div>
							<div class="cell">'.$PageInfo['name_villiage'].'</div>
						</div>';
					}
				}
				
				/*
				*  Площадь посёлка
				*/
				$area_village = '';
				if(!empty($PageInfo['area_village'])){
					$area_village = '<div class="row">
						<div class="cell label">Площадь посёлка:</div>
						<div class="cell">'.$PageInfo['area_village'].'</div>
					</div>';
				}
				
				/*
				*  Количество участков
				*/
				$count_plots = '';
				if(!empty($PageInfo['count_plots'])){
					$count_plots = '<div class="row">
						<div class="cell label">Количество участков:</div>
						<div class="cell">'.$PageInfo['count_plots'].'</div>
					</div>';
				}
				
				/*
				*  Тип дома
				*/
				$type_house = '';
				if(!empty($PageInfo['type_house'])){
					$type_house = '<div class="row">
						<div class="cell label">Тип дома:</div>
						<div class="cell">'.$_TYPE_HOUSE_COUNTRY[$PageInfo['type_house']].'</div>
					</div>';
				}
				
				/*
				*  Год постройки дома
				*/
				$year = '';
				if(!empty($PageInfo['year'])){
					$year = '<div class="row">
						<div class="cell label">Год постройки дома:</div>
						<div class="cell">'.$PageInfo['year'].'</div>
					</div>';
				}
				 
				$paramsBlock = $name_villiage.$area_village.$count_plots.$type_house.$year;
				
				/*
				*  Площадь дома
				*/
				$full_square = '';
				if(!empty($PageInfo['full_square'])){
					$full_square = '<div class="row">
						<div class="cell label">Площадь дома:</div>
						<div class="cell">'.$PageInfo['full_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Площадь кухни
				*/
				$kitchen_square = '';
				if(!empty($PageInfo['kitchen_square'])){
					$kitchen_square = '<div class="row">
						<div class="cell label">Площадь кухни:</div>
						<div class="cell">'.$PageInfo['kitchen_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Количество спален
				*/
				$bedrooms = '';
				if(!empty($PageInfo['bedrooms'])){
					$bedrooms = '<div class="row">
						<div class="cell label">Количество спален:</div>
						<div class="cell">'.$PageInfo['bedrooms'].'</div>
					</div>';
				}
				
				/*
				*  Высота потолков
				*/
				$ceiling_height = '';
				if(!empty($PageInfo['ceiling_height'])){
					$ceiling_height = '<div class="row">
						<div class="cell label">Высота потолков:</div>
						<div class="cell">'.str_replace(".", ",", $PageInfo['ceiling_height']).' м</div>
					</div>';
				}
				
				/*
				*  Парковка
				*/
				$parking_value = '';
				if(!empty($PageInfo['parking_value'])){
					$parking_value = '<div class="row">
						<div class="cell label">Парковка:</div>
						<div class="cell">'.$_PARKING[$PageInfo['parking_value']].'</div>
					</div>';
				}
				
				/*
				*  Банкоматы
				*/
				$atms = '';
				if(!empty($PageInfo['atms'])){
					$atms = '<div class="row">
						<div class="cell label">Банкоматы:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
/******************/
				$arrayInfras = array();
				/*
				*  Круглосуточная охрана территории
				*/
				$security = '';
				if(!empty($PageInfo['security'])){
					$security = '<div class="row">
						<div class="cell label">Круглосуточная охрана территории:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Круглосуточная охрана территории</li>');
				}
				
				/*
				*  Ограждение территории поселка
				*/
				$fencing = '';
				if(!empty($PageInfo['fencing'])){
					$fencing = '<div class="row">
						<div class="cell label">Ограждение территории поселка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Ограждение территории поселка</li>');
				}
				
				/*
				*  Ограждение территории поселка
				*/
				$maintenance = '';
				if(!empty($PageInfo['maintenance'])){
					$maintenance = '<div class="row">
						<div class="cell label">Служба эксплуатации в поселка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Служба эксплуатации в поселка</li>');
				}
				
				/*
				*  Кафе
				*/
				$cafe = '';
				if(!empty($PageInfo['cafe'])){
					$cafe = '<div class="row">
						<div class="cell label">Кафе:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Кафе</li>');
				}
				
				/*
				*  Магазин
				*/
				$shop = '';
				if(!empty($PageInfo['shop'])){
					$shop = '<div class="row">
						<div class="cell label">Магазин:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Магазин</li>');
				}
				
				/*
				*  Детская площадка
				*/
				$playground = '';
				if(!empty($PageInfo['playground'])){
					$playground = '<div class="row">
						<div class="cell label">Детская площадка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Детская площадка</li>');
				}
				
				/*
				*  Спортивная площадка
				*/
				$sport_playground = '';
				if(!empty($PageInfo['sport_playground'])){
					$sport_playground = '<div class="row">
						<div class="cell label">Спортивная площадка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Спортивная площадка</li>');
				}
				
				/*
				*  Медпункт
				*/
				$first_post = '';
				if(!empty($PageInfo['first_post'])){
					$first_post = '<div class="row">
						<div class="cell label">Медпункт:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Медпункт</li>');
				}
				
				/*
				*  Гостевая парковка
				*/
				$guest_parking = '';
				if(!empty($PageInfo['guest_parking'])){
					$guest_parking = '<div class="row">
						<div class="cell label">Гостевая парковка:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Гостевая парковка</li>');
				}
				
				/*
				*  Водоём
				*/
				$water = '';
				if(!empty($PageInfo['water'])){
					$water = '<div class="row">
						<div class="cell label">Водоём:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Водоём</li>');
				}
				
				/*
				*  Поликлиника
				*/
				$polyclinic = '';
				if(!empty($PageInfo['polyclinic'])){
					$polyclinic = '<div class="row">
						<div class="cell label">Поликлиника:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Поликлиника</li>');
				}
				
				/*
				*  Пирс
				*/
				$pier = '';
				if(!empty($PageInfo['pier'])){
					$pier = '<div class="row">
						<div class="cell label">Пирс:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Пирс</li>');
				}
				
				/*
				*  Школа
				*/
				$school = '';
				if(!empty($PageInfo['school'])){
					$school = '<div class="row">
						<div class="cell label">Школа:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Школа</li>');
				}
				
				/*
				*  Рядом лес
				*/
				$forest = '';
				if(!empty($PageInfo['forest'])){
					$forest = '<div class="row">
						<div class="cell label">Рядом лес:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Рядом лес</li>');
				}
				
				/*
				*  Детский сад
				*/
				$kindergarten = '';
				if(!empty($PageInfo['kindergarten'])){
					$kindergarten = '<div class="row">
						<div class="cell label">Детский сад:</div>
						<div class="cell">Да</div>
					</div>';
					array_push($arrayInfras,'<li>Детский сад</li>');
				}
/******************/

				/*
				*  Ремонт
				*/
				$finishing = '';
				if(!empty($PageInfo['finishing'])){
					$finishing = '<div class="row">
						<div class="cell label">Ремонт:</div>
						<div class="cell">'.$_TYPE_FINISHING_COUNTRY[$PageInfo['finishing']].'</div>
					</div>';
				}
				
				/*
				*  Гараж
				*/
				$garage = '';
				if(!empty($PageInfo['garage'])){
					$garage = '<div class="row">
						<div class="cell label">Гараж:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Баня
				*/
				$sauna = '';
				if(!empty($PageInfo['sauna'])){
					$sauna = '<div class="row">
						<div class="cell label">Баня:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Бассейн
				*/
				$pool = '';
				if(!empty($PageInfo['pool'])){
					$pool = '<div class="row">
						<div class="cell label">Бассейн:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Электричество
				*/
				$electricity = '';
				if(!empty($PageInfo['electricity'])){
					$electricity = '<div class="row">
						<div class="cell label">Электричество:</div>
						<div class="cell">'.$PageInfo['electricity'].'</div>
					</div>';
				}
				
				/*
				*  Коммуникации
				*/
				$communications = '';
				if(!empty($PageInfo['communications'])){
					$communications = '<div class="row">
						<div class="cell label">Коммуникации:</div>
						<div class="cell">'.$_COMMUNICATIONS[$PageInfo['communications']].'</div>
					</div>';
				}
				
				/*
				*  Водоснабжение
				*/
				$plumbing = '';
				if(!empty($PageInfo['plumbing'])){
					$plumbing = '<div class="row">
						<div class="cell label">Водоснабжение:</div>
						<div class="cell">'.$_PLUMBING[$PageInfo['plumbing']].'</div>
					</div>';
				}
				
				/*
				*  Газоснабжение
				*/
				$gas_supply = '';
				if(!empty($PageInfo['gas_supply'])){
					$gas_supply = '<div class="row">
						<div class="cell label">Газоснабжение:</div>
						<div class="cell">'.$_GASSUPPLY[$PageInfo['gas_supply']].'</div>
					</div>';
				}
				
				/*
				*  Канализиция
				*/
				$sewerage = '';
				if(!empty($PageInfo['sewerage'])){
					$sewerage = '<div class="row">
						<div class="cell label">Канализация:</div>
						<div class="cell">'.$_SEWERAGE[$PageInfo['sewerage']].'</div>
					</div>';
				}
				
				/*
				*  Электричество
				*/
				$power_max = '';
				if(!empty($PageInfo['power_max'])){
					$power_max = '<div class="row">
						<div class="cell label">Электричество:</div>
						<div class="cell">'.$PageInfo['power_max'].' кВт</div>
					</div>';
				}
				/*
				*  Отопление
				*/
				$heating = '';
				$heatingArray = array();
				if(!empty($PageInfo['heating'])){
					array_push($heatingArray,$_HEATING_COUNTRY[$PageInfo['heating']]);
				}
				if(!empty($PageInfo['fireplace'])){
					array_push($heatingArray,'+ камин');
				}
				if(count($heatingArray)>0){
					$heating = '<div class="row">
						<div class="cell label">Отопление:</div>
						<div class="cell">'.implode(' ',$heatingArray).'</div>
					</div>';
				}
				
				/*
				*  Площадь земельного участка
				*/
				$area_square = '';
				if(!empty($PageInfo['area_square'])){
					$area_square = '<div class="row">
						<div class="cell label">Площадь земельного участка:</div>
						<div class="cell">'.str_replace(".", ",", $PageInfo['area_square']).' соток</div>
					</div>';
				}
				
				/*
				*  Разрешенное использование
				*/
				$type_use = '';
				if(!empty($PageInfo['type_use'])){
					$type_use = '<div class="row">
						<div class="cell label">Разрешенное использование:</div>
						<div class="cell">'.str_replace(".", ",", $_TYPES_USE[$PageInfo['type_use']]).'</div>
					</div>';
				}
				
				/*
				*  Категория ЗУ
				*/
				$category = '';
				if(!empty($PageInfo['category'])){
					$category = '<div class="row">
						<div class="cell label">Категория ЗУ:</div>
						<div class="cell">'.str_replace(".", ",", $_CATEGORY_COUNTRY[$PageInfo['category']]).'</div>
					</div>';
				}
				
				/*
				*  Право на ЗУ
				*/
				$rights = '';
				if(!empty($PageInfo['rights'])){
					$rights = '<div class="row">
						<div class="cell label">Право на ЗУ:</div>
						<div class="cell">'.str_replace(".", ",", $_RIGHTS[$PageInfo['rights']]).'</div>
					</div>';
				}
				
				/*
				*  На участке
				*/
				$have = '';
				if(!empty($PageInfo['internet']) || !empty($PageInfo['cab_tv']) || !empty($PageInfo['garage']) || !empty($PageInfo['sauna']) || !empty($PageInfo['pool'])){
					$arr = '';
					$ar = array();
					if(!empty($PageInfo['internet'])){
						array_push($ar,'Интернет');
					}
					if(!empty($PageInfo['cab_tv'])){
						array_push($ar,'Кабельное ТВ');
					}
					if(!empty($PageInfo['garage'])){
						array_push($ar,'Гараж');
					}
					if(!empty($PageInfo['sauna'])){
						array_push($ar,'Баня');
					}
					if(!empty($PageInfo['pool'])){
						array_push($ar,'Бассейн');
					}
					if(count($ar)>0){
						$arr = implode(', ',$ar);
					}
					$have = '<div class="row">
						<div class="cell label">На участке:</div>
						<div class="cell">'.$arr.'</div>
					</div>';
				}
				
				/*
				*  Кадастровый номер
				*/
				$cadastr = '';
				if(!empty($PageInfo['cadastr'])){
					$cadastr = '<div class="row">
						<div class="cell label">Кадастровый номер:</div>
						<div class="cell">'.$PageInfo['cadastr'].'</div>
					</div>';
				}
				
				$paramsBlock2 = $floors.$full_square.$kitchen_square.$area_square.$type_use.$rights.$category.$bedrooms.$ceiling_height.$finishing.$cadastr.$electricity.$communications.$plumbing.$sewerage.$heating.$gas_supply.$power_max.$have;
			}

			
			$idUser = 0;
			$textFavorite = 'В избранное';
			$classFavorite = '';
			if($_SESSION['idAuto']){
				$idUser = $_SESSION['idAuto'];
				$res = mysql_query("
					SELECT COUNT(*) AS count
					FROM ".$template."_favorites
					WHERE estate=5 && user_id=".$idUser." && float_id=".$PageInfo['id']." && activation=1
				") or die(mysql_error());
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					if($row['count']){
						$textFavorite = 'В избранном';
						$classFavorite = ' at';
					}
				}
			}
			if(!empty($PageInfo['floors'])){
				if($PageInfo['type_country']==2){
					$nameEstate = ' '.$PageInfo['floors'].'-этажного таунхауса';
				}
			}
			$type_countrycials = '<div class="cell">'.$_TYPE_COUNTRY_ESTATE[$PageInfo['type_country']].$nameEstate.'</div>';
		?>
		<div class="card_info">
			<div class="info_housing">
				<div class="row first">
					<div class="cell label">ID: <?=$PageInfo['id']?></div>
					<div class="cell"><a class="add_favorite<?=$classFavorite?>" onclick="return addFavorite(this)" href="javascript:void(0)"><span><?=$textFavorite?></span></a></div>
				</div>
				<div class="row">
					<div class="cell label">Загородная недвижимость:</div>
					<?=$type_countrycials?>
				</div>
				<?=$mayBeUseText?>
				<?=$paramsBlock?>
			</div>
			<div class="info_housing">
				<?
				echo $paramsBlock2;
				?>
			</div>
			<?
			$arrayBuy = array();
			if(isset($PageInfo['mortgages']) && !empty($PageInfo['mortgages'])){
				array_push($arrayBuy,'<li>Ипотека</li>');
			}
			if(isset($PageInfo['subsidies']) && !empty($PageInfo['subsidies'])){
				array_push($arrayBuy,'<li>Субсидии</li>');
			}
			if(isset($PageInfo['installment']) && !empty($PageInfo['installment'])){
				array_push($arrayBuy,'<li>Рассрочка</li>');
			}
			if(count($arrayBuy)>0){
				echo '<div class="ways_buy">
					<ul>
						'.implode('',$arrayBuy).'
					</ul>
				</div>';
				
			}

			$linkThisPage = urlencode('http://'.$_SERVER['HTTP_HOST'].'/'.$_SERVER['REQUEST_URI']);
			?>
			
			<div class="share_block">
				<ul>
					<li>Поделиться:</li>
					<li><a onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="http://vk.com/share.php?url=<?=$linkThisPage?>"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
					<li><a onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://www.facebook.com/sharer.php?src=sp&u=<?=$linkThisPage?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://plus.google.com/share?url=<?=$linkThisPage?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
					<li><a onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://twitter.com/intent/tweet?url=<?=$linkThisPage?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl=<?=$linkThisPage?>"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="clear"></div>
		<div class="text_info">
			<?
				if(!empty($PageInfo['short_text'])){
					echo '<div class="row add">
						<div class="cell label">Дополнительная информация:</div>
						<div class="cell desc">'.nl2br(htmlspecialchars_decode($PageInfo['short_text'])).'</div>
					</div>';
				}
				else {
					if(!empty($PageInfo['text'])){
						echo '<div class="row add">
							<div class="cell label">Дополнительная информация:</div>
							<div class="cell desc">'.nl2br(strip_tags(htmlspecialchars_decode($PageInfo['text']))).'</div>
						</div>';
					}
				}
				if(count($arrayInfras)>0){
					echo '<div class="infras_list">';
					echo '<ul>'.implode('',$arrayInfras).'</ul>';
					echo '</div>';
				}
			?>
		</div>
		<!--<div class="more_info">
			<div class="near_objects">
				<div class="infrastructure_block">
					<div class="title">Инфраструктура</div>
					<ul>
						<li>
							<div class="item"><span class="school icon"></span>Школа имени Римского-Корсакого</div>
						</li>
						<li>
							<div class="item"><span class="kindergarten icon"></span>Детский сад №56</div>
						</li>
						<li>
							<div class="item"><span class="clinic icon"></span>Поликлиника им. Св. Марии</div>
						</li>
					</ul>
				</div>
				<div class="safety_block">
					<div class="title">Безопасность</div>
					<ul>
						<li>Огороженный периметр</li>
						<li>Видеонаблюдение</li>
						<li>Пропускная система</li>
						<li>Консьерж</li>
						<li>Охраняемая парковка</li>
						<li>Сигнализация</li>
					</ul>
				</div>
			</div>
			<div class="other_offers_flats">
				<div class="list_flats">
					<div class="row">
						<div class="name">Студия. от <strong>27,5 м<sup>2</sup></strong></div>
						<div class="price">от <strong><a href="#">995 000 руб.</a></strong></div>
					</div>
					<div class="row fone">
						<div class="name">1-комн. от <strong>36,4 м<sup>2</sup></strong></div>
						<div class="price">от <strong><a href="#">1 638 000 руб.</a></strong></div>
					</div>
					<div class="row">
						<div class="name">2-комн. от <strong>44,7 м<sup>2</sup></strong></div>
						<div class="price">от <strong><a href="#">1 922 100 руб.</a></strong></div>
					</div>
				</div>
			</div>
		</div>-->
		<div class="bottom_block">
			<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
			<style>#map {width:650px;height:780px;padding:0;margin:0}</style>
			<?
			// echo '<pre>';
			// print_r($PageInfo);
			// echo '</pre>';
			$coordsYandex = '';
			$ex_coords = array('30.315868','59.939095');
			if(empty($PageInfo['coords'])){
				$params2 = array(
					'geocode' => 'Санкт-Петербург, '.$PageInfo['address'],
					'format'  => 'json',
					'results' => 1,
					// 'key'     => 'AKDIIFYBAAAAqIYtRgIAZYb8P32hIxnbRRSe9CpggONv4PEAAAAAAAAAAADu6wMZJIEJ0SDd0mRe33ag1JRdNA==',
				);
				$response = json_decode(@file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params2)));
				
				if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0){
					$coordsYandex = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
				}
			}
			else {
				$coordsYandex = $PageInfo['coords'];
				$ex_coords = explode(',',$coordsYandex);
				if(count($ex_coords)>1){
					$coordsYandex = $ex_coords[1].' '.$ex_coords[0];
				}
			}
			$coordsYandex = trim($coordsYandex);
			if(!empty($coordsYandex)){
				$coordsYandex = str_replace(',',' ',$coordsYandex);
				$ex_coords = explode(' ',$coordsYandex);
				$ex_coords = array($ex_coords[1],$ex_coords[0]);
			}
			
			?>
			
			
			<?if(empty($coordsYandex)){?>
				<script>
					ymaps.ready(function () {
						var myMap = new ymaps.Map('map', {
							center: [<?=$ex_coords[1]?>,<?=$ex_coords[0]?>],
							zoom: 13
						}, {
							searchControlProvider: 'yandex#search'
						}),
						myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
							hintContent: 'Метка на карте',
							balloonContent: 'г. <?=$PageInfo['city_name']?>, <?=$PageInfo['address']?>'
						}, {
							// Опции.
							// Необходимо указать данный тип макета.
							iconLayout: 'default#image',
							// Своё изображение иконки метки.
							iconImageHref: '/images/neitralIcon.png',
							// Размеры метки.
							iconImageSize: [52, 44],
							// Смещение левого верхнего угла иконки относительно
							// её "ножки" (точки привязки).
							iconImageOffset: [-3, -42]
						});

						myMap.geoObjects.add(myPlacemark);
						
/* 						var address = "г. <?=$PageInfo['city_name']?>, <?=$PageInfo['address']?>";
						ymaps.geocode(address,{kind:'metro'}).then(function(res){
							var m0 = res.geoObjects.get(0);
							var m0_coords = m0.geometry.getCoordinates();
							var addressName = m0.properties.get('name');
						
							ymaps.geocode(m0_coords,{kind:'metro'}).then(function(res){
								var m0 = res.geoObjects.get(0);
								var m0_coords = m0.geometry.getCoordinates();
								var name = m0.properties.get('name');
								<?if(!empty($PageInfo['station_name'])){?>
									var myRouter = ymaps.route([
									  "г. <?=$PageInfo['city_name']?>, <?=$PageInfo['station_name']?>", {
										type: "viaPoint",                   
										point: addressName
									  },
									], {
									  mapStateAutoApply: true 
									});
								<?}else{?>
									var myRouter = ymaps.route([
									  name, {
										type: "viaPoint",                   
										point: addressName
									  },
									], {
									  mapStateAutoApply: true 
									});
								<?}?>
								
								myRouter.then(function(route) {
									var points = route.getWayPoints();
									points.options.set('preset', 'islands#blackStretchyIcon');
									points.get(0).properties.set("iconContent", 'Санкт-Петербург, м. <?=$PageInfo['station_name']?>');
									points.get(1).properties.set("iconContent", '<?=htmlspecialchars_decode($PageInfo['name'])?>');
									// Добавление маршрута на карту
									myMap.geoObjects.add(route);
									var routeLength = route.getHumanLength().toString();
									var routeLength2 = route.getLength();
									// $('<div class="full_route">Расстояние от <span>Санкт-Петербург, м. <?=$PageInfo['station_name']?></span> до <span><?=htmlspecialchars_decode($PageInfo['address'])?></span>: <strong>'+routeLength+'</strong></div>').insertAfter('#map');
									var parent = arrLinkPage[1];
									var id = arrLinkPage[2];
									var data = "buildDist="+routeLength2+"&parent="+parent+"&id="+id;
									xhr = $.ajax({
										type: "POST",
										url: '/include/handler.php',
										data: data+"&rnd="+Math.random(),
										dataType: "json",
										success: function (data, textStatus) {
											var json = eval(data);
											if(json.action!='error'){
												// window.location = window.location;
											}
											else {
												alert('Возникла системная ошибка! Данные не сохранены. Попробуйте позднее.');
											}
										}
									});
								  },
								  // Обработка ошибки
								  function (error) {
									// alert("Возникла ошибка: " + error.message);
								  }
								)
							});
						});
 */							
					});
				</script>
			<?}else{?>
				<script>
				
					ymaps.ready(function(){
						var myMap = new ymaps.Map('map', {
								center: [<?=$ex_coords[0]?>,<?=$ex_coords[1]?>],
								zoom: 13
							}, {
								searchControlProvider: 'yandex#search'
							}),
							myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
								hintContent: '<?=$PageInfo['name']?>',
								balloonContent: '<?=$PageInfo['address']?>'
							}, {
								// Опции.
								// Необходимо указать данный тип макета.
								iconLayout: 'default#image',
								// Своё изображение иконки метки.
								iconImageHref: '/images/neitralIcon.png',
								// Размеры метки.
								iconImageSize: [52, 44],
								// Смещение левого верхнего угла иконки относительно
								// её "ножки" (точки привязки).
								iconImageOffset: [-3, -42]
							});

						myMap.geoObjects.add(myPlacemark);
						// myMap.behaviors.disable('scrollZoom');
						
						var myRouter = ymaps.route([
						  'Санкт-Петербург, м. <?=$PageInfo['station_name']?>', {
							type: "viaPoint",                   
							point: [<?=$ex_coords[1]?>,<?=$ex_coords[0]?>]
						  },
						], {
						  mapStateAutoApply: true 
						});
						
						/* myRouter.then(function(route) {
							// Задание контента меток в начальной и конечной точках
							var points = route.getWayPoints();
							points.options.set('preset', 'islands#blackStretchyIcon');
							points.get(0).properties.set("iconContent", 'Санкт-Петербург, м. <?=$PageInfo['station_name']?>');
							points.get(1).properties.set("iconContent", '<?=htmlspecialchars_decode($PageInfo['name'])?>');
							// Добавление маршрута на карту
							myMap.geoObjects.add(route);
							var routeLength = route.getHumanLength();
							var routeLength2 = route.getLength();
							// $('<div class="full_route">Расстояние от <span>Санкт-Петербург, м. <?=$PageInfo['station_name']?></span> до <span><?=htmlspecialchars_decode($PageInfo['name'])?></span>: <strong>'+routeLength+'</strong></div>').insertAfter('#map');
							var parent = arrLinkPage[1];
							var id = arrLinkPage[2];
							var data = "buildDist="+routeLength2+"&parent="+parent+"&id="+id;
							xhr = $.ajax({
								type: "POST",
								url: '/include/handler.php',
								data: data+"&rnd="+Math.random(),
								dataType: "json",
								success: function (data, textStatus) {
									var json = eval(data);
									if(json.action!='error'){
										// window.location = window.location;
									}
									else {
										alert('Возникла системная ошибка! Данные не сохранены. Попробуйте позднее.');
									}
								}
							});
							// alert(routeLength2);
						  },
						  // Обработка ошибки
						  function (error) {
							// alert("Возникла ошибка: " + error.message);
						  }
						) */
						// var myGeocoder = ymaps.geocode(<?=$ex_coords[1]?>,<?=$ex_coords[0]?>, {kind: 'metro'}).then(function(res) {var nearest = res.geoObjects.get(0); name = nearest.properties.get('name');});

					});
				</script>
			<?}
			$verified = '<i class="verified"></i>';
			$verified = '';
			$userBlock = '';
			$styleForm = ' style="display:block;text-align:center;width:100%"';
			if(!empty($PageInfo['user_id'])){
				$userBlock = '<span class="id_block">ID: '.$PageInfo['user_id'].$verified.'</span>';
				$styleForm = '';
			}
			?>
			<div class="map"><div id="map"></div></div>
			<div class="feedback_block">
				<div class="representative">
					<div class="user_info">
						<span<?=$styleForm?> class="form">Представитель</span>
						<?=$userBlock?>
					</div>
					<div class="show_phone">
						<div class="btn_save top">
							<label class="btn"><input onclick="return show_phone(this,<?=$PageInfo['id']?>)" type="button" value="Показать ТЕЛЕФОН"><span class="angle-right"></span></label>
						</div>
					</div>
				</div>
				
				<div class="feedback_form">
					<form action="/include/handler.php" method="post" onsubmit="return checkSideForm(this)">
						<div class="send_request">
							<input type="hidden" name="type_form" value="application">
							<div class="title">Запросить информацию по объекту</div>
							<div class="table">
								<div class="row">
									<div class="cell">
										<div class="label">Ваше имя<b>*</b></div>
									</div>
									<div class="cell">
										<div class="input_text required">
											<input type="text" name="s[name]">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="cell">
										<div class="label">Ваш телефон<b>*</b></div>
									</div>
									<div class="cell">
										<div class="input_text required">
											<input class="phone" type="text" name="s[phone]">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="cell">
										<div class="label">Ваш e-mail<b>*</b></div>
									</div>
									<div class="cell">
										<div class="input_text required">
											<input type="text" name="s[email]">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="cell">
										<div class="label">Сообщение</div>
									</div>
									<div class="cell">
										<div class="input_text required"><textarea placeholder="Меня интересует этот объект недвижимости, пожалуйста, свяжитесь со мной..." name="s[msg]"></textarea></div>
									</div>
								</div>
								<div class="row">
									<div class="cell">
										<div class="label">&nbsp;</div>
									</div>
									<div class="cell" style="margin-right:-35px">
										<script src='https://www.google.com/recaptcha/api.js'></script>
										<div style="transform:scale(0.9);-webkit-transform:scale(0.9);transform-origin:0 0;-webkit-transform-origin:0 0;" class="g-recaptcha" data-sitekey="6Lc25CkUAAAAAGSUoAgMgNQkIdiYVivlI5xP0cPI"></div>
									</div>
								</div>
								<div class="row">
									<div class="cell">
										<div class="label">&nbsp;</div>
									</div>
									<div class="cell">
										<div class="checkbox_single">
											<input disabled="disabled" type="hidden" name="agree">
											<div class="container">
												<span class="check"></span><span class="label">Нажимая кнопку «Отправить» я выражаю согласие на правила обработки персональных данных согласно <a href="/user-agreement" target="_blank">"Политике защиты персональной информации пользователей сайта"</a></span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="btn">
								<label class="btn disabled"><input disabled="disabled" type="submit" value="Отправить"><span class="angle-right"></span></label>
							</div>
							<div class="table">
								<div class="row">
									<div class="cell full comment">
										<div class="req"><b>*</b> - поля обязательные для заполнения</div>
									</div>
								</div>
							</div>
							
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<?
		$res = mysql_query("
			SELECT *
			FROM ".$template."_articles
			WHERE activation=1
			ORDER BY date DESC,id DESC
			LIMIT 10
		");
		if(mysql_num_rows($res)>0){
			echo '<div class="news_events">';
			echo '<h3>Новости и статьи</h3>';
			echo '<div id="owl-demo">';
			while($row=mysql_fetch_assoc($res)){
				$images = '';
				$link = '/articles/'.$row['link'];
				if(!empty($row['images'])){
					$ex_avatar = explode(',',$row['images']);
					$images = '<div class="avatar"><img width="125" src="/admin_2/uploads/'.$ex_avatar[1].'"></div>';
				}
				echo '<div class="item">
					<div class="date">'.date_rus($row['date']).'</div>
					<div class="title"><a target="_blank" href="'.$link.'"><span>'.$row['name'].'</span></a></div>
				</div>';
			}
			echo '</div>';
			echo '</div><script src="/libs/owl-carousel/js/owl.carousel.js?ver=<'.time().'"></script>';
		}
		?>
		</div>
		
		<!--<div id="comments_block">
			<h3>Комментарии пользователей</h3>
			<div class="container_block">
				<div class="scrollbar-inner">
					<ul>
						<li>
							<div class="image"><a href="#"><img src="/images/avatar.jpg"></a></div>
							<div class="body">
								<div class="title">
									<div class="info">
										<a href="#">Вера Пчёлкина</a><strong>19:35</strong> | <span>25 Окт. 2016</span>
									</div>
									<div class="id_comment">#24590</div>
								</div>
								<div class="message_block">
									<div class="msg">Очевидно, что марксизм формирует прагматический культ личности. Механизм власти, как бы это ни казалось парадоксальным, теоретически возможен. Авторитаризм, короче говоря, верифицирует системный бихевиоризм.</div>
									<div class="btns">
										<div class="answer"><a href="#">Ответить</a></div>
										<div class="complain"><a href="#">Пожаловаться</a></div>
									</div>
								</div>
							</div>
							<ul>
								<li>
									<div class="image"><a href="#"><img src="/images/avatar.jpg"></a></div>
									<div class="body">
										<div class="title">
											<div class="info">
												<a href="#">Вера Пчёлкина</a><strong>19:35</strong> | <span>25 Окт. 2016</span>
											</div>
											<div class="id_comment">#24590</div>
										</div>
										<div class="message_block">
											<div class="msg">Очевидно, что марксизм формирует прагматический культ личности. Механизм власти, как бы это ни казалось парадоксальным, теоретически возможен. Авторитаризм, короче говоря, верифицирует системный бихевиоризм.</div>
											<div class="btns">
												<div class="answer"><a href="#">Ответить</a></div>
												<div class="complain"><a href="#">Пожаловаться</a></div>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</li>
						<li>
							<div class="image"><a href="#"><img src="/images/avatar.jpg"></a></div>
							<div class="body">
								<div class="title">
									<div class="info">
										<a href="#">Вера Пчёлкина</a><strong>19:35</strong> | <span>25 Окт. 2016</span>
									</div>
									<div class="id_comment">#24590</div>
								</div>
								<div class="message_block">
									<div class="msg">Очевидно, что марксизм формирует прагматический культ личности. Механизм власти, как бы это ни казалось парадоксальным, теоретически возможен. Авторитаризм, короче говоря, верифицирует системный бихевиоризм.</div>
									<div class="btns">
										<div class="answer"><a href="#">Ответить</a></div>
										<div class="complain"><a href="#">Пожаловаться</a></div>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="btn_info cost">
					<div class="table_form">
						<div class="row btn_row">
							<div class="cell center full last_col">
								<label class="btn link"><a class="back_page" href="javascript:void(0)"><span>Оставить сообщение</span></a></label>
								<label class="btn"><input type="submit" value="Перейти к обсуждению темы"><span class="angle-right"></span></label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>-->
		<?
		$priceParams = " && c.price>='".($PageInfo['price']-300000)."' && c.price<='".($PageInfo['price']+300000)."'";
		if($PageInfo['PageInfo']=='rent'){
			$priceParams = " && c.price>='".($PageInfo['price']-10000)."' && c.price<='".($PageInfo['price']+10000)."'";
		}
		$priceParams = '';
		$typeCommerMain = '';
		if(!empty($PageInfo['type'])){
			$typeCommerMain = " && type='".$PageInfo['type']."'";
		}
		$res = mysql_query("
			SELECT c.*,
			(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=c.id && estate='country' LIMIT 1) AS images,
			(SELECT title FROM ".$template."_stations WHERE activation='1' && id=c.station) AS station_name
			FROM ".$template."_countries AS c
			WHERE c.activation=1 && c.type_country='".$PageInfo['type_country']."'".$priceParams." && c.id!='".$PageInfo['id']."'".$typeCommerMain." && c.location=".$PageInfo['location']."
			ORDER BY RAND()
			LIMIT 4
		") or die(mysql_error());
		if(mysql_num_rows($res)>0){
			echo '<div id="related_offers">';
			echo '<h3>Похожие предложения</h3>';
			echo '<div class="container_block">';
			

			while($row = mysql_fetch_assoc($res)){
				$image = '<span><img src="/images/no_photo_238x156.png"/></span>';
				// if(!empty($row['images'])){
					// $ex_image = explode(',',$row['images']);
					// $size = @getimagesize($_SERVER['DOCUMENTS_ROOT'].'/admin_2/uploads/'.$ex_image[3]);
					// if(!empty($size[0])){
						// $image = '<img src="/admin_2/uploads/'.$ex_image[3].'">';
					// }
				// }
				$photosList = '';
				$photos = mysql_query("
					SELECT *
					FROM ".$template."_photo_catalogue
					WHERE estate='country' && p_main='".$row['id']."'
					LIMIT 1
				");
				if(mysql_num_rows($photos)>0){
					$ph = mysql_fetch_assoc($photos);
					$ex_images = explode(',',$ph['images']);
					if($ph['user_id']!=0){
						$photosList = '/users/'.$ph['user_id'].'/'.$ex_images[3];
					}
					else {
						$photosList = '/admin_2/uploads/'.$ex_images[3];
					}
					$image = '<span class="img" style="background-image:url('.$photosList.')"></span>';
				}
				
				$linkBuild = '/countries/'.$row['p_main'];
				$link = '/countries/'.$row['id'];
				$exploitation = explode('_',$row['exploitation']);
				$deadline = '';
				if(count($exploitation)>0){
					if(count($exploitation)>1){
						$deadline = '<span>'.$exploitation[1].' кв. '.$exploitation[0].'</span>';
					}
					else {
						$deadline = '<span>'.$exploitation[0].'</span>';
					}
				}
				
				$station_name = '';
				if(!empty($row['station_id'])){
					$r = mysql_query("
						SELECT title
						FROM ".$template."_stations
						WHERE id='".$row['station_id']."' && activation=1
					");
					if(mysql_num_rows($r)>0){
						$rw = mysql_fetch_assoc($r);
						$stat_name = $rw['title'];
						$link_station = '/search?type=sell&estate=1&priceAll=all&station='.$row['station'];
						$station_name = '<div class="station"><a href="'.$link_station.'">'.$stat_name.'</a><span class="dist">'.$row['dist_value'].' км</span></div>';
					}
				}
				
				echo '<div class="item">
					<div class="title_block">
						<a href="'.$linkBuild.'">'.$row['nameBuilds'].'</a>
						<div class="info_item">
							<div class="price">'.price_cell($row['price'],0).' <span class="rub">Р</span></div>
							<!--<div class="deadline">Срок сдачи: <span class="queue">'.$deadline.'</span></div>-->
						</div>
						<div class="image">
							<a href="'.$link.'">'.$image.'</a>
						</div>
						<div class="address">'.$row['address'].'</div>
						'.$station_name.'
					</div>
				</div>';
			}
			echo '</div>';
			echo '</div>';
		}
		?>
	</div>
	</div>
</div>	
<?}
if(count($params)==3){
	if($params[2]=='housing'){
		
	}
	if($params[2]=='countries'){
		
	}

?>
<?}
// if(count($params)==4){
	// include("include/mods/flat_open_new.php");
// }
?>