<?
// Хлебные крошки (эталон)
echo '<div id="center">
	<h1 class="inner_pages">'.$titlePage.'</h1>';
	// echo '<pre>';
	// print_r($PageInfoAd);
	// echo '</pre>';
	/*
	*  Шаги страницы
	*/
	$step1 = '<li><a href="/add?id='.$idAd.'">Расположение</a></li>';
	$step2 = '<li><a href="/add2?id='.$idAd.'">Параметры</a></li>';
	$step3 = '<li><span>Стоимость</span></li>';
	$step4 = '<li><span>Условия размещения</span></li>';
	$step5 = '<li class="current"><span>Ваше объявление</span></li>';
	$btnBack = '<label class="btn link"><a class="back_page" href="/add?id='.$idAd.'"><span>Редактировать объявление</span></a></label>';
	if($PageInfoAd['all_save']){
		$step1 = '<li><a href="/add?id='.$idAd.'">Расположение</a></li>';
		$step2 = '<li><a href="/add2?id='.$idAd.'">Параметры</a></li>';
		$step3 = '<li><a href="/add3?id='.$idAd.'">Стоимость</a></li>';
		$step4 = '<li><a href="/add4?id='.$idAd.'">Условия размещения</a></li>';
		$step5 = '<li class="current"><a href="/add5?id='.$idAd.'">Ваше объявление</a></li>';
		$btnBack = '<label class="btn link"><a class="back_page" href="/add?id='.$idAd.'"><span>Редактировать объявление</span></a></label>';
	}
	
	echo '<div class="text_block">
		'.$textPage.'
	</div>';
	
	/**********/
	$type_queues = '';
	/*
	*  Новостройки
	*/
	if($PageInfoAd['estate']==1){
		$table_name = $template.'_new_flats';
		$type_queues = 'new';
		$flats = mysql_query("
			SELECT SQL_CALC_FOUND_ROWS f.*,e.distance,e.dist_value,e.name AS name_build,e.address,e.ceiling_height,e.finishing_value,
			(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main='".$PageInfoAd['float_id']."' && estate='new_flats') AS images,
			(SELECT COUNT(*) FROM ".$template."_photo_catalogue WHERE activation='1' && p_main='".$PageInfoAd['float_id']."' && estate='new_flats') AS images_count,
			(SELECT title FROM ".$template."_stations WHERE activation='1' && id=e.station) AS station_name,
			(SELECT name FROM ".$template."_developers WHERE activation='1' && id=e.developer) AS name_developer,
			(SELECT name FROM ".$template."_location WHERE activation='1' && id=e.location) AS location_name,
			(SELECT COUNT(id) FROM ".$template."_new_flats WHERE p_main='".$row['id']."' && rooms=f.rooms && queue=f.queue) AS count_flats
			FROM ".$template."_new_flats AS f
			LEFT JOIN ".$template."_m_catalogue_left AS e
			ON e.id=f.p_main && e.activation='1'
			WHERE f.id='".$PageInfoAd['float_id']."'
		") or die(mysql_error());
		$type_name = '<span class="type">Новостройка</span>';
	}
	/*
	*  Вторичка
	*/
	if($PageInfoAd['estate']==2){
		$table_name = $template.'_second_flats';
		$flats = mysql_query("
			SELECT SQL_CALC_FOUND_ROWS s.*,
			".$searchParamFlat."
			(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=s.id && estate='flats') AS images,
			(SELECT COUNT(*) FROM ".$template."_photo_catalogue WHERE activation='1' && p_main=s.id && estate='flats') AS images_count,
			(SELECT title FROM ".$template."_stations WHERE activation='1' && id=s.station) AS station_name,
			(SELECT name FROM ".$template."_location WHERE activation='1' && id=s.location) AS location_name
			FROM ".$template."_second_flats AS s
			WHERE s.id='".$PageInfoAd['float_id']."'
		") or die(mysql_error());
		$type_name = '<span class="type">Вторичка</span>';
	}
	/*
	*  Комната
	*/
	if($PageInfoAd['estate']==3){
		$table_name = $template.'_rooms';
		$flats = mysql_query("
			SELECT SQL_CALC_FOUND_ROWS s.*,
			".$searchParamRoom."
			(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=s.id && estate='rooms') AS images,
			(SELECT COUNT(*) FROM ".$template."_photo_catalogue WHERE activation='1' && p_main=s.id && estate='rooms') AS images_count,
			(SELECT title FROM ".$template."_stations WHERE activation='1' && id=s.station) AS station_name,
			(SELECT name FROM ".$template."_location WHERE activation='1' && id=s.location) AS location_name
			FROM ".$template."_rooms AS s
			WHERE s.id='".$PageInfoAd['float_id']."'
		") or die(mysql_error());
		$type_name = '<span class="type">Комната</span>';
	}
	/*
	*  Загородная
	*/
	if($PageInfoAd['estate']==4){
		$table_name = $template.'_countries';
	}
	/*
	*  Коммерческая
	*/
	if($PageInfoAd['estate']==5){
		$table_name = $template.'_commercial';
	}
	/*
	*  Переуступки
	*/
	if($PageInfoAd['estate']==6){
		$table_name = $template.'_cessions';
		$type_queues = 'cession';
		$flats = mysql_query("
			SELECT SQL_CALC_FOUND_ROWS f.*,e.distance,e.dist_value,e.name AS name_build,e.address,e.ceiling_height,e.finishing_value,
			(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=f.id && estate='cession') AS images,
			(SELECT COUNT(*) FROM ".$template."_photo_catalogue WHERE activation='1' && p_main=f.id && estate='cession') AS images_count,
			(SELECT title FROM ".$template."_stations WHERE activation='1' && id=e.station) AS station_name,
			(SELECT name FROM ".$template."_developers WHERE activation='1' && id=e.developer) AS name_developer,
			(SELECT name FROM ".$template."_location WHERE activation='1' && id=e.location) AS location_name,
			(SELECT COUNT(id) FROM ".$template."_cessions WHERE p_main='".$row['id']."' && rooms=f.rooms && queue=f.queue) AS count_flats
			FROM ".$template."_cessions AS f
			LEFT JOIN ".$template."_m_catalogue_left AS e
			ON e.id=f.p_main && e.activation='1'
			WHERE f.id='".$PageInfoAd['float_id']."'
		") or die(mysql_error());
		$type_name = '<span class="type">Переуступка</span>';
	}
	
	if(mysql_num_rows($flats)>0){
		$flat = mysql_fetch_assoc($flats);
		/*
		*  Новостройка
		*/
		if($PageInfoAd['estate']==1){
			$link = '/newbuilding/'.$flat['p_main'].'/flat/'.$flat['id'];
			$image = '<span>Нет фото</span>';
			if($flat['images'] && !empty($flat['images'])){
				$ex_image = explode(',',$flat['images']);
				$image = '<img src="/users/'.$_SESSION['idAuto'].'/'.$ex_image[2].'">';
			}
			
			$metro = '';
			$metroTable = '';
			if(!empty($flat['station'])){
				$metro = '<div class="metro">&laquo;'.$flat['station_name'].'&raquo; / <strong>'.$flat['distance'].'</strong></div>';
				$metroTable = '<div class="metro">&laquo;'.$flat['station_name'].'&raquo; <strong>/ '.$flat['distance'].'</strong></div>';
				
				$dist = floor($flat['dist_value']).' м';
				if($dist/1000>=1){
					$dist = price_cell($dist/1000,2).' км';
				}
				$metro = '<div class="metro">&laquo;'.$flat['station_name'].'&raquo; / <strong>'.$dist.'</strong></div>';
				$metroTable = '<div class="metro">&laquo;'.$flat['station_name'].'&raquo; <strong>/ '.$dist.'</strong></div>';
			}
			$name_title = $flat['rooms'].'-комн. квартира';
			$name_title2 = $flat['rooms'].'-комн. кв.';
			if($flat['rooms']==0){
				$name_title = 'Квартира студия';
				$name_title2 = 'Студия';
			}
			
			if($flat['full_square']==0){
				$flat['full_square'] = 1;
			}
			$square_meter = ceil($flat['price']/$flat['full_square']);
			$settings = '';
			$settings2 = '';
			$lift = '<i class="no">нет</i>';
			$wc = '<i class="no">нет</i>';
			$balcony = '<i class="no">нет</i>';
			$phone = '<i class="no">нет</i>';
			$www = '<i class="no">нет</i>';
			$ceiling_height = '<i>-</i>';
			if(!empty($flat['ceiling_height'])){
				$ceiling_height = '<i>'.str_replace(".", ",", $flat['ceiling_height']).' м</i>';
			}
			if(!empty($flat['lift'])){
				$lift = '<i class="ok">да</i>';
			}
			if(!empty($flat['wc'])){
				$wc = '<i class="ok">'.$_TYPE_WC[$flat['wc']].'</i>';
			}
			if(!empty($flat['balcony'])){
				$balcony = '<i class="ok">да</i>';
			}
			if(!empty($flat['phone'])){
				$phone = '<i class="ok">да</i>';
			}
			if(!empty($flat['internet'])){
				$www = '<i class="ok">да</i>';
			}
			$floors_type = '&nbsp;'; //этаж 4/б
			$floors_type_table = '';
			if(!empty($flat['floor'])){
				$floors_type = 'этаж '.$flat['floor'];
				$floors_type_table = '<strong>'.$flat['floor'].'</strong>';
			}
			if(!empty($flat['floors'])){
				$floors_type .= ' (из '.$flat['floors'].')';
				$floors_type_table .= ' (из '.$flat['floors'].')';
			}
			$settings = '<span><ins>Потолки:</ins>'.$ceiling_height.'</span><span><ins>Лифт:</ins><i>'.$lift.'</i></span><span><ins>Санузел:</ins><i>'.$wc.'</i></span><span><ins>Балкон:</ins><i>'.$balcony.'</i></span>';
			$settings2 = '<span><ins>Телефон:</ins><i>'.$phone.'</i></span><span><ins>Интернет:</ins><i>'.$www.'</i>';
			$rooms_square = '';
			if(!empty($flat['rooms_square'])){
				$rooms_square = '<div class="rooms">Комнаты <strong>'.$flat['rooms_square'].' м<sup>2</sup></strong></div>';
			}
		}
		
		/*
		*  Квартира
		*/
		if($PageInfoAd['estate']==2){
			$link = '/flats/'.$flat['id'];
			$image = '<span>Нет фото</span>';
			if($flat['images'] && !empty($flat['images'])){
				$ex_image = explode(',',$flat['images']);
				$image = '<img src="/users/'.$_SESSION['idAuto'].'/'.$ex_image[2].'">';
			}
			
			$metro = '';
			$metroTable = '';
			if(!empty($flat['station'])){
				$metro = '<div class="metro">&laquo;'.$flat['station_name'].'&raquo; / <strong>'.$flat['distance'].'</strong></div>';
				$metroTable = '<div class="metro">&laquo;'.$flat['station_name'].'&raquo; <strong>/ '.$flat['distance'].'</strong></div>';
				
				$dist = floor($flat['dist_value']).' м';
				if($dist/1000>=1){
					$dist = price_cell($dist/1000,2).' км';
				}
				$metro = '<div class="metro">&laquo;'.$flat['station_name'].'&raquo; / <strong>'.$dist.'</strong></div>';
				$metroTable = '<div class="metro">&laquo;'.$flat['station_name'].'&raquo; <strong>/ '.$dist.'</strong></div>';
			}
			$name_title = $flat['rooms'].'-комн. квартира';
			$name_title2 = $flat['rooms'].'-комн. кв.';
			if($flat['rooms']==0){
				$name_title = 'Квартира студия';
				$name_title2 = 'Студия';
			}
			
			$square_meter = ceil($flat['price']/$flat['full_square']);
			$settings = '';
			$settings2 = '';
			$lift = '<i class="no">нет</i>';
			$wc = '<i class="no">нет</i>';
			$balcony = '<i class="no">нет</i>';
			$phone = '<i class="no">нет</i>';
			$www = '<i class="no">нет</i>';
			$ceiling_height = '<i>-</i>';
			if(!empty($flat['ceiling_height'])){
				$ceiling_height = '<i>'.str_replace(".", ",", $flat['ceiling_height']).' м</i>';
			}
			if(!empty($flat['lift'])){
				$lift = '<i class="ok">да</i>';
			}
			if(!empty($flat['wc'])){
				$wc = '<i class="ok">'.$_TYPE_WC[$flat['wc']].'</i>';
			}
			if(!empty($flat['balcony'])){
				$balcony = '<i class="ok">да</i>';
			}
			if(!empty($flat['phone'])){
				$phone = '<i class="ok">да</i>';
			}
			if(!empty($flat['internet'])){
				$www = '<i class="ok">да</i>';
			}
			$floors_type = '&nbsp;'; //этаж 4/б
			$floors_type_table = '';
			if(!empty($flat['floor'])){
				$floors_type = 'этаж '.$flat['floor'];
				$floors_type_table = '<strong>'.$flat['floor'].'</strong>';
			}
			if(!empty($flat['floors'])){
				$floors_type .= ' (из '.$flat['floors'].')';
				$floors_type_table .= ' (из '.$flat['floors'].')';
			}
			$settings = '<span><ins>Потолки:</ins>'.$ceiling_height.'</span><span><ins>Лифт:</ins><i>'.$lift.'</i></span><span><ins>Санузел:</ins><i>'.$wc.'</i></span><span><ins>Балкон:</ins><i>'.$balcony.'</i></span>';
			$settings2 = '<span><ins>Телефон:</ins><i>'.$phone.'</i></span><span><ins>Интернет:</ins><i>'.$www.'</i>';
			$rooms_square = '';
			if(!empty($flat['rooms_square'])){
				$rooms_square = '<div class="rooms">Комнаты <strong>'.$flat['rooms_square'].' м<sup>2</sup></strong></div>';
			}
		}
		
		/*
		*  Комната
		*/
		if($PageInfoAd['estate']==3){
			$link = '/rooms/'.$flat['id'];
			$image = '<span>Нет фото</span>';
			if($flat['images'] && !empty($flat['images'])){
				$ex_image = explode(',',$flat['images']);
				$image = '<img src="/users/'.$_SESSION['idAuto'].'/'.$ex_image[2].'">';
			}
			
			$metro = '';
			if(!empty($flat['station'])){
				$metro = '<div class="metro">&laquo;'.$flat['station_name'].'&raquo; / <strong>'.$flat['distance'].'</strong></div>';
			}
			$name_title = 'Комната в '.$flat['rooms'].'-комн. квартире';
			
			$square_meter = '';
			$meter_price = '&nbsp;';
			$fullMeterSquare = '';
			if(!empty($flat['full_square'])){
				$square_meter = ceil($flat['price']/$flat['full_square']);
				$meter_price = '<div class="meter_price">'.price_cell($square_meter,0).' руб/м<sup>2</sup></div>';
				$fullMeterSquare = '<div class="full">Общая <strong>'.str_replace(".", ",", price_cell($flat['full_square'],1)).' м<sup>2</sup></strong></div>';
			}
			$kitchenMeterSquare = '';
			if(!empty($flat['kitchen_square'])){
				$kitchenMeterSquare = '<div class="kitchen">Кухня <strong>'.$flat['kitchen_square'].' м<sup>2</sup></strong></div>';
			}
			$settings = '';
			$settings2 = '';
			$lift = '<i class="no">нет</i>';
			$wc = '<i class="no">нет</i>';
			$balcony = '<i class="no">нет</i>';
			$phone = '<i class="no">нет</i>';
			$www = '<i class="no">нет</i>';
			$ceiling_height = '<i>-</i>';
			if(!empty($flat['ceiling_height'])){
				$ceiling_height = '<i>'.str_replace(".", ",", $flat['ceiling_height']).' м</i>';
			}
			if(!empty($flat['lift'])){
				$lift = '<i class="ok">да</i>';
			}
			if(!empty($flat['wc'])){
				$wc = '<i class="ok">'.$_TYPE_WC[$flat['wc']].'</i>';
			}
			if(!empty($flat['balcony'])){
				$balcony = '<i class="ok">да</i>';
			}
			if(!empty($flat['phone'])){
				$phone = '<i class="ok">да</i>';
			}
			if(!empty($flat['internet'])){
				$www = '<i class="ok">да</i>';
			}
			$floors_type = '&nbsp;'; //этаж 4/б
			$floors_type_table = '';
			if(!empty($flat['floor'])){
				$floors_type = 'этаж '.$flat['floor'];
				$floors_type_table = '<strong>'.$flat['floor'].'</strong>';
			}
			if(!empty($flat['floors'])){
				$floors_type .= ' (из '.$flat['floors'].')';
				$floors_type_table .= ' (из '.$flat['floors'].')';
			}
			$settings = '<span><ins>Потолки:</ins>'.$ceiling_height.'</span><span><ins>Лифт:</ins><i>'.$lift.'</i></span><span><ins>Санузел:</ins><i>'.$wc.'</i></span><span><ins>Балкон:</ins><i>'.$balcony.'</i></span>';
			$settings2 = '<span><ins>Телефон:</ins><i>'.$phone.'</i></span><span><ins>Интернет:</ins><i>'.$www.'</i>';
			$rooms_square = '';
			if(!empty($flat['rooms_square'])){
				$rooms_square = '<div class="rooms">Комнаты <strong>'.$flat['rooms_square'].' м<sup>2</sup></strong></div>';
			}
			
			$fullSquare = '';
			if(!empty($flat['full_square'])){
				$fullSquare = '<div class="full">Общая <strong>'.str_replace(".", ",", price_cell($flat['full_square'],1)).' м<sup>2</sup></strong></div>';
			}
			
			$fullKitchen = '';
			if(!empty($flat['kitchen_square'])){
				$fullKitchen = '<div class="kitchen">Кухня <strong>'.$flat['kitchen_square'].' м<sup>2</sup></strong></div>';
			}
			
			$priceMeter = '';
			if(!empty($square_meter)){
				$priceMeter = '<div class="meter_price">'.price_cell($square_meter,0).' руб/м<sup>2</sup></div>';
			}

			if($flat['type']=='rent'){
				$priceMeter = '';
			}
		}
		
		/*
		*  Переуступка
		*/
		if($PageInfoAd['estate']==6){
			$link = '/cession/'.$flat['p_main'].'/flat/'.$flat['id'];
			$image = '<span>Нет фото</span>';
			if($flat['images'] && !empty($flat['images'])){
				$ex_image = explode(',',$flat['images']);
				$image = '<img src="/users/'.$_SESSION['idAuto'].'/'.$ex_image[2].'">';
			}
			
			$metro = '';
			$metroTable = '';
			if(!empty($flat['station'])){
				$metro = '<div class="metro">&laquo;'.$flat['station_name'].'&raquo; / <strong>'.$flat['distance'].'</strong></div>';
				$metroTable = '<div class="metro">&laquo;'.$flat['station_name'].'&raquo; <strong>/ '.$flat['distance'].'</strong></div>';
				
				$dist = floor($flat['dist_value']).' м';
				if($dist/1000>=1){
					$dist = price_cell($dist/1000,2).' км';
				}
				$metro = '<div class="metro">&laquo;'.$flat['station_name'].'&raquo; / <strong>'.$dist.'</strong></div>';
				$metroTable = '<div class="metro">&laquo;'.$flat['station_name'].'&raquo; <strong>/ '.$dist.'</strong></div>';
			}
			$name_title = $flat['rooms'].'-комн. квартира';
			$name_title2 = $flat['rooms'].'-комн. кв.';
			if($flat['rooms']==0){
				$name_title = 'Квартира студия';
				$name_title2 = 'Студия';
			}
			
			if($flat['full_square']==0){
				$flat['full_square'] = 1;
			}
			$square_meter = ceil($flat['price']/$flat['full_square']);
			$settings = '';
			$settings2 = '';
			$lift = '<i class="no">нет</i>';
			$wc = '<i class="no">нет</i>';
			$balcony = '<i class="no">нет</i>';
			$phone = '<i class="no">нет</i>';
			$www = '<i class="no">нет</i>';
			$ceiling_height = '<i>-</i>';
			if(!empty($flat['ceiling_height'])){
				$ceiling_height = '<i>'.str_replace(".", ",", $flat['ceiling_height']).' м</i>';
			}
			if(!empty($flat['lift'])){
				$lift = '<i class="ok">да</i>';
			}
			if(!empty($flat['wc'])){
				$wc = '<i class="ok">'.$_TYPE_WC[$flat['wc']].'</i>';
			}
			if(!empty($flat['balcony'])){
				$balcony = '<i class="ok">да</i>';
			}
			if(!empty($flat['phone'])){
				$phone = '<i class="ok">да</i>';
			}
			if(!empty($flat['internet'])){
				$www = '<i class="ok">да</i>';
			}
			$floors_type = '&nbsp;'; //этаж 4/б
			$floors_type_table = '';
			if(!empty($flat['floor'])){
				$floors_type = 'этаж '.$flat['floor'];
				$floors_type_table = '<strong>'.$flat['floor'].'</strong>';
			}
			if(!empty($flat['floors'])){
				$floors_type .= ' (из '.$flat['floors'].')';
				$floors_type_table .= ' (из '.$flat['floors'].')';
			}
			$settings = '<span><ins>Потолки:</ins>'.$ceiling_height.'</span><span><ins>Лифт:</ins><i>'.$lift.'</i></span><span><ins>Санузел:</ins><i>'.$wc.'</i></span><span><ins>Балкон:</ins><i>'.$balcony.'</i></span>';
			$settings2 = '<span><ins>Телефон:</ins><i>'.$phone.'</i></span><span><ins>Интернет:</ins><i>'.$www.'</i>';
			$rooms_square = '';
			if(!empty($flat['rooms_square'])){
				$rooms_square = '<div class="rooms">Комнаты <strong>'.$flat['rooms_square'].' м<sup>2</sup></strong></div>';
			}
		}
		
		$name_title = $flat['rooms'].'-комн.';
		if($flat['rooms']==0){
			$name_title = 'Студия';
		}
		$rooms_square = '';
		if(!empty($flat['rooms_square'])){
			$rooms_square = '<p class="comm center"><span>'.$flat['rooms_square'].' м<sup>2</sup></span></p>';
		}
		$type_house = '';
		if(!empty($flat['type_house'])){
			$type_house = '<div class="type_house">'.$type_houseArray[$flat['type_house']].'</div>';
		}
		$img_count = '';
		if(substr($flat['images_count'],-1,1)==1 && substr($flat['images_count'],-2,2)!=11){
			$img_count = $flat['images_count'].' фотография';
		}
		else if(substr($flat['images_count'],-1,1)==2 && substr($flat['images_count'],-2,2)!=12 || substr($flat['images_count'],-1,1)==3 && substr($flat['images_count'],-2,2)!=13 || substr($flat['images_count'],-1,1)==4 && substr($flat['images_count'],-2,2)!=14){
			$img_count = $flat['images_count'].' фотографии';
		}
		else {
			$img_count = $flat['images_count'].' фотографий';
		}
		
		$text = '';
		$txt = strip_tags(htmlspecialchars_decode($flat['text']));
		if(!empty($txt)){
			$text = '<div class="text">
				'.cutString(strip_tags(htmlspecialchars_decode($flat['text'])),600).'
			</div>';
		}
		
		$paramsFloat = '';
		
		/*
		*  Высота потолков
		*/
		$ceiling_height = '';
		if(!empty($flat['ceiling_height'])){
			$ceiling_height = '<div class="comm"><span class="name">Потолки: </span><span class="stat">'.str_replace(".", ",", $flat['ceiling_height']).' м</span></div>';
		}
		
		/*
		*  Ремонт
		*/
		$repairs = '';
		if(!empty($flat['finishing_value'])){
			$repairs = '<div class="comm"><span class="name">Отделка: </span><span class="stat">'.$_TYPE_FINISHING[$flat['finishing_value']].'</span></div>';
		}
		
		/*
		*  Лифт
		*/
		$lift_value = '<span class="no stat">нет</span>';
		if($flat['lift']==1){
			$lift_value = '<span class="ok stat">да</span>';
		}
		$lift = '<div class="comm"><span class="name">Лифт: </span>'.$lift_value.'</div>';
		
		/*
		*  Санузел
		*/
		$wc = '<span class="stat no">нет</span>';
		if(!empty($flat['wc'])){
			$wc = '<span class="stat ok">'.$_TYPE_WC_ADMIN[$flat['wc']].'</span>';
		}
		// $wc = '<span class="stat">'.$_TYPE_WC_ADMIN[$flat['wc']].'</span>';
		$wc = '<div class="comm"><span class="name">Санузел: </span>'.$wc.'</div>';
		
		/*
		*  Балкон
		*/
		
		$balcony = '';
		// $balcony = '<span class="stat no">нет</span>';
		// if(!empty($flat['balcony'])){
			// $balcony = '<span class="stat ok">'.$_TYPE_BALCONY_ADMIN[$flat['balcony']].'</span>';
		// }
		// $balcony = '<div class="comm"><span class="name">Балкон: </span>'.$balcony.'</div>';
		
		/*
		*  Телефон
		*/
		$phone = '';
		// $phone_value = '<span class="no stat">нет</span>';
		// if($flat['phone']==1){
			// $phone_value = '<span class="ok stat">да</span>';
		// }
		// $phone = '<div class="comm"><span class="name">Телефон: </span>'.$phone_value.'</div>';
		
		$paramsFloat = $ceiling_height.$repairs.$lift.$wc.$balcony.$phone;

		$metroTable = '';
		if(isset($flat['station_name']) && !empty($flat['station_name'])){
			$metroTable = '<div class="metro">&laquo;'.$flat['station_name'].'&raquo; <strong>/ '.$flat['distance'].'</strong></div>';
			$dist = floor($flat['dist_value']).' м';
			if($dist/1000>=1){
				$dist = price_cell($dist/1000,2).' км';
			}
			$metroTable = '<div class="metro">&laquo;'.$flat['station_name'].'&raquo; <strong>/ '.$dist.'</strong></div>';
		}
		if($flat['full_square']==0){
			$flat['full_square'] = 1;
		}
		$square_meter = ceil($flat['price']/$flat['full_square']);
		
		$type_developer = '';
		if(!empty($flat['name_developer'])){
			$type_developer = '<div class="developer">Застройщик: <a href="'.$link_developer.'">'.$flat['name_developer'].'</a></div>';
		}

		$liveSquare = '';
		if(!empty($flat['live_square'])){
			$liveSquare = '<div class="comm">Жилая: <strong>'.str_replace(".", ",", price_cell($flat['live_square'],1)).' м<sup>2</sup></strong></div>';
		}

		$kitchenSquare = '';
		if(!empty($flat['kitchen_square'])){
			$kitchenSquare = '<div class="comm">Кухня: <strong>'.$flat['kitchen_square'].' м<sup>2</sup></strong></div>';
		}
		$requestSearch = '<div id="id_'.$flat['id'].'" class="item">
			<div style="width:485px" class="left_block">
				<div class="title"><a href="'.$link.'">'.$name_title.' <span><strong>'.str_replace(".", ",", price_cell($flat['full_square'],1)).'/</strong>'.str_replace(".", ",", price_cell($flat['live_square'],1)).' м<sup>2</sup></span></a></div>
				<div class="address">'.$flat['address'].'</div>
				'.$metro.'
				<div class="image">
					<a href="'.$link.'">'.$image.'</a>
				</div>
			</div>
			<div class="right_block">
				<div class="row">
					<div style="width:260px" class="ceil">
						<div class="full_price"><strong>'.price_cell($flat['price'],0).'</strong> руб.</div>
						<div class="meter_price">'.price_cell($square_meter,0).' руб/м<sup>2</sup></div>
					</div>
					<div class="ceil info">
						<div class="floor">'.$floors_type.'</div>
						'.$type_name.'
					</div>
					<div class="ceil more last">
						<div class="more"><a href="'.$link.'">Подробно</a></div>
					</div>
				</div>
				<div class="row fone">
					<div class="footage">
						<div class="full">Общая <strong>'.str_replace(".", ",", price_cell($flat['full_square'],1)).' м<sup>2</sup></strong></div>
						<div class="area">Жилая <strong>'.str_replace(".", ",", price_cell($flat['live_square'],1)).' м<sup>2</sup></strong></div>
						'.$rooms_square.'
						<div class="kitchen">Кухня <strong>'.$flat['kitchen_square'].' м<sup>2</sup></strong></div>
					</div>
					<div class="settings">
						'.$settings.'
					</div>
					<div class="settings last">
						'.$settings2.'
					</div>
				</div>
			</div>
		</div>';
		echo '<div class="add_block">
			<ul class="steps"><li><a href="/add?id='.$idAd.'">Расположение</a></li><li><a href="/add2?id='.$idAd.'">Параметры</a></li><li><a href="/add3?id='.$idAd.'">Стоимость</a></li><li><a href="/add4?id='.$idAd.'">Условия размещения</a></li><li class="current"><a href="/add5?id='.$idAd.'">Ваше объявление</a></li></ul>
			<div class="container_block ads step-5">
				<form action="/include/handler.php" method="POST">
					<div class="table_form">
						<div class="title_block">
							<h2>Размещение</h2>
							<div class="choose_view">
								<ul>
									<li>Посмотреть объявление в виде:</li>
									<li><a data-type="list" onclick="return choose_view(this)" href="javascript:void(0)">Списка</a></li>
									<li class="current"><a data-type="table" onclick="return choose_view(this)" href="javascript:void(0)">Таблицы</a></li>
									<!--<li><a data-type="map" onclick="return choose_view(this)" href="javascript:void(0)">Карты</a></li>-->
								</ul>
							</div>
						</div>
						<div style="display:none" id="result_search">
							<div class="list_items">
								'.$requestSearch.'
							</div>
						</div>
						<div class="table_items">
							<table class="result_search">
								<colgroup>
									<col style="width:190px">
									<col style="width:110px">
									<col style="width:125px">
									<col style="width:110px">
									<col style="width:180px">
									<col style="width:350px">
								</colgroup>
								<tr>
									<td>
										<div class="address">'.$flat['address'].'</div>'.$metroTable.'
										<div style="margin-top:10px" class="build"><a href="'.$link.'">'.htmlspecialchars_decode($flat['name_build']).'</a></div>
										'.$type_developer.'
									</td>
									<td>
										<div class="title"><a href="'.$link.'">'.$name_title.'</a>'.$type_name.'</div>
										<div class="floors">'.$floors_type_table.'</div>
										<!--'.$type_house.'-->
									</td>
									<td>
										<div class="comm">Общая: <strong>'.str_replace(".", ",", price_cell($flat['full_square'],1)).' м<sup>2</sup></strong></div>
										'.$liveSquare.'
										'.$rooms_square.'
										'.$kitchenSquare.'
									</td>
									<td>
										<center><div class="full_price"><strong>'.price_cell($flat['price'],0).'</strong> р.</div><div class="price_meter">'.price_cell($square_meter,0).' р./м<sup>2</sup></div></center></td>
									<td>
										'.$paramsFloat.'
									</td>
									<td style="vertical-align:top!important">
										<div class="info">
											<div class="id">ID: '.$flat['id'].'</div>
											<div class="images_count"><a href="'.$link.'">'.$img_count.'</a></div>
										</div>
										'.$text.'
										<div class="link"><a href="'.$link.'">Подробнее</a></div>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="descr_center">
						<p>Ваше объявление будет доступно на сайте после проверки нашими модераторами и будет показываться в результатах поиска по соответствующим параметрам...</p>
					</div>
					<div class="btn_info cost">
						<div class="table_form">
							<div class="row btn_row">
								<div class="cell center full last_col">
									'.$btnBack.'
									<label class="btn"><a href="/add" class="button">Разместить ещё +1 объявление</a></label>
								</div>
							</div>
						</div>					
					</div>
				</form>
			</div>
		</div>';
	}
echo '</div>';
?>