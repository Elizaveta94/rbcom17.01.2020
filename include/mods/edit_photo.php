<div id="breadcrumbs"><ul><li><a href="/">Главная</a></li><li>Редактирование личных данных</li></ul></div>
		<div id="right">
			<div id="content">
				<div id="user_settings">
					<form id="user_info_save">
						<div class="top_info">
							<div class="title">
								<h1 class="user_name">Эмин Кулиев</h1>
								<span class="city_name">Петропавловск-Камчатский (Россия)</span><span class="plumb">PRO</span>
							</div>	
						</div>
						<div class="checked_inform">
							<div class="photo_checked">
								<div class="inform">
									<p>Здесь Вы видите фотографии, которые отменены как Лучшие в Вашем портфолио. Именно они будут использоваться при размещении в журнале.</p>
									<div class="right-angle"></div>
								</div>
							</div>
							<div class="list_checked">
								<div class="load_inner_block">
									<ul>
									<?
										$res = mysql_query("
											SELECT *
											FROM ".$template."_photo_catalogue
											WHERE user_id='".$_SESSION['idAuto']."' && cover='1'
											ORDER BY id
										");
										if(mysql_num_rows($res)>0){
											while($row=mysql_fetch_assoc($res)){
												$ex_images = explode(',',$row['images']);
												echo '<li data-id="'.$row['id'].'"><img data-img-mobile="/users/'.$_SESSION['idAuto'].'/gallery/'.$ex_images[0].'" src="/users/'.$_SESSION['idAuto'].'/gallery/'.$ex_images[2].'"></li>';
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="drag_drop_container">
							<div id="dropzone" class="dropzone"></div>
							<div class="load_images">
								<span id="uploadsImages">Вложить новые файлы</span> или перетащите файлы сюда
							</div>
						</div>
					</form>
					<div class="list_photos">
						<div class="load_inner_block">
							<ul>
								<?
								$start = 0;
								$num = 35;
								$page_name = $linkOfPage.'/';
								
								// Инициализация pager
								@$pages = (int)$params[count($params)-1];
								$result00 = mysql_query("
									SELECT COUNT(*) 
									FROM ".$template."_photo_catalogue 
									WHERE user_id='".$_SESSION['idAuto']."'
								");
								if(mysql_num_rows($result00)>0){
									$temp = mysql_fetch_array($result00);
									$posts = $temp[0];
									if($posts > 0) {
										$total = (($posts - 1) / $num) + 1;
										$total =  intval($total);
										$pages = intval($pages);
										if(empty($pages) or $pages < 0) $pages = 1;
										  if($pages > $total) $pages = $total;
										$start = $pages * $num - $num;
									}
								}
																
								$res = mysql_query("
									SELECT SQL_CALC_FOUND_ROWS *
									FROM ".$template."_photo_catalogue
									WHERE user_id='".$_SESSION['idAuto']."'
									ORDER BY id
									LIMIT ".$start.",".$num."
								");
								if(mysql_num_rows($res)>0){
									$res2 = mysql_query("SELECT FOUND_ROWS()", $db);
									$max_rows = mysql_result($res2, 0);
									while($row=mysql_fetch_assoc($res)){
										$ex_images = explode(',',$row['images']);
										$cover_img = '';
										$title_img = 'Выбрать изображение';
										$del_sel = '';
										if($row['cover']){
											$cover_img = '<span class="use_image">Используется в журнале</span>';
											$del_sel = ' del_sel';
											$title_img = 'Снять выделение';
										}
										echo '<li data-id="'.$row['id'].'">
											<a href="#"><img width="100%" data-img-mobile="/users/'.$_SESSION['idAuto'].'/gallery/'.$ex_images[0].'" src="/users/'.$_SESSION['idAuto'].'/gallery/'.$ex_images[2].'">'.$cover_img.'</a>
											<div class="hidden_block">
												<div class="item select'.$del_sel.'"><a onclick="changeThisPhoto(this,'.$row['id'].')" title="'.$title_img.'" href="javascript:void(0)"></a></div>
												<div class="item delete"><a onclick="delThisPhoto(this,'.$row['id'].')" title="Удалить изображение" href="javascript:void(0)"></a></div>
												<div class="item cut"><a onclick="cutThisPhoto(this,'.$row['id'].')" title="Подрезать изображение" href="javascript:void(0)"></a></div>
											</div>
										</li>';
									}
								}
								?>
							</ul>
						</div>
					</div>
				<?
					// pager
					$prev = '';
					if(($pages - 1)>-1){
						if(($pages - 1)==1){
							$prev = '<li class="prev"><a title="Предыдущая" href="/'.$page_name.'"><i class="triangle-left"></i></a></li>';
						}
						else {
							$prev = '<li class="prev"><a title="Предыдущая" href="/'.$page_name.''.($pages - 1).'/"><i class="triangle-left"></i></a></li>';
						}
					}
					if ($pages != 1) $pervpage = $prev;
					if ($pages != $total) $nextpage = '<li class=next><a title="Следующая" href="/'.$page_name.''. ($pages + 1) .'"><i class="triangle-right"></i></a></li>';
					if($pages - 2 > 0) {
						if(($pages - 2)==1){
							$pagesleft = '<li><a href="/'.$page_name.'">'. ($pages - 2) .'</a></li>';
						}
						else {
							$pagesleft = '<li><a href="/'.$page_name.''. ($pages - 2) .'">'. ($pages - 2) .'</a></li>';
						}
					}
					if($pages - 1 > 0){
						if(($pages - 1)==1){
							$page1left = '<li><a href="/'.$page_name.'">'. ($pages - 1) .'</a></li>';
						}
						else {
							$page1left = '<li><a href="/'.$page_name.''. ($pages - 1) .'">'. ($pages - 1) .'</a></li>';
						}
					}
					
					if($pages + 2 <= $total) $pagesright = '<li><a href="/'.$page_name.''. ($pages + 2) .'">'. ($pages + 2) .'</a></li>';
					if($pages + 1 <= $total) $page1right = '<li><a href="/'.$page_name.''. ($pages + 1) .'">'. ($pages + 1) .'</a></li>';
					if ($total > 1){
						Error_Reporting(E_ALL & ~E_NOTICE);
						echo "<div id=\"pager\"><ul>";
						echo $pervpage.$pagesleft.$page1left.'<li class=current><span>'.$pages.'</span></li>'.$page1right.$pagesright.$nextpage.'</ul></div>';
					}
				?>
				</div>
			</div>
		</div>