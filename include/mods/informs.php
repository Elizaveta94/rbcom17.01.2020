<div id="center">
<?
	echo '<h1 class="page_list">'.$titlePage.'</h1>';
	//Вывод списка новостей
	if(count($params)==2){
		echo '<div id="news_list">';
		echo '<div class="article_block">';
		echo '<div class="title">Статьи</div>';
		echo '<div class="articles_list">';
		echo '<div class="scrollbar-inner">';
		echo '<ul>';
		$res = mysql_query("
			SELECT * 
			FROM ".$template."_articles_news
			WHERE activation='1' && lang='".$lang."'
			ORDER BY date DESC,id DESC 
		");
		if(mysql_num_rows($res)>0){
			$n = 1;
			while($row = mysql_fetch_assoc($res)){
				$date = '';
				$link = '/news/'.$row['link'];
				$ex_date = explode('-',$row['date']);
				$ex_date = $ex_date[2].' '.$ex_date[1].' '.$ex_date[0];
				$ex_date = explode(' ',$ex_date);
				for($d=0; $d<count($ex_date); $d++){
					$name_date = ' '.$ex_date[$d];
					if($d==1){
						$m = intval($ex_date[$d]);
						$name_date = ' '.$_SHORT_MONTH[$m];
					}
					$date .= $name_date;
				}
				$current = '';
				$hidden_block = '';
				if($n==1){
					$current = ' class="current"';
					$hidden_block = ' style="display:block"';
				}
				echo '<li'.$current.'>
						<div class="name"><a href="javascript:void(0)"><span>'.$row['name'].'</span></a></div>
						<div'.$hidden_block.' class="hidden_block">
							<div class="text_info">
								<div class="h3">'.$row['name'].'</div>
								<div class="text">
									'.$row['short_news'].'
								</div>
								<div class="more_read">
									<a href="/informs/'.$row['link'].'">Читать статью целиком</a>
								</div>
							</div>
						</div>
					</li>';
				$n++;
			}
		}
		else {
			echo '<li><div class="name"><a href="javascript:void(0)">Статей пока нет</a></div></li>';
		}
		echo '</ul>';
		echo '</div>';
		echo '</div>';
		echo '</div>';
		
		echo '<div class="news_block">';
		echo '<div class="title"><a href="/news">Назад в раздел "Новости и статьи"</a></div>';
		echo '<div class="news_list">';
		$date = '';
		$ex_date = explode('-',$PageInfo['date']);
		$ex_date = $ex_date[2].' '.$ex_date[1].' '.$ex_date[0];
		$ex_date = explode(' ',$ex_date);
		for($d=0; $d<count($ex_date); $d++){
			$name_date = ' '.$ex_date[$d];
			if($d==1){
				$m = intval($ex_date[$d]);
				$name_date = ' '.$_SHORT_MONTH[$m];
			}
			$date .= $name_date;
		}
		
		$linkThisPage = urlencode('https://'.$_SERVER['HTTP_HOST'].'/'.$_SERVER['REQUEST_URI']);
		
		$images = '';
		if(!empty($PageInfo['images'])){
			$ex_images = explode(',',$PageInfo['images']);
			$images = '<div class="image float_left"><img src="/admin_2/uploads/'.$ex_images[2].'"></div>';
		}
		
		$idUser = 0;
		$textFavorite = 'В избранное';
		$classFavorite = '';
		if($_SESSION['idAuto']){
			$idUser = $_SESSION['idAuto'];
			$res = mysql_query("
				SELECT COUNT(*) AS count
				FROM ".$template."_favorites
				WHERE estate=7 && user_id=".$idUser." && float_id=".$PageInfo['id']." && activation=1
			") or die(mysql_error());
			if(mysql_num_rows($res)>0){
				$row = mysql_fetch_assoc($res);
				if($row['count']){
					$textFavorite = 'В избранном';
					$classFavorite = ' at';
				}
			}
		}
		$favorite = '<a class="add_favorite'.$classFavorite.'" onclick="return addFavorite(this,\'article\','.$PageInfo['id'].')" href="javascript:void(0)"><span>'.$textFavorite.'</span></a>';
		echo '<div class="big_news card">
			<div class="date">'.$date.'</div>
			<div class="favorite">'.$favorite.'</div>
			<!--<div class="title">'.$PageInfo['name'].'</div>-->
			<div class="text_info" style="max-height:100%">
				<div class="text">
					'.$images.'
					'.htmlspecialchars_decode($PageInfo['text']).'
				</div>
				<div class="share_block">
					<ul>
						<li>Поделиться:</li>
						<li><a onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;" href="http://vk.com/share.php?url='.$linkThisPage.'"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
						<li><a onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;" href="https://www.facebook.com/sharer.php?src=sp&u='.$linkThisPage.'"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;" href="https://plus.google.com/share?url='.$linkThisPage.'"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
						<li><a onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;" href="https://twitter.com/intent/tweet?url='.$linkThisPage.'"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;" href="https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl='.$linkThisPage.'"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a></li>
					</ul>
				</div>

			</div>
		</div>';
		echo '</div>';
		$res = mysql_query("
			SELECT * 
			FROM ".$template."_m_news_left 
			WHERE activation='1' && lang='".$lang."' && id!='".$PageInfo['id']."'
			ORDER BY date DESC,id DESC 
			LIMIT 2
		");
		if(mysql_num_rows($res)>0){
			echo '<div class="other_blocks">';
			echo '<div class="title">Другие новости</div>';
			echo '<ul>';
			while($row = mysql_fetch_assoc($res)){
				$date = '';
				$link = '/news/'.$row['link'];
				$ex_date = explode('-',$row['date']);
				$ex_date = $ex_date[2].' '.$ex_date[1].' '.$ex_date[0];
				$ex_date = explode(' ',$ex_date);
				for($d=0; $d<count($ex_date); $d++){
					$name_date = ' '.$ex_date[$d];
					if($d==1){
						$m = intval($ex_date[$d]);
						$name_date = ' '.$_SHORT_MONTH[$m];
					}
					$date .= $name_date;
				}

				echo '<li>
					<div class="date">'.$date.'</div>
					<div class="name"><a href="'.$link.'">'.$row['name'].'</a></div>
				</li>';
			}
			echo '</ul>';
			echo '</div>';
		}
		echo '</div>';
		echo '</div>';
	}
?>
</div>