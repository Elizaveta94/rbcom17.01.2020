<?
echo '<div id="center">
	<h1 class="inner_pages">'.$titlePage.'</h1>';
	$region = '<div class="select_block count">';
	$valueParams = '';
	$currentParams = '';
	// $regions = '<li'.$current.'><a href="javascript:void(0)" data-id="1">Санкт-Петербург</a></li>';
	$type_houseArray = array();;
	$nameParams = 'Не выбран';
	// $devs = mysql_query("
		// SELECT *, (SELECT name FROM ".$template."_type_house WHERE activation='1' && id='".$currentParams."') AS choose_value
		// FROM ".$template."_type_house
		// WHERE activation='1'
		// ORDER BY num
	// ");
	// if(mysql_num_rows($devs)>0){
		// while($dev = mysql_fetch_assoc($devs)){
			// $regions .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$dev['id'].'">'.$dev['name'].'</a></li>';
			// $type_houseArray[$dev['id']] = $dev['name'];										
		// }
	// }
	
	$regionsArray = array();
	$res = mysql_query("
		SELECT *
		FROM ".$template."_location
		WHERE activation='1'
		ORDER BY name
	");
	if(mysql_num_rows($res)>0){
		while($row = mysql_fetch_assoc($res)){
			$reg = '<li><a href="javascript:void(0)" data-id="'.$row['id'].'">'.$row['name'].'</a></li>';
			array_push($regionsArray,$reg);
		}
	}
	$region .= '<input type="hidden" name="s[region]">';
	$region .= '<div class="choosed_block">'.$nameParams.'</div>';
	$region .= '<div class="scrollbar-inner">';
	$region .= '<ul>';
	if(count($regionsArray)>0){
		$region .= implode('',$regionsArray);
	}
	$region .= '</ul>';
	$region .= '</div>';
	$region .= '</div>';
	echo '<div class="text_block">
		'.$textPage.'
	</div>
	<div class="registration_block">
		<ul class="sub"><li><a href="/enter">ВХОД</a></li><li class="current"><a href="/reg">Регистрация</a></li></ul>
		<div class="container_block">
			<div class="left_block">
				<form onsubmit="return checkSideUniForm(this)" action="/include/handler.php" method="POST">
					<input type="hidden" name="actionForm" value="reg">
					<div class="table_form">
						<div class="row">
							<div class="cell label right one_second">
								<label>Имя, Фамилия<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="text" class="text" name="s[name]">
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Регион<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								'.$region.'
							</div>
						</div>
						<div class="row">
							<div class="cell label one_second">&nbsp;</div>
							<div class="cell one_second last_col">
								<div class="checkbox_single">
									<input disabled="disabled" type="hidden" name="s[agency_true]">
									<div class="container">
										<span class="check"></span><span class="label">Я представляю агентство</span>
									</div>
								</div>
							</div>
						</div>
						<div class="row agency">
							<div class="cell label right one_second">
								<label>Агентство</label>
							</div>
							<div class="cell one_second last_col">
								<input disabled="disabled" type="text" class="text" name="s[agency]">
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label class="top">Мобильный телефон<b>*</b></label>
							</div>
							<div class="cell one_second last_col">
								<div class="column required"><input maxlength="4" placeholder="код" type="text" class="code" name="mobile[code]"></div>
								<div class="column required last"><input type="text" class="phone_type phone_mask" name="mobile[phone]"></div>
							</div>
						</div>
						<div class="row agency">
							<div class="cell label right one_second">
								<label class="top">Городской телефон</label>
							</div>
							<div class="cell one_second last_col">
								<div class="column"><input disabled="disabled" maxlength="4" placeholder="код" type="text" class="code" name="city[code]"></div>
								<div class="column last"><input disabled="disabled" type="text" class="phone_type phone_mask" name="city[phone]"></div>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>e-mail<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="text" class="text" name="s[email]">
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Пароль<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="password" class="text" name="s[password]">
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Пароль ещё раз<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="password" class="text" name="s[password_repeat]">
							</div>
						</div>
						<div class="row">
							<div class="cell label one_second">&nbsp;</div>
							<div class="cell one_second last_col">
								<div class="checkbox_single">
									<input disabled="disabled" type="hidden" name="delivery">
									<div class="container">
										<span class="check"></span><span class="label">Я хочу получать новостную рассылку</span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="cell label one_second">&nbsp;</div>
							<div class="cell one_second last_col">
								<div class="checkbox_single">
									<input disabled="disabled" type="hidden" name="agree">
									<div class="container">
										<span class="check"></span><span class="label">Нажимая кнопку «Зарегистрироваться» я выражаю согласие на правила обработки персональных данных согласно <a href="/user-agreement" target="_blank">"Политике защиты персональной информации пользователей сайта"</a></span>
									</div>
								</div>
							</div>
						</div>
						<div class="row btn_row">
							<div class="cell label one_second">&nbsp;</div>
							<div class="cell one_second last_col">
								<label class="btn disabled"><input onclick="return list_areas_close(true,this)" disabled="disabled" type="submit" value="Зарегистрироваться"><span class="angle-right"></span></label>
							</div>
						</div>
						<div class="row">
							<div class="cell full">
								<div class="req"><b>*</b> - поля обязательные для заполнения</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="right_block">
				<div class="text_info">	
					<p>Портал RBCom предлагает своим пользователям моментальное размещение любого количества объявлений на сайте с потенциальной аудиторией в 15 000 человек/день.</p>
				</div>
				<div class="centers_block">
					<div class="item list">
						<div class="text">Каталог актуальных предложений<br> объектов недвижимости<br> списком</div>
					</div>
					<div class="item map">
						<div class="text">Отображение результатов поиска <br>объектов недвижимости прямо <br>на карте</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>';
?>