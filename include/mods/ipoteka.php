<?
// Хлебные крошки (эталон)
$arrayLinkWay = array();
breadArrayLink($idParentComp);
$parentBreadcrumb = "";
for($c=count($arrayLinkWay)-1; $c>=0; $c--){
	$linkArray = '';
	$n = 0;
	for($d=$c; $d<count($arrayLinkWay); $d++){
		$linkArray .= '/'.$arrayLinkWay[(count($arrayLinkWay)-1)-$n]['link'];
		$n = $n+1;
	}
	$parentBreadcrumb .= '<li><a href="'.$linkArray.'">'.$arrayLinkWay[$c]['bread'].'</a></li>';
}
// Хлебные крошки (эталон)

echo '<div id="center">';
echo '<h1 class="inner_pages">'.$titlePage.'</h1>';

$estates = '';
$nameParams = 'Не выбран';
$valueInput = '';
$disabledEstate = '';
for($t=1; $t<=count($_TYPE_ESTATE_NAME); $t++){
	$current = "";
	if($t==1){
		$current = ' class="current"';
		$nameParams = $_TYPE_ESTATE_NAME[$t];
		$valueInput = ' value="'.$t.'"';
	}
	$estates .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$t.'">'.$_TYPE_ESTATE_NAME[$t].'</a></li>';		
}
$estate = '<div class="select_block count">';
$estate .= '<input type="hidden" name="estate"'.$valueInput.'>';
$estate .= '<div class="choosed_block">'.$nameParams.'</div>';
$estate .= '<div class="scrollbar-inner">';
$estate .= '<ul>';
$estate .= $estates;
$estate .= '</ul>';
$estate .= '</div>';
$estate .= '</div>';

$region = '<div class="select_block count">';
$valueParams = '';
$currentParams = '';
$regions = '<li'.$current.'><a href="javascript:void(0)" data-id="1">Санкт-Петербург</a></li>';
$type_houseArray = array();;
$nameParams = 'Санкт-Петербург';
// $devs = mysql_query("
	// SELECT *, (SELECT name FROM ".$template."_type_house WHERE activation='1' && id='".$currentParams."') AS choose_value
	// FROM ".$template."_type_house
	// WHERE activation='1'
	// ORDER BY num
// ");
// if(mysql_num_rows($devs)>0){
	// while($dev = mysql_fetch_assoc($devs)){
		// $regions .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$dev['id'].'">'.$dev['name'].'</a></li>';
		// $type_houseArray[$dev['id']] = $dev['name'];										
	// }
// }

$region .= '<input type="hidden" name="s[region]" value="1">';
$region .= '<div class="choosed_block">'.$nameParams.'</div>';
$region .= '<div class="scrollbar-inner">';
$region .= '<ul>';
$region .= $regions;
$region .= '</ul>';
$region .= '</div>';
	$region .= '</div>';
?>
<div id="ipoteka">
	<div class="calculation_block">
		<div class="container_block">
			<div class="left_block">
				<form onsubmit="return checkSideUniForm(this)" action="/include/handler.php" method="POST">
					<input type="hidden" name="actionForm" value="calc">
					<div class="table_form">
						<div class="row">
							<div class="cell label right one_second">
								<label>Цена объекта</label>
							</div>
							<div class="cell required one_second last_col">
								<input type="hidden" name="s[price]" value="5500000">
								<input type="hidden" class="val" value="5 500 000">
								<span class="input_text"><span class="value">5500000</span><span class="ch"> р.</span></span>
								<div data-default="5500000" data-min="900000" data-max="30000000" data-step="100000" data-value="р." class="slider"></div>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Первоначальный взнос</label>
							</div>
							<div class="cell required one_second last_col">
								<input type="hidden" name="s[contribution]" value="2500000">
								<input type="hidden" class="val" value="2 500 000">
								<span class="input_text"><span class="value">2500000</span><span class="ch"> р.</span></span>
								<div data-default="2500000" data-min="100000" data-max="20000000" data-step="10000" data-value="р." class="slider"></div>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Срок кредита</label>
							</div>
							<div class="cell required one_second last_col">
								<input type="hidden" name="s[credit]" value="60">
								<span class="input_text"><span class="value">60</span><span class="ch"> месяцев</span></span>
								<div data-default="60" data-min="60" data-max="300" data-step="6" data-value="месяцев" class="slider"></div>
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Тип недвижимости</label>
							</div>
							<div class="cell required one_second last_col">
								<?=$estate?>
							</div>
						</div>
						<div class="separate"></div>
					</div>
				</form>
			</div>
			<div class="right_block">
				<div class="total_calculation">
					<div class="label">Ежемесячный платёж составляет:</div>
					<div class="total_count">45 612 р.</div>
				</div>
			</div>
		</div>
	</div>
	<div class="offers_block">
		<h2>Ознакомьтесь с несколькими предложениями банков</h2>
		<table class="result_search">
			<colgroup>
				<col style="width:275px">
				<col style="width:100px">
				<col style="width:160px">
				<col style="width:172px">
				<col style="width:160px">
				<col style="width:160px">
				<col style="width:70px">
			</colgroup>
			<tr>
				<th style="width:275px"><span>Банк</span></th>
				<!--<th style="width:100px"><span><strong>Вероятность <br>одобрения</strong></span></th>-->
				<th style="width:160px"><span>Минимальная <br>ставка</span></th>
				<th style="width:172px"><span>Максимальный <br>срок</span></th>
				<th style="width:160px"><span>Срок <br>рассмотрения</span></th>
				<th style="width:160px"><span>Первоначальный <br>взнос</span></th>
				<th class="small" style="width:70px"><span></span></th>
			</tr>
			<tr class="enabled" data-id="1">
				<td><div style="background-image:url(/images/bank_logo_1.png)" class="logo_bank"></div></td>
				<!--<td><div class="probability high">Высокая</div></td>-->
				<td>от <strong>12.5</strong>%</td>
				<td><strong>15</strong> лет</td>
				<td><strong>2</strong> дня</td>
				<td>от <strong>1</strong>%</td>
				<td><div class="choose"><a href="javascript:void(0)"><span>Выбрать</span></a></div></td>
			</tr>
			<tr class="enabled" data-id="2">
				<td><div style="background-image:url(/images/bank_logo_2.png)" class="logo_bank"></div></td>
				<!--<td><div class="probability middle">Средняя</div></td>-->
				<td>от <strong>12.5</strong>%</td>
				<td><strong>15</strong> лет</td>
				<td><strong>2</strong> дня</td>
				<td>от <strong>1</strong>%</td>
				<td><div class="choose"><a href="javascript:void(0)"><span>Выбрать</span></a></div></td>
			</tr>
		</table>
		<div class="offer_tables">
			<h2>Для получения персонального предложения заполните заявку</h2>
			<form onsubmit="return checkSideUniForm(this)" action="/include/handler.php" method="POST">
				<input type="hidden" name="actionForm" value="app">
				<div class="tables_form">
					<div class="table_form left">
						<div class="row">
							<div class="cell label right one_second">
								<label>Ваше имя<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="text" name="s[name]">
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Мобильный телефон<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input class="mask_phone" type="text" name="s[phone]">
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Серия и номер паспорта<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input class="mask_passport" type="text" name="s[passport]">
							</div>
						</div>
					</div>
					<div class="table_form right">
						<div class="row">
							<div class="cell label right one_second">
								<label>Ваша фамилия<b>*</b></label>
							</div>
							<div class="cell required one_second last_col">
								<input type="text" name="s[surname]">
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Ваш e-mail</label>
							</div>
							<div class="cell one_second last_col">
								<input type="text" name="s[email]">
							</div>
						</div>
						<div class="row">
							<div class="cell label right one_second">
								<label>Регион</label>
							</div>
							<div class="cell one_second last_col">
								<?=$region?>
							</div>
						</div>
					</div>
					<div class="table_form full">
						<div class="row">
							<div style="width:180px" class="cell label right one_second">&nbsp;</div>
							<div class="cell required one_second last_col">
								<div class="checkbox_single">
									<input disabled="disabled" type="hidden" name="s[agree]">
									<div class="container">
										<span class="check"></span><span class="label">Я согласен(на) с условиями передачи информации</span>
									</div>
								</div>
								<div class="save_datas">Ваши данные абсолютно защищены.<br>Подбор кредита осуществляется на основе Вашего кредитного рейтинга по Вашим персональньным данным.</div>
							</div>
						</div>
					</div>
				</div>
				<div class="separate"></div>
				<div class="table_form btn">
					<div class="row btn_row">
						<div class="cell label one_second">&nbsp;</div>
						<div class="cell one_second last_col">
							<label class="btn"><input type="submit" value="Отправить заявку"><span class="angle-right"></span></label>
						</div>
					</div>
					<div class="row">
						<div class="cell full">
							<div class="req"><b>*</b> - поля обязательные для заполнения</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?echo '</div>';?>