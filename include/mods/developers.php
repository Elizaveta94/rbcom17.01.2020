<?if(count($params)==1){
$LOAD_PAGE = 120;
$NXT_CTLG = 0;
$max_rows = 0;
$limmitSet = "";
$AMNT = ",".$LOAD_PAGE;
if(isset($arrayLinkSearch['page'])){
	$NXT_CTLG = ((int)$arrayLinkSearch['page']-1)*$LOAD_PAGE;
	$AMNT = ",".$LOAD_PAGE;
}
$limmitSet = "LIMIT	".$NXT_CTLG.$AMNT."";
$developers = '';
$res = mysql_query("
	SELECT SQL_CALC_FOUND_ROWS d.*,
	(SELECT COUNT(*) FROM ".$template."_m_catalogue_left WHERE developer=d.developer_id && ready!=5) AS countBuild,
	(SELECT COUNT(*) FROM ".$template."_m_catalogue_left WHERE developer=d.developer_id && ready=5) AS countReady
	FROM ".$template."_developers_info AS d
	WHERE d.activation=1
	ORDER BY d.num
	".$limmitSet."
");
if(mysql_num_rows($res)>0){
	$res2 = mysql_query("SELECT FOUND_ROWS()", $db);
	$max_rows = mysql_result($res2, 0);
	while($row = mysql_fetch_assoc($res)){
		$site = 'http://'.$row['site'];
		$images = '';
		if(!empty($row['images'])){
			$img = explode(',',$row['images']);
			$images = '<div class="image">
				<img src="/admin_2/uploads/'.$img[0].'">
			</div>';
		}
		$site_row = '';
		if(!empty($row['site'])){
			$site_row = '<div class="row"><span>Сайт:</span><a target="_blank" href="'.$site.'">'.$row['site'].'</a></div>';
		}
		$link = '/developers/'.$row['id'];
		$images = '';
		if(!empty($row['images'])){
			$img = explode(',',$row['images']);
			$images = '<div class="image">
				<img src="/admin_2/uploads/'.$img[0].'">
			</div>';
		}
		$phone_one = '';
		if(!empty($row['phone_one'])){
			$phone_one = '<div class="row"><span>Телефон:</span><strong>'.$row['phone_one'].'</strong></div>';
		}
		$phone_two = '';
		if(!empty($row['phone_two'])){
			$phone_two = '<div class="row"><span>&nbsp;</span><strong>'.$row['phone_two'].'</strong></div>';
		}
		
		$full_text = '';
		$text = htmlspecialchars_decode($row['text']);
		if(!empty($text)){
			$full_text = '<div class="text">'.$text.'</div><div class="more_read"><a href="javascript:void(0)">Читать всё</a></div>';
		}

		$today = strtotime(date("Y-m-d")) - strtotime($row['date']);
		$days = $today/86400;
		$months = $days/30;
		$months = floor($months);
		$reg_date = '';
		if($months<12){
			if($months>=1){
				if($months==1){
					$reg_date = '<strong>1 месяц</strong> на сайте';
				}
				else if($months==2 || $months==3 || $months==4){
					$reg_date = '<strong>'.$months.' месяца</strong> на сайте';
				}
				else {
					$reg_date = '<strong>'.$months.' месяцев</strong> на сайте';
				}
			}
			else {
				$reg_date = '<strong>меньше месяца</strong> на сайте';
			}
		}
		else if($months==12) {
			$reg_date = 'На сайте 1 год';
		}
		else {
			$year = floor($months/12);
			$months = $months - ($year * 12);
			if($months==0){
				$month = '';
			}
			else if($months==1){
				$month = ' и 1 месяц';
			}
			else if($months==2 || $months==3 || $months==4){
				$month = ' и '.$months.' месяца';
			}
			else {
				$month = ' и '.$months.' месяцев';
			}
			if($year==1){
				$year = '1 год';
			}
			else if($year==2 || $year==3 || $year==4){
				$year = $year.' года';
			}
			else {
				$year = $year.' лет';
			}
			$reg_date = '<strong>'.$year.$month.'</strong> на сайте';
		}
		$countReady = $row['countReady'];
		$linkReady = '/search/kupit/novostroyka/all?developer='.$row['developer_id'].'&ready=5';
		$countBuild = $row['countBuild'];
		$linkBuild = '/search/kupit/novostroyka/all?developer='.$row['developer_id'].'&ready=0';
		
		/*
		*  !!! ПОМЕНЯТЬ ПАРАМЕТРЫ WHERE !!!
		*/
		$countFull = 0;
		$counts = mysql_query("
			SELECT c.id,
			(SELECT COUNT(*) FROM ".$template."_new_flats WHERE activation=1 && p_main=c.id) AS count
			FROM ".$template."_m_catalogue_left AS c
			WHERE c.activation=1 && c.developer='".$row['developer_id']."'
		");
		if(mysql_num_rows($counts)>0){
			while($c = mysql_fetch_assoc($counts)){
				$countFull = $countFull + $c['count'];
			}
		}
		
		$developers .= '<div id="id_'.$row['id'].'" class="item">
				<div class="container_block">
					<div class="main_info_block">
						<div class="name"><a target="_blank" href="'.$link.'">'.$row['name'].'</a></div>
						<ul>
							<li>Сдано: <a href="'.$linkReady.'">'.$countReady.' ЖК</a></li>
							<li>Строится: <a href="'.$linkBuild.'">'.$countBuild.' ЖК</a></li>
							<li class="last">Всего предложений: <a href="/search/kupit/novostroyka/all?developer='.$row['developer_id'].'">'.price_cell($countFull,0).'</a></li>
						</ul>
					</div>
					<div class="info_developer">
						'.$images.'
						<div class="description">
							<div class="contacts">
								'.$phone_one.'
								'.$phone_two.'
								'.$site_row.'
							</div>
						</div>
					</div>
				</div>
				<div class="text_full">
					<div class="reg_time">'.$reg_date.'</div>
					'.$full_text.'
				</div>
			</div>';
	}
}
else {
	$developers = '<h3>Застройщике в системе не найдены</h3>';
}
?>
<div id="center">
	<div class="title_pages">Застройщики</div>
	<div class="sort_offers">
		<div class="count_offers">Всего<span><?=$max_rows?></span> застройщика: <a href="#">Санкт-Петербург</a></div>
		<div class="sort_block">
			<div class="label_select">Сортировать:</div>
			<div class="select_block sort">
				<input type="hidden" name="sort">
				<?
				$countOffersValue = ' class="current"';
				$nameValue = '';
				$nameSort = 'По кол-ву объектов';
				if($valueSortName=='count'){
					$nameSort = 'По кол-ву объектов';
					$countOffersValue = ' class="current"';
				}
				if($valueSortName=='name'){
					$nameSort = 'По имени';
					$nameValue = ' class="current"';
					$countOffersValue = '';
				}
				?>
				<div style="width:160px" class="choosed_block"><?=$nameSort?></div>
				<div class="scrollbar-inner">
					<ul>
						<li<?=$countOffersValue?>><a href="<?=$linkSortCounts?>" data-id="1">По кол-ву объектов</a></li>
						<li<?=$nameValue?>><a href="<?=$linkSortNames?>" data-id="2">По имени</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!--<div style="line-height:1;margin-bottom:18px" class="pager">
		<ul>
			<li class="current"><span>1</span></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li class="next"><a title="Следующая" href="#">→</a></li>
			<li class="last"><a title="В конец" href="#">В конец</a></li>
		</ul>
	</div>-->
	<div id="result_search" class="developer_block">
		<div class="list_users">
			<?=$developers?>
		</div>
	</div>
	<!--<div style="line-height:1;margin-bottom:18px" class="pager">
		<ul>
			<li class="current"><span>1</span></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li class="next"><a title="Следующая" href="#">→</a></li>
			<li class="last"><a title="В конец" href="#">В конец</a></li>
		</ul>
	</div>-->
</div>
<?}?>
<?
if(count($params)==2){
	$image = '';
	if(!empty($PageInfo['images'])){
		$img = explode(',',$PageInfo['images']);
		$image = '<div class="image">
			<img src="/admin_2/uploads/'.$img[0].'">
		</div>';
	}
?>
<div id="card_personal">
<?
	$r = mysql_query("
		SELECT COUNT(*) AS countBuild
		FROM ".$template."_m_catalogue_left 
		WHERE developer='".$PageInfo['developer_id']."' && ready!=5
	");
	$rw = mysql_fetch_assoc($r);
	$countBuild = $rw['countBuild'];
	$r = mysql_query("
		SELECT COUNT(*) AS countReady
		FROM ".$template."_m_catalogue_left 
		WHERE developer='".$PageInfo['developer_id']."' && ready=5
	");
	$rw = mysql_fetch_assoc($r);
	$countReady = $rw['countReady'];
	
	$linkReady = '/search/kupit/novostroyka/all?developer='.$PageInfo['developer_id'].'&ready=5';
	$linkBuild = '/search/kupit/novostroyka/all?developer='.$PageInfo['developer_id'].'&ready=0';

	$full_text = '';
	$text = htmlspecialchars_decode($PageInfo['text']);
	if(!empty($text)){
		$full_text = '<div class="text_full">
			<div class="other_info">'.$text.'</div>
		</div>';
	}
	
	$phone_one = '';
	if(!empty($PageInfo['phone_one'])){
		$phone_one = '<div class="row"><span>Телефон:</span><strong>'.$PageInfo['phone_one'].'</strong></div>';
	}
	$phone_two = '';
	if(!empty($PageInfo['phone_two'])){
		$phone_two = '<div class="row"><span>&nbsp;</span><strong>'.$PageInfo['phone_two'].'</strong></div>';
	}
	$site_row = '';
	if(!empty($PageInfo['site'])){
		$site_row = '<div class="row"><span>Сайт:</span><a target="_blank" href="'.$site.'">'.$PageInfo['site'].'</a></div>';
	}
	echo '<div class="personal_info">
		<div class="container_block">
			<div class="description">
				<div class="name">'.$PageInfo['name'].'</div>
				<div class="list_back"><a href="/developers"><span>Вернуться к списку застройщиков</span></a></div>
				<div class="main_info_block">
					<ul>
						<li>Сдано: <a href="'.$linkReady.'">'.$countReady.' ЖК</a></li>
						<li>Строится: <a href="'.$linkBuild.'">'.$countBuild.' ЖК</a></li>
					</ul>
				</div>
				'.$full_text.'
			</div>
		</div>
		'.$image.'
		<div class="contacts">
			'.$phone_one.'
			'.$phone_two.'
			'.$site_row.'
		</div>
	</div>';
	
	$max_rows = 0;
	$res = mysql_query("
		SELECT SQL_CALC_FOUND_ROWS e.*,
		MIN(f.price/f.full_square) AS min_price,
		(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=e.id && estate='jk') AS images,
		(SELECT title FROM ".$template."_stations WHERE activation='1' && id=e.station) AS station_name,
		(SELECT name FROM ".$template."_developers WHERE activation='1' && id=e.developer) AS name_developer,
		(SELECT name FROM ".$template."_location WHERE activation='1' && id=e.location) AS location_name
		FROM ".$template."_m_catalogue_left AS e
		LEFT JOIN ".$template."_new_flats AS f
		ON f.p_main=e.id
		WHERE e.developer='".$PageInfo['developer_id']."'
		GROUP BY e.id
		ORDER BY min_price
		LIMIT 0,2
	") or die(mysql_error());
	
	$show_all_new = '';
	$requestSearch = '';
	$requestSearchList = '';
	$coordsArray = array();
	$coordsArray2 = array();
	$estateArray = array();
	$arrayBuilds = array();
	if(mysql_num_rows($res)>0){
		$res2 = mysql_query("SELECT FOUND_ROWS()", $db);
		$max_rows = mysql_result($res2, 0);
		$s = 1;
		while($row = mysql_fetch_assoc($res)){
			array_push($arrayBuilds,$row['id']);
			$addParam = '';
			
			$show_all_new = '';
			if($max_rows-2>0){
				$show_all_new = '<div class="show_all"><a href="/search/kupit/novostroyka/all?developer='.$PageInfo['developer_id'].'"><strong>+'.($max_rows-2).'</strong><span class="zoom"></span><span class="see">(Смотреть все предложения)</span></a></div>';
			}
			if(isset($arrayLinkSearch['rooms'])){
				$ex_rooms2 = explode(',',$arrayLinkSearch['rooms']);
				$arrRoomForm = array();
				if(count($ex_rooms2)>1){
					for($e2=0; $e2<count($ex_rooms2); $e2++){
						if($ex_rooms2[$e2]==4){
							array_push($arrRoomForm,"f.rooms>='".$ex_rooms2[$e2]."'");
						}
						else {
							array_push($arrRoomForm,"f.rooms='".$ex_rooms2[$e2]."'");
						}
					}
					$addParam = ' && ('.implode(' || ',$arrRoomForm).')';
				}
				else {
					if($ex_rooms2[0]==4){
						$addParam = " && f.rooms>='".$ex_rooms2[0]."'";
					}
					else {
						$addParam = " && f.rooms='".$ex_rooms2[0]."'";
					}
				}
			}
			
			/*
			*  Поиск минимальной общей площади
			*/
			$min_full_square = 1;
			$min_full_squares = mysql_query("
				SELECT MIN(full_square) AS min_square,
				(SELECT COUNT(*) FROM ".$template."_new_flats WHERE activation='1' && p_main='".$row['id']."') AS count
				FROM ".$template."_new_flats 
				WHERE activation='1' && p_main='".$row['id']."'
			");
			if(mysql_num_rows($min_full_squares)>0){
				$min_full_square2 = mysql_fetch_assoc($min_full_squares);
				if($min_full_square2['count']>0){
					$min_full_square = $min_full_square2['min_square'];
				}
				else {
					$min_full_squares = mysql_query("
						SELECT MIN(full_square) AS min_square,
						(SELECT COUNT(*) FROM ".$template."_cessions WHERE activation='1' && p_main='".$row['id']."') AS count
						FROM ".$template."_cessions
						WHERE activation='1' && p_main='".$row['id']."'
					");
					if(mysql_num_rows($min_full_squares)>0){
						$min_full_square2 = mysql_fetch_assoc($min_full_squares);
						if($min_full_square2['count']>0){
							$min_full_square = $min_full_square2['min_square'];
						}
						else {
							$min_full_square = 1;
						}
					}
				}
			}

			/*
			*  Поиск максимальной общей площади
			*/
			$max_full_square = 1;
			$max_full_squares = mysql_query("
				SELECT MAX(full_square) AS max_square,
				(SELECT COUNT(*) FROM ".$template."_new_flats WHERE activation='1' && p_main='".$row['id']."') AS count
				FROM ".$template."_new_flats 
				WHERE activation='1' && p_main='".$row['id']."'
			");
			if(mysql_num_rows($max_full_squares)>0){
				$max_full_square2 = mysql_fetch_assoc($max_full_squares);
				if($max_full_square2['count']>0){
					$max_full_square = $max_full_square2['max_square'];
				}
				else {
					$max_full_squares = mysql_query("
						SELECT MAX(full_square) AS max_square,
						(SELECT COUNT(*) FROM ".$template."_cessions WHERE activation='1' && p_main='".$row['id']."') AS count
						FROM ".$template."_cessions
						WHERE activation='1' && p_main='".$row['id']."'
					");
					if(mysql_num_rows($max_full_squares)>0){
						$max_full_square2 = mysql_fetch_assoc($max_full_squares);
						if($max_full_square2['count']>0){
							$max_full_square = $max_full_square2['max_square'];
						}
						else {
							$max_full_square = 1;
						}
					}
				}
			}
						
			/*
			*  Поиск минимальной стоимости
			*/
			$min_price = 0;
			$min_prices = mysql_query("
				SELECT MIN(f.price) AS min_price,
				(SELECT COUNT(*) FROM ".$template."_new_flats WHERE activation='1' && p_main='".$row['id']."') AS count
				FROM ".$template."_new_flats AS f
				WHERE f.activation='1' && f.p_main='".$row['id']."'
			");
			if(mysql_num_rows($min_prices)>0){
				$min_price2 = mysql_fetch_assoc($min_prices);
				if($min_price2['count']>0){
					$min_price = $min_price2['min_price'];
				}
				else {
					$min_prices = mysql_query("
						SELECT MIN(price) AS min_price,
						(SELECT COUNT(*) FROM ".$template."_cessions WHERE activation='1' && p_main='".$row['id']."') AS count
						FROM ".$template."_cessions
						WHERE activation='1' && p_main='".$row['id']."'
					");
					if(mysql_num_rows($min_prices)>0){
						$min_price2 = mysql_fetch_assoc($min_prices);
						if($min_price2['count']>0){
							$min_price = $min_price2['min_price'];
						}
						else {
							$min_price = 1;
						}
					}
				}
			}
			
			/*
			*  Поиск максимальной стоимости
			*/
			$max_price = 0;
			$max_prices = mysql_query("
				SELECT MAX(price) AS max_price,
				(SELECT COUNT(*) FROM ".$template."_new_flats WHERE activation='1' && p_main='".$row['id']."') AS count
				FROM ".$template."_new_flats 
				WHERE activation='1' && p_main='".$row['id']."'
			");
			if(mysql_num_rows($max_prices)>0){
				$max_price2 = mysql_fetch_assoc($max_prices);
				if($max_price2['count']>0){
					$max_price = $max_price2['max_price'];
				}
				else {
					$max_prices = mysql_query("
						SELECT MAX(price) AS max_price,
						(SELECT COUNT(*) FROM ".$template."_cessions WHERE activation='1' && p_main='".$row['id']."') AS count
						FROM ".$template."_cessions
						WHERE activation='1' && p_main='".$row['id']."'
					");
					if(mysql_num_rows($max_prices)>0){
						$max_price2 = mysql_fetch_assoc($max_prices);
						if($max_price2['count']>0){
							$max_price = $max_price2['max_price'];
						}
						else {
							$max_price = 1;
						}
					}
				}
			}
			
			$link = '/newbuilding/'.$row['id'];
			$link_developer = '/search?type=sell&estate=1&priceAll=all&developer='.$row['developer'];
			$flats_list = '';

			$searchParamsFlat2 = $searchParamsFlats;
			if(!empty($searchParamsFlats)){
				$searchParamsFlat2 = " && ".$searchParamsFlats;
			}
			$flats = mysql_query("
				SELECT SQL_CALC_FOUND_ROWS f.*,
				(SELECT COUNT(id) FROM ".$template."_new_flats WHERE p_main='".$row['id']."' && rooms=f.rooms && queue=f.queue) AS count_flats
				FROM ".$template."_new_flats AS f
				LEFT JOIN ".$template."_m_catalogue_left AS e
				ON e.id=f.p_main && e.activation='1'
				WHERE f.activation='1' && f.p_main='".$row['id']."'".$searchParamsFlat2."
				GROUP BY f.rooms,f.queue
				ORDER BY f.rooms
			") or die(mysql_error());
			if(mysql_num_rows($flats)>0){
				$flats_list = '<div class="row flats">';
				$n = 1;
				while($flat = mysql_fetch_assoc($flats)){
					$fone = '';
					if($n==1){
						$fone = ' fone';
						$n++;
					}
					else {
						$n=1;
					}
					$name_title = $flat['rooms'].'-комн.';
					if($flat['rooms']==0){
						$name_title = 'Студия';
					}
					
					/*
					*  Поиск минимальной общей площади
					*/
					$queues = '';
					if(!empty($flat['queue'])){
						$queues = " && f.queue='".$flat['queue']."'";
					}
					$full_square_min = 1;
					$full_squares_min = mysql_query("
						SELECT MIN(f.full_square) AS full_square_min
						FROM ".$template."_new_flats AS f
						WHERE f.p_main='".$row['id']."' && f.rooms='".$flat['rooms']."' && f.queue='".$flat['queue']."' && f.activation='1'
					");
					if(mysql_num_rows($full_squares_min)>0){
						$full_square_min = mysql_fetch_assoc($full_squares_min);
						$full_square_min = $full_square_min['full_square_min'];
					}
					// echo $searchParamsFlat2.'<br>';
					/*
					*  Поиск минимальной стоимости
					*/
					$min_price_flat = 0;
					$min_price_flats = mysql_query("
						SELECT MIN(f.price) AS min_price
						FROM ".$template."_new_flats AS f
						WHERE f.p_main='".$row['id']."' && f.rooms='".$flat['rooms']."' && f.queue='".$flat['queue']."' && f.activation='1'
					");
					if(mysql_num_rows($min_price_flats)>0){
						$min_price_flat = mysql_fetch_assoc($min_price_flats);
						$min_price_flat = $min_price_flat['min_price'];
					}
			
					$flats_list .= '<div class="row_flats'.$fone.'">
						<div class="name">'.$name_title.' от <strong>'.str_replace(".", ",", price_cell($full_square_min,1)).' м<sup>2</sup></strong></div>
						<div class="housing"><a href="/newbuilding?housing='.$flat['queue'].'&estate=1&parent='.$row['id'].'">Корпус '.$flat['queue'].'</a></div>
						<div class="price">от <strong>'.price_cell($min_price_flat,0).' руб.</strong></div>
						<div class="count_offers"><a href="/newbuilding?housing='.$flat['queue'].'&rooms='.$flat['rooms'].'&estate=1&parent='.$row['id'].'">'.$flat['count_flats'].' в продаже</a></div>
					</div>';
					$s++;
				}
				$flats_list .= '</div>';
					
				$coordsYandex = '';
				if(empty($row['coords'])){
					$notUpdate = false;
					$params2 = array(
						'geocode' => 'Санкт-Петербург, '.$row['address'],
						'format'  => 'json',
						'results' => 1,
						'key'     => 'AKDIIFYBAAAAqIYtRgIAZYb8P32hIxnbRRSe9CpggONv4PEAAAAAAAAAAADu6wMZJIEJ0SDd0mRe33ag1JRdNA==',
					);
					$response = json_decode(@file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params2)));
					
					if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0){
						$coordsYandex = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
					}
				}
				else {
					$coordsYandex = $row['coords'];
					$notUpdate = true;
				}
				if(!empty($coordsYandex)){
					if(!$notUpdate){
						mysql_query("
							UPDATE ".$template."_m_catalogue_left
							SET coords='".$coordsYandex."'
							WHERE id='".$row['id']."'
						");
					}
					$ex_coords = explode(' ',$coordsYandex);
					$ex_coords2 = "[".$ex_coords[1].",".$ex_coords[0]."]";
					array_push($coordsArray,$ex_coords);
					array_push($coordsArray2,$ex_coords2);
					$arr = array(
						"color_name" => $classRoom[$flat['rooms']],
						"name" => $name_title.' <span><strong>от '.str_replace(".", ",", price_cell($full_square_min,1)).' м²</strong></span>',
						"price" => 'от '.price_cell($min_price_flat,0),
						"floors" => 'этаж '.$flat['floor'].'/'.$row['floors'],
						"link" => '<a target="_blank" href="/newbuilding?housing='.$flat['queue'].'&rooms='.$flat['rooms'].'&estate=1&parent='.$row['id'].'">'.$flat['count_flats'].' в продаже</a>',
						"address" => $row['address'],
						"full_square" => 'от '.str_replace(".", ",", price_cell($full_square_min,1)),
						"rooms" => $flat['rooms'],
					);
					array_push($estateArray,$arr);
				}			
			}
			$exploitation = explode('_',$row['exploitation']);
			$deadline = '';
			if(!empty($row['exploitation'])){
				if(count($exploitation)>0){
					if(count($exploitation)>1){
						$deadline = '<span>'.$exploitation[1].' кв. '.$exploitation[0].'</span>';
					}
					else {
						$deadline = '<span>'.$exploitation[0].'</span>';
					}
				}
			}
			else {
				$queues = mysql_query("
					SELECT commissioning,queue
					FROM ".$template."_queues
					WHERE p_main='".$row['id']."' && type_name='new'
					GROUP BY queue
				");
				if(mysql_num_rows($queues)>0){
					$deadline .= '<span>';
					$deadlineArray = array();
					while($queue = mysql_fetch_assoc($queues)){
						array_push($deadlineArray,$queue['queue'].' очередь - '.$queue['commissioning']);
					}
					$deadline .= $deadlineArray[0];
					// for($q=0; $q<count($deadlineArray); $q++){
						// $deadline .= implode(', ',$deadlineArray);
					// }
					$deadline .= '</span>';
				}
			}

			
			$ex_image = explode(',',$row['images']);
			$image = '<img src="/admin_2/uploads/'.$ex_image[2].'">';
			
			$metro = '';
			if(!empty($row['station'])){
				$metro = '<div class="metro">&laquo;'.$row['station_name'].'&raquo; / <strong>'.$row['distance'].'</strong></div>';
				$dist = floor($row['dist_value']).' м';
				if($dist/1000>=1){
					$dist = price_cell($dist/1000,2).' км';
				}
				$metro = '<div class="metro">&laquo;'.$row['station_name'].'&raquo; / <strong>'.$dist.'</strong></div>';
			}
			
			if($min_full_square==0){
				$min_full_square = 1;
			}
			if($max_full_square==0){
				$max_full_square = 1;
			}
			$square_meter_min = ceil($min_price/$min_full_square);
			$square_meter_max = ceil($max_price/$max_full_square);
			if($square_meter_min>$square_meter_max){
				$square_meter_min2 = $square_meter_min;
				$square_meter_min = $square_meter_max;
				$square_meter_max = $square_meter_min2;
			}

			$type_developer = '';
			$type_deadline = '';
			if(!empty($row['name_developer'])){
				$type_developer = '<div class="type">Застройщик: <a href="'.$link_developer.'">'.$row['name_developer'].'</a></div>';
			}
			if(!empty($deadline)){
				$type_deadline = '<div class="type">Срок сдачи: '.$deadline.'</div>';
			}
		
			$requestSearch .= '<div id="id_'.$row['id'].'" class="item">
				<div class="left_block">
					<div class="title"><a target="_blank" href="'.$link.'">'.htmlspecialchars_decode($row['name']).'</a></div>
					<div class="address">'.$row['address'].'</div>
					'.$metro.'
					<div class="image">
						<a target="_blank" href="'.$link.'">'.$image.'</a>
					</div>
				</div>
				<div class="right_block">
					<div class="row">
						<div class="ceil prices">
							<div class="full_price"><strong>'.price_cell($min_price,0).'</strong><div class="max_price">- '.price_cell($max_price,0).' руб.</div></div>
							<div class="meter_price">'.str_replace(".", ",", price_cell($square_meter_min,0)).' - '.str_replace(".", ",", price_cell($square_meter_max,0)).' руб/м<sup>2</sup></div>
						</div>
						<div class="ceil prices">
							'.$type_developer.'
							'.$type_deadline.'
						</div>
					</div>
					'.$flats_list.'
				</div>
			</div>';
		}

	}
?>
	<div class="estates_block">
		<div class="items">
			<div class="header_block">
				<div class="header_name">Новостройки</div>
				<div class="count_offers"><a href="/search/kupit/novostroyka/all?developer=<?=$PageInfo['developer_id']?>">Всего <?=$max_rows?> ЖК</a></div>
			</div>
			<div class="separate"></div>
			<div class="result_search">
				<div class="list_items">
					<?=$requestSearch?>
					<?=$show_all_new?>
				</div>
			</div>
		</div>
	</div>
</div>
<?}?>