<?
$add_params = '';
$orderByFlats = 'num';
$LOAD_PAGE = 12;

$coordsArray = array();
$coordsArray2 = array();
if(count($params)==1){
	$res = mysql_query("
		SELECT SQL_CALC_FOUND_ROWS s.*,
		(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=s.id && estate='commercials') AS images,
		(SELECT title FROM ".$template."_stations WHERE activation='1' && id=s.station) AS station_name,
		(SELECT name FROM ".$template."_location WHERE activation='1' && id=s.location) AS location_name
		FROM ".$template."_commercials AS s
		WHERE s.activation='1'".$add_params."
		GROUP BY s.id
		ORDER BY ".$orderByFlats."
		LIMIT 0,".$LOAD_PAGE."
	");
	$requestSearch = '';
	if(mysql_num_rows($res)>0){
		$res2 = mysql_query("SELECT FOUND_ROWS()", $db);
		$max_rows = mysql_result($res2, 0);
		while($row = mysql_fetch_assoc($res)){
			$link = '/commercials/'.$row['id'];
			$image = '<span>Нет фото</span>';
			if($row['images'] && !empty($row['images'])){
				$ex_image = explode(',',$row['images']);
				$image = '<img src="/admin_2/uploads/'.$ex_image[2].'">';
			}
			
			$metro = '';
			if(!empty($row['station'])){
				$distance = $row['distance'];
				/*
				*  Расстояние по прямой
				*/
				$dist2 = streightDistance($row['coords'],$row['center_coords']);
				if(!empty($dist2)){
					$distance = $dist2;
				}
				$metro = '<div class="metro">&laquo;'.$row['station_name'].'&raquo; / <strong>'.$distance.'</strong></div>';
			}
			$name_title = $_TYPE_COMMERCIAL_ESTATE[$row['type_commer']].' ';
			
			$square_meter = 0;
			if(!empty($row['full_square'])){
				$square_meter = ceil($row['price']/$row['full_square']);
			}
			$settings = '';
			$lift = 'нет';
			if(!empty($row['lift'])){
				$lift = 'есть';
			}
			$floors_type = '&nbsp;'; //этаж 4/б
			if(!empty($row['floor'])){
				$floors_type = $_FLOOR_COMMERCIAL[$row['floor']];
			}
			// $settings = '<span>Лифт: <i>'.$lift.'</i>,</span><span>Санузел: <i>'.$wc.'</i>,</span><span>Балкон: <i>'.$balcony.'</i>,</span><span>Телефон: <i>'.$phone.'</i>,</span>';
			$requestSearch .= '<div id="id_'.$row['id'].'" class="item">
				<div class="left_block">
					<div class="title"><a href="'.$link.'">'.$name_title.' <span><strong>'.str_replace(".", ",", price_cell($row['full_square'],1)).'/</strong>'.str_replace(".", ",", price_cell($row['live_square'],1)).' м<sup>2</sup></span></a></div>
					<div class="address">'.$row['address'].'</div>
					'.$metro.'
					<div class="image">
						<a href="'.$link.'">'.$image.'</a>
					</div>
				</div>
				<div class="right_block">
					<div class="row">
						<div class="ceil">
							<div class="full_price"><strong>'.price_cell($row['price'],0).'</strong> руб.</div>
							<div class="meter_price">'.price_cell($square_meter,0).' руб/м<sup>2</sup></div>
						</div>
						<div class="ceil">
							<div class="floor">'.$floors_type.'</div>
							<div class="type">Коммерческая</div>
						</div>
					</div>
					<div class="row fone">
						<div class="footage">
							<div class="full">Общая <strong>'.str_replace(".", ",", price_cell($row['full_square'],1)).' м<sup>2</sup></strong></div>
						</div>
						<div class="settings">
							'.$settings.'
							<div class="more"><a href="'.$link.'">подробно</a></div>
						</div>
					</div>
				</div>
			</div>';
			
			// $params = array(
				// 'geocode' => $row['address'], // адрес
				// 'format'  => 'json',                          // формат ответа
				// 'results' => 1,                               // количество выводимых результатов
				// 'key'     => 'AKDIIFYBAAAAqIYtRgIAZYb8P32hIxnbRRSe9CpggONv4PEAAAAAAAAAAADu6wMZJIEJ0SDd0mRe33ag1JRdNA==',                           // ваш api key
			// );
			// $response = json_decode(@file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params)));
			
			// $coordsYandex = '';
			// if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0){
				// $coordsYandex = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
			// }
			// if(!empty($coordsYandex)){
				// $ex_coords = explode(' ',$coordsYandex);
				// $ex_coords2 = "[".$ex_coords[1].",".$ex_coords[0]."]";
				// array_push($coordsArray,$ex_coords);
				// array_push($coordsArray2,$ex_coords2);
			// }
			$title_search_page = 'Квартиры в г. '.$row['location_name'];
			if(isset($rooms_flat_sell_var) || isset($rooms_flat_rent_var)){
				$title_search_page = '';
				if(isset($rooms_flat_sell_var)){
					$roomsArray = $rooms_flat_sell_var;
				}
				if(isset($rooms_flat_rent_var)){
					$roomsArray = $rooms_flat_rent_var;
				}
				$exRoomsArray = explode(',',$roomsArray);
				$roomsArray = array();
				for($c=0; $c<count($exRoomsArray); $c++){
					$roomsName = 'Студии';
					if(!empty($exRoomsArray[$c])){
						$roomsName = $exRoomsArray[$c].'-комн.';
					}
					array_push($roomsArray,$roomsName);
				}
				$title_search_page = implode(', ',$roomsArray).' квартиры в г. '.$row['location_name'];
			}
		}
	}
	?>
	<div id="center">
		<h1 class="inner_pages">Коммерческая недвижимость</h1>
		<div class="sort_offers">
			<div class="count_offers">Подходящих предложений:<span><?=$max_rows?></span></div>
			<div class="sort_block">
				<div class="label_select">Сортировать:</div>
				<div class="select_block price">
					<input type="hidden" name="sort">
					<div class="choosed_block">По умолчанию</div>
					<div class="scrollbar-inner">
						<ul>
							<li class="current"><a href="javascript:void(0)" data-id="1">По умолчанию</a></li>
							<li><a href="javascript:void(0)" data-id="1">По цене</a></li>
							<li><a href="javascript:void(0)" data-id="1">По метражу</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div id="result_search">
			<div class="list_items">
				<?=$requestSearch?>
			</div>
		</div>
		<!--<div id="pager"><ul><li class="prev"><a title="Предыдущая" href="#"><i class="fa fa-angle-left"></i><span>Предыдущая</span></a></li><li><a href="#">1</a></li><li><a href="#">2</a></li><li class=current><span>3</span></li><li><a href="#">4</a></li><li><a href="#">5</a></li><li class="next"><a title="Следующая" href="#"><span>Следующая</span><i class="fa fa-angle-right"></i></a></li></ul></div>-->
	</div>
<?}
if(count($params)==2){?>
<div id="center">
	<div class="title_page new">
		<div class="location_card">
			<h1 class="inner_pages"><?=$PageInfo['h1']?></h1>
			<?
			$address = $PageInfo['address'];
			$addressArray = array();
			if(!empty($PageInfo['short_address'])){
				$address = $PageInfo['short_address'];
			}
			$ex_address = explode(',',$address);
			$addss = '';
			for($a=0; $a<count($ex_address); $a++){
				$m = 0;
				if(count($ex_address)==1){
					$m = -1;
				}
				if(count($ex_address)==2){
					$m = -1;
				}
				if(count($ex_address)==4){
					$m = 0;
				}
				if(count($ex_address)==6){
					$m = 2;
				}
				if($a>$m){
					array_push($addressArray,$ex_address[$a]);
				}
			}
	
			$location_name = '';
			if(!empty($PageInfo['location_name'])){
				$l_name = $PageInfo['location_name'];
				$location_name = $l_name;
				if($l_name == 'Санкт-Петербург'){
					$location_name = 'СПб';
				}
				if($l_name == 'Ленинградская область' || $l_name == 'Ленинградская обл.'){
					$location_name = 'ЛО';
				}
				if($l_name == 'Московская область' || $l_name == 'Московская обл.'){
					$location_name = 'МО';
				}
				if($l_name == 'Москва'){
					$location_name = 'МСК';
				}
			}

			$area_name = '';
			$clearAreaName = str_replace('р-н','',$PageInfo['area_name']);
			$clearAreaName = str_replace('г.','',$clearAreaName);
			$clearAreaName = trim($clearAreaName);
			$pos = strripos($PageInfo['area_name'], 'р-н');
			if($pos===true){
				$area_name = $PageInfo['area_name'].' р-н';
			}
			else {
				$pos = strripos($PageInfo['area_name'], 'г.');
				$area_name = $PageInfo['area_name'];
			}
			
			if(count($addressArray)>0){
				$addss = implode(', ',$addressArray);
				$addss = str_replace('улица','ул.',$addss);
				$addss = str_replace('проспект','пр-т',$addss);
				$addss = str_replace('переулок','пер.',$addss);
				$addss = str_replace($clearAreaName,'',$addss);
				$addss = str_replace($locationName,'',$addss);
			}
			$address2 = '<div class="address">'.$location_name.', '.$area_name.', '.$addss.'</div>';
			echo $address2;

			if(!empty($PageInfo['station'])){
				$v = $PageInfo['dist_value'];
				if($v/1000>=1){
					$v = price_cell($v/1000,2).' км';
				}
				else {
					$v = price_cell($v,0).' м';
				}
				$coordsWay = $PageInfo['center_coords'];
				if(!empty($PageInfo['coords_station'])){
					$coordsWay = $PageInfo['coords_station'];
				}
				$coords = $PageInfo['coords'];
				$ex_coords = explode(',',$coords);
				if(count($ex_coords)>1){
					$coords = $ex_coords[1].' '.$ex_coords[0];
				}
				/*
				*  Расстояние по прямой
				*/
				$dist2 = streightDistance($coordsWay,$coords);
				if(!empty($dist2)){
					$v = $dist2;
				}
				echo '<div class="metro"><a href="/search?type=sell&estate=1&priceAll=all&station='.$PageInfo['station'].'">'.$PageInfo['station_name'].'</a><strong>'.$v.'</strong> | <a class="map" data-link="#map" href="#map">Показать на карте</a></div>';
				// echo '<div class="metro">«'.$PageInfo['station_name'].'» / <strong>'.$PageInfo['distance'].'</strong></div>';
			}
			else {
				
				if(!empty($PageInfo['coords']) && !empty($PageInfo['center_coords'])){
					$coordsWay = $PageInfo['coords_area'];
					if(!empty($PageInfo['coords_station'])){
						$coordsWay = $PageInfo['coords_station'];
					}
					$coords = $PageInfo['coords'];
					$ex_coords = explode(',',$coords);
					if(count($ex_coords)>1){
						$coords = $ex_coords[1].' '.$ex_coords[0];
					}
					/*
					*  Расстояние по прямой
					*/
					$dist2 = @streightDistance($coords,$coordsWay);
					if(!empty($dist2)){
						$dist = $dist2;
					}
					
					$placeCoords = '';
					
					$typeCoords = '';
					$types = mysql_query("
						SELECT type
						FROM ".$template."_areas
						WHERE activation='1' && id='".$PageInfo['area']."' && coords!='' 
						LIMIT 1
					");
					if(mysql_num_rows($types)>0){
						$ts = mysql_fetch_assoc($types);
						$typeCoords = $ts['type'];
					}
					if(!empty($typeCoords)){
						$placeCoords = $typeCoords.' / ';
					}
					echo '<div class="place">'.$placeCoords.'<strong>'.$dist.'</strong> | <a class="map" data-link="#map" href="#map">Показать на карте</a></div>';
				}
			}
			if($PageInfo['type_commer']==6){
				$square_meter = ceil($PageInfo['price']/$PageInfo['area_square']);
			}
			else {
				if(empty($PageInfo['full_square'])){
					$square_meter = 0;
				}
				else {
					$square_meter = ceil($PageInfo['price']/$PageInfo['full_square']);
				}
			}
			$rentText = '';
			$rentArray = array();
			if(!empty($PageInfo['communal'])){
				array_push($rentArray,'включая коммунальные платежи');
			}
			if(!empty($PageInfo['prepayment'])){
				if($PageInfo['type']=='rent'){
					if($PageInfo['prepayment']!=1){
						if($PageInfo['prepayment']<=100){
							array_push($rentArray,'Комиссия '.$PageInfo['prepayment'].'%');
						}
						else {
							array_push($rentArray,'Комиссия '.$PageInfo['prepayment'].' <span class="rub">Р</span>');
						}
					}
				}
			}
			if(!empty($PageInfo['rental_holidays'])){
				array_push($rentArray,'арендные каникулы');
			}
			if(!empty($PageInfo['no_agent'])){
				array_push($rentArray,'агентам не звонить');
			}
			if(count($rentArray)>0){
				$rentText = '<div class="communal_block">'.implode(', ',$rentArray).'</div>';
			}
			
			$meter_price = '';
			if(!empty($square_meter)){
				$meter_price = '<div class="meter_price">'.price_cell($square_meter,0).' <span class="rub">Р</span>/м<sup>2</sup></div>';
				if($PageInfo['type']=='rent'){
					$meter_price = '<div class="meter_price">'.price_cell($square_meter,0).' <span class="rub">Р</span>/м<sup>2</sup> в мес.</div>';
				}
			}
			
			$full_price = '<div class="full_price"><strong>'.price_cell($PageInfo['price'],0).'</strong> <span class="rub">Р</span></div>';
			if($PageInfo['type']=='rent'){
				$full_price = '<div class="full_price"><strong>'.price_cell($PageInfo['price'],0).'</strong> <span class="rub">Р</span>/мес.</div>';
			}
			?>
		</div>
		<div class="price_card">
			<div class="prices">
				<?=$full_price?>
				<?=$meter_price?>
				<?=$rentText?>
			</div>
		</div>
	</div>
	<div class="separate"></div>
	<div class="card_estate new_card" id="housing_estate">
		<?
			$res = mysql_query("
				SELECT *
				FROM ".$template."_photo_catalogue
				WHERE estate='commercial' && p_main='".$PageInfo['id']."'
				ORDER BY cover DESC, num
			");
			if(mysql_num_rows($res)>0){
				echo '<div class="fotogallery">';
				echo '<div class="fotorama fotorama-skin-rsp fotorama-skin-rsp_hidden"
					 data-nav="thumbs"
					 data-height="421"
					 data-keyboard="true"
					 data-navposition="bottom"
					 data-loop="true"
					 data-allowfullscreen="native"
					 data-fit="contain">';
				while($row = mysql_fetch_assoc($res)){
					$ex_images = explode(',',$row['images']);
					if($row['cover']==1){
						$linkImgMid = '/admin_2/uploads/'.$ex_images[3];
						$linkImgMini = '/admin_2/uploads/'.$ex_images[4];
						if($row['user_id']!=0){
							$linkImgMid = '/users/'.$row['user_id'].'/'.$ex_images[3];
							$linkImgMini = '/users/'.$row['user_id'].'/'.$ex_images[4];
							$size = @getimagesize($linkImgMid);
							if(!$size[0]){
								$linkImgMid = '/admin_2/uploads/'.$ex_images[3];
								$linkImgMini = '/admin_2/uploads/'.$ex_images[4];
							}
						}
						echo '<a href="'.$linkImgMini.'" data-thumb="'.$linkImgMid.'" data-full="'.$linkImgMini.'">
							<img src="'.$linkImgMini.'" />
						</a>';
					}
					else {
						$linkImgMid = '/admin_2/uploads/'.$ex_images[3];
						$linkImgMini = '/admin_2/uploads/'.$ex_images[4];
						if($row['user_id']!=0){
							$linkImgMid = '/users/'.$row['user_id'].'/'.$ex_images[3];
							$linkImgMini = '/users/'.$row['user_id'].'/'.$ex_images[4];
							$size = @getimagesize($linkImgMid);
							if(!$size[0]){
								$linkImgMid = '/admin_2/uploads/'.$ex_images[3];
								$linkImgMini = '/admin_2/uploads/'.$ex_images[4];
							}
						}
						echo '<a href="'.$linkImgMini.'" data-thumb="'.$linkImgMid.'" data-full="'.$linkImgMini.'">
							<img src="'.$linkImgMini.'" />
						</a>';
					}
				}
				echo '</div>';
				echo '</div>';
				echo '<script type="text/javascript" src="/libs/fotorama/js/fotorama.js"></script>';
				echo '<script type="text/javascript" src="/libs/fotorama/js/_fotorama_2.js"></script>';
			}
			else {
				echo '<div class="fotogallery">';
				echo '<div class="fotorama fotorama-skin-rsp fotorama-skin-rsp_hidden"
					 data-nav="thumbs"
					 data-height="420"
					 data-keyboard="true"
					 data-navposition="bottom"
					 data-loop="true"
					 data-allowfullscreen="native"
					 data-fit="scaledown">
						<img src="/images/no_photo_650x420.png"/></div>';
				echo '</div>';
				echo '<script type="text/javascript" src="/libs/fotorama/js/fotorama.js"></script>';
				echo '<script type="text/javascript" src="/libs/fotorama/js/_fotorama_2.js"></script>';
			}
			
			/*
			*  Количество комнат
			*/
			$rooms = '';
			if(isset($PageInfo['rooms'])){
				$room = 'Студия';
				if($PageInfo['rooms']>0){
					$room = $PageInfo['rooms'];
				}
				$rooms = '<div class="row">
					<div class="cell label">Количество комнат:</div>
					<div class="cell">'.$room.'</div>
				</div>';
			}
			
			/*
			*  Тип дома
			*/
			$type_house = '';
			if(!empty($PageInfo['type_house'])){
				$r = mysql_query("
					SELECT name
					FROM ".$template."_type_house
					WHERE id='".$PageInfo['type_house']."'
				");
				if(mysql_num_rows($r)>0){
					$rw = mysql_fetch_assoc($r);
					$type_house = '<div class="row">
						<div class="cell label">Тип дома:</div>
						<div class="cell">'.$rw['name'].'</div>
					</div>';
				}
			}
			
			/*
			*  Кол-во этажей
			*/
			$floors = '';
			if(!empty($PageInfo['floor'])){
				if(!empty($PageInfo['floors'])){
					$floors = '<div class="row">
						<div class="cell label">Этаж:</div>
						<div class="cell">'.$PageInfo['floor'].' (из '.$PageInfo['floors'].')</div>
					</div>';
				}
				else {
					$floors = '<div class="row">
						<div class="cell label">Этаж:</div>
						<div class="cell">'.$PageInfo['floor'].'</div>
					</div>';
				}
			}
			
			/*
			*  Высота потолков
			*/
			$ceiling_height = '';
			if(!empty($PageInfo['ceiling_height'])){
				$ceiling_height = '<div class="row">
					<div class="cell label">Высота потолков:</div>
					<div class="cell">'.str_replace(".", ",", $PageInfo['ceiling_height']).' м</div>
				</div>';
			}
			
			/*
			*  Отделка
			*/
			$finishing_value = '<span class="no">нет</span>';
			$finishing = '';
			if(!empty($PageInfo['finishing'])){
				$finishing_value = '<span class="ok">да</span>';
				$finishing = '<div class="row">
					<div class="cell label">Отделка:</div>
					<div class="cell places">'.$finishing_value.'</div>
				</div>';
			}
			
			$paramsBlock = $rooms.$type_house.$floors.$ceiling_height.$finishing;
			$paramsBlock2 = '';

			/*
			*  Офисы
			*/
			if($PageInfo['type_commer']==1){
				/*
				*  Название комплекса
				*/
				$name_build = '';
				if(!empty($PageInfo['show_build'])){
					if(!empty($PageInfo['name_build'])){
						$name_build = '<div class="row">
							<div class="cell label">Название комплекса:</div>
							<div class="cell">'.$PageInfo['name_build'].'</div>
						</div>';
					}
				}
				
				/*
				*  Класс объекта
				*/
				$class_object = '';
				if(!empty($PageInfo['class_object'])){
					$class_object = '<div class="row">
						<div class="cell label">Класс объекта:</div>
						<div class="cell">'.str_replace('.',',',$_CLASS_OBJECTS[$PageInfo['class_object']]).'</div>
					</div>';
				}
				
				/*
				*  Тип здания
				*/
				$type_build = '';
				if(!empty($PageInfo['type_build'])){
					$type_build = '<div class="row">
						<div class="cell label">Тип здания:</div>
						<div class="cell">'.str_replace('.',',',$_TYPE_BUILD_OFFICE[$PageInfo['type_build']]).'</div>
					</div>';
				}
				
				/*
				*  Готовность
				*/
				$readis = '<div class="row">
						<div class="cell label">Готовность:</div>
						<div class="cell">'.str_replace('.',',',$_READIS[$PageInfo['readis']]).'</div>
					</div>';
					
				/*
				*  Вход
				*/
				$entrance = '';
				if(!empty($PageInfo['entrance'])){
					$entrance = '<div class="row">
						<div class="cell label">Вход:</div>
						<div class="cell">'.str_replace('.',',',$_ENTRANCES[$PageInfo['entrance']]).'</div>
					</div>';
				}
				
				$paramsBlock = $name_build.$class_object.$type_build.$readis.$entrance;
				
				/*
				*  Общая площадь
				*/
				$full_square = '';
				if(!empty($PageInfo['full_square'])){
					$full_square = '<div class="row">
						<div class="cell label">Общая площадь:</div>
						<div class="cell">'.$PageInfo['full_square'].' м²</div>
					</div>';
				}
				/*
				*  Площадь помещений
				*/
				$rooms_square = '';
				if(!empty($PageInfo['rooms_square'])){
					$rooms_square = '<div class="row">
						<div class="cell label">Площадь помещений:</div>
						<div class="cell">'.$PageInfo['rooms_square'].' м²</div>
					</div>';
				}
				/*
				*  Высота потолков
				*/
				$ceiling_height = '';
				if(!empty($PageInfo['ceiling_height'])){
					$ceiling_height = '<div class="row">
						<div class="cell label">Высота потолков:</div>
						<div class="cell">'.str_replace(".", ",", $PageInfo['ceiling_height']).' м</div>
					</div>';
				}
				$floorsType = '';
				$floorsArray = array();
				if(!empty($PageInfo['floor'])){
					$floorsArray = array($PageInfo['floor']);
					$floorsType = $PageInfo['floor'];
				}
				if(count($floorsArray)>0){
					array_push($floorsArray,'/'.$PageInfo['floors']);
					$floorsType = implode('',$floorsArray);
					$floorsType = '<div class="row">
						<div class="cell label">Этаж:</div>
						<div class="cell">'.$floorsType.'</div>
					</div>';
				}
				if(!empty($PageInfo['basement']) || !empty($PageInfo['semibasement'])){
					$floorsArray = array();
					if(!empty($PageInfo['basement'])){
						array_push($floorsArray,'Подвал');
					}
					if(!empty($PageInfo['semibasement'])){
						array_push($floorsArray,'Полуподвал');
					}
					if(count($floorsArray)>0){
						$floorsType = implode(', ',$floorsArray);
					}
					$floorsType = '<div class="row">
						<div class="cell label">Этаж:</div>
						<div class="cell">'.$floorsType.'</div>
					</div>';
				}
				/*
				*  Состояние
				*/
				$decoration = '';
				if(!empty($PageInfo['decoration'])){
					$decoration = '<div class="row">
						<div class="cell label">Состояние:</div>
						<div class="cell">'.$_DECORATIONS[$PageInfo['decoration']].'</div>
					</div>';
				}
				/*
				*  Мебель
				*/
				$furniture = '';
				if(!empty($PageInfo['furniture'])){
					$furniture = '<div class="row">
						<div class="cell label">Мебель:</div>
						<div class="cell">'.$_FURNITURE[$PageInfo['furniture']].'</div>
					</div>';
				}
				
				/*
				*  Вход
				*/
				$entrance = '';
				if(!empty($PageInfo['entrance'])){
					$entrance = '<div class="row">
						<div class="cell label">Вход:</div>
						<div class="cell">'.$_ENTRANCES[$PageInfo['entrance']].'</div>
					</div>';
				}
				
				/*
				*  Парковка
				*/
				$parking_value = '';
				if(!empty($PageInfo['parking_value'])){
					$parking_value = '<div class="row">
						<div class="cell label">Парковка:</div>
						<div class="cell">'.$_PARKING[$PageInfo['parking_value']].'</div>
					</div>';
				}
				
				/*
				*  Банкоматы
				*/
				$atms = '';
				if(!empty($PageInfo['atms'])){
					$atms = '<div class="row">
						<div class="cell label">Банкоматы:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Кафе
				*/
				$cafe = '';
				if(!empty($PageInfo['cafe'])){
					$cafe = '<div class="row">
						<div class="cell label">Кафе:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Конференц-залы
				*/
				$confer = '';
				if(!empty($PageInfo['confer'])){
					$confer = '<div class="row">
						<div class="cell label">Конференц-залы:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Столовая
				*/
				$canteen = '';
				if(!empty($PageInfo['canteen'])){
					$canteen = '<div class="row">
						<div class="cell label">Столовая:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				$paramsBlock2 = $full_square.$rooms_square.$ceiling_height.$floorsType.$decoration.$furniture.$entrance.$parking_value.$atms.$cafe.$confer.$canteen;
			}
			
			/*
			*  Торговля, общепит, услуги
			*/
			if($PageInfo['type_commer']==2){
				/*
				*  Класс объекта
				*/
				$class_object = '';
				if(!empty($PageInfo['class_object'])){
					$class_object = '<div class="row">
						<div class="cell label">Класс объекта:</div>
						<div class="cell">'.str_replace('.',',',$_CLASS_OBJECTS[$PageInfo['class_object']]).'</div>
					</div>';
				}
				
				/*
				*  Тип здания
				*/
				$type_build = '';
				if(!empty($PageInfo['type_build'])){
					$type_build = '<div class="row">
						<div class="cell label">Тип здания:</div>
						<div class="cell">'.str_replace('.',',',$_TYPE_BUILD_OFFICE[$PageInfo['type_build']]).'</div>
					</div>';
				}
				
				/*
				*  Готовность
				*/
				$readis = '<div class="row">
						<div class="cell label">Готовность:</div>
						<div class="cell">'.str_replace('.',',',$_READIS[$PageInfo['readis']]).'</div>
					</div>';

				$paramsBlock = $class_object.$type_build.$readis;
				
				/*
				*  Общая площадь
				*/
				$full_square = '';
				if(!empty($PageInfo['full_square'])){
					$full_square = '<div class="row">
						<div class="cell label">Общая площадь:</div>
						<div class="cell">'.$PageInfo['full_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Площадь помещений
				*/
				$rooms_square = '';
				if(!empty($PageInfo['rooms_square'])){
					$rooms_square = '<div class="row">
						<div class="cell label">Площадь помещений:</div>
						<div class="cell">'.$PageInfo['rooms_square'].' м²</div>
					</div>';
				}
				/*
				*  Высота потолков
				*/
				$ceiling_height = '';
				if(!empty($PageInfo['ceiling_height'])){
					$ceiling_height = '<div class="row">
						<div class="cell label">Высота потолков:</div>
						<div class="cell">'.str_replace(".", ",", $PageInfo['ceiling_height']).' м</div>
					</div>';
				}
				$floorsType = '';
				$floorsArray = array();
				if(!empty($PageInfo['floor'])){
					$floorsArray = array($PageInfo['floor']);
					$floorsType = $PageInfo['floor'];
				}
				if(count($floorsArray)>0){
					array_push($floorsArray,'/'.$PageInfo['floors']);
					$floorsType = implode('',$floorsArray);
					$floorsType = '<div class="row">
						<div class="cell label">Этаж:</div>
						<div class="cell">'.$floorsType.'</div>
					</div>';
				}
				if(!empty($PageInfo['basement']) || !empty($PageInfo['semibasement'])){
					$floorsArray = array();
					if(!empty($PageInfo['basement'])){
						array_push($floorsArray,'Подвал');
					}
					if(!empty($PageInfo['semibasement'])){
						array_push($floorsArray,'Полуподвал');
					}
					if(count($floorsArray)>0){
						$floorsType = implode(', ',$floorsArray);
					}
					$floorsType = '<div class="row">
						<div class="cell label">Этаж:</div>
						<div class="cell">'.$floorsType.'</div>
					</div>';
				}
				/*
				*  Состояние
				*/
				$conditions = '';
				if(!empty($PageInfo['conditions'])){
					$conditions = '<div class="row">
						<div class="cell label">Состояние:</div>
						<div class="cell">'.$_CONDITIONS_SKLAD[$PageInfo['conditions']].'</div>
					</div>';
				}
				/*
				*  Мебель
				*/
				$furniture = '';
				if(!empty($PageInfo['furniture'])){
					$furniture = '<div class="row">
						<div class="cell label">Мебель:</div>
						<div class="cell">'.$_FURNITURE[$PageInfo['furniture']].'</div>
					</div>';
				}
				/*
				*  Вход
				*/
				$entrance = '';
				if(!empty($PageInfo['entrance'])){
					$entrance = '<div class="row">
						<div class="cell label">Вход:</div>
						<div class="cell">'.str_replace('.',',',$_ENTRANCES[$PageInfo['entrance']]).'</div>
					</div>';
				}
				/*
				*  Электроснабжение
				*/
				$power_max = '';
				if(!empty($PageInfo['power_max'])){
					$power_max = '<div class="row">
						<div class="cell label">Электромощность:</div>
						<div class="cell">'.$PageInfo['power_max'].' кВт</div>
					</div>';
				}
				$paramsBlock2 = $full_square.$rooms_square.$ceiling_height.$floorsType.$conditions.$furniture.$entrance.$power_max;
			}
			
			/*
			*  Мойка, Автосервис
			*/
			if($PageInfo['type_commer']==3){
				$arrTypes = array();
				if(!empty($PageInfo['wash'])){
					array_push($arrTypes,'Автомойка');
				}
				if(!empty($PageInfo['autoservice'])){
					array_push($arrTypes,'Автосервис');
				}
				
				/*
				*  Площадь
				*/
				$full_square = '';
				if(!empty($PageInfo['full_square'])){
					$full_square = '<div class="row">
						<div class="cell label">Площадь:</div>
						<div class="cell">'.$PageInfo['full_square'].' м²</div>
					</div>';
				}
				
				$floorsType = '';
				$floorsArray = array();
				if(!empty($PageInfo['floor'])){
					$floorsArray = array($PageInfo['floor']);
					$floorsType = $PageInfo['floor'];
				}
				if(count($floorsArray)>0){
					array_push($floorsArray,'/'.$PageInfo['floors']);
					$floorsType = implode('',$floorsArray);
					$floorsType = '<div class="row">
						<div class="cell label">Этаж:</div>
						<div class="cell">'.$floorsType.'</div>
					</div>';
				}
				if(!empty($PageInfo['basement']) || !empty($PageInfo['semibasement'])){
					$floorsArray = array();
					if(!empty($PageInfo['basement'])){
						array_push($floorsArray,'Подвал');
					}
					if(!empty($PageInfo['semibasement'])){
						array_push($floorsArray,'Полуподвал');
					}
					if(count($floorsArray)>0){
						$floorsType = implode(', ',$floorsArray);
					}
					$floorsType = '<div class="row">
						<div class="cell label">Этаж:</div>
						<div class="cell">'.$floorsType.'</div>
					</div>';
				}
				
				/*
				*  Состояние
				*/
				$conditions = '';
				if(!empty($PageInfo['conditions'])){
					$conditions = '<div class="row">
						<div class="cell label">Состояние:</div>
						<div class="cell">'.$_CONDITIONS_FRESH[$PageInfo['conditions']].'</div>
					</div>';
				}
				
				$paramsBlock = $full_square.$floorsType.$conditions;
				
				/*
				*  Тип здания
				*/
				$type_build = '';
				if(!empty($PageInfo['type_build'])){
					$type_build = '<div class="row">
						<div class="cell label">Тип здания:</div>
						<div class="cell">'.str_replace('.',',',$_TYPE_BUILD_FRESH[$PageInfo['type_build']]).'</div>
					</div>';
				}
				/*
				*  Год постройки
				*/
				$yearBuild = '';
				if(!empty($PageInfo['year'])){
					$yearBuild = '<div class="row">
						<div class="cell label">Год постройки:</div>
						<div class="cell">'.$PageInfo['year'].'</div>
					</div>';
				}
				
				/*
				*  Земельный участок
				*/
				$area_square = '';
				if(!empty($PageInfo['area_square'])){
					$area_square = '<div class="row">
						<div class="cell label">Земельный участок:</div>
						<div class="cell">'.$PageInfo['area_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Электроснабжение
				*/
				$power_max = '';
				if(!empty($PageInfo['power_max'])){
					$power_max = '<div class="row">
						<div class="cell label">Электроснабжение:</div>
						<div class="cell">'.$PageInfo['power_max'].' кВт</div>
					</div>';
				}
				
				/*
				*  Водоснабжение
				*/
				$plumbing = '';
				if(!empty($PageInfo['plumbing'])){
					$plumbing = '<div class="row">
						<div class="cell label">Водоснабжение:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Теплоснабжение
				*/
				$heating = '';
				if(!empty($PageInfo['heating'])){
					$heating = '<div class="row">
						<div class="cell label">Теплоснабжение:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Канализация
				*/
				$sewerage = '';
				if(!empty($PageInfo['sewerage'])){
					$sewerage = '<div class="row">
						<div class="cell label">Канализация:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Газоснабжение
				*/
				$gas = '';
				if(!empty($PageInfo['gas'])){
					$gas = '<div class="row">
						<div class="cell label">Газоснабжение:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Кафе
				*/
				$cafe = '';
				if(!empty($PageInfo['cafe'])){
					$cafe = '<div class="row">
						<div class="cell label">Кафе:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Гостиница
				*/
				$hotel = '';
				if(!empty($PageInfo['hotel'])){
					$hotel = '<div class="row">
						<div class="cell label">Кафе:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Столовая
				*/
				$canteen = '';
				if(!empty($PageInfo['canteen'])){
					$canteen = '<div class="row">
						<div class="cell label">Столовая:</div>
						<div class="cell">Да</div>
					</div>';
				}
				/*
				*  Офисные помещения
				*/
				$office_rooms = '';
				if(!empty($PageInfo['office_rooms'])){
					$office_rooms = '<div class="row">
						<div class="cell label">Офисные помещения:</div>
						<div class="cell">Да</div>
					</div>';
				}
				$paramsBlock2 = $type_build.$yearBuild.$area_square.$power_max.$plumbing.$heating.$sewerage.$gas.$cafe.$hotel.$canteen.$office_rooms;
			}
			
			/*
			*  ОСЗ, БЦ, ТРК
			*/
			if($PageInfo['type_commer']==4){
				/*
				*  Тип здания
				*/
				$type_build = '';
				if(!empty($PageInfo['type_build'])){
					$type_build = '<div class="row">
						<div class="cell label">Тип здания:</div>
						<div class="cell">'.str_replace('.',',',$_TYPE_BUILD_OFFICE[$PageInfo['type_build']]).'</div>
					</div>';
				}
				/*
				*  Класс объекта
				*/
				$class_object = '';
				if(!empty($PageInfo['class_object'])){
					$class_object = '<div class="row">
						<div class="cell label">Класс объекта:</div>
						<div class="cell">'.str_replace('.',',',$_CLASS_OBJECTS[$PageInfo['class_object']]).'</div>
					</div>';
				}
				/*
				*  Год постройки
				*/
				$yearBuild = '';
				if(!empty($PageInfo['year'])){
					$yearBuild = '<div class="row">
						<div class="cell label">Год постройки:</div>
						<div class="cell">'.$PageInfo['year'].'</div>
					</div>';
				}
				
				/*
				*  Площадь здания
				*/
				$full_square = '';
				if(!empty($PageInfo['full_square'])){
					$full_square = '<div class="row">
						<div class="cell label">Площадь здания:</div>
						<div class="cell">'.$PageInfo['full_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Земельный участок
				*/
				$area_square = '';
				if(!empty($PageInfo['area_square'])){
					$area_square = '<div class="row">
						<div class="cell label">Земельный участок:</div>
						<div class="cell">'.$PageInfo['area_square'].' м²</div>
					</div>';
				}
				
				$paramsBlock = $type_build.$class_object.$yearBuild;
				
				/*
				*  Высота потолков
				*/
				$ceiling_height = '';
				if(!empty($PageInfo['ceiling_height'])){
					$ceiling_height = '<div class="row">
						<div class="cell label">Высота потолков:</div>
						<div class="cell">'.str_replace(".", ",", $PageInfo['ceiling_height']).' м</div>
					</div>';
				}
				$floorsType = '';
				if(!empty($PageInfo['floors'])){
					$floorsType = '<div class="row">
						<div class="cell label">Этажей:</div>
						<div class="cell">'.$PageInfo['floors'].'</div>
					</div>';
				}

				/*
				*  Состояние
				*/
				$conditions = '';
				if(!empty($PageInfo['conditions'])){
					$conditions = '<div class="row">
						<div class="cell label">Состояние:</div>
						<div class="cell">'.$_CONDITIONS_FRESH[$PageInfo['conditions']].'</div>
					</div>';
				}
				
				/*
				*  Вход
				*/
				$entrance = '';
				if(!empty($PageInfo['entrance'])){
					$entrance = '<div class="row">
						<div class="cell label">Вход:</div>
						<div class="cell">'.str_replace('.',',',$_ENTRANCES[$PageInfo['entrance']]).'</div>
					</div>';
				}
				
				/*
				*  Готовность
				*/
				$readis = '<div class="row">
						<div class="cell label">Готовность:</div>
						<div class="cell">'.str_replace('.',',',$_READIS[$PageInfo['readis']]).'</div>
					</div>';
					
				/*
				*  Парковка
				*/
				$parking_value = '';
				if(!empty($PageInfo['parking_value'])){
					$parking_value = '<div class="row">
						<div class="cell label">Парковка:</div>
						<div class="cell">'.$_PARKING[$PageInfo['parking_value']].'</div>
					</div>';
				}
				/*
				*  Право на ЗУ
				*/
				$rights = '';
				if(!empty($PageInfo['rights'])){
					$rights = '<div class="row">
						<div class="cell label">Право на ЗУ:</div>
						<div class="cell">'.$_RIGHTS[$PageInfo['rights']].'</div>
					</div>';
				}
				
				/*
				*  Водоснабжение
				*/
				$plumbing = '';
				if(!empty($PageInfo['plumbing'])){
					$plumbing = '<div class="row">
						<div class="cell label">Водоснабжение:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Теплоснабжение
				*/
				$heating = '';
				if(!empty($PageInfo['heating'])){
					$heating = '<div class="row">
						<div class="cell label">Теплоснабжение:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Канализация
				*/
				$sewerage = '';
				if(!empty($PageInfo['sewerage'])){
					$sewerage = '<div class="row">
						<div class="cell label">Канализация:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Газоснабжение
				*/
				$gas = '';
				if(!empty($PageInfo['gas'])){
					$gas = '<div class="row">
						<div class="cell label">Газоснабжение:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Наличие Ж/д тупика
				*/
				$deadlock = '';
				if(!empty($PageInfo['deadlock'])){
					$deadlock = '<div class="row">
						<div class="cell label">Наличие Ж/д тупика:</div>
						<div class="cell">Да</div>
					</div>';
				}
				$paramsBlock2 = $full_square.$floorsType.$conditions.$entrance.$readis.$parking_value.$area_square.$rights.$plumbing.$heating.$sewerage.$gas.$deadlock;
			}
						
			/*
			*  Торговля, общепит, услуги
			*/
			if($PageInfo['type_commer']==5){
				/*
				*  Класс объекта
				*/
				$class_object = '';
				if(!empty($PageInfo['class_object'])){
					$class_object = '<div class="row">
						<div class="cell label">Класс объекта:</div>
						<div class="cell">'.$_CLASS_OBJECTS[$PageInfo['class_object']].'</div>
					</div>';
				}
				/*
				*  Тип здания
				*/
				$type_build = '';
				if(!empty($PageInfo['type_build'])){
					$type_build = '<div class="row">
						<div class="cell label">Тип здания:</div>
						<div class="cell">'.$_TYPE_BUILD_FRESH[$PageInfo['type_build']].'</div>
					</div>';
				}
				/*
				*  Шаг колонн
				*/
				$column_space = '';
				if(!empty($PageInfo['column_space'])){
					$column_space = '<div class="row">
						<div class="cell label">Шаг колонн:</div>
						<div class="cell">'.$_COLUMN_SPACE[$PageInfo['column_space']].'</div>
					</div>';
				}
				/*
				*  Количество ворот
				*/
				$gates = '';
				if(!empty($PageInfo['gates'])){
					$gates = '<div class="row">
						<div class="cell label">Количество ворот:</div>
						<div class="cell">'.$PageInfo['gates'].'</div>
					</div>';
				}
				/*
				*  Состояние
				*/
				$conditions = '';
				if(!empty($PageInfo['conditions'])){
					$conditions = '<div class="row">
						<div class="cell label">Состояние:</div>
						<div class="cell">'.$_CONDITIONS_SKLAD[$PageInfo['conditions']].'</div>
					</div>';
				}
				/*
				*  Кран-балка
				*/
				$cathead = '';
				if(!empty($PageInfo['cathead'])){
					$cathead = '<div class="row">
						<div class="cell label">Кран-балка:</div>
						<div class="cell">'.$PageInfo['cathead'].' кг</div>
					</div>';
				}
				/*
				*  Тельфер
				*/
				$telpher = '';
				if(!empty($PageInfo['telpher'])){
					$telpher = '<div class="row">
						<div class="cell label">Тельфер:</div>
						<div class="cell">'.$PageInfo['telpher'].' кг</div>
					</div>';
				}
				/*
				*  Лифт
				*/
				$lift_massa = '';
				if(!empty($PageInfo['lift_massa'])){
					$lift_massa = '<div class="row">
						<div class="cell label">Лифт:</div>
						<div class="cell">'.$PageInfo['lift_massa'].' кг</div>
					</div>';
				}

				$paramsBlock = $class_object.$type_build.$column_space.$gates.$conditions.$cathead.$telpher.$lift_massa;
				/*
				*  Общая площадь
				*/
				$full_square = '';
				if(!empty($PageInfo['full_square'])){
					$full_square = '<div class="row">
						<div class="cell label">Общая площадь:</div>
						<div class="cell">'.$PageInfo['full_square'].' м²</div>
					</div>';
				}
				/*
				*  Право на ЗУ
				*/
				$rights = '';
				if(!empty($PageInfo['rights'])){
					$rights = '<div class="row">
						<div class="cell label">Право на ЗУ:</div>
						<div class="cell">'.$_RIGHTS[$PageInfo['rights']].'</div>
					</div>';
				}
				/*
				*  Площадь зем. участка
				*/
				$area_square = '';
				if(!empty($PageInfo['area_square'])){
					$area_square = '<div class="row">
						<div class="cell label">Площадь ЗУ:</div>
						<div class="cell">'.$PageInfo['area_square'].' м²</div>
					</div>';
				}
				/*
				*  Площадь помещений
				*/
				$rooms_square = '';
				if(!empty($PageInfo['rooms_square'])){
					$rooms_square = '<div class="row">
						<div class="cell label">Площадь помещений:</div>
						<div class="cell">'.$PageInfo['rooms_square'].' м²</div>
					</div>';
				}
				/*
				*  Высота потолков
				*/
				$ceiling_height = '';
				if(!empty($PageInfo['ceiling_height'])){
					$ceiling_height = '<div class="row">
						<div class="cell label">Высота потолков:</div>
						<div class="cell">'.str_replace(".", ",", $PageInfo['ceiling_height']).' м</div>
					</div>';
				}
				$floorsType = '';
				$floorsArray = array();
				if(!empty($PageInfo['floor'])){
					$floorsArray = array($PageInfo['floor']);
					$floorsType = $PageInfo['floor'];
				}
				if(count($floorsArray)>0){
					array_push($floorsArray,'/'.$PageInfo['floors']);
					$floorsType = implode('',$floorsArray);
					$floorsType = '<div class="row">
						<div class="cell label">Этаж:</div>
						<div class="cell">'.$floorsType.'</div>
					</div>';
				}
				if(!empty($PageInfo['basement']) || !empty($PageInfo['semibasement'])){
					$floorsArray = array();
					if(!empty($PageInfo['basement'])){
						array_push($floorsArray,'Подвал');
					}
					if(!empty($PageInfo['semibasement'])){
						array_push($floorsArray,'Полуподвал');
					}
					if(count($floorsArray)>0){
						$floorsType = implode(', ',$floorsArray);
					}
					$floorsType = '<div class="row">
						<div class="cell label">Этаж:</div>
						<div class="cell">'.$floorsType.'</div>
					</div>';
				}
				/*
				*  Мебель
				*/
				$furniture = '';
				if(!empty($PageInfo['furniture'])){
					$furniture = '<div class="row">
						<div class="cell label">Мебель:</div>
						<div class="cell">'.$_FURNITURE[$PageInfo['furniture']].'</div>
					</div>';
				}
				/*
				*  Вход
				*/
				$entrance = '';
				if(!empty($PageInfo['entrance'])){
					$entrance = '<div class="row">
						<div class="cell label">Вход:</div>
						<div class="cell">'.str_replace('.',',',$_ENTRANCES[$PageInfo['entrance']]).'</div>
					</div>';
				}
				/*
				*  Электроснабжение
				*/
				$power_max = '';
				if(!empty($PageInfo['power_max'])){
					$power_max = '<div class="row">
						<div class="cell label">Электроснабжение:</div>
						<div class="cell">'.$PageInfo['power_max'].' кВт</div>
					</div>';
				}
				/*
				*  Водоснабжение
				*/
				$plumbing = '';
				if(!empty($PageInfo['plumbing'])){
					$plumbing = '<div class="row">
						<div class="cell label">Водоснабжение:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Теплоснабжение
				*/
				$heating = '';
				if(!empty($PageInfo['heating'])){
					$heating = '<div class="row">
						<div class="cell label">Теплоснабжение:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Канализация
				*/
				$sewerage = '';
				if(!empty($PageInfo['sewerage'])){
					$sewerage = '<div class="row">
						<div class="cell label">Канализация:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Газоснабжение
				*/
				$gas = '';
				if(!empty($PageInfo['gas'])){
					$gas = '<div class="row">
						<div class="cell label">Газоснабжение:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Наличие Ж/д тупика
				*/
				$deadlock = '';
				if(!empty($PageInfo['deadlock'])){
					$deadlock = '<div class="row">
						<div class="cell label">Наличие Ж/д тупика:</div>
						<div class="cell">Да</div>
					</div>';
				}
				$paramsBlock2 = $full_square.$area_square.$rights.$rooms_square.$ceiling_height.$floorsType.$furniture.$entrance.$power_max.$plumbing.$heating.$sewerage.$gas.$deadlock;
			}
			
			/*
			*  Земельный участок
			*/
			if($PageInfo['type_commer']==6){
				/*
				*  Площадь ЗУ
				*/
				$area_square = '';
				if(!empty($PageInfo['area_square'])){
					$area_square = '<div class="row">
						<div class="cell label">Площадь ЗУ:</div>
						<div class="cell">'.str_replace('.',',',$PageInfo['area_square']).' м²</div>
					</div>';
				}
				/*
				*  Право на ЗУ
				*/
				$rights = '';
				if(!empty($PageInfo['rights'])){
					$rights = '<div class="row">
						<div class="cell label">Право на ЗУ:</div>
						<div class="cell">'.$_RIGHTS[$PageInfo['rights']].'</div>
					</div>';
				}
				/*
				*  Категория ЗУ
				*/
				$category = '';
				if(!empty($PageInfo['category'])){
					$category = '<div class="row">
						<div class="cell label">Категория ЗУ:</div>
						<div class="cell">'.$_CATEGORY_COMM_COUNTRY[$PageInfo['category']].'</div>
					</div>';
				}
				/*
				*  Санитарная классификация
				*/
				$sanitary = '';
				if(!empty($PageInfo['sanitary'])){
					$sanitary = '<div class="row">
						<div class="cell label">Санитарная классификация:</div>
						<div class="cell">'.$_SANITARY[$PageInfo['sanitary']].'</div>
					</div>';
				}
				/*
				*  Зона согласно ППЗ
				*/
				$ppz = '';
				if(!empty($PageInfo['ppz'])){
					$ppz = '<div class="row">
						<div class="cell label">Зона согласно ППЗ:</div>
						<div class="cell">'.$PageInfo['ppz'].'</div>
					</div>';
				}
				/*
				*  Разрешенное использование
				*/
				$used = '';
				if(!empty($PageInfo['used'])){
					$used = '<div class="row">
						<div class="cell label">Разрешенное использование:</div>
						<div class="cell">'.$PageInfo['used'].'</div>
					</div>';
				}
				
				$paramsBlock = $area_square.$rights.$category.$sanitary.$ppz.$used;
				
				/*
				*  Состояние
				*/
				$conditions = '';
				if(!empty($PageInfo['conditions'])){
					$conditions = '<div class="row">
						<div class="cell label">Состояние:</div>
						<div class="cell">'.$_CONDITIONS[$PageInfo['conditions']].'</div>
					</div>';
				}
				
				/*
				*  Подъезд авто
				*/
				$auto_staircase = '';
				if(!empty($PageInfo['auto_staircase'])){
					$auto_staircase = '<div class="row">
						<div class="cell label">Подъезд авто:</div>
						<div class="cell">'.$_AUTO_STAIRCASE[$PageInfo['auto_staircase']].'</div>
					</div>';
				}
				
				/*
				*  Подъезд ж/д
				*/
				$station_staircase = '';
				if(!empty($PageInfo['station_staircase'])){
					$station_staircase = '<div class="row">
						<div class="cell label">Подъезд ж/д:</div>
						<div class="cell">'.$_STATION_STAIRCASE[$PageInfo['station_staircase']].'</div>
					</div>';
				}
				
				/*
				*  Ограждение
				*/
				$fencing = '<div class="row">
						<div class="cell label">Ограждение:</div>
						<div class="cell">Нет</div>
					</div>';
				if(!empty($PageInfo['fencing'])){
					$fencing = '<div class="row">
						<div class="cell label">Ограждение:</div>
						<div class="cell">Да</div>
					</div>';
				}
					
				/*
				*  Наличие обременений
				*/
				$encumbrances = '';
				if(!empty($PageInfo['encumbrances'])){
					$encumbrances = '<div class="row">
						<div class="cell label">Наличие обременений:</div>
						<div class="cell">Да</div>
					</div>';
				}
			
				/*
				*  Площадь строений на ЗУ
				*/
				$building_area = '';
				if(!empty($PageInfo['building_area'])){
					$building_area = '<div class="row">
						<div class="cell label">Площадь строений на ЗУ:</div>
						<div class="cell">'.$PageInfo['building_area'].' м²</div>
					</div>';
				}
				
				/*
				*  Электроснабжение
				*/
				$power_max = '';
				if(!empty($PageInfo['power_max'])){
					$power_max = '<div class="row">
						<div class="cell label">Электроснабжение:</div>
						<div class="cell">'.$PageInfo['power_max'].' кВт</div>
					</div>';
				}
				/*
				*  Водоснабжение
				*/
				$plumbing = '';
				if(!empty($PageInfo['plumbing'])){
					$plumbing = '<div class="row">
						<div class="cell label">Водоснабжение:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Теплоснабжение
				*/
				$heating = '';
				if(!empty($PageInfo['heating'])){
					$heating = '<div class="row">
						<div class="cell label">Теплоснабжение:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Канализация
				*/
				$sewerage = '';
				if(!empty($PageInfo['sewerage'])){
					$sewerage = '<div class="row">
						<div class="cell label">Канализация:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Газоснабжение
				*/
				$gas = '';
				if(!empty($PageInfo['gas'])){
					$gas = '<div class="row">
						<div class="cell label">Газоснабжение:</div>
						<div class="cell">Да</div>
					</div>';
				}
				$paramsBlock2 = $conditions.$auto_staircase.$station_staircase.$fencing.$encumbrances.$building_area.$plumbing.$heating.$sewerage.$gas.$power_max;
			}

			/*
			*  Готовый бизнес
			*/
			if($PageInfo['type_commer']==7){
				/*
				*  Тип бизнеса
				*/
				$type_business = '';
				if(!empty($PageInfo['type_business'])){
					$franchising = '';
					if(!empty($PageInfo['franchising'])){
						$franchising = ' (франчайзинг)';
					}
					$type_business = '<div class="row">
						<div class="cell label">Тип бизнеса:</div>
						<div class="cell">'.$_READY_BUSINESS[$PageInfo['type_business']].$franchising.'</div>
					</div>';
				}
				
				/*
				*  Общая площадь
				*/
				$full_square = '';
				if(!empty($PageInfo['full_square'])){
					$full_square = '<div class="row">
						<div class="cell label">Общая площадь:</div>
						<div class="cell">'.$PageInfo['full_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Земельный участок
				*/
				$area_square = '';
				if(!empty($PageInfo['area_square'])){
					$area_square = '<div class="row">
						<div class="cell label">Площадь <br>земельного участка:</div>
						<div class="cell">'.$PageInfo['area_square'].' м²</div>
					</div>';
				}
				
				$paramsBlock = $type_business.$full_square.$area_square;
				
				$floorsType = '';
				if(!empty($PageInfo['floor'])){
					$basement = '';
					if(!empty($PageInfo['basement'])){
						$basement = ' подвал/полуподвал';
					}
					$floorsType = '<div class="row">
						<div class="cell label">Этаж:</div>
						<div class="cell">'.$PageInfo['floor'].$basement.'</div>
					</div>';
				}

				/*
				*  Право на помещение
				*/
				$right_build = '';
				if(!empty($PageInfo['right_build'])){
					$right_build = '<div class="row">
						<div class="cell label">Право на помещение:</div>
						<div class="cell">'.$_RIGHTS[$PageInfo['right_build']].'</div>
					</div>';
				}

				/*
				*  Состояние
				*/
				$conditions = '';
				if(!empty($PageInfo['conditions'])){
					$conditions = '<div class="row">
						<div class="cell label">Состояние:</div>
						<div class="cell">'.$_CONDITIONS_FRESH[$PageInfo['conditions']].'</div>
					</div>';
				}

				/*
				*  Электроснабжение
				*/
				$power_max = '';
				if(!empty($PageInfo['power_max'])){
					$power_max = '<div class="row">
						<div class="cell label">Электроснабжение:</div>
						<div class="cell">'.$PageInfo['power_max'].' кВт</div>
					</div>';
				}

				/*
				*  Кадастровый номер
				*/
				$cadastr = '';
				if(!empty($PageInfo['cadastr'])){
					$cadastr = '<div class="row">
						<div class="cell label">Кадастровый номер:</div>
						<div class="cell">'.$PageInfo['cadastr'].'</div>
					</div>';
				}

				/*
				*  Право на ЗУ
				*/
				$rights = '';
				if(!empty($PageInfo['rights'])){
					$rights = '<div class="row">
						<div class="cell label">Право на ЗУ:</div>
						<div class="cell">'.$_RIGHTS[$PageInfo['rights']].'</div>
					</div>';
				}

				/*
				*  Категория ЗУ
				*/
				$category = '';
				if(!empty($PageInfo['category'])){
					$category = '<div class="row">
						<div class="cell label">Категория ЗУ:</div>
						<div class="cell">'.$_CATEGORY_COUNTRY[$PageInfo['category']].'</div>
					</div>';
				}

				/*
				*  Разрешенное использование
				*/
				$used = '';
				if(!empty($PageInfo['used'])){
					$used = '<div class="row">
						<div class="cell label">Разрешенное использование:</div>
						<div class="cell">'.$PageInfo['used'].'</div>
					</div>';
				}
				
				$paramsBlock2 = $floorsType.$right_build.$conditions.$power_max.$cadastr.$rights.$category.$used;
			}

			/*
			*  Гараж, Машиноместо
			*/
			if($PageInfo['type_commer']==8){
				/*
				*  Тип здания
				*/
				$type_build = '';
				if(!empty($PageInfo['type_build'])){
					$type_build = '<div class="row">
						<div class="cell label">Тип здания:</div>
						<div class="cell">'.str_replace('.',',',$_TYPE_BUILD_FRESH[$PageInfo['type_build']]).'</div>
					</div>';
				}
				/*
				*  Класс объекта
				*/
				$class_object = '';
				if(!empty($PageInfo['class_object'])){
					$class_object = '<div class="row">
						<div class="cell label">Класс объекта:</div>
						<div class="cell">'.str_replace('.',',',$_CLASS_OBJECTS[$PageInfo['class_object']]).'</div>
					</div>';
				}
				/*
				*  Год постройки
				*/
				$yearBuild = '';
				if(!empty($PageInfo['year'])){
					$yearBuild = '<div class="row">
						<div class="cell label">Год постройки:</div>
						<div class="cell">'.$PageInfo['year'].'</div>
					</div>';
				}
				
				/*
				*  Площадь объекта
				*/
				$full_square = '';
				if(!empty($PageInfo['full_square'])){
					$full_square = '<div class="row">
						<div class="cell label">Площадь объекта:</div>
						<div class="cell">'.$PageInfo['full_square'].' м²</div>
					</div>';
				}
				
				/*
				*  Земельный участок
				*/
				$area_square = '';
				if(!empty($PageInfo['area_square'])){
					$area_square = '<div class="row">
						<div class="cell label">Земельный участок:</div>
						<div class="cell">'.$PageInfo['area_square'].' м²</div>
					</div>';
				}
				
				$paramsBlock = $type_build.$class_object.$yearBuild;
				
				/*
				*  Высота потолков
				*/
				$ceiling_height = '';
				if(!empty($PageInfo['ceiling_height'])){
					$ceiling_height = '<div class="row">
						<div class="cell label">Высота потолков:</div>
						<div class="cell">'.str_replace(".", ",", $PageInfo['ceiling_height']).' м</div>
					</div>';
				}
				$floorsType = '';
				if(!empty($PageInfo['floors'])){
					$floorsType = '<div class="row">
						<div class="cell label">Этажей:</div>
						<div class="cell">'.$PageInfo['floors'].'</div>
					</div>';
				}

				/*
				*  Состояние
				*/
				$conditions = '';
				if(!empty($PageInfo['conditions'])){
					$conditions = '<div class="row">
						<div class="cell label">Состояние:</div>
						<div class="cell">'.$_CONDITIONS_FRESH[$PageInfo['conditions']].'</div>
					</div>';
				}
				
				/*
				*  Вход
				*/
				$entrance = '';
				if(!empty($PageInfo['entrance'])){
					$entrance = '<div class="row">
						<div class="cell label">Вход:</div>
						<div class="cell">'.str_replace('.',',',$_ENTRANCES[$PageInfo['entrance']]).'</div>
					</div>';
				}
				
				/*
				*  Готовность
				*/
				// $readis = '<div class="row">
						// <div class="cell label">Готовность:</div>
						// <div class="cell">'.str_replace('.',',',$_READIS[$PageInfo['readis']]).'</div>
					// </div>';
					
				/*
				*  Парковка
				*/
				$parking_value = '';
				if(!empty($PageInfo['parking_value'])){
					$parking_value = '<div class="row">
						<div class="cell label">Парковка:</div>
						<div class="cell">'.$_PARKING[$PageInfo['parking_value']].'</div>
					</div>';
				}
				/*
				*  Право на ЗУ
				*/
				$rights = '';
				if(!empty($PageInfo['rights'])){
					$rights = '<div class="row">
						<div class="cell label">Право на ЗУ:</div>
						<div class="cell">'.$_RIGHTS[$PageInfo['rights']].'</div>
					</div>';
				}
				
				/*
				*  Водоснабжение
				*/
				$plumbing = '';
				if(!empty($PageInfo['plumbing'])){
					$plumbing = '<div class="row">
						<div class="cell label">Водоснабжение:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Теплоснабжение
				*/
				$heating = '';
				if(!empty($PageInfo['heating'])){
					$heating = '<div class="row">
						<div class="cell label">Теплоснабжение:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Канализация
				*/
				$sewerage = '';
				if(!empty($PageInfo['sewerage'])){
					$sewerage = '<div class="row">
						<div class="cell label">Канализация:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Газоснабжение
				*/
				$gas = '';
				if(!empty($PageInfo['gas'])){
					$gas = '<div class="row">
						<div class="cell label">Газоснабжение:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Кафе
				*/
				$cafe = '';
				if(!empty($PageInfo['cafe'])){
					$cafe = '<div class="row">
						<div class="cell label">Кафе:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Гостиница
				*/
				$hotel = '';
				if(!empty($PageInfo['hotel'])){
					$hotel = '<div class="row">
						<div class="cell label">Кафе:</div>
						<div class="cell">Да</div>
					</div>';
				}
				
				/*
				*  Столовая
				*/
				$canteen = '';
				if(!empty($PageInfo['canteen'])){
					$canteen = '<div class="row">
						<div class="cell label">Столовая:</div>
						<div class="cell">Да</div>
					</div>';
				}
				/*
				*  Офисные помещения
				*/
				$office_rooms = '';
				if(!empty($PageInfo['office_rooms'])){
					$office_rooms = '<div class="row">
						<div class="cell label">Офисные помещения:</div>
						<div class="cell">Да</div>
					</div>';
				}
				$floorsType = '';
				$floorsArray = array();
				if(!empty($PageInfo['floor'])){
					$floorsArray = array($PageInfo['floor']);
					$floorsType = $PageInfo['floor'];
				}
				if(count($floorsArray)>0){
					if(!empty($PageInfo['floors'])){
						array_push($floorsArray,'/'.$PageInfo['floors']);
					}
					$floorsType = implode('',$floorsArray);
					$floorsType = '<div class="row">
						<div class="cell label">Этаж:</div>
						<div class="cell">'.$floorsType.'</div>
					</div>';
				}
				if(!empty($PageInfo['basement']) || !empty($PageInfo['semibasement'])){
					$floorsArray = array();
					if(!empty($PageInfo['basement'])){
						array_push($floorsArray,'Подвал');
					}
					if(!empty($PageInfo['semibasement'])){
						array_push($floorsArray,'Полуподвал');
					}
					if(count($floorsArray)>0){
						$floorsType = implode(', ',$floorsArray);
					}
					$floorsType = '<div class="row">
						<div class="cell label">Этаж:</div>
						<div class="cell">'.$floorsType.'</div>
					</div>';
				}
				
				$paramsBlock2 = $full_square.$floorsType.$conditions.$entrance.$parking_value.$area_square.$rights.$plumbing.$heating.$sewerage.$gas.$cafe.$hotel.$canteen.$office_rooms;
			}
			
			$idUser = 0;
			$textFavorite = 'В избранное';
			$classFavorite = '';
			if($_SESSION['idAuto']){
				$idUser = $_SESSION['idAuto'];
				$res = mysql_query("
					SELECT COUNT(*) AS count
					FROM ".$template."_favorites
					WHERE estate=5 && user_id=".$idUser." && float_id=".$PageInfo['id']." && activation=1
				") or die(mysql_error());
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					if($row['count']){
						$textFavorite = 'В избранном';
						$classFavorite = ' at';
					}
				}
			}
			$type_commercials = '<div class="cell">'.$_TYPE_COMMERCIAL_ESTATE[$PageInfo['type_commer']].'</div>';
			if($PageInfo['type_commer']==8){
				if(!empty($PageInfo['garage'])){
					$typeName = 'Гараж';
				}
				else if(!empty($PageInfo['car_place'])){
					$typeName = 'Машиноместо';
				}
				else if(!empty($PageInfo['garage']) && !empty($PageInfo['car_place'])){
					$typeName = $_TYPE_COMMERCIAL_ESTATE[$PageInfo['type_commer']];
				}
				else {
					$typeName = $_TYPE_COMMERCIAL_ESTATE[$PageInfo['type_commer']];					
				}
				$type_commercials = '<div class="cell">'.$typeName.'</div>';
			}
			if($PageInfo['type_commer']==3){
				if(!empty($PageInfo['wash'])){
					$typeName = 'Автомойка';
				}
				else if(!empty($PageInfo['autoservice'])){
					$typeName = 'Автосервис';
				}
				else if(!empty($PageInfo['garage']) && !empty($PageInfo['car_place'])){
					$typeName = $_TYPE_COMMERCIAL_ESTATE[$PageInfo['type_commer']];
				}
				else {
					$typeName = $_TYPE_COMMERCIAL_ESTATE[$PageInfo['type_commer']];					
				}
				$type_commercials = '<div class="cell">'.$typeName.'</div>';
			}
			if($PageInfo['type_commer']==5){
				if(!empty($PageInfo['production'])){
					$typeName = 'Производство';
				}
				else if(!empty($PageInfo['stock'])){
					$typeName = 'Склад';
				}
				else if(!empty($PageInfo['production']) && !empty($PageInfo['stock'])){
					$typeName = $_TYPE_COMMERCIAL_ESTATE[$PageInfo['type_commer']];
				}
				else {
					$typeName = $_TYPE_COMMERCIAL_ESTATE[$PageInfo['type_commer']];					
				}
				$type_commercials = '<div class="cell">'.$typeName.'</div>';
			}
			$mayBeUseText = '';
			$mayBeUse = array();
			if($PageInfo['type_commer']==2){
				if(!empty($PageInfo['shop'])){
					array_push($mayBeUse,'магазин');
				}
				if(!empty($PageInfo['cafe'])){
					array_push($mayBeUse,'кафе');
				}
				if(!empty($PageInfo['hotel'])){
					array_push($mayBeUse,'гостиница');
				}
				if(!empty($PageInfo['bank'])){
					array_push($mayBeUse,'банк');
				}
				if(!empty($PageInfo['beauty'])){
					array_push($mayBeUse,'салон красоты');
				}
				if(!empty($PageInfo['domestic'])){
					array_push($mayBeUse,'бытовые услуги');
				}
				if(!empty($PageInfo['pharmacy'])){
					array_push($mayBeUse,'аптека');
				}
			}
			if(count($mayBeUse)>0){
				$mayBeUseText = '<div class="row"><div class="cell label">Возможное использование:</div><div class="cell">'.implode(', ',$mayBeUse).'</div></div>';
			}
			
		?>
		<div class="card_info">
			<div class="info_housing">
				<div class="row first">
					<div class="cell label">ID: <?=$PageInfo['id']?></div>
					<div class="cell"><a class="add_favorite<?=$classFavorite?>" onclick="return addFavorite(this)" href="javascript:void(0)"><span><?=$textFavorite?></span></a></div>
				</div>
				<div class="row">
					<div class="cell label">Коммерческая недвижимость:</div>
					<?=$type_commercials?>
				</div>
				<?=$mayBeUseText?>
				<?=$paramsBlock?>
			</div>
			<div class="info_housing">
				<?
				echo $paramsBlock2;
				if(!empty($PageInfo['short_text'])){
					echo '<div class="row add">
						<div class="cell label">Дополнительно:</div>
						<div class="cell desc">'.nl2br(htmlspecialchars_decode($PageInfo['short_text'])).'</div>
					</div>';
				}
				else {
					if(!empty($PageInfo['text'])){
						echo '<div class="row add">
							<div class="cell label">Дополнительно:</div>
							<div class="cell desc">'.nl2br(strip_tags(htmlspecialchars_decode($PageInfo['text']))).'</div>
						</div>';
					}
				}
				?>
			</div>
			
			<?
			$arrayBuy = array();
			if(isset($PageInfo['mortgages']) && !empty($PageInfo['mortgages'])){
				array_push($arrayBuy,'<li>Ипотека</li>');
			}
			if(isset($PageInfo['subsidies']) && !empty($PageInfo['subsidies'])){
				array_push($arrayBuy,'<li>Субсидии</li>');
			}
			if(isset($PageInfo['credit']) && !empty($PageInfo['credit'])){
				array_push($arrayBuy,'<li>Кредит</li>');
			}
			if(isset($PageInfo['installment']) && !empty($PageInfo['installment'])){
				array_push($arrayBuy,'<li>Рассрочка</li>');
			}
			if(count($arrayBuy)>0){
				echo '<div class="ways_buy">
					<ul>
						'.implode('',$arrayBuy).'
					</ul>
				</div>';
				
			}

			$linkThisPage = urlencode('http://'.$_SERVER['HTTP_HOST'].'/'.$_SERVER['REQUEST_URI']);
			?>
			
			<div class="share_block">
				<ul>
					<li>Поделиться:</li>
					<li><a onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="http://vk.com/share.php?url=<?=$linkThisPage?>"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
					<li><a onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://www.facebook.com/sharer.php?src=sp&u=<?=$linkThisPage?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://plus.google.com/share?url=<?=$linkThisPage?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
					<li><a onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://twitter.com/intent/tweet?url=<?=$linkThisPage?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" href="https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl=<?=$linkThisPage?>"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="clear"></div>
		<div class="text_info">
			<!--<h2><?=$PageInfo['h1']?></h2>-->
			<?
			// if(!empty($PageInfo['text'])){
				// echo '<div class="text"><p>'.strip_tags(nl2br($textPage)).'</p></div>';
				// echo '<div class="more_read"><a href="javascript:void(0)">Читать всё</a></div>';
			// }
			?>
		</div>
		<!--<div class="more_info">
			<div class="near_objects">
				<div class="infrastructure_block">
					<div class="title">Инфраструктура</div>
					<ul>
						<li>
							<div class="item"><span class="school icon"></span>Школа имени Римского-Корсакого</div>
						</li>
						<li>
							<div class="item"><span class="kindergarten icon"></span>Детский сад №56</div>
						</li>
						<li>
							<div class="item"><span class="clinic icon"></span>Поликлиника им. Св. Марии</div>
						</li>
					</ul>
				</div>
				<div class="safety_block">
					<div class="title">Безопасность</div>
					<ul>
						<li>Огороженный периметр</li>
						<li>Видеонаблюдение</li>
						<li>Пропускная система</li>
						<li>Консьерж</li>
						<li>Охраняемая парковка</li>
						<li>Сигнализация</li>
					</ul>
				</div>
			</div>
			<div class="other_offers_flats">
				<div class="list_flats">
					<div class="row">
						<div class="name">Студия. от <strong>27,5 м<sup>2</sup></strong></div>
						<div class="price">от <strong><a href="#">995 000 руб.</a></strong></div>
					</div>
					<div class="row fone">
						<div class="name">1-комн. от <strong>36,4 м<sup>2</sup></strong></div>
						<div class="price">от <strong><a href="#">1 638 000 руб.</a></strong></div>
					</div>
					<div class="row">
						<div class="name">2-комн. от <strong>44,7 м<sup>2</sup></strong></div>
						<div class="price">от <strong><a href="#">1 922 100 руб.</a></strong></div>
					</div>
				</div>
			</div>
		</div>-->
		<div class="bottom_block">
			<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
			<style>#map {width:650px;height:780px;padding:0;margin:0}</style>
			<?
			// echo '<pre>';
			// print_r($PageInfo);
			// echo '</pre>';
			$coordsYandex = '';
			$ex_coords = array('30.315868','59.939095');
			if(empty($PageInfo['coords'])){
				$params2 = array(
					'geocode' => 'Санкт-Петербург, '.$PageInfo['address'],
					'format'  => 'json',
					'results' => 1,
					// 'key'     => 'AKDIIFYBAAAAqIYtRgIAZYb8P32hIxnbRRSe9CpggONv4PEAAAAAAAAAAADu6wMZJIEJ0SDd0mRe33ag1JRdNA==',
				);
				$response = json_decode(@file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params2)));
				
				if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0){
					$coordsYandex = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
				}
			}
			else {
				$coordsYandex = $PageInfo['coords'];
				$ex_coords = explode(',',$coordsYandex);
				if(count($ex_coords)>1){
					$coordsYandex = $ex_coords[1].' '.$ex_coords[0];
				}
			}
			$coordsYandex = trim($coordsYandex);
			if(!empty($coordsYandex)){
				$coordsYandex = str_replace(',',' ',$coordsYandex);
				$ex_coords = explode(' ',$coordsYandex);
			}
			
			?>
			
			
			<?if(empty($coordsYandex)){?>
				<script>
					ymaps.ready(function () {
						var myMap = new ymaps.Map('map', {
							center: [<?=$ex_coords[1]?>,<?=$ex_coords[0]?>],
							zoom: 13
						}, {
							searchControlProvider: 'yandex#search'
						}),
						myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
							hintContent: 'Метка на карте',
							balloonContent: 'г. <?=$PageInfo['city_name']?>, <?=$PageInfo['address']?>'
						}, {
							// Опции.
							// Необходимо указать данный тип макета.
							iconLayout: 'default#image',
							// Своё изображение иконки метки.
							iconImageHref: '/images/neitralIcon.png',
							// Размеры метки.
							iconImageSize: [52, 44],
							// Смещение левого верхнего угла иконки относительно
							// её "ножки" (точки привязки).
							iconImageOffset: [-3, -42]
						});

						myMap.geoObjects.add(myPlacemark);
						
						var address = "г. <?=$PageInfo['city_name']?>, <?=$PageInfo['address']?>";
							/*ymaps.geocode(address,{kind:'metro'}).then(function(res){
							var m0 = res.geoObjects.get(0);
							var m0_coords = m0.geometry.getCoordinates();
							var addressName = m0.properties.get('name');
						
							ymaps.geocode(m0_coords,{kind:'metro'}).then(function(res){
								var m0 = res.geoObjects.get(0);
								var m0_coords = m0.geometry.getCoordinates();
								var name = m0.properties.get('name');
								<?if(!empty($PageInfo['station_name'])){?>
									var myRouter = ymaps.route([
									  "г. <?=$PageInfo['city_name']?>, <?=$PageInfo['station_name']?>", {
										type: "viaPoint",                   
										point: addressName
									  },
									], {
									  mapStateAutoApply: true 
									});
								<?}else{?>
									var myRouter = ymaps.route([
									  name, {
										type: "viaPoint",                   
										point: addressName
									  },
									], {
									  mapStateAutoApply: true 
									});
								<?}?>
								
								myRouter.then(function(route) {
									var points = route.getWayPoints();
									points.options.set('preset', 'islands#blackStretchyIcon');
									points.get(0).properties.set("iconContent", 'Санкт-Петербург, м. <?=$PageInfo['station_name']?>');
									points.get(1).properties.set("iconContent", '<?=htmlspecialchars_decode($PageInfo['name'])?>');
									// Добавление маршрута на карту
									myMap.geoObjects.add(route);
									var routeLength = route.getHumanLength().toString();
									var routeLength2 = route.getLength();
									// $('<div class="full_route">Расстояние от <span>Санкт-Петербург, м. <?=$PageInfo['station_name']?></span> до <span><?=htmlspecialchars_decode($PageInfo['address'])?></span>: <strong>'+routeLength+'</strong></div>').insertAfter('#map');
									var parent = arrLinkPage[1];
									var id = arrLinkPage[2];
									var data = "buildDist="+routeLength2+"&parent="+parent+"&id="+id;
									xhr = $.ajax({
										type: "POST",
										url: '/include/handler.php',
										data: data+"&rnd="+Math.random(),
										dataType: "json",
										success: function (data, textStatus) {
											var json = eval(data);
											if(json.action!='error'){
												// window.location = window.location;
											}
											else {
												alert('Возникла системная ошибка! Данные не сохранены. Попробуйте позднее.');
											}
										}
									});
								  },
								  // Обработка ошибки
								  function (error) {
									// alert("Возникла ошибка: " + error.message);
								  }
								)
							});
						}); */
							
					});
				</script>
			<?}else{?>
				<script>
				
					ymaps.ready(function () {
					var myMap = new ymaps.Map('map', {
							center: [<?=$ex_coords[1]?>,<?=$ex_coords[0]?>],
							zoom: 13
						}, {
							searchControlProvider: 'yandex#search'
						}),
						myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
							hintContent: '<?=$PageInfo['name']?>',
							balloonContent: '<?=$PageInfo['address']?>'
						}, {
							// Опции.
							// Необходимо указать данный тип макета.
							iconLayout: 'default#image',
							// Своё изображение иконки метки.
							iconImageHref: '/images/<?=$_ICONS_MAPS[5]['card']?>',
							// Размеры метки.
							iconImageSize: [52, 44],
							// Смещение левого верхнего угла иконки относительно
							// её "ножки" (точки привязки).
							iconImageOffset: [-3, -42]
						});

					myMap.geoObjects.add(myPlacemark);
					var myRouter = ymaps.route([
					  'Санкт-Петербург, м. <?=$PageInfo['station_name']?>', {
						type: "viaPoint",                   
						point: "Санкт-Петербург, <?=$PageInfo['address']?>"
					  },
					], {
					  mapStateAutoApply: true 
					});
					
/* 					myRouter.then(function(route) {
						var points = route.getWayPoints();
						points.options.set('preset', 'islands#blackStretchyIcon');
						points.get(0).properties.set("iconContent", 'Санкт-Петербург, м. <?=$PageInfo['station_name']?>');
						points.get(1).properties.set("iconContent", '<?=htmlspecialchars_decode($PageInfo['name'])?>');
						// Добавление маршрута на карту
						myMap.geoObjects.add(route);
						var routeLength = route.getHumanLength();
						var routeLength2 = route.getLength();
						$('<div class="full_route">Расстояние от <span>Санкт-Петербург, м. <?=$PageInfo['station_name']?></span> до <span><?=htmlspecialchars_decode($PageInfo['name'])?></span>: <strong>'+routeLength+'</strong></div>').insertAfter('#map');
						var parent = arrLinkPage[1];
						var id = arrLinkPage[2];
						var data = "buildDist="+routeLength2+"&parent="+parent+"&id="+id;
						xhr = $.ajax({
							type: "POST",
							url: '/include/handler.php',
							data: data+"&rnd="+Math.random(),
							dataType: "json",
							success: function (data, textStatus) {
								var json = eval(data);
								if(json.action!='error'){
									// window.location = window.location;
								}
								else {
									alert('Возникла системная ошибка! Данные не сохранены. Попробуйте позднее.');
								}
							}
						});
						// alert(routeLength2);
					  },
					  // Обработка ошибки
					  function (error) {
						alert("Возникла ошибка: " + error.message);
					  }
					) */
				});
				</script>
			<?}
			$verified = '<i class="verified"></i>';
			$verified = '';
			$userBlock = '';
			$styleForm = ' style="display:block;text-align:center;width:100%"';
			if(!empty($PageInfo['user_id'])){
				$userBlock = '<span class="id_block">ID: '.$PageInfo['user_id'].$verified.'</span>';
				$styleForm = '';
			}
			?>
			<div class="map"><div id="map"></div></div>
			<div class="feedback_block">
				<div class="representative">
					<div class="user_info">
						<span<?=$styleForm?> class="form">Представитель</span>
						<?=$userBlock?>
					</div>
					<div class="show_phone">
						<div class="btn_save top">
							<label class="btn"><input onclick="return show_phone(this,<?=$PageInfo['id']?>)" type="button" value="Показать ТЕЛЕФОН"><span class="angle-right"></span></label>
						</div>
					</div>
				</div>
				
				<div class="feedback_form">
					<form action="/include/handler.php" method="post" onsubmit="return checkSideForm(this)">
						<div class="send_request">
							<input type="hidden" name="type_form" value="application">
							<div class="title">Запросить информацию по объекту</div>
							<div class="table">
								<div class="row">
									<div class="cell">
										<div class="label">Ваше имя<b>*</b></div>
									</div>
									<div class="cell">
										<div class="input_text required">
											<input type="text" name="s[name]">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="cell">
										<div class="label">Ваш телефон<b>*</b></div>
									</div>
									<div class="cell">
										<div class="input_text required">
											<input class="phone" type="text" name="s[phone]">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="cell">
										<div class="label">Ваш e-mail<b>*</b></div>
									</div>
									<div class="cell">
										<div class="input_text required">
											<input type="text" name="s[email]">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="cell">
										<div class="label">Сообщение</div>
									</div>
									<div class="cell">
										<div class="input_text required"><textarea placeholder="Меня интересует этот объект недвижимости, пожалуйста, свяжитесь со мной..." name="s[msg]"></textarea></div>
									</div>
								</div>
								<div class="row">
									<div class="cell">
										<div class="label">&nbsp;</div>
									</div>
									<div class="cell" style="margin-right:-35px">
										<script src='https://www.google.com/recaptcha/api.js'></script>
										<div style="transform:scale(0.9);-webkit-transform:scale(0.9);transform-origin:0 0;-webkit-transform-origin:0 0;" class="g-recaptcha" data-sitekey="6Lc25CkUAAAAAGSUoAgMgNQkIdiYVivlI5xP0cPI"></div>
									</div>
								</div>
								<div class="row">
									<div class="cell">
										<div class="label">&nbsp;</div>
									</div>
									<div class="cell">
										<div class="checkbox_single">
											<input disabled="disabled" type="hidden" name="agree">
											<div class="container">
												<span class="check"></span><span class="label">Нажимая кнопку «Отправить» я выражаю согласие на правила обработки персональных данных согласно <a href="/user-agreement" target="_blank">"Политике защиты персональной информации пользователей сайта"</a></span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="btn">
								<label class="btn disabled"><input disabled="disabled" type="submit" value="Отправить"><span class="angle-right"></span></label>
							</div>
							<div class="table">
								<div class="row">
									<div class="cell full comment">
										<div class="req"><b>*</b> - поля обязательные для заполнения</div>
									</div>
								</div>
							</div>
							
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<?
		$res = mysql_query("
			SELECT *
			FROM ".$template."_articles
			WHERE activation=1
			ORDER BY date DESC,id DESC
			LIMIT 10
		");
		if(mysql_num_rows($res)>0){
			echo '<div class="news_events">';
			echo '<h3>Новости и статьи</h3>';
			echo '<div id="owl-demo">';
			while($row=mysql_fetch_assoc($res)){
				$images = '';
				$link = '/articles/'.$row['link'];
				if(!empty($row['images'])){
					$ex_avatar = explode(',',$row['images']);
					$images = '<div class="avatar"><img width="125" src="/admin_2/uploads/'.$ex_avatar[1].'"></div>';
				}
				echo '<div class="item">
					<div class="date">'.date_rus($row['date']).'</div>
					<div class="title"><a target="_blank" href="'.$link.'"><span>'.$row['name'].'</span></a></div>
				</div>';
			}
			echo '</div>';
			echo '</div><script src="/libs/owl-carousel/js/owl.carousel.js?ver=<'.time().'"></script>';
		}
		?>
		</div>
		
		<!--<div id="comments_block">
			<h3>Комментарии пользователей</h3>
			<div class="container_block">
				<div class="scrollbar-inner">
					<ul>
						<li>
							<div class="image"><a href="#"><img src="/images/avatar.jpg"></a></div>
							<div class="body">
								<div class="title">
									<div class="info">
										<a href="#">Вера Пчёлкина</a><strong>19:35</strong> | <span>25 Окт. 2016</span>
									</div>
									<div class="id_comment">#24590</div>
								</div>
								<div class="message_block">
									<div class="msg">Очевидно, что марксизм формирует прагматический культ личности. Механизм власти, как бы это ни казалось парадоксальным, теоретически возможен. Авторитаризм, короче говоря, верифицирует системный бихевиоризм.</div>
									<div class="btns">
										<div class="answer"><a href="#">Ответить</a></div>
										<div class="complain"><a href="#">Пожаловаться</a></div>
									</div>
								</div>
							</div>
							<ul>
								<li>
									<div class="image"><a href="#"><img src="/images/avatar.jpg"></a></div>
									<div class="body">
										<div class="title">
											<div class="info">
												<a href="#">Вера Пчёлкина</a><strong>19:35</strong> | <span>25 Окт. 2016</span>
											</div>
											<div class="id_comment">#24590</div>
										</div>
										<div class="message_block">
											<div class="msg">Очевидно, что марксизм формирует прагматический культ личности. Механизм власти, как бы это ни казалось парадоксальным, теоретически возможен. Авторитаризм, короче говоря, верифицирует системный бихевиоризм.</div>
											<div class="btns">
												<div class="answer"><a href="#">Ответить</a></div>
												<div class="complain"><a href="#">Пожаловаться</a></div>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</li>
						<li>
							<div class="image"><a href="#"><img src="/images/avatar.jpg"></a></div>
							<div class="body">
								<div class="title">
									<div class="info">
										<a href="#">Вера Пчёлкина</a><strong>19:35</strong> | <span>25 Окт. 2016</span>
									</div>
									<div class="id_comment">#24590</div>
								</div>
								<div class="message_block">
									<div class="msg">Очевидно, что марксизм формирует прагматический культ личности. Механизм власти, как бы это ни казалось парадоксальным, теоретически возможен. Авторитаризм, короче говоря, верифицирует системный бихевиоризм.</div>
									<div class="btns">
										<div class="answer"><a href="#">Ответить</a></div>
										<div class="complain"><a href="#">Пожаловаться</a></div>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="btn_info cost">
					<div class="table_form">
						<div class="row btn_row">
							<div class="cell center full last_col">
								<label class="btn link"><a class="back_page" href="javascript:void(0)"><span>Оставить сообщение</span></a></label>
								<label class="btn"><input type="submit" value="Перейти к обсуждению темы"><span class="angle-right"></span></label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>-->
		<?
		$priceParams = " && c.price>='".($PageInfo['price']-300000)."' && c.price<='".($PageInfo['price']+300000)."'";
		if($PageInfo['PageInfo']=='rent'){
			$priceParams = " && c.price>='".($PageInfo['price']-10000)."' && c.price<='".($PageInfo['price']+10000)."'";
		}
		$priceParams = '';
		$typeCommerMain = '';
		if(!empty($PageInfo['type'])){
			$typeCommerMain = " && type='".$PageInfo['type']."'";
		}
		
		$locationAdds = " && c.location=".$PageInfo['location'];
		if($PageInfo['type_commer']==6){
			$locationAdds = "";
		}
		
		$res = mysql_query("
			SELECT c.*,
			(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=c.id && estate='commercial' LIMIT 1) AS images,
			(SELECT title FROM ".$template."_stations WHERE activation='1' && id=c.station) AS station_name
			FROM ".$template."_commercials AS c
			WHERE c.activation=1".$locationAdds." && c.type_commer='".$PageInfo['type_commer']."'".$priceParams." && c.id!='".$PageInfo['id']."'".$typeCommerMain." && c.location=".$PageInfo['location']."
			ORDER BY RAND()
			LIMIT 4
		") or die(mysql_error());
		if(mysql_num_rows($res)>0){
			echo '<div id="related_offers">';
			echo '<h3>Похожие предложения</h3>';
			echo '<div class="container_block">';
			

			while($row = mysql_fetch_assoc($res)){
				$image = '<span><img src="/images/no_photo_238x156.png"/></span>';
				// if(!empty($row['images'])){
					// $ex_image = explode(',',$row['images']);
					// $size = @getimagesize($_SERVER['DOCUMENTS_ROOT'].'/admin_2/uploads/'.$ex_image[3]);
					// if(!empty($size[0])){
						// $image = '<img src="/admin_2/uploads/'.$ex_image[3].'">';
					// }
				// }
				$photosList = '';
				$photos = mysql_query("
					SELECT *
					FROM ".$template."_photo_catalogue
					WHERE estate='commercial' && p_main='".$row['id']."'
					LIMIT 1
				");
				if(mysql_num_rows($photos)>0){
					$ph = mysql_fetch_assoc($photos);
					$ex_images = explode(',',$ph['images']);
					if($ph['user_id']!=0){
						$photosList = '/users/'.$ph['user_id'].'/'.$ex_images[3];
					}
					else {
						$photosList = '/admin_2/uploads/'.$ex_images[3];
					}
					$image = '<span class="img" style="background-image:url('.$photosList.')"></span>';
				}
				
				$linkBuild = '/commercials/'.$row['p_main'];
				$link = '/commercials/'.$row['id'];
				$exploitation = explode('_',$row['exploitation']);
				$deadline = '';
				if(count($exploitation)>0){
					if(count($exploitation)>1){
						$deadline = '<span>'.$exploitation[1].' кв. '.$exploitation[0].'</span>';
					}
					else {
						$deadline = '<span>'.$exploitation[0].'</span>';
					}
				}
				
				$station_name = '';
				if(!empty($row['station_id'])){
					$r = mysql_query("
						SELECT title
						FROM ".$template."_stations
						WHERE id='".$row['station_id']."' && activation=1
					");
					if(mysql_num_rows($r)>0){
						$rw = mysql_fetch_assoc($r);
						$stat_name = $rw['title'];
						$link_station = '/search?type=sell&estate=1&priceAll=all&station='.$row['station'];
						$station_name = '<div class="station"><a href="'.$link_station.'">'.$stat_name.'</a><span class="dist">'.$row['dist_value'].' км</span></div>';
					}
				}
				
				echo '<div class="item">
					<div class="title_block">
						<a href="'.$linkBuild.'">'.$row['nameBuilds'].'</a>
						<div class="info_item">
							<div class="price">'.price_cell($row['price'],0).' <span class="rub">Р</span></div>
							<!--<div class="deadline">Срок сдачи: <span class="queue">'.$deadline.'</span></div>-->
						</div>
						<div class="image">
							<a href="'.$link.'">'.$image.'</a>
						</div>
						<div class="address">'.$row['address'].'</div>
						'.$station_name.'
					</div>
				</div>';
			}
			echo '</div>';
			echo '</div>';
		}
		?>
	</div>
	</div>
</div>	
<?}
if(count($params)==3){
	if($params[2]=='housing'){
		
	}
	if($params[2]=='commercials'){
		
	}

?>
<?}
// if(count($params)==4){
	// include("include/mods/flat_open_new.php");
// }
?>