<div id="center">
	<?
	$documents_block = '';
	$max_rows = 0;
	$titlePage = '';
	$res = mysql_query("
		SELECT h1 AS titlePage
		FROM ".$template."_types_document 
		WHERE id='".$nowDocumentPage."' && activation=1
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$row = mysql_fetch_assoc($res);
		$titlePage = $row['titlePage'];
	}
	$res = mysql_query("
		SELECT SQL_CALC_FOUND_ROWS *
		FROM ".$template."_documents
		WHERE p_main='".$nowDocumentPage."'
		ORDER BY num
	");
	if(mysql_num_rows($res)>0){
		$res2 = mysql_query("SELECT FOUND_ROWS()", $db);
		$max_rows = mysql_result($res2, 0);
		while($row = mysql_fetch_assoc($res)){
			$img = '';
			if(!empty($row['images'])){
				$ex_image = explode(',',$row['images']);
				$img = '<div class="image">
					<img src="/admin_2/uploads/'.$ex_image[0].'">
				</div>';
			}
			
			$short_text = '';
			if(!empty($row['short_text'])){
				$short_text = '<div class="short_text">'.$row['short_text'].'</div>';
			}
			
			$full_text = '';
			$text = htmlspecialchars_decode($row['text']);
			$link = '/admin_2/uploads/files/'.$row['files'];
			if(!empty($text)){
				$full_text = '<div class="text_full">
					<div class="text">'.htmlspecialchars_decode($row['text']).'</div>
					<div class="more_read"><a href="javascript:void(0)">Читать всё</a></div>
				</div>';
			}
			if(!empty($row['files'])){
				$sizeFile = round($row['size']/1025).' КБ';
				$documents_block .= '<div id="id_'.$row['id'].'" class="item">
					<div class="icon_file">
						<span class="icon '.$_CLASS_FILES[$row['type']].'"></span>
					</div>
					<div class="description_block">
						<div class="title"><a rel="nofollow" href="'.$link.'"><span>'.$row['name'].'</span></a></div>
						<div class="params">Размер файла: <span>'.$sizeFile.'</span> / '.$_TYPE_FILES[$row['type']].'</div>
						'.$full_text.'
						<div class="download"><a rel="nofollow" href="'.$link.'">Скачать</a></div>
					</div>
				</div>';
			}
			else {
				$documents_block .= '<div id="id_'.$row['id'].'" class="item">
					<div class="icon_file">
						<span class="icon '.$_CLASS_FILES[$row['type']].'"></span>
					</div>
					<div class="description_block">
						<div class="title"><a href="javascript:void(0)"><span>'.$row['name'].'</span></a></div>
						'.$full_text.'
					</div>
				</div>';
			}
		}
	}
	else {
		$documents_block = '<h3>Нет документов в базе</h3>';
	}
	?>
	<div class="title_pages">Образцы документов: <?=$titlePage?></div>
	<div class="sort_offers">
		<div class="count_offers">Всего<span><?=$max_rows?></span> документов</div>
		<div class="sort_block">
			<div class="label_select">Сортировать:</div>
			<div class="select_block sort">
				<input type="hidden" name="sort">
				<?
				$countOffersValue = ' class="current"';
				$nameValue = '';
				$nameSort = 'По дате загрузки';
				if($valueSortName=='count'){
					$nameSort = 'По дате загрузки';
					$countOffersValue = ' class="current"';
				}
				if($valueSortName=='name'){
					$nameSort = 'По имени';
					$nameValue = ' class="current"';
					$countOffersValue = '';
				}
				?>
				<div style="width:160px" class="choosed_block"><?=$nameSort?></div>
				<div class="scrollbar-inner">
					<ul>
						<li<?=$countOffersValue?>><a href="<?=$linkSortCounts?>" data-id="1">По дате загрузки</a></li>
						<li<?=$nameValue?>><a href="<?=$linkSortNames?>" data-id="2">По имени</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div id="result_search" class="documents_block">
		<div class="menu_block">
			<div class="menu_gray">
				<?
				$res = mysql_query("
					SELECT *
					FROM ".$template."_types_document
					WHERE activation=1
					ORDER BY num
				");
				if(mysql_num_rows($res)>0){
					echo '<ul>';
					while($row = mysql_fetch_assoc($res)){
						$current = '';
						if($row['link']==$params[1] && count($params)==2 || count($params)==1 && $row['num']==1){
							$current = ' class="current"';
						}
						if($row['num']==1){
							echo '<li'.$current.'><a href="/documents">'.$row['name'].'</a></li>';
						}
						else {
							echo '<li'.$current.'><a href="/documents/'.$row['link'].'">'.$row['name'].'</a></li>';
						}
					}
					echo '</ul>';
				}
				?>
			</div>
		<?
		$res = mysql_query("
			SELECT c.*,
			(SELECT images FROM ".$template."_photo_catalogue WHERE activation='1' && cover='1' && p_main=c.id && estate='jk') AS images,
			(SELECT MIN(price) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS min_price,
			(SELECT MAX(price) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS max_price,
			(SELECT MIN(full_square) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS min_square,
			(SELECT MAX(full_square) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS max_square,
			(SELECT COUNT(*) FROM ".$template."_new_flats WHERE activation='1' && p_main=c.id) AS count_estate,
			(SELECT name FROM ".$template."_developers WHERE activation='1' && id=c.developer) AS name_developer
			FROM ".$template."_m_catalogue_left AS c
			WHERE c.activation='1'
			ORDER BY RAND()
			LIMIT 2
		") or die(mysql_error());
		if(mysql_num_rows($res)>0){
			echo '<div class="our_offers">';
			echo '<div class="centers_block">';
			echo '<h3>Возможно вам понравится</h3>';
			echo '<div class="offers_list">';
			while($row = mysql_fetch_assoc($res)){
				$ex_image = explode(',',$row['images']);
				$image = '<img src="/admin_2/uploads/'.$ex_image[0].'">';
				$link = '/newbuilding/'.$row['id'];
				$exploitation = explode('_',$row['exploitation']);
				$deadline = '';
				if(empty($row['min_square']) || empty($row['max_square'])){
					$square_meter_min = 0;
					$square_meter_max = 0;
				}
				else {
					$square_meter_min = ceil($row['min_price']/$row['min_square']);
					$square_meter_max = ceil($row['max_price']/$row['max_square']);
				}
				if(count($exploitation)>0){
					if(count($exploitation)>1){
						if(!empty($exploitation[0]) && !empty($exploitation[1])){
							$deadline = '<div class="deadline">Срок сдачи: <span>'.$exploitation[1].' кв. '.$exploitation[0].'</span></div>';
						}
					}
					else {
						if(!empty($exploitation[0])){
							$deadline = '<div class="deadline">Срок сдачи: <span>'.$exploitation[0].'</span></div>';
						}
					}
				}
				echo '<div class="item">
					<div class="image">
						<a target="_blank" href="'.$link.'">'.$image.'</a>
						<div class="type_name">'.$row['name_developer'].'</div>
						'.$deadline.'
					</div>
					<div class="body">
						<div class="location">
							<div class="title"><a target="_blank" href="'.$link.'">'.$row['name'].'</a></div>
							<div class="street">'.$row['address'].'</div>
						</div>
						<div class="cost_block">
							<div class="price">от <strong>'.price_cell($row['min_price'],0).' руб.</strong></div>
							<div class="square">от <strong>'.price_cell($square_meter_min,0).' руб/м<sup>2</sup></strong></div>
							<div class="count_offers"><a target="_blank" href="'.$link.'">'.$row['count_estate'].' в продаже</a></div>
						</div>
					</div>
				</div>';
			}
			echo '</div>';
			echo '</div>';
			echo '</div>';
		}
		?>
		</div>
		<div class="list_documents">
			<?=$documents_block?>
		</div>
	</div>
	<!--<div style="line-height:1;margin-bottom:18px" class="pager">
		<ul>
			<li class="current"><span>1</span></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li class="next"><a title="Следующая" href="#">→</a></li>
			<li class="last"><a title="В конец" href="#">В конец</a></li>
		</ul>
	</div>-->
</div>