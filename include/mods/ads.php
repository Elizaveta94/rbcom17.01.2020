	<?
	// Хлебные крошки (эталон)
	echo '<div id="center">
		<h1 class="inner_pages">Размещение объявления</h1>
		<p class="comment_page">Пожалуйста, укажите основные параметры объекта</p>';
		// echo '<pre>';
		// print_r($PageInfoAd);
		// echo '</pre>';
		if($PageInfoAd['type']){
			$type = '<div class="select_block count disabled">';
			$not_active = '<i class="not_active"></i>';		
		}
		else {
			$type = '<div class="select_block count">';
			$not_active = '';
		}
		$types = '';
		$nameParams = 'Не выбран';
		$valueInput = '';
		$disabledType = '';
		for($t=0; $t<count($_TYPE_MAIN); $t++){
			$current = "";
			if(empty($PageInfoAd['type']) && $t==0){
				$current = ' class="current"';
				$nameParams = $_TYPE_MAIN[$t]['name'];
			}
			if($PageInfoAd['type']==$_TYPE_MAIN[$t]['value']){
				$current = ' class="current"';
				$nameParams = $_TYPE_MAIN[$t]['name'];
				$valueInput = ' value="'.$_TYPE_MAIN[$t]['value'].'"';
				$typeIsset = true;
				$disabledType = ' disabled="disabled"';
			}
			$types .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$_TYPE_MAIN[$t]['value'].'">'.$_TYPE_MAIN[$t]['name'].'</a></li>';
		}

		if(!$PageInfoAd['type']){
			$type .= '<input type="hidden" name="typeName"'.$valueInput.$disabledType.'>';
		}
		$type .= '<div class="choosed_block">'.$nameParams.'</div>';
		$type .= '<div class="scrollbar-inner">';
		$type .= '<ul>';
		if(!$PageInfoAd['type']){
			$type .= $types;
		}
		$type .= '</ul>';
		$type .= '</div>';
		$type .= $not_active;
		$type .= '</div>';
		
		if($PageInfoAd['estate']){
			$estate = '<div class="select_block count disabled">';
			$not_active = '<i class="not_active"></i>';		
		}
		else {
			$estate = '<div class="select_block count">';
			$not_active = '';
		}
		$estates = '';
		$nameParams = 'Не выбран';
		$typeId = 0;
		$valueInput = '';
		$disabledEstate = '';
		if($PageInfoAd['type']){
			$typeId = $PageInfoAd['type'];
		}
		for($t=0; $t<count($_ESTATE_MAIN[$typeId]); $t++){
			$current = "";
			if(empty($PageInfoAd['estate']) && $t==0){
				$current = ' class="current"';
				$nameParams = $_ESTATE_MAIN[$typeId][$t]['name'];
			}
			if($PageInfoAd['estate']==$_ESTATE_MAIN[$typeId][$t]['value']){
				$current = ' class="current"';
				$nameParams = $_ESTATE_MAIN[$typeId][$t]['name'];
				$valueInput = ' value="'.$_ESTATE_MAIN[$typeId][$t]['value'].'"';
				$disabledEstate = ' disabled="disabled"';
			}
			$estates .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$_ESTATE_MAIN[$typeId][$t]['value'].'">'.$_ESTATE_MAIN[$typeId][$t]['name'].'</a></li>';		
		}

		if(!$PageInfoAd['estate']){
			$estate .= '<input type="hidden" name="estateName"'.$valueInput.$disabledEstate.'>';
		}
		$estate .= '<div class="choosed_block">'.$nameParams.'</div>';
		$estate .= '<div class="scrollbar-inner">';
		$estate .= '<ul>';
		if(!$PageInfoAd['estate']){
			$estate .= $estates;
		}
		$estate .= '</ul>';
		$estate .= '</div>';
		$estate .= $not_active;
		$estate .= '</div>';
		
		/*
		*  Парковочные места
		*/
		$parking_place = '';
		if(!empty($PageInfoAd['parking_place'])){
			$parking_place = ' value="'.$PageInfoAd['parking_place'].'"';
		}
		
		/*
		*  Текст ЖК
		*/
		$build_text = '';
		if(!empty($PageInfoAd['build_text'])){
			$build_text = ' value="'.$PageInfoAd['build_text'].'"';
			if(!empty($PageInfoAd['p_main'])){
				$bs = mysql_query("
					SELECT name
					FROM ".$template."_m_catalogue_left
					WHERE id=".intval($PageInfoAd['p_main'])." && activation=1
				");
				if(mysql_num_rows($bs)>0){
					$b = mysql_fetch_assoc($bs);
					$build_text = ' value="'.str_replace('"','',htmlspecialchars_decode($b['name'])).'"';
				}
			}
		}
		
		/*
		*  ID ЖК
		*/
		$build = '';
		if(!empty($PageInfoAd['p_main'])){
			$build = ' value="'.$PageInfoAd['p_main'].'"';
		}
		
		/*
		*  Наименование метро
		*/
		$station_id = '';
		if(!empty($PageInfoAd['station_id'])){
			$station_id = ' value="'.$PageInfoAd['station_id'].'"';
		}
		
		/*
		*  Лифт
		*/
		$lift_massa = '';
		if(!empty($PageInfoAd['lift_massa'])){
			$lift_massa = ' value="'.$PageInfoAd['lift_massa'].'"';
		}
		
		/*
		*  ID метро
		*/
		$station = '';
		if(!empty($PageInfoAd['station'])){
			$station = ' value="'.$PageInfoAd['station'].'"';
		}
		
		echo '<div class="text_block">
			'.$textPage.'
		</div>';
		


		/*
		*  Формирование ссылки в параметры
		*/
		$page_name = '';
		$url_param_dinam = $_SERVER['REQUEST_URI'];			
		$pos = strripos($url_param_dinam, "?");
		$pageArray = array();
		$pageParams = array();
		$arrayLinkSearch = array();
		if(!empty($pos)){
			$url_paramDinamicFull = explode("?",$url_param_dinam);
			$url_paramDinamic = explode("&",$url_paramDinamicFull[count($url_paramDinamicFull)-1]);
			foreach($url_paramDinamic as $name => $value){
				$url_par = explode('=',$value);
				$arrayLinkSearch[$url_par[0]] = $url_par[1];
			}		
			foreach($arrayLinkSearch as $key => $value){
				if($key!='page'){
					array_push($pageArray,$key."=".$value);
					$pageParams[$key] = $value;
				}
			}
			if(count($pageArray)>0){
				$page_name = $url_paramDinamicFull[0]."?".implode('&',$pageArray);
			}
		}
				
		$idParam = intval($pageParams['idParam']);
		$typeParam = $pageParams['type'];
		
		if(!isset($idParam) || !isset($typeParam)){
			$typeParam = 'sell';
			$idParam = 1;
		}
		
		if(isset($PageInfoAd['estate']) && isset($PageInfoAd['type'])){
			$typeParam = $PageInfoAd['type'];
			$idParam = $PageInfoAd['estate'];		
		}
		
		if($idParam==4){
			if(!isset($pageParams['type_country']) || empty($pageParams['type_country'])){
				$type_country = 1;
				$type_country = $PageInfoAd['type_country'];
			}
			else {
				$type_country = $pageParams['type_country'];
			}
			if($type_country==1){
				$type_countries = $pageParams['type_country'];
			}
		}
		
		if($idParam==5){
			if(!isset($pageParams['type_commer']) || empty($pageParams['type_commer'])){
				$type_commer = 1;
				$type_commer = $PageInfoAd['type_commer'];
			}
			else {
				$type_commer = $pageParams['type_commer'];
			}
			if($type_commer==7){
				$type_business = $pageParams['type_business'];
			}
		}
		
		$stationRow = '';
		
		$stations = '';
		if(!empty($PageInfoAd['station_id'])){
			$distance = '';
			if(isset($PageInfoAd['dist_value']) && !empty($PageInfoAd['dist_value'])){
				$dist = floor($PageInfoAd['dist_value']).' м';
				if($dist/1000>=1){
					$dist = price_cell($dist/1000,2).' км';
				}
				$distance = '/ <strong>'.$dist.'</strong>';
			}
			$stations = '<li><div class="metro">«'.$PageInfoAd['station_id'].'» '.$distance.'<span onclick="return removeMetro(this)" class="close"></span></div></li>';
		}
		
		/*
		*  Аренда/Продажа
		*/
		if(isset($PageInfoAd['type']) && $PageInfoAd['type']=='sell' || !isset($PageInfoAd['type'])){
			$sellTypeClass = ' class="current"';
			$rentTypeClass = '';
			$valueTypeName = ' value="sell"';
		}
		else {
			$sellTypeClass = '';
			$rentTypeClass = ' class="current"';			
			$valueTypeName = ' value="rent"';
		}
		
		/*
		*  Новостройка/Квартира/Комната/Загородная/Коммерческая
		*/
		$valueEstateName = ' value="1"';
		$newEstateClass = ' class="current"';
		$flatEstateClass = '';
		$roomEstateClass = '';
		$countryEstateClass = '';
		$commercialEstateClass = '';
		if(isset($PageInfoAd['estate']) && $PageInfoAd['estate']==2){
			$flatEstateClass = ' class="current"';
			$valueEstateName = ' value="2"';
			$newEstateClass = '';
		}
		else if(isset($PageInfoAd['estate']) && $PageInfoAd['estate']==3){
			$roomEstateClass = ' class="current"';
			$valueEstateName = ' value="3"';
			$newEstateClass = '';
		}
		else if(isset($PageInfoAd['estate']) && $PageInfoAd['estate']==4){
			$countryEstateClass = ' class="current"';
			$valueEstateName = ' value="4"';
			$newEstateClass = '';
		}
		else if(isset($PageInfoAd['estate']) && $PageInfoAd['estate']==5){
			$commercialEstateClass = ' class="current"';
			$valueEstateName = ' value="5"';
			$newEstateClass = '';
		}
		else {
			$newEstateClass = ' class="current"';
			$rentTypeClass = '';			
			$valueEstateName = ' value="1"';
		}
							
		$periodValue = '';
		if(isset($PageInfoAd['period']) && !empty($PageInfoAd['period'])){
			$periodValue = ' value="'.$PageInfoAd['period'].'"';
		}
							
		$typeCountryValue = '';
		$typeCountrySecondValue = ' disabled="disabled" value="5"';
		if(isset($PageInfoAd['type_country']) && !empty($PageInfoAd['type_country'])){
			$typeCountryValue = ' value="'.$PageInfoAd['type_country'].'"';
			$typeCountrySecondValue = ' value="'.$PageInfoAd['type_country'].'"';
		}
							
		$typeCommerValue = '';
		if(isset($PageInfoAd['type_commer']) && !empty($PageInfoAd['type_commer'])){
			$typeCommerValue = ' value="'.$PageInfoAd['type_commer'].'"';
		}
							
		$typeBusinessValue = '';
		if(isset($PageInfoAd['type_business']) && !empty($PageInfoAd['type_business'])){
			$typeBusinessValue = ' value="'.$PageInfoAd['type_business'].'"';
		}
		
		echo '<div class="add_block">
			<form onsubmit="return saveAdsUsers(this)" action="/include/handler.php" method="POST">
				<div class="container_block ads">
					<h2 class="title">Тип сделки</h2>';
					$disabledBlock = '';
					if(isset($pageParams['id'])){
						$disabledBlock = '<i class="disabled_block"></i>';
					}
					echo '<div id="tabs_estate">
						<div class="first_inform">
							<div class="types">
								<input type="hidden" name="typeName"'.$valueTypeName.'>
								<ul>
									<li'.$sellTypeClass.'><a data-type="sell" href="javascript:void(0)">Продажа</a></li>
									<li'.$rentTypeClass.'><a data-type="rent" href="javascript:void(0)">Аренда</a></li>
								</ul>
							</div>
							<div class="estates">
							<input type="hidden" name="estateName"'.$valueEstateName.'>
								<ul>
									<li'.$newEstateClass.'><a data-type="new" data-id="1" href="javascript:void(0)">Новостройка</a></li>
									<li'.$flatEstateClass.'><a data-type="flat" data-id="2" href="javascript:void(0)">Квартира</a></li>
									<li'.$roomEstateClass.'><a data-type="room" data-id="3" href="javascript:void(0)">Комната</a></li>
									<li'.$countryEstateClass.'><a data-type="country" data-id="4" href="javascript:void(0)">Загородная</a></li>
									<li'.$commercialEstateClass.'><a data-type="commercial" data-id="5" href="javascript:void(0)">Коммерческая</a></li>
								</ul>
							</div>
						</div>';
						
						$flatRoomSecond = ' style="display:none"';
						if(isset($PageInfoAd['estate']) && ($PageInfoAd['estate']==2 || $PageInfoAd['estate']==3)){
							$flatRoomSecond = '';
						}
/* 						echo '<div'.$flatRoomSecond.' class="second_inform rent flat room">
							<div class="types">
								<span class="label">Срок аренды:</span>
							</div>
							<div class="estates">
								<input type="hidden" name="period"'.$periodValue.'>
								<ul>
									<li'.($PageInfoAd['period']==1?' class="current"':'').'><a data-id="1" href="javascript:void(0)">Длительно</a></li>
									<li'.($PageInfoAd['period']==3?' class="current"':'').'><a data-id="3" href="javascript:void(0)">Посуточно</a></li>
								</ul>
							</div>
						</div>';
 */						
						$countrySecond = ' style="display:none"';
						if(isset($PageInfoAd['estate']) && $PageInfoAd['estate']==4){
							$countrySecond = '';
						}
						
						$rentBlockCountry = ' hide';
						$sellAddParams = ' sell';
						if(isset($PageInfoAd['type']) && $PageInfoAd['type']=='rent'){
							$rentBlockCountry = ' show';
							$sellAddParams = '';
						}

						echo '<div'.$countrySecond.' class="second_inform country rent sell">
							<!--<div class="rent_block'.$rentBlockCountry.'">
								<div class="types">
									<span class="label">Срок аренды:</span>
								</div>
								<div class="estates">
									<input type="hidden" name="period"'.$periodValue.'>
									<ul>
										<li'.($PageInfoAd['period']==1?' class="current"':'').'><a data-id="1" href="javascript:void(0)">Длительно</a></li>
										<li'.($PageInfoAd['period']==3?' class="current"':'').'><a data-id="3" href="javascript:void(0)">Посуточно</a></li>
									</ul>
								</div>
							</div>-->
							<div class="add_params'.$sellAddParams.'">
								<input type="hidden" name="type_country"'.$typeCountryValue.'>
								<ul>
									<li'.($PageInfoAd['type_country']==1?' class="current"':'').'><a href="javascript:void(0)">Коттеджный поселок</a></li>
									<li'.($PageInfoAd['type_country']==2?' class="current"':'').'><a data-id="2" href="javascript:void(0)">Таунхаус</a></li>
									<li'.($PageInfoAd['type_country']==3?' class="current"':'').'><a data-id="3" href="javascript:void(0)">Дом</a></li>
									<li'.($PageInfoAd['type_country']==4?' class="current"':'').'><a data-id="4" href="javascript:void(0)">Земельный участок</a></li>
								</ul>
							</div>';
							
							$countryPersonal = ' style="display:none"';
							if(isset($PageInfoAd['type_country']) && $PageInfoAd['type_country']==1){
								$countryPersonal = '';
							}	
							echo '<div'.$countryPersonal.' class="personal_params">
									<div style="width:100%" class="children_params">
										<input type="hidden" name="type_country"'.$typeCountrySecondValue.'>
										<ul style="float:left;width:630px;margin-left:390px;margin-top:5px;">
											<li'.($PageInfoAd['type_country']==5 || !$PageInfoAd['type_country']?' class="current"':'').'><a data-id="5" href="javascript:void(0)">Коттедж</a></li>
											<li'.($PageInfoAd['type_country']==6?' class="current"':'').'><a data-id="6" href="javascript:void(0)">Загородный участок</a></li>
										</ul>
									</div>
								</div>';
							
						echo '</div>';
						
						$commercialSecond = ' style="display:none"';
						if(isset($PageInfoAd['estate']) && $PageInfoAd['estate']==5){
							$commercialSecond = '';
						}
						echo '<div'.$commercialSecond.' class="second_inform rent sell commercial">
							<div class="add_params">
								<input type="hidden" name="type_commer"'.$typeCommerValue.'>
								<ul>
									<li'.($PageInfoAd['type_commer']==1?' class="current"':'').'><a data-id="1" href="javascript:void(0)">Офис</a></li>
									<li'.($PageInfoAd['type_commer']==2?' class="current"':'').'><a data-id="2" href="javascript:void(0)">Торговля, общепит, услуги</a></li>
									<li'.($PageInfoAd['type_commer']==3?' class="current"':'').'><a data-id="3" href="javascript:void(0)">Мойка, Автосервис</a></li>
									<li'.($PageInfoAd['type_commer']==4?' class="current"':'').'><a data-id="4" href="javascript:void(0)">ОСЗ, БЦ, ТРК</a></li>
									<li'.($PageInfoAd['type_commer']==5?' class="current"':'').'><a data-id="5" href="javascript:void(0)">Производство, склад</a></li>
									<li'.($PageInfoAd['type_commer']==6?' class="current"':'').'><a data-id="6" href="javascript:void(0)">Земельный участок</a></li>
									<li'.($PageInfoAd['type_commer']==7?' class="current"':'').'><a data-id="7" href="javascript:void(0)">Готовый бизнес</a></li>
									<li'.($PageInfoAd['type_commer']==8?' class="current"':'').'><a data-id="8" href="javascript:void(0)">Гараж, Машиноместо</a></li>
								</ul>
							</div>';
														
						$commercialPersonal = ' style="display:none"';
						if(isset($PageInfoAd['type_commer']) && $PageInfoAd['type_commer']==9){
							$commercialPersonal = '';
						}	
						echo '<div'.$commercialPersonal.' class="personal_params">
								<div style="width:100%" class="children_params">
									<input type="hidden" name="type_business"'.$typeBusinessValue.'>
									<ul style="float:left;width:630px">
										<li'.($PageInfoAd['type_business']==1?' class="current"':'').'><a data-id="1" href="javascript:void(0)">Авто-бизнесы</a></li>
										<li'.($PageInfoAd['type_business']==2?' class="current"':'').'><a data-id="2" href="javascript:void(0)">Аптечный</a></li>
										<li'.($PageInfoAd['type_business']==3?' class="current"':'').'><a data-id="3" href="javascript:void(0)">Арендный</a></li>
										<li'.($PageInfoAd['type_business']==4?' class="current"':'').'><a data-id="4" href="javascript:void(0)">Гостиничный</a></li>
										<li'.($PageInfoAd['type_business']==5?' class="current"':'').'><a data-id="5" href="javascript:void(0)">Интернет-бизнес</a></li>
										<li'.($PageInfoAd['type_business']==6?' class="current"':'').'><a data-id="6" href="javascript:void(0)">Мед. центры</a></li>
										<li'.($PageInfoAd['type_business']==7?' class="current"':'').'><a data-id="7" href="javascript:void(0)">Образование</a></li>
										<li'.($PageInfoAd['type_business']==8?' class="current"':'').'><a data-id="8" href="javascript:void(0)">Обществ. питание</a></li>
										<li'.($PageInfoAd['type_business']==9?' class="current"':'').'><a data-id="9" href="javascript:void(0)">Производство</a></li>
										<li'.($PageInfoAd['type_business']==10?' class="current"':'').'><a data-id="10" href="javascript:void(0)">Прочее</a></li>
										<li'.($PageInfoAd['type_business']==11?' class="current"':'').'><a data-id="11" href="javascript:void(0)">Салоны красоты</a></li>
										<li'.($PageInfoAd['type_business']==12?' class="current"':'').'><a data-id="12" href="javascript:void(0)">Сельское хоз-во</a></li>
										<li'.($PageInfoAd['type_business']==13?' class="current"':'').'><a data-id="13" href="javascript:void(0)">Строительство</a></li>
										<li'.($PageInfoAd['type_business']==14?' class="current"':'').'><a data-id="14" href="javascript:void(0)">Торговля</a></li>
										<li'.($PageInfoAd['type_business']==15?' class="current"':'').' style="position:absolute;right:167px"><a data-id="15" href="javascript:void(0)">Инвест. проекты</a></li>
										<li'.($PageInfoAd['type_business']==16?' class="current"':'').' style="position:absolute;right:73px"><a data-id="16" href="javascript:void(0)">Франшизы</a></li>
									</ul>
								</div>
							</div>';
							
						echo '</div>';
					echo $disabledBlock;
					echo '</div>';
					echo '<div class="separate"></div>
					</div>
					<div class="reload_block">
					<div class="upload">
					<div style="border-top:none;margin-top:-23px;padding-top:10px" class="container_block ads">';
					echo '
						<h2 class="title no-margin">Местоположение</h2>';
					/*
					*  Новостройки и переуступки (Местоположение)
					*/
					if($PageInfoAd['estate']==1 || $idParam == 1 || $PageInfoAd['estate']==6 || $idParam == 6){
						require_once("include/mods/cabinet/new/location_new_sell.php");
					}
					
					/*
					*  Аренда квартир (Местоположение)
					*/
					if($PageInfoAd['estate']==2 && $PageInfoAd['type']=='rent' || $idParam == 2 && $typeParam == 'rent'){
						require_once("include/mods/cabinet/flat/location_flat_rent.php");
					}
					
					/*
					*  Продажа квартир (Местоположение)
					*/
					if($PageInfoAd['estate']==2 && $PageInfoAd['type']=='sell' || $idParam == 2 && $typeParam == 'sell'){
						require_once("include/mods/cabinet/flat/location_flat_sell.php");
					}
					
					/*
					*  Аренда комнат (Местоположение)
					*/
					if($PageInfoAd['estate']==3 && $PageInfoAd['type']=='rent' || $idParam == 3 && $typeParam == 'rent'){
						require_once("include/mods/cabinet/room/location_room_rent.php");
					}
					
					/*
					*  Продажа комнат (Местоположение)
					*/
					if($PageInfoAd['estate']==3 && $PageInfoAd['type']=='sell' || $idParam == 3 && $typeParam == 'sell'){
						require_once("include/mods/cabinet/room/location_room_sell.php");
					}
					
					/*
					*  Аренда загородной (Местоположение)
					*/
					if($PageInfoAd['estate']==4 && $PageInfoAd['type']=='rent' || $idParam == 4 && $typeParam == 'rent'){
						require_once("include/mods/cabinet/country/location_country_rent.php");
					}
					
					/*
					*  Продажа загородной (Местоположение)
					*/
					if($PageInfoAd['estate']==4 && $PageInfoAd['type']=='sell' || $idParam == 4 && $typeParam == 'sell'){
						require_once("include/mods/cabinet/country/location_country_sell.php");
					}
					
					/*
					*  Аренда коммерческой (Местоположение)
					*/
					if($PageInfoAd['estate']==5 && $PageInfoAd['type']=='rent' || $idParam == 5 && $typeParam == 'rent'){
						require_once("include/mods/cabinet/commercial/location_commercial_rent.php");
					}
					
					/*
					*  Продажа коммерческой (Местоположение)
					*/
					if($PageInfoAd['estate']==5 && $PageInfoAd['type']=='sell' || $idParam == 5 && $typeParam == 'sell'){
						require_once("include/mods/cabinet/commercial/location_commercial_sell.php");
					}
					
					$area_id = $usersInfo['area_id'];
					$cityRegionName = 'Санкт-Петербург';
					$coordsPlace = '30.315868 59.939095';
					$ex_place_array = explode(' ',$coordsPlace);
					if(!empty($area_id)){
						$rs = mysql_query("
							SELECT *
							FROM ".$template."_location
							WHERE activation='1' && id=".$area_id."
						");
						if(mysql_num_rows($rs)>0){
							$rw = mysql_fetch_assoc($rs);
							$cityRegionName = $rw['name'];
							if(!empty($rw['coords'])){
								$coordsPlace = $rw['coords'];
								$ex_place_array = explode(' ',$coordsPlace);
							}
						}
					}
					
					echo '<div style="border-left:1px solid #b5babe" class="right_block">
						<div class="text_info">	
							<p>Убедитесь, что соблюдаете <a class="dash" href="javascript:void(0)">правила размещения</a> объявления!</p>
						</div>
						<div class="centers_block">';?>
							<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
							<style>
								#map {
									width:502px;height:435px;padding:0;margin:0;border:1px solid #ffffff;
								}
							</style>
							<?
							$coordsYandex = '';
							$ex_coords = $ex_place_array;
							if($PageInfoAd && empty($PageInfoAd['coords'])){
								$params2 = array(
									'geocode' => $cityRegionName.', '.$PageInfoAd['address'],
									'format'  => 'json',
									'results' => 1,
									'key'     => 'AKDIIFYBAAAAqIYtRgIAZYb8P32hIxnbRRSe9CpggONv4PEAAAAAAAAAAADu6wMZJIEJ0SDd0mRe33ag1JRdNA==',
								);
								$response = json_decode(@file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params2)));
								
								if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0){
									$coordsYandex = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
								}
							}
							else {
								$coordsYandex = $coordsPlace;
							}
							if(!empty($coordsYandex)){
								$ex_coords = explode(' ',$coordsYandex);
							}
							if(!empty($PageInfoAd['coords'])){
								$ex_coords = explode(',',$PageInfoAd['coords']);
								if(count($ex_coords)>1){
									$ex_coords = array($ex_coords[1],$ex_coords[0]);
								}
								else {
									$ex_coords = explode(' ',$PageInfoAd['coords']);
								}
								// $coordsInfo = $ex_coords[1].' '.$ex_coords[0];
								// $ex_coords = explode(' ',$coordsInfo);
								// echo $coordsInfo;
							}
							?>
							
							<script>
								ymaps.ready(function () {
									var myMap = new ymaps.Map('map', {
										center: [<?=$ex_coords[1]?>,<?=$ex_coords[0]?>],
										zoom: 15
									}, {
										searchControlProvider: 'yandex#search'
									}),
									myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
										hintContent: '<?=$PageInfoAd['name']?>',
										balloonContent: '<?=$PageInfoAd['address']?>'
									}, {
										// Опции.
										// Необходимо указать данный тип макета.
										iconLayout: 'default#image',
										// Своё изображение иконки метки.
										iconImageHref: '/images/neitralIcon.png',
										// Размеры метки.
										iconImageSize: [52, 44],
										// Смещение левого верхнего угла иконки относительно
										// её "ножки" (точки привязки).
										iconImageOffset: [-3, -42]
									});

									myMap.geoObjects.add(myPlacemark);
									
/* 									var myRouter = ymaps.route([
									  'м. <?=$PageInfoAd['station_id']?>', {
										type: "viaPoint",                   
										point: "<?=$PageInfoAd['address']?>"
									  },
									], {
									  mapStateAutoApply: true 
									});
									
									myRouter.then(function(route) {
										// Задание контента меток в начальной и конечной точках
										var points = route.getWayPoints();
										points.options.set('preset', 'islands#blackStretchyIcon');
										points.get(0).properties.set("iconContent", 'м. <?=$PageInfoAd['station_id']?>');
										points.get(1).properties.set("iconContent", 'Объект');
										// Добавление маршрута на карту
										myMap.geoObjects.add(route);
										var routeLength = route.getHumanLength();
										var routeLength2 = route.getLength();
										// alert(routeLength2);
									  },
									  // Обработка ошибки
									  function (error) {
										// alert("Возникла ошибка: " + error.message);
									  }
									) */
								});
								$(document).ready(function () {
									// $('#center .add_block .table_form input[name="s[address]"]').keyup(function(){
										// getCoords($(this));
									// });
								});
							</script>
							<div class="map"><div id="map"></div></div>
							<?
						echo '</div>
					</div>
				</div>';

				
				
				
	$rooms = '<div style="width:auto" class="select_block count">';
	$rooms_list = '';
	$nameParams = 'Не выбран';
	$valueInput = '';
	for($r=0; $r<=5; $r++){
		$name_param = $r;
		if($r==0){
			$name_param = 'Студия';
		}
		if($r==5){
			$name_param = '5+';
		}
		
		$current = "";
		if($r==0){
			if(!isset($PageInfoAd)){
				$current = ' class="current"';
			}
			$rooms_list .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не выбран</a></li>';
		}
		if($PageInfoAd['rooms']==$r){
			$current = ' class="current"';
			$nameParams = $name_param;
			$valueInput = ' value="'.$r.'"';
			$typeIsset = true;
		}
		$rooms_list .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$r.'">'.$name_param.'</a></li>';
	}

	$rooms .= '<input type="hidden" name="s[rooms]"'.$valueInput.'>';
	$rooms .= '<div style="width:121px" class="choosed_block">'.$nameParams.'</div>';
	$rooms .= '<div class="scrollbar-inner">';
	$rooms .= '<ul>';
	$rooms .= $rooms_list;
	$rooms .= '</ul>';
	$rooms .= '</div>';
	$rooms .= '</div>';

	if($PageInfoAd['estate']==3){
		$rooms = '<div class="checkbox_block rooms">';
		$ex_rooms = array(2);
		$valueInput = ' value="2"';
		if(isset($PageInfoAd['rooms']) && !empty($PageInfoAd['rooms'])){
			$ex_rooms = explode(',',$PageInfoAd['rooms']);
			$valueInput = ' value="'.$PageInfoAd['rooms'].'"';
		}
		$rooms .= '<input type="hidden" name="s[rooms]"'.$valueInput.'>';
		$rooms .= '<div class="select_checkbox">';
		$rooms .= '<ul>';
		$rooms .= '<li '.(in_array(2,$ex_rooms)?'class="checked"':'').'><a href="javascript:void(0)" data-id="2">2</a></li>';
		$rooms .= '<li '.(in_array(3,$ex_rooms)?'class="checked"':'').'><a href="javascript:void(0)" data-id="3">3</a></li>';
		$rooms .= '<li '.(in_array(4,$ex_rooms)?'class="checked"':'').'><a href="javascript:void(0)" data-id="4">4</a></li>';
		$rooms .= '<li '.(in_array(5,$ex_rooms)?'class="checked"':'').'><a href="javascript:void(0)" data-id="5">5+</a></li>';
		// $rooms .= '<li '.(in_array(6,$ex_rooms)?'class="checked"':'').'><a href="javascript:void(0)" data-id="6">6+</a></li>';
		$rooms .= '</ul>';
		$rooms .= '</div>';
		$rooms .= '</div>';
		$roomsParams = '<div class="row">
			<div class="cell label right one_second">
				<label>Комнат в квартире<b>*</b></label>
			</div>
			<div class="cell required one_second last_col">
				'.$rooms.'
			</div>
		</div>';
	}
	else {
		$roomsParams = '<div class="row">
			<div class="cell label right one_second">
				<label>Комнаты<b>*</b></label>
			</div>
			<div class="cell required one_second last_col">
				'.$rooms.'
			</div>
		</div>';
	}
		
	/*
	*  Вывод долей для квартир и комнат
	*/
	$share = '';
	if($PageInfoAd['estate']==2 || $PageInfoAd['estate']==3){
		$checked = '';
		$valueParams = '';
		$disabled = ' disabled="disabled"';
		$disabledParams = ' disabled="disabled"';
		$none = ' none';
		if(isset($PageInfoAd['share'])){
			if($PageInfoAd['share']){
				$disabled = '';
				$disabledParams = '';
				$checked = ' checked';
				$valueParams = ' value="'.$PageInfoAd['share'].'"';
				$none = '';
			}
		}
		// $share = '<div class="checkbox_block shares">
			// <input type="hidden" name="s[share]" value="0">
			// <input'.$disabled.' type="hidden" name="s[share]"'.$valueParams.'>
			// <div class="block_checkbox">
				// <ul>
					// <li class="vert_line"></li>
					// <li class="share'.$checked.'"><a href="javascript:void(0)" data-id="1">Доля</a></li>
					// <li class="shares'.$none.'"><input'.$disabledParams.' type="text" name="s[count_share]" value="'.$PageInfoAd['count_share'].'"></li>
					// <li class="hint'.$none.'">долей из</li>
					// <li class="shares'.$none.'"><input'.$disabledParams.' type="text" name="s[all_share]" value="'.$PageInfoAd['all_share'].'"></li>
				// </ul>
			// </div>
		// </div>';
		
		if($PageInfoAd['estate']==2){
			$roomsParams = '<div class="row">
				<div class="cell label right one_second">
					<label>Комнаты<b>*</b></label>
				</div>
				<div style="width:auto" class="cell required one_second last_col">
					'.$rooms.$share.'
				</div>
			</div>';		
		}
		if($PageInfoAd['estate']==3){
			$roomsParams = '<div class="row">
				<div class="cell label right one_second">
					<label>Комнаты<b>*</b></label>
				</div>
				<div style="width:auto" class="cell required one_second last_col">
					'.$rooms.$share.'
				</div>
			</div>';		
		}
	}
	
	/*
	*  Балкон
	*/
	$balcony = '';
	$balconis = '';
	$valueInput = '';
	for($c=0; $c<count($_TYPE_BALCONY_CAB); $c++){
		$checked = '';
		if(isset($PageInfoAd['balcony']) && $PageInfoAd['balcony']==$c){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';
		}
		$balconis .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_BALCONY_CAB[$c].'</a></li>';
	}
	$balcony = '<div class="checkbox_block">';
	$balcony .= '<input type="hidden" name="s[balcony]"'.$valueInput.'>';
	$balcony .= '<div class="select_checkbox">';
	$balcony .= '<ul>';
	$balcony .= $balconis;
	$balcony .= '</ul>';
	$balcony .= '</div>';
	$balcony .= '</div>';
	
	/*
	*  Лоджия
	*/
	$loggia = '';
	$balconis = '';
	$valueInput = '';
	for($c=0; $c<count($_TYPE_LOGGIA_CAB); $c++){
		$checked = '';
		if(isset($PageInfoAd['loggia']) && $PageInfoAd['loggia']==$c){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';
		}
		$balconis .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_LOGGIA_CAB[$c].'</a></li>';
	}
	$loggia = '<div class="checkbox_block">';
	$loggia .= '<input type="hidden" name="s[loggia]"'.$valueInput.'>';
	$loggia .= '<div class="select_checkbox">';
	$loggia .= '<ul>';
	$loggia .= $balconis;
	$loggia .= '</ul>';
	$loggia .= '</div>';
	$loggia .= '</div>';
	
	/*
	*  Санузел
	*/
	$wc = '';
	$wcs = '';
	$valueInput = '';
	for($c=0; $c<count($_TYPE_WC_CAB); $c++){
		$checked = '';
		$name_param = $_TYPE_WC_CAB[$c];
		if(isset($PageInfoAd['wc']) && $PageInfoAd['wc']==$c){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';
		}
		if($c==0){
			$name_param = 'Нет';
		}
		$wcs .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$name_param.'</a></li>';
	}
	$wc = '<div class="checkbox_block">';
	$wc .= '<input type="hidden" name="s[wc]"'.$valueInput.'>';
	$wc .= '<div class="select_checkbox">';
	$wc .= '<ul>';
	$wc .= $wcs;
	$wc .= '</ul>';
	$wc .= '</div>';
	$wc .= '</div>';
	
	/*
	*  Отделка
	*/
	$finish = '';
	$finishes = '';
	$valueInput = '';
	for($c=1; $c<count($_TYPE_FINISHING_CAB); $c++){
		$checked = '';
		if(isset($PageInfoAd['finishing']) && $PageInfoAd['finishing']==$c){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';
		}
		if(empty($PageInfoAd['finishing'])){
			if($c==1){
				$checked = ' class="checked"';
				$valueInput = ' value="'.$c.'"';				
			}
		}
		$finishes .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_FINISHING_CAB[$c].'</a></li>';
	}
	$finish = '<div class="checkbox_block">';
	$finish .= '<input type="hidden" name="s[finishing]"'.$valueInput.'>';
	$finish .= '<div class="select_checkbox">';
	$finish .= '<ul>';
	$finish .= $finishes;
	$finish .= '</ul>';
	$finish .= '</div>';
	$finish .= '</div>';
	
	/*
	*  Ремонт (загородная)
	*/
	$finishCountry = '';
	$finishCountries = '';
	$valueInput = '';
	for($c=1; $c<count($_TYPE_FINISHING_COUNTRY); $c++){
		$checked = '';
		if(isset($PageInfoAd['finishing']) && $PageInfoAd['finishing']==$c){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';
		}
		if(empty($PageInfoAd['finishing'])){
			if($c==1){
				$checked = ' class="checked"';
				$valueInput = ' value="'.$c.'"';				
			}
		}
		$finishCountries .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_FINISHING_COUNTRY[$c].'</a></li>';
	}
	$finishCountry = '<div class="checkbox_block">';
	$finishCountry .= '<input type="hidden" name="s[finishing]"'.$valueInput.'>';
	$finishCountry .= '<div class="select_checkbox">';
	$finishCountry .= '<ul>';
	$finishCountry .= $finishCountries;
	$finishCountry .= '</ul>';
	$finishCountry .= '</div>';
	$finishCountry .= '</div>';
	
	/*
	*  Отопление (загородная)
	*/
	$heatingCountry = '';
	$heatingCountries = '';
	$valueInput = '';
	for($c=0; $c<count($_HEATING_COUNTRY); $c++){
		$checked = '';
		if(isset($PageInfoAd['heating']) && $PageInfoAd['heating']==$c){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';
		}
		if(empty($PageInfoAd['heating'])){
			if($c==0){
				$checked = ' class="checked"';
				$valueInput = ' value="'.$c.'"';				
			}
		}
		$heatingCountries .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_HEATING_COUNTRY[$c].'</a></li>';
	}
	$heatingCountry = '<div class="checkbox_block">';
	$heatingCountry .= '<input type="hidden" name="s[heating]"'.$valueInput.'>';
	$heatingCountry .= '<div class="select_checkbox">';
	$heatingCountry .= '<ul>';
	$heatingCountry .= $heatingCountries;
	$heatingCountry .= '</ul>';
	$heatingCountry .= '</div>';
	$heatingCountry .= '</div>';
	
	/*
	*  Лифт
	*/
	$lift = '';
	$liftes = '';
	for($c=0; $c<count($_TYPE_LIFT); $c++){
		$checked = '';
		$valueName = $_TYPE_LIFT[$c];
		$value = $c;
		if($c==0 && !isset($PageInfoAd['lift'])){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$value.'"';
		}
		if(isset($PageInfoAd['lift']) && $PageInfoAd['lift']==$value){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$value.'"';
		}
		$liftes .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$value.'">'.$valueName.'</a></li>';
	}
	$lift = '<div class="checkbox_block">';
	$lift .= '<input type="hidden" name="s[lift]"'.$valueInput.'>';
	$lift .= '<div class="select_checkbox">';
	$lift .= '<ul style="margin-right:-20px;">';
	$lift .= $liftes;
	$lift .= '</ul>';
	$lift .= '</div>';
	$lift .= '</div>';
	
	/*
	*  Грузовой лифт
	*/
	$lift_service = '';
	$liftes = '';
	for($c=0; $c<count($_TYPE_LIFT); $c++){
		$checked = '';
		$valueName = $_TYPE_LIFT[$c];
		$value = $c;
		if($c==0 && !isset($PageInfoAd['lift_service'])){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$value.'"';
		}
		if(isset($PageInfoAd['lift_service']) && $PageInfoAd['lift_service']==$value){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$value.'"';
		}
		$liftes .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$value.'">'.$valueName.'</a></li>';
	}
	$lift_service = '<div class="checkbox_block">';
	$lift_service .= '<input type="hidden" name="s[lift_service]"'.$valueInput.'>';
	$lift_service .= '<div class="select_checkbox">';
	$lift_service .= '<ul style="margin-right:-20px;">';
	$lift_service .= $liftes;
	$lift_service .= '</ul>';
	$lift_service .= '</div>';
	$lift_service .= '</div>';

	/*
	*  Санитарная классификация
	*/
	$sanitary = '<div class="select_block count">';
	$sanitaries = '';
	$nameParams = 'Не важно';
	$valueInput = '';
	for($c=0; $c<count($_SANITARY); $c++){
		$current = '';
		if(isset($PageInfoAd['sanitary']) && $PageInfoAd['sanitary']==$c){
			$current = ' class="current"';
			$valueInput = ' value="'.$c.'"';
			$nameParams = $_SANITARY[$c];
		}
		$sanitaries .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_SANITARY[$c].'</a></li>';
	}
	$sanitary .= '<input type="hidden" name="s[sanitary]"'.$valueInput.'>';
	$sanitary .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
	$sanitary .= '<div class="scrollbar-inner">';
	$sanitary .= '<ul>';
	$sanitary .= $sanitaries;
	$sanitary .= '</ul>';
	$sanitary .= '</div>';
	$sanitary .= '</div>';
	
	/*
	*  Тип дома
	*/
	$type_house = '<div class="select_block count">';
	$type_houses = '';
	$nameParams = 'Не важно';
	$valueInput = '';
	$devs = mysql_query("
		SELECT *
		FROM ".$template."_type_house
		WHERE activation='1'
		ORDER BY num
	");
	if(mysql_num_rows($devs)>0){
		$n = 0;
		while($dev = mysql_fetch_assoc($devs)){
			$current = "";
			
			if($n==0 && !isset($PageInfoAd['type_house'])){
				$type_houses .= '<li class="current"><a href="javascript:void(0)" data-id="">Не важно</a></li>';
			}
			if($PageInfoAd['type_house']==$dev['id']){
				$current = ' class="current"';
				$nameParams = $dev['name'];
				$valueInput = ' value="'.$dev['id'].'"';
			}
			$type_houses .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$dev['id'].'">'.$dev['name'].'</a></li>';
			$n++;
		}
	}
	$type_house .= '<input type="hidden" name="s[type_house]"'.$valueInput.'>';
	$type_house .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
	$type_house .= '<div class="scrollbar-inner">';
	$type_house .= '<ul>';
	$type_house .= $type_houses;
	$type_house .= '</ul>';
	$type_house .= '</div>';
	$type_house .= '</div>';
	
	/*
	*  Сдача
	*/
	$ex_min_value_delivery_new_sell = explode(' ',$min_value_delivery_new_sell);
	$ex_max_value_delivery_new_sell = explode(' ',$max_value_delivery_new_sell);
	$_min_value_delivery_year = (int)$ex_min_value_delivery_new_sell[1];
	$_max_value_delivery_year = (int)$ex_max_value_delivery_new_sell[1];
	$yearNow = date("Y");
	if($_min_value_delivery_year<$yearNow-2){
		$_min_value_delivery_year = $yearNow-2;
	}

	$n = 1;
	$deliveryAdd = '';
	$deliveryArrayMin = '';
	$_QUATRE = array("I","II","III","IV");
	$currentParams = '';
	// echo '<pre>';
	// print_r($_min_value_delivery_year);
	// echo '</pre>';
	if($PageInfoAd['commissioning']){
		$currentParams = $PageInfoAd['commissioning'];
	}

	if($PageInfoAd['estate']==1 || $idParam==1){
		$queues = mysql_query("
			SELECT *
			FROM ".$template."_queues
			WHERE p_main='".$PageInfoAd['id']."' && type_name='new_flats'
		");
		if(mysql_num_rows($queues)>0){
			$queue = mysql_fetch_assoc($queues);
			$commissioning = $queue['commissioning'];
			$PageInfoAd['commissioning'] = $commissioning;
			$currentParams = $commissioning;
		}
		
		$current = "";
		$yearCurrent = explode(' ',$currentParams);
		$earlier = false;
		if(intval($yearCurrent[count($yearCurrent)-1])==($yearNow-3)){
			$current = ' class="current"';
			$earlier = true;
		}
		$deliveryAdd .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.($yearNow-3).'">Ранее</a></li>';
		for($c=$_min_value_delivery_year; $c<=$_max_value_delivery_year; $c++){
			$min_search = array_search($ex_min_value_delivery_new_sell[0],$_QUATRE);
			$max_search = array_search($ex_max_value_delivery_new_sell[0],$_QUATRE);
			if($c==$_min_value_delivery_year){
				for($q=$min_search; $q<count($_QUATRE); $q++){
					$q_full = $_QUATRE[$q].' '.$c;
					$q_name = $_QUATRE[$q]." кв. ".$c;
					$current = "";
					if($q_full==$currentParams){
						$current = ' class="current"';
						$deliveryArrayMin = $q_name;
					}
					$deliveryAdd .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$q_full.'">'.$q_name.'</a></li>';
					$n++;
				}
			}
			else if($c==$_max_value_delivery_year){
				for($q=0; $q<=$max_search; $q++){
					$q_name = $_QUATRE[$q]." кв. ".$c;
					$q_full = $_QUATRE[$q].' '.$c;
					$current = "";
					if($q_full==$currentParams){
						$current = ' class="current"';
						$deliveryArrayMin = $q_name;
					}
					$deliveryAdd .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$q_full.'">'.$q_name.'</a></li>';
					$n++;
				}
			}
			else {
				for($q=0; $q<count($_QUATRE); $q++){
					$q_name = $_QUATRE[$q]." кв. ".$c;
					$q_full = $_QUATRE[$q].' '.$c;
					$current = "";
					if($n==$currentParams){
						$current = ' class="current"';
						$deliveryArrayMin = $q_name;
					}
					$deliveryAdd .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.$q_full.'">'.$q_name.'</a></li>';
					$n++;
				}
			}
		}
		$current = "";
		$yearCurrent = explode(' ',$currentParams);
		$later = false;
		if(intval($yearCurrent[count($yearCurrent)-1])==$_max_value_delivery_year+1){
			$current = ' class="current"';
			$later = true;
		}
		$deliveryAdd .= '<li'.$current.'><a href="javascript:void(0)" data-id="'.($_max_value_delivery_year+1).'">Позднее</a></li>';
		$delivery .= '<div class="select_block count">';
		$nameParams = 'От';
		$valueParams = '';
		if(!empty($PageInfoAd['commissioning'])){
			$nameParams = $PageInfoAd['commissioning'];
			$ex_name = explode(' ',$nameParams);
			if(count($ex_name)==2){
				$nameParams = $ex_name[0].' кв. '.$ex_name[1];
			}
			$valueParams = ' value="'.$PageInfoAd['commissioning'].'"';
		}
		if($earlier){
			$nameParams = 'Ранее';
			$valueParams = ' value="'.($yearNow-2).'"';
		}
		if($later){
			$nameParams = 'Позднее';
			$valueParams = ' value="'.($_max_value_delivery_year+1).'"';
		}
		$delivery .= '<input type="hidden" name="queues[commissioning]"'.$valueParams.'>';
		$delivery .= '<div style="width:146px" class="choosed_block">'.$nameParams.'</div>';
		$delivery .= '<div class="scrollbar-inner">';
		$delivery .= '<ul>';
		$delivery .= $deliveryAdd;
		$delivery .= '</ul>';
		$delivery .= '</div>';
		$delivery .= '</div>';
	}
	
	/*
	*  Дополнительная информация
	*/
	$short_text = '';
	if(isset($PageInfoAd['short_text']) && !empty($PageInfoAd['short_text'])){
		$short_text = strip_tags(htmlspecialchars_decode($PageInfoAd['short_text']));
	}
	
	/*
	*  Лифты/подъемники
	*/
	$lifts = '';
	if(isset($PageInfoAd['lifts']) && !empty($PageInfoAd['lifts'])){
		$lifts = ' value="'.$PageInfoAd['lifts'].'"';
	}
	
	/*
	*  Этаж
	*/
	$floor = '';
	if(isset($PageInfoAd['floor']) && !empty($PageInfoAd['floor'])){
		$floor = ' value="'.$PageInfoAd['floor'].'"';
	}
		
	/*
	*  Кран-балка
	*/
	$cathead = '';
	if(isset($PageInfoAd['cathead']) && !empty($PageInfoAd['cathead'])){
		$cathead = ' value="'.str_replace(".", ",", $PageInfoAd['cathead']).'"';
	}
	
	/*
	*  Email
	*/
	$emailValue = '';
	if(isset($PageInfoAd['email']) && !empty($PageInfoAd['email'])){
		$emailValue = ' value="'.$PageInfoAd['email'].'"';
	}
	
	/*
	*  Тельфер
	*/
	$telpher = '';
	if(isset($PageInfoAd['telpher']) && !empty($PageInfoAd['telpher'])){
		$telpher = ' value="'.str_replace(".", ",", $PageInfoAd['telpher']).'"';
	}
	
	/*
	*  Общая площадь
	*/
	$full_square = '';
	if(isset($PageInfoAd['full_square']) && !empty($PageInfoAd['full_square'])){
		$full_square = ' value="'.str_replace(".", ",", $PageInfoAd['full_square']).'"';
	}
	
	/*
	*  Жилая площадь
	*/
	$live_square = '';
	if(isset($PageInfoAd['live_square']) && !empty($PageInfoAd['live_square'])){
		$live_square = ' value="'.str_replace(".", ",", $PageInfoAd['live_square']).'"';
	}
	
	/*
	*  Кухня
	*/
	$kitchen_square = '';
	if(isset($PageInfoAd['kitchen_square']) && !empty($PageInfoAd['kitchen_square'])){
		$kitchen_square = ' value="'.str_replace(".", ",", $PageInfoAd['kitchen_square']).'"';
	}
	
	/*
	*  Этажей в доме
	*/
	$floors = '';
	if(isset($PageInfoAd['floors']) && !empty($PageInfoAd['floors'])){
		$floors = ' value="'.$PageInfoAd['floors'].'"';
	}
	
	/*
	*  Количество спален
	*/
	$bedrooms = '';
	if(isset($PageInfoAd['bedrooms']) && !empty($PageInfoAd['bedrooms'])){
		$bedrooms = ' value="'.str_replace(".", ",", $PageInfoAd['bedrooms']).'"';
	}
	
	/*
	*  Высота потолков
	*/
	$ceiling_height = '';
	if(isset($PageInfoAd['ceiling_height']) && !empty($PageInfoAd['ceiling_height'])){
		$ceiling_height = ' value="'.str_replace(".", ",", $PageInfoAd['ceiling_height']).'"';
	}
	
	/*
	*  Год постройки
	*/
	$year = '';
	if(isset($PageInfoAd['year']) && !empty($PageInfoAd['year'])){
		$year = ' value="'.$PageInfoAd['year'].'"';
	}
	
	/*
	*  Площадь комнат
	*/
	$rooms_square = '';
	if(isset($PageInfoAd['rooms_square']) && !empty($PageInfoAd['rooms_square'])){
		$rooms_square = ' value="'.$PageInfoAd['rooms_square'].'"';
	}
	
	/*
	*  Очередь
	*/
	$queues = '';
	if(isset($PageInfoAd['queue']) && !empty($PageInfoAd['queue'])){
		$queues = ' value="'.$PageInfoAd['queue'].'"';
	}
	
	/*
	*  Корпус
	*/
	$building = '';
	if(isset($PageInfoAd['building']) && !empty($PageInfoAd['building'])){
		$building = ' value="'.$PageInfoAd['building'].'"';
	}
	
	/*
	*  Кадастровый номер
	*/
	$cadastr = '';
	if(isset($PageInfoAd['cadastr']) && !empty($PageInfoAd['cadastr'])){
		$cadastr = ' value="'.$PageInfoAd['cadastr'].'"';
	}
	
	/*
	*  ПЗЗ
	*/
	$pzz = '';
	if(isset($PageInfoAd['pzz']) && !empty($PageInfoAd['pzz'])){
		$pzz = ' value="'.$PageInfoAd['pzz'].'"';
	}
	
	/*
	*  ППЗ
	*/
	$ppz = '';
	if(isset($PageInfoAd['ppz']) && !empty($PageInfoAd['ppz'])){
		$ppz = ' value="'.$PageInfoAd['ppz'].'"';
	}
	
	/*
	*  Использование
	*/
	$used = '';
	if(isset($PageInfoAd['used']) && !empty($PageInfoAd['used'])){
		$used = ' value="'.$PageInfoAd['used'].'"';
	}
	
	/*
	*  Максимальная мощность
	*/
	$power_max = '';
	if(isset($PageInfoAd['power_max']) && !empty($PageInfoAd['power_max'])){
		$power_max = ' value="'.$PageInfoAd['power_max'].'"';
	}
	
	/*
	*  Название поселка
	*/
	$name_villiage = '';
	if(isset($PageInfoAd['name_villiage']) && !empty($PageInfoAd['name_villiage'])){
		$name_villiage = ' value="'.$PageInfoAd['name_villiage'].'"';
	}
	
	/*
	*  Площадь участка
	*/
	$area_square = '';
	if(isset($PageInfoAd['area_square']) && !empty($PageInfoAd['area_square'])){
		$area_square = ' value="'.$PageInfoAd['area_square'].'"';
	}

	/*
	*  Магазин
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['shop'])){
		if($PageInfoAd['shop']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['shop'].'"';
		}
	}
	$shop = '<div style="float:left;margin-right:40px" class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[shop]" value="0">
		<input type="hidden"'.$valueParams.' name="s[shop]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Магазин</span>
		</div>
	</div>';
	
	/*
	*  Бытовые услуги
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['domestic'])){
		if($PageInfoAd['domestic']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['domestic'].'"';
		}
	}
	$domestic = '<div style="float:left;margin-right:40px" class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[domestic]" value="0">
		<input type="hidden"'.$valueParams.' name="s[domestic]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Бытовые услуги</span>
		</div>
	</div>';
	
	/*
	*  Салон красоты
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['beauty'])){
		if($PageInfoAd['beauty']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['beauty'].'"';
		}
	}
	$beauty = '<div style="float:left;margin-right:40px" class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[beauty]" value="0">
		<input type="hidden"'.$valueParams.' name="s[beauty]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Салон красоты</span>
		</div>
	</div>';

	
	/*
	*  Аптека
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['pharmacy'])){
		if($PageInfoAd['pharmacy']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['pharmacy'].'"';
		}
	}
	$pharmacy = '<div style="float:left;margin-right:40px" class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[pharmacy]" value="0">
		<input type="hidden"'.$valueParams.' name="s[pharmacy]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Аптека</span>
		</div>
	</div>';

	
	/*
	*  Банк
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['bank'])){
		if($PageInfoAd['bank']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['bank'].'"';
		}
	}
	$bank = '<div style="float:left;margin-right:40px" class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[bank]" value="0">
		<input type="hidden"'.$valueParams.' name="s[bank]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Банк</span>
		</div>
	</div>';


	/*
	*  Камин
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['fireplace'])){
		if($PageInfoAd['fireplace']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['fireplace'].'"';
		}
	}
	$fireplace = '<div style="float:left;margin-right:40px" class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[fireplace]" value="0">
		<input type="hidden"'.$valueParams.' name="s[fireplace]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Камин</span>
		</div>
	</div>';

	/*
	*  Подвал
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['basement'])){
		if($PageInfoAd['basement']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['basement'].'"';
		}
	}
	$basement = '<div style="float:left;margin-right:40px" class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[basement]" value="0">
		<input type="hidden"'.$valueParams.' name="s[basement]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Подвал</span>
		</div>
	</div>';
		
	/*
	*  Дом сдан
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['ready'])){
		if($PageInfoAd['ready']==5){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['ready'].'"';
		}
	}
	$ready = '<div class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[ready]" value="0">
		<input type="hidden"'.$valueParams.' name="s[ready]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">дом сдан</span>
		</div>
	</div>';
	
	/*
	*  Паркинг
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['parking'])){
		if($PageInfoAd['parking']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['parking'].'"';
		}
	}
	$parking = '<div class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[parking]" value="0">
		<input type="hidden"'.$valueParams.' name="s[parking]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Паркинг</span>
		</div>
	</div>';
	
	/*
	*  Мусоропровод
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['rubbish'])){
		if($PageInfoAd['rubbish']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['rubbish'].'"';
		}
	}
	$rubbish = '<div class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[rubbish]" value="0">
		<input type="hidden"'.$valueParams.' name="s[rubbish]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Мусоропровод</span>
		</div>
	</div>';
	
	/*
	*  Охраняемая территория
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['protected_area'])){
		if($PageInfoAd['protected_area']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['protected_area'].'"';
		}
	}
	$protected_area = '<div class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[protected_area]" value="0">
		<input type="hidden"'.$valueParams.' name="s[protected_area]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Охраняемая территория</span>
		</div>
	</div>';
	
	/*
	*  Детская площадка
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['playground'])){
		if($PageInfoAd['playground']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['playground'].'"';
		}
	}
	$playground = '<div class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[playground]" value="0">
		<input type="hidden"'.$valueParams.' name="s[playground]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Детская площадка</span>
		</div>
	</div>';
	
	/*
	*  Спортивная площадка
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['sport_playground'])){
		if($PageInfoAd['sport_playground']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['sport_playground'].'"';
		}
	}
	$sport_playground = '<div class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[sport_playground]" value="0">
		<input type="hidden"'.$valueParams.' name="s[sport_playground]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Спортивная площадка</span>
		</div>
	</div>';
	
	/*
	*  Мебель
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['furniture'])){
		if($PageInfoAd['furniture']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['furniture'].'"';
		}
	}
	$furniture = '<div class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[furniture]" value="0">
		<input type="hidden"'.$valueParams.' name="s[furniture]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Мебель</span>
		</div>
	</div>';
	
	/*
	*  Кухонная мебель
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['furniture_kitchen'])){
		if($PageInfoAd['furniture_kitchen']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['furniture_kitchen'].'"';
		}
	}
	$furniture_kitchen = '<div class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[furniture_kitchen]" value="0">
		<input type="hidden"'.$valueParams.' name="s[furniture_kitchen]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Кухонная мебель</span>
		</div>
	</div>';
	
	/*
	*  Холодильник
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['fridge'])){
		if($PageInfoAd['fridge']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['fridge'].'"';
		}
	}
	$fridge = '<div class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[fridge]" value="0">
		<input type="hidden"'.$valueParams.' name="s[fridge]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Холодильник</span>
		</div>
	</div>';
	
	/*
	*  Можно с животными
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['animal'])){
		if($PageInfoAd['animal']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['animal'].'"';
		}
	}
	$animal = '<div class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[animal]" value="0">
		<input type="hidden"'.$valueParams.' name="s[animal]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Можно с животными</span>
		</div>
	</div>';
	
	/*
	*  Телевизор
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['tv'])){
		if($PageInfoAd['tv']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['tv'].'"';
		}
	}
	$tv = '<div class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[tv]" value="0">
		<input type="hidden"'.$valueParams.' name="s[tv]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Телевизор</span>
		</div>
	</div>';
	
	/*
	*  Кабельное ТВ
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['cab_tv'])){
		if($PageInfoAd['cab_tv']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['cab_tv'].'"';
		}
	}
	$cab_tv = '<div style="float:left" class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[cab_tv]" value="0">
		<input type="hidden"'.$valueParams.' name="s[cab_tv]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Кабельное ТВ</span>
		</div>
	</div>';
	
	/*
	*  Можно с детьми
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['children'])){
		if($PageInfoAd['children']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['children'].'"';
		}
	}
	$children = '<div class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[children]" value="0">
		<input type="hidden"'.$valueParams.' name="s[children]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Можно с детьми</span>
		</div>
	</div>';
	
	/*
	*  Комунальные платежи
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['communal'])){
		if($PageInfoAd['communal']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['communal'].'"';
		}
	}
	$communal = '<div class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[communal]" value="0">
		<input type="hidden"'.$valueParams.' name="s[communal]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span style="display:block;margin-top:2px" class="label">В стоимость <br>аренды включены коммунальные платежи <i>(кроме счетчиков)</i></span>
		</div>
	</div>';
	
	/*
	*  Стиральная машина
	*/
	$checked = '';
	$valueParams = '';
	$disabled = ' disabled="disabled"';
	if(isset($PageInfoAd['washer'])){
		if($PageInfoAd['washer']){
			$disabled = '';
			$checked = ' checked';
			$valueParams = ' value="'.$PageInfoAd['washer'].'"';
		}
	}
	$washer = '<div class="checkbox_single'.$checked.'">
		<input type="hidden" name="s[washer]" value="0">
		<input type="hidden"'.$valueParams.' name="s[washer]"'.$disabled.'>
		<div class="container">
			<span class="check"></span><span class="label">Стиральная машина</span>
		</div>
	</div>';
	
	/*
	*  Шаг колонн
	*/
	$column_space = '<div class="select_block count">';
	$column_spaces = '';
	$nameParams = 'Не важно';
	$valueInput = '';
	for($c=1; $c<=count($_COLUMN_SPACE); $c++){
		$checked = '';
		if($c==1){
			if(!isset($PageInfoAd['column_space'])){
				$checked = ' class="checked"';
			}
			$column_spaces .= '<li'.$checked.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
		}

		$checked = '';
		if(isset($PageInfoAd['column_space']) && $PageInfoAd['column_space']==$c){
			$checked = ' class="checked"';
			$valueInput = ' value="'.$c.'"';
			$nameParams = $_COLUMN_SPACE[$c];
		}	
		$column_spaces .= '<li'.$checked.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_COLUMN_SPACE[$c].'</a></li>';
	}
	$column_space .= '<input type="hidden" name="s[column_space]"'.$valueInput.'>';
	$column_space .= '<div style="width:137px" class="choosed_block">'.$nameParams.'</div>';
	$column_space .= '<div class="scrollbar-inner">';
	$column_space .= '<ul>';
	$column_space .= $column_spaces;
	$column_space .= '</ul>';
	$column_space .= '</div>';
	$column_space .= '</div>';

	
	/*
	*  Фотографии
	*/
	$estateName = '';
	if($PageInfoAd['estate']==1 || $idParam == 1){
		$estateName = 'new_flats';
	}
	if($PageInfoAd['estate']==2 || $idParam == 2){
		$estateName = 'flats';
	}
	if($PageInfoAd['estate']==3 || $idParam == 3){
		$estateName = 'rooms';
	}
	if($PageInfoAd['estate']==4 || $idParam == 4){
		$estateName = 'country';
	}
	if($PageInfoAd['estate']==5 || $idParam == 5){
		$estateName = 'commercial';
	}
	if($PageInfoAd['estate']==6 || $idParam == 6){
		$estateName = 'cession';
	}
	$photos_list = '';
	$idAd = 0;
	$addParams = " && user_id=".$PageInfoAd['user_id']."";
	
	if(!$PageInfoAd['user_id']){
		$addParams = " && user_id='".$_SESSION['idAuto']."'";
	}
	if(!empty($PageInfoAd['id']) || !empty($pageParams['id'])){
		$idAd = $PageInfoAd['id'];
		if(!empty($pageParams['id'])){
			$r = mysql_query("
				SELECT float_id
				FROM ".$template."_user_ads
				WHERE id=".$pageParams['id']." && activation=1
			");
			if(mysql_num_rows($r)>0){
				$rw = mysql_fetch_assoc($r);
				$idAd = $rw['float_id'];	
				$addParams = "";
			}
		}
	}
	
	$photos = mysql_query("
		SELECT *
		FROM ".$template."_photo_catalogue
		WHERE p_main='".$idAd."'".$addParams." && activation='1' && estate='".$estateName."'
		ORDER BY num
	");
	if(mysql_num_rows($photos)>0){
		while($photo = mysql_fetch_assoc($photos)){
			$img = explode(',',$photo['images']);
			if(!$PageInfoAd['user_id']){
				$image = '/users/'.$_SESSION['idAuto'].'/'.$img[1];
			}
			else {
				$image = '/users/'.$PageInfoAd['user_id'].'/'.$img[1];
			}
			if(empty($photo['user_id'])){
				$image = '/admin_2/uploads/'.$img[1];			
			}			
			$photos_list .= '<li data-id="'.$photo['id'].'"><input type="hidden" name="img[]" value="'.$photo['id'].'"><img width="170" height="110" src="'.$image.'"><span onclick="return deletePhoto(this)" class="close"></span></li>';
		}
	}
				// echo '<div class="reload_block">';				
				echo '<div id="step-2" class="container_block ads">';
				/*
				*  Новостройки и переуступки (информация о квартире и доме)
				*/
				if($PageInfoAd['estate']==1 || $idParam == 1 || $PageInfoAd['estate']==6 || $idParam == 6){
					require_once("include/mods/cabinet/new/info_place_new_sell.php");
				}
				
				/*
				*  Аренда квартиры (информация о квартире и доме)
				*/
				if($PageInfoAd['estate']==2 && $PageInfoAd['type']=='rent' || $idParam == 2 && $typeParam == 'rent'){
					require_once("include/mods/cabinet/flat/info_place_flat_rent.php");
				}
				
				/*
				*  Продажа квартиры (информация о квартире и доме)
				*/
				if($PageInfoAd['estate']==2 && $PageInfoAd['type']=='sell' || $idParam == 2 && $typeParam == 'sell'){
					require_once("include/mods/cabinet/flat/info_place_flat_sell.php");
				}
				
				/*
				*  Аренда комнаты (информация о квартире и доме)
				*/
				if($PageInfoAd['estate']==3 && $PageInfoAd['type']=='rent' || $idParam == 3 && $typeParam == 'rent'){
					require_once("include/mods/cabinet/room/info_place_room_rent.php");
				}
				
				/*
				*  Продажа комнаты (информация о квартире и доме)
				*/
				if($PageInfoAd['estate']==3 && $PageInfoAd['type']=='sell' || $idParam == 3 && $typeParam == 'sell'){
					require_once("include/mods/cabinet/room/info_place_room_sell.php");
				}
				
				/*
				*  Аренда загородной (информация о строении и участке)
				*/
				if($PageInfoAd['estate']==4 && $PageInfoAd['type']=='rent' || $idParam == 4 && $typeParam == 'rent'){
					require_once("include/mods/cabinet/country/info_place_country_rent.php");
				}
				
				/*
				*  Продажа загородной (информация о строении и участке)
				*/
				if($PageInfoAd['estate']==4 && $PageInfoAd['type']=='sell' || $idParam == 4 && $typeParam == 'sell'){
					require_once("include/mods/cabinet/country/info_place_country_sell.php");
				}
				
				/*
				*  Аренда коммерческой (информация о строении и участке)
				*/
				if($PageInfoAd['estate']==5 && $PageInfoAd['type']=='rent' || $idParam == 5 && $typeParam == 'rent'){
					require_once("include/mods/cabinet/commercial/info_place_commercial_rent.php");
				}
				
				/*
				*  Продажа коммерческой (информация о строении и участке)
				*/
				if($PageInfoAd['estate']==5 && $PageInfoAd['type']=='sell' || $idParam == 5 && $typeParam == 'sell'){
					require_once("include/mods/cabinet/commercial/info_place_commercial_sell.php");
				}
				
				echo '<div class="add_info">
						<div style="border:none" class="left_block">
							<div class="table_form">
								<h2 class="no-margin">Дополнительная информация</h2>
								<textarea placeholder="Расскажите ещё немного о Вашем объекте недвижимости..." name="s[short_text]">'.$short_text.'</textarea>
							</div>
						</div>
						<div style="border-left:1px solid #b5babe" class="right_block">
							<div class="table_form">
								<h2 class="no-margin">Фотографии объекта</h2>
							</div>
							<div style="margin-top:-5px" class="gallery_block">
								<div class="title">Первая загруженная фотография определяется как главная в галерее</div>
								<div class="photos_list">
									<ul style="overflow:hidden;margin-right:-18px">
										'.$photos_list.'
										<li>
											<div class="drag_drop_container">
												<div id="dropzone" class="dropzone"></div>
												<div class="load_images">
													<div class="load_image" id="uploadsImages"><a href="javascript:void(0)"><span>Загрузить фотографию</span></a></div>
												</div>
											</div>
										</li>
									</ul>
								</div>
								<div class="hint">
									<strong>Не допускаются к размещению!</strong>
									<p>Фотографии чужих объектов и рекламные изображения</p>
								</div>
							</div>
						</div>
					</div>';
				echo '</div>';
				
				/*
				*  Цена
				*/
				$price = '';
				if(isset($PageInfoAd['price']) && !empty($PageInfoAd['price'])){
					$price = ' value="'.$PageInfoAd['price'].'"';
				}
				
				/*
				*  Залог
				*/
				$deposit = '';
				if(isset($PageInfoAd['deposit']) && !empty($PageInfoAd['deposit'])){
					$deposit = ' value="'.$PageInfoAd['deposit'].'"';
				}
								
				/*
				*  Комиссия
				*/
				$prepayment = '';
				if(isset($PageInfoAd['prepayment']) && !empty($PageInfoAd['prepayment'])){
					$prepayment = ' value="'.$PageInfoAd['prepayment'].'"';
				}
				
				/*
				*  Показывать название комплекса (коммерция)
				*/
				$checked = '';
				$valueParams = '';
				$disabled = ' disabled="disabled"';
				if(isset($PageInfoAd['show_build'])){
					if($PageInfoAd['show_build']){
						$disabled = '';
						$checked = ' checked';
						$valueParams = ' value="'.$PageInfoAd['show_build'].'"';
					}
				}
				$showBuild = '<div class="checkbox_single'.$checked.'">
					<input type="hidden" name="s[show_build]" value="0">
					<input type="hidden"'.$valueParams.' name="s[show_build]"'.$disabled.'>
					<div class="container">
						<span style="margin-bottom:10px" class="check"></span><span class="label">Показывать в объявлении название комплекса, в котором расположен предлагаемый объект</span>
					</div>
				</div>';
				
				/*
				*  Ипотека
				*/
				$checked = '';
				$valueParams = '';
				$disabled = ' disabled="disabled"';
				if(isset($PageInfoAd['mortgages'])){
					if($PageInfoAd['mortgages']){
						$disabled = '';
						$checked = ' checked';
						$valueParams = ' value="'.$PageInfoAd['mortgages'].'"';
					}
				}
				$ipoteka = '<div class="checkbox_single'.$checked.'">
					<input type="hidden" name="s[mortgages]" value="0">
					<input type="hidden"'.$valueParams.' name="s[mortgages]"'.$disabled.'>
					<div class="container">
						<span class="check"></span><span class="label">Ипотека</span>
					</div>
				</div>';
				
				/*
				*  Субсидия
				*/
				$checked = '';
				$valueParams = '';
				$disabled = ' disabled="disabled"';
				if(isset($PageInfoAd['subsidies'])){
					if($PageInfoAd['subsidies']){
						$disabled = '';
						$checked = ' checked';
						$valueParams = ' value="'.$PageInfoAd['subsidies'].'"';
					}
				}
				$subsidies = '<div class="checkbox_single'.$checked.'">
					<input type="hidden" name="s[subsidies]" value="0">
					<input type="hidden"'.$valueParams.' name="s[subsidies]"'.$disabled.'>
					<div class="container">
						<span class="check"></span><span class="label">Субсидия</span>
					</div>
				</div>';
					
				/*
				*  Рассрочка
				*/
				$checked = '';
				$valueParams = '';
				$disabled = ' disabled="disabled"';
				if(isset($PageInfoAd['installment'])){
					if($PageInfoAd['installment']){
						$disabled = '';
						$checked = ' checked';
						$valueParams = ' value="'.$PageInfoAd['installment'].'"';
					}
				}
				$installment = '<div class="checkbox_single'.$checked.'">
					<input type="hidden" name="s[installment]" value="0">
					<input type="hidden"'.$valueParams.' name="s[installment]"'.$disabled.'>
					<div class="container">
						<span class="check"></span><span class="label">Рассрочка</span>
					</div>
				</div>';
					
				/*
				*  Телефон, указанный при регистрации
				*/
				$checked = '';
				$valueParams = '';
				$disabled = ' disabled="disabled"';
				$code_phone = '';
				$number_phone = '';
				$phoneReg = '';
				$res = mysql_query("
					SELECT code_phone,number_phone
					FROM ".$template."_phone_users
					WHERE user_id='".$_SESSION['idAuto']."' && type_phone='0' && activation='1'
				");
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$code_phone = $row['code_phone'];
					$number_phone = $row['number_phone'];
					if(!empty($code_phone) && !empty($number_phone)){
						$code_phone = str_replace("(", "", $code_phone);
						$code_phone = str_replace(")", "", $code_phone);
						$number_phone = str_replace("-", "", $number_phone);			
						$phoneReg = '+7 ('.$code_phone.') '.$number_phone;
					}
				}
				if(isset($PageInfoAd['reg_phone']) || $PageInfoAd['all_save']==0){
					if($PageInfoAd['reg_phone']){
						$disabled = '';
						$checked = ' checked';
						$valueParams = ' value="'.$PageInfoAd['reg_phone'].'"';
					}
					if($PageInfoAd['all_save']==0){
						$disabled = '';
						$checked = ' checked';
						$valueParams = ' value="1"';
					}
				}
				$reg_phone = '<div class="checkbox_single big'.$checked.'">
					<input type="hidden" name="s[reg_phone]" value="0">
					<input type="hidden"'.$valueParams.' name="s[reg_phone]"'.$disabled.'>
					<div class="container">
						<span class="check"></span><span class="label">'.$phoneReg.'</span>
					</div>
				</div>';
				
				/*
				*  Все телефоны
				*/
				$codeArray = array();
				$numberArray = array();
				$codeNumber1 = '';
				$codeNumber2 = '';
				$codePhone1 = '';
				$codePhone2 = '';
				$estateName = '';
				$type_names = '';
				if($PageInfoAd['estate']==1 || $idParam == 1){
					$type_names = 'new_flats';
				}
				if($PageInfoAd['estate']==2 || $idParam == 2){
					$type_names = 'flats';
				}
				if($PageInfoAd['estate']==3 || $idParam == 3){
					$type_names = 'rooms';
				}
				if($PageInfoAd['estate']==4 || $idParam == 4){
					$type_names = 'countries';
				}
				if($PageInfoAd['estate']==5 || $idParam == 5){
					$type_names = 'commercials';
				}
				if($PageInfoAd['estate']==6 || $idParam == 6){
					$type_names = 'cession';
				}
				$r = mysql_query("
					SELECT *
					FROM ".$template."_phone_ads
					WHERE user_id='".$_SESSION['idAuto']."' && float_id='".$PageInfoAd['float_id']."' && type='".$type_names."'
					ORDER BY num
				");
				if(mysql_num_rows($r)>0){
					while($rw = mysql_fetch_assoc($r)){
						array_push($codeArray,$rw['code_phone']);
						array_push($numberArray,$rw['number_phone']);
					}
					if(count($codeArray)>0){
						$codePhone1 = ' value="'.$codeArray[0].'"';
						$codePhone2 = ' value="'.$codeArray[1].'"';
					}
					if(count($numberArray)>0){
						$codeNumber1 = ' value="'.$numberArray[0].'"';
						$codeNumber2 = ' value="'.$numberArray[1].'"';
					}
				}
				
				$countNights = 0;
				/*
				*  Новостройки и переуступки (условия сделки)
				*/
				if($PageInfoAd['estate']==1 || $idParam == 1 || $PageInfoAd['estate']==6 || $idParam == 6){
					require_once("include/mods/cabinet/new/terms_transaction_new_sell.php");
					$countNights = 60;
				}
				
				/*
				*  Аренда квартиры (условия сделки)
				*/
				if($PageInfoAd['estate']==2 && $PageInfoAd['type']=='rent' || $idParam == 2 && $typeParam == 'rent'){
					require_once("include/mods/cabinet/flat/terms_transaction_flat_rent.php");
					$countNights = 60;
				}
				
				/*
				*  Продажа квартиры (условия сделки)
				*/
				if($PageInfoAd['estate']==2 && $PageInfoAd['type']=='sell' || $idParam == 2 && $typeParam == 'sell'){
					require_once("include/mods/cabinet/flat/terms_transaction_flat_sell.php");
					$countNights = 60;
				}
				
				/*
				*  Аренда комнаты (условия сделки)
				*/
				if($PageInfoAd['estate']==3 && $PageInfoAd['type']=='rent' || $idParam == 3 && $typeParam == 'rent'){
					require_once("include/mods/cabinet/room/terms_transaction_room_rent.php");
					$countNights = 60;
				}
				
				/*
				*  Продажа комнаты (условия сделки)
				*/
				if($PageInfoAd['estate']==3 && $PageInfoAd['type']=='sell' || $idParam == 3 && $typeParam == 'sell'){
					require_once("include/mods/cabinet/room/terms_transaction_room_sell.php");
					$countNights = 60;
				}
				
				/*
				*  Аренда загородной (условия сделки)
				*/
				if($PageInfoAd['estate']==4 && $PageInfoAd['type']=='rent' || $idParam == 4 && $typeParam == 'rent'){
					require_once("include/mods/cabinet/country/terms_transaction_country_rent.php");
					$countNights = 60;
				}
				
				/*
				*  Продажа загородной (условия сделки)
				*/
				if($PageInfoAd['estate']==4 && $PageInfoAd['type']=='sell' || $idParam == 4 && $typeParam == 'sell'){
					require_once("include/mods/cabinet/country/terms_transaction_country_sell.php");
					$countNights = 60;
				}
				
				/*
				*  Аренда коммерческой (условия сделки)
				*/
				if($PageInfoAd['estate']==5 && $PageInfoAd['type']=='rent' || $idParam == 5 && $typeParam == 'rent'){
					require_once("include/mods/cabinet/commercial/terms_transaction_commercial_rent.php");
					$countNights = 60;
				}
				
				/*
				*  Продажа коммерческой (условия сделки)
				*/
				if($PageInfoAd['estate']==5 && $PageInfoAd['type']=='sell' || $idParam == 5 && $typeParam == 'sell'){
					require_once("include/mods/cabinet/commercial/terms_transaction_commercial_sell.php");
					$countNights = 60;
				}
				
				$_ESTATES_CONVERSION = array(1=>'new',2=>'flat',3=>'room',4=>'country',5=>'commercial');
				
				$costTariff = 0;
				$paramsAdd = "type='".$typeParam."' && estate='".$_ESTATES_CONVERSION[$idParam]."'";
				if(!isset($idParam) || !isset($typeParam)){
					$paramsAdd = "type='sell' && estate='new'";
				}
				
				$costs = mysql_query("
					SELECT *
					FROM tariffs
					WHERE ".$paramsAdd."
				");
				if(mysql_num_rows($costs)>0){
					$cost = mysql_fetch_assoc($costs);
					$costTariff = $cost['color'];
				}
				
				$count_days = $countNights;
				$today = date("Y-m-d");
				if($PageInfoAd['date']+(59*86400)>=strtotime($today)){ // dateplus
					$day = $PageInfoAd['date']+(59*86400) - strtotime($today);
					$day = ceil($day/86400);
					$days = '<div class="days">Осталось '.$day.' дн.</div>';//pay, none
					$count_days = $day;
				}
				
				$btnBack = '<label class="btn link"><a onclick="return archiveFunc(this)" class="back_page" href="javascript:void(0)"><span>Сохранить в архив</span></a></label>';
				$btnPage = '<label class="btn"><input type="submit" value="Разместить объявление"><span class="angle-right"></span></label>';
/* 				if($PageInfoAd){
					$btnBack = '<label class="btn link"><a class="back_page" href="javascript:void(0)"><span>Сохранить в архив</span></a></label>';
					$btnPage = '<label class="btn"><input type="submit" value="Сохранить изменения"><span class="angle-right"></span></label>';				
				}
 */				
				$checkedPay = '';
				$checkedFree = ' checked';
				if($PageInfoAd['special']==2){
					$checkedPay = ' checked';
					$checkedFree = '';
				}
				
				$valueAdds = ' value="3"';
				if(isset($PageInfoAd['special'])){
					$valueAdds = ' value="'.$PageInfoAd['special'].'"';
				}
				echo '<div id="step-4" class="container_block ads">
					<input type="hidden" name="ads_save" value="true"> 
					<div class="conditions_block">
						<div class="table_form">
							<h2>Условия размещения</h2>
							<div class="placement_choose">
								<div class="placement_list">
									<input type="hidden" name="ads[ads_type]"'.$valueAdds.'>
									<div class="item'.$checkedPay.'">
										<a data-id="2" data-cost="'.$costTariff.'" href="javascript:void(0)">
											<span class="middle">
												<span class="check"></span><span style="width:420px" class="name">Выделение цветом до отключения вами <br>опции, либо окончания срока размещения</span><span class="cost"><strong>'.$costTariff.'</strong> руб./сутки</span>
											</span>
										</a>
									</div>
									<div class="item'.$checkedFree.'">
										<a data-id="3" data-cost="0" href="javascript:void(0)">
											<span class="middle">
												<span class="check"></span><span class="name">Бесплатное</span><span class="time">Срок размещения: <strong>'.$countNights.' дней</strong></span>
											</span>
										</a>
									</div>
								</div>
								<div class="count_days">Оставшийся срок размещения на сайте: <strong>'.$count_days.' дней</strong>.</div>
								<div class="total">На Вашем счету <strong>'.$costAccount.'</strong> руб. <a target="_blank" href="/payment">Пополнить счёт сейчас</a></div>
								<div style="position:absolute;bottom:12px;right:-160px;width:430px;" class="block">
									<div class="row">
										<div class="cell auto last_col">
											'.$showBuild.'
										</div>
									</div>
								</div>
							</div>

							<div class="inform_block">
								<!--<div class="block">
									<div class="row">
										<div style="margin-top:-6px;width:200px!important;" class="cell left label one_second">Выделить объявление цветом<div class="color"><strong>+5</strong> руб./сутки</div></div>
										<div class="cell auto last_col">
											'.$ipoteka.'
										</div>
									</div>
								</div>-->
								<div style="margin-top:0" class="total_sum">
									<div class="sum_conditions">Итого: <span><strong>0</strong> руб./сутки</span></div>
									<div class="descriptions">
										<div data-placement="1" class="item current">
											<div class="hint">Публикация объявления оплачивается ежесуточно путем автоматического списания средств с Вашего счета</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="btn_info cost">
						<div class="table_form">
							<div class="row btn_row">
								<div class="cell center full last_col">
									'.$btnBack.'
									'.$btnPage.'
								</div>
							</div>
							<div class="row">
								<div class="cell full">
									<div class="req"><b>*</b> - поля обязательные для заполнения</div>
								</div>
							</div>
							<div style="margin-top:0px" class="row">
								<div class="cell full">
									<div class="req"><b>*</b><b>*</b> - опция действует до принудительного отключения вами или до окончания срока размещения объекта на сайте</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>';
							
			echo '</form>
			<div class="fixed_block">
				<a onclick="return sendForma(this,\'problem\')" href="javascript:void(0)"><span>Возникла проблема?</span></a>
			</div>
		</div>
	</div>';
?>