<? session_start();
include('../admin_2/include/bd.php');
include('../admin_2/include/functions.php');
//ширина и высота в пикселях
$pic_weight = 5000;
$pic_height = 5000;
$action = 'error';

if(isset($_SESSION['idAuto'])){
	if(isset($_FILES)){
	  $data = array();
	  //пролистываем весь массив изображений по одному $_FILES['file']['name'] as $k=>$v
	  foreach ($_FILES['uploadfile']['name'] as $k=>$v){
		
		//директория загрузок
		$uploaddir = "../users/".$_SESSION['idAuto']."/"; // Оригинал
		$big = 		 "../users/".$_SESSION['idAuto']."/"; // Большое изображение по ширине 450px
		$large = 	 "../users/".$_SESSION['idAuto']."/"; // 216x175
		$middle = 	 "../users/".$_SESSION['idAuto']."/"; // 106x106
		$min = 		 "../users/".$_SESSION['idAuto']."/"; // 45x45
		
		//новое имя изображения
		$apend  = substr(md5(date('YmdHis').rand(100,1000)),0,20).".jpg";
		$apend1 = substr(md5(date('YmdHis').rand(100,1000)),0,20).".jpg";
		$apend2 = substr(md5(date('YmdHis').rand(100,1000)),0,20).".jpg";
		$apend3 = substr(md5(date('YmdHis').rand(100,1000)),0,20).".jpg";
		$apend4 = substr(md5(date('YmdHis').rand(100,1000)),0,20).".jpg";
		
		//путь к новому изображению
		$uploadfile = "$uploaddir$apend";
		$bigfile = "$big$apend1";
		$largefile = "$large$apend2";
		$middlefile = "$middle$apend3";
		$minfile = "$min$apend4";
		
		//Проверка расширений загружаемых изображений
		if($_FILES['uploadfile']['type'][$k] == "image/gif" || $_FILES['uploadfile']['type'][$k] == "image/png" ||
		$_FILES['uploadfile']['type'][$k] == "image/jpg" || $_FILES['uploadfile']['type'][$k] == "image/jpeg"){
		  //черный список типов файлов
		  $blacklist = array(".php", ".phtml", ".php3", ".php4");
		  foreach ($blacklist as $item){
			if(preg_match("/$item\$/i", $_FILES['uploadfile']['name'][$k]))
			{
			  echo "Нельзя загружать скрипты.";
			  exit;
			}
		  }
	 
		  //перемещаем файл из временного хранилища
		  if(move_uploaded_file($_FILES['uploadfile']['tmp_name'][$k], $uploadfile)){
			//получаем размеры файла
			$size = getimagesize($uploadfile);
			
			//проверяем размеры файла, если они нам подходят, то оставляем файл
			if ($size[0] < $pic_weight && $size[1] < $pic_height){
				if($size[0] > $size[1]){
					resize($uploadfile,$bigfile, 400, 0,false);

					resize($uploadfile,$largefile, 0, 103,false);
					crop($largefile,$largefile, array(0, 0, 103, 103),false,103,$position = 'center',$type_img = 'H');
					
					resize($bigfile,$middlefile, 0, 32,false);
					crop($middlefile,$middlefile, array(0, 0, 32, 32),false,32,$position = 'center',$type_img = 'H');
				}
				else {
					resize($uploadfile,$bigfile, 400, 0,false);

					resize($uploadfile,$largefile, 103, 0,false);
					crop($largefile,$largefile, array(0, 0, 103, 103),false,103,$position = 'center',$type_img = 'V');
					
					resize($bigfile,$middlefile, 32, 0,false);
					crop($middlefile,$middlefile, array(0, 0, 32, 32),false,32,$position = 'center',$type_img = 'V');
				}
				list($w_size, $h_size) = getimagesize($bigfile);
				$middlefile = explode("/",$middlefile);
				$largefile = explode("/",$largefile);
				$bigfile = explode("/",$bigfile);
				$uploadfile = explode("/",$uploadfile);
				
				$middlefile = $middlefile[count($middlefile)-1];
				$largefile = $largefile[count($largefile)-1];
				$bigfile = $bigfile[count($bigfile)-1];
				$uploadfile = $uploadfile[count($uploadfile)-1];
				
				$arr2 = $middlefile.",".$largefile.",".$bigfile.",".$uploadfile;
				$coords = '0,103,0,103,103,103';
				$update = mysql_query ("
					UPDATE ".$template."_users
					SET avatar='".$arr2."',ava_set='".$coords."'  
					WHERE id='".$_SESSION['idAuto']."' && activation='1'
				");
				
				if($update=='true'){
					$action = 'success';
					$arr = array($action,$_SESSION['idAuto'],$middlefile,$largefile,$bigfile,$w_size,$h_size,"0","103","0","103");
					array_push($data,$arr);
				}
			}
			//если размеры файла нам не подходят, то удаляем файл unlink($uploadfile);
			else {
			  echo "<center><br>Размер пикселей превышает допустимые нормы.</center>";
			  unlink($uploadfile);
			}
		  }
		  else
			echo "<center><br>Файл не загружен, вернитесь и попробуйте еще раз.</center>";
		}
		else
		  echo "<center><br>Можно загружать только изображения в форматах jpg, jpeg, gif и png.</center>";
	  }
		echo json_encode($data);
	}
}
else {
	$data = array("error");
	echo json_encode($data);
}
?>