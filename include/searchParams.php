<?php session_start();
/*
*  Свойство, позволяющее искать только пользовательские объекты
*/
$realUserParam = " && user_id!='0'";
$realUserParam = "";
$realUserParamBuild = " && f.user_id!='0'";
$realUserParamBuild = "";
$realUserParamFlat = " && s.user_id!='0'";
$realUserParamFlat = "";

/*
*  Минимальные и максимальные значения сроков сдачи новостроек
*/
$min_value_delivery_new_sell = '';
$max_value_delivery_new_sell = '';
$valueParamsDelivery = array();
$descParamsName = array();
$descParams = array();
$relParamsSearch = array();

$start = microtime(true);
$LEFT_JOIN_DELIVERY_MIN = '';
$res = mysql_query("
	SELECT commissioning
	FROM ".$template."_queues
	WHERE type_name='new'
	ORDER BY commissioning
");
if(mysql_num_rows($res)>0){
	$arrayValues = array();
	while($row = mysql_fetch_assoc($res)){
		$commissioning = trim($row['commissioning']);
		if(!empty($commissioning) && $commissioning!='Дом сдан' && $commissioning!='Заселен' && $commissioning!='Собств-ть'){
			array_push($arrayValues,$row['commissioning']);
		}
	}
	if(count($arrayValues)>0){
		for($a=0; $a<count($arrayValues); $a++){
			if($a==0){
				$min_value_delivery_new_sell = $arrayValues[$a];
			}
			if($a+1==count($arrayValues)){
				$max_value_delivery_new_sell = $arrayValues[$a];
			}
		}
	}
	$ex = explode(' ',$min_value_delivery_new_sell);
	$ex2 = explode(' ',$max_value_delivery_new_sell);
	if(count($ex)==1){
		$min_value_delivery_new_sell = 'I '.$min_value_delivery_new_sell;
	}
	if(count($ex2)==1){
		$max_value_delivery_new_sell = 'I '.$max_value_delivery_new_sell;
	}
}

/*
*  Минимальные и максимальные значения сроков сдачи переуступок
*/
$min_value_delivery_cession_sell = '';
$max_value_delivery_cession_sell = '';
$res = mysql_query("
	SELECT commissioning
	FROM ".$template."_queues
	WHERE type_name='cession'
	ORDER BY commissioning
");
if(mysql_num_rows($res)>0){
	$arrayValues = array();
	while($row = mysql_fetch_assoc($res)){
		$commissioning = trim($row['commissioning']);
		if(!empty($commissioning) && $commissioning!='Дом сдан' && $commissioning!='Заселен' && $commissioning!='Собств-ть'){
			array_push($arrayValues,$row['commissioning']);
		}
	}
	if(count($arrayValues)>0){
		for($a=0; $a<count($arrayValues); $a++){
			if($a==0){
				$min_value_delivery_cession_sell = $arrayValues[$a];
			}
			if($a+1==count($arrayValues)){
				$max_value_delivery_cession_sell = $arrayValues[$a];
			}
		}
	}
}

$_QUATRE = array("I","II","III","IV");

$arrayRedirectParams = array(
	"type" => array(
		"sell" => "kupit",
		"rent" => "snyat",
	),
	"estate" => array(
		"1" => "novostroyka",
		"2" => "kvartira",
		"3" => "komnata",
		"4" => "zagorodnaya",
		"5" => "kommercheskaya",
		"6" => "pereustupka",
	),
	"rooms" => array(
		"0" => "studiya",
		"1" => "1-komnata",
		"2" => "2-komnata",
		"3" => "3-komnata",
		"4" => "4-komnata",
		"5" => "5-komnata",
		"6" => "6-komnata",
		"7" => "7-komnata",
		"8" => "8-komnata",
		"9" => "9-komnata",
	),
	"priceAll" => array(
		"all" => "all",
		"meters" => "meters",
	),
	"furniture" => array(
		"1" => "s-mebelju",
		"2" => "bez-mebeli",
	),
	"finishing" => array(
		"1" => "bez-otdelki",
		"2" => "chernovaya",
		"3" => "predchistovaya",
		"4" => "otdelka-pod-klyuch",
	),
	"repairs" => array(
		"1" => "s-remontom",
		"2" => "bez-remonta",
		"3" => "evrootdelka",
	),
	"transaction" => array(
		"1" => "pryamaya-prodaja",
		"2" => "dogovor-dareniya",
		"3" => "vstrechnaya-pokupka",
	),
	"type_commer" => array(
		"0" => "all-commerce",
		"1" => "office",
		"2" => "premise",
		"3" => "auto",
		"4" => "building",
		"5" => "factory",
		"6" => "areas",
		"7" => "business",
		"8" => "garage",
	),
	"type_country" => array(
		"1" => "cottage-village",
		"2" => "townhouse",
		"3" => "house",
		"4" => "land-plot",
	),
	"subtype_country" => array(
		"1" => "cottage",
		"2" => "areas2",
	),
	"rooms_flat" => array(
		"2" => "2-komnaty",
		"3" => "3-komnaty",
		"4" => "4-komnaty",
	),
	"period" => array(
		"1" => "long",
		"2" => "few-months",
		"3" => "daily",
	),
);

$arrayRedirectSearch = array();
if($_SERVER['QUERY_STRING']=='search'){
	$search_param = preg_replace("/\/\?/", '', urldecode($_SERVER['REQUEST_URI']));
	$search_param = str_replace("/", "", $search_param);
	$search_param = str_replace("search?", "", $search_param);
	$findParamArray = array();
	$search_param = explode("&",$search_param);
	foreach($search_param as $name => $value){
		$url_par = explode('=',$value);
		$findParameter = false;
		list($n, $v) = $url_par;
		if($n=='area' || $n=='station' || $n=='build' || $n=='developer'){
			array_push($descParamsName,$n);
		}
		if($n=='rooms' || $n=='area' || $n=='station' || $n=='city'){
			$ex_value = explode(',',$v);
			if($n=='rooms'){
				for($e=0;$e<count($ex_value);$e++){
					if($arrayRedirectParams[$n][$ex_value[$e]]){
						array_push($arrayRedirectSearch,$arrayRedirectParams[$n][$ex_value[$e]]);
					}
				}
			}
			if($n=='area'){
				$p_arrays = array();
				for($e=0;$e<count($ex_value);$e++){
					array_push($p_arrays,"id='".$ex_value[$e]."'");
				}
				$add = '';
				if(count($p_arrays)>1){
					$add = " && (".implode(' || ',$p_arrays).")";
				}
				else {
					$add = " && ".$p_arrays[0];
				}
				$res = mysql_query("
					SELECT *
					FROM ".$template."_areas
					WHERE activation='1'".$add."
				");
				if(mysql_num_rows($res)>0){
					while($row = mysql_fetch_assoc($res)){
						array_push($arrayRedirectSearch,$row['link']);
					}
				}
			}
			if($n=='station'){
				$p_arrays = array();
				for($e=0;$e<count($ex_value);$e++){
					array_push($p_arrays,"id='".$ex_value[$e]."'");
				}
				$add = '';
				if(count($p_arrays)>1){
					$add = " && (".implode(' || ',$p_arrays).")";
				}
				else {
					$add = " && ".$p_arrays[0];
				}
				$res = mysql_query("
					SELECT *
					FROM ".$template."_stations
					WHERE activation='1'".$add."
				");
				if(mysql_num_rows($res)>0){
					while($row = mysql_fetch_assoc($res)){
						array_push($arrayRedirectSearch,$row['link']);
					}
				}
			}
			if($n=='city'){
				$p_arrays = array();
				for($e=0;$e<count($ex_value);$e++){
					array_push($p_arrays,"id='".$ex_value[$e]."'");
				}
				$add = '';
				if(count($p_arrays)>1){
					$add = " && (".implode(' || ',$p_arrays).")";
				}
				else {
					$add = " && ".$p_arrays[0];
				}
				$res = mysql_query("
					SELECT *
					FROM ".$template."_location
					WHERE activation='1'".$add."
				");
				if(mysql_num_rows($res)>0){
					while($row = mysql_fetch_assoc($res)){
						array_push($arrayRedirectSearch,$row['link']);
					}
				}
			}
			$findParameter = true;
		}
		else {
			if($arrayRedirectParams[$n][$v]){
				array_push($arrayRedirectSearch,$arrayRedirectParams[$n][$v]);
				$findParameter = true;
			}
		}
		if(!$findParameter){
			array_push($findParamArray,$n.'='.$v);
		}
	}
	$dinamicLink = true;
}
if($dinamicLink){
	$linkPageGenerator = '/search/'.implode('/',$arrayRedirectSearch);
	if(count($findParamArray)>0){
		$linkPageGenerator = $linkPageGenerator.'?'.implode('&',$findParamArray);
	}
	if(count($descParamsName)>0){
		$_SESSION['location'] = $descParamsName;
	}
	header("Status: 301 Moved Permanently");
	header("Location: https://".$_SERVER['HTTP_HOST'].$linkPageGenerator);
	exit;
}
else {
	// echo '<pre>';
	// print_r($_SERVER);
	// echo '</pre>';
	$ex_local_link = explode('?',$_SERVER['REQUEST_URI']);
	$linkWithSlash = $ex_local_link[0];
	$linkDinamical = $ex_local_link[1];
	$ex_local_link = explode('/',$linkWithSlash);
	$ex_local_link2 = array();
	$allCount = 0;
	for($l=0; $l<count($ex_local_link); $l++){
		if($ex_local_link[$l]=='all'){
			if($allCount==0){
				array_push($ex_local_link2,$ex_local_link[$l]);
			}
			$allCount++;
		}
		else {
			array_push($ex_local_link2,$ex_local_link[$l]);
		}
	}
	if($allCount>0){
		if(!empty($linkDinamical)){
			$linkDinamical = '?'.$linkDinamical;
		}
		// header("Status: 301 Moved Permanently");
		// header("Location: https://".$_SERVER['HTTP_HOST'].implode('/',$ex_local_link2).$linkDinamical);
		// exit;
	}
}
/*===================================================================*/




$arrayLinkSearch = array();
$arrayLinkSearch2 = array();
$url_param = array();

$citiesArrays = array();
$areasArrays = array();
$stationsArrays = array();
$typesArray = array(
	"kupit" => "sell",
	"snyat" => "rent",
);

$estatesArray = array(
	"novostroyka" => 1,
	"kvartira" => 2,
	"komnata" => 3,
	"zagorodnaya" => 4,
	"kommercheskaya" => 5,
	"pereustupka" => 6,
);

$roomsArray = array(
	"studiya" => 0,
	"1-komnata" => 1,
	"2-komnata" => 2,
	"3-komnata" => 3,
	"4-komnata" => 4,
	"5-komnata" => 5,
	"6-komnata" => 6,
	"7-komnata" => 7,
	"8-komnata" => 8,
	"9-komnata" => 9,
);

/*
*  Массив городов
*/
$res = mysql_query("
	SELECT *
	FROM ".$template."_location
	WHERE activation='1'
");
if(mysql_num_rows($res)>0){
	while($row = mysql_fetch_assoc($res)){
		$arr = array(
			"id" => $row['id'],
			"link" => $row['link'],
			"name" => $row['name'],
		);
		array_push($citiesArrays,$arr);
	}
}

/*
*  Массив районов
*/
$res = mysql_query("
	SELECT *
	FROM ".$template."_areas
	WHERE activation='1'
");
if(mysql_num_rows($res)>0){
	while($row = mysql_fetch_assoc($res)){
		$arr = array(
			"id" => $row['id'],
			"link" => $row['link'],
			"name" => $row['title'],
		);
		array_push($areasArrays,$arr);
	}
}

/*
*  Массив станций метро
*/
$res = mysql_query("
	SELECT *
	FROM ".$template."_stations
	WHERE activation='1'
");
if(mysql_num_rows($res)>0){
	while($row = mysql_fetch_assoc($res)){
		$arr = array(
			"id" => $row['id'],
			"link" => $row['link'],
			"name" => $row['title'],
		);
		array_push($stationsArrays,$arr);
	}
}

/*
*  Массив подразделов загородной
*/
$arrayTypeCountry = array(
	0 => array(
		"link" => "cottage-village",
		"key" => "type_country",
		"value" => 1,
	),
	1 => array(
		"link" => "townhouse",
		"key" => "type_country",
		"value" => 2,
	),
	2 => array(
		"link" => "house",
		"key" => "type_country",
		"value" => 3,
	),
	3 => array(
		"link" => "land-plot",
		"key" => "type_country",
		"value" => 4,
	),
	4 => array(
		"link" => "cottage",
		"key" => "subtype_country",
		"value" => 5,
	),
	5 => array(
		"link" => "areas2",
		"key" => "subtype_country",
		"value" => 6,
	),
);

$anotherLinksArray = array(
	0 => array(
		"link" => "all",
		"key" => "priceAll",
		"value" => "all",
	),
	1 => array(
		"link" => "meters",
		"key" => "priceAll",
		"value" => "meters",
	),
	2 => array(
		"link" => "s-mebelju",
		"key" => "furniture",
		"value" => 1,
	),
	3 => array(
		"link" => "bez-mebeli",
		"key" => "furniture",
		"value" => 2,
	),
	4 => array(
		"link" => "bez-otdelki",
		"key" => "finishing",
		"value" => 1,
	),
	5 => array(
		"link" => "chernovaya",
		"key" => "finishing",
		"value" => 2,
	),
	6 => array(
		"link" => "predchistovaya",
		"key" => "finishing",
		"value" => 3,
	),
	7 => array(
		"link" => "otdelka-pod-klyuch",
		"key" => "finishing",
		"value" => 4,
	),
	8 => array(
		"link" => "s-remontom",
		"key" => "repairs",
		"value" => 1,
	),
	9 => array(
		"link" => "bez-remonta",
		"key" => "repairs",
		"value" => 2,
	),
	10 => array(
		"link" => "evrootdelka",
		"key" => "repairs",
		"value" => 3,
	),
	11 => array(
		"link" => "pryamaya-prodaja",
		"key" => "transaction",
		"value" => 1,
	),
	12 => array(
		"link" => "dogovor-dareniya",
		"key" => "transaction",
		"value" => 2,
	),
	13 => array(
		"link" => "vstrechnaya-pokupka",
		"key" => "transaction",
		"value" => 3,
	),
	14 => array(
		"link" => "2-komnaty",
		"key" => "rooms_flat",
		"value" => 2,
	),
	15 => array(
		"link" => "3-komnaty",
		"key" => "rooms_flat",
		"value" => 3,
	),
	16 => array(
		"link" => "4-komnaty",
		"key" => "rooms_flat",
		"value" => 4,
	),
	17 => array(
		"link" => "long",
		"key" => "period",
		"value" => 1,
	),
	18 => array(
		"link" => "few-months",
		"key" => "period",
		"value" => 2,
	),
	19 => array(
		"link" => "daily",
		"key" => "period",
		"value" => 3,
	),
	20 => array(
		"link" => "office",
		"key" => "type_commer",
		"value" => 1,
	),
	21 => array(
		"link" => "premise",
		"key" => "type_commer",
		"value" => 2,
	),
	22 => array(
		"link" => "auto",
		"key" => "type_commer",
		"value" => 3,
	),
	23 => array(
		"link" => "building",
		"key" => "type_commer",
		"value" => 4,
	),
	24 => array(
		"link" => "factory",
		"key" => "type_commer",
		"value" => 5,
	),
	25 => array(
		"link" => "areas",
		"key" => "type_commer",
		"value" => 6,
	),
	26 => array(
		"link" => "business",
		"key" => "type_commer",
		"value" => 7,
	),
	27 => array(
		"link" => "garage",
		"key" => "type_commer",
		"value" => 8,
	),
	28 => array(
		"link" => "all-commerce",
		"key" => "type_commer",
		"value" => 0,
	),
);

if($_SERVER['QUERY_STRING']=='search' || $_SERVER['QUERY_STRING']=='newbuilding' || $_SERVER['QUERY_STRING']=='cession'){
	$url_param = preg_replace("/\/\?/", '', urldecode($_SERVER['REQUEST_URI']));
	$url_param = str_replace("/", "", $url_param);
	$url_param = str_replace("search?", "", $url_param);
	$url_param2 = str_replace("newbuilding?", "", $url_param);
	$url_param3 = str_replace("cession?", "", $url_param);
	$staticLink = false;
}
else {
	$url_param = urldecode($_SERVER['QUERY_STRING']);
	$url_param_dinam = $_SERVER['REQUEST_URI'];
		
	$pos = strripos($url_param_dinam, "?");
	if(!empty($pos)){
		$url_paramDinamic = explode("?",$url_param_dinam);
		$url_paramDinamic = explode("&",$url_paramDinamic[count($url_paramDinamic)-1]);
		foreach($url_paramDinamic as $name => $value){
			$url_par = explode('=',$value);
			$arrayLinkSearch[$url_par[0]] = $url_par[1];
		}		
	}
	$ex_url_param_dinam = explode('/',$url_param_dinam);
	if(in_array('search',$ex_url_param_dinam)){
		$paramSearchDbase = $url_param_dinam;
		$del = mysql_query("
			DELETE FROM ".$template."_sessions_search 
			WHERE sess_id='".$_COOKIE['PHPSESSID']."'
		");
		$res = mysql_query("
			INSERT INTO ".$template."_sessions_search
			SET sess_id='".$_COOKIE['PHPSESSID']."',date=UNIX_TIMESTAMP(),params='".$paramSearchDbase."'
		");
	}
	$staticLink = true;
}
if(!empty($url_param2)){
	$url_param = explode("&",$url_param2);
	foreach($url_param as $name => $value){
		$url_par = explode('=',$value);
		list($n, $v) = $url_par;
		$arrayLinkSearch[$n] = $v;
	}
	if(!isset($arrayLinkSearch['housing']) && (isset($arrayLinkSearch['estate']) || isset($arrayLinkSearch['type']))){
		$paramSearchDbase = $url_param2;
		// $del = mysql_query("
			// DELETE FROM ".$template."_sessions_search 
			// WHERE sess_id='".$_COOKIE['PHPSESSID']."'
		// ");
		// $res = mysql_query("
			// INSERT INTO ".$template."_sessions_search
			// SET sess_id='".$_COOKIE['PHPSESSID']."',date=UNIX_TIMESTAMP(),params='".$paramSearchDbase."'
		// ");
	}
	else {
		foreach($url_param as $name => $value){
			$url_par = explode('=',$value);
			list($n, $v) = $url_par;
			$arrayLinkSearch2[$n] = $v;
		}
	}
}
if(!empty($url_param3)){
	$url_param = explode("&",$url_param3);
	foreach($url_param as $name => $value){
		$url_par = explode('=',$value);
		list($n, $v) = $url_par;
		$arrayLinkSearch[$n] = $v;
	}
	if(!isset($arrayLinkSearch['housing']) && (isset($arrayLinkSearch['estate']) || isset($arrayLinkSearch['type']))){
		$paramSearchDbase = $url_param3;
		// $del = mysql_query("
			// DELETE FROM ".$template."_sessions_search 
			// WHERE sess_id='".$_COOKIE['PHPSESSID']."'
		// ");
		// $res = mysql_query("
			// INSERT INTO ".$template."_sessions_search
			// SET sess_id='".$_COOKIE['PHPSESSID']."',date=UNIX_TIMESTAMP(),params='".$paramSearchDbase."'
		// ");
	}
	else {
		foreach($url_param as $name => $value){
			$url_par = explode('=',$value);
			list($n, $v) = $url_par;
			$arrayLinkSearch2[$n] = $v;
		}
	}
}

/*
*  Если делался поисковый запрос
*/ 
$paramSearchDbase = '';
$res = mysql_query("
	SELECT id,params
	FROM ".$template."_sessions_search
	WHERE sess_id='".$_COOKIE['PHPSESSID']."' && UNIX_TIMESTAMP()-date < 86400
	ORDER BY id DESC
	LIMIT 1
");
if(mysql_num_rows($res)>0){
	$row = mysql_fetch_assoc($res);
	$paramSearchDbase = $row['params'];
}
if(!empty($paramSearchDbase)){
	$url_param = $paramSearchDbase;
}

	// echo '<pre>';
	// print_r($url_param);
	// echo '</pre>';

/*
*  Преобразование статичных ссылок в динамичные значения
*/
if($staticLink){
	$exArrayLinkSearch2 = explode("?",$url_param);
	$exArrayLinkSearch = explode("/",$exArrayLinkSearch2[0]);
	if(empty($exArrayLinkSearch[0])){
		array_splice($exArrayLinkSearch,0,1);
	}
	if($exArrayLinkSearch[0]=='search'){
		array_splice($exArrayLinkSearch,0,1);
	}
	$exArrayLinkSearch3 = array();
	if(!empty($exArrayLinkSearch2[1])){
		$exArrayLinkSearch3 = explode('&',$exArrayLinkSearch2[1]);
	}
	if(count($exArrayLinkSearch3)>0){
		foreach($exArrayLinkSearch3 as $name => $value){
			$url_par = explode('=',$value);
			list($n, $v) = $url_par;
			$arrayLinkSearch[$n] = $v;
		}
	}
	// echo '<pre>';
	// print_r($exArrayLinkSearch);
	// echo '</pre>';
	for($c=0; $c<count($exArrayLinkSearch); $c++){
		$foundParam = false;
		if($typesArray[$exArrayLinkSearch[$c]]){// тип поиска: купить/снять
			$arrayLinkSearch['type'] = $typesArray[$exArrayLinkSearch[$c]];
			$foundParam = true;
		}
		if($estatesArray[$exArrayLinkSearch[$c]]){// тип раздела: новостройки, квартира и т.п.
			if(!$foundParam){
				$arrayLinkSearch['estate'] = $estatesArray[$exArrayLinkSearch[$c]];
				$foundParam = true;
			}
		}
		if($roomsArray[$exArrayLinkSearch[$c]]>-1){// количество квартир
			if(!$foundParam){
				$foundParam = true;
				if(isset($arrayLinkSearch['rooms'])){
					$arrayLinkSearch['rooms'] = $arrayLinkSearch['rooms'].','.$roomsArray[$exArrayLinkSearch[$c]];
				}
				else {
					$arrayLinkSearch['rooms'] = $roomsArray[$exArrayLinkSearch[$c]];
				}
			}
		}
		/*
		*  Поиск в городах
		*/
		if(!$foundParam){
			for($a=0; $a<count($citiesArrays); $a++){
				if($citiesArrays[$a]['link']==$exArrayLinkSearch[$c]){
					$arrayLinkSearch['city'] = $citiesArrays[$a]['id'];
					$foundParam = true;
					break;
				}
			}
		}
		/*
		*  Поиск в районах
		*/
		if(!$foundParam){
			for($a=0; $a<count($areasArrays); $a++){
				if($areasArrays[$a]['link']==$exArrayLinkSearch[$c]){
					// $arrayLinkSearch['area'] = $areasArrays[$a]['id'];
					$foundParam = true;
					if(isset($arrayLinkSearch['area'])){
						$arrayLinkSearch['area'] = $arrayLinkSearch['area'].','.$areasArrays[$a]['id'];
					}
					else {
						$arrayLinkSearch['area'] = $areasArrays[$a]['id'];
					}
					// break;
				}
			}
		}
		/*
		*  Поиск по метро
		*/
		if(!$foundParam){
			$stationsArrayAdd = array();
			for($a=0; $a<count($stationsArrays); $a++){
				if($stationsArrays[$a]['link']==$exArrayLinkSearch[$c]){
					// $arrayLinkSearch['station'] = $stationsArrays[$a]['id'];
					$foundParam = true;
					if(isset($arrayLinkSearch['station'])){
						$arrayLinkSearch['station'] = $arrayLinkSearch['station'].','.$stationsArrays[$a]['id'];
					}
					else {
						$arrayLinkSearch['station'] = $stationsArrays[$a]['id'];
					}
				}
			}
		}
		/*
		*  Поиск по подтипам загородной
		*/
		if(!$foundParam){
			for($a=0; $a<count($arrayTypeCountry); $a++){
				if($arrayTypeCountry[$a]['link']==$exArrayLinkSearch[$c]){
					$foundParam = true;
					if(isset($arrayLinkSearch['type_country'])){
						$arrayLinkSearch['type_country'] = $arrayLinkSearch['type_country'].','.$arrayTypeCountry[$a]['value'];
					}
					else {
						$arrayLinkSearch['type_country'] = $arrayTypeCountry[$a]['value'];
					}
					break;
				}
			}
		}
		/*
		*  Поиск в других элементах
		*/
		if(!$foundParam){
			for($a=0; $a<count($anotherLinksArray); $a++){
				if($anotherLinksArray[$a]['link']==$exArrayLinkSearch[$c]){
					$arrayLinkSearch[$anotherLinksArray[$a]['key']] = $anotherLinksArray[$a]['value'];
					$foundParam = true;
				}
			}
		}
	}
	if(!isset($arrayLinkSearch['housing']) && (isset($arrayLinkSearch['estate']) || isset($arrayLinkSearch['type']))){
		$paramSearchDbase = $url_param;
		$del = mysql_query("
			DELETE FROM ".$template."_sessions_search 
			WHERE sess_id='".$_COOKIE['PHPSESSID']."'
		");
		$res = mysql_query("
			INSERT INTO ".$template."_sessions_search
			SET sess_id='".$_COOKIE['PHPSESSID']."',date=UNIX_TIMESTAMP(),params='".$paramSearchDbase."'
		");
	}
	else {
		if(count($url_param_dinam)==0){
			foreach($url_param as $name => $value){
				$url_par = explode('=',$value);
				list($n, $v) = $url_par;
				$arrayLinkSearch2[$n] = $v;
			}
		}
	}
}

// echo '<pre>';
// print_r($arrayLinkSearch);
// echo '</pre>';

$special_add = "";
$special_adds = "";
$hobbies = '';
$textValue = '';

$priceParams = '';
$addressParams = '';
$fullSquareParams = '';
$roomSquareParams = '';
$liveSquareParams = '';
$kitchenSquareParams = '';
$placeSquareParams = '';
$areaSquareParams = '';
$floorParams = '';
$floorsParams = '';
$farSubwayParams = '';
$readyParams = '';
$typeHouseParams = '';
$classObjectParams = '';
$datesParams = '';
$finishingParams = '';
$repairsParams = '';
$communicationsParams = '';
$ppaParams = '';
$entranceParams = '';
$heightCeilingParams = '';
$legalStatusParams = '';
$plumbingParams = '';
$sewerageParams = '';
$transactionParams = '';
$wcParams = '';
$periodParams = '';
$prepaymentParams = '';
$deliveryParams = '';
$deliveryParamsArray = array();
$furnitureParams = '';
$electricityParams = '';
$heatingParams = '';
$gasParams = '';
$furnitureKitchenParams = '';
$balconyParams = '';
$liftParams = '';
$saunaParams = '';
$waterParams = '';
$garageParams = '';
$forestParams = '';
$tvParams = '';
$washerParams = '';
$fridgeParams = '';
$animalParams = '';
$childrenParams = '';
$depositParams = '';
$except_firstParams = '';
$except_lastParams = '';
$valueParamsDelivery = array();

if(!empty($arrayLinkSearch)){
	$sadd = array();
	$estateParam = $arrayLinkSearch['estate'];
	$typeParam = $arrayLinkSearch['type'];
	// if(isset($arrayLinkSearch['cession']) && $arrayLinkSearch['cession']==1){
		// $estateParam = 6;
		// $arrayLinkSearch['estate'] = 6;
	// }
	$arrayParamName = array(
		"rooms"=>"rooms",
		"type_country"=>"type_country",
		"subtype_country"=>"subtype_country",
		"rooms_flat"=>"rooms_flat",
		"type_commer"=>"type_commer",
		"ppa"=>"ppa",
		"entrance"=>"entrance",
		"height_ceiling"=>"ceiling_height",
		"share"=>"share",
		"legal_status"=>"legal_status",
		"plumbing"=>"plumbing",
		"sewerage"=>"sewerage",
		"electricity"=>"electricity",
		"heating"=>"heating",
		"gas"=>"gas",
		"sauna"=>"sauna",
		"water"=>"water",
		"garage"=>"garage",
		"forest"=>"forest",
		"city"=>"location",
		"area"=>"area",
		"build"=>"build",
		"address"=>"address",
		"station"=>"station",
		"railway"=>"railway",
		"developer"=>"developer",
		"parent"=>"p_main",
		"housing"=>"queue",
		"priceMin"=>"price",
		"priceMax"=>"price",
		"priceMeterMin"=>"price",
		"priceMeterMax"=>"price",
		"roomsMin"=>"rooms",
		"roomsMax"=>"rooms",
		"squareRoomMin"=>"room_square",
		"squareRoomMax"=>"room_square",
		"squareFullMin"=>"full_square",
		"squareFullMax"=>"full_square",
		"squareLiveMin"=>"live_square",
		"squareLiveMax"=>"live_square",
		"squareKitchenMin"=>"kitchen_square",
		"squareKitchenMax"=>"kitchen_square",
		"squarePlaceMin"=>"place_square",
		"squarePlaceMax"=>"place_square",
		"squareAreaMin"=>"area_square",
		"squareAreaMax"=>"area_square",
		"floor"=>"floor",
		"floorMin"=>"floor",
		"floorMax"=>"floor",
		"floorsMin"=>"floors",
		"floorsMax"=>"floors",
		"finishing"=>"finishing_value",
		"repairs"=>"repairs",
		"transaction"=>"transaction",
		"wc"=>"wc",
		"period"=>"period",
		"communications"=>"communications",
		"prepayment"=>"prepayment",
		"deliveryMin"=>"exploitation",
		"deliveryMax"=>"exploitation",
		"type_house"=>"type_house",
		"class"=>"class_object",
		"dates"=>"date",
		"ready"=>"ready",
		"far_subway"=>"dist_value",
		"furniture"=>"furniture",
		"furniture_kitchen"=>"furniture_kitchen",
		"balcony"=>"balcony",
		"lift"=>"lift",
		"tv"=>"tv",
		"washer"=>"washer",
		"fridge"=>"fridge",
		"animal"=>"animal",
		"children"=>"children",
		"deposit"=>"deposit",
		"except_first"=>"floor",
		"except_last"=>"floor",
	);

	foreach($arrayLinkSearch as $name => $value){
		if($arrayParamName[$name]){
			$var = $name."_var";
			$$var = $value;
			if($name=='housing'){
				$var = $name."_var2";
				$$var = $value;
			}
			
			// if($estateParam==1){
				// $ex_min_value_delivery_new_sell = explode(' ',$min_value_delivery_new_sell);
				// $ex_max_value_delivery_new_sell = explode(' ',$max_value_delivery_new_sell);
				// $_min_value_delivery_year = (int)$ex_min_value_delivery_new_sell[1];
				// $_max_value_delivery_year = (int)$ex_max_value_delivery_new_sell[1];

				// $n = 1;
				// for($c=$_min_value_delivery_year; $c<=$_max_value_delivery_year; $c++){
					// $min_search = array_search($ex_min_value_delivery_new_sell[0],$_QUATRE);
					// $max_search = array_search($ex_max_value_delivery_new_sell[0],$_QUATRE);
					// if($c==$_min_value_delivery_year){
						// for($q=$min_search; $q<count($_QUATRE); $q++){
							// $q_name = $_QUATRE[$q]." ".$c;
							// $valueParamsDelivery[$n] = $q_name;
							// $n++;
						// }
					// }
					// else if($c==$_max_value_delivery_year){
						// for($q=0; $q<=$max_search; $q++){
							// $q_name = $_QUATRE[$q]." ".$c;
							// $valueParamsDelivery[$n] = $q_name;
							// $n++;
						// }
					// }
					// else {
						// for($q=0; $q<count($_QUATRE); $q++){
							// $q_name = $_QUATRE[$q]." ".$c;
							// $valueParamsDelivery[$n] = $q_name;
							// $n++;
						// }
					// }
				// }
			// }
			if($estateParam==6){
				$ex_min_value_delivery_cession_sell = explode(' ',$min_value_delivery_cession_sell);
				$ex_max_value_delivery_cession_sell = explode(' ',$max_value_delivery_cession_sell);
				$_min_value_delivery_year = (int)$ex_min_value_delivery_cession_sell[1];
				$_max_value_delivery_year = (int)$ex_max_value_delivery_cession_sell[1];

				$n = 1;
				for($c=$_min_value_delivery_year; $c<=$_max_value_delivery_year; $c++){
					$min_search = array_search($ex_min_value_delivery_cession_sell[0],$_QUATRE);
					$max_search = array_search($ex_max_value_delivery_cession_sell[0],$_QUATRE);
					if($c==$_min_value_delivery_year){
						for($q=$min_search; $q<count($_QUATRE); $q++){
							$q_name = $_QUATRE[$q]." ".$c;
							$valueParamsDelivery[$n] = $q_name;
							$n++;
						}
					}
					else if($c==$_max_value_delivery_year){
						for($q=0; $q<=$max_search; $q++){
							$q_name = $_QUATRE[$q]." ".$c;
							$valueParamsDelivery[$n] = $q_name;
							$n++;
						}
					}
					else {
						for($q=0; $q<count($_QUATRE); $q++){
							$q_name = $_QUATRE[$q]." ".$c;
							$valueParamsDelivery[$n] = $q_name;
							$n++;
						}
					}
				}
			}
			
			if($name=='rooms' || $name=='rooms_flat'){ // кол-во комнат
				if(!empty($value) || $value==0){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					$more_than = 4;
					if($estateParam==2){// квартиры
						$more_than = 5;
					}
					if($estateParam==3){// комнаты
						$more_than = 6;
					}
					if(count($ex_values)>1){
						for($c=0; $c<count($ex_values); $c++){
							if($ex_values[$c]==$more_than){
								if($estateParam==1 || $estateParam==6){
									array_push($rooms_array,'f.'.$arrayParamName[$name].">='".$ex_values[$c]."'");
								}
								else {
									array_push($rooms_array,'s.'.$arrayParamName[$name].">='".$ex_values[$c]."'");
								}
							}
							else {
								if($estateParam==1 || $estateParam==6){
									array_push($rooms_array,'f.'.$arrayParamName[$name]."='".$ex_values[$c]."'");
								}
								else {
									array_push($rooms_array,'s.'.$arrayParamName[$name]."='".$ex_values[$c]."'");
								}
							}
						}
						$special_add .= " && (".implode(" || ",$rooms_array).")";
						$special_add2 .= " && (".implode(" || ",$rooms_array).")";
					}
					else {
						if($ex_values[0]==$more_than){
							if($estateParam==1 || $estateParam==6){
								$special_add .= " && f.".$arrayParamName[$name].">='".$ex_values[0]."'";
								$special_add2 .= " && f.".$arrayParamName[$name].">='".$ex_values[0]."'";
							}
							else {
								$special_add .= " && s.".$arrayParamName[$name].">='".$ex_values[0]."'";
							}
						}
						else {
							if($estateParam==1 || $estateParam==6){
								$special_add .= " && f.".$arrayParamName[$name]."='".$ex_values[0]."'";
								$special_add2 .= " && f.".$arrayParamName[$name]."='".$ex_values[0]."'";
							}
							else {
								$special_add .= " && s.".$arrayParamName[$name]."='".$ex_values[0]."'";
							}
						}
					}
				}
			}
			if($name=='type_country'){ // подтипы загородной
				if(!empty($value) || $value==0){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					if(count($ex_values)>1){
						for($c=0; $c<count($ex_values); $c++){
							array_push($rooms_array,'s.'.$arrayParamName[$name]."='".$ex_values[$c]."'");
						}
						$special_add .= " && (".implode(" || ",$rooms_array).")";
						$special_add2 .= " && (".implode(" || ",$rooms_array).")";
					}
					else {
						$special_add .= " && s.".$arrayParamName[$name]."='".$ex_values[0]."'";
					}
				}
			}
			else if($name=='share'){ // доля
				if(!empty($value)){
					$ex_values = $value;
					if($ex_values==1){
						if($estateParam==1 || $estateParam==6){
							$special_add .= " && e.".$arrayParamName[$name]."='".$ex_values[0]."'";
						}
						else {
							$special_add .= " && s.".$arrayParamName[$name]."='".$ex_values[0]."'";
						}
					}
					if($ex_values==2){
						if($estateParam==1 || $estateParam==6){
							$special_add .= " && e.".$arrayParamName[$name]."='".$ex_values[0]."'";
						}
						else {
							$special_add .= " && s.".$arrayParamName[$name]."=0";
						}
					}
				}
			}
			else if($name=='type_commer'){ // Тип коммерции
				if(!empty($value)){
					$transactionParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
				}
				else {
					$transactionParams = "";
				}
			}
			// else if($name=='type_country'){ // Тип загородной
				// if(!empty($value)){
					// $transactionParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
				// }
			// }
			else if($name=='floor'){ // Этаж (коммерция)
				if(!empty($value)){
					$transactionParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
				}
			}
			// else if($name=='type_commer'){ // Тип коммерции
				// if(!empty($value)){
					// $rooms_array = array();
					// $ex_values = explode(',',$value);
					// if(count($ex_values)>1){
						// for($c=0; $c<count($ex_values); $c++){
							// array_push($rooms_array,'s.'.$arrayParamName[$name]."='".$ex_values[$c]."'");
						// }
						// $special_add .= " && (".implode(" || ",$rooms_array).")";
					// }
					// else {
						// $special_add .= " && s.".$arrayParamName[$name]."='".$ex_values[0]."'";
					// }
				// }
			// }
			else if($name=='city'){ // город
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					if(count($ex_values)>1){
						for($c=0; $c<count($ex_values); $c++){
							if($estateParam==1 || $estateParam==6){
								array_push($rooms_array,'e.'.$arrayParamName[$name]."='".$ex_values[$c]."'");
							}
							else {
								array_push($rooms_array,'s.'.$arrayParamName[$name]."='".$ex_values[$c]."'");
							}
						}
						$special_add .= " && (".implode(" || ",$rooms_array).")";
					}
					else {
						if($estateParam==1 || $estateParam==6){
							$special_add .= " && e.".$arrayParamName[$name]."='".$ex_values[0]."'";
						}
						else {
							$special_add .= " && s.".$arrayParamName[$name]."='".$ex_values[0]."'";
						}
					}
				}
			}
			else if($name=='developer'){ // застройщик
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					$special_adds = '';
					if(count($ex_values)>1){
						for($c=0; $c<count($ex_values); $c++){
							if($estateParam==1 || $estateParam==6){
								array_push($rooms_array,'e.'.$arrayParamName[$name]."='".$ex_values[$c]."'");
							}
							else {
								array_push($rooms_array,'s.'.$arrayParamName[$name]."='".$ex_values[$c]."'");
							}
						}
						// $special_adds .= " && (".implode(" || ",$rooms_array).")";
						$special_adds .= "(".implode(" || ",$rooms_array).")";
					}
					else {
						if($estateParam==1 || $estateParam==6){
							// $special_adds .= " && e.".$arrayParamName[$name]."='".$ex_values[0]."'";
							$special_adds .= "e.".$arrayParamName[$name]."='".$ex_values[0]."'";
						}
						else {
							// $special_adds .= " && s.".$arrayParamName[$name]."='".$ex_values[0]."'";
							$special_adds .= "s.".$arrayParamName[$name]."='".$ex_values[0]."'";
						}
					}
					if($estateParam==1 || $estateParam==6){
						array_push($descParams,$name);
						array_push($relParamsSearch,$special_adds);
					}
				}
			}
			else if($name=='parent'){ // ЖК
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					if(count($ex_values)>1){
						for($c=0; $c<count($ex_values); $c++){
							array_push($rooms_array,'f.'.$arrayParamName[$name]."='".$ex_values[$c]."'");
						}
						$special_add .= " && (".implode(" || ",$rooms_array).")";
						$special_add2 .= " && (".implode(" || ",$rooms_array).")";
					}
					else {
						$special_add .= " && f.".$arrayParamName[$name]."='".$ex_values[0]."'";
						$special_add2 .= " && f.".$arrayParamName[$name]."='".$ex_values[0]."'";
					}
				}
			}
			else if($name=='housing'){ // Корпус
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					if(count($ex_values)>1){
						for($c=0; $c<count($ex_values); $c++){
							array_push($rooms_array,'f.'.$arrayParamName[$name]."='".$ex_values[$c]."'");
						}
						$special_add .= " && (".implode(" || ",$rooms_array).")";
						$special_add2 .= " && (".implode(" || ",$rooms_array).")";
					}
					else {
						$special_add .= " && f.".$arrayParamName[$name]."='".$ex_values[0]."'";
						$special_add2 .= " && f.".$arrayParamName[$name]."='".$ex_values[0]."'";
					}
				}
			}
			else if($name=='area'){ // район
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					$special_adds = '';
					if(count($ex_values)>1){
						for($c=0; $c<count($ex_values); $c++){
							if($estateParam==1 || $estateParam==6){
								array_push($rooms_array,'e.'.$arrayParamName[$name]."='".$ex_values[$c]."'");
							}
							else {
								array_push($rooms_array,'s.'.$arrayParamName[$name]."='".$ex_values[$c]."'");
							}
						}
						// $special_adds .= " && (".implode(" || ",$rooms_array).")";
						// $special_adds .= "(".implode(" || ",$rooms_array).")"; // !!!OLD!!!
						$special_adds .= implode(" || ",$rooms_array);
					}
					else {
						if($estateParam==1 || $estateParam==6){
							// $special_adds .= " && e.".$arrayParamName[$name]."='".$ex_values[0]."'";
							$special_adds .= "e.".$arrayParamName[$name]."='".$ex_values[0]."'";
						}
						else {
							// $special_adds .= " && s.".$arrayParamName[$name]."='".$ex_values[0]."'";
							$special_adds .= "s.".$arrayParamName[$name]."='".$ex_values[0]."'";
						}
					}
					array_push($descParams,$name);
					array_push($relParamsSearch,$special_adds);
				}
			}
			else if($name=='build'){ // ЖК
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					$special_adds = '';
					if(count($ex_values)>1){
						for($c=0; $c<count($ex_values); $c++){
							if($estateParam==1){
								array_push($rooms_array,"e.id='".$ex_values[$c]."'");
							}
							if($estateParam==6){
								array_push($rooms_array,"e.id='".$ex_values[$c]."'");
							}
						}
						// $special_adds .= " && (".implode(" || ",$rooms_array).")";
						$special_adds .= "(".implode(" || ",$rooms_array).")";
					}
					else {
						if($estateParam==1){
							// $special_adds .= " && e.id='".$ex_values[0]."'";
							$special_adds .= "e.id='".$ex_values[0]."'";
						}
						if($estateParam==6){
							// $special_adds .= " && e.p_main='".$ex_values[0]."'";
							$special_adds .= "e.id='".$ex_values[0]."'";
						}
					}
					if($estateParam==1 || $estateParam==6){
						array_push($descParams,$name);
						array_push($relParamsSearch,$special_adds);
					}
				}
			}
			else if($name=='address'){ // адрес
				if(!empty($value)){
					$value = clearValueText($value);
					if($estateParam==1 || $estateParam==6){
						$addressParams .= " && e.".$arrayParamName[$name]." LIKE '%$value%'";
					}
					else {
						$addressParams .= " && s.".$arrayParamName[$name]." LIKE '%$value%'";
					}
				}
			}
			else if($name=='station'){ // метро
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					$special_adds = '';
					if(count($ex_values)>1){
						for($c=0; $c<count($ex_values); $c++){
							if($estateParam==1 || $estateParam==6){
								if(!empty($ex_values[$c])){
									array_push($rooms_array,'e.'.$arrayParamName[$name]."='".$ex_values[$c]."'");
								}
							}
							else {
								if(!empty($ex_values[$c])){
									array_push($rooms_array,'s.'.$arrayParamName[$name]."='".$ex_values[$c]."'");
								}
							}
						}
						// $special_adds .= " && (".implode(" || ",$rooms_array).")";
						// $special_adds .= "(".implode(" || ",$rooms_array).")"; // !!!OLD!!!
						$special_adds .= implode(" || ",$rooms_array);
					}
					else {
						if($estateParam==1 || $estateParam==6){
							// $special_adds .= " && e.".$arrayParamName[$name]."='".$ex_values[0]."'";
							if(!empty($ex_values[0])){
								$special_adds .= "e.".$arrayParamName[$name]."='".$ex_values[0]."'";
							}
						}
						else {
							// $special_adds .= " && s.".$arrayParamName[$name]."='".$ex_values[0]."'";
							if(!empty($ex_values[0])){
								$special_adds .= "s.".$arrayParamName[$name]."='".$ex_values[0]."'";
							}
						}
					}
					array_push($descParams,$name);
					array_push($relParamsSearch,$special_adds);
				}
			}
			else if($name=='railway'){ // ж/д
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					$special_adds = '';
					if(count($ex_values)>1){
						for($c=0; $c<count($ex_values); $c++){
							if($estateParam==1 || $estateParam==6){
								array_push($rooms_array,'e.'.$arrayParamName[$name]."='".$ex_values[$c]."'");
							}
							else {
								array_push($rooms_array,'s.'.$arrayParamName[$name]."='".$ex_values[$c]."'");
							}
						}
						// $special_adds .= " && (".implode(" || ",$rooms_array).")";
						// $special_adds .= "(".implode(" || ",$rooms_array).")"; // !!!OLD!!!
						$special_adds .= implode(" || ",$rooms_array);
					}
					else {
						if($estateParam==1 || $estateParam==6){
							// $special_adds .= " && e.".$arrayParamName[$name]."='".$ex_values[0]."'";
							$special_adds .= "e.".$arrayParamName[$name]."='".$ex_values[0]."'";
						}
						else {
							// $special_adds .= " && s.".$arrayParamName[$name]."='".$ex_values[0]."'";
							$special_adds .= "s.".$arrayParamName[$name]."='".$ex_values[0]."'";
						}
					}
					array_push($descParams,$name);
					array_push($relParamsSearch,$special_adds);
				}
			}
			else if($name=='priceMin'){ // минимальная цена
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					if(count($ex_values)>0){
						$priceMin = $ex_values[0];
						if(!$priceMax){
							if($estateParam==1 || $estateParam==6){
								$priceParams = " && f.".$arrayParamName[$name].">='".$ex_values[0]."'";
							}
							else {
								$priceParams = " && s.".$arrayParamName[$name].">='".$ex_values[0]."'";
							}
						}
						else {
							if($estateParam==1 || $estateParam==6){
								$priceParams = " && (f.".$arrayParamName[$name].">='".$ex_values[0]."' && f.".$arrayParamName[$name]."<='".$priceMax."')";
							}
							else {
								$priceParams = " && (s.".$arrayParamName[$name].">='".$ex_values[0]."' && s.".$arrayParamName[$name]."<='".$priceMax."')";
							}
						}
					}
				}
			}
			else if($name=='priceMax'){ // максимальная цена
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					if(count($ex_values)>0){
						$priceMax = $ex_values[0];
						if(!$priceMin){
							if($estateParam==1 || $estateParam==6){
								$priceParams = " && f.".$arrayParamName[$name]."<='".$ex_values[0]."'";
							}
							else {
								$priceParams = " && s.".$arrayParamName[$name]."<='".$ex_values[0]."'";
							}
						}
						else {
							if($estateParam==1 || $estateParam==6){
								$priceParams = " && (f.".$arrayParamName[$name].">='".$priceMin."' && f.".$arrayParamName[$name]."<='".$ex_values[0]."')";
							}
							else {
								$priceParams = " && (s.".$arrayParamName[$name].">='".$priceMin."' && s.".$arrayParamName[$name]."<='".$ex_values[0]."')";
							}
						}
					}
				}
			}
			else if($name=='priceMeterMin'){ // минимальная цена за метр
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					if(count($ex_values)>0){
						$priceMeterMin = $ex_values[0];
						if(!$priceMeterMax){
							if($estateParam==1 || $estateParam==6){
								$priceParams = " && f.price/f.full_square>='".$ex_values[0]."'";
							}
							else {
								$priceParams = " && s.".$arrayParamName[$name].">='".$ex_values[0]."'";
							}
						}
						else {
							if($estateParam==1 || $estateParam==6){
								$priceParams = " && (f.price/f.full_square>='".$ex_values[0]."' && f.price/f.full_square<='".$priceMeterMax."')";
							}
							else {
								$priceParams = " && (s.".$arrayParamName[$name].">='".$ex_values[0]."' && s.".$arrayParamName[$name]."<='".$priceMeterMax."')";
							}
						}
					}
				}
			}
			else if($name=='priceMeterMax'){ // максимальная цена за метр
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					if(count($ex_values)>0){
						$priceMeterMax = $ex_values[0];
						if(!$priceMeterMin){
							if($estateParam==1 || $estateParam==6){
								$priceParams = " && f.price/f.full_square<='".$ex_values[0]."'";
							}
							else {
								$priceParams = " && s.".$arrayParamName[$name]."<='".$ex_values[0]."'";
							}
						}
						else {
							if($estateParam==1 || $estateParam==6){
								$priceParams = " && (f.price/f.full_square>='".$priceMeterMin."' && f.price/f.full_square<='".$ex_values[0]."')";
							}
							else {
								$priceParams = " && (s.".$arrayParamName[$name].">='".$priceMeterMin."' && s.".$arrayParamName[$name]."<='".$ex_values[0]."')";
							}
						}
					}
				}
			}
			else if($name=='squareFullMin'){ // минимальная общая площадь
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					if(count($ex_values)>0){
						if(!$squareFullMax_var){
							if($estateParam==1 || $estateParam==6){
								$fullSquareParams = " && f.".$arrayParamName[$name].">='".$ex_values[0]."'";
							}
							else {
								$fullSquareParams = " && s.".$arrayParamName[$name].">='".$ex_values[0]."'";
							}
						}
						else {
							if($estateParam==1 || $estateParam==6){
								$fullSquareParams = " && (f.".$arrayParamName[$name].">='".$ex_values[0]."' && f.".$arrayParamName[$name]."<='".$squareFullMax_var."')";
							}
							else {
								$fullSquareParams = " && (s.".$arrayParamName[$name].">='".$ex_values[0]."' && s.".$arrayParamName[$name]."<='".$squareFullMax_var."')";
							}
						}
					}
				}
			}
			else if($name=='squareFullMax'){ // максимальная общая площадь
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					if(count($ex_values)>0){
						if(!$squareFullMin_var){
							if($estateParam==1 || $estateParam==6){
								$fullSquareParams = " && f.".$arrayParamName[$name]."<='".$ex_values[0]."'";
							}
							else {
								$fullSquareParams = " && s.".$arrayParamName[$name]."<='".$ex_values[0]."'";
							}
						}
						else {
							if($estateParam==1 || $estateParam==6){
								$fullSquareParams = " && (f.".$arrayParamName[$name].">='".$squareFullMin_var."' && f.".$arrayParamName[$name]."<='".$ex_values[0]."')";
							}
							else {
								$fullSquareParams = " && (s.".$arrayParamName[$name].">='".$squareFullMin_var."' && s.".$arrayParamName[$name]."<='".$ex_values[0]."')";
							}
						}
					}
				}
			}
			else if($name=='squareRoomMin'){ // минимальная площадь комнаты
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					if(count($ex_values)>0){
						if(!$squareRoomMax_var){
							$roomSquareParams = " && s.".$arrayParamName[$name].">='".$ex_values[0]."'";
						}
						else {
							$roomSquareParams = " && (s.".$arrayParamName[$name].">='".$ex_values[0]."' && s.".$arrayParamName[$name]."<='".$squareRoomMax_var."')";
						}
					}
				}
			}
			else if($name=='squareRoomMax'){ // максимальная площадь комнаты
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					if(count($ex_values)>0){
						if(!$squareRoomMin_var){
							$roomSquareParams = " && s.".$arrayParamName[$name]."<='".$ex_values[0]."'";
						}
						else {
							$roomSquareParams = " && (s.".$arrayParamName[$name].">='".$squareRoomMin_var."' && s.".$arrayParamName[$name]."<='".$ex_values[0]."')";
						}
					}
				}
			}
			else if($name=='squareLiveMin'){ // минимальная жилая площадь
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					if(count($ex_values)>0){
						if(!$squareLiveMax_var){
							if($estateParam==1 || $estateParam==6){
								$liveSquareParams = " && f.".$arrayParamName[$name].">='".$ex_values[0]."'";
							}
							else {
								$liveSquareParams = " && s.".$arrayParamName[$name].">='".$ex_values[0]."'";
							}
						}
						else {
							if($estateParam==1 || $estateParam==6){
								$liveSquareParams = " && (f.".$arrayParamName[$name].">='".$ex_values[0]."' && f.".$arrayParamName[$name]."<='".$squareLiveMax_var."')";
							}
							else {
								$liveSquareParams = " && (s.".$arrayParamName[$name].">='".$ex_values[0]."' && s.".$arrayParamName[$name]."<='".$squareLiveMax_var."')";
							}
						}
					}
				}
			}
			else if($name=='squareLiveMax'){ // максимальная жилая площадь
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					if(count($ex_values)>0){
						if(!$squareLiveMin_var){
							if($estateParam==1 || $estateParam==6){
								$liveSquareParams = " && f.".$arrayParamName[$name]."<='".$ex_values[0]."'";
							}
							else {
								$liveSquareParams = " && s.".$arrayParamName[$name]."<='".$ex_values[0]."'";
							}
						}
						else {
							if($estateParam==1 || $estateParam==6){
								$liveSquareParams = " && (f.".$arrayParamName[$name].">='".$squareLiveMin_var."' && f.".$arrayParamName[$name]."<='".$ex_values[0]."')";
							}
							else {
								$liveSquareParams = " && (s.".$arrayParamName[$name].">='".$squareLiveMin_var."' && s.".$arrayParamName[$name]."<='".$ex_values[0]."')";
							}
						}
					}
				}
			}
			else if($name=='squareKitchenMin'){ // минимальная площадь кухни
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					if(count($ex_values)>0){
						if(!$squareKitchenMax_var){
							if($estateParam==1 || $estateParam==6){
								$kitchenSquareParams = " && f.".$arrayParamName[$name].">='".$ex_values[0]."'";
							}
							else {
								$kitchenSquareParams = " && s.".$arrayParamName[$name].">='".$ex_values[0]."'";
							}
						}
						else {
							if($estateParam==1 || $estateParam==6){
								$kitchenSquareParams = " && (f.".$arrayParamName[$name].">='".$ex_values[0]."' && f.".$arrayParamName[$name]."<='".$squareKitchenMax_var."')";
							}
							else {
								$kitchenSquareParams = " && (s.".$arrayParamName[$name].">='".$ex_values[0]."' && s.".$arrayParamName[$name]."<='".$squareKitchenMax_var."')";
							}
						}
					}
				}
			}
			else if($name=='squareKitchenMax'){ // максимальная площадь кухни
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					if(count($ex_values)>0){
						if(!$squareKitchenMin_var){
							if($estateParam==1 || $estateParam==6){
								$kitchenSquareParams = " && f.".$arrayParamName[$name]."<='".$ex_values[0]."'";
							}
							else {
								$kitchenSquareParams = " && s.".$arrayParamName[$name]."<='".$ex_values[0]."'";
							}
						}
						else {
							if($estateParam==1 || $estateParam==6){
								$kitchenSquareParams = " && (f.".$arrayParamName[$name].">='".$squareKitchenMin_var."' && f.".$arrayParamName[$name]."<='".$ex_values[0]."')";
							}
							else {
								$kitchenSquareParams = " && (s.".$arrayParamName[$name].">='".$squareKitchenMin_var."' && s.".$arrayParamName[$name]."<='".$ex_values[0]."')";
							}
						}
					}
				}
			}
			else if($name=='squarePlaceMin'){ // минимальная площадь зала
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					if(count($ex_values)>0){
						if(!$squarePlaceMax_var){
							$placeSquareParams = " && s.".$arrayParamName[$name].">='".$ex_values[0]."'";
						}
						else {
							$placeSquareParams = " && (s.".$arrayParamName[$name].">='".$ex_values[0]."' && s.".$arrayParamName[$name]."<='".$squarePlaceMax_var."')";
						}
					}
				}
			}
			else if($name=='squarePlaceMax'){ // максимальная площадь зала
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					if(count($ex_values)>0){
						if(!$squarePlaceMin_var){
							$placeSquareParams = " && s.".$arrayParamName[$name]."<='".$ex_values[0]."'";
						}
						else {
							$placeSquareParams = " && (s.".$arrayParamName[$name].">='".$squarePlaceMin_var."' && s.".$arrayParamName[$name]."<='".$ex_values[0]."')";
						}
					}
				}
			}
			else if($name=='squareAreaMin'){ // минимальная площадь участка
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					if(count($ex_values)>0){
						if(!$squareAreaMax_var){
							$areaSquareParams = " && s.".$arrayParamName[$name].">='".$ex_values[0]."'";
						}
						else {
							$areaSquareParams = " && (s.".$arrayParamName[$name].">='".$ex_values[0]."' && s.".$arrayParamName[$name]."<='".$squareAreaMax_var."')";
						}
					}
				}
			}
			else if($name=='squareAreaMax'){ // максимальная площадь участка
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					if(count($ex_values)>0){
						if(!$squareAreaMin_var){
							$areaSquareParams = " && s.".$arrayParamName[$name]."<='".$ex_values[0]."'";
						}
						else {
							$areaSquareParams = " && (s.".$arrayParamName[$name].">='".$squareAreaMin_var."' && s.".$arrayParamName[$name]."<='".$ex_values[0]."')";
						}
					}
				}
			}
			else if($name=='floorMin'){ // минимальный этаж квартиры
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					// if($estateParam!=1){
						if(count($ex_values)>0){
							if(!$floorMax_var){
								if($estateParam==1 || $estateParam==6){
									$floorParams = " && f.".$arrayParamName[$name].">='".$ex_values[0]."'";
								}
								else {
									$floorParams = " && s.".$arrayParamName[$name].">='".$ex_values[0]."'";
								}
							}
							else {
								if($estateParam==1 || $estateParam==6){
									$floorParams = " && (f.".$arrayParamName[$name].">='".$ex_values[0]."' && f.".$arrayParamName[$name]."<='".$floorMax_var."')";
								}
								else {
									$floorParams = " && (s.".$arrayParamName[$name].">='".$ex_values[0]."' && s.".$arrayParamName[$name]."<='".$floorMax_var."')";
								}
							}
						}
					// }
				}
			}
			else if($name=='floorMax'){ // максимальный этаж квартиры
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					// if($estateParam!=1){
						if(count($ex_values)>0){
							if(!$floorMin_var){
								if($estateParam==1 || $estateParam==6){
									$floorParams = " && f.".$arrayParamName[$name]."<='".$ex_values[0]."'";
								}
								else {
									$floorParams = " && s.".$arrayParamName[$name]."<='".$ex_values[0]."'";
								}
							}
							else {
								if($estateParam==1 || $estateParam==6){
									$floorParams = " && (f.".$arrayParamName[$name].">='".$floorMin_var."' && f.".$arrayParamName[$name]."<='".$ex_values[0]."')";
								}
								else {
									$floorParams = " && (s.".$arrayParamName[$name].">='".$floorMin_var."' && s.".$arrayParamName[$name]."<='".$ex_values[0]."')";
								}
							}
						}
					// }
				}
			}
			else if($name=='floorsMin'){ // минимальная этажность дома
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					// if($estateParam!=1){
						if(count($ex_values)>0){
							if(!$floorsMax_var){
								if($estateParam==1 || $estateParam==6){
									$floorsParams = " && e.".$arrayParamName[$name].">='".$ex_values[0]."'";
								}
								else {
									$floorsParams = " && s.".$arrayParamName[$name].">='".$ex_values[0]."'";
								}
							}
							else {
								if($estateParam==1 || $estateParam==6){
									$floorsParams = " && (e.".$arrayParamName[$name].">='".$ex_values[0]."' && e.".$arrayParamName[$name]."<='".$floorsMax_var."')";
								}
								else {
									$floorsParams = " && (s.".$arrayParamName[$name].">='".$ex_values[0]."' && s.".$arrayParamName[$name]."<='".$floorsMax_var."')";
								}
							}
						}
					// }
				}
			}
			else if($name=='floorsMax'){ // максимальная этажность дома
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					// if($estateParam!=1){
						if(count($ex_values)>0){
							if(!$floorsMin_var){
								if($estateParam==1 || $estateParam==6){
									$floorsParams = " && e.".$arrayParamName[$name]."<='".$ex_values[0]."'";
								}
								else {
									$floorsParams = " && s.".$arrayParamName[$name]."<='".$ex_values[0]."'";
								}
							}
							else {
								if($estateParam==1 || $estateParam==6){
									$floorsParams = " && (e.".$arrayParamName[$name].">='".$floorsMin_var."' && e.".$arrayParamName[$name]."<='".$ex_values[0]."')";
								}
								else {
									$floorsParams = " && (s.".$arrayParamName[$name].">='".$floorsMin_var."' && s.".$arrayParamName[$name]."<='".$ex_values[0]."')";
								}
							}
						}
					// }
				}
			}
			// else if($name=='far_subway'){ // расстояние до метро
				// if(!empty($value)){
					// $rooms_array = array();
					// $ex_values = explode(',',$value);
					// if(count($ex_values)>0){
						// if($estateParam==1 || $estateParam==6){
							// $farSubwayParams = " && e.".$arrayParamName[$name]."<='".$ex_values[0]."'";
						// }
						// else {
							// $farSubwayParams = " && s.".$arrayParamName[$name]."<='".$ex_values[0]."'";
						// }
					// }
				// }
			// }
			else if($name=='far_subway'){ // расстояние до метро
				if(!empty($value)){
					$ex_values = explode(',',$value);
					if(count($ex_values)>0){
						if($estateParam==1 || $estateParam==6){
							if(count($_FAR_SUBWAY_VALUE)<=$ex_values[0]){
								$ex_values[0] = count($_FAR_SUBWAY_VALUE);
								$farSubwayParams = " && e.".$arrayParamName[$name].">'".$_FAR_SUBWAY_VALUE[$ex_values[0]]."'";
							}
							else {
								$farSubwayParams = " && e.".$arrayParamName[$name]."<='".$_FAR_SUBWAY_VALUE[$ex_values[0]]."'";
							}
						}
						else {
							if(count($_FAR_SUBWAY_VALUE)<=$ex_values[0]){
								$ex_values[0] = count($_FAR_SUBWAY_VALUE);
								$farSubwayParams = " && s.".$arrayParamName[$name].">'".$_FAR_SUBWAY_VALUE[$ex_values[0]]."'";
							}
							else {
								$farSubwayParams = " && s.".$arrayParamName[$name]."<='".$_FAR_SUBWAY_VALUE[$ex_values[0]]."'";
							}
						}
					}
					// echo $farSubwayParams;
				}
			}
			else if($name=='finishing'){ // Отделка дома
				if(!empty($value)){
					if($estateParam==1 || $estateParam==6){
						$params_array = array();
						$ex_values = explode(',',$value);
						if(count($ex_values)>1){
							for($ex=0; $ex<count($ex_values); $ex++){
								if($estateParam==6){
									array_push($params_array,'f.'.$arrayParamName[$name]."='".$ex_values[$ex]."'");
								}
								else {
									array_push($params_array,'e.'.$arrayParamName[$name]."='".$ex_values[$ex]."'");
								}
							}
							$finishingParams .= " && (".implode(" || ",$params_array).")";
						}
						else {
							if($estateParam==6){
								$finishingParams .= " && f.".$arrayParamName[$name]."='".$ex_values[0]."'";
							}
							else {
								$finishingParams .= " && e.".$arrayParamName[$name]."='".$ex_values[0]."'";
							}
						}
					}
					else {
						$finishingParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
					}
				}
			}
			else if($name=='ppa'){ // ППА
				if(!empty($value)){
					$ppaParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
				}
			}
			else if($name=='entrance'){ // Отдельный вход
				if(!empty($value)){
					$entranceParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
				}
			}
			else if($name=='height_ceiling'){ // Высота потолков
				if(!empty($value)){
					$heightCeilingParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
				}
			}
			else if($name=='repairs'){ // ремонт квартиры
				if(!empty($value)){
					$params_array = array();
					$ex_values = explode(',',$value);					
					
					if(count($ex_values)>1){
						for($ex=0; $ex<count($ex_values); $ex++){
							array_push($params_array,'s.'.$arrayParamName[$name]."='".$ex_values[$ex]."'");
						}
						$repairsParams .= " && (".implode(" || ",$params_array).")";
					}
					else {
						$repairsParams .= " && s.".$arrayParamName[$name]."='".$ex_values[0]."'";
					}
				}
			}
			else if($name=='communications'){ // Коммуникации (Загородная)
				if(!empty($value)){
					$params_array = array();
					$ex_values = explode(',',$value);
					
					if(count($ex_values)>1){
						for($ex=0; $ex<count($ex_values); $ex++){
							array_push($params_array,'s.'.$arrayParamName[$_COMM_PARAMS[$ex_values[$ex]]]."='1'");
						}
						$communicationsParams .= " && (".implode(" || ",$params_array).")";
					}
					else {
						$communicationsParams .= " && s.".$arrayParamName[$_COMM_PARAMS[$ex_values[0]]]."='1'";
					}
				}
			}
			else if($name=='legal_status'){ // Юридический статус
				if(!empty($value)){
					$legalStatusParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
				}
			}
			else if($name=='plumbing'){ // Водопровод
				if(!empty($value)){
					$plumbingParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
				}
			}
			else if($name=='sewerage'){ // Канализация
				if(!empty($value)){
					$sewerageParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
				}
			}
			else if($name=='transaction'){ // Тип сделки
				if(!empty($value)){
					$transactionParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
				}
			}
			else if($name=='ready'){ // Стадия проекта
				if(!empty($value)){
					if($estateParam==1 || $estateParam==6){
						$readyParams = " && e.".$arrayParamName[$name]."='".(int)$value."'";
					}
					else {
						$readyParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
					}
				}
			}
			else if($name=='type_house'){ // Тип дома
				if(!empty($value)){
					if($estateParam==1 || $estateParam==6){
						$params_array = array();
						$ex_values = explode(',',$value);
						if(count($ex_values)>1){
							for($ex=0; $ex<count($ex_values); $ex++){
								array_push($params_array,'f.'.$arrayParamName[$name]."='".$ex_values[$ex]."'");
								array_push($params_array,'e.'.$arrayParamName[$name]."='".$ex_values[$ex]."'");
							}
							$typeHouseParams .= " && (".implode(" || ",$params_array).")";
						}
					}
					else {
						if(!empty($value) || $value==0){
							$params_array = array();
							$ex_values = explode(',',$value);
							if(count($ex_values)>1){
								for($c=0; $c<count($ex_values); $c++){
									array_push($params_array,'s.'.$arrayParamName[$name]."='".$ex_values[$c]."'");
								}
								$typeHouseParams .= " && (".implode(" || ",$params_array).")";
							}
							else {
								$typeHouseParams .= " && s.".$arrayParamName[$name]."='".$ex_values[0]."'";
							}
						}
					}
				}
			}
			else if($name=='class'){ // Класс объекта
				if(!empty($value)){
					if($estateParam==1){
						$params_array = array();
						$ex_values = explode(',',$value);
						if(count($ex_values)>1){
							for($ex=0; $ex<count($ex_values); $ex++){
								array_push($params_array,'f.'.$arrayParamName[$name]."='".$ex_values[$ex]."'");
								array_push($params_array,'e.'.$arrayParamName[$name]."='".$ex_values[$ex]."'");
							}
							$classObjectParams .= " && (".implode(" || ",$params_array).")";
						}
						else {
							$classObjectParams .= " && e.".$arrayParamName[$name]."='".$ex_values[0]."'";
						}
					}
				}
			}
			else if($name=='dates'){ // Дата публикации
				if(!empty($value)){
					$dateFull = strtotime(date("Y-m-d"))-(int)$value*86400;
					if($value>1000){
						$dateFull = (int)$value;
					}
					if($estateParam==1 || $estateParam==6){
						$datesParams = " && (ua.".$arrayParamName[$name].">='".$dateFull."' || f.date_place>='".$dateFull."')";
					}
					else {
						$datesParams = " && (ua.".$arrayParamName[$name].">='".$dateFull."' || s.date_place>='".$dateFull."')";
					}
				}
			}
/* 			else if($name=='wc'){ // Санузел
				// if(!empty($value)){
				if($estateParam==1 || $estateParam==6){
					$wcParams = " && f.".$arrayParamName[$name]."='".(int)$value."'";
				}
				else {
					$wcParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
				}
				// }
			}
 */
			else if($name=='wc'){ // Санузел
				$params_array = array();
				$ex_values = explode(',',$value);
				if(count($ex_values)>1){
					for($ex=0; $ex<count($ex_values); $ex++){
						if($estateParam==1 || $estateParam==6){
							array_push($params_array,'f.'.$arrayParamName[$name]."='".$ex_values[$ex]."'");
						}
						else {
							array_push($params_array,'s.'.$arrayParamName[$name]."='".$ex_values[$ex]."'");
						}
					}
					$wcParams .= " && (".implode(" || ",$params_array).")";
				}
				else {
					if($estateParam==1 || $estateParam==6){
						$wcParams .= " && f.".$arrayParamName[$name]."='".$ex_values[0]."'";
					}
					else {
						$wcParams .= " && s.".$arrayParamName[$name]."='".$ex_values[0]."'";
					}
				}
			}
			
			else if($name=='period'){ // Срок аренды
				if(!empty($value)){
					$periodParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
				}
			}
			else if($name=='prepayment'){ // Комиссия
				if(!empty($value)){
					$prepaymentParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
				}
			}
			// else if($name=='delivery'){ // Сдача
				// if(!empty($value)){
					// if($estateParam==1 || $estateParam==6){
						// $deliveryParams = " && e.".$arrayParamName[$name]." LIKE '%$value%'";
					// }
					// else {
						// $deliveryParams = " && s.".$arrayParamName[$name]." LIKE '%$value%'";
					// }
				// }
			// }
			else if($name=='deadline'){ // Год сдачи
				if(!empty($value)){
					if($estateParam==1){
						$deliveryParams = " && e.".$arrayParamName[$name]." LIKE '%$value%'";
					}
				}
			}
			else if($name=='furniture'){ // Мебель
				if(!empty($value)){
					if($estateParam==1 || $estateParam==6){
						$furnitureParams = " && f.".$arrayParamName[$name]."='".(int)$value."'";
					}
					else {
						if($value==2){
							$value = 0;
						}
						$furnitureParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
					}
				}
			}
			else if($name=='electricity'){ // Электричество
				if(!empty($value)){
					if($value==2){
						$value = 0;
					}
					$electricityParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
				}
			}
			else if($name=='heating'){ // Отопление
				if(!empty($value)){
					if($value==2){
						$value = 0;
					}
					$heatingParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
				}
			}
			else if($name=='gas'){ // Газ
				if(!empty($value)){
					if($value==2){
						$value = 0;
					}
					$gasParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
				}
			}
			else if($name=='furniture_kitchen'){ // Кухонная мебель
				if(!empty($value)){
					if($estateParam==1 || $estateParam==6){
						$furnitureKitchenParams = " && f.".$arrayParamName[$name]."='".(int)$value."'";
					}
					else {
						if($value==2){
							$value = 0;
						}
						$furnitureKitchenParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
					}
				}
			}
			else if($name=='balcony'){ // Балкон
				if(!empty($value)){
					if($estateParam==1 || $estateParam==6){
						$balconyParams = " && f.".$arrayParamName[$name]."='".(int)$value."'";
					}
					else {
						$balconyParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
					}
				}
			}
			else if($name=='lift'){ // Лифт
				if(!empty($value)){
					if($estateParam==1 || $estateParam==6){
						$liftParams = " && f.".$arrayParamName[$name]."='".(int)$value."'";
					}
					else {
						$liftParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
					}
				}
			}
			else if($name=='sauna'){ // Баня
				if(!empty($value)){
					$saunaParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
				}
			}
			else if($name=='water'){ // Водоем
				if(!empty($value)){
					$waterParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
				}
			}
			else if($name=='garage'){ // Гараж
				if(!empty($value)){
					$garageParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
				}
			}
			else if($name=='forest'){ // Рядом лес
				if(!empty($value)){
					$forestParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
				}
			}
			else if($name=='tv'){ // Телевизор
				if(!empty($value)){
					if($estateParam==1 || $estateParam==6){
						$tvParams = " && f.".$arrayParamName[$name]."='".(int)$value."'";
					}
					else {
						$tvParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
					}
				}
			}
			else if($name=='washer'){ // Стиральная машина
				if(!empty($value)){
					if($estateParam==1 || $estateParam==6){
						$washerParams = " && f.".$arrayParamName[$name]."='".(int)$value."'";
					}
					else {
						$washerParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
					}
				}
			}
			else if($name=='fridge'){ // Холодильник
				if(!empty($value)){
					if($estateParam==1 || $estateParam==6){
						$fridgeParams = " && f.".$arrayParamName[$name]."='".(int)$value."'";
					}
					else {
						$fridgeParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
					}
				}
			}
			else if($name=='animal'){ // Можно с животными
				if(!empty($value)){
					if($estateParam==1 || $estateParam==6){
						$animalParams = " && f.".$arrayParamName[$name]."='".(int)$value."'";
					}
					else {
						$animalParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
					}
				}
			}
			else if($name=='children'){ // Можно с детьми
				if(!empty($value)){
					if($estateParam==1 || $estateParam==6){
						$childrenParams = " && f.".$arrayParamName[$name]."='".(int)$value."'";
					}
					else {
						$childrenParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
					}
				}
			}
			else if($name=='deposit'){ // Залог
				if(!empty($value)){
					if($estateParam==1 || $estateParam==6){
						$depositParams = " && f.".$arrayParamName[$name]."='".(int)$value."'";
					}
					else {
						$depositParams = " && s.".$arrayParamName[$name]."='".(int)$value."'";
					}
				}
			}
			else if($name=='except_first'){ // Кроме первого этажа
				if(!empty($value)){
					if($estateParam==1 || $estateParam==6){
						$except_firstParams = " && f.".$arrayParamName[$name]."!='1'";
					}
					else {
						$except_firstParams = " && s.".$arrayParamName[$name]."!='1'";
					}
				}
			}
			else if($name=='except_last'){ // Кроме последнего этажа
				if(!empty($value)){
					if($estateParam==1 || $estateParam==6){
						$except_lastParams = " && f.".$arrayParamName[$name]."!=e.floors";
					}
					else {
						$except_lastParams = " && s.".$arrayParamName[$name]."!=s.floors";
					}
				}
			}
			else if($name=='deliveryMin'){ // минимальная сдача
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					$deliveryParams2 = '';
					if(count($ex_values)>0){
						$deliveryRequest = array();
						if(!$deliveryMax_var){
							if(count($valueParamsDelivery)>0){
								for($v=$deliveryMin_var;$v<=count($valueParamsDelivery);$v++){
									array_push($deliveryRequest,"q.commissioning='".$valueParamsDelivery[$v]."'");
								}
							}
						}
						else {
							if(count($valueParamsDelivery)>0){
								for($v=$deliveryMin_var;$v<=$deliveryMax_var;$v++){
									array_push($deliveryRequest,"q.commissioning='".$valueParamsDelivery[$v]."'");
								}
							}
						}
						if(count($deliveryRequest)>0){
							$deliveryRequest = " && (".implode(' || ',$deliveryRequest).")";
						}
						else {
							$deliveryRequest = "";
						}
						$res = mysql_query("
							SELECT p_main
							FROM ".$template."_queues AS q
							WHERE q.commissioning!='Дом сдан ' && q.commissioning!='Дом сдан' && q.commissioning!='Заселен' && q.commissioning!='Заселен ' && q.commissioning!='Собств-ть' && q.commissioning!='Собств-ть '".$deliveryRequest."
						") or die(mysql_error());
						if(mysql_num_rows($res)>0){
							while($row = mysql_fetch_assoc($res)){
								if(!in_array($row['p_main'],$deliveryParamsArray)){
									array_push($deliveryParamsArray,$row['p_main']);
								}
							}
						}
					}
				}
			}
			else if($name=='deliveryMax'){ // максимальная сдача
				if(!empty($value)){
					$rooms_array = array();
					$ex_values = explode(',',$value);
					$deliveryParams2 = '';
					if(count($ex_values)>0){
						$deliveryRequest = array();
						if(!$deliveryMin_var){
							if(count($valueParamsDelivery)>0){
								for($v=1;$v<=$deliveryMax_var;$v++){
									array_push($deliveryRequest,"q.commissioning='".$valueParamsDelivery[$v]."'");
								}
							}
						}
						else {
							if(count($valueParamsDelivery)>0){
								for($v=$deliveryMin_var;$v<=$deliveryMax_var;$v++){
									array_push($deliveryRequest,"q.commissioning='".$valueParamsDelivery[$v]."'");
								}
							}
						}
						if(count($deliveryRequest)>0){
							$deliveryRequest = " && (".implode(' || ',$deliveryRequest).")";
						}
						else {
							$deliveryRequest = "";
						}
						$res = mysql_query("
							SELECT p_main
							FROM ".$template."_queues AS q
							WHERE q.commissioning!='Дом сдан ' && q.commissioning!='Дом сдан' && q.commissioning!='Заселен' && q.commissioning!='Заселен ' && q.commissioning!='Собств-ть' && q.commissioning!='Собств-ть '".$deliveryRequest."
						") or die(mysql_error());
						if(mysql_num_rows($res)>0){
							while($row = mysql_fetch_assoc($res)){
								if(!in_array($row['p_main'],$deliveryParamsArray)){
									array_push($deliveryParamsArray,$row['p_main']);
								}
							}
						}
					}
				}
			}
			else if($name=='q'){
				$value = urldecode ($value);
				array_push($sadd, "user.name LIKE '$value%' || user.surname LIKE '$value%'");
				$textValue = $value;
				// array_push($sadd, "MATCH (name,surname) AGAINST ('+".$value."* MySQL' IN BOOLEAN MODE)");
			}
			else {
				array_push($sadd,$arrayParamName[$name]."='".(int)$value."'");
			}
		}
	}
	
	// echo $addressParams;
	// echo $priceParams;
	// echo $fullSquareParams;
	// echo $roomSquareParams;
	// echo $liveSquareParams;
	// echo $kitchenSquareParams;
	// echo $floorParams;
	// echo $floorsParams;
	// echo $deliveryParams;
	// echo $farSubwayParams;
	// echo $readyParams;
	// echo $typeHouseParams;
	// echo $classObjectParams;
	// echo $finishingParams;
	// echo $communicationsParams;
	// echo $wcParams;
	// echo $deliveryParams;
	// echo $balconyParams;
	// echo $except_firstParams;
	// echo $except_lastParams;
	$searchParamsFlat = '';
	$searchParamsFlats = '';
	$searchParams = '';
	if($estateParam==1 || $estateParam==6){
		$deliveryArrayParams = array();
		if(count($deliveryParamsArray)>0){
			for($d=0;$d<count($deliveryParamsArray);$d++){
				array_push($deliveryArrayParams,"e.id='".$deliveryParamsArray[$d]."'");
			}
			if(count($deliveryArrayParams)>0){
				$deliveryParams = " && (".implode(' || ',$deliveryArrayParams).")";
			}
		}
		if(empty($priceParams)){
			$priceParams = ' && f.price!=0';
		}
		if(empty($fullSquareParams)){
			$fullSquareParams = ' && f.full_square!=0';
		}
		if(!isset($arrayLinkSearch['city']) || empty($arrayLinkSearch['city'])){
			if($location_id){
				$special_add .= " && e.location=".$location_id;
			}
			else {
				$special_add .= " && e.location IN (1,2)";			
			}
		}
		$searchParams = "e.activation='1' && f.activation='1'".$special_add.$addressParams.$priceParams.$fullSquareParams.$liveSquareParams.$kitchenSquareParams.$floorParams.$floorsParams.$deliveryParams.$farSubwayParams.$readyParams.$typeHouseParams.$classObjectParams.$finishingParams.$wcParams.$periodParams.$prepaymentParams.$furnitureParams.$furnitureKitchenParams.$balconyParams.$liftParams.$tvParams.$washerParams.$fridgeParams.$animalParams.$childrenParams.$depositParams.$except_firstParams.$except_lastParams;
		// if(count($relParamsSearch)>0){
			// $searchParams3 = '';
			// $searchArray = array();
			// $searchParams2 = $searchParams;
			// for($r=0; $r<count($relParamsSearch); $r++){
				// $searchParams3 = $searchParams2.' && '.$relParamsSearch[$r];
				// array_push($searchArray,$searchParams3);
			// }
			// $searchParams = implode(' || ',$searchArray);
		// }
		
		
		// if(count($relParamsSearch)>0){
			// $searchParams .= ' && '.implode(" || ",$relParamsSearch);
		// }
		
		
		if(count($relParamsSearch)>0){
			// echo implode(" || ",$relParamsSearch);
			$searchParams .= ' && ('.implode(" || ",$relParamsSearch).')';
		}
		
		if($estateParam==1){
			$floorsParams = '';
		}
		$searchParamsFlat = "f.activation='1'".$special_add.$addressParams.$priceParams.$fullSquareParams.$liveSquareParams.$kitchenSquareParams.$floorsParams.$readyParams.$typeHouseParams.$classObjectParams.$finishingParams.$wcParams.$periodParams.$prepaymentParams.$furnitureParams.$furnitureKitchenParams.$balconyParams.$liftParams.$tvParams.$washerParams.$fridgeParams.$animalParams.$childrenParams.$depositParams.$except_firstParams.$except_lastParams;
		
		$searchParamsFlats = "f.activation='1'".$special_add2.$addressParams.$priceParams.$fullSquareParams.$liveSquareParams.$kitchenSquareParams.$floorParams.$floorsParams.$farSubwayParams.$readyParams.$typeHouseParams.$classObjectParams.$finishingParams.$wcParams.$periodParams.$prepaymentParams.$furnitureParams.$furnitureKitchenParams.$balconyParams.$liftParams.$tvParams.$washerParams.$fridgeParams.$animalParams.$childrenParams.$depositParams.$except_firstParams.$except_lastParams;
	}
	else {
		if(empty($priceParams)){
			// $priceParams = ' && s.price!=0';
		}
		if(empty($fullSquareParams)){
			if($type_commer_var==6){
				$fullSquareParams = ' && s.area_square!=0';			
			}
			else {
				$fullSquareParams = ' && s.full_square!=0';
				$fullSquareParams = '';
			}
			if($estateParam==3){
				$fullSquareParams = '';
			}
		}
		if(!isset($arrayLinkSearch['city']) || empty($arrayLinkSearch['city'])){
			if($location_id){
				$special_add .= " && s.location=".$location_id;
			}
			else {
				$special_add .= " && s.location IN (1,2)";			
			}
		}
		$searchParams = "s.activation='1' && s.type='".$typeParam."'".$special_add.$addressParams.$priceParams.$fullSquareParams.$roomSquareParams.$liveSquareParams.$kitchenSquareParams.$placeSquareParams.$areaSquareParams.$floorParams.$floorsParams.$farSubwayParams.$readyParams.$typeHouseParams.$classObjectParams.$finishingParams.$repairsParams.$ppaParams.$entranceParams.$heightCeilingParams.$legalStatusParams.$plumbingParams.$sewerageParams.$transactionParams.$wcParams.$periodParams.$prepaymentParams.$deliveryParams.$furnitureParams.$communicationsParams.$electricityParams.$heatingParams.$gasParams.$furnitureKitchenParams.$balconyParams.$liftParams.$saunaParams.$waterParams.$garageParams.$forestParams.$tvParams.$washerParams.$fridgeParams.$animalParams.$childrenParams.$depositParams.$except_firstParams.$except_lastParams;
		if(count($relParamsSearch)>0){
			// $searchParams .= ' && '.implode(" || ",$relParamsSearch);
			$searchParams .= ' && ('.implode(" || ",$relParamsSearch).')';
		}
	}
	
	if(count($sadd)>0){
		if(!empty($special_add)){
			$special_add .= " && ".implode(" && ",$sadd);
		}
		else {
			$special_add = " && ".implode(" && ",$sadd);
		}
	}
	// echo $searchParams;
$finish = round(microtime(true) - $start,5);

// echo $finish;
}
$_ESTATE_ARRAY = array("1"=>"new","2"=>"flat","3"=>"room","4"=>"country","5"=>"commercial","6"=>"cession");

if(is_array($arrayLinkSearch) && count($arrayLinkSearch)>0){
	foreach($arrayLinkSearch as $name => $value){
		$var = $name."_".$_ESTATE_ARRAY[$arrayLinkSearch['estate']]."_".$arrayLinkSearch['type']."_var";
		// echo $var.'<br>';
		$$var = $value;
	}
}

if(!$location_id){
	$cityIdLocation = 1;
}
if($city_commercial_sell_var){
	$cityIdLocation = $city_commercial_sell_var;
}
if($city_commercial_rent_var){
	$cityIdLocation = $city_commercial_rent_var;
}
if($city_country_sell_var){
	$cityIdLocation = $city_country_sell_var;
}
if($city_country_rent_var){
	$cityIdLocation = $city_country_rent_var;
}
if($city_room_sell_var){
	$cityIdLocation = $city_room_sell_var;
}
if($city_room_rent_var){
	$cityIdLocation = $city_room_rent_var;
}
if($city_flat_sell_var){
	$cityIdLocation = $city_flat_sell_var;
}
if($city_flat_rent_var){
	$cityIdLocation = $city_flat_rent_var;
}
if($city_new_sell_var){
	$cityIdLocation = $city_new_sell_var;
}
/* if(isset($_SESSION['local']) && !empty($_SESSION['local'])){
	$cityIdLocation = $_SESSION['local'];
	$location_id = $_SESSION['local'];
} */

// echo $cityIdLocation;
// if($location_id){
	// $cityIdLocation = $location_id;
// }

/*
*  Делаем редирект на поддомен
*/
$dataLocation = array();
$data = array();
$res = mysql_query("
	SELECT *
	FROM ".$template."_location
	WHERE activation='1' && id='".$cityIdLocation."'
");
if(mysql_num_rows($res)>0){
	$row = mysql_fetch_assoc($res);
	$arr = array(
		"id" => $row['id'],
		"name" => $row['name'],
		"subdomen" => $row['subdomen'],
		"link" => $row['link'],
	);
	$REQUEST_URI = $_SERVER['REQUEST_URI']; ///search/snyat/kvartira/all/murmanskaya-obl?priceNightMin=&priceNightMax=
	$REQUEST_URI = str_replace('/'.$row['link'],'',$REQUEST_URI);
	$REQUEST_URI = str_replace($row['link'],'',$REQUEST_URI);
	$exLinkPages = explode('.',$_SERVER['HTTP_HOST']);// khmao.rbcom.ru
	
	if($exLinkPages[0]!='www' && ($cityIdLocation==1 || $cityIdLocation==2)){
		if($cityIdLocation==2){
			header("Location: https://www.rbcom.ru".$_SERVER['REQUEST_URI']);
			// header("Location: ".$_SERVER['REQUEST_URI']);
			exit;
		}
		if($row['link']=='sankt-peterburg'){
			header("Location: https://www.rbcom.ru".$REQUEST_URI);
			// header("Location: ".$REQUEST_URI);
			exit;
		}
	}
	if($cityIdLocation!=1 && $cityIdLocation!=2){
		if($exLinkPages[0]!=$row['subdomen']){
			$HTTP_HOST = $row['subdomen'].'.'.$exLinkPages[1].'.'.$exLinkPages[2];
		}
		else {
			$HTTP_HOST = $_SERVER['HTTP_HOST'];
		}
		// echo "https://".$HTTP_HOST.$REQUEST_URI;
		header("Location: https://".$HTTP_HOST.$REQUEST_URI);
		// header("Location: ".$HTTP_HOST.$REQUEST_URI);
		exit;
	}
}
/* /Делаем редирект на поддомен  */

?>