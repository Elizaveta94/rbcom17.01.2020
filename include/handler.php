<?php session_start();
include('../admin_2/include/bd.php');
include('../admin_2/include/functions.php');
require_once "../recaptchalib.php";
// ваш секретный ключ
$secret = "6Lc25CkUAAAAADzH-1bsb3HTrSuIRAWz5xcWqk73";
$response = null;
$reCaptcha = new ReCaptcha($secret);

/*
*  Проработанный вариант отправки с формы обратной связи
*/
if(isset($_POST['p_main']) && isset($_POST['feedback']) && isset($_POST['s'])){
	$p_main = (int)$_POST['p_main'];
	if(isset($_POST['b_code'])){
		if ($_POST['b_code'] == '') {
			$action = 'error';
			$title 	= 'Форма не отправлена!';
			$text 	= 'Не все поля заполнены!';
		}
		if ($_SESSION['captcha_keystring'] != $_POST['b_code']){
			$action = 'error';
			$title 	= 'Форма не отправлена!';
			$text 	= 'Не верно введен код с картинки!';
		}
		if ($_SESSION['captcha_keystring'] == $_POST['b_code']){
			$data = array("time");
			$values = array("'".time()."'");
			foreach ($_POST['s'] as $name => $value){
				$var = $name."_var";
				$value = stripslashes($value);
				$value = htmlspecialchars($value);
				$value = trim($value);
				$value = mysql_real_escape_string($value);
				array_push($data,$name);
				array_push($values,"'".$value."'");
				$$var = $value;
			}
			$insert = mysql_query ("
				INSERT INTO 
					".$template."_feedback_request (".implode(',',$data).") 
				VALUES(".implode(',',$values).")
			") or die(mysql_error());

			$res = mysql_query("SELECT * FROM ".$template."_m_feedback_left WHERE activation='1' && p_main='".$p_main."'",$db);
			$row = mysql_fetch_array($res);
			$email = $row['f_email'];
			
			$b_cont = '';
			$b_phone = '';
			$b_msg = '';
			if(isset($b_cont_var) && !empty($b_cont_var)){
				$b_cont = '<tr>
					<td style="border:1px solid #999;padding:7px 10px 5px;">Контактная информация:</td>
					<td style="border:1px solid #999;padding:7px 10px 5px;">'.$b_cont_var.'</td>
				</tr>';
			}
			if(isset($b_phone_var) && !empty($b_phone_var)){
				$b_phone = '<tr>
					<td style="border:1px solid #999;padding:7px 10px 5px;">Телефон:</td>
					<td style="border:1px solid #999;padding:7px 10px 5px;">'.$b_phone_var.'</td>
				</tr>';
			}
			if(isset($b_msg_var) && !empty($b_msg_var)){
				$b_msg = '<tr>
					<td style="border:1px solid #999;padding:7px 10px 5px;">Сообщение:</td>
					<td style="border:1px solid #999;padding:7px 10px 5px;">'.$b_msg_var.'</td>
				</tr>';
			}
			
			$subject = "Форма с сайта ".$_SERVER['HTTP_HOST']."";
			// Сообщение в виде HTML-формате
			$message = '<style type="text/css">div,p,span,strong,b,em,i,a,li,td{-webkit-text-size-adjust:none;}p,td {color: #8a8a8a;font-size:12px;line-height: 16px;}a {color: #60972f;}</style>
				<table style="font-family:Arial,sans-serif;background-color:#dbdbdb;background-repeat:no-repeat;background-position:center;background-size:cover;" width="100%" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td align="center">
							<table style="margin: 50px auto;-moz-border-radius:15px;-webkit-border-radius:15px;-ms-border-radius:15px;-o-border-radius:15px;-khtml-border-radius:15px;border-radius:15px;" width="640" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
								<tr>
									<td colspan="3" align="center">
										<table width="570" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
											<tr>
												<td style="padding-bottom:20px;">
												<p>Здравствуйте!<br> Только что с сайта <a href="http://'.$_SERVER['HTTP_HOST'].'">'.$_SERVER['HTTP_HOST'].'</a> Вам пришло письмо следующего содержания:<br></p>
												<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
													<tr>
														<td style="border:1px solid #999;padding:7px 10px 5px;" width="250">Имя:</td>
														<td style="border:1px solid #999;padding:7px 10px 5px;">'.$b_name_var.'</td>
													</tr>
													<tr>
														<td style="border:1px solid #999;padding:7px 10px 5px;">E-mail:</td>
														<td style="border:1px solid #999;padding:7px 10px 5px;">'.$email_var.'</td>
													</tr>
													'.$b_cont.'
													'.$b_phone.'
													'.$b_msg.'
												</table>
												<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
													<tr>
														<td>
															<p style="color:#222222;text-align:right">С уважением,<br>команда <a href="http://'.$_SERVER['HTTP_HOST'].'">'.$_SERVER['HTTP_HOST'].'</a></p>
														</td>
													</tr>
												</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!--content end-->
							</table>
						</td>
					</tr>
				</table>
			';
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; Charset=utf-8' . "\r\n";
			$headers .= 'From: '.$_SERVER['HTTP_HOST'].' <support@'.$_SERVER['HTTP_HOST'].'>' . "\r\n";
			ini_set("SMTP", "localhost");
			ini_set("smtp_port", "25");
			mail($email, $subject, $message, $headers);

			if($insert == 'true') {
				$action = 'ok';
				$title 	= 'Спасибо!';
				$text 	= 'Ваше письмо успешно отправлено! Мы Вам ответим в ближайшее время.';
			}
		}
	}
	else {
		$action = 'error';
		$title 	= 'Форма не отправлена!';
		$text 	= 'Не все поля заполнены!';
	}
	
	$dat = array(
	  "action" 	=> $action,
	  "title" 	=> $title,
	  "text" 	=> $text,
	  "module" 	=> "feedback"
	);
	 
	echo json_encode( $dat );
}

/*
*  Универсальная отправка форм с сайта v 2.0
*/
if(isset($_POST['s']) && isset($_POST['type_form'])){
	$action = 'error';
	$title = 'Ошибка!';
	$text = 'Произошла ошибка! Попробуйте повторить позднее';
	$date =	date('Y-m-d');
	$time = date('H:i:s');
	$type = $_POST['type_form'];

	if(isset($_POST["g-recaptcha-response"]) && !empty($_POST["g-recaptcha-response"])){
		$response = $reCaptcha->verifyResponse(
			$_SERVER["REMOTE_ADDR"],
			$_POST["g-recaptcha-response"],
			$secret
		);
		if($response != null && $response->success) {
			
		}
		else{
			$action = 'error';
			$title 	= 'Форма не отправлена!';
			$text 	= 'Вы не прошли проверку на робота!';
			sleep(1);	 
			$data = array(
			  "action" 	=> $action,
			  "title" 	=> $title,
			  "text" 	=> $text
			);
			echo json_encode($data);
			die();
		}
	}
	else{
		$action = 'error';
		$title 	= 'Форма не отправлена!';
		$text 	= 'Вы не прошли проверку на робота!';
		sleep(1);	 
		$data = array(
		  "action" 	=> $action,
		  "title" 	=> $title,
		  "text" 	=> $text
		);
		echo json_encode($data);
		die();
	}
		
	/*
	*  Инициализация форм на сайте
	*/
	$array_form = array("feed_block","guestbook","application","feedback","problema");// параметр отправки 
	$array_date = array("date","date_begin","date_end");// имя параметр для формирования даты из dd.mm.yyyy в yyyy-mm-dd  
	$array_form_table = array($template."_feedback_request",$template."_m_guestbook_left",$template."_clients",$template."_feedback_request",$template."_problems"); // Таблицы для работы
	
	$search = array_search($type,$array_form);
	if($search!=-1){
		$typeEstate = array("newbuilding"=>1,"flats"=>2,"rooms"=>3,"country"=>4,"commercial"=>5,"cession"=>6);
		$nameForEmail = array(1=>"email_new",2=>"email_second",3=>"email_rooms",4=>"email_country",5=>"email_com",6=>"email_cession");
		$arrayApp = array("msg","estate","p_main");
		$data = array("time","date");
		$values = array("'".$time."','".$date."'");
		$appData = array("time","date");
		$appValues = array("'".$time."','".$date."'");
		
		foreach ($_POST['s'] as $name => $value){
			$var = $name."_var";			
			$value = clearValueText($value);
			if(in_array($name,$array_date)){
				$value = date_discount($value);
			}
			if($name=='estate'){
				$value = $typeEstate[$value];
				array_push($appData,'page_link');
				array_push($appValues,"'".$_SERVER['HTTP_REFERER']."'");
			}
			if(!in_array($name,$arrayApp)){
				array_push($data,$name);
				array_push($values,"'".$value."'");
			}
			else {
				array_push($appData,$name);
				array_push($appValues,"'".$value."'");
			}
			$$var = $value;
		}
		
		/* if(isset($code_var)){
			// if ($_SESSION['captcha_keystring'] != $code_var) {
			if(empty($code_var)){
				$action = 'error';
				$title 	= 'Форма не отправлена!';
				$text 	= 'Не верно введен код с картинки!';
				sleep(2);	 
				$data = array(
				  "action" 	=> $action,
				  "title" 	=> $title,
				  // "title2" 	=> $_SESSION['captcha_keystring'],
				  "text" 	=> $text
				);
				echo json_encode($data);
				die();
			}
		} */
		
		$last_client = 0;
		$last_app = 0;
		if($search==2){
			array_push($data,'activation');
			array_push($values,"'1'");
		}
		if($search==3){
			array_push($data,'new');
			array_push($values,"'1'");
		}
		if($search==4){
			array_push($data,'new','link_page','user_id');
			array_push($values,"'1'","'".$_SERVER['HTTP_REFERER']."'","'".$_SESSION['idAuto']."'");
			if(isset($_POST['type_commer'])){
				array_push($data,'type_commer');
				array_push($values,"'".$_POST['type_commer']."'");
			}
			if(isset($_POST['type_country'])){
				array_push($data,'type_country');
				array_push($values,"'".$_POST['type_country']."'");
			}
			if(isset($_POST['type'])){
				array_push($data,'type');
				array_push($values,"'".$_POST['type']."'");
			}
			if(isset($_POST['estate'])){
				array_push($data,'estate');
				array_push($values,"'".$_POST['estate']."'");
			}
		}
		$insert = mysql_query ("
			INSERT INTO 
				".$array_form_table[$search]." (".implode(',',$data).") 
			VALUES(".implode(',',$values).")
		") or die(mysql_error());
		if($search==2){
			$res = mysql_query("
				SELECT id
				FROM ".$array_form_table[$search]."
				WHERE email='".$email_var."' || phone='".$phone_var."'
				ORDER BY id DESC
				LIMIT 1
			");
			if(mysql_num_rows($res)>0){
				$row = mysql_fetch_assoc($res);
				$last_client = $row['id'];
			}
		}
		
		// Тема сообщения
		$subject = "Форма с сайта ".$_SERVER['HTTP_HOST'];
		if($search==0){
			$text2 = '<center>Ваше сообщение успешно отправлено! <br>Оно появится здесь после проверки модератором.</center>';
		}
		if($search==1){
			$text2 = '<center>Ваше сообщение успешно отправлено! <br>Оно появится здесь после проверки модератором.</center>';
			$questbook = true;
			$subject = 'Отзыв с сайта '.$_SERVER['HTTP_HOST'];
		}
		if($search==2){
			$name_page = '';
			$text2 = '<center>Ваша заявка успешно отправлена! <br>С Вами свяжутся в ближайшее время.</center>';
			if(isset($estate_var) && isset($p_main_var)){
				if($estate_var==1){
					$name_page = ' по новостройкам';
					$type = "sell";
				}
				if($estate_var==2){
					$name_page = ' по вторичкам';
					$res = mysql_query("
						SELECT type
						FROM ".$template."_second_flats
						WHERE id='".(int)$p_main_var."'
					");
					if(mysql_num_rows($res)>0){
						$row = mysql_fetch_assoc($res);
						$type = $row['type'];
					}
				}
				if($estate_var==3){
					$name_page = ' по комнатам';
					$res = mysql_query("
						SELECT type
						FROM ".$template."_rooms
						WHERE id='".(int)$p_main_var."'
					");
					if(mysql_num_rows($res)>0){
						$row = mysql_fetch_assoc($res);
						$type = $row['type'];
					}
				}
				if($estate_var==4){
					$name_page = ' по загородной';
				}
				if($estate_var==5){
					$name_page = ' по коммерческой';
					$res = mysql_query("
						SELECT type
						FROM ".$template."_commercial
						WHERE id='".(int)$p_main_var."'
					");
					if(mysql_num_rows($res)>0){
						$row = mysql_fetch_assoc($res);
						$type = $row['type'];
					}
				}
				if($estate_var==6){
					$name_page = ' по переуступкам';
					$type = "sell";
				}
				array_push($appData,"client","type");
				array_push($appValues,"'".$last_client."'","'".$type."'");

				$insert = mysql_query ("
					INSERT INTO 
						".$template."_applications (".implode(',',$appData).")
					VALUES(".implode(',',$appValues).")
				");
				$res = mysql_query("
					SELECT id
					FROM ".$template."_applications
					ORDER BY id DESC
					LIMIT 1
				");
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$last_app = $row['id'];
				}
			}
			$subject = 'Заявка с сайта '.$_SERVER['HTTP_HOST'].$name_page;
		}
		if($search==3){
			$text2 = '<center>Ваше сообщение успешно отправлено! <br>Вам ответят в ближайшее время.</center>';
		}
		if($search==4){
			$text2 = '<center>Ваше сообщение успешно отправлено! <br>Вам ответят в ближайшее время.</center>';
			$subject = 'Проблема в личном кабинете на сайте '.$_SERVER['HTTP_HOST'];
		}
				
		$b_questbook = '';
		$b_link = '';
		$page_client = '';
		$page_app = '';
		$b_name = '';
		$b_email = '';
		$b_region = '';
		$b_theme = '';
		$b_msg = '';
		$b_phone = '';
		$b_agency = '';
		$b_delivery = '';
		$link = $_SERVER['HTTP_REFERER'];
		
		/*
		*  Ссылка, с который отправлена форма
		*/
		if(isset($link) && !empty($link)){
			$b_link = '<tr>
				<td style="border:1px solid #999;padding:7px 10px 5px;">Страница отправки:</td>
				<td style="border:1px solid #999;padding:7px 10px 5px;"><a href="'.$link.'">'.$link.'</a></td>
			</tr>';
		}
		
		if($search==2){
			$page_client = '<tr>
				<td style="border:1px solid #999;padding:7px 10px 5px;">ID клиента:</td>
				<td style="border:1px solid #999;padding:7px 10px 5px;"><a href="http://'.$_SERVER['HTTP_HOST'].'/admin_2/block_pages.php?block_module=clients">'.$last_client.'</a></td>
			</tr>';
			$page_app = '<tr>
				<td style="border:1px solid #999;padding:7px 10px 5px;">ID заявки:</td>
				<td style="border:1px solid #999;padding:7px 10px 5px;"><a href="http://'.$_SERVER['HTTP_HOST'].'/admin_2/block_pages.php?block_module=applications">'.$last_app.'</a></td>
			</tr>';
		}
		
		/*
		*  Дата отправки сообщения
		*/
		$b_date = '<tr>
			<td style="border:1px solid #999;padding:7px 10px 5px;width:160px">Дата отправки:</td>
			<td style="border:1px solid #999;padding:7px 10px 5px;">'.date_rus($date).'</td>
		</tr>';
		
		/*
		*  Время отправки сообщения
		*/
		$b_time = '<tr>
			<td style="border:1px solid #999;padding:7px 10px 5px;">Время отправки:</td>
			<td style="border:1px solid #999;padding:7px 10px 5px;">'.$time.'</td>
		</tr>';
		
		/*
		*  Имя автора сообщения
		*/
		if(isset($name_var) && !empty($name_var)){
			$b_name = '<tr>
				<td style="border:1px solid #999;padding:7px 10px 5px;">Имя:</td>
				<td style="border:1px solid #999;padding:7px 10px 5px;">'.$name_var.'</td>
			</tr>';
		}
		
		/*
		*  Email автора сообщения
		*/
		if(isset($email_var) && !empty($email_var)){
			$b_email = '<tr>
				<td style="border:1px solid #999;padding:7px 10px 5px;">E-mail:</td>
				<td style="border:1px solid #999;padding:7px 10px 5px;">'.$email_var.'</td>
			</tr>';
		}
		
		/*
		*  Регион
		*/
		if(isset($region_var) && !empty($region_var)){
			$region_name = '';
			$r = mysql_query("
				SELECT name
				FROM ".$template."_location
				WHERE id='".$region_var."' && activation=1
			");
			if(mysql_num_rows($r)>0){
				$rw = mysql_fetch_assoc($r);
				$region_name = $rw['name'];
			}
			$b_region = '<tr>
				<td style="border:1px solid #999;padding:7px 10px 5px;">Регион:</td>
				<td style="border:1px solid #999;padding:7px 10px 5px;">'.$region_name.'</td>
			</tr>';
		}
		
		/*
		*  Сообщение
		*/
		if(isset($msg_var) && !empty($msg_var)){
			$b_msg = '<tr>
				<td style="border:1px solid #999;padding:7px 10px 5px;vertical-align:top;">Сообщение:</td>
				<td style="border:1px solid #999;padding:7px 10px 5px;">'.$msg_var.'</td>
			</tr>';
		}
		
		/*
		*  Комментарий
		*/
		if(isset($comment_var) && !empty($comment_var)){
			$b_comment = '<tr>
				<td style="border:1px solid #999;padding:7px 10px 5px;vertical-align:top;">Сообщение:</td>
				<td style="border:1px solid #999;padding:7px 10px 5px;">'.$comment_var.'</td>
			</tr>';
		}
		
		/*
		*  Напоминание отзыва
		*/
		if($questbook){
			// $b_questbook = '<tr>
				// <td colspan="2" style="border:1px solid #999;padding:7px 10px 5px;"><i style="color:red">Внимание! Это отзыв со страницы <a href="http:///opinions">Отзывов</a>. Не забудьте отредактировать его в CMS, чтобы он появился на этой странице сайта.</i></td>
			// </tr>';
		}

		$mailInfo = $b_link.$page_client.$page_app.$b_name.$b_email.$b_region.$b_msg.$b_comment.$b_date.$b_time.$b_questbook;
		if($search==3){
			/*
			*  Телефон
			*/
			if(isset($phone_var) && !empty($phone_var)){
				$b_phone = '<tr>
					<td style="border:1px solid #999;padding:7px 10px 5px;vertical-align:top;">Телефон:</td>
					<td style="border:1px solid #999;padding:7px 10px 5px;">'.$phone_var.'</td>
				</tr>';
			}
			/*
			*  Тема сообщения
			*/
			if(isset($theme_var) && !empty($theme_var)){
				$b_theme = '<tr>
					<td style="border:1px solid #999;padding:7px 10px 5px;vertical-align:top;">Тема сообщения:</td>
					<td style="border:1px solid #999;padding:7px 10px 5px;">'.$theme_var.'</td>
				</tr>';
			}
			/*
			*  Агенство
			*/
			if(isset($agency_true_var) && !empty($agency_true_var) && isset($agency_var) && !empty($agency_var)){
				$b_agency = '<tr>
					<td style="border:1px solid #999;padding:7px 10px 5px;vertical-align:top;">Представляет агенство:</td>
					<td style="border:1px solid #999;padding:7px 10px 5px;">'.$agency_var.'</td>
				</tr>';
			}
			/*
			*  Рассылка
			*/
			if(isset($delivery_var) && !empty($delivery_var)){
				$b_delivery = '<tr>
					<td style="border:1px solid #999;padding:7px 10px 5px;vertical-align:top;">Получать рассылку:</td>
					<td style="border:1px solid #999;padding:7px 10px 5px;">Да</td>
				</tr>';
			}
		
			/*
			*  Сообщение
			*/
			if(isset($message_var) && !empty($message_var)){
				$b_msg = '<tr>
					<td style="border:1px solid #999;padding:7px 10px 5px;vertical-align:top;">Сообщение:</td>
					<td style="border:1px solid #999;padding:7px 10px 5px;">'.$message_var.'</td>
				</tr>';
			}
			$mailInfo = $b_link.$b_name.$b_phone.$b_email.$b_agency.$b_theme.$b_region.$b_msg.$b_delivery.$b_date.$b_time;
		}
		if($search==4){
			$b_type_commer = '';
			$b_type_country = '';
			/*
			*  Имя автора сообщения
			*/
			if(isset($name_var) && !empty($name_var)){
				$b_name = '<tr>
					<td style="border:1px solid #999;padding:7px 10px 5px;">Имя (ID):</td>
					<td style="border:1px solid #999;padding:7px 10px 5px;">'.$name_var.' ('.$_SESSION['idAuto'].')</td>
				</tr>';
			}
			/*
			*  Тема сообщения
			*/
			if(isset($theme_var) && !empty($theme_var)){
				$b_theme = '<tr>
					<td style="border:1px solid #999;padding:7px 10px 5px;vertical-align:top;">Тема сообщения:</td>
					<td style="border:1px solid #999;padding:7px 10px 5px;">'.$theme_var.'</td>
				</tr>';
			}
			/*
			*  Раздел, где наблюдалась проблема
			*/
			if(isset($_POST['type']) && !empty($_POST['type']) && isset($_POST['estate']) && !empty($_POST['estate'])){
				$type_name = 'Продажа';
				if($_POST['type']=='rent'){
					$type_name = 'Аренда';
				}
				$b_type = '<tr>
					<td style="border:1px solid #999;padding:7px 10px 5px;">Тип объявления в ЛК:</td>
					<td style="border:1px solid #999;padding:7px 10px 5px;">'.$type_name.'</td>
				</tr>';
				$b_estate = '<tr>
					<td style="border:1px solid #999;padding:7px 10px 5px;">Раздел в ЛК:</td>
					<td style="border:1px solid #999;padding:7px 10px 5px;">'.$_TYPE_ESTATE_NAME[$_POST['estate']].'</td>
				</tr>';
				if(isset($_POST['type_commer'])){
					$b_type_commer = '<tr>
						<td style="border:1px solid #999;padding:7px 10px 5px;">Подраздел коммерции:</td>
						<td style="border:1px solid #999;padding:7px 10px 5px;">'.$_TYPE_COMMERCIAL_ESTATE[$_POST['type_commer']].'</td>
					</tr>';
				}
				if(isset($_POST['type_country'])){
					$b_type_country = '<tr>
						<td style="border:1px solid #999;padding:7px 10px 5px;">Подраздел загородной:</td>
						<td style="border:1px solid #999;padding:7px 10px 5px;">'.$_TYPE_COUNTRY_ESTATE[$_POST['type_country']].'</td>
					</tr>';
				}
			}

			$mailInfo = $b_link.$page_client.$page_app.$b_type.$b_estate.$b_type_commer.$b_type_country.$b_name.$b_email.$b_theme.$b_msg.$b_comment.$b_date.$b_time.$b_questbook;
		}
		
		$HTTP_HOST = $_SERVER['HTTP_HOST'];
		
		// Сообщение в виде HTML-формате
		$message = '<style type="text/css">div,p,span,strong,b,em,i,a,li,td{-webkit-text-size-adjust:none;}p,td {color: #8a8a8a;font-size:12px;line-height: 16px;}a {color: #60972f;}</style>
		<table style="font-family:Arial,sans-serif;background:#fbf6ef;" width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td align="center">
					<table style="margin:50px auto;-moz-border-radius:15px;-webkit-border-radius:15px;-ms-border-radius:15px;-o-border-radius:15px;-khtml-border-radius:15px;border-radius:15px;" width="640" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
						<tr>
							<td colspan="3" align="center">
								<table width="570" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
									<tr>
										<td style="padding-bottom:20px;">
											<p>Здравствуйте!<br> Только что с сайта <a href="http://'.$HTTP_HOST.'">'.$HTTP_HOST.'</a> Вам пришло письмо следующего содержания:<br></p>
											<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
												'.$mailInfo.'
												<tr>
													<td colspan="2"><span style="color:#b8b8b8;font-size:11px;padding-top:15px;display:block">Данное письмо отправленно с официального сайта портала недвижимости '.$HTTP_HOST.'. Во избежании блокировки письма или попадания его в "спам" настоятельно рекомендуем добавить данный электронный адрес <strong>noreply@'.$HTTP_HOST.'</strong> в адресную книгу Вашей почты. Это снизит до нуля возможность пропустить важное письмо от нашей компании.</span></td>
												</tr>
											</table>
											<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
												<tr>
													<td>
														<p style="color:#222222;text-align:right">С уважением,<br>команда <a href="http://'.$HTTP_HOST.'">'.$HTTP_HOST.'</a></p>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<!--content end-->
					</table>
				</td>
			</tr>
		</table>';
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; Charset=utf-8' . "\r\n";
		$headers .= 'From: '.$HTTP_HOST.' <noreply@'.$HTTP_HOST.'>' . "\r\n";
		ini_set("SMTP", "localhost");
		ini_set("smtp_port", "25");
		$email_info = explode(',',$_MAINSET['emails']);
		// $email_info = array("dmitri1988@mail.ru");
		// if($search==2 && isset($estate_var)){
			// if(!empty($_MAINSET[$nameForEmail[$estate_var]])){
				// $email_info = explode(',',$_MAINSET[$nameForEmail[$estate_var]]);
			// }
		// }
		if(count($email_info)>1){
			for($c=0; $c<count($email_info); $c++){
				mail($email_info[$c], $subject, $message, $headers);
				sleep(1);
			}
		}
		else {
			mail($email_info[0], $subject, $message, $headers);
		}
		
		if($insert == 'true') {
			$action = 'ok';
			$title 	= 'Спасибо!';
			$text 	= $text2;
		}
	}
	
	$data = array(
	  "action" 	=> $action,
	  "title" 	=> $title,
	  "text" 	=> $text,
	);
	 
	echo json_encode($data);
}

// Регистрация нового пользователя
if(isset($_POST['actionForm']) && isset($_POST['s'])){
	$action = 'error';
	$title = 'Ошибка регистрации';
	$type = $_POST['actionForm'];
	$redirect = false;
	$text = 'Такой E-mail уже зарегистрирован в системе';
	if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')){
		foreach ($_POST['s'] as $name => $value){
			$var = $name."_var";			
			$value = clearValueText($value);
			$$var = $value;
		}
		/*
		*  Авторизация на сайте
		*/
		if($type=='auth'){
			$res = mysql_query ("
				SELECT id 
				FROM ".$template."_site_users 
				WHERE email='".$email_var."' && password='".md5($password_var)."' && activation='1'
			");
			if(mysql_num_rows($res)>0){
				$row = mysql_fetch_assoc($res);
				$id = $row['id'];
				$_SESSION['idAuto']	= $id;
				$redirect = true;
				$action = 'success';
				if($remember_var){
					setcookie("idAuto", $id, time()+9999999);
				}
				$text = '';
			}
			else {
				$title = 'Ошибка авторизации';
				$text = 'Данные не верны';
			}
			$data = array(
				"action" => $action,
				"title" => $title,
				"redirect" => $redirect,
				"link" => '/items',
				"text" => $text,
			);
			echo json_encode($data);
			exit();
		}
		
		/*
		*  Восстановление забытого пароля
		*/
		if($type=='forgot'){
			// $email_var = $_POST['email'];
			$email_var = clearValueText($email_var);
			$res = mysql_query ("
				SELECT id
				FROM ".$template."_site_users 
				WHERE email='".$email_var."' && del='0'
			") or die(mysql_error());
			if(mysql_num_rows($res)>0){
				$row = mysql_fetch_assoc($res);
				$id = $row['id'];
				$time = time();
				
				$arr = array('a','b','c','d','e','f',  
					 'g','h','i','j','k','l',  
					 'm','n','o','p','r','s',  
					 't','u','v','x','y','z',  
					 'A','B','C','D','E','F',  
					 'G','H','I','J','K','L',  
					 'M','N','O','P','R','S',  
					 'T','U','V','X','Y','Z',  
					 '1','2','3','4','5','6',  
					 '7','8','9','0');  
				// Генерируем пароль  
				$pass = "";  
				for($i = 0; $i < 8; $i++){  
				  // Вычисляем случайный индекс массива  
				  $index = rand(0, count($arr) - 1);  
				  $pass .= $arr[$index];  
				}
				$activation = md5($pass);

				// Тема сообщения
				$subject = "Восстановление пароля на сайте ".$_SERVER['HTTP_HOST'];
				// Сообщение в виде HTML-формате
				$message = '<style type="text/css">div,p,span,strong,b,em,i,a,li,td{-webkit-text-size-adjust:none;}p,td {color: #8a8a8a;font-size:12px;line-height: 16px;}a {color: #60972f;}</style>
					<table style="font-family:Arial,sans-serif;background-color:#FFFFFF;background-image:url('.$_SERVER['HTTP_HOST'].'/images/bg_main_fone2.jpg);background-repeat:no-repeat;background-position:center;background-size:cover;" width="100%" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td align="center">
								<table style="margin: 100px auto;-moz-border-radius:15px;-webkit-border-radius:15px;-ms-border-radius:15px;-o-border-radius:15px;-khtml-border-radius:15px;border-radius:15px;" width="640" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
									<tr>
										<td style="padding-top:20px;padding-left:20px;padding-bottom:20px" colspan="2">
											<div style="margin: 0; padding: 0; line-height: 17px;">
												<a target="_blank" style="text-decoration:none!important;" href="http://'.$_SERVER['HTTP_HOST'].'">
												<img src="http://'.$_SERVER['HTTP_HOST'].'/images/logo.png" border="0" width="132" height="65" style="display: block;margin:0 auto"/>
												</a>
											</div>
										</td>
									</tr>
									<!--content statrs-->
									<tr>
										<td colspan="3" align="center">
											<table width="570" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
												<tr>
													<td style="padding-bottom:0px;">
														<p style="font-size:14px;">Вы запросили восстановление пароля к аккаунту на сайте <a href="http://'.$_SERVER['HTTP_HOST'].'">'.$_SERVER['HTTP_HOST'].'</a>.<br><br>Ваш новый пароль: <strong>'.$pass.'</strong><br><br>Прежде чем использовать новый пароль необходимо его активировать. Для этого пройдите по ссылке ниже и Вы сразу войдете на сайт с новым паролем: <br></p>
														<a style="border: 1px solid #353535;
			height: 35px;
			line-height: 35px;
			font-size: 14px;
			color: #ffffff;
			text-shadow: none;
			text-align: center;
			font-weight: bold;
			width: 100%;
			background: #4c9980;
			display: block;
			text-decoration: none;
			background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzRjOTk4MCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMxZDhlNjkiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
			background: -moz-linear-gradient(top, #4c9980 0%, #1d8e69 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#4c9980), color-stop(100%,#1d8e69));
			background: -webkit-linear-gradient(top, #4c9980 0%,#1d8e69 100%);
			background: -o-linear-gradient(top, #4c9980 0%,#1d8e69 100%);
			background: -ms-linear-gradient(top, #4c9980 0%,#1d8e69 100%);
			background: linear-gradient(to bottom, #4c9980 0%,#1d8e69 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\'#4c9980\', endColorstr=\'#1d8e69\',GradientType=0 );" target="_blank" href="http://'.$_SERVER['HTTP_HOST'].'/?fg='.$activation.'&email='.$email_var.'">Активировать новый пароль</a>
														<p style="font-size:14px;">Для доступа к аккаунту в дальнейшем Вы можете использовать указанный выше пароль,
														но, в целях безопасности, мы Вам рекомендуем сменить его в личном кабинете.</p>
														<p style="font-size:14px;">Если произошла ошибка и Вы не запрашивали восстановления пароля, то просто проигнорируйте данное письмо.</p>
														<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
															<tr>
																<td>
																	<p style="color:#222222;text-align:right">С уважением,<br>команда <a href="http://'.$_SERVER['HTTP_HOST'].'">'.$_SERVER['HTTP_HOST'].'</a></p>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<!--content end-->
								</table>
							</td>
						</tr>
					</table>';
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; Charset=utf-8' . "\r\n";
				$headers .= 'From: '.$_SERVER['HTTP_HOST'].' <support@'.$_SERVER['HTTP_HOST'].'>' . "\r\n";
				ini_set("SMTP", "localhost");
				ini_set("smtp_port", "25");
				mail($email_var, $subject, $message, $headers);
				
				$insert = mysql_query("
					INSERT INTO pass_recovery (pass,user_id,date,activation) 
					VALUES('".$activation."','".$id."','".$time."','0')
				");

				if($insert == 'true') {
					$title = 'Восстановление пароля';
					$text = '<div class="success"><h3>Ваш новый пароль отправлен</h3><p>Ссылка для восстановления будет доступна в течении 24-х часов.</p></div>';
					$action = 'clear';
				}
				else {
					$title = 'Восстановление пароля';
					$text = '<div class="error"><p>К сожалению, произошла ошибка!</p></div>';
					$action = 'error';
				}
			}
			else {
				$title = 'Восстановление пароля';
				$text = '<div class="error"><p>Такой e-mail у нас не зарегистрирован!</p></div>';
				$action = 'error';
			}
		}
		
		/*
		*  Регистрация на сайте
		*/
		if($type=='reg'){
			$res = mysql_query ("
				SELECT id 
				FROM ".$template."_site_users 
				WHERE email='".$email_var."'
			");
			if(mysql_num_rows($res)==0){
				$delivery = 0;
				$agency_true = 0;
				if(isset($agency_true_var)){
					$agency_true = 1;
				}
				if(isset($_POST['delivery'])){
					$delivery = 1;
				}
				if($password_var==$password_repeat_var){
					if(!isset($agency_var)){
						$agency_var = '';
					}
					$insert = mysql_query ("
						INSERT INTO ".$template."_site_users (email,name,area_id,agency_true,agency,p_main,password,date_reg,delivery) 
						VALUES('".$email_var."','".$name_var."','".$region_var."','".$agency_true."','".$agency_var."','".$p_main_var."','".md5($password_var)."','".time()."','".$delivery."')
					") or die(mysql_error());
					
					$last_id = mysql_insert_id();
					$id = $last_id;
					
					/*
					*  Создание персонального аккаунта
					*/
					mysql_query ("INSERT INTO personal_account (user_id,cost) VALUES('".$id."',0)");
					
					/*
					*  Мобильный телефон
					*/
					$insertMobile = '';
					if(isset($_POST['mobile']) && is_array($_POST['mobile'])){
						foreach ($_POST['mobile'] as $n => $v){
							$var = $n."_mob";
							$v = clearValueText($v);
							$$var = $v;
						}
						if(isset($code_mob) && isset($phone_mob)){
							$insertMobile .= "INSERT INTO ".$template."_phone_users (user_id,type_phone,code_phone,number_phone,activation)";
							$insertMobile .= "VALUES('".$id."','0','".$code_mob."','".$phone_mob."','1')";
						}
					}
					
					/*
					*  Городской телефон
					*/
					if(isset($_POST['city']) && is_array($_POST['city'])){
						foreach ($_POST['city'] as $n => $v){
							$var = $n."_city";
							$v = clearValueText($v);
							$$var = $v;
						}
						if(isset($code_city) && isset($phone_city)){
							$insertMobile .= ",('".$id."','1','".$code_city."','".$phone_city."','1')";
						}
					}
					
					if(!empty($insertMobile)){
						$insertMobile .= ";";
						mysql_query ("
							".$insertMobile."
						") or die(mysql_error());
					}					
					
					$activation = md5(md5($id).md5($email_var));
					mysql_query("
						UPDATE ".$template."_site_users
						SET hash='".$activation."'
						WHERE id='".$id."'
					");
					
					// $arrayEmails = array("info@".$_SERVER['HTTP_HOST'],$email_var);
					$arrayEmails = array("support@".$_SERVER['HTTP_HOST'],$email_var);

					mkdir($_SERVER['DOCUMENT_ROOT']."/users/$id", 0755);

					// Тема сообщения
					$subject = "Подтверждение регистрации на сайте ".$_SERVER['HTTP_HOST'];
					// Сообщение в виде HTML-формате
					$message =  array('
						<style type="text/css">div,p,span,strong,b,em,i,a,li,td{-webkit-text-size-adjust:none;}p,td {color: #8a8a8a;font-size:12px;line-height: 16px;}a {color: #60972f;}</style>
						<table style="font-family:Arial,sans-serif;background-color:#FFFFFF;background-image:url('.$_SERVER['HTTP_HOST'].'/images/bg_main_fone2.jpg);background-repeat:no-repeat;background-position:center;background-size:cover;" width="100%" cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td align="center">
									<table style="margin: 100px auto;-moz-border-radius:15px;-webkit-border-radius:15px;-ms-border-radius:15px;-o-border-radius:15px;-khtml-border-radius:15px;border-radius:15px;" width="640" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
										<tr>
											<td style="padding-top:20px;padding-left:20px;padding-bottom:20px" colspan="2">
												<div style="margin: 0; padding: 0; line-height: 17px;">
													<a target="_blank" style="text-decoration:none!important;" href="http://'.$_SERVER['HTTP_HOST'].'">
													<img src="http://'.$_SERVER['HTTP_HOST'].'/images/logo.png" border="0" width="132" height="65" style="display: block;margin:0 auto"/>
													</a>
												</div>
											</td>
										</tr>
										<!--content statrs-->
										<tr>
											<td colspan="3" align="center">
												<table width="570" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
													<tr>
														<td style="padding-bottom:20px;">
														<p>
														Здравствуйте,<br>Вы (или кто-то другой) зарегистрировались на <a href="http://'.$_SERVER['HTTP_HOST'].'">'.$_SERVER['HTTP_HOST'].'</a>.<br>При регистрации был указан данный e-mail. <br>
														Для подтверждения регистрации пройдите по этой ссылке: <br></p>
														<!--<a target="_blank" style="color:#39b1f5" href="http://'.$_SERVER['HTTP_HOST'].'/activation?email='.$email_var.'&code='.$activation.'">http://'.$_SERVER['HTTP_HOST'].'/activation?email='.$email_var.'&code='.$activation.'</a>-->
														<a style="border: 1px solid #353535;
				height: 35px;
				line-height: 35px;
				font-size: 14px;
				color: #ffffff;
				text-shadow: none;
				text-align: center;
				font-weight: bold;
				width: 100%;
				background: #4c9980;
				display: block;
				text-decoration: none;
				background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzRjOTk4MCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMxZDhlNjkiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
				background: -moz-linear-gradient(top, #4c9980 0%, #1d8e69 100%);
				background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#4c9980), color-stop(100%,#1d8e69));
				background: -webkit-linear-gradient(top, #4c9980 0%,#1d8e69 100%);
				background: -o-linear-gradient(top, #4c9980 0%,#1d8e69 100%);
				background: -ms-linear-gradient(top, #4c9980 0%,#1d8e69 100%);
				background: linear-gradient(to bottom, #4c9980 0%,#1d8e69 100%);
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\'#4c9980\', endColorstr=\'#1d8e69\',GradientType=0 );" target="_blank" href="http://'.$_SERVER['HTTP_HOST'].'/activation?email='.$email_var.'&code='.$activation.'">Подтвердить регистрацию</a>
														<center><p>Для отмены регистрации — просто проигнорируйте это письмо.</p></center>
														<span style="color:#b8b8b8;font-size:11px;padding-top:5px;display:block">Не следует отвечать на это письмо. Если у Вас остались вопросы, обратитесь в нашу
						службу поддержки пользователей на support@'.$_SERVER['HTTP_HOST'].'.</span></p>										
														<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
															<tr>
																<td>
																	<p style="color:#222222;text-align:right">С уважением,<br>команда <a href="http://'.$_SERVER['HTTP_HOST'].'">'.$_SERVER['HTTP_HOST'].'</a></p>
																</td>
															</tr>
														</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<!--content end-->
									</table>
								</td>
							</tr>
						</table>
					',
					'<style type="text/css">div,p,span,strong,b,em,i,a,li,td{-webkit-text-size-adjust:none;}p,td {color: #8a8a8a;font-size:12px;line-height: 16px;}a {color: #60972f;}</style>
					<table style="font-family:Arial,sans-serif;background-color:#FFFFFF;background-image:url('.$_SERVER['HTTP_HOST'].'/images/bg_main_fone2.jpg);background-repeat:no-repeat;background-position:center;background-size:cover;" width="100%" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td align="center">
								<table style="margin: 100px auto;-moz-border-radius:15px;-webkit-border-radius:15px;-ms-border-radius:15px;-o-border-radius:15px;-khtml-border-radius:15px;border-radius:15px;" width="640" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
									<tr>
										<td style="padding-top:20px;padding-left:20px;padding-bottom:20px" colspan="2">
											<div style="margin: 0; padding: 0; line-height: 17px;">
												<a target="_blank" style="text-decoration:none!important;" href="http://'.$_SERVER['HTTP_HOST'].'">
												<img src="http://'.$_SERVER['HTTP_HOST'].'/images/logo.png" border="0" width="132" height="65" style="display: block;margin:0 auto"/>
												</a>
											</div>
										</td>
									</tr>
									<!--content statrs-->
									<tr>
										<td colspan="3" align="center">
											<table width="570" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
												<tr>
													<td style="padding-bottom:20px;">
													<p>Пользователь по имени <strong>'.$name_var.'</strong> и email <strong>'.$email_var.'</strong> только что зарегистрировался(лась) на <a href="http://'.$_SERVER['HTTP_HOST'].'">'.$_SERVER['HTTP_HOST'].'</a>.<br><strong>ID пользователя: </strong>'.$id.'</p>										
													<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
														<tr>
															<td>
																<p style="color:#222222;text-align:right">С уважением,<br>команда <a href="http://'.$_SERVER['HTTP_HOST'].'">'.$_SERVER['HTTP_HOST'].'</a></p>
															</td>
														</tr>
													</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<!--content end-->
								</table>
							</td>
						</tr>
					</table>
					');
					// error_reporting(E_ALL); // Вывод ошибок.
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; Charset=utf-8' . "\r\n";
					$headers .= 'From: '.$_SERVER['HTTP_HOST'].' <support@'.$_SERVER['HTTP_HOST'].'>' . "\r\n";
					ini_set("SMTP", "localhost");
					ini_set("smtp_port", "25");
					for($i=0; $i<count($arrayEmails); $i++){
						$mess = $message[0];
						if($i==0){
							$mess = $message[1];
						}
						mail($arrayEmails[$i], $subject, $mess, $headers);	
						sleep(1);				
					}

					if($insert == 'true') {
						$title = 'Регистрация на сайте';
						$text = '<div class="success"><h3>Спасибо за регистрацию на сайте</h3><p>Вам на почту отправлено письмо с подтверждением вашей почты.</p></div>';
						$action = 'clear';
					}
					else {
						$text = '<div class="error"><p>К сожалению, произошла ошибка!</p></div>';
						$action = 'error';
					}
				}
				else {
					$action = 'error';
					$title = 'Регистрация на сайте';
					$text = '<div class="error"><p>Пароли не совпадают!</p></div>';
				}
			}
		}
		$data = array(
			"action" => $action,
			"title" => $title,
			"text"  => $text
		);
		// sleep(2);
		echo json_encode($data);
	}
}

if(isset($_POST['email_activate']) && isset($_POST['hash'])){
	$action = 'error';
	$title = 'Ошибка';
	$text = 'Такой E-mail уже зарегистрирован на сайте';
	$res = mysql_query("
		SELECT *
		FROM ".$template."_site_users
		WHERE email='".clearValueText($_POST['email_activate'])."'
	");
	if(mysql_num_rows($res)>0){
		$data = array(
			"action" => $action,
			"title" => $title,
			"text"  => $text
		);
		echo json_encode($data);
		exit;
	}
	if(isset($_SESSION['auth_hash'])){
		$email_var = $_POST['email_activate'];
		$hash = preg_replace("/[^0-9a-z]/", '', $_SESSION['auth_hash']);
		$res = mysql_query("
			SELECT *
			FROM ".$template."_auth
			WHERE hash='".$hash."'
		");
		if(mysql_num_rows($res)>0){
			$userInfo2 = mysql_fetch_assoc($res);
			$insert = mysql_query ("
				INSERT INTO ".$template."_site_users (social,email,uid,name,surname,date,time,activation) 
				VALUES('1','".clearValueText($email_var)."','".$userInfo2['uid']."','".clearValueText($userInfo2['first_name'])."','".clearValueText($userInfo2['last_name'])."','".date("Y-m-d")."','".date("H:i:s")."','0')
			") or die(mysql_error());
				
			$res = mysql_query ("
				SELECT id 
				FROM ".$template."_site_users 
				WHERE uid='".$userInfo2['uid']."'
			");
			$row = mysql_fetch_assoc($res);
			$id = $row['id'];
				
			$activation = md5(md5($id).md5($email_var));
			mysql_query("
				UPDATE ".$template."_site_users
				SET hash='".$activation."'
				WHERE id='".$id."'
			");
			
			$arrayEmails = array("support@".$_SERVER['HTTP_HOST'],$email_var);
			// $arrayEmails = array("dmitri1988@Mail.ru",$email_var);

			mkdir($_SERVER['DOCUMENT_ROOT']."/users/$id", 0755);
			mkdir($_SERVER['DOCUMENT_ROOT']."/users/$id/gallery", 0755);
			mkdir($_SERVER['DOCUMENT_ROOT']."/users/$id/story", 0755);
			mkdir($_SERVER['DOCUMENT_ROOT']."/users/$id/video", 0755);
			
			$photo_max_orig = $userInfo2['photo_max_orig'];
			
			/*
			*  Ссылка на изображение извне
			*/
			$response = file_get_contents('https://api.vk.com/method/photos.get?owner_id='.$userInfo2['uid'].'&album_id=profile&v=5.2'); //делаем запрос к апи vk
			$dataImage = json_decode($response, true);
			$dataResponse = $dataImage['response'];
			$countImage = $dataResponse['count'];
			if($countImage>0){
				$uploaddir = $_SERVER['DOCUMENT_ROOT']."/users/".$id."/"; // Оригинал
				$big = 		 $_SERVER['DOCUMENT_ROOT']."/users/".$id."/"; // Большое изображение по ширине 450px
				$large = 	 $_SERVER['DOCUMENT_ROOT']."/users/".$id."/"; // 216x175
				$middle = 	 $_SERVER['DOCUMENT_ROOT']."/users/".$id."/"; // 106x106
				$min = 		 $_SERVER['DOCUMENT_ROOT']."/users/".$id."/"; // 45x45

				//новое имя изображения
				$apend  = substr(md5(date('YmdHis').rand(100,1000)),0,20).".jpg";
				$apend1 = substr(md5(date('YmdHis').rand(100,1000)),0,20).".jpg";
				$apend2 = substr(md5(date('YmdHis').rand(100,1000)),0,20).".jpg";
				$apend3 = substr(md5(date('YmdHis').rand(100,1000)),0,20).".jpg";
				$apend4 = substr(md5(date('YmdHis').rand(100,1000)),0,20).".jpg";

				//путь к новому изображению
				$uploadfile = "$uploaddir$apend";
				$bigfile = "$big$apend1";
				$largefile = "$large$apend2";
				$middlefile = "$middle$apend3";
				$minfile = "$min$apend4";
				
				$photo = $dataResponse['items'][$countImage-1];
				if($photo['photo_2560']){
					$url = $photo['photo_2560'];
				}
				else {
					if($photo['photo_1280']){
						$url = $photo['photo_1280'];
					}
					else {
						if($photo['photo_807']){
							$url = $photo['photo_807'];
						}
						else {
							if($photo['photo_604']){
								$url = $photo['photo_604'];
							}
							else {
								$url = $photo_max_orig;
							}
						}
					}
				}
				
				file_put_contents($uploadfile, file_get_contents($url));
				
				if($size[0] > $size[1]){
					resize($uploadfile,$bigfile, 0, 400,false);

					resize($bigfile,$largefile, 0, 103,false);
					crop($largefile,$largefile, array(0, 0, 103, 103),false,103,$position = 'center',$type_img = 'H');
					
					resize($bigfile,$middlefile, 0, 32,false);
					crop($middlefile,$middlefile, array(0, 0, 32, 32),false,32,$position = 'center',$type_img = 'H');
				}
				else {
					resize($uploadfile,$bigfile, 0, 400,false);

					resize($bigfile,$largefile, 0, 103,false);
					crop($largefile,$largefile, array(0, 0, 103, 103),false,103,$position = 'center',$type_img = 'H');
					
					resize($bigfile,$middlefile, 32, 0,false);
					crop($middlefile,$middlefile, array(0, 0, 32, 32),false,32,$position = 'center',$type_img = 'H');
				}
				list($w_size, $h_size) = getimagesize($bigfile);
				$middlefile = explode("/",$middlefile);
				$largefile = explode("/",$largefile);
				$bigfile = explode("/",$bigfile);
				$uploadfile = explode("/",$uploadfile);
				
				$middlefile = $middlefile[count($middlefile)-1];
				$largefile = $largefile[count($largefile)-1];
				$bigfile = $bigfile[count($bigfile)-1];
				$uploadfile = $uploadfile[count($uploadfile)-1];
				
				$arr2 = $middlefile.",".$largefile.",".$bigfile.",".$uploadfile;
				$coords = '0,103,0,103,103,103';
				$update = mysql_query ("
					UPDATE ".$template."_site_users
					SET avatar='".$arr2."',ava_set='".$coords."'  
					WHERE id='".$id."' && activation='0'
				");
			}

			$activation = md5(md5($id).md5($email_var));
			mysql_query("
				UPDATE ".$template."_site_users
				SET hash='".$activation."'
				WHERE id='".$id."'
			");

			// Тема сообщения
			$subject = "Подтверждение регистрации на сайте ".$_SERVER['HTTP_HOST'];
			// Сообщение в виде HTML-формате
			$message =  array('
				<style type="text/css">div,p,span,strong,b,em,i,a,li,td{-webkit-text-size-adjust:none;}p,td {color: #8a8a8a;font-size:12px;line-height: 16px;}a {color: #60972f;}</style>
				<table style="font-family:Arial,sans-serif;background-color:#FFFFFF;background-image:url('.$_SERVER['HTTP_HOST'].'/images/bg_main_fone2.jpg);background-repeat:no-repeat;background-position:center;background-size:cover;" width="100%" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td align="center">
							<table style="margin: 100px auto;-moz-border-radius:15px;-webkit-border-radius:15px;-ms-border-radius:15px;-o-border-radius:15px;-khtml-border-radius:15px;border-radius:15px;" width="640" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
								<tr>
									<td style="padding-top:20px;padding-left:20px;padding-bottom:20px" colspan="2">
										<div style="margin: 0; padding: 0; line-height: 17px;">
											<a target="_blank" style="text-decoration:none!important;" href="http://'.$_SERVER['HTTP_HOST'].'">
											<img src="http://'.$_SERVER['HTTP_HOST'].'/images/logo.png" border="0" width="132" height="65" style="display: block;margin:0 auto"/>
											</a>
										</div>
									</td>
								</tr>
								<!--content statrs-->
								<tr>
									<td colspan="3" align="center">
										<table width="570" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
											<tr>
												<td style="padding-bottom:20px;">
												<p>
												Здравствуйте,<br>Вы (или кто-то другой) зарегистрировались на <a href="http://'.$_SERVER['HTTP_HOST'].'">'.$_SERVER['HTTP_HOST'].'</a>.<br>При регистрации был указан данный e-mail. <br>
												Для подтверждения регистрации пройдите по этой ссылке: <br></p>
												<!--<a target="_blank" style="color:#39b1f5" href="http://'.$_SERVER['HTTP_HOST'].'/?email='.$email_var.'&code='.$activation.'">http://'.$_SERVER['HTTP_HOST'].'/?email='.$email_var.'&code='.$activation.'</a>-->
												<a style="border: 1px solid #353535;
			height: 35px;
			line-height: 35px;
			font-size: 14px;
			color: #ffffff;
			text-shadow: none;
			text-align: center;
			font-weight: bold;
			width: 100%;
			background: #4c9980;
			display: block;
			text-decoration: none;
			background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzRjOTk4MCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMxZDhlNjkiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
			background: -moz-linear-gradient(top, #4c9980 0%, #1d8e69 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#4c9980), color-stop(100%,#1d8e69));
			background: -webkit-linear-gradient(top, #4c9980 0%,#1d8e69 100%);
			background: -o-linear-gradient(top, #4c9980 0%,#1d8e69 100%);
			background: -ms-linear-gradient(top, #4c9980 0%,#1d8e69 100%);
			background: linear-gradient(to bottom, #4c9980 0%,#1d8e69 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\'#4c9980\', endColorstr=\'#1d8e69\',GradientType=0 );" target="_blank" href="http://'.$_SERVER['HTTP_HOST'].'/?email='.$email_var.'&code='.$activation.'">Подтвердить регистрацию</a>
												<center><p>Для отмены регистрации — просто проигнорируйте это письмо.</p></center>
												<span style="color:#b8b8b8;font-size:11px;padding-top:5px;display:block">Не следует отвечать на это письмо. Если у Вас остались вопросы, обратитесь в нашу
				службу поддержки пользователей на support@'.$_SERVER['HTTP_HOST'].'.</span></p>										
												<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
													<tr>
														<td>
															<p style="color:#222222;text-align:right">С уважением,<br>команда <a href="http://'.$_SERVER['HTTP_HOST'].'">'.$_SERVER['HTTP_HOST'].'</a></p>
														</td>
													</tr>
												</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!--content end-->
							</table>
						</td>
					</tr>
				</table>
			',
			'<style type="text/css">div,p,span,strong,b,em,i,a,li,td{-webkit-text-size-adjust:none;}p,td {color: #8a8a8a;font-size:12px;line-height: 16px;}a {color: #60972f;}</style>
			<table style="font-family:Arial,sans-serif;background-color:#FFFFFF;background-image:url('.$_SERVER['HTTP_HOST'].'/images/bg_main_fone2.jpg);background-repeat:no-repeat;background-position:center;background-size:cover;" width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td align="center">
						<table style="margin: 100px auto;-moz-border-radius:15px;-webkit-border-radius:15px;-ms-border-radius:15px;-o-border-radius:15px;-khtml-border-radius:15px;border-radius:15px;" width="640" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
							<tr>
								<td style="padding-top:20px;padding-left:20px;padding-bottom:20px" colspan="2">
									<div style="margin: 0; padding: 0; line-height: 17px;">
										<a target="_blank" style="text-decoration:none!important;" href="http://'.$_SERVER['HTTP_HOST'].'">
										<img src="http://'.$_SERVER['HTTP_HOST'].'/images/logo.png" border="0" width="132" height="65" style="display: block;margin:0 auto"/>
										</a>
									</div>
								</td>
							</tr>
							<!--content statrs-->
							<tr>
								<td colspan="3" align="center">
									<table width="570" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
										<tr>
											<td style="padding-bottom:20px;">
											<p>Пользователь по имени <strong>'.$name_var.'</strong> и email <strong>'.$email_var.'</strong> только что зарегистрировался(лась) на <a href="http://'.$_SERVER['HTTP_HOST'].'">'.$_SERVER['HTTP_HOST'].'</a>.<br><strong>ID пользователя: </strong>'.$id.'</p>										
											<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
												<tr>
													<td>
														<p style="color:#222222;text-align:right">С уважением,<br>команда <a href="http://'.$_SERVER['HTTP_HOST'].'">'.$_SERVER['HTTP_HOST'].'</a></p>
													</td>
												</tr>
											</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<!--content end-->
						</table>
					</td>
				</tr>
			</table>
			');
			// error_reporting(E_ALL); // Вывод ошибок.
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; Charset=utf-8' . "\r\n";
			$headers .= 'From: '.$_SERVER['HTTP_HOST'].' <support@'.$_SERVER['HTTP_HOST'].'>' . "\r\n";
			ini_set("SMTP", "localhost");
			ini_set("smtp_port", "25");
			for($i=0; $i<count($arrayEmails); $i++){
				$mess = $message[0];
				if($i==0){
					$mess = $message[1];
				}
				mail($arrayEmails[$i], $subject, $mess, $headers);	
				sleep(1);				
			}

			if($insert == 'true') {
				$title = 'Регистрация на сайте';
				$text = '<div class="success"><h3>Спасибо за регистрацию на сайте</h3><p>Вам на почту отправлено письмо с подтверждением вашей почты.</p></div>';
				$action = 'clear';
			}
			else {
				$title = 'Ошибка';
				$text = '<div class="error"><p>К сожалению, произошла ошибка!</p></div>';
				$action = 'error';
			}
		}
	}
	$data = array(
		"action" => $action,
		"title" => $title,
		"text"  => $text
	);
	echo json_encode($data);
}

if(isset($_POST['mainPhoto'])){
	$action = 'error';
	$id = (int)$_POST['mainPhoto'];
	$type = 'cover';
	$cover = 1;
	$res = mysql_query("
		SELECT cover
		FROM ".$template."_photo_catalogue
		WHERE id='".$id."' && user_id='".$_SESSION['idAuto']."'
	");
	if(mysql_num_rows($res)>0){
		$row = mysql_fetch_assoc($res);
		
		if($row['cover']==1){
			$type='not_cover';
			$cover = 0;
		}
		$upd = mysql_query("
			UPDATE ".$template."_photo_catalogue
			SET cover='".$cover."'
			WHERE id='".$id."' && user_id='".$_SESSION['idAuto']."'
		");
		if($upd=='true'){
			$action = 'success';
		}
	}
	$data = array(
		"action" => $action,
		"type" => $type,
	);
	echo json_encode($data);
}

if(isset($_POST['delPhoto'])){
	$action = 'error';
	$id = (int)$_POST['delPhoto'];
	$res = mysql_query("
		SELECT images
		FROM ".$template."_photo_catalogue
		WHERE id='".$id."' && user_id='".$_SESSION['idAuto']."'
	");
	if(mysql_num_rows($res)>0){
		$row = mysql_fetch_assoc($res);

		$images = $row['images'];
		$del = mysql_query("
			DELETE FROM ".$template."_photo_catalogue 
			WHERE id='".$id."' && user_id='".$_SESSION['idAuto']."'
		");
		$ex_images = explode(',',$images);
		for($i=0; $i<count($ex_images); $i++){
			unlink($_SERVER['DOCUMENT_ROOT']."/users/".$_SESSION['idAuto']."/gallery/".$ex_images[$i]);
		}

		if($del=='true'){
			$action = 'success';
		}
	}
	$data = array(
		"action" => $action,
	);
	echo json_encode($data);
}

if($_POST['cutPhoto']){
	$action = 'error';
	$id = (int)$_POST['cutPhoto'];
	$arr = array();
	$res = mysql_query("
		SELECT images,coords
		FROM ".$template."_photo_catalogue
		WHERE id='".$id."' && user_id='".$_SESSION['idAuto']."'
	");
	if(mysql_num_rows($res)>0){
		$row = mysql_fetch_assoc($res);
		$action = 'success';
		$coords = explode(',',$row['coords']);
		$ex_images = explode(',',$row['images']);
		$size = getimagesize($_SERVER['DOCUMENT_ROOT']."/users/".$_SESSION['idAuto']."/gallery/".$ex_images[3]);
		$arr = array($action,$_SESSION['idAuto'],$ex_images[0],$ex_images[1],$ex_images[2],$ex_images[3],$ex_images[4],$coords[0],$coords[2],$coords[1],$coords[3],$id,$size[0],$size[1]);
	}
	$data = array(
		"action" => $action,
		"img" => $arr,
	);
	echo json_encode($data);
}

/*
*  Получение координат
*/
if(isset($_POST['geolocation'])){
	$address = clearValueText($_POST['geolocation']);
	$ex_coords = array('30.315868','59.939095');
	
	if(!empty($_SESSION['coords'])){
		$ex_coords = explode(' ',$_SESSION['coords']);
	}
	
	if(isset($_POST['location'])){
		$location = intval($_POST['location']);
	}
	
	$area_id = 0;
	// $res = mysql_query("
		// SELECT *
		// FROM ".$template."_site_users
		// WHERE id=".$_SESSION['idAuto']."
	// ");
	// if(@mysql_num_rows($res)>0){
		// $row = mysql_fetch_assoc($res);
		// $area_id = $row['area_id'];
	// }
	
	if($location){
		$area_id = $location;
	}
	
	$cityRegionName = 'Санкт-Петербург';
	$coordsPlace = '30.315868 59.939095';
	$ex_place_array = explode(' ',$coordsPlace);
	if(!empty($area_id)){
		$rs = mysql_query("
			SELECT *
			FROM ".$template."_location
			WHERE activation='1' && id=".$area_id."
		");
		if(mysql_num_rows($rs)>0){
			$rw = mysql_fetch_assoc($rs);
			$cityRegionName = $rw['name'];
			if(!empty($rw['coords'])){
				$coordsPlace = $rw['coords'];
				$ex_place_array = explode(' ',$coordsPlace);
			}
		}
	}	
	
	$params2 = array(
		'geocode' => $cityRegionName.', '.$address,
		'format'  => 'json',
		'results' => 100,
		// 'key'     => 'AKDIIFYBAAAAqIYtRgIAZYb8P32hIxnbRRSe9CpggONv4PEAAAAAAAAAAADu6wMZJIEJ0SDd0mRe33ag1JRdNA==',
	);
	$response = json_decode(@file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params2)));

	// echo '<pre>'; 
	// print_r($response->response->GeoObjectCollection);
	// echo '<pre>';
	$arrayNames = array();
	if($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0){
		// echo '<pre>'; 
		// print_r($response->response->GeoObjectCollection);
		// echo '<pre>';
		$allResult = $response->response->GeoObjectCollection->featureMember;
		if(count($allResult)>0){
			for($c=0; $c<count($allResult); $c++){
				$nameObject = $allResult[$c]->GeoObject->metaDataProperty->GeocoderMetaData->AddressDetails->Country->AddressLine;
				$locationName = $allResult[$c]->GeoObject->metaDataProperty->GeocoderMetaData->Address->Components;
				$locationName2 = '';
				$areaName2 = '';
				$locality = '';
				for($l=0; $l<count($locationName); $l++){
					// echo '<pre>';
					// print_r( $locationName[$l] );
					// echo '</pre>';
					if($locationName[$l]->kind=='province'){
						$locationName2 = $locationName[$l]->name;
					}
					if($locationName[$l]->kind=='area'){
						if(empty($areaName2)){
							$areaName2 = $locationName[$l]->name;
						}
					}
					if($locationName[$l]->kind=='locality'){
						if(empty($locality)){
							$locality = $locationName[$l]->name;
						}
					}
				}
				$coords = $allResult[$c]->GeoObject->Point->pos;
				$locs = '';
				$locsArray = array();
				if(!empty($locationName2)){
					array_push($locsArray,$locationName2);
				}
				if(!empty($areaName2)){
					array_push($locsArray,$areaName2);
				}
				if(!empty($locality)){
					array_push($locsArray,$locality);
				}
				if(count($locsArray)>0){
					$locs = implode('|',$locsArray);
				}
				
				$arr = array(
					"location" => $locationName2,
					"area" => $areaName2,
					"locality" => $locs,
					"address" => $nameObject,
					"coords" => $coords,
					"metro" => $metro,
				);
				array_push($arrayNames,$arr);
			}
		}
	}
	echo json_encode($arrayNames);
}

// Включение/отключение объявления пользователем
if(isset($_POST['toggleAds']) && isset($_POST['id'])){
	$action = 'error';
	$status = (int)$_POST['toggleAds'];
	$text = 'Объявление не включено. Попробуйте повторить позднее';
	if($status==0){
		$text = 'Объявление не отключено. Попробуйте повторить позднее';
	}
	$id_ad = (int)$_POST['id'];
	$res = mysql_query("
		SELECT *
		FROM ".$template."_user_ads
		WHERE id='".$id_ad."' && user_id='".$_SESSION['idAuto']."'
	");
	if(mysql_num_rows($res)>0){
		$upd = mysql_query("
			UPDATE ".$template."_user_ads
			SET active='".$status."'
			WHERE id='".$id_ad."' && user_id='".$_SESSION['idAuto']."'
		");
		if($upd=='true'){
			$action = 'success';
			$text = '';
		}
	}
	$data = array(
		"action" => $action,
		"text"  => $text
	);
	echo json_encode($data);
}

// Удаление объявления пользователя
if(isset($_POST['delAds']) && isset($_POST['id'])){
	$action = 'error';
	$text = 'Ошибка удаления';
	$text = 'Объявление не удалено. Попробуйте повторить позднее';
	$id_ad = (int)$_POST['id'];
	$res = mysql_query("
		SELECT *
		FROM ".$template."_user_ads
		WHERE id='".$id_ad."' && user_id='".$_SESSION['idAuto']."'
	");
	if(mysql_num_rows($res)>0){
		$row = mysql_fetch_assoc($res);
		$estate = $row['estate'];
		$float_id = $row['float_id'];
		/*
		*  Новостройки
		*/
		if($estate==1){
			$table_name = $template.'_new_flats';
			$photos = 'new_flats';
		}
		/*
		*  Вторичка
		*/
		if($estate==2){
			$table_name = $template.'_second_flats';
			$photos = 'flats';
		}
		/*
		*  Комната
		*/
		if($estate==3){
			$table_name = $template.'_rooms';
			$photos = 'rooms';
		}
		/*
		*  Загородная
		*/
		if($estate==4){
			$table_name = $template.'_countries';
			$photos = 'country';
		}
		/*
		*  Коммерческая
		*/
		if($estate==5){
			$table_name = $template.'_commercials';
			$photos = 'commercial';
		}
		/*
		*  Переуступки
		*/
		if($estate==6){
			$table_name = $template.'_cessions';
			$photos = 'cessions';
		}
		$res = mysql_query("
			SELECT *
			FROM ".$table_name."
			WHERE id='".$float_id."' && user_id='".$_SESSION['idAuto']."'
		");
		if(mysql_num_rows($res)>0){
			$row = mysql_fetch_assoc($res);
			$id = $row['id'];
			$photos = mysql_query("
				SELECT images
				FROM ".$template."_photo_catalogue 
				WHERE p_main='".$id."' && estate='".$photos."' && user_id='".$_SESSION['idAuto']."'
			");
			if(mysql_num_rows($photos)>0){
				$photo  = mysql_fetch_assoc($photos);
				$images = $photo['images'];
				$ex_images = explode(',',$images);
				$del = mysql_query("
					DELETE FROM ".$template."_photo_catalogue 
					WHERE p_main='".$id."' && estate='".$photos."' && user_id='".$_SESSION['idAuto']."'
				");
				if($del=='true'){
					for($i=0; $i<count($ex_images); $i++){
						unlink($_SERVER['DOCUMENT_ROOT']."/users/".$_SESSION['idAuto']."/".$ex_images[$i]);
					}
				}
			}
			$del = mysql_query("
				DELETE FROM ".$table_name."
				WHERE id='".$id."' && user_id='".$_SESSION['idAuto']."'
			");
			if($del=='true'){
				mysql_query("
					DELETE FROM ".$template."_user_ads
					WHERE id='".$id_ad."' && user_id='".$_SESSION['idAuto']."'
				");
				$action = 'success';
				$title = '';
				$text = '';
			}
		}
		
	}
	$data = array(
		"action" => $action,
		"title" => $title,
		"text"  => $text
	);
	echo json_encode($data);
}

// Сохранение данных пользователя
if(isset($_POST['userInfo']) && isset($_POST['s'])){
	$action = 'error';
	$title = 'Ошибка запроса';
	$type = $_POST['userInfo'];
	$redirect = false;
	$text = 'Изменения не сохранены';
	if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')){
		foreach ($_POST['s'] as $name => $value){
			$var = $name."_var";			
			$value = clearValueText($value);
			$$var = $value;
		}
		$id_user = $_SESSION['idAuto'];
		
		if($type=='user_info'){
			if(isset($_POST['p'])){
				foreach($_POST['p'] as $name => $value){
					$var = $name."_var";
					$value = clearValueText($value);
					$$var = $value;
				}
				if($new_pass_var==$repeat_pass_var){
					$res = mysql_query ("
						SELECT id 
						FROM ".$template."_site_users 
						WHERE id='".$id_user."' && password='".md5($old_pass_var)."'
					");
					if(mysql_num_rows($res)>0){
						$upd = mysql_query("
							UPDATE ".$template."_site_users
							SET password='".md5($new_pass_var)."',old_pass='".md5($old_pass_var)."'
							WHERE id='".$id_user."'
						");
						if($upd == 'true') {
							$title = '<div class="successBlock"><p>Пароль изменён</p></div>';
							$text = '';
							$action = 'success';
						}
						else {
							$text = '<div class="errorBlock"><p>К сожалению, произошла ошибка!</p></div>';
							$action = 'error';
							$data = array(
								"action" => $action,
								"type" => $type,
								"title" => $title,
								"text"  => $text
							);
							echo json_encode($data);
							exit;
						}
					}
					else {
						$text = '<div class="errorBlock"><p>Ваш старый пароль не верен</p></div>';
						$data = array(
							"action" => $action,
							"type" => $type,
							"title" => $title,
							"text"  => $text
						);
						echo json_encode($data);
						exit;
					}
				}
				else {
					$text = '<div class="errorBlock"><p>Пароли не совпадают</p></div>';
					$data = array(
						"action" => $action,
						"type" => $type,
						"title" => $title,
						"text"  => $text
					);
					echo json_encode($data);
					exit;
				}
			}
			
			$data = array();
			foreach($_POST['s'] as $name => $value){
				$var = $name."_var";
				$value = clearValueText($value);
				if($name=='region'){
					$name = 'area_id';
				}
				array_push($data,$name."='".$value."'");
				$$var = $value;
			}
			
			/*
			*  Мобильный телефон
			*/
			$insertMobile = '';
			if(isset($_POST['mobile']) && is_array($_POST['mobile'])){
				foreach ($_POST['mobile'] as $n => $v){
					$var = $n."_mob";
					$v = clearValueText($v);
					$$var = $v;
				}
				if(isset($code_mob) && isset($phone_mob)){
					$res = mysql_query("
						SELECT id
						FROM ".$template."_phone_users
						WHERE user_id='".$id_user."' && type_phone='0'
					");
					if(mysql_num_rows($res)>0){
						mysql_query("
							UPDATE ".$template."_phone_users
							SET code_phone='".$code_mob."',number_phone='".$phone_mob."',activation='1'
							WHERE user_id='".$id_user."' && type_phone='0'
						");
					}
					else {
						mysql_query("
							INSERT INTO ".$template."_phone_users (user_id,type_phone,code_phone,number_phone,activation)
							VALUES ('".$id_user."','0','".$code_mob."','".$phone_mob."','1')
						");
					}
				}
			}
			
			/*
			*  Городской телефон
			*/
			if(isset($_POST['city']) && is_array($_POST['city'])){
				foreach ($_POST['city'] as $n => $v){
					$var = $n."_city";
					$v = clearValueText($v);
					$$var = $v;
				}
				if(isset($code_city) && isset($phone_city)){
					$res = mysql_query("
						SELECT id
						FROM ".$template."_phone_users
						WHERE user_id='".$id_user."' && type_phone='1'
					");
					if(mysql_num_rows($res)>0){
						mysql_query("
							UPDATE ".$template."_phone_users
							SET code_phone='".$code_city."',number_phone='".$phone_city."',activation='1'
							WHERE user_id='".$id_user."' && type_phone='1'
						");
					}
					else {
						mysql_query("
							INSERT INTO ".$template."_phone_users (user_id,type_phone,code_phone,number_phone,activation)
							VALUES ('".$id_user."','1','".$code_city."','".$phone_city."','1')
						");
					}
				}
			}

			$delivery = 0;
			$agency_true = 0;
			if(isset($agency_true_var) && !empty($agency_true_var)){
				$agency_true = 1;
			}
			if(isset($_POST['delivery']) && !empty($_POST['delivery'])){
				$delivery = 1;
			}
			$upd = mysql_query("
				UPDATE ".$template."_site_users
				SET ".implode(',',$data).",delivery='".$delivery."',agency_true='".$agency_true."'
				WHERE id='".$id_user."'
			") or die(mysql_error());
			if($upd == 'true') {
				$title = '<div class="successBlock"><p>Изменения сохранены</p></div>';
				$text = '';
				$action = 'success';
			}
			else {
				$text = '<div class="errorBlock"><p>К сожалению, произошла ошибка!</p></div>';
				$action = 'error';
			}
		}
		$data = array(
			"action" => $action,
			"type" => $type,
			"title" => $title,
			"text"  => $text
		);
		// sleep(2);
		echo json_encode($data);
	}
}

// Сохранение правка объявлений пользователя
if(isset($_POST['ads_save']) && (isset($_POST['s']) || isset($_POST['ads']))){
	$action = 'error';
	$title = 'Ошибка запроса';
	$redirect = false;
	$link = '/items';
	$text = 'Изменения не сохранены';
	if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')){
		$id_user = $_SESSION['idAuto'];
		$type = $_POST['typeName'];
		$estate = $_POST['estateName'];
		$time = time();
		
		/*
		*  Новостройки
		*/
		$type_queues = '';
		if($estate==1){
			$table_name = $template.'_new_flats';
			$type_module = 'new_flats';
			$type_queues = 'new_flats';
			$estate_name = 'Новостройки';
			$type_name = 'Продажа';
		}
		/*
		*  Вторичка
		*/
		if($estate==2){
			$table_name = $template.'_second_flats';
			$type_module = 'flats';
			$estate_name = 'Вторичное жилье';
			$type_name = 'Продажа';
			if($type=='rent'){
				$type_name = 'Аренда';
			}
		}
		/*
		*  Комната
		*/
		if($estate==3){
			$table_name = $template.'_rooms';
			$type_module = 'rooms';
			$estate_name = 'Комната';
			$type_name = 'Продажа';
			if($type=='rent'){
				$type_name = 'Аренда';
			}
		}
		/*
		*  Загородная
		*/
		if($estate==4){
			$table_name = $template.'_countries';
			$type_module = 'countries';
			$estate_name = 'Загородная недвижимость';
			$type_name = 'Продажа';
			if($type=='rent'){
				$type_name = 'Аренда';
			}
		}
		/*
		*  Коммерческая
		*/
		if($estate==5){
			$table_name = $template.'_commercials';
			$type_module = 'commercials';
			$estate_name = 'Коммерческая недвижимость';
			$type_name = 'Продажа';
			if($type=='rent'){
				$type_name = 'Аренда';
			}
		}

		/*
		*  Обновление созданного
		*/
		if(isset($_POST['idAd'])){
			$id = intval($_POST['idAd']);
			$res = mysql_query("
				SELECT float_id,estate,type,all_save
				FROM ".$template."_user_ads
				WHERE user_id='".$id_user."' && id='".$id."'
			");
			if(mysql_num_rows($res)>0){
				$row = mysql_fetch_assoc($res);
				$float_id = $row['float_id'];
				$estate = $row['estate'];
				$type = $row['type'];
				$all_save = $row['all_save'];
				$newAdds = false;
			}
			else {
				$data = array(
					"action" => $action,
					"type" => $type,
					"link" => $link,
					"text"  => $text
				);
				echo json_encode($data);
				return false;
			}
		}
		
		/*
		*  Рекламное размещение объявления
		*/
		if(isset($_POST['ads'])){
			foreach($_POST['ads'] as $name => $value){
				if($name=='ads_type'){
					$name = 'special';
				}
				$var = $name."_var";			
				$value = clearValueText($value);
				$$var = $value;	
			}
		}
		$archive = 0;
		if(isset($_POST['save_archive'])){
			$archive = 1;
		}
		
		/*
		*  Создание нового объявления
		*/
		if(isset($_POST['typeName']) && isset($_POST['estateName']) && !isset($_POST['idAd'])){
			mysql_query("
				INSERT INTO ".$template."_user_ads (user_id,type,estate,date,activation,all_save,active,special,archive)
				VALUES ('".$id_user."','".$type."','".$estate."','".$time."',1,1,1,".$special_var.",".$archive.")
			");
			$id = mysql_insert_id();
			
			/*
			*  Вставка информации в таблицу раздела
			*/
			$data = array("user_id='".$id_user."'","type='".$type."'","special='".$special_var."'");
			$res = mysql_query("
				INSERT INTO ".$table_name."
				SET ".implode(',',$data)."
			") or die(mysql_error());
			if($res == 'true'){
				$float_id = mysql_insert_id();
				mysql_query("
					UPDATE ".$template."_user_ads
					SET float_id='".$float_id."', special=".$special_var."
					WHERE id='".$id."' && user_id='".$id_user."'
				") or die(mysql_error());
				$redirect = true;
				$action = 'success';
				$text = 'Изменения сохранены';
				$newAdds = true;
			}
			else {
				$float_id = 0;			
			}
		}		
		
		/*
		*  Регион объекта
		*/
		if(isset($_POST['s']['locality'])){
			$locality = $_POST['s']['locality'];
			$ex_locality = explode('|',$locality);
			$arrayLocality = array();
			echo $_POST['locality'];
			for($a=0; $a<count($ex_locality); $a++){
				$locInfo = str_replace('район','',$ex_locality[$a]);
				$locInfo = trim($locInfo);
				array_push($arrayLocality,"title LIKE '%$locInfo%'");
			}
			if(count($arrayLocality)>0){
				// $likeValue = " && (".implode(' || ',$arrayLocality).")";
				$locs = '';
				$locs2 = $arrayLocality[0];
				$coordsValue0 = false;
				$coordsValue1 = false;
				$coordsValue2 = false;
				if(!empty($locs2)){
					$locs = " && ".$locs2;
					$res = mysql_query("
						SELECT id,coords
						FROM ".$template."_areas
						WHERE city_id=".intval($_POST['s']['location']).$locs."
						LIMIT 1
					");
					if(mysql_num_rows($res)>0){
						$row = mysql_fetch_assoc($res);
						$areaValue = $row['id'];
						if(!empty($row['coords'])){
							$coordsValue0 = $row['coords'];
						}
					}
				}
				
				$locs = '';
				$locs2 = $arrayLocality[1];
				if(!empty($locs2)){
					$locs = " && ".$locs2;
					$res = mysql_query("
						SELECT id,coords
						FROM ".$template."_areas
						WHERE city_id=".intval($_POST['s']['location']).$locs."
						LIMIT 1
					");
					if(mysql_num_rows($res)>0){
						$row = mysql_fetch_assoc($res);
						$areaValue = $row['id'];
						if(!empty($row['coords'])){
							$coordsValue1 = $row['coords'];
							$regionDone = true;
						}
					}
				}
				
				$locs = '';
				$locs2 = $arrayLocality[2];
				if(!empty($locs2)){
					$locs = " && ".$locs2;
					$res = mysql_query("
						SELECT id,coords
						FROM ".$template."_areas
						WHERE city_id=".intval($_POST['s']['location']).$locs."
						LIMIT 1
					");
					if(mysql_num_rows($res)>0){
						$row = mysql_fetch_assoc($res);
						$areaValue = $row['id'];
						if(!empty($row['coords'])){
							$coordsValue2 = $row['coords'];
							$cityDone = true;
						}
					}
				}
				
				
				if(!$regionDone && !$cityDone){
					$coordsValue = $coordsValue0;
				}
				
				if($regionDone && !$cityDone){
					$coordsValue = $coordsValue1;
				}
				
				if(!$regionDone && $cityDone){
					$coordsValue = $coordsValue2;
				}
				
				// $res = mysql_query("
					// SELECT id,coords
					// FROM ".$template."_areas
					// WHERE city_id=".intval($_POST['s']['location']).$likeValue."
					// LIMIT 1
				// ");
				// if(mysql_num_rows($res)>0){
					// $row = mysql_fetch_assoc($res);
					// $areaValue = $row['id'];
					// echo $areaValue;
					// if(!empty($row['coords'])){
						// $coordsValue = $row['coords'];
					// }
				// }
			}
		}
		
		if($id){
			if($newAdds){
				$data = array("date_place=".$time);
				$datas = array("date=".$time);
			}
			else {
				$data = array("date_upd=".$time,"special=".$special_var);
				$datas = array("date_upd=".$time,"special=".$special_var,"archive=".$archive);
			}
			mysql_query("
				UPDATE ".$template."_user_ads
				SET ".implode(',',$datas)."
				WHERE id='".$id."' && user_id='".$id_user."'
			");
			if(isset($_POST['type_country']) && !empty($_POST['type_country'])){
				array_push($data, "type_country=".$_POST['type_country']);
			}
			if(isset($_POST['type_commer']) && !empty($_POST['type_commer'])){
				array_push($data, "type_commer=".$_POST['type_commer']);
			}
			if(isset($_POST['type_business']) && !empty($_POST['type_business'])){
				if($_POST['type_commer']==9){
					array_push($data, "type_business=".$_POST['type_business']);
				}
			}
			// echo '<pre>';
			// print_r($data);
			// echo '</pre>';
			$dataQueue = array();
			$values = array();
			
			/*
			*  Сроки сдачи
			*/
			if(isset($_POST['queues']) && is_array($_POST['queues'])){
				foreach($_POST['queues'] as $name => $value){
					$var = $name."_var";
					$value = clearValueText($value);
					array_push($dataQueue,$name."='".$value."'");
					$$var = $value;
				}
				$r = mysql_query("
					SELECT *
					FROM ".$template."_queues
					WHERE type_name='".$type_queues."' && p_main='".$float_id."'
					ORDER BY id DESC
					LIMIT 1
				");
				array_push($dataQueue,"type_name='".$type_queues."'","p_main='".$float_id."'");
				if(mysql_num_rows($r)>0){
					$rw = mysql_fetch_assoc($r);
					mysql_query("
						UPDATE ".$template."_queues
						SET ".implode(',',$dataQueue)."
						WHERE id='".$rw['id']."'
					");
				}
				else {
					mysql_query("
						INSERT INTO ".$template."_queues
						SET ".implode(',',$dataQueue)."
					");
				}
				
				$ex_deadline = explode(' ',$commissioning_var);
				$ex_deadline = $ex_deadline[count($ex_deadline)-1];
				mysql_query("
					UPDATE ".$table_name."
					SET deadline=".$ex_deadline."
					WHERE id=".$float_id."
				");
			}
			
			/*
			*  Вставка телефонов
			*/
			if(isset($_POST['phones']) && is_array($_POST['phones'])){
				$code_phones = array();
				$numArray = array();
				$number_phones = array();
				foreach($_POST['phones'] as $name => $value){
					foreach($value as $n => $v){
						$var = $name."_phone";
						$v = clearValueText($v);
						if($name=='code'){
							array_push($code_phones,$v);
						}
						else {
							array_push($number_phones,$v);
						}
						array_push($numArray,$n);
					}
				}
				if(count($code_phones)>0 && count($number_phones)>0){
					for($c=0;$c<2;$c++){
						$dataQueue = array();
						$r = mysql_query("
							SELECT *
							FROM ".$template."_phone_ads
							WHERE user_id='".$_SESSION['idAuto']."' && float_id='".$float_id."' && num='".$numArray[$c]."' && type='".$type_module."'
							LIMIT 1
						");
						array_push($dataQueue,"num='".$numArray[$c]."'","number_phone='".$number_phones[$c]."'","code_phone='".$code_phones[$c]."'","user_id='".$_SESSION['idAuto']."'","float_id='".$float_id."'","activation='1'","type='".$type_module."'");
						if(mysql_num_rows($r)>0){
							$rw = mysql_fetch_assoc($r);
							mysql_query("
								UPDATE ".$template."_phone_ads
								SET ".implode(',',$dataQueue)."
								WHERE id='".$rw['id']."'
							");
						}
						else {
							mysql_query("
								INSERT INTO ".$template."_phone_ads
								SET ".implode(',',$dataQueue)."
							");
						}
						// echo implode(',',$dataQueue);
					}
				}
			}
			
			if(isset($_POST['s'])){
				foreach($_POST['s'] as $name => $value){
					$var = $name."_var";
					$value = clearValueText($value);
					if($name=='region'){
						$name = 'area_id';
					}
					if($name=='build'){
						$name = 'p_main';
					}
					if($estate==2 || $estate==3){
						if($name=='finishing'){
							$name = 'repairs';
						}
					}
					if($estate==6){
						if($name=='finishing'){
							$name = 'finishing_value';
						}
						if($name=='build_text'){
							array_push($data,"name_build='".$value."'");
						}
					}
					if($name=='center_coords'){
						$value = trim($value);
						if(!empty($value)){
							$ex_value = str_replace(',',' ',$value);
							$ex_value = explode(' ',$ex_value);
							if(count(explode(',',$value))>1){
								$value = $ex_value[1].' '.$ex_value[0];
							}
							else {
								$value = $ex_value[0].' '.$ex_value[1];
							}
						}
					}
					if($name == 'full_square' 
					|| $name == 'kitchen_square' 
					|| $name == 'dist_value' 
					|| $name == 'ceiling_height' 
					|| $name == 'live_square'){
						$value = str_replace(",", ".", $value);
					}
/* 					if($name=='location_text'){
						$r2 = mysql_query("
							SELECT id
							FROM ".$template."_location
							WHERE activation=1 && name LIKE '%$value%'
							LIMIT 1
						");
						if(mysql_num_rows($r2)>0){
							$d2 = mysql_fetch_assoc($r2);
							array_push($data,"location='".$d2['id']."'"); // Для обновления
						}
					}
 */					if($name=='area_text'){
						$value = str_replace('район','',$value);
						$value = trim($value);
						$r2 = mysql_query("
							SELECT *
							FROM ".$template."_areas
							WHERE activation=1 && title LIKE '%$value%'
							LIMIT 1
						");
						if(mysql_num_rows($r2)>0){
							$d2 = mysql_fetch_assoc($r2);
							if(!$areaValue){
								array_push($data,"area='".$d2['id']."'");
							}
						}
					}
					
					array_push($data,$name."='".$value."'"); // Для обновления
					// array_push($values,"'".$value."'"); // Для вставки
					$$var = $value;
				}
			}

			if(empty($station_var) && empty($center_coords_var)){
				$search = array_search("center_coords=''",$data);
				if($coordsValue && $search!==false){
					$ex_coordsValue = explode(' ',$coordsValue);
					$data[$search] = "center_coords = '".$ex_coordsValue[1]." ".$ex_coordsValue[0]."'";
					// $data[$search] = "center_coords = '".$ex_coordsValue[0]." ".$ex_coordsValue[1]."'";
				}
			}
			
			if(!isset($area_text_var) && isset($station_var)){
				$r2 = mysql_query("
					SELECT area_id
					FROM ".$template."_stations
					WHERE activation=1 && id=".$station_var."
					LIMIT 1
				");
				if(@mysql_num_rows($r2)>0){
					$d2 = mysql_fetch_assoc($r2);
					if(!$areaValue){
						$metroValue = true;
						array_push($data,"area='".$d2['area_id']."'");
					}
				}
			}
			
			if($areaValue && !$metroValue){
				array_push($data,"area=".$areaValue);			
			}

			/*
			*  Обновление информации в таблице раздела
			*/
			// echo implode(',',$data);
			if($float_id){
				$idParam = $float_id;
				if(isset($coords_var) && !empty($coords_var)){
					$cabinetSave = true;
					$table = $table_name;
					$idParam = $float_id;
					include("../admin_2/include/localParams.php");
				}
				if(count($data)>0){
					array_push($data,"type='".$type."'");
					$res = mysql_query("
						SELECT COUNT(*) AS count
						FROM ".$table_name."
						WHERE user_id='".$id_user."' && id='".$float_id."'
					") or die(mysql_error());
					$row = mysql_fetch_assoc($res);
					if($row['count']>0){
						$upd = mysql_query("
							UPDATE ".$table_name."
							SET ".implode(',',$data)."
							WHERE id='".$float_id."' && user_id='".$id_user."'
						") or die(mysql_error());
						if($upd == 'true') {
							$redirect = true;
							$action = 'success';
							$text = 'Изменения сохранены';
						}
					}
				}
			}
			
			/*
			*  Отправляем письмо админам
			*/
			if($newAdds && !isset($_POST['save_archive'])){
				$b_estate = '';
				$b_type = '';
				$b_id = '';
				
				/*
				*  Дата отправки
				*/
				$b_date = '<tr>
					<td style="border:1px solid #999;padding:7px 10px 5px;width:160px">Дата отправки:</td>
					<td style="border:1px solid #999;padding:7px 10px 5px;">'.date_rus(date("Y-m-d")).'</td>
				</tr>';
				
				/*
				*  Время отправки
				*/
				$b_time = '<tr>
					<td style="border:1px solid #999;padding:7px 10px 5px;">Время отправки:</td>
					<td style="border:1px solid #999;padding:7px 10px 5px;">'.date("H:i:s").'</td>
				</tr>';
				
				/*
				*  ID объекта
				*/
				if(isset($float_id) && !empty($float_id)){
					$b_id = '<tr>
						<td style="border:1px solid #999;padding:7px 10px 5px;">ID объекта:</td>
						<td style="border:1px solid #999;padding:7px 10px 5px;">'.$float_id.'</td>
					</tr>';
				}
				
				/*
				*  Раздел
				*/
				if(isset($estate_name) && !empty($estate_name)){
					$b_estate = '<tr>
						<td style="border:1px solid #999;padding:7px 10px 5px;">Раздел:</td>
						<td style="border:1px solid #999;padding:7px 10px 5px;">'.$estate_name.'</td>
					</tr>';
				}
				
				/*
				*  Тип объекта
				*/
				if(isset($type_name) && !empty($type_name)){
					$b_type = '<tr>
						<td style="border:1px solid #999;padding:7px 10px 5px;">Тип объекта:</td>
						<td style="border:1px solid #999;padding:7px 10px 5px;">'.$type_name.'</td>
					</tr>';
				}
				
				/*
				*  ID автора
				*/
				$b_user = '<tr>
					<td style="border:1px solid #999;padding:7px 10px 5px;">ID автора:</td>
					<td style="border:1px solid #999;padding:7px 10px 5px;">'.$_SESSION['idAuto'].'</td>
				</tr>';

				$mailInfo = $b_estate.$b_type.$b_id.$b_user.$b_date.$b_time;
				
				$HTTP_HOST = $_SERVER['HTTP_HOST'];
				$HTTP_HOST2 = $_SERVER['HTTP_HOST'];
				
				if(isset($location_var)){
					$r = mysql_query("
						SELECT *
						FROM ".$template."_location
						WHERE id='".$location_var."' && activation=1
					");
					if(mysql_num_rows($r)>0){
						$rw = mysql_fetch_assoc($r);
						$HTTP_HOST = 'www.rbcom.ru';
						$HTTP_HOST2 = 'rbcom.ru';
						if(!empty($rw['subdomen'])){
							$HTTP_HOST = $rw['subdomen'].'.rbcom.ru';
						}
					}
				}
				
				// Сообщение в виде HTML-формате
				$message = '<style type="text/css">div,p,span,strong,b,em,i,a,li,td{-webkit-text-size-adjust:none;}p,td {color: #8a8a8a;font-size:12px;line-height: 16px;}a {color: #60972f;}</style>
				<table style="font-family:Arial,sans-serif;background:#fbf6ef;" width="100%" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td align="center">
							<table style="margin:50px auto;-moz-border-radius:15px;-webkit-border-radius:15px;-ms-border-radius:15px;-o-border-radius:15px;-khtml-border-radius:15px;border-radius:15px;" width="640" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
								<tr>
									<td colspan="3" align="center">
										<table width="570" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
											<tr>
												<td style="padding-bottom:20px;">
													<p>Здравствуйте!<br> Только что пользователь на сайте добавил новое объявление:<br></p>
													<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
														'.$mailInfo.'
														<tr>
															<td colspan="2"><span style="color:#b8b8b8;font-size:11px;padding-top:15px;display:block">Данное письмо отправленно с официального сайта портала недвижимости '.$HTTP_HOST.'. Во избежании блокировки письма или попадания его в "спам" настоятельно рекомендуем добавить данный электронный адрес <strong>support@'.$HTTP_HOST.'</strong> в адресную книгу Вашей почты. Это снизит до нуля возможность пропустить важное письмо от нашей компании.</span></td>
														</tr>
													</table>
													<table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
														<tr>
															<td>
																<p style="color:#222222;text-align:right">С уважением,<br>команда <a href="http://'.$HTTP_HOST.'">'.$HTTP_HOST.'</a></p>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!--content end-->
							</table>
						</td>
					</tr>
				</table>';
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; Charset=utf-8' . "\r\n";
				$headers .= 'From: '.$HTTP_HOST.' <support@'.$HTTP_HOST2.'>' . "\r\n";
				ini_set("SMTP", "localhost");
				ini_set("smtp_port", "25");
				$email_info = explode(',',$_MAINSET['emails']);
				// $email_info = array('dmitri1988@mail.ru');
				if(count($email_info)>1){
					for($c=0; $c<count($email_info); $c++){
						mail($email_info[$c], $subject, $message, $headers);
						sleep(1);
					}
				}
				else {
					mail($email_info[0], $subject, $message, $headers);
				}

			}
		}
		
		if(isset($_POST['img'])){
			if(is_array($_POST['img'])){
				foreach($_POST['img'] as $key => $img_id){
					mysql_query("
						UPDATE ".$template."_photo_catalogue
						SET p_main=".$float_id."
						WHERE id=".$img_id."
					");
				}
			}
		}
		$text = '';
		$data = array(
			"action" => $action,
			"type" => $type,
			"redirect" => $redirect,
			"link" => $link,
			"text"  => $text
		);
		echo json_encode($data);
	}
}

// Удаление фото
if(isset($_POST['deletePhoto']) && isset($_POST['id'])){
	$action = 'error';
	if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')){
		$id = intval($_POST['id']);
		$idAd = intval($_POST['idAd']);
		$res = mysql_query("
			SELECT float_id
			FROM ".$template."_user_ads
			WHERE id=".$idAd."
		");
		if(mysql_num_rows($res)>0){
			$row = mysql_fetch_assoc($res);
			$idAd = $row['float_id'];
		}
		$res = mysql_query("
			SELECT images,cover,p_main,user_id
			FROM ".$template."_photo_catalogue
			WHERE id='".$id."' && user_id='".$_SESSION['idAuto']."' || id='".$id."' && p_main=".$idAd."
		");
		if(mysql_num_rows($res)>0){
			$row = mysql_fetch_assoc($res);
			$user_id = $row['user_id'];
			$images = $row['images'];
			$del = mysql_query("
				DELETE FROM ".$template."_photo_catalogue 
				WHERE id='".$id."' && p_main=".$idAd."
			");
			if($row['cover']==1){
				$p_main = $row['p_main'];
				$r = mysql_query("
					SELECT id
					FROM ".$template."_photo_catalogue
					WHERE p_main='".$p_main."'
					ORDER BY id LIMIT 1
				",$db);
				if(mysql_num_rows($r)>0){
					$rw = mysql_fetch_assoc($r);
					$id_last = $rw['id'];
					$update = mysql_query ("
						UPDATE ".$template."_photo_catalogue 
						SET cover='1'  
						WHERE id='".$id_last."' && p_main='".$p_main."'
					");
				}
			}
			$ex_images = explode(',',$images);
			for($i=0; $i<count($ex_images); $i++){
				if(empty($user_id)){
					unlink($_SERVER['DOCUMENT_ROOT']."/admin_2/uploads/".$ex_images[$i]);
				}
				else {
					unlink($_SERVER['DOCUMENT_ROOT']."/users/".$_SESSION['idAuto']."/".$ex_images[$i]);
				}
			}

			if($del=='true'){
				$action = 'success';
			}
		}
		$data = array(
			"action" => $action,
		);
		echo json_encode($data);
	}
}

// Подрезка фото портфолио
if(isset($_POST['cutPhotos'])){
	$id = (int)$_POST['cutPhotos'];
	$res = mysql_query("
		SELECT images
		FROM ".$template."_photo_catalogue
		WHERE id='".$id."' && user_id='".$_SESSION['idAuto']."'
	");
	if(mysql_num_rows($res)>0){
		$row = mysql_fetch_assoc($res);
		$images = $row['images'];
	}
	else {
		return false;
		$data = array(
			"action" => "error",
		);
		echo json_encode($data);
	}
	$big_img2 = explode(',',$images);
	$big_img = "/users/".(int)$_SESSION['idAuto']."/gallery/".$big_img2[3];
	$lar_img = "/users/".(int)$_SESSION['idAuto']."/gallery/".$big_img2[2];
	$mid_img = "/users/".(int)$_SESSION['idAuto']."/gallery/".$big_img2[1];
	$min_img = "/users/".(int)$_SESSION['idAuto']."/gallery/".$big_img2[0];
	$width_avatar = 226;
	$height_avatar = 226;
	$height = (int)$_POST['h'];
	$width = (int)$_POST['w'];
	$x1 = (int)$_POST['x1'];
	$x2 = (int)$_POST['x2'];
	$y1 = (int)$_POST['y1'];
	$y2 = (int)$_POST['y2'];
	crop_avatar($_SERVER['DOCUMENT_ROOT'].$big_img,$_SERVER['DOCUMENT_ROOT'].$lar_img, array($x1, $y1, $width, $height, $x2, $y2));
	resize($_SERVER['DOCUMENT_ROOT'].$lar_img,$_SERVER['DOCUMENT_ROOT'].$mid_img, 0, 226,false);
	crop($_SERVER['DOCUMENT_ROOT'].$mid_img,$_SERVER['DOCUMENT_ROOT'].$mid_img, array(0, 0, 226, 226),false,226,$position = 'center',$type_img = 'H');
	resize($_SERVER['DOCUMENT_ROOT'].$mid_img,$_SERVER['DOCUMENT_ROOT'].$min_img, 0, 185,false);
	// if($_POST['type_page']=='groups'){
		// resize("..".$big_img,"..".$big_img, $width_avatar, $height_avatar,false);
	// }
	// else {
		// resize("..".$lar_img,"..".$lar_img, $width_avatar, $height_avatar,false);
	// }
	$data = array(
		"action" => "success",
	);
	$coords = $x1.",".$x2.",".$y1.",".$y2.",".$width.",".$height;
	$update = mysql_query ("
		UPDATE ".$template."_photo_catalogue 
		SET coords='".$coords."'  
		WHERE id='".$id."' && user_id='".$_SESSION['idAuto']."'
	");
	echo json_encode($data);
}

if(isset($_POST['s']) && isset($_POST['guestbook'])){
	$date	 =	 date('Y-m-d');
	$time	 =	 date('H:i:s');
	$data = array("time","date");
	$values = array("'".$time."','".$date."'");
	foreach ($_POST['s'] as $name => $value){
		$var = $name."_var";
		$value = stripslashes($value);
		$value = htmlspecialchars($value);
		$value = trim($value);
		$value = mysql_real_escape_string($value);
		array_push($data,$name);
		array_push($values,"'".$value."'");
		$$var = $value;
	}
	$insert = mysql_query ("
		INSERT INTO 
			".$template."_m_guestbook_left (".implode(',',$data).") 
		VALUES(".implode(',',$values).")
	") or die(mysql_error());
		
	// Тема сообщения
	$subject = "Форма с сайта ".$_SERVER['HTTP_HOST']."";
	// Сообщение в виде HTML-формате
	$message =  "Новый отзыв на сайте! <strong>От пользователя:</strong> ".$name_var."<br>".$time.", ".$date."<br><strong>Сообщение:</strong> ".$msg_var;
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; Charset=utf-8' . "\r\n";
	// Добавляем необходимые заголовки
	$headers .= 'To: '.$email_info.', '. "\r\n";
	$headers .= 'From: '.$_SERVER['HTTP_HOST'].' <support@'.$_SERVER['HTTP_HOST'].'>' . "\r\n";
	ini_set("SMTP", "localhost");
	ini_set("smtp_port", "25");
	mail($email_info, $subject, $message, $headers);

	if($result == 'true') {
		$action = 'ok';
		$title 	= 'Спасибо!';
		$text 	= 'Ваше сообщение успешно отправлено! Оно появится здесь после проверки модератором.';
	}
	
	$data = array(
	  "action" 	=> $action,
	  "title" 	=> $title,
	  "text" 	=> $text
	);
	 
	echo json_encode($data);
}

/*
*  Фильтр районов, городов, метро
*/
if(isset($_POST['filter_local']) && isset($_POST['type']) && isset($_POST['id_local'])){
	$type = $_POST['type'];
	$action = 'error';
	$id_local = (int)$_POST['id_local'];
	$areas_count = 0;
	$stations_count = 0;
	$cities = '';
	$areas = '';
	$stations = '';
	if($type=='city'){
		$res = mysql_query("
			SELECT *, 
			(SELECT COUNT(id) FROM ".$template."_areas WHERE activation='1' && city_id='".$id_local."') AS areas_count 
			FROM ".$template."_areas
			WHERE activation='1' && city_id='".$id_local."'
			ORDER BY title
		");
		if(mysql_num_rows($res)>0){
			$word = array();
			$data = array();
			while($row = mysql_fetch_assoc($res)){
				$areas_count = $row['areas_count'];
				$w = substr(rawurldecode($row['title']),0,2);
				if(!array_search($w,$word)){
					array_push($word,$w);
				}
				$arr = array(
					"id" => $row['id'],
					"title" => $row['title'],
					"word" => $w,
				);
				array_push($data,$arr);
			}
			$column_count = ceil(count($data)/4);
			$n2 = 0;
			$newWord = array();
			$column = array();
			$count = 1;
			for($d=0; $d<count($data); $d++){
				$open_ul = '';
				$close_ul = '';
				if(!in_array($n2,$column)){
					array_push($column,$n2);
					$open_ul = '<ul>';
				}
				$areas .= $open_ul;
				if(!in_array($data[$d]['word'],$newWord)){
					array_push($newWord,$data[$d]['word']);
					$areas .= '<li class="title">'.$data[$d]['word'].'</li>';
				}
				$areas .= '<li><a data-tags="'.$data[$d]['title'].' р-н" data-id="'.$data[$d]['id'].'" href="javascript:void(0)"><span>'.$data[$d]['title'].'</span></a></li>';
				if($count==$column_count){
					if(!in_array($data[$d+1]['word'],$newWord)){
						$close_ul = '</ul>';
						$n2++;
						$count = 1;
					}
				}
				else {
					$count++;
				}
				$areas .= $close_ul;
			}
		}
		$res = mysql_query("
			SELECT *, 
			(SELECT COUNT(id) FROM ".$template."_stations WHERE activation='1' && city_id='".$id_local."') AS stations_count
			FROM ".$template."_stations
			WHERE activation='1' && city_id='".$id_local."'
			ORDER BY title
		");
		if(mysql_num_rows($res)>0){
			$word = array();
			$data = array();
			while($row = mysql_fetch_assoc($res)){
				$stations_count = $row['stations_count'];
				$w = substr(rawurldecode($row['title']),0,2);
				if(!array_search($w,$word)){
					array_push($word,$w);
				}
				$arr = array(
					"id" => $row['id'],
					"title" => $row['title'],
					"word" => $w,
				);
				array_push($data,$arr);
			}
			$column_count = ceil(count($data)/4);
			$n2 = 0;
			$newWord = array();
			$column = array();
			$count = 1;
			for($d=0; $d<count($data); $d++){
				$open_ul = '';
				$close_ul = '';
				if(!in_array($n2,$column)){
					array_push($column,$n2);
					$open_ul = '<ul>';
				}
				$stations .= $open_ul;
				if(!in_array($data[$d]['word'],$newWord)){
					array_push($newWord,$data[$d]['word']);
					$stations .= '<li class="title">'.$data[$d]['word'].'</li>';
				}
				$stations .= '<li><a data-tags="м. '.$data[$d]['title'].'" data-id="'.$data[$d]['id'].'" href="javascript:void(0)"><span>'.$data[$d]['title'].'</span></a></li>';
				if($count==$column_count){
					if(!in_array($data[$d+1]['word'],$newWord)){
						$close_ul = '</ul>';
						$n2++;
						$count = 1;
					}
				}
				else {
					$count++;
				}
				$stations .= $close_ul;
			}
		}
		$action = 'success';
	}
	$data = array(
		"action" => $action,
		"cities" => $cities,
		"areas"  => $areas,
		"areas_count"  => $areas_count,
		"stations"  => $stations,
		"stations_count"  => $stations_count,
	);
	echo json_encode($data);
}

/*
*  Сохранение расстояния от метро до объекта
*/
if(isset($_POST['buildDist']) && isset($_POST['id']) && isset($_POST['parent'])){
	$parent = $_POST['parent'];
	$distance = $_POST['buildDist'];
	$id = (int)$_POST['id'];
	$action = 'error';
	if($parent=='newbuilding'){
		$res = mysql_query("
			SELECT *
			FROM ".$template."_m_catalogue_left
			WHERE activation='1' && id='".$id."'
		");
		if(mysql_num_rows($res)>0){
			$row = mysql_fetch_assoc($res);
			if($row['dist_value']!=$distance){
				mysql_query("
					UPDATE ".$template."_m_catalogue_left 
					SET dist_value='".$distance."'
					WHERE id='".$id."'
				");
			}
		}
		$action = 'success';
	}
	if($parent=='flats'){
		$res = mysql_query("
			SELECT *
			FROM ".$template."_second_flats
			WHERE activation='1' && id='".$id."'
		");
		if(mysql_num_rows($res)>0){
			$row = mysql_fetch_assoc($res);
			if($row['dist_value']!=$distance){
				mysql_query("
					UPDATE ".$template."_second_flats 
					SET dist_value='".$distance."'
					WHERE id='".$id."'
				");
			}
		}
		$action = 'success';
	}
	if($parent=='rooms'){
		$res = mysql_query("
			SELECT *
			FROM ".$template."_rooms
			WHERE activation='1' && id='".$id."'
		");
		if(mysql_num_rows($res)>0){
			$row = mysql_fetch_assoc($res);
			if($row['dist_value']!=$distance){
				mysql_query("
					UPDATE ".$template."_rooms 
					SET dist_value='".$distance."'
					WHERE id='".$id."'
				");
			}
		}
		$action = 'success';
	}
	if($parent=='countries'){
		$res = mysql_query("
			SELECT *
			FROM ".$template."_countries
			WHERE activation='1' && id='".$id."'
		");
		if(mysql_num_rows($res)>0){
			$row = mysql_fetch_assoc($res);
			if($row['dist_value']!=$distance){
				mysql_query("
					UPDATE ".$template."_countries 
					SET dist_value='".$distance."'
					WHERE id='".$id."'
				");
			}
		}
		$action = 'success';
	}
	if($parent=='commercials'){
		$res = mysql_query("
			SELECT *
			FROM ".$template."_commercials
			WHERE activation='1' && id='".$id."'
		");
		if(mysql_num_rows($res)>0){
			$row = mysql_fetch_assoc($res);
			if($row['dist_value']!=$distance){
				mysql_query("
					UPDATE ".$template."_commercials 
					SET dist_value='".$distance."'
					WHERE id='".$id."'
				");
			}
		}
		$action = 'success';
	}
	$data = array(
		"action" => $action,
		"cities" => $cities,
		"areas"  => $areas,
	);
	echo json_encode($data);
}

/*
*  Переработка данных из парсера
*/
if(isset($_POST['houseId'])){
	
	/*
	*  Идентификатор ЖК в системе НМаркет
	*/
	$houseMarketId = (int)$_POST['houseId'];
	
	/*
	*  Идентификатор ЖК в ссылке НМаркет
	*/
	$complexId = (int)$_POST['complexId'];
	
	/*
	*  Параметр действия с данными. Изначально 
	*  подразумевает вставку данных в базу
	*/
	$updateDatas = false;
	
	/*
	*  Проверка на существование ЖК в базе.
	*  Если ID уже присутствует, значит обновляем данные ($updateDatas == true).
	*  Если ID нет в базе, значит записываем новые данные ($updateDatas == false)
	*/
	$res = mysql_query("
		SELECT *, COUNT(*) AS count
		FROM ".$template."_m_catalogue_left
		WHERE nmarket_id='".$houseMarketId."' && parser_lsr='0'
	");
	$row = mysql_fetch_assoc($res);
	$p_main = 0;
	if($row['count']>0){
		$updateDatas = true;
		$p_main = $row['id'];
		
		/*
		*  Массив параметров проверки для
		*  ЖК при обновлении данных
		*/
		$arrayParamStages = array();
		$res = mysql_query("
			SELECT name_param,not_update
			FROM ".$template."_upd_value
			WHERE type='catalogue' && p_main='".$p_main."'
		");
		if(mysql_num_rows($res)>0){
			while($row2 = mysql_fetch_assoc($res)){
				$arrayParamStages[$row2['name_param']] = $row2['not_update'];
			}
		}
		
		/*
		*  Собираем все квартиры данного ЖК в массив,
		*  и те, что не попали в него - будут скрыты
		*/
		$arrayNewFlats = array();
		$newFlats = mysql_query("
			SELECT id 
			FROM ".$template."_new_flats 
			WHERE p_main='".$p_main."'
		");
		if(mysql_num_rows($newFlats)>0){
			while($newFlat = mysql_fetch_assoc($newFlats)){
				array_push($arrayNewFlats,$newFlats['id']);
			}
		}
					
		/*
		*  Если ЖК скрыт, то скрываем все его квартиры
		*/
		if($row['activation']==0){
			mysql_query("
				UPDATE ".$template."_new_flats 
				SET activation='0'
				WHERE p_main='".$p_main."'
			");
			$data = array(
			  "action" 	=> $action,
			  "title" 	=> 'Скрыт',
			  "text" 	=> $text,
			);
			 
			echo json_encode($data);
		}
		else {
			if(is_array($_POST['stages'])){
				$arrayQueues = array();
				$arrayExploitation = array();
				$arrayBuildings = array();
				foreach($_POST['stages'] as $queue => $valArray){// Номер очереди => массив
					$buildings = $valArray['buildings'];
					foreach($buildings as $building => $housingArray){// Номер/буква корпуса => массив
						
						/*
						*  Массив квартир в корпусе
						*/
						$roomsArray = $housingArray['rooms'];
						
						/*
						*  Когда сдаётся
						*/
						$dateEnd = $housingArray['dateEnd'];
						$exploitation = '';
						if($dateEnd != 'Сдан'){
							$ex_dateEnd = explode(' ',$dateEnd);
							$exploitation = $ex_dateEnd[1].'_'.$ex_dateEnd[0];
							array_push($arrayExploitation,$dateEnd);
							array_push($arrayBuildings,$building);
						}
						
						for($r=0; $r<6; $r++){
							if($roomsArray[$r]['totalCount']){
								
								/*
								*  Комнатность квартиры, 0 - студия
								*/
								$rooms = $r;
								
								/*
								*  Общее количество таких квартир
								*/
								$totalCount = $roomsArray[$r]['totalCount'];
								
								/*
								*  Массив типовых квартир
								*/
								$apartments = $roomsArray[$r]['apartments'];
								if(count($apartments)>0){
									$start = microtime(true);
									for($a=0; $a<count($apartments); $a++){
										
										/*
										*  Идентификатор квартиры в системе НМаркет
										*/
										$aid = $apartments[$a]['aid'];
										
										/*
										*  Общая площадь квартиры
										*/
										$sall = $apartments[$a]['sall'];
										
										/*
										*  Площадь кухни
										*/
										$skitchen = $apartments[$a]['skitchen'];
										
										/*
										*  Жилая площадь
										*/
										$sliving = $apartments[$a]['sliving'];
										
										/*
										*  Цена текущей квартиры
										*/
										$priceTotal = $apartments[$a]['priceTotal'];
										$ex_priceTotal = explode('.',$priceTotal);
										$ex_priceTotal = $ex_priceTotal[0];
										
										$flates = mysql_query("
											SELECT id 
											FROM ".$template."_new_flats 
											WHERE aid='".$aid."' && p_main='".$p_main."'
										");
										
										/*
										*  Обновляем
										*/
										$id_flat = 0;
										if(mysql_num_rows($flates)>0){
											$flat = mysql_fetch_assoc($flates);
											$id_flat = $flat['id'];
											
											/*
											*  Массив параметров проверки для
											*  квартиры при обновлении данных
											*/
											$arrayParamStages = array();
											$res = mysql_query("
												SELECT name_param,not_update
												FROM ".$template."_upd_value
												WHERE type='flats' && p_main='".$id_flat."'
											");
											if(mysql_num_rows($res)>0){
												while($row = mysql_fetch_assoc($res)){
													$arrayParamStages[$row['name_param']] = $row['not_update'];
												}
											}
											
											$arrayUpdate = array();
											
											$arrayAllFlatParam = array(
												"building" => $queue,
												"queue" => $building,
												"totalCount" => $totalCount,
												"full_square" => $sall,
												"kitchen_square" => $skitchen,
												"live_square" => $sliving,
												"price" => $ex_priceTotal,
												"rooms" => $rooms,
												"activation" => 1,
											);
											
											foreach($arrayAllFlatParam as $key => $val){
												if($arrayParamStages[$key]==0 || $arrayParamStages[$key]==1){
													if($arrayParamStages[$key]==0){
														array_push($arrayUpdate,$key."='".$val."'");
													}
												}
											}
											if(count($arrayUpdate)>0){
												mysql_query("
													UPDATE ".$template."_new_flats 
													SET ".implode(',',$arrayUpdate)."
													WHERE aid='".$aid."' && p_main='".$p_main."'
												");
											}
										}
										
										/*
										*  Вставляем
										*/
										else {
											$insert = mysql_query("
												INSERT INTO ".$template."_new_flats (p_main,building,queue,picUrl,totalCount,aid,full_square,kitchen_square,live_square,price,rooms,activation)
												VALUE('".$p_main."','".$queue."','".$building."','".$picUrl."','".$totalCount."','".$aid."','".$sall."','".$skitchen."','".$sliving."','".$ex_priceTotal."','".$rooms."','1')
											");
											if($insert=='true'){
												$r = mysql_query("
													SELECT id
													FROM ".$template."_new_flats 
													ORDER BY id DESC
													LIMIT 1
												");
												if(mysql_num_rows($r)>0){
													$rw = mysql_fetch_assoc($r);
													$id_flat = $rw['id'];
													
													/*
													*  Вставляем параметры для нового ЖК в базу,
													*  которые в дальнейшем можно обновлять
													*/
													$arrayValueParam = array();
													for($p=0; $p<count($arrayDatasParamsFlat); $p++){
														$arr = "('flats','".$arrayDatasParamsFlat[$p]."','".$id_flat."','0')";
														array_push($arrayValueParam,$arr);
													}
													
													mysql_query("
														INSERT INTO ".$template."_upd_value (type,name_param,p_main,not_update)
														VALUES ".implode(',',$arrayValueParam)."
													");

													
													/*
													*  Ссылка на картинку
													*/
													$picUrl = $apartments[$a]['picUrl'];
													$ex_img = explode('/',$picUrl);
													$arrayImg = $picUrl;
													$arrayImgCut = $ex_img[count($ex_img)-2];
													$images = mysql_query("
														SELECT id
														FROM ".$template."_photo_catalogue
														WHERE link_market LIKE '%$arrayImgCut%' && p_main='".$id_flat."' && estate='new_flats'
													");
													if(mysql_num_rows($images)==0){
														$pic_weight = 8000;
														$pic_height = 8000;
														$save = true;
														$module = 'new_flats';
														$estate = 'new_flats';
														$id2 = $id_flat;
														$images2 = $arrayImg;
														
														$type = explode('.',$images2);
														$type = $type[count($type)-1];
														$type = 'jpg';
														
														$uploaddir  = $_SERVER['DOCUMENT_ROOT']."/admin_2/uploads/"; // Оригинал
													 
														$apend  = substr(md5(date('YmdHis').rand(100,1000)),0,20).".".$type;
														$apend0 = substr(md5(date('YmdHis').rand(100,1000)),0,20).".".$type;
														$apend1 = substr(md5(date('YmdHis').rand(100,1000)),0,20).".".$type;
														$apend2 = substr(md5(date('YmdHis').rand(100,1000)),0,20).".".$type;
														$apend3 = substr(md5(date('YmdHis').rand(100,1000)),0,20).".".$type;
														$apend4 = substr(md5(date('YmdHis').rand(100,1000)),0,20).".".$type;
														
														$uploadfile = "$uploaddir$apend"; // original
														$bigfile2 = "$uploaddir$apend0";
														$bigfile = "$uploaddir$apend1";
														$largefile = "$uploaddir$apend2";
														$middlefile = "$uploaddir$apend3";
														$minfile = "$uploaddir$apend4";
														
														@file_put_contents($uploadfile, file_get_contents($images2));
														if (file_exists($uploadfile) && ($size = getimagesize($uploadfile))){
															$size = getimagesize($uploadfile);
														
															if ($size[0] < $pic_weight && $size[1] < $pic_height){
																if($size[0] > $size[1]){
																	resize($uploadfile,$bigfile2, $_IMG_MOD[$module]['big_redactor']['x'], 0,false);
																	resize($uploadfile,$bigfile, $_IMG_MOD[$module]['big']['x'], 0,false);
																	
																	resize($bigfile,$largefile, 0, $_IMG_MOD[$module]['lar']['x'],false);
																	crop($largefile,$largefile, array(0, 0, $_IMG_MOD[$module]['lar']['x'], $_IMG_MOD[$module]['lar']['y']),false,$_IMG_MOD[$module]['lar']['x'],$position = 'center',$type_img = 'H');
																	
																	resize($bigfile,$middlefile, 0, $_IMG_MOD[$module]['mid']['x'],false);
																	crop($middlefile,$middlefile, array(0, 0, $_IMG_MOD[$module]['mid']['x'], $_IMG_MOD[$module]['mid']['y']),false,$_IMG_MOD[$module]['mid']['x'],$position = 'center',$type_img = 'H');
																	
																	resize($bigfile,$minfile, 0, $_IMG_MOD[$module]['min']['x'],false);
																	crop($minfile,$minfile, array(0, 0, $_IMG_MOD[$module]['min']['x'], $_IMG_MOD[$module]['min']['y']),false,$_IMG_MOD[$module]['min']['x'],$position = 'center',$type_img = 'H');				
																}
																else {
																	resize($uploadfile,$bigfile2, $_IMG_MOD[$module]['big_redactor']['x'], 0,false);
																	resize($uploadfile,$bigfile, $_IMG_MOD[$module]['big']['x'], 0,false);

																	resize($bigfile,$largefile, $_IMG_MOD[$module]['lar']['x'], 0,false);
																	crop($largefile,$largefile, array(0, 0, $_IMG_MOD[$module]['lar']['x'], $_IMG_MOD[$module]['lar']['y']),false,$_IMG_MOD[$module]['lar']['x'],$position = 'center',$type_img = 'H');
																	
																	resize($bigfile,$middlefile, $_IMG_MOD[$module]['mid']['x'], 0,false);
																	crop($middlefile,$middlefile, array(0, 0, $_IMG_MOD[$module]['mid']['x'], $_IMG_MOD[$module]['mid']['y']),false,$_IMG_MOD[$module]['mid']['x'],$position = 'center',$type_img = 'H');				
																	
																	resize($bigfile,$minfile, $_IMG_MOD[$module]['min']['x'], 0,false);
																	crop($minfile,$minfile, array(0, 0, $_IMG_MOD[$module]['min']['x'], $_IMG_MOD[$module]['min']['y']),false,$_IMG_MOD[$module]['min']['x'],$position = 'center',$type_img = 'H');				
																}
																$action = 'clear';
																$minfile = explode("/",$minfile);
																$middlefile = explode("/",$middlefile);
																$largefile = explode("/",$largefile);
																$bigfile = explode("/",$bigfile);
																$bigfile2 = explode("/",$bigfile2);
																$uploadfile = explode("/",$uploadfile);
																
																$minfile = $minfile[count($minfile)-1];
																$middlefile = $middlefile[count($middlefile)-1];
																$largefile = $largefile[count($largefile)-1];
																$bigfile = $bigfile[count($bigfile)-1];
																$bigfile2 = $bigfile2[count($bigfile2)-1];
																$uploadfile = $uploadfile[count($uploadfile)-1];
																
																$arr2 = $minfile.",".$middlefile.",".$largefile.",".$bigfile.",".$bigfile2.",".$uploadfile;
																$num = 0;

																$coords = $_IMG_MOD[$module]['coords']['x1'].','.$_IMG_MOD[$module]['coords']['y1'].','.$_IMG_MOD[$module]['coords']['x2'].','.$_IMG_MOD[$module]['coords']['y2'].','.$_IMG_MOD[$module]['coords']['width'].','.$_IMG_MOD[$module]['coords']['height'];
																if(!empty($id2)){
																	$rows = mysql_query("
																		SELECT num 
																		FROM ".$template."_photo_catalogue
																		WHERE p_main='".$id2."' && estate='".$estate."'
																		ORDER BY id DESC LIMIT 1
																	") or die(mysql_error());
																	if(mysql_num_rows($rows)>0){
																		$num = mysql_fetch_assoc($rows);
																		$num = $num['num'];
																	}
																}
																$res = mysql_query ("
																	INSERT INTO ".$template."_photo_catalogue (p_main,estate,images,num,date,activation,coords,link_market,cover) 
																	VALUES('".$id2."','".$estate."','".$arr2."','".($num+1)."','".time()."','1','".$coords."','".$arrayImg."','1')
																") or die(mysql_error());
															}
														}
														else {
															unlink($uploadfile);
														}
													}
												}
											}
										}
										$search = array_search($id_flat,$arrayNewFlats);
										if($search!==false){
											array_splice($arrayNewFlats,$search,1);
										}
									}
									$finish = round(microtime(true) - $start,5);
								}
							}
						}
						
						
						// echo '<pre>';
						// print_r($roomsArray);
						// echo '</pre>';
					}
					if(count($arrayExploitation)>0){
						// echo '<pre>';
						// print_r($arrayExploitation);
						// echo '</pre>';
						for($c=0; $c<count($arrayExploitation); $c++){
							$res = mysql_query("
								SELECT id
								FROM ".$template."_queues
								WHERE p_main='".$p_main."' && type_name='new' && queue='".$queue."' && building='".$arrayBuildings[$c]."'
							");
							
							/*
							*  Обновляем
							*/
							if(mysql_num_rows($res)>0){
								$rw = mysql_fetch_assoc($res);
								mysql_query("
									UPDATE ".$template."_queues
									SET commissioning='".$arrayExploitation[$c]."'
									WHERE id='".$rw['id']."'
								");
							}
							
							/*
							*  Вставляем
							*/
							else {
								mysql_query("
									INSERT INTO ".$template."_queues
									SET commissioning='".$arrayExploitation[$c]."', p_main='".$p_main."', type_name='new', queue='".$queue."', building='".$arrayBuildings[$c]."'
								");
							}
						}
					}
				}
			}
			
			$hides = '';				
			if(count($arrayNewFlats)>0){
				$hides = implode(',',$arrayNewFlats);
			}
			
			$data = array(
			  "action" 	=> $action,
			  "title" 	=> $title,
			  "hides" 	=> $hides,
			  "time" 	=> $finish,
			);
			 
			echo json_encode($data);
		}
	}

}

/*
*  Удаление поисковой сессии
*/
if(isset($_POST['clear_filter'])){
	$del = mysql_query("
		DELETE FROM ".$template."_sessions_search 
		WHERE sess_id='".$_COOKIE['PHPSESSID']."'
	");
}

/*
*  Формирование ссылки для перехода с текстового поиска
*/
if(isset($_POST['typeSearch']) && isset($_POST['typeChoose']) && isset($_POST['idInput'])){
	$typeSearch = $_POST['typeSearch'];
	$typeChoose = $_POST['typeChoose'];
	$idInput = clearValueText($_POST['idInput']);
	$linkRequest = '';
	$action = 'error';
	
	$typeSearchArray = array("build","station","area","address","rooms");
	$search = array_search($typeSearch,$typeSearchArray);
	
	/*
	*  Проверка адекватности данных
	*/
	if($search!=-1){
		/*
		*  Определяем с какого раздела 
		*  происходил поисковый запрос
		*/
		if($typeChoose==1){ // Новостройки
			$type = 'type=sell';
			$estate = '&estate=1';
			if($search==0){ // Поиск по ЖК
				$res = mysql_query("
					SELECT *
					FROM ".$template."_m_catalogue_left
					WHERE activation='1' && id='".$idInput."'
				");
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$link = $row['id'];
					if(!empty($row['link'])){
						$link = $row['link'];
					}
					$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/newbuilding/'.$link;
					$action = 'success';
				}
			}
			if($search==1){ // Поиск по метро
				$res = mysql_query("
					SELECT *
					FROM ".$template."_stations
					WHERE activation='1' && id='".$idInput."'
				");
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$link = $row['id'];
					if(!empty($row['link'])){
						$link = $row['link'];
					}
					$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&station='.$link;
					$action = 'success';
				}
			}
			if($search==2){ // Поиск по району
				$res = mysql_query("
					SELECT *
					FROM ".$template."_areas
					WHERE activation='1' && id='".$idInput."'
				");
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$link = $row['id'];
					if(!empty($row['link'])){
						$link = $row['link'];
					}
					$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&area='.$link;
					$action = 'success';
				}
			}
			if($search==3){ // Поиск по адресу
				$res = mysql_query("
					SELECT *
					FROM ".$template."_m_catalogue_left
					WHERE activation='1' && address LIKE '%$idInput%'
				");
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$address = '';
					if(!empty($row['address'])){
						$address = $row['address'];
					}
					$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&address='.$address;
					$action = 'success';
				}
			}
			if($search==4){ // Поиск по комнатам
				$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&rooms='.$idInput;
				$action = 'success';
			}
		}
		if($typeChoose==2){ // Переуступки
			$type = 'type=sell';
			$estate = '&estate=6';
			if($search==0){ // Поиск по ЖК
				$res = mysql_query("
					SELECT *
					FROM ".$template."_cessions
					WHERE activation='1' && id='".$idInput."'
				");
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$link = $row['id'];
					if(!empty($row['link'])){
						$link = $row['link'];
					}
					$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/cession/'.$link;
					$action = 'success';
				}
			}
			if($search==1){ // Поиск по метро
				$res = mysql_query("
					SELECT *
					FROM ".$template."_stations
					WHERE activation='1' && id='".$idInput."'
				");
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$link = $row['id'];
					if(!empty($row['link'])){
						$link = $row['link'];
					}
					$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&station='.$link;
					$action = 'success';
				}
			}
			if($search==2){ // Поиск по району
				$res = mysql_query("
					SELECT *
					FROM ".$template."_areas
					WHERE activation='1' && id='".$idInput."'
				") or die(mysql_error());
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$link = $row['id'];
					if(!empty($row['link'])){
						$link = $row['link'];
					}
					$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&area='.$link;
					$action = 'success';
				}
			}
			if($search==3){ // Поиск по адресу
				$res = mysql_query("
					SELECT *
					FROM ".$template."_cessions
					WHERE activation='1' && address LIKE '%$idInput%'
				");
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$address = '';
					if(!empty($row['address'])){
						$address = $row['address'];
					}
					$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&address='.$address;
					$action = 'success';
				}
			}
			if($search==4){ // Поиск по комнатам
				$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&rooms='.$idInput;
				$action = 'success';
			}
		}
		if($typeChoose==3){ // Квартира (купить)
			$type = 'type=sell';
			$estate = '&estate=2';
			if($search==1){ // Поиск по метро
				$res = mysql_query("
					SELECT *
					FROM ".$template."_stations
					WHERE activation='1' && id='".$idInput."'
				");
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$link = $row['id'];
					if(!empty($row['link'])){
						$link = $row['link'];
					}
					$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&station='.$link;
					$action = 'success';
				}
			}
			if($search==2){ // Поиск по району
				$res = mysql_query("
					SELECT *
					FROM ".$template."_areas
					WHERE activation='1' && id='".$idInput."'
				") or die(mysql_error());
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$link = $row['id'];
					if(!empty($row['link'])){
						$link = $row['link'];
					}
					$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&area='.$link;
					$action = 'success';
				}
			}
			if($search==3){ // Поиск по адресу
				$res = mysql_query("
					SELECT *
					FROM ".$template."_second_flats
					WHERE activation='1' && address LIKE '%$idInput%'
				");
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$address = '';
					if(!empty($row['address'])){
						$address = $row['address'];
					}
					$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&address='.$address;
					$action = 'success';
				}
			}
			if($search==4){ // Поиск по комнатам
				$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&rooms='.$idInput;
				$action = 'success';
			}
		}
		if($typeChoose==4){ // Квартира (снять)
			$type = 'type=rent';
			$estate = '&estate=2';
			if($search==1){ // Поиск по метро
				$res = mysql_query("
					SELECT *
					FROM ".$template."_stations
					WHERE activation='1' && id='".$idInput."'
				");
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$link = $row['id'];
					if(!empty($row['link'])){
						$link = $row['link'];
					}
					$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&station='.$link;
					$action = 'success';
				}
			}
			if($search==2){ // Поиск по району
				$res = mysql_query("
					SELECT *
					FROM ".$template."_areas
					WHERE activation='1' && id='".$idInput."'
				") or die(mysql_error());
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$link = $row['id'];
					if(!empty($row['link'])){
						$link = $row['link'];
					}
					$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&area='.$link;
					$action = 'success';
				}
			}
			if($search==3){ // Поиск по адресу
				$res = mysql_query("
					SELECT *
					FROM ".$template."_second_flats
					WHERE activation='1' && address LIKE '%$idInput%'
				");
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$address = '';
					if(!empty($row['address'])){
						$address = $row['address'];
					}
					$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&address='.$address;
					$action = 'success';
				}
			}
			if($search==4){ // Поиск по комнатам
				$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&rooms='.$idInput;
				$action = 'success';
			}
		}
		if($typeChoose==5){ // Комната (купить)
			$type = 'type=sell';
			$estate = '&estate=3';
			if($search==1){ // Поиск по метро
				$res = mysql_query("
					SELECT *
					FROM ".$template."_stations
					WHERE activation='1' && id='".$idInput."'
				");
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$link = $row['id'];
					if(!empty($row['link'])){
						$link = $row['link'];
					}
					$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&station='.$link;
					$action = 'success';
				}
			}
			if($search==2){ // Поиск по району
				$res = mysql_query("
					SELECT *
					FROM ".$template."_areas
					WHERE activation='1' && id='".$idInput."'
				") or die(mysql_error());
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$link = $row['id'];
					if(!empty($row['link'])){
						$link = $row['link'];
					}
					$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&area='.$link;
					$action = 'success';
				}
			}
			if($search==3){ // Поиск по адресу
				$res = mysql_query("
					SELECT *
					FROM ".$template."_rooms
					WHERE activation='1' && address LIKE '%$idInput%'
				");
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$address = '';
					if(!empty($row['address'])){
						$address = $row['address'];
					}
					$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&address='.$address;
					$action = 'success';
				}
			}
			if($search==4){ // Поиск по комнатам
				$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&rooms='.$idInput;
				$action = 'success';
			}
		}
		if($typeChoose==6){ // Комната (снять)
			$type = 'type=rent';
			$estate = '&estate=3';
			if($search==1){ // Поиск по метро
				$res = mysql_query("
					SELECT *
					FROM ".$template."_stations
					WHERE activation='1' && id='".$idInput."'
				");
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$link = $row['id'];
					if(!empty($row['link'])){
						$link = $row['link'];
					}
					$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&station='.$link;
					$action = 'success';
				}
			}
			if($search==2){ // Поиск по району
				$res = mysql_query("
					SELECT *
					FROM ".$template."_areas
					WHERE activation='1' && id='".$idInput."'
				") or die(mysql_error());
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$link = $row['id'];
					if(!empty($row['link'])){
						$link = $row['link'];
					}
					$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&area='.$link;
					$action = 'success';
				}
			}
			if($search==3){ // Поиск по адресу
				$res = mysql_query("
					SELECT *
					FROM ".$template."_rooms
					WHERE activation='1' && address LIKE '%$idInput%'
				");
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					$address = '';
					if(!empty($row['address'])){
						$address = $row['address'];
					}
					$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&address='.$address;
					$action = 'success';
				}
			}
			if($search==4){ // Поиск по комнатам
				$linkRequest = 'http://'.$_SERVER['HTTP_HOST'].'/search?'.$type.$estate.'&priceAll=all&rooms='.$idInput;
				$action = 'success';
			}
		}
		if($typeChoose==7){ // Коммерческая (купить)
			
		}
		if($typeChoose==8){ // Коммерческая (снять)
			
		}
	}
	$data = array(
	  "action" 	=> $action,
	  "linkRequest" => $linkRequest,
	);
	 
	echo json_encode($data);
}

if(isset($_POST['createPay']) && isset($_POST['cost'])){
	$action = 'error';
	$title = 'Ошибка';
	$linkRedirect = '';
	$text = 'Произошла ошибка! Попробуйте повторить позднее';
	
	if(isset($_SESSION['idAuto']) && $_SESSION['idAuto']){
		$arrName = array();
		$arrValue = array();
		
		mysql_query("
			INSERT INTO payments (user_id,cost,date_pay,success) 
			VALUES('".$_SESSION['idAuto']."','".intval($_POST['cost'])."',0,0)
		");
		$last_id = mysql_insert_id();
		$res = mysql_query("
			SELECT *
			FROM payments
			WHERE id='".$last_id."'
		");
		if(mysql_num_rows($res)>0){
			$row = mysql_fetch_assoc($res);
			$hash = md5($row['id']."|".$row['user_id']."|".$row['cost']);
			$upd = mysql_query("
				UPDATE payments
				SET hash='".$hash."'
				WHERE id='".$row['id']."'
			");
			if($upd==true){								
				$action = "success";
				$title = 'Платежка успешно создана';
				$linkRedirect = 'http://'.$_SERVER['HTTP_HOST'].'/create_payment.php?hash='.$hash;
				$text = '';									
			}
		}
	}
	
	$data = array(
		"action" => $action,
		"title"  => $title,
		"link"   => $linkRedirect,
		"text"   => $text,
	);
	echo json_encode($data);
}

if(isset($_POST['showPhone']) && isset($_POST['type'])){
	$action = 'error';
	$phone = 'Отсутствует';
	$id = intval($_POST['showPhone']);
	
	$addType = '';
	$table = $template.'_new_flats';
	if($_POST['type']=='newbuilding'){
		$table = $template.'_new_flats';
		$addType = " && type='new_flats'";
	}
	if($_POST['type']=='flats'){
		$table = $template.'_second_flats';
	}
	if($_POST['type']=='rooms'){
		$table = $template.'_rooms';
		$addType = " && type='rooms'";
	}
	if($_POST['type']=='countries'){
		$table = $template.'_countries';
	}
	if($_POST['type']=='commercials'){
		$table = $template.'_commercials';
	}

	$ex_phones = array();
	$res = mysql_query("
		SELECT num_phone,user_id,reg_phone
		FROM ".$table."
		WHERE activation=1 && id='".$id."'
	") or die(mysql_error());
	if(mysql_num_rows($res)>0){
		$row = mysql_fetch_assoc($res);
		$reg_phone = $row['reg_phone'];
		$user_id = $row['user_id'];
		if(!empty($row['num_phone'])){
			$phone = $row['num_phone'];
			$action = 'success';
		}
		else {
			if(!empty($reg_phone)){
				$res = mysql_query("
					SELECT * 
					FROM ".$template."_phone_users
					WHERE user_id=".$user_id." && activation=1 && type_phone=0
				");
				if(mysql_num_rows($res)>0){
					$row = mysql_fetch_assoc($res);
					if(!empty($row['code_phone']) && !empty($row['number_phone'])){
						array_push($ex_phones,'+7 '.$row['code_phone'].' '.$row['number_phone']);
					}
					$action = 'success';
				}
			}
			$res = mysql_query("
				SELECT p.*
				FROM ".$template."_phone_ads AS p
				WHERE p.activation=1 && p.float_id=".$id."".$addType."
				ORDER BY p.num
			") or die(mysql_error());
			if(mysql_num_rows($res)>0){
				while($row = mysql_fetch_assoc($res)){
					if(!empty($row['code_phone']) && !empty($row['number_phone'])){
						if(!in_array('+7 '.$row['code_phone'].' '.$row['number_phone'],$ex_phones)){
							array_push($ex_phones,'+7 '.$row['code_phone'].' '.$row['number_phone']);
						}
					}
				}
				$action = 'success';
			}
			$phone = implode('<br>',$ex_phones);
		}
	}
	
	$data = array(
		"action" => $action,
		"phone"  => $phone,
	);
	echo json_encode($data);
}

if(isset($_POST['addFavorite']) && isset($_POST['id']) && isset($_POST['estate']) && isset($_POST['build'])){
	$action = 'error';
	$title = 'Ошибка';
	$text = 'Не удалось добавить в избранное';
	$activation = 0;
	$id = intval($_POST['id']);
	
	$table = $template.'_new_flats';
	$build = $_POST['build'];
	if($_POST['estate']==1){
		$table = $template.'_new_flats';
	}
	if($_POST['estate']==2){
		$table = $template.'_second_flats';
	}
	if($_POST['estate']==3){
		$table = $template.'_rooms';
	}
	if($_POST['estate']==4){
		$table = $template.'_countries';
	}
	if($_POST['estate']==5){
		$table = $template.'_commercials';
	}
	if($_POST['estate']==7){
		$table = $template.'_articles_news';
	}

	$estate = intval($_POST['estate']);
	
	if($build=='true'){
		$table = $template.'_m_catalogue_left';
		$estate = 11;
	}
	$res = mysql_query("
		SELECT COUNT(*) AS count
		FROM ".$table."
		WHERE activation=1 && id='".$id."'
	");
	if(mysql_num_rows($res)>0){
		$row = mysql_fetch_assoc($res);
		if($row['count']>0){
			$idUser = 0;
			if($_SESSION['idAuto']){
				$idUser = $_SESSION['idAuto'];
			}
			$res = mysql_query("
				SELECT *
				FROM ".$template."_favorites
				WHERE user_id=".$idUser." && estate=".$estate." && float_id=".$id."
			");
			if(mysql_num_rows($res)>0){
				$row = mysql_fetch_assoc($res);
				$rid = $row['id'];
				if($row['activation']==0){
					$activation = 1;
					$title = 'В избранном';
					$text = 'Объявление у Вас в избранном';
				}
				else {
					$activation = 0;
					$title = 'Не в избранном';
					$text = 'Объявление убрано из избранных';
				}
				$upd = mysql_query("
					UPDATE ".$template."_favorites
					SET sess_id='".$_COOKIE['PHPSESSID']."', float_id=".$id.", estate=".$estate.", activation=".$activation.", user_id=".$_SESSION['idAuto'].",datetime='".date("Y-m-d H:i:s")."'
					WHERE user_id=".$_SESSION['idAuto']." && id=".$rid."
				");
				if($upd===true){
					$action = 'success';
				}
			}
			else {
				$activation = 1;
				$upd = mysql_query("
					INSERT INTO ".$template."_favorites
					SET sess_id='".$_COOKIE['PHPSESSID']."', float_id=".$id.", estate=".$estate.", activation=".$activation.", user_id=".$_SESSION['idAuto'].", datetime='".date("Y-m-d H:i:s")."'
				");
				if($upd===true){
					$action = 'success';
					$title = 'В избранном';
					$text = 'Объявление у Вас в избранном';
				}
			}
		}
	}
	
	$data = array(
		"action" => $action,
		"user"  => $_SESSION['idAuto'],
		"title"  => $title,
		"text"  => $text,
		"activation"  => $activation,
	);
	echo json_encode($data);
}


?>