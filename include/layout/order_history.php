<h1>Личный кабинет</h1>
		<div class="module tabs">
			<ul>
				<li ><a href="?tab=profile">Информация вашего профиля</a></li>
				<li class="current"><a href="?tab=history">История заказов</a></li>
				<li ><a href="?tab=password">Сменить пароль</a></li>
			</ul>
			<div class="border"></div>
			<div class="border-bg"></div>
		</div>
	<h2>История заказов</h2>
					<div class="module order">
						<div class="left_data">
							<h3>Заказ № 15 от 27.07.12<span class="status s0">не обработан</span></h3>
						</div>
						<div class="right_table">
				
					<table width="100%">
						<col>
						<col width="100">
						<col width="70">
						<col width="100">
						<tr class="first">
							<th>Наименование</td>
							<th>Цена, руб.</td>
							<th>Кол-во</td>
							<th>Сумма, руб.</td>
						</tr>
				<tr><td><span>Солнечный конструктивный синтезатор</span></td><td class="js-price" align="center">1320.00</td><td align="center">3</td><td class="js-summa" align="center">3960.00</td></tr><tr><td><span>Производственный экономический корпус на фоне леса</span></td><td class="js-price" align="center">9537.00</td><td align="center">1</td><td class="js-summa" align="center">9537.00</td></tr><tr><td><span>Усатый принтер</span></td><td class="js-price" align="center">5256.00</td><td align="center">8</td><td class="js-summa" align="center">42048.00</td></tr><tr><td><span>Необходимый самокат на Луне</span></td><td class="js-price" align="center">5848.00</td><td align="center">4</td><td class="js-summa" align="center">23392.00</td></tr><tr><td><span>Сногсшибательный продукт</span></td><td class="js-price" align="center">1650.00</td><td align="center">1</td><td class="js-summa" align="center">1650.00</td></tr><tr><td colspan="3"><strong>Сумма заказа</strong></td><td class="js-summa">80587.00</td></tr><tr><td colspan="3"><strong>Скидка</strong></td><td class="js-summa">8059.00</td></tr><tr><td colspan="3"><strong>Итого с учетом скидки</strong></td><td class="js-summa">72528.00 руб.</td></tr></table>
						</div>
					</div>
				
					<div class="module order">
						<div class="left_data">
							<h3>Заказ № 14 от 27.07.12<span class="status s1">принят</span></h3>
						</div>
						<div class="right_table">
				
					<table width="100%">
						<col>
						<col width="100">
						<col width="70">
						<col width="100">
						<tr class="first">
							<th>Наименование</td>
							<th>Цена, руб.</td>
							<th>Кол-во</td>
							<th>Сумма, руб.</td>
						</tr>
				<tr><td><span>Производственный экономический корпус на фоне леса</span></td><td class="js-price" align="center">9537.00</td><td align="center">5</td><td class="js-summa" align="center">47685.00</td></tr><tr><td colspan="3"><strong>Сумма заказа</strong></td><td class="js-summa">47685.00</td></tr><tr><td colspan="3"><strong>Скидка</strong></td><td class="js-summa">4769.00</td></tr><tr><td colspan="3"><strong>Итого с учетом скидки</strong></td><td class="js-summa">42916.00 руб.</td></tr></table>
						</div>
					</div>
				
					<div class="module order">
						<div class="left_data">
							<h3>Заказ № 13 от 27.07.12<span class="status s2">ожидание оплаты</span></h3>
						</div>
						<div class="right_table">
				
					<table width="100%">
						<col>
						<col width="100">
						<col width="70">
						<col width="100">
						<tr class="first">
							<th>Наименование</td>
							<th>Цена, руб.</td>
							<th>Кол-во</td>
							<th>Сумма, руб.</td>
						</tr>
				<tr><td><span>Производственный экономический корпус на фоне леса</span></td><td class="js-price" align="center">9537.00</td><td align="center">1</td><td class="js-summa" align="center">9537.00</td></tr><tr><td colspan="3"><strong>Сумма заказа</strong></td><td class="js-summa">9537.00</td></tr></table>
						</div>
					</div>
				
					<div class="module order">
						<div class="left_data">
							<h3>Заказ № 12 от 27.07.12<span class="status s3">комплектация</span></h3>
						</div>
						<div class="right_table">
				
					<table width="100%">
						<col>
						<col width="100">
						<col width="70">
						<col width="100">
						<tr class="first">
							<th>Наименование</td>
							<th>Цена, руб.</td>
							<th>Кол-во</td>
							<th>Сумма, руб.</td>
						</tr>
				<tr><td><span>Удобный организационный самолет</span></td><td class="js-price" align="center">648.00</td><td align="center">3</td><td class="js-summa" align="center">1944.00</td></tr><tr><td><span>Сногсшибательный продукт</span></td><td class="js-price" align="center">1650.00</td><td align="center">1</td><td class="js-summa" align="center">1650.00</td></tr><tr><td colspan="3"><strong>Сумма заказа</strong></td><td class="js-summa">3594.00</td></tr></table>
						</div>
					</div>
				
					<div class="module order">
						<div class="left_data">
							<h3>Заказ № 11 от 27.07.12<span class="status s4">отгрузка</span></h3>
						</div>
						<div class="right_table">
				
					<table width="100%">
						<col>
						<col width="100">
						<col width="70">
						<col width="100">
						<tr class="first">
							<th>Наименование</td>
							<th>Цена, руб.</td>
							<th>Кол-во</td>
							<th>Сумма, руб.</td>
						</tr>
				<tr><td><span>Отдельный работник</span></td><td class="js-price" align="center">4664.00</td><td align="center">15</td><td class="js-summa" align="center">69960.00</td></tr><tr><td colspan="3"><strong>Сумма заказа</strong></td><td class="js-summa">69960.00</td></tr><tr><td colspan="3"><strong>Скидка</strong></td><td class="js-summa">6996.00</td></tr><tr><td colspan="3"><strong>Итого с учетом скидки</strong></td><td class="js-summa">62964.00 руб.</td></tr></table>
						</div>
					</div>
				
					<div class="module order">
						<div class="left_data">
							<h3>Заказ № 10 от 27.07.12<span class="status s5">выполнен</span></h3>
						</div>
						<div class="right_table">
				
					<table width="100%">
						<col>
						<col width="100">
						<col width="70">
						<col width="100">
						<tr class="first">
							<th>Наименование</td>
							<th>Цена, руб.</td>
							<th>Кол-во</td>
							<th>Сумма, руб.</td>
						</tr>
				<tr><td><span>Усатый принтер</span></td><td class="js-price" align="center">5256.00</td><td align="center">1</td><td class="js-summa" align="center">5256.00</td></tr><tr><td><span>Солнечный край</span></td><td class="js-price" align="center">6226.00</td><td align="center">2</td><td class="js-summa" align="center">12452.00</td></tr><tr><td colspan="3"><strong>Сумма заказа</strong></td><td class="js-summa">17708.00</td></tr><tr><td colspan="3"><strong>Скидка</strong></td><td class="js-summa">1771.00</td></tr><tr><td colspan="3"><strong>Итого с учетом скидки</strong></td><td class="js-summa">15937.00 руб.</td></tr></table>
						</div>
					</div>
				
					<div class="module order">
						<div class="left_data">
							<h3>Заказ № 9 от 27.07.12<span class="status s6">отменен</span></h3>
						</div>
						<div class="right_table">
				
					<table width="100%">
						<col>
						<col width="100">
						<col width="70">
						<col width="100">
						<tr class="first">
							<th>Наименование</td>
							<th>Цена, руб.</td>
							<th>Кол-во</td>
							<th>Сумма, руб.</td>
						</tr>
				<tr><td><span>Солнечный конструктивный синтезатор</span></td><td class="js-price" align="center">1320.00</td><td align="center">2</td><td class="js-summa" align="center">2640.00</td></tr><tr><td colspan="3"><strong>Сумма заказа</strong></td><td class="js-summa">2640.00</td></tr></table>
						</div>
					</div>
							
			</div>