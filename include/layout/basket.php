<h1>Корзина</h1>
			<div class="module basket">
			<form class="mod-basket" method="post" action="/cart.php">
			<input type="hidden" name="cart_recalculate" value="1">
			<table class="cart" width="100%">
			<col>
			<col width="100">
			<col width="70">
			<col width="100">
			<col width="70">
			<tr>
				<th>Наименование</th>
				<th>Цена, руб.</th>
				<th>Кол-во</th>
				<th>Сумма, руб.</th>
				<th></th>
			</tr>
		
				<tr>
					<td><a href="/?page=10&amp;node=5&amp;item=82">Фантастический бурундук в сокращенной версии, работающий уже много лет</a></td>
					<td class="js-price">2 480,00</td>
					<td class="pt"><input type="text" class="mod-basket-number" value="1" name="item[82]"></td>
					<td class="js-summa">2 480,00</td>
					<td class="del"><a href="?cart_delete=82">Удалить</a></td>
				</tr>
			
				<tr>
					<td><a href="/?page=10&amp;node=5&amp;item=83">Солнечный конструктивный синтезатор</a></td>
					<td class="js-price">1 320,00</td>
					<td class="pt"><input type="text" class="mod-basket-number" value="1" name="item[83]"></td>
					<td class="js-summa">1 320,00</td>
					<td class="del"><a href="?cart_delete=83">Удалить</a></td>
				</tr>
			
				<tr>
					<td><a href="/?page=10&amp;node=5&amp;item=84">Производственный экономический корпус на фоне леса</a></td>
					<td class="js-price">9 537,00</td>
					<td class="pt"><input type="text" class="mod-basket-number" value="1" name="item[84]"></td>
					<td class="js-summa">9 537,00</td>
					<td class="del"><a href="?cart_delete=84">Удалить</a></td>
				</tr>
			
				<tr>
					<td><a href="/?page=10&amp;node=5&amp;item=85">Усатый принтер</a></td>
					<td class="js-price">5 256,00</td>
					<td class="pt"><input type="text" class="mod-basket-number" value="1" name="item[85]"></td>
					<td class="js-summa">5 256,00</td>
					<td class="del"><a href="?cart_delete=85">Удалить</a></td>
				</tr>
			
				<tr>
					<td><a href="/?page=10&amp;node=5&amp;item=86">Солнечный край</a></td>
					<td class="js-price">6 226,00</td>
					<td class="pt"><input type="text" class="mod-basket-number" value="1" name="item[86]"></td>
					<td class="js-summa">6 226,00</td>
					<td class="del"><a href="?cart_delete=86">Удалить</a></td>
				</tr>
			
			<tr class="total">
				<td colspan="2"><strong>Итого по заказу</strong></td>
				<td align="center">5</td>
				<td class="js-summa">24 819,00</td>
				<td>&nbsp;</td>
			</tr>
		
			</table>
			<p class="before"><span><a href="?cart_clear">Очистить корзину</a></span><input type="submit" value="Пересчитать"></p>
			</form>
		</div>
		<h2>Вы можете снабдить ваш заказ комментарием</h2><div class="module form "><a name="form"></a><form method="post" action="/cart.php" enctype="multipart/form-data"><input name="form_cart_registered" type="hidden" value="1"><input name="redirect" type="hidden" value="/cart.php"><input name="id" type="hidden" value="0"><input type="hidden" name="cart_save" value="1"><table style="width:100%;">
							<tr class="">
								<td class="label">Комментарий к заказу</td>
								<td class=""></td>
								<td class="input">
									<textarea class="" rows="5" cols="10" name="r_text"></textarea>
									
								</td>
							</tr>
						
				<tr>
					<td class="label"></td>
					<td></td>
					<td class="input">
						<input type="submit" style="width:120px" value="Отправить заказ" class="button">
					</td>
				</tr>
			</table></form></div>
