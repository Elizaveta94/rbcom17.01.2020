<div class="module form ">
	<a name="form"></a>
	<div class="module msg_error">
		<div>
			<h2>Форма была заполнена с ошибками</h2>
			<p>Некоторые из полей формы не заполнены или заполнены неправильно.<br>Проверьте данные и повторите отправку формы.</p>
		</div>
	</div>
	<p><strong>Обратите внимание!<br>Поля, отмеченные звёздочкой (*), обязательны для заполнения.</strong></p>
		<form enctype="multipart/form-data" action="/" method="post">
			<input type="hidden" value="1" name="form_demo">
			<input type="hidden" value="/?page=16" name="redirect">
			<input type="hidden" value="0" name="id">
				<table style="width:100%;">
					<tr>
						<td colspan="3"><div style="width:100%;" class="title">Заголовок группы полей</div></td>
					</tr>
					<tr class="">
						<td class="label">Контактное лицо</td>
						<td class="required"></td>
						<td class="input">
							<input type="text" name="r_name" value="111" style="width:260px;" class="">
							<label class="comment">Комментарий</label>
						</td>
					</tr>
					
					<tr class="">
						<td class="label">Комментарий</td>
						<td class="required"></td>
						<td class="input">
							<textarea name="r_text" cols="10" rows="5" class="">222</textarea>
							<label class="comment">Комментарий</label>
						</td>
					</tr>
					
					<tr>
						<td colspan="3"><div style="width:100%;" class="title">Заголовок группы полей</div></td>
					</tr>
					
					<tr class="">
						<td class="label">Пароль</td>
						<td class="required"></td>
						<td class="input">
							<input type="password" name="r_password" value="12345" style="width:260px;" class="">
							<label class="comment">Комментарий</label>
						</td>
					</tr>
					
					<tr class="">
						<td class="label">Дата</td>
						<td class="required"></td>
						<td class="input">
							<input type="text" class="datepicker " style="width:74px;" value="01.01.1970" name="r_date">
							<label class="comment">Комментарий</label>
						</td>
					</tr>
					
					<tr class="">
						<td class="label">Город</td>
						<td class="required"></td>
						<td class="input">
							<select name="r_select" style="width:260px;" class="">
								<option value="">&nbsp;</option>
								<option value="1">Месторождение каменного угля</option>
								<option selected="" value="2">Основная магистраль</option>
								<option value="3">Бамбуковый медведь</option>
								<option value="4">Бесплатный официальный язык</option>
								<option value="5">Центральная площадь</option>
							</select>
							<label class="comment">Комментарий</label>
						</td>
					</tr>
						
					<tr class="">
						<td class="label">Файл</td>
						<td class=""></td>
						<td class="input">
							<div class="relativ">
								<div class="name_file"></div>
								<div class="send_file">
									<div class="button_f">
										<div>Обзор</div>
										<input type="file" size="1" name="f" class="file_type file">
									</div>
								</div>
							</div>
							
						</td>
					</tr>
						
					<tr>
						<td class="label"></td>
						<td></td>
						<td class="input"><img src="/libs/kcaptcha/index.php?future=l56es8imsbhhnfhqd86c80vv15" alt=""></td>
					</tr>
					<tr class="">
						<td class="label">Код с картинки</td>
						<td class="required"></td>
						<td class="input">
							<input type="text" name="captcha" style="width:120px;" class="">
							<label class="comment">Цифры и латинские буквы, без учета регистра</label>
						</td>
					</tr>
					
					<tr>
						<td class="label"></td>
						<td></td>
						<td class="input"><input name="" type="checkbox" value="1" id="check_1"><label for="check_1"><span><label class="comment">Телефон</label></span></label><br></td>
					</tr>
						
					<tr>
						<td class="label"></td>
						<td></td>
						<td class="input">
							<input type="submit" class="button" value="Отправить" style="width:120px">
						</td>
					</tr>
				</table>
		</form>
</div>