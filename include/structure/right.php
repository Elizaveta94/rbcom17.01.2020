	<!-- Голосование
	<div class="module vote">
		<div class="inner">
			<h3>Голосование</h3>
			<h4>Оранжевый предельный капитан из улучшенного материала, ставший известным во всем мире — что это?</h4>

			<div class="item">
				Название
					<div style="width:'.(0.8*(int)$per*$multiplier).'%" class="bar"></div>
					<span>50%</span>
			</div>
			<br>
			<div>Всего голосов: </div>
	

			<form action="" method="post">
				<input type="hidden" name="__form_vote" value="1">
				<input type="hidden" name="id" value="">
				<input type="radio" value="" name="v" id="v2"><label for="v1">Название</label><br>
				<input type="submit" value="Голосовать">
			</form>
		</div>
	</div> -->

	<div class="module sideform">
		<div class="inner">
			<h3>Обратная связь</h3>
			<form method="post" action="" onsubmit="return checkSideForm(this)">
			<input type="hidden" name="__form_side" value="1">
				<table class="feedback">
					<tr>
						<td class="label"><label>ФИО <b>*</b></label><br>
						<input type="text" name="r_name"></td>
					</tr>
					<tr>
						<td class="label"><label>Организация</label><br>
						<input type="text" name="r_firm"></td>
					</tr>
					<tr>
						<td class="label"><label>E-mail</label><br>
						<input type="text" name="r_email"></td>
					</tr>
					<tr>
						<td class="label"><label>Телефон <b>*</b></label><br>
						<input type="text" name="r_phone"></td>
					</tr>
					<tr>
						<td class="label"><label>Текст <b>*</b></label><br>
						<textarea name="r_text"></textarea></td>
					</tr>
				</table>
				<span class="little">* Поля для обязательного заполнения</span>
				<input type="submit" value="Отправить">
			</form>
		</div>
	</div>

	<!---->
	<div class="module discount">
		<div class="inner">
			<h3>Спецпредложения</h3>
			<div class="item">
				<p class="img"><a href="#"><img src="/images/disc1.jpg" alt=""></a></p>
				<p><a href="#">Краткое изложение новости, возможно первое предложение текста...</a></p>
			</div>
			<div class="item">
				<p class="img"><a href="#"><img src="/images/disc2.jpg" alt=""></a></p>
				<p><a href="#">Краткое изложение новости, возможно первое предложение текста...</a></p>
			</div>
			<div class="item">
				<p class="img"><a href="#"><img src="/images/disc1.jpg" alt=""></a></p>
				<p><a href="#">Краткое изложение новости, возможно первое предложение текста...</a></p>
			</div>
			<a href="#" class="all">Все спецпредложения</a>
		</div>
	</div>
	