<?
	$client_version = 1; 
?>
<link type="text/css" rel="stylesheet" href="/css/style.css">
<?if(count($params)==4 && $params[0]!='search' || (count($params)==2 && $params[0]=='flats') || (count($params)==2 && $params[0]=='newbuilding') || (count($params)==2 && $params[0]=='commercials') || (count($params)==2 && $params[0]=='countries') || (count($params)==2 && $params[0]=='rooms') || $_SERVER['REQUEST_URI']=='/card_new.php'){?>
<script src="https://code.jquery.com/jquery-1.8.3.min.js"></script>
<?}else{?>
<script type="text/javascript" src="/libs/jquery-1.7.1.min.js"></script>	
<?}?>
<!--<script type="text/javascript">p_main=<?=$p_main?></script>-->

<script type="text/javascript" src="/js/f.js?ver=<?=$client_version;?>"></script>
<script type="text/javascript" src="/js/init.js?ver=<?=$client_version;?>"></script>
<link type="text/css" rel="stylesheet" href="/libs/prettyPhoto/css/prettyPhoto.css">
<link rel="stylesheet" type="text/css" href="/libs/datepicer_redmond/jquery-ui-1.8.7.custom.css">
<link type="text/css" href="/libs/peppermint/css/peppermint.required.css" rel="stylesheet" />
<link type="text/css" href="/libs/peppermint/css/peppermint.suggested.css" rel="stylesheet" />
<link type="text/css" rel="stylesheet" href="/css/font-awesome.min.css" />
<link href="/libs/confirm/css/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/libs/fotorama/css/fotorama.css" rel="stylesheet" type="text/css">
<link href="/libs/fotorama/css/fotorama-skin.css" rel="stylesheet" type="text/css">
<!--<link href="/libs/jquery.imgareaselect-0.9.10/css/imgareaselect-animated.css" rel="stylesheet" type="text/css">
<link href="/libs/jquery.imgareaselect-0.9.10/css/imgareaselect-default.css" rel="stylesheet" type="text/css">
<link href="/libs/jquery.imgareaselect-0.9.10/css/imgareaselect-deprecated.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/libs/jquery.imgareaselect-0.9.10/js/jquery.imgareaselect.pack.js" ></script>-->
<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/x-icon" href="/images/favicon.png">
<!--<script type="text/javascript" src="/libs/jquery.price_format.js"></script>-->
<script type="text/javascript" src="/js/SearchParam.php"></script>
<?if(!isset($arrayLinkSearch['page']) || isset($arrayLinkSearch['page']) && $arrayLinkSearch['page']==1){?>
<link rel="canonical" href="https://<?=$_SERVER['HTTP_HOST'].'/'.$_SERVER['QUERY_STRING']?>" />
<?
}
	unset($client_version);
?>