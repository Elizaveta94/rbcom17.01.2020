<?
$_max_value_room = 50; // максимальная площадь комнаты
$_max_value_square_commer = 80; // максимальная площадь коммерческого помещения
$_max_value_country = 250; // максимальная площадь загородной недвижимости
$_max_value_country_land = 20; // максимальная площадь участка в сотках
$_max_value_rooms_commer = 5; // максимальное кол-во комнат коммерческого помещения
$_min_value_square_full = 1; // максимальная общая площадь
$_max_value_square_full = 150; // максимальная общая площадь
$_max_value_square_live = 60; // максимальная жилая площадь
$_max_value_delivery = 2019; // максимальный год сдачи квартир
$_min_value_square_kitchen = 8; // максимальная площадь кухни
$_max_value_square_kitchen = 30; // максимальная площадь кухни
$_max_value_floor = 30; // максимальное количество этажей в доме
$_max_value_floor_commer = 30; // максимальное количество этажей в здании ком.помещения

/*
*  Цены на типы недвижимостей
*/

/*
*  Новостройки
*/
$start = microtime(true);
$res = mysql_query("
	SELECT 
		MIN(price/full_square) AS minPrice,
		MAX(price/full_square) AS maxPrice,
		MIN(price) AS min_price, 
		MAX(price) AS max_price, 
		MIN(full_square) AS min_full_square, 
		MAX(full_square) AS max_full_square,
		MIN(kitchen_square) AS min_kitchen_square, 
		MAX(kitchen_square) AS max_kitchen_square,
		(SELECT MIN(floors) FROM ".$template."_m_catalogue_left WHERE activation='1'".$paramLocation.") AS min_floors, 
		(SELECT MAX(floors) FROM ".$template."_m_catalogue_left WHERE activation='1'".$paramLocation.") AS max_floors
	FROM ".$template."_new_flats
	WHERE activation='1'".$realUserParam.$paramLocation."
") or die(mysql_error());
if(mysql_num_rows($res)>0){//_count_params
	$row = mysql_fetch_assoc($res);
	$min_price_new_sell = $row['min_price'];
	$max_price_new_sell = $row['max_price'];
	$min_full_square_new_sell = $row['min_full_square'];
	$max_full_square_new_sell = $row['max_full_square'];
	$min_kitchen_square_new_sell = $row['min_kitchen_square'];
	$max_kitchen_square_new_sell = $row['max_kitchen_square'];
	$min_floors_new_sell = $row['min_floors'];
	$max_floors_new_sell = $row['max_floors'];
	
	$min_full_square_new_sell = floor($min_full_square_new_sell);
	$max_full_square_new_sell = ceil($max_full_square_new_sell);
	
	if($min_full_square_new_sell==0){
		$min_full_square_new_sell = 1;
	}
	
	if($max_full_square_new_sell==0){
		$max_full_square_new_sell = 1;
	}
	$min_meter_price_new_sell = floor($row['minPrice']);
	$max_meter_price_new_sell = ceil($row['maxPrice']);
	
	$min_meter_price_new_sell = floor($min_meter_price_new_sell/1000);
	$max_meter_price_new_sell = ceil($max_meter_price_new_sell/1000);

	if($min_price_new_sell<1000000){
		$min_price_new_sell = floor($min_price_new_sell/100000);
		$min_price_new_sell = $min_price_new_sell/10;
	}
	else {
		$min_price_new_sell = floor($min_price_new_sell/1000000);
	}
	$max_price_new_sell = ceil($max_price_new_sell/1000000);
	
	$_min_value_square_full = $min_full_square_new_sell;
	// $_min_value_square_full = 1;
	$_max_value_square_full = $max_full_square_new_sell;
	
	$min_kitchen_square_new_sell = floor($min_kitchen_square_new_sell);
	// $min_kitchen_square_new_sell = 1;
	$max_kitchen_square_new_sell = ceil($max_kitchen_square_new_sell);
}

/*
*  Переуступки
*/
$start2 = microtime(true);
$res = mysql_query("
	SELECT 
		MIN(price) AS min_price, 
		MAX(price) AS max_price, 
		MIN(full_square) AS min_full_square, 
		MAX(full_square) AS max_full_square,
		MIN(kitchen_square) AS min_kitchen_square, 
		MAX(kitchen_square) AS max_kitchen_square,
		(SELECT MIN(floors) FROM ".$template."_m_catalogue_left WHERE activation='1'".$paramLocation.") AS min_floors, 
		(SELECT MAX(floors) FROM ".$template."_m_catalogue_left WHERE activation='1'".$paramLocation.") AS max_floors
	FROM ".$template."_cessions
	WHERE activation='1' && price!='0' && how_buy!='Бронь'".$realUserParam.$paramLocation."
");
if(mysql_num_rows($res)>0){//_count_params
	$row = mysql_fetch_assoc($res);
	$min_price_cession_sell = $row['min_price'];
	$max_price_cession_sell = $row['max_price'];
	$min_full_square_cession_sell = $row['min_full_square'];
	$max_full_square_cession_sell = $row['max_full_square'];
	$min_kitchen_square_cession_sell = $row['min_kitchen_square'];
	$max_kitchen_square_cession_sell = $row['max_kitchen_square'];
	$min_floors_cession_sell = $row['min_floors'];
	$max_floors_cession_sell = $row['max_floors'];
	
	$min_full_square_cession_sell = floor($min_full_square_cession_sell);
	$max_full_square_cession_sell = ceil($max_full_square_cession_sell);
	
	if($min_full_square_cession_sell==0){
		$min_full_square_cession_sell = 1;
	}
	
	if($max_full_square_cession_sell==0){
		$max_full_square_cession_sell = 1;
	}
	$min_meter_price_cession_sell = floor($min_price_cession_sell/$min_full_square_cession_sell);
	$max_meter_price_cession_sell = ceil($max_price_cession_sell/$max_full_square_cession_sell);
	
	$min_meter_price_cession_sell = floor($min_meter_price_cession_sell/1000);
	$max_meter_price_cession_sell = ceil($max_meter_price_cession_sell/1000);

	$min_price_cession_sell = floor($min_price_cession_sell/1000000);
	$max_price_cession_sell = ceil($max_price_cession_sell/1000000);
	
	$_min_value_square_full_cession = $min_full_square_cession_sell;
	// $_min_value_square_full_cession = 1;
	$_max_value_square_full_cession = $max_full_square_cession_sell;
	
	$min_kitchen_square_cession_sell = floor($min_kitchen_square_cession_sell);
	// $min_kitchen_square_cession_sell = 1;
	$max_kitchen_square_cession_sell = ceil($max_kitchen_square_cession_sell);
}

/*
*  Квартиры
*/
$res = mysql_query("
	SELECT 
		MIN(price) AS min_price, 
		MAX(price) AS max_price, 
		MIN(full_square) AS min_full_square, 
		MAX(full_square) AS max_full_square,
		MIN(price/full_square) AS min_full_square_meter, 
		MAX(price/full_square) AS max_full_square_meter,
		MIN(kitchen_square) AS min_kitchen_square, 
		MAX(kitchen_square) AS max_kitchen_square,
		MIN(live_square) AS min_live_square, 
		MAX(live_square) AS max_live_square,
		MIN(floors) AS min_floors, 
		MAX(floors) AS max_floors
	FROM ".$template."_second_flats
	WHERE activation='1' && type='sell'".$realUserParam.$paramLocation."
");
if(mysql_num_rows($res)>0){//_count_params
	$row = mysql_fetch_assoc($res);
	$min_price_flat_sell = $row['min_price'];
	$max_price_flat_sell = $row['max_price'];
	$min_full_square_flat_sell = $row['min_full_square'];
	$max_full_square_flat_sell = $row['max_full_square'];
	$min_full_square_meter = $row['min_full_square_meter'];
	$max_full_square_meter = $row['max_full_square_meter'];
	$min_kitchen_square_flat_sell = $row['min_kitchen_square'];
	$max_kitchen_square_flat_sell = $row['max_kitchen_square'];
	$min_live_square_flat_sell = $row['min_live_square'];
	$max_live_square_flat_sell = $row['max_live_square'];
	$min_floors_flat_sell = $row['min_floors'];
	$max_floors_flat_sell = $row['max_floors'];

	$min_meter_price_flat_sell = floor($min_full_square_meter);
	$max_meter_price_flat_sell = ceil($max_full_square_meter);
	
	$min_meter_price_flat_sell = floor($min_meter_price_flat_sell/1000);
	$max_meter_price_flat_sell = ceil($max_meter_price_flat_sell/1000);
	
	$min_price_flat_sell = floor($min_price_flat_sell/1000000);
	$max_price_flat_sell = ceil($max_price_flat_sell/1000000);
	
	$min_full_square_flat_sell = floor($min_full_square_flat_sell);
	// $min_full_square_flat_sell = 1;
	$max_full_square_flat_sell = ceil($max_full_square_flat_sell);

	$min_kitchen_square_flat_sell = floor($min_kitchen_square_flat_sell);
	// $min_kitchen_square_flat_sell = 1;
	$max_kitchen_square_flat_sell = ceil($max_kitchen_square_flat_sell);

	$min_live_square_flat_sell = floor($min_live_square_flat_sell);
	// $min_live_square_flat_sell = 1;
	$max_live_square_flat_sell = ceil($max_live_square_flat_sell);
}
$res = mysql_query("
	SELECT 
		MIN(price) AS min_price, 
		MAX(price) AS max_price, 
		MIN(full_square) AS min_full_square, 
		MAX(full_square) AS max_full_square,
		MIN(kitchen_square) AS min_kitchen_square, 
		MAX(kitchen_square) AS max_kitchen_square,
		MIN(live_square) AS min_live_square, 
		MAX(live_square) AS max_live_square,
		MIN(floors) AS min_floors, 
		MAX(floors) AS max_floors
	FROM ".$template."_second_flats
	WHERE activation='1' && type='rent'".$realUserParam.$paramLocation."
");
if(mysql_num_rows($res)>0){//_count_params
	$row = mysql_fetch_assoc($res);
	$min_price_flat_rent = $row['min_price'];
	$max_price_flat_rent = $row['max_price'];

	$min_full_square_flat_rent = floor($row['min_full_square']);
	// $min_full_square_flat_rent = 1;
	$max_full_square_flat_rent = ceil($row['max_full_square']);

	$min_kitchen_square_flat_rent = floor($row['min_kitchen_square']);
	// $min_kitchen_square_flat_rent = 1;
	$max_kitchen_square_flat_rent = ceil($row['max_kitchen_square']);

	$min_live_square_flat_rent = floor($row['min_live_square']);
	// $min_live_square_flat_rent = 1;
	$max_live_square_flat_rent = ceil($row['min_live_square']);

	$min_floors_flat_rent = $row['min_floors'];
	$max_floors_flat_rent = $row['max_floors'];

	$min_price_flat_rent = floor($min_price_flat_rent/1000);
	$max_price_flat_rent = ceil($max_price_flat_rent/1000);
	
	if($min_full_square_flat_rent==0){
		$min_full_square_flat_rent = 1;
	}
	
	if($max_full_square_flat_rent==0){
		$max_full_square_flat_rent = 1;
	}
	$min_meter_price_flat_rent = floor($min_price_flat_rent/$min_full_square_flat_rent);
	$max_meter_price_flat_rent = ceil($max_price_flat_rent/$max_full_square_flat_rent);
	
	$min_meter_price_flat_rent = floor($min_meter_price_flat_rent/1000);
	$max_meter_price_flat_rent = ceil($max_meter_price_flat_rent/1000);
}

/*
*  Загородная
*/
$res = mysql_query("
	SELECT 
		MIN(price) AS min_price, 
		MAX(price) AS max_price, 
		MIN(full_square) AS min_full_square, 
		MAX(full_square) AS max_full_square,
		MIN(price/full_square) AS min_full_square_meter, 
		MAX(price/full_square) AS max_full_square_meter,
		MIN(area_square) AS min_area_square, 
		MAX(area_square) AS max_area_square
	FROM ".$template."_countries
	WHERE activation='1' && type='sell'".$realUserParam.$paramLocation."
");
if(mysql_num_rows($res)>0){//_count_params
	$row = mysql_fetch_assoc($res);
	$min_price_country_sell = $row['min_price'];
	$max_price_country_sell = $row['max_price'];
	$min_full_square_country_sell = $row['min_full_square'];
	$max_full_square_country_sell = $row['max_full_square'];
	$min_full_square_meter = $row['min_full_square_meter'];
	$max_full_square_meter = $row['max_full_square_meter'];
	$min_area_square_country_sell = $row['min_area_square'];
	$max_area_square_country_sell = $row['max_area_square'];

	$min_meter_price_country_sell = floor($min_full_square_meter);
	$max_meter_price_country_sell = ceil($max_full_square_meter);
	
	$min_meter_price_country_sell = floor($min_meter_price_country_sell/1000);
	$max_meter_price_country_sell = ceil($max_meter_price_country_sell/1000);
	
	$min_price_country_sell = floor($min_price_country_sell/1000000);
	$max_price_country_sell = ceil($max_price_country_sell/1000000);
	
	$min_full_square_country_sell = floor($min_full_square_country_sell);
	// $min_full_square_country_sell = 1;
	$max_full_square_country_sell = ceil($max_full_square_country_sell);

	$min_area_square_country_sell = floor($min_area_square_country_sell);
	// $min_area_square_country_sell = 1;
	$max_area_square_country_sell = ceil($max_area_square_country_sell);
}
$res = mysql_query("
	SELECT 
		MIN(price) AS min_price, 
		MAX(price) AS max_price, 
		MIN(full_square) AS min_full_square, 
		MAX(full_square) AS max_full_square,
		MIN(price/full_square) AS min_full_square_meter, 
		MAX(price/full_square) AS max_full_square_meter,
		MIN(area_square) AS min_area_square, 
		MAX(area_square) AS max_area_square
	FROM ".$template."_countries
	WHERE activation='1' && type='rent'".$realUserParam.$paramLocation."
");
if(mysql_num_rows($res)>0){//_count_params
	$row = mysql_fetch_assoc($res);
	$min_price_country_rent = $row['min_price'];
	$max_price_country_rent = $row['max_price'];

	$min_full_square_country_rent = floor($row['min_full_square']);
	// $min_full_square_country_rent = 1;
	$max_full_square_country_rent = ceil($row['max_full_square']);

	$min_area_square_country_rent = floor($row['min_area_square']);
	// $min_area_square_country_rent = 1;
	$max_area_square_country_rent = ceil($row['min_area_square']);

	$min_price_country_rent = floor($min_price_country_rent/1000);
	$max_price_country_rent = ceil($max_price_country_rent/1000);
	
	if($min_full_square_country_rent==0){
		$min_full_square_country_rent = 1;
	}
	
	if($max_full_square_country_rent==0){
		$max_full_square_country_rent = 1;
	}
	$min_meter_price_country_rent = floor($min_price_country_rent/$min_full_square_country_rent);
	$max_meter_price_country_rent = ceil($max_price_country_rent/$max_full_square_country_rent);
	
	$min_meter_price_country_rent = floor($min_meter_price_country_rent/1000);
	$max_meter_price_country_rent = ceil($max_meter_price_country_rent/1000);
}

/*
*  Коммерция
*/
$res = mysql_query("
	SELECT 
		MIN(price) AS min_price, 
		MAX(price) AS max_price, 
		MIN(full_square) AS min_full_square, 
		MAX(full_square) AS max_full_square,
		MIN(price/full_square) AS min_full_square_meter, 
		MAX(price/full_square) AS max_full_square_meter,
		MIN(place_square) AS min_place_square, 
		MAX(place_square) AS max_place_square,
		MIN(area_square) AS min_area_square, 
		MAX(area_square) AS max_area_square
	FROM ".$template."_commercials
	WHERE activation='1' && type='sell'".$realUserParam.$paramLocation."
");
if(mysql_num_rows($res)>0){//_count_params
	$row = mysql_fetch_assoc($res);
	$min_price_commercial_sell = $row['min_price'];
	$max_price_commercial_sell = $row['max_price'];
	
	/*
	*  Min/Max цены в офисах
	*/
	$_minPriceCommercialTypeCommer_1 = $min_price_commercial_sell;
	$_maxPriceCommercialTypeCommer_1 = $max_price_commercial_sell;
	$r = mysql_query("
		SELECT 
			MIN(price) AS min_price_type_commer_1,
			MAX(price) AS max_price_type_commer_1
		FROM ".$template."_commercials 
		WHERE activation=1 && type='sell' && type_commer=1".$paramLocation."
	");
	if(mysql_num_rows($r)>0){
		$rw = mysql_fetch_assoc($r);
		$_minPriceCommercialTypeCommer_1 = $rw['min_price_type_commer_1'];
		$_maxPriceCommercialTypeCommer_1 = $rw['max_price_type_commer_1'];
	}

	$_minPriceCommercialTypeCommer_1 = floor($_minPriceCommercialTypeCommer_1/1000000);
	$_maxPriceCommercialTypeCommer_1 = ceil($_maxPriceCommercialTypeCommer_1/1000000);
	
	/*
	*  Min/Max цены в складах
	*/
	$_minPriceCommercialTypeCommer_2 = $min_price_commercial_sell;
	$_maxPriceCommercialTypeCommer_2 = $max_price_commercial_sell;
	$r = mysql_query("
		SELECT 
			MIN(price) AS min_price_type_commer_2,
			MAX(price) AS max_price_type_commer_2
		FROM ".$template."_commercials 
		WHERE activation=1 && type='sell' && type_commer=2".$paramLocation."
	");
	if(mysql_num_rows($r)>0){
		$rw = mysql_fetch_assoc($r);
		$_minPriceCommercialTypeCommer_2 = $rw['min_price_type_commer_2'];
		$_maxPriceCommercialTypeCommer_2 = $rw['max_price_type_commer_2'];
	}
	
	/*
	*  Min/Max цены в производство
	*/
	$_minPriceCommercialTypeCommer_3 = $min_price_commercial_sell;
	$_maxPriceCommercialTypeCommer_3 = $max_price_commercial_sell;
	$r = mysql_query("
		SELECT 
			MIN(price) AS min_price_type_commer_3,
			MAX(price) AS max_price_type_commer_3
		FROM ".$template."_commercials 
		WHERE activation=1 && type='sell' && type_commer=3".$paramLocation."
	");
	if(mysql_num_rows($r)>0){
		$rw = mysql_fetch_assoc($r);
		$_minPriceCommercialTypeCommer_3 = $rw['min_price_type_commer_3'];
		$_maxPriceCommercialTypeCommer_3 = $rw['max_price_type_commer_3'];
	}

	$_minPriceCommercialTypeCommer_3 = floor($_minPriceCommercialTypeCommer_3/1000000);
	$_maxPriceCommercialTypeCommer_3 = ceil($_maxPriceCommercialTypeCommer_3/1000000);

	/*
	*  Min/Max цены в торговля
	*/
	$_minPriceCommercialTypeCommer_4 = $min_price_commercial_sell;
	$_maxPriceCommercialTypeCommer_4 = $max_price_commercial_sell;
	$r = mysql_query("
		SELECT 
			MIN(price) AS min_price_type_commer_4,
			MAX(price) AS max_price_type_commer_4
		FROM ".$template."_commercials 
		WHERE activation=1 && type='sell' && type_commer=4".$paramLocation."
	");
	if(mysql_num_rows($r)>0){
		$rw = mysql_fetch_assoc($r);
		$_minPriceCommercialTypeCommer_4 = $rw['min_price_type_commer_4'];
		$_maxPriceCommercialTypeCommer_4 = $rw['max_price_type_commer_4'];
	}

	$_minPriceCommercialTypeCommer_4 = floor($_minPriceCommercialTypeCommer_4/1000000);
	$_maxPriceCommercialTypeCommer_4 = ceil($_maxPriceCommercialTypeCommer_4/1000000);

	/*
	*  Min/Max цены в зем. участке
	*/
	$_minPriceCommercialTypeCommer_5 = $min_price_commercial_sell;
	$_maxPriceCommercialTypeCommer_5 = $max_price_commercial_sell;
	$r = mysql_query("
		SELECT 
			MIN(price) AS min_price_type_commer_5,
			MAX(price) AS max_price_type_commer_5
		FROM ".$template."_commercials 
		WHERE activation=1 && type='sell' && type_commer=5".$paramLocation."
	");
	if(mysql_num_rows($r)>0){
		$rw = mysql_fetch_assoc($r);
		$_minPriceCommercialTypeCommer_5 = $rw['min_price_type_commer_5'];
		$_maxPriceCommercialTypeCommer_5 = $rw['max_price_type_commer_5'];
	}

	$_minPriceCommercialTypeCommer_5 = floor($_minPriceCommercialTypeCommer_5/1000000);
	$_maxPriceCommercialTypeCommer_5 = ceil($_maxPriceCommercialTypeCommer_5/1000000);

	/*
	*  Min/Max цены в сфера услуг
	*/
	$_minPriceCommercialTypeCommer_6 = $min_price_commercial_sell;
	$_maxPriceCommercialTypeCommer_6 = $max_price_commercial_sell;
	$r = mysql_query("
		SELECT 
			MIN(price) AS min_price_type_commer_6,
			MAX(price) AS max_price_type_commer_6
		FROM ".$template."_commercials 
		WHERE activation=1 && type='sell' && type_commer=6".$paramLocation."
	");
	if(mysql_num_rows($r)>0){
		$rw = mysql_fetch_assoc($r);
		$_minPriceCommercialTypeCommer_6 = $rw['min_price_type_commer_6'];
		$_maxPriceCommercialTypeCommer_6 = $rw['max_price_type_commer_6'];
	}

	$_minPriceCommercialTypeCommer_6 = floor($_minPriceCommercialTypeCommer_6/1000000);
	$_maxPriceCommercialTypeCommer_6 = ceil($_maxPriceCommercialTypeCommer_6/1000000);

	/*
	*  Min/Max цены в АЗС
	*/
	$_minPriceCommercialTypeCommer_7 = $min_price_commercial_sell;
	$_maxPriceCommercialTypeCommer_7 = $max_price_commercial_sell;
	$r = mysql_query("
		SELECT 
			MIN(price) AS min_price_type_commer_7,
			MAX(price) AS max_price_type_commer_7
		FROM ".$template."_commercials 
		WHERE activation=1 && type='sell' && type_commer=7".$paramLocation."
	");
	if(mysql_num_rows($r)>0){
		$rw = mysql_fetch_assoc($r);
		$_minPriceCommercialTypeCommer_7 = $rw['min_price_type_commer_7'];
		$_maxPriceCommercialTypeCommer_7 = $rw['max_price_type_commer_7'];
	}

	$_minPriceCommercialTypeCommer_7 = floor($_minPriceCommercialTypeCommer_7/1000000);
	$_maxPriceCommercialTypeCommer_7 = ceil($_maxPriceCommercialTypeCommer_7/1000000);

	/*
	*  Min/Max цены в общепит
	*/
	$_minPriceCommercialTypeCommer_8 = $min_price_commercial_sell;
	$_maxPriceCommercialTypeCommer_8 = $max_price_commercial_sell;
	$r = mysql_query("
		SELECT 
			MIN(price) AS min_price_type_commer_8,
			MAX(price) AS max_price_type_commer_8
		FROM ".$template."_commercials 
		WHERE activation=1 && type='sell' && type_commer=8".$paramLocation."
	");
	if(mysql_num_rows($r)>0){
		$rw = mysql_fetch_assoc($r);
		$_minPriceCommercialTypeCommer_8 = $rw['min_price_type_commer_8'];
		$_maxPriceCommercialTypeCommer_8 = $rw['max_price_type_commer_8'];
	}
	
	$min_full_square_commercial_sell = $row['min_full_square'];
	$max_full_square_commercial_sell = $row['max_full_square'];
	$min_full_square_meter = $row['min_full_square_meter'];
	$max_full_square_meter = $row['max_full_square_meter'];
	$min_place_square_commercial_sell = $row['min_place_square'];
	$max_place_square_commercial_sell = $row['max_place_square'];
	$min_area_square_commercial_sell = $row['min_area_square'];
	$max_area_square_commercial_sell = $row['max_area_square'];

	$min_meter_price_commercial_sell = floor($min_full_square_meter);
	$max_meter_price_commercial_sell = ceil($max_full_square_meter);
	
	$min_meter_price_commercial_sell = floor($min_meter_price_commercial_sell/1000);
	$max_meter_price_commercial_sell = ceil($max_meter_price_commercial_sell/1000);
	
	$min_price_commercial_sell = floor($min_price_commercial_sell/1000000);
	$max_price_commercial_sell = ceil($max_price_commercial_sell/1000000);
	
	$min_full_square_commercial_sell = floor($min_full_square_commercial_sell);
	// $min_full_square_commercial_sell = 1;
	$max_full_square_commercial_sell = ceil($max_full_square_commercial_sell);

	$min_place_square_commercial_sell = floor($min_place_square_commercial_sell);
	// $min_place_square_commercial_sell = 1;
	$max_place_square_commercial_sell = ceil($max_place_square_commercial_sell);

	$min_area_square_commercial_sell = floor($min_area_square_commercial_sell);
	// $min_area_square_commercial_sell = 1;
	$max_area_square_commercial_sell = ceil($max_area_square_commercial_sell);
}
$res = mysql_query("
	SELECT 
		MIN(price) AS min_price, 
		MAX(price) AS max_price, 
		MIN(full_square) AS min_full_square, 
		MAX(full_square) AS max_full_square,
		MIN(price/full_square) AS min_full_square_meter, 
		MAX(price/full_square) AS max_full_square_meter,
		MIN(place_square) AS min_place_square, 
		MAX(place_square) AS max_place_square,
		MIN(area_square) AS min_area_square, 
		MAX(area_square) AS max_area_square
	FROM ".$template."_commercials
	WHERE activation='1' && type='rent'".$realUserParam.$paramLocation."
");
if(mysql_num_rows($res)>0){//_count_params
	$row = mysql_fetch_assoc($res);
	$min_price_commercial_rent = $row['min_price'];
	$max_price_commercial_rent = $row['max_price'];
	
	/*
	*  Min/Max цены в офисах
	*/
	$_minPriceCommercialTypeCommerRent_1 = $min_price_commercial_rent;
	$_maxPriceCommercialTypeCommerRent_1 = $max_price_commercial_rent;
	$r = mysql_query("
		SELECT 
			MIN(price) AS min_price_type_commer_1,
			MAX(price) AS max_price_type_commer_1
		FROM ".$template."_commercials 
		WHERE activation=1 && type='rent' && type_commer=1".$paramLocation."
	");
	if(mysql_num_rows($r)>0){
		$rw = mysql_fetch_assoc($r);
		$_minPriceCommercialTypeCommerRent_1 = $rw['min_price_type_commer_1'];
		$_maxPriceCommercialTypeCommerRent_1 = $rw['max_price_type_commer_1'];
	}

	$_minPriceCommercialTypeCommerRent_1 = floor($_minPriceCommercialTypeCommerRent_1/1000000);
	$_maxPriceCommercialTypeCommerRent_1 = ceil($_maxPriceCommercialTypeCommerRent_1/1000000);
	
	/*
	*  Min/Max цены в складах
	*/
	$_minPriceCommercialTypeCommerRent_2 = $min_price_commercial_rent;
	$_maxPriceCommercialTypeCommerRent_2 = $max_price_commercial_rent;
	$r = mysql_query("
		SELECT 
			MIN(price) AS min_price_type_commer_2,
			MAX(price) AS max_price_type_commer_2
		FROM ".$template."_commercials 
		WHERE activation=1 && type='rent' && type_commer=2".$paramLocation."
	");
	if(mysql_num_rows($r)>0){
		$rw = mysql_fetch_assoc($r);
		$_minPriceCommercialTypeCommerRent_2 = $rw['min_price_type_commer_2'];
		$_maxPriceCommercialTypeCommerRent_2 = $rw['max_price_type_commer_2'];
	}
	
	/*
	*  Min/Max цены в производство
	*/
	$_minPriceCommercialTypeCommerRent_3 = $min_price_commercial_rent;
	$_maxPriceCommercialTypeCommerRent_3 = $max_price_commercial_rent;
	$r = mysql_query("
		SELECT 
			MIN(price) AS min_price_type_commer_3,
			MAX(price) AS max_price_type_commer_3
		FROM ".$template."_commercials 
		WHERE activation=1 && type='rent' && type_commer=3".$paramLocation."
	");
	if(mysql_num_rows($r)>0){
		$rw = mysql_fetch_assoc($r);
		$_minPriceCommercialTypeCommerRent_3 = $rw['min_price_type_commer_3'];
		$_maxPriceCommercialTypeCommerRent_3 = $rw['max_price_type_commer_3'];
	}

	$_minPriceCommercialTypeCommerRent_3 = floor($_minPriceCommercialTypeCommerRent_3/1000000);
	$_maxPriceCommercialTypeCommerRent_3 = ceil($_maxPriceCommercialTypeCommerRent_3/1000000);

	/*
	*  Min/Max цены в торговля
	*/
	$_minPriceCommercialTypeCommerRent_4 = $min_price_commercial_rent;
	$_maxPriceCommercialTypeCommerRent_4 = $max_price_commercial_rent;
	$r = mysql_query("
		SELECT 
			MIN(price) AS min_price_type_commer_4,
			MAX(price) AS max_price_type_commer_4
		FROM ".$template."_commercials 
		WHERE activation=1 && type='rent' && type_commer=4".$paramLocation."
	");
	if(mysql_num_rows($r)>0){
		$rw = mysql_fetch_assoc($r);
		$_minPriceCommercialTypeCommerRent_4 = $rw['min_price_type_commer_4'];
		$_maxPriceCommercialTypeCommerRent_4 = $rw['max_price_type_commer_4'];
	}

	$_minPriceCommercialTypeCommerRent_4 = floor($_minPriceCommercialTypeCommerRent_4/1000000);
	$_maxPriceCommercialTypeCommerRent_4 = ceil($_maxPriceCommercialTypeCommerRent_4/1000000);

	/*
	*  Min/Max цены в зем. участке
	*/
	$_minPriceCommercialTypeCommerRent_5 = $min_price_commercial_rent;
	$_maxPriceCommercialTypeCommerRent_5 = $max_price_commercial_rent;
	$r = mysql_query("
		SELECT 
			MIN(price) AS min_price_type_commer_5,
			MAX(price) AS max_price_type_commer_5
		FROM ".$template."_commercials 
		WHERE activation=1 && type='rent' && type_commer=5".$paramLocation."
	");
	if(mysql_num_rows($r)>0){
		$rw = mysql_fetch_assoc($r);
		$_minPriceCommercialTypeCommerRent_5 = $rw['min_price_type_commer_5'];
		$_maxPriceCommercialTypeCommerRent_5 = $rw['max_price_type_commer_5'];
	}

	$_minPriceCommercialTypeCommerRent_5 = floor($_minPriceCommercialTypeCommerRent_5/1000000);
	$_maxPriceCommercialTypeCommerRent_5 = ceil($_maxPriceCommercialTypeCommerRent_5/1000000);

	/*
	*  Min/Max цены в сфера услуг
	*/
	$_minPriceCommercialTypeCommerRent_6 = $min_price_commercial_rent;
	$_maxPriceCommercialTypeCommerRent_6 = $max_price_commercial_rent;
	$r = mysql_query("
		SELECT 
			MIN(price) AS min_price_type_commer_6,
			MAX(price) AS max_price_type_commer_6
		FROM ".$template."_commercials 
		WHERE activation=1 && type='rent' && type_commer=6".$paramLocation."
	");
	if(mysql_num_rows($r)>0){
		$rw = mysql_fetch_assoc($r);
		$_minPriceCommercialTypeCommerRent_6 = $rw['min_price_type_commer_6'];
		$_maxPriceCommercialTypeCommerRent_6 = $rw['max_price_type_commer_6'];
	}

	$_minPriceCommercialTypeCommerRent_6 = floor($_minPriceCommercialTypeCommerRent_6/1000000);
	$_maxPriceCommercialTypeCommerRent_6 = ceil($_maxPriceCommercialTypeCommerRent_6/1000000);

	/*
	*  Min/Max цены в АЗС
	*/
	$_minPriceCommercialTypeCommerRent_7 = $min_price_commercial_rent;
	$_maxPriceCommercialTypeCommerRent_7 = $max_price_commercial_rent;
	$r = mysql_query("
		SELECT 
			MIN(price) AS min_price_type_commer_7,
			MAX(price) AS max_price_type_commer_7
		FROM ".$template."_commercials 
		WHERE activation=1 && type='rent' && type_commer=7".$paramLocation."
	");
	if(mysql_num_rows($r)>0){
		$rw = mysql_fetch_assoc($r);
		$_minPriceCommercialTypeCommerRent_7 = $rw['min_price_type_commer_7'];
		$_maxPriceCommercialTypeCommerRent_7 = $rw['max_price_type_commer_7'];
	}

	$_minPriceCommercialTypeCommerRent_7 = floor($_minPriceCommercialTypeCommerRent_7/1000000);
	$_maxPriceCommercialTypeCommerRent_7 = ceil($_maxPriceCommercialTypeCommerRent_7/1000000);

	/*
	*  Min/Max цены в общепит
	*/
	$_minPriceCommercialTypeCommerRent_8 = $min_price_commercial_rent;
	$_maxPriceCommercialTypeCommerRent_8 = $max_price_commercial_rent;
	$r = mysql_query("
		SELECT 
			MIN(price) AS min_price_type_commer_8,
			MAX(price) AS max_price_type_commer_8
		FROM ".$template."_commercials 
		WHERE activation=1 && type='rent' && type_commer=8".$paramLocation."
	");
	if(mysql_num_rows($r)>0){
		$rw = mysql_fetch_assoc($r);
		$_minPriceCommercialTypeCommerRent_8 = $rw['min_price_type_commer_8'];
		$_maxPriceCommercialTypeCommerRent_8 = $rw['max_price_type_commer_8'];
	}

	$_minPriceCommercialTypeCommerRent_8 = floor($_minPriceCommercialTypeCommerRent_8/1000000);
	$_maxPriceCommercialTypeCommerRent_8 = ceil($_maxPriceCommercialTypeCommerRent_8/1000000);
	
	$min_full_square_commercial_rent = $row['min_full_square'];
	$max_full_square_commercial_rent = $row['max_full_square'];
	$min_full_square_meter = $row['min_full_square_meter'];
	$max_full_square_meter = $row['max_full_square_meter'];
	$min_place_square_commercial_rent = $row['min_place_square'];
	$max_place_square_commercial_rent = $row['max_place_square'];
	$min_area_square_commercial_rent = $row['min_area_square'];
	$max_area_square_commercial_rent = $row['max_area_square'];

	$min_meter_price_commercial_rent = floor($min_full_square_meter);
	$max_meter_price_commercial_rent = ceil($max_full_square_meter);
	
	$min_meter_price_commercial_rent = floor($min_meter_price_commercial_rent/1000);
	$max_meter_price_commercial_rent = ceil($max_meter_price_commercial_rent/1000);
	
	$min_price_commercial_rent = floor($min_price_commercial_rent/1000000);
	$max_price_commercial_rent = ceil($max_price_commercial_rent/1000000);
	
	$min_full_square_commercial_rent = floor($min_full_square_commercial_rent);
	// $min_full_square_commercial_rent = 1;
	$max_full_square_commercial_rent = ceil($max_full_square_commercial_rent);

	$min_place_square_commercial_rent = floor($min_place_square_commercial_rent);
	// $min_place_square_commercial_rent = 1;
	$max_place_square_commercial_rent = ceil($max_place_square_commercial_rent);

	$min_area_square_commercial_rent = floor($min_area_square_commercial_rent);
	// $min_area_square_commercial_rent = 1;
	$max_area_square_commercial_rent = ceil($max_area_square_commercial_rent);
}

/*
*  Комнаты
*/
$res = mysql_query("
	SELECT 
		MIN(price) AS min_price, 
		MAX(price) AS max_price, 
		MIN(room_square) AS min_room_square, 
		MAX(room_square) AS max_room_square,
		MIN(full_square) AS min_full_square, 
		MAX(full_square) AS max_full_square,
		MIN(price/full_square) AS min_full_square_meter, 
		MAX(price/full_square) AS max_full_square_meter, 
		MIN(kitchen_square) AS min_kitchen_square, 
		MAX(kitchen_square) AS max_kitchen_square,
		MIN(floors) AS min_floors, 
		MAX(floors) AS max_floors
	FROM ".$template."_rooms
	WHERE activation='1' && type='sell'".$realUserParam.$paramLocation."
");
if(mysql_num_rows($res)>0){//_count_params
	$row = mysql_fetch_assoc($res);
	$min_price_room_sell = $row['min_price'];
	$max_price_room_sell = $row['max_price'];
	$min_room_square_room_sell = floor($row['min_room_square']);
	// $min_room_square_room_sell = 1;
	$max_room_square_room_sell = ceil($row['max_room_square']);

	$min_full_square_room_sell = floor($row['min_full_square']);
	// $min_full_square_room_sell = 1;
	$max_full_square_room_sell = ceil($row['max_full_square']);

	$min_full_square_meter = floor($row['min_full_square_meter']);
	// $min_full_square_meter = 1;
	$max_full_square_meter = ceil($row['max_full_square_meter']);

	$min_kitchen_square_room_sell = floor($row['min_kitchen_square']);
	// $min_kitchen_square_room_sell = 1;
	$max_kitchen_square_room_sell = ceil($row['max_kitchen_square']);

	$min_floors_room_sell = $row['min_floors'];
	// $min_floors_room_sell = 1;
	$max_floors_room_sell = $row['max_floors'];

	$min_meter_price_room_sell = $min_full_square_meter;
	$max_meter_price_room_sell = $max_full_square_meter;
	
	$min_meter_price_room_sell = floor($min_meter_price_room_sell/1000);
	$max_meter_price_room_sell = ceil($max_meter_price_room_sell/1000);
	
	$min_price_room_sell = floor($min_price_room_sell/1000);
	$max_price_room_sell = ceil($max_price_room_sell/1000);
}
$res = mysql_query("
	SELECT 
		MIN(price) AS min_price, 
		MAX(price) AS max_price, 
		MIN(room_square) AS min_room_square, 
		MAX(room_square) AS max_room_square,
		MIN(full_square) AS min_full_square, 
		MAX(full_square) AS max_full_square,
		MIN(kitchen_square) AS min_kitchen_square, 
		MAX(kitchen_square) AS max_kitchen_square,
		MIN(floors) AS min_floors, 
		MAX(floors) AS max_floors
	FROM ".$template."_rooms
	WHERE activation='1' && type='rent'".$realUserParam.$paramLocation."
");
if(mysql_num_rows($res)>0){//_count_params
	$row = mysql_fetch_assoc($res);
	$min_price_room_rent = $row['min_price'];
	$max_price_room_rent = $row['max_price'];
	$min_room_square_room_rent = floor($row['min_room_square']);
	$max_room_square_room_rent = ceil($row['max_room_square']);

	$min_full_square_room_rent = floor($row['min_full_square']);
	// $min_full_square_room_rent = 1;
	$max_full_square_room_rent = ceil($row['max_full_square']);

	$min_kitchen_square_room_rent = floor($row['min_kitchen_square']);
	// $min_kitchen_square_room_rent = 1;
	$max_kitchen_square_room_rent = ceil($row['max_kitchen_square']);

	$min_floors_room_rent = $row['min_floors'];
	$max_floors_room_rent = $row['max_floors'];

	$min_price_room_rent = floor($min_price_room_rent/1000);
	$max_price_room_rent = ceil($max_price_room_rent/1000);
	
	if($min_full_square_room_rent==0){
		$min_full_square_room_rent = 1;
	}	
	if($max_full_square_room_rent==0){
		$max_full_square_room_rent = 1;
	}
	$min_meter_price_room_rent = floor($min_price_room_rent/$min_full_square_room_rent);
	$max_meter_price_room_rent = ceil($max_price_room_rent/$max_full_square_room_rent);
	
	$min_meter_price_room_rent = floor($min_meter_price_room_rent/1000);
	$max_meter_price_room_rent = ceil($max_meter_price_room_rent/1000);
}


$_minPriceNew = $min_price_new_sell; // минимальная цена новостройки за всё
$_maxPriceNew = $max_price_new_sell; // максимальная новостройки цена за всё

$_minMeterNewPrice = 40; // минимальная цена новостройки за метр
$_maxMeterNewPrice = 160; // максимальная цена новостройки за метр

$_minPriceFlat = $min_price_flat_sell; // минимальная цена вторички за всё (купить)
$_maxPriceFlat = $max_price_flat_sell; // максимальная вторички цена за всё (купить)

$_minMeterFlatPrice = 40; // минимальная цена вторички за метр (купить)
$_maxMeterFlatPrice = 160; // максимальная цена вторички за метр (купить)

$_minPriceFlatRent = $min_price_flat_rent; // минимальная цена вторички за всё (снять)
$_maxPriceFlatRent = $max_price_flat_rent; // максимальная вторички цена за всё (снять)

$_minPriceRoom = $min_price_room_sell; // минимальная цена комнаты за всё (купить)
$_maxPriceRoom = $max_price_room_sell; // максимальная комнаты цена за всё (купить)

$_minMeterRoomPrice = 30; // минимальная цена комнаты за метр (купить)
$_maxMeterRoomPrice = 150; // максимальная цена комнаты за метр (купить)

$_minPriceRoomRent = $min_price_room_rent; // минимальная цена комнаты за всё (снять)
$_maxPriceRoomRent = $max_price_room_rent; // максимальная комнаты цена за всё (снять)

$_minPriceCountry = $min_price_country_sell; // минимальная цена загородной за всё (купить)
$_maxPriceCountry = $max_price_country_sell; // максимальная загородной цена за всё (купить)

$_minMeterCountryPrice = 70; // минимальная цена загородной за метр (купить)
$_maxMeterCountryPrice = 200; // максимальная цена загородной за метр (купить)

$_minPriceCountryRent = $min_price_country_sell; // минимальная цена загородной за всё (снять)
$_maxPriceCountryRent = $max_price_country_sell; // максимальная загородной цена за всё (снять)

$_minMeterCountryPrice = 70; // минимальная цена загородной за метр (снять)
$_maxMeterCountryPrice = 200; // максимальная цена загородной за метр (снять)

$_minPriceCommercial = $min_price_commercial_sell; // минимальная цена коммерческой за всё
$_maxPriceCommercial = $max_price_commercial_sell; // максимальная коммерческой цена за всё

$_minMeterCommercialPrice = 70; // минимальная цена коммерческой за метр
$_maxMeterCommercialPrice = 260; // максимальная цена коммерческой за метр

$_minPriceCommercialRent = $min_price_commercial_rent; // минимальная цена коммерческой за всё (снять)
$_maxPriceCommercialRent = $max_price_commercial_rent; // максимальная цена коммерческой за всё (снять)

// $start = microtime(true);

$searchPage = false;
if($params[0] && $params[0]=='search'){
	$searchPage = true;
}

$estateValue = 1;
$typeValue = 'sell';
$delivery_new_var = '';
if(count($arrayLinkSearch['type'])==0){
	$sellNewParam = true;
}
if(count($arrayLinkSearch['type'])>0){
	$typeValue = $arrayLinkSearch['type'];
}
if(count($arrayLinkSearch['estate'])>0){
	$estateValue = $arrayLinkSearch['estate'];
}
if($arrayLinkSearch['type']=='sell' && $arrayLinkSearch['estate']==1){
	$sellNewParam = true;
}

$_ESTATE_ARRAY = array("1"=>"new","2"=>"flat","3"=>"room","4"=>"country","5"=>"commercial","6"=>"cession");

// echo '<pre>';
// print_r($arrayLinkSearch);
// echo '</pre>';

if(is_array($arrayLinkSearch) && count($arrayLinkSearch)>0){
	foreach($arrayLinkSearch as $name => $value){
		$var = $name."_".$_ESTATE_ARRAY[$arrayLinkSearch['estate']]."_".$arrayLinkSearch['type']."_var";
		// echo $var.'<br>';
		$$var = $value;
	}
}

if(!$estate_flat_sell_var && !$estate_flat_rent_var && !$estate_room_rent_var && !$estate_room_sell_var && !$estate_country_rent_var && !$estate_country_sell_var && !$estate_commercial_rent_var && !$estate_commercial_sell_var && !$estate_cession_sell_var){
	$estate_new_sell_var = 1;
}

if(!$type_new_sell_var && !$searchPage){
	$type_new_sell_var = 'sell';
}
if(!$arrayLinkSearch){
	$priceAll_new_sell_var = 'all';
}
?>


<div id="filter_search"<?if($type_commercial_rent_var || $type_commercial_sell_var){print' class="commercial"';}else{if($type_country_rent_var || $type_country_sell_var){print' class="country"';}}?>>
	<form action="/search" method="get">
		<div class="simple_search">
			<div class="inner_block">
				<div class="row">
					<div class="checkbox_block types">
						<input type="hidden" name="type" value="<?=$typeValue?>">
						<div class="select_checkbox">
							<ul>
								<li <?$typeValue=='sell'?print'class="checked"':''?>><a data-type="sell" href="javascript:void(0)" data-id="sell">Купить</a></li>
								<?
								// if($estateValue==1 || $estateValue==6){
									// echo '<li class="none"><a data-type="rent" href="javascript:void(0)" data-id="rent">Снять</a><i class="not_active"></i></li>';
								// }
								// else {
									$checked = '';
									if($typeValue=='rent'){
										$checked = ' class="checked"';
									}
									echo '<li'.$checked.'><a data-type="rent" href="javascript:void(0)" data-id="rent">Снять</a></li>';
								// }
								?>
							</ul>
						</div>
					</div>
					<div class="checkbox_block estate">
						<input type="hidden" name="estate" value="<?=$estateValue?>">
						<div class="select_checkbox">
							<ul>
								<li <?$estateValue==1?print'class="checked"':''?>><a data-type="new" href="javascript:void(0)" data-id="1">Новостройку</a></li>
								<!--<li <?$estateValue==6?print'class="checked"':''?>><a data-type="cession" href="javascript:void(0)" data-id="6">Переуступку</a></li>-->
								<li <?$estateValue==2?print'class="checked"':''?>><a data-type="flat" href="javascript:void(0)" data-id="2">Квартиру</a></li>
								<li <?$estateValue==3?print'class="checked"':''?>><a data-type="room" href="javascript:void(0)" data-id="3">Комнату</a></li>
								<!--<li <?$estateValue==4?print'class="checked"':''?>><a data-type="country" href="javascript:void(0)" data-id="4">Загородную</a></li>-->
								<li <?$estateValue==5?print'class="checked"':''?>><a data-type="commercial" href="javascript:void(0)" data-id="5">Коммерческую</a></li>
								<!--<li class="none" <?$estateValue==5?print'class="checked"':''?>><a data-type="commercial" href="javascript:void(0)" data-id="5">Коммерческую</a><i class="not_active"></i></li>-->
							</ul>
						</div>
					</div>
					<div <?!$priceAll_new_sell_var && !$sellNewParam || $cession_new_sell_var?print'style="display:none"':'';?> class="price_block new sell">
						<div <?$priceAll_new_sell_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeNewSell = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_new_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeNewSell = '<li><a class="min" data-name_select="price" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_new_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeNewSell = '<li><a class="max" data-name_select="price" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_new_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeNewSell = '<li><a class="min max" data-name_select="price" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'цена От';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">цена От</a></li>
										<?
										for($c=$_minPriceNew; $c<=$_maxPriceNew;){
											$current = "";
											if($c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceNew){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="price" data-tags="от 4 000 000." data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="price" data-tags="от 8 000 000." data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50){
												$c = $c+5;
											}
											if($c>$_maxPriceNew){
												$c = $_maxPriceNew;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'цена До';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">цена До</a></li>
										<?									
										for($c=$_minPriceNew; $c<=$_maxPriceNew;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceNew){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="price" data-tags="от 4 000 000" data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="price" data-tags="от 8 000 000" data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50){
												$c = $c+5;
											}
											if($c>$_maxPriceNew){
												$c = $_maxPriceNew;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						
						<div <?$priceAll_new_sell_var=='all' || !isset($priceAll_new_sell_var)?print'style="display:none"':print'style="display:block!important"'?> class="prices_choose meters<?!$searchPage?print' nosearchpage':''?>">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_new_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeNewSell = '<li><a class="min" data-name_select="price_meter" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_new_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeNewSell = '<li><a class="max" data-name_select="price_meter" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_new_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeNewSell = '<li><a class="min max" data-name_select="price_meter" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMeterMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$min_meter_price_new_sell; $c<=$max_meter_price_new_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMeterMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_meter_price_new_sell; $c<=$max_meter_price_new_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($sellNewParam && $arrayLinkSearch['priceAll']=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="angles">
								<div class="angle-top-border"></div>
								<div class="angle-top-white"></div>
							</div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all"><span>Цена за все</span></a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters"><span>Цена за м<sup>2</sup></span></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$cession_new_sell_var?print'style="display:none"':print'style="display:block"';?> class="price_block cession sell">
						<div <?if($priceAll_new_sell_var=='meters' && $cession_new_sell_var){print'style="display:none"';}if($priceAll_new_sell_var=='all' && $cession_new_sell_var){print'style="display:block"';}?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCessionSell = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_new_sell_var && $cession_new_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCessionSell = '<li><a class="min" data-name_select="price" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_new_sell_var && $cession_new_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCessionSell = '<li><a class="max" data-name_select="price" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_new_sell_var && $cession_new_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCessionSell = '<li><a class="min max" data-name_select="price" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'. до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_new_sell_var && $cession_new_sell_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$min_price_cession_sell; $c<=$max_price_cession_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price" data-tags="от '.str_replace(".", ",", $c).' '.$name_type.'." data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_price_cession_sell){
												break;
											}
											if($c<=4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<=8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<=12){
												$c = $c+0.3;
											}
											if($c>=12 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50){
												$c = $c+5;
											}
											if($c>$max_price_cession_sell){
												$c = $max_price_cession_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_new_sell_var && $cession_new_sell_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
										for($c=$min_price_cession_sell; $c<=$max_price_cession_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price" data-tags="до '.str_replace(".", ",", $c).' '.$name_type.'." data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_price_cession_sell){
												break;
											}
											if($c<=5){
												$c = $c+0.1;
											}
											if($c>5 && $c<=10){
												$c = $c+0.2;
											}
											if($c>10 && $c<=20){
												$c = $c+0.5;
											}
											if($c>20){
												$c = $c+1;
											}
											if($c>$max_price_cession_sell){
												$c = $max_price_cession_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?if($priceAll_new_sell_var=='all' && $cession_new_sell_var){print'style="display:none"';}if($priceAll_new_sell_var=='meters' && $cession_new_sell_var){print'style="display:block"';}?> class="prices_choose meters<?!$searchPage?print' nosearchpage':''?>">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_new_sell_var && $cession_new_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCessionSell = '<li><a class="min" data-name_select="price_meter" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_new_sell_var && $cession_new_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCessionSell = '<li><a class="max" data-name_select="price_meter" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_new_sell_var && $cession_new_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCessionSell = '<li><a class="min max" data-name_select="price_meter" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.str_replace(".", ",", $c1).' '.$name_type.'. до '.str_replace(".", ",", $c2).' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_new_sell_var && $cession_new_sell_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div style="width:90px" class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$min_meter_price_new_sell; $c<=$max_meter_price_new_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter" data-tags="от '.str_replace(".", ",", $c).' '.$name_type.'." data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_new_sell_var && $cession_new_sell_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_meter_price_new_sell; $c<=$max_meter_price_new_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter" data-tags="до '.str_replace(".", ",", $c).' '.$name_type.'." data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_new_sell_var && $cession_new_sell_var && $arrayLinkSearch['priceAll']=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">Цена за все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">Цена за м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_flat_sell_var?print'style="display:none"':print'style="display:block"';?> class="price_block flat sell">
						<div <?$priceAll_flat_sell_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeFlatSell = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_flat_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeFlatSell = '<li><a class="min" data-name_select="priceflat" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_flat_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeFlatSell = '<li><a class="max" data-name_select="priceflat" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_flat_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeFlatSell = '<li><a class="min max" data-name_select="priceflat" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_flat_sell_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$min_price_flat_sell; $c<=$max_price_flat_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="priceflat" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_price_flat_sell){
												break;
											}
											if($c<=4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<=8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<=12){
												$c = $c+0.3;
											}
											if($c>=12 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50){
												$c = $c+5;
											}
											// if($c<=5){
												// $c = $c+0.1;
											// }
											// if($c>5 && $c<=10){
												// $c = $c+0.2;
											// }
											// if($c>10 && $c<=20){
												// $c = $c+0.5;
											// }
											// if($c>20){
												// $c = $c+1;
											// }
											if($c>$max_price_flat_sell){
												$c = $max_price_flat_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_flat_sell_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
										for($c=$min_price_flat_sell; $c<=$max_price_flat_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="priceflat" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_price_flat_sell){
												break;
											}
											if($c<=5){
												$c = $c+0.1;
											}
											if($c>5 && $c<=10){
												$c = $c+0.2;
											}
											if($c>10 && $c<=20){
												$c = $c+0.5;
											}
											if($c>20){
												$c = $c+1;
											}
											if($c>$max_price_flat_sell){
												$c = $max_price_flat_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_flat_sell_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_flat_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeFlatSell = '<li><a class="min" data-name_select="pricefloat_meter" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_flat_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeFlatSell = '<li><a class="max" data-name_select="pricefloat_meter" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_flat_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeFlatSell = '<li><a class="min max" data-name_select="pricefloat_meter" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_flat_sell_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<!--<div style="width:90px" class="choosed_block"><?=$nameParams?></div>-->
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMeterMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$min_meter_price_flat_sell; $c<=$max_meter_price_flat_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricefloat_meter" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_meter_price_flat_sell){
												break;
											}
											$c = $c+1;
											if($c>$max_meter_price_flat_sell){
												$c = $max_meter_price_flat_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_flat_sell_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMeterMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_meter_price_flat_sell; $c<=$max_meter_price_flat_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricefloat_meter" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_meter_price_flat_sell){
												break;
											}
											$c = $c+1;
											if($c>$max_meter_price_flat_sell){
												$c = $max_meter_price_flat_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_flat_sell_var && $priceAll_flat_sell_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">Цена за все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">Цена за м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_flat_rent_var?print'style="display:none"':print'style="display:block"';?> class="price_block flat rent">
						<div <?$priceAll_flat_rent_var=='night'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$mult = 1000;
								$name_type = 'тыс';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_flat_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeFlatRent = '<li><a class="min" data-name_select="priceflat_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_flat_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeFlatRent = '<li><a class="max" data-name_select="priceflat_rent" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_flat_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeFlatRent = '<li><a class="min max" data-name_select="priceflat_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_flat_rent_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$min_price_flat_rent; $c<=$max_price_flat_rent;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="priceflat_rent" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_flat_rent_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:90px" class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_price_flat_rent; $c<=$max_price_flat_rent;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="priceflat_rent" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_flat_rent_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose night">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$mult = 1000;
								$name_type = 'тыс';
								if($arrayLinkSearch['priceNightMin'] && !$arrayLinkSearch['priceNightMax'] && $priceAll_flat_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceNightMin']/$mult;
									$priceChangeFlatRent = '<li><a class="min" data-name_select="priceflat_rent" data-name="priceNightMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceNightMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceNightMin'] && $arrayLinkSearch['priceNightMax'] && $priceAll_flat_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceNightMax']/$mult;
									$priceChangeFlatRent = '<li><a class="max" data-name_select="priceflat_rent" data-name="priceNightMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceNightMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceNightMin'] && $arrayLinkSearch['priceNightMax'] && $priceAll_flat_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceNightMin']/$mult;
									$c2 = $arrayLinkSearch['priceNightMax']/$mult;
									$priceChangeFlatRent = '<li><a class="min max" data-name_select="priceflat_rent" data-name="priceNightMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceNightMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_flat_rent_var && $arrayLinkSearch['priceNightMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceNightMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceNightMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceNightMin'].'"';
								}
								?>
								<input type="hidden" name="priceNightMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceNightMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$min_price_flat_rent; $c<=$max_price_flat_rent;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="priceflat_rent" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_flat_rent_var && $arrayLinkSearch['priceNightMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceNightMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceNightMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceNightMax'].'"';
								}
								?>
								<input type="hidden" name="priceNightMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceNightMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_price_flat_rent; $c<=$max_price_flat_rent;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="priceflat_rent" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'руб/мес';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_flat_rent_var && $priceAll_flat_rent_var=='night'){
								$nameParams = 'руб/сутки';
								$valueParams = ' value="night"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div style="width:90px" class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">руб/мес</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="night">руб/сутки</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_room_sell_var?print'style="display:none"':print'style="display:block"';?> class="price_block room sell">
						<div <?$priceAll_room_sell_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 100000;
								$priceChangeRoomSell = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_room_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeRoomSell = '<li><a class="min" data-name_select="priceroom" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_room_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeRoomSell = '<li><a class="max" data-name_select="priceroom" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_room_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeRoomSell = '<li><a class="min max" data-name_select="priceroom" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_room_sell_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$min_price_room_sell; $c<=$max_price_room_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="priceroom" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_price_room_sell){
												break;
											}
											$c = $c+50;
											if($c>$max_price_room_sell){
												$c = $max_price_room_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_room_sell_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
										for($c=$min_price_room_sell; $c<=$max_price_room_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="priceroom" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_price_room_sell){
												break;
											}
											$c = $c+50;
											if($c>$max_price_room_sell){
												$c = $max_price_room_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_room_sell_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_room_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeRoomSell = '<li><a class="min" data-name_select="price_meter_room" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_room_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeRoomSell = '<li><a class="max" data-name_select="price_meter_room" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_room_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeRoomSell = '<li><a class="min max" data-name_select="price_meter_room" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_room_sell_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMeterMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_meter_price_room_sell; $c<=$max_meter_price_room_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_room" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_meter_price_room_sell){
												break;
											}
											$c = $c+1;
											if($c>$max_meter_price_room_sell){
												$c = $max_meter_price_room_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_room_sell_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMeterMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_meter_price_room_sell; $c<=$max_meter_price_room_sell;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_room" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_meter_price_room_sell){
												break;
											}
											$c = $c+1;
											if($c>$max_meter_price_room_sell){
												$c = $max_meter_price_room_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_room_sell_var && $priceAll_room_sell_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">Цена за все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">Цена за м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_room_rent_var?print'style="display:none"':print'style="display:block"';?> class="price_block room rent">
						<div <?$priceAll_room_rent_var=='night'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$mult = 1000;
								$name_type = 'тыс';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_room_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeRoomRent = '<li><a class="min" data-name_select="priceroom_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_room_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeRoomRent = '<li><a class="max" data-name_select="priceroom_rent" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_room_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeRoomRent = '<li><a class="min max" data-name_select="priceroom_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_room_rent_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$min_price_room_rent; $c<=$max_price_room_rent;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="priceroom_rent" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_price_room_rent){
												break;
											}
											$c = $c+1;
											if($c>$max_price_room_rent){
												$c = $max_price_room_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_room_rent_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_price_room_rent; $c<=$max_price_room_rent;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="priceroom_rent" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_price_room_rent){
												break;
											}
											$c = $c+1;
											if($c>$max_price_room_rent){
												$c = $max_price_room_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_room_rent_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose night">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$mult = 1000;
								$name_type = 'тыс';
								if($arrayLinkSearch['priceNightMin'] && !$arrayLinkSearch['priceNightMax'] && $priceAll_room_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceNightMin']/$mult;
									$priceChangeRoomRent = '<li><a class="min" data-name_select="priceroom_rent" data-name="priceNightMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceNightMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceNightMin'] && $arrayLinkSearch['priceNightMax'] && $priceAll_room_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceNightMax']/$mult;
									$priceChangeRoomRent = '<li><a class="max" data-name_select="priceroom_rent" data-name="priceNightMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceNightMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceNightMin'] && $arrayLinkSearch['priceNightMax'] && $priceAll_room_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceNightMin']/$mult;
									$c2 = $arrayLinkSearch['priceNightMax']/$mult;
									$priceChangeRoomRent = '<li><a class="min max" data-name_select="priceroom_rent" data-name="priceNightMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceNightMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_room_rent_var && $arrayLinkSearch['priceNightMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceNightMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceNightMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceNightMin'].'"';
								}
								?>
								<input type="hidden" name="priceNightMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceNightMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$min_price_room_rent; $c<=$max_price_room_rent;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="priceroom_rent" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_price_room_rent){
												break;
											}
											$c = $c+1;
											if($c>$max_price_room_rent){
												$c = $max_price_room_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_room_rent_var && $arrayLinkSearch['priceNightMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceNightMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceNightMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceNightMax'].'"';
								}
								?>
								<input type="hidden" name="priceNightMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceNightMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_price_room_rent; $c<=$max_price_room_rent;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="priceroom_rent" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_price_room_rent){
												break;
											}
											$c = $c+1;
											if($c>$max_price_room_rent){
												$c = $max_price_room_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'руб/мес';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_room_rent_var && $priceAll_room_rent_var=='night'){
								$nameParams = 'руб/сутки';
								$valueParams = ' value="night"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div style="width:90px" class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">руб/мес</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="night">руб/сутки</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_country_sell_var?print'style="display:none"':print'style="display:block"';?> class="price_block country sell">
						<div <?$priceAll_country_sell_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCountrySell = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_country_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCountrySell = '<li><a class="min" data-name_select="pricecountry" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_country_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCountrySell = '<li><a class="max" data-name_select="pricecountry" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_country_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCountrySell = '<li><a class="min max" data-name_select="pricecountry" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_country_sell_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minPriceCountry; $c<=$_maxPriceCountry;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecountry" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+0.1;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_country_sell_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
										for($c=$_minPriceCountry; $c<=$_maxPriceCountry;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecountry" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+0.1;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_country_sell_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_country_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCountrySell = '<li><a class="min" data-name_select="price_meter_country" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_country_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCountrySell = '<li><a class="max" data-name_select="price_meter_country" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_country_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCountrySell = '<li><a class="min max" data-name_select="price_meter_country" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_country_sell_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMeterMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minMeterCountryPrice; $c<=$_maxMeterCountryPrice;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_country" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_country_sell_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMeterMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$_minMeterCountryPrice; $c<=$_maxMeterCountryPrice;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_country" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_country_sell_var && $priceAll_country_sell_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">Цена за все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">Цена за м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_country_rent_var?print'style="display:none"':print'style="display:block"';?> class="price_block country rent">
						<div class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$mult = 1000000;
								$name_type = 'млн';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_country_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCountryRent = '<li><a class="min" data-name_select="pricecountry_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_country_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCountryRent = '<li><a class="max" data-name_select="pricecountry_rent" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_country_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCountryRent = '<li><a class="min max" data-name_select="pricecountry_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_country_rent_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minPriceCountryRent; $c<=$_maxPriceCountryRent;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecountry_rent" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_country_rent_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$_minPriceCountryRent; $c<=$_maxPriceCountryRent;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecountry_rent" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_country_rent_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose night">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$mult = 1000;
								$name_type = 'тыс';
								$priceChangeCountryRent = '';
								if($arrayLinkSearch['priceNightMin'] && !$arrayLinkSearch['priceNightMax'] && $priceAll_country_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceNightMin']/$mult;
									$priceChangeCountryRent = '<li><a class="min" data-name_select="pricecountry_rent" data-name="priceNightMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceNightMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceNightMin'] && $arrayLinkSearch['priceNightMax'] && $priceAll_country_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceNightMax']/$mult;
									$priceChangeCountryRent = '<li><a class="max" data-name_select="pricecountry_rent" data-name="priceNightMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceNightMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceNightMin'] && $arrayLinkSearch['priceNightMax'] && $priceAll_country_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceNightMin']/$mult;
									$c2 = $arrayLinkSearch['priceNightMax']/$mult;
									$priceChangeCountryRent = '<li><a class="min max" data-name_select="pricecountry_rent" data-name="priceNightMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceNightMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_country_rent_var && $arrayLinkSearch['priceNightMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceNightMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceNightMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceNightMin'].'"';
								}
								?>
								<input type="hidden" name="priceNightMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceNightMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$min_price_country_rent; $c<=$max_price_country_rent;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecountry_rent" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_price_country_rent){
												break;
											}
											$c = $c+1;
											if($c>$max_price_country_rent){
												$c = $max_price_country_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_country_rent_var && $arrayLinkSearch['priceNightMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceNightMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceNightMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceNightMax'].'"';
								}
								?>
								<input type="hidden" name="priceNightMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceNightMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_price_country_rent; $c<=$max_price_country_rent;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecountry_rent" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$max_price_country_rent){
												break;
											}
											$c = $c+1;
											if($c>$max_price_country_rent){
												$c = $max_price_country_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'руб/мес';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_country_rent_var && $priceAll_country_rent_var=='night'){
								$nameParams = 'руб/сутки';
								$valueParams = ' value="night"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div style="width:90px" class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">руб/мес</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="night">руб/сутки</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_commercial_sell_var && $type_commer_commercial_sell_var!=1?print'style="display:none"':print'style="display:block"';?> class="price_block commercial sell type_commer_1">
						<div <?$priceAll_commercial_sell_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCommercialSell = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCommercialSell = '<li><a class="min" data-name_select="pricecommercial" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="max" data-name_select="pricecommercial" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="min max" data-name_select="pricecommercial" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?																	
/* 										for($c=$_minPriceCommercialTypeCommer_1; $c<=$_maxPriceCommercialTypeCommer_1;){
											$current = "";
											if($c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommer_1){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 4 000 000." data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 8 000 000." data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommer_1){
												$c = $_maxPriceCommercialTypeCommer_1;
											}
										}
 */										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
/* 										for($c=$_minPriceCommercialTypeCommer_1; $c<=$_maxPriceCommercialTypeCommer_1;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommer_1){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 4 000 000" data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 8 000 000" data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommer_1){
												$c = $_maxPriceCommercialTypeCommer_1;
											}
										}
 */										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_commercial_sell_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCommercialSell = '<li><a class="min" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="max" data-name_select="price_meter_commercial" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="min max" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMeterMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
/* 										for($c=$_minMeterCommercialTypeCommer_1; $c<=$_maxMeterCommercialTypeCommer_1;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
 */										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMeterMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
/* 										for($c=$_minMeterCommercialTypeCommer_1; $c<=$_maxMeterCommercialTypeCommer_1;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
 */										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_commercial_sell_var && $priceAll_commercial_sell_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">Цена за все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">Цена за м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_commercial_sell_var || $priceAll_commercial_sell_var && $type_commer_commercial_sell_var!=2?print'style="display:none"':print'style="display:block"';?> class="price_block commercial sell type_commer_2">
						<div <?$priceAll_commercial_sell_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCommercialSell = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCommercialSell = '<li><a class="min" data-name_select="pricecommercial" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="max" data-name_select="pricecommercial" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="min max" data-name_select="pricecommercial" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
/* 										for($c=$_minPriceCommercialTypeCommer_2; $c<=$_maxPriceCommercialTypeCommer_2;){
											$current = "";
											if($c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommer_2){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 4 000 000." data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 8 000 000." data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommer_2){
												$c = $_maxPriceCommercialTypeCommer_2;
											}
										}
 */										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
/* 										for($c=$_minPriceCommercialTypeCommer_2; $c<=$_maxPriceCommercialTypeCommer_2;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommer_2){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 4 000 000" data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 8 000 000" data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommer_2){
												$c = $_maxPriceCommercialTypeCommer_2;
											}
										}
 */										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_commercial_sell_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCommercialSell = '<li><a class="min" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="max" data-name_select="price_meter_commercial" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="min max" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMeterMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
/* 										for($c=$_minMeterCommercialTypeCommer_2; $c<=$_maxMeterCommercialTypeCommer_2;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
 */										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMeterMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
/* 										for($c=$_minMeterCommercialTypeCommer_2; $c<=$_maxMeterCommercialTypeCommer_2;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
 */										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_commercial_sell_var && $priceAll_commercial_sell_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">Цена за все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">Цена за м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_commercial_sell_var || $priceAll_commercial_sell_var && $type_commer_commercial_sell_var!=3?print'style="display:none"':print'style="display:block"';?> class="price_block commercial sell type_commer_3">
						<div <?$priceAll_commercial_sell_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCommercialSell = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCommercialSell = '<li><a class="min" data-name_select="pricecommercial" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="max" data-name_select="pricecommercial" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="min max" data-name_select="pricecommercial" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
/* 										for($c=$_minPriceCommercialTypeCommer_3; $c<=$_maxPriceCommercialTypeCommer_3;){
											$current = "";
											if($c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommer_3){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 4 000 000." data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 8 000 000." data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommer_3){
												$c = $_maxPriceCommercialTypeCommer_3;
											}
										}
 */										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
/* 										for($c=$_minPriceCommercialTypeCommer_3; $c<=$_maxPriceCommercialTypeCommer_3;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommer_3){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 4 000 000" data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 8 000 000" data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommer_3){
												$c = $_maxPriceCommercialTypeCommer_3;
											}
										}
 */										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_commercial_sell_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCommercialSell = '<li><a class="min" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="max" data-name_select="price_meter_commercial" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="min max" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMeterMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
/* 										for($c=$_minMeterCommercialTypeCommer_3; $c<=$_maxMeterCommercialTypeCommer_3;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
 */										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMeterMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
/* 										for($c=$_minMeterCommercialTypeCommer_3; $c<=$_maxMeterCommercialTypeCommer_3;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
 */										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_commercial_sell_var && $priceAll_commercial_sell_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">Цена за все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">Цена за м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_commercial_sell_var || $priceAll_commercial_sell_var && $type_commer_commercial_sell_var!=4?print'style="display:none"':print'style="display:block"';?> class="price_block commercial sell type_commer_4">
						<div <?$priceAll_commercial_sell_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCommercialSell = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCommercialSell = '<li><a class="min" data-name_select="pricecommercial" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="max" data-name_select="pricecommercial" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="min max" data-name_select="pricecommercial" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
/* 										for($c=$_minPriceCommercialTypeCommer_4; $c<=$_maxPriceCommercialTypeCommer_4;){
											$current = "";
											if($c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommer_4){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 4 000 000." data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 8 000 000." data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommer_4){
												$c = $_maxPriceCommercialTypeCommer_4;
											}
										}
 */										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
/* 										for($c=$_minPriceCommercialTypeCommer_4; $c<=$_maxPriceCommercialTypeCommer_4;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommer_4){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 4 000 000" data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 8 000 000" data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommer_4){
												$c = $_maxPriceCommercialTypeCommer_4;
											}
										}
 */										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_commercial_sell_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCommercialSell = '<li><a class="min" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="max" data-name_select="price_meter_commercial" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="min max" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMeterMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minMeterCommercialTypeCommer_4; $c<=$_maxMeterCommercialTypeCommer_4;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMeterMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$_minMeterCommercialTypeCommer_4; $c<=$_maxMeterCommercialTypeCommer_4;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_commercial_sell_var && $priceAll_commercial_sell_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">Цена за все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">Цена за м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_commercial_sell_var || $priceAll_commercial_sell_var && $type_commer_commercial_sell_var!=5?print'style="display:none"':print'style="display:block"';?> class="price_block commercial sell type_commer_5">
						<div <?$priceAll_commercial_sell_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCommercialSell = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCommercialSell = '<li><a class="min" data-name_select="pricecommercial" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="max" data-name_select="pricecommercial" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="min max" data-name_select="pricecommercial" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
/* 										for($c=$_minPriceCommercialTypeCommer_5; $c<=$_maxPriceCommercialTypeCommer_5;){
											$current = "";
											if($c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommer_5){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 4 000 000." data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 8 000 000." data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommer_5){
												$c = $_maxPriceCommercialTypeCommer_5;
											}
										}
 */										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
/* 										for($c=$_minPriceCommercialTypeCommer_5; $c<=$_maxPriceCommercialTypeCommer_5;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommer_5){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 4 000 000" data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 8 000 000" data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommer_5){
												$c = $_maxPriceCommercialTypeCommer_5;
											}
										}
 */										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_commercial_sell_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCommercialSell = '<li><a class="min" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="max" data-name_select="price_meter_commercial" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="min max" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMeterMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minMeterCommercialTypeCommer_5; $c<=$_maxMeterCommercialTypeCommer_5;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMeterMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$_minMeterCommercialTypeCommer_5; $c<=$_maxMeterCommercialTypeCommer_5;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_commercial_sell_var && $priceAll_commercial_sell_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">Цена за все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">Цена за м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_commercial_sell_var || $priceAll_commercial_sell_var && $type_commer_commercial_sell_var!=6?print'style="display:none"':print'style="display:block"';?> class="price_block commercial sell type_commer_6">
						<div <?$priceAll_commercial_sell_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCommercialSell = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCommercialSell = '<li><a class="min" data-name_select="pricecommercial" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="max" data-name_select="pricecommercial" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="min max" data-name_select="pricecommercial" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
/* 										for($c=$_minPriceCommercialTypeCommer_6; $c<=$_maxPriceCommercialTypeCommer_6;){
											$current = "";
											if($c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommer_6){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 4 000 000." data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 8 000 000." data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommer_6){
												$c = $_maxPriceCommercialTypeCommer_6;
											}
										}
 */										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
/* 										for($c=$_minPriceCommercialTypeCommer_6; $c<=$_maxPriceCommercialTypeCommer_6;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommer_6){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 4 000 000" data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 8 000 000" data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommer_6){
												$c = $_maxPriceCommercialTypeCommer_6;
											}
										}
 */										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_commercial_sell_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCommercialSell = '<li><a class="min" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="max" data-name_select="price_meter_commercial" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="min max" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMeterMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minMeterCommercialTypeCommer_6; $c<=$_maxMeterCommercialTypeCommer_6;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMeterMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$_minMeterCommercialTypeCommer_6; $c<=$_maxMeterCommercialTypeCommer_6;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_commercial_sell_var && $priceAll_commercial_sell_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">Цена за все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">Цена за м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_commercial_sell_var || $priceAll_commercial_sell_var && $type_commer_commercial_sell_var!=7?print'style="display:none"':print'style="display:block"';?> class="price_block commercial sell type_commer_7">
						<div <?$priceAll_commercial_sell_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCommercialSell = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCommercialSell = '<li><a class="min" data-name_select="pricecommercial" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="max" data-name_select="pricecommercial" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="min max" data-name_select="pricecommercial" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
/* 										for($c=$_minPriceCommercialTypeCommer_7; $c<=$_maxPriceCommercialTypeCommer_7;){
											$current = "";
											if($c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommer_7){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 4 000 000." data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 8 000 000." data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommer_7){
												$c = $_maxPriceCommercialTypeCommer_7;
											}
										}
 */										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
/* 										for($c=$_minPriceCommercialTypeCommer_7; $c<=$_maxPriceCommercialTypeCommer_7;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommer_7){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 4 000 000" data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 8 000 000" data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommer_7){
												$c = $_maxPriceCommercialTypeCommer_7;
											}
										}
 */										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_commercial_sell_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCommercialSell = '<li><a class="min" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="max" data-name_select="price_meter_commercial" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="min max" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMeterMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minMeterCommercialTypeCommer_7; $c<=$_maxMeterCommercialTypeCommer_7;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMeterMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$_minMeterCommercialTypeCommer_7; $c<=$_maxMeterCommercialTypeCommer_7;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_commercial_sell_var && $priceAll_commercial_sell_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">Цена за все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">Цена за м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_commercial_sell_var || $priceAll_commercial_sell_var && $type_commer_commercial_sell_var!=8?print'style="display:none"':print'style="display:block"';?> class="price_block commercial sell type_commer_8">
						<div <?$priceAll_commercial_sell_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCommercialSell = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCommercialSell = '<li><a class="min" data-name_select="pricecommercial" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="max" data-name_select="pricecommercial" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="min max" data-name_select="pricecommercial" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
/* 										for($c=$_minPriceCommercialTypeCommer_8; $c<=$_maxPriceCommercialTypeCommer_8;){
											$current = "";
											if($c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommer_8){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 4 000 000." data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 8 000 000." data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommer_8){
												$c = $_maxPriceCommercialTypeCommer_8;
											}
										}
 */										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
/* 										for($c=$_minPriceCommercialTypeCommer_8; $c<=$_maxPriceCommercialTypeCommer_8;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommer_8){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 4 000 000" data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial" data-tags="от 8 000 000" data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommer_8){
												$c = $_maxPriceCommercialTypeCommer_8;
											}
										}
 */										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_commercial_sell_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCommercialSell = '<li><a class="min" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="max" data-name_select="price_meter_commercial" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_sell_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialSell = '<li><a class="min max" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMeterMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minMeterCommercialTypeCommer_8; $c<=$_maxMeterCommercialTypeCommer_8;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_sell_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMeterMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$_minMeterCommercialTypeCommer_8; $c<=$_maxMeterCommercialTypeCommer_8;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_commercial_sell_var && $priceAll_commercial_sell_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">Цена за все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">Цена за м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					
					<div <?!$priceAll_commercial_rent_var && $type_commer_commercial_rent_var!=1?print'style="display:none"':print'style="display:block"';?> class="price_block commercial rent type_commer_1">
						<div <?$priceAll_commercial_rent_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCommercialRent = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCommercialRent = '<li><a class="min" data-name_select="pricecommercial_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="max" data-name_select="pricecommercial_rent" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="min max" data-name_select="pricecommercial_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?																	
										for($c=$_minPriceCommercialTypeCommerRent_1; $c<=$_maxPriceCommercialTypeCommerRent_1;){
											$current = "";
											if($c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommerRent_1){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 4 000 000." data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 8 000 000." data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommerRent_1){
												$c = $_maxPriceCommercialTypeCommerRent_1;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
										for($c=$_minPriceCommercialTypeCommerRent_1; $c<=$_maxPriceCommercialTypeCommerRent_1;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommerRent_1){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 4 000 000" data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 8 000 000" data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommerRent_1){
												$c = $_maxPriceCommercialTypeCommerRent_1;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_commercial_rent_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCommercialRent = '<li><a class="min" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="max" data-name_select="price_meter_commercial" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="min max" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMeterMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minMeterCommercialTypeCommer_1; $c<=$_maxMeterCommercialTypeCommer_1;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMeterMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$_minMeterCommercialTypeCommer_1; $c<=$_maxMeterCommercialTypeCommer_1;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_commercial_rent_var && $priceAll_commercial_rent_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">Цена за все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">Цена за м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_commercial_rent_var || $priceAll_commercial_rent_var && $type_commer_commercial_rent_var!=2?print'style="display:none"':print'style="display:block"';?> class="price_block commercial rent type_commer_2">
						<div <?$priceAll_commercial_rent_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCommercialRent = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCommercialRent = '<li><a class="min" data-name_select="pricecommercial_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="max" data-name_select="pricecommercial_rent" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="min max" data-name_select="pricecommercial_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minPriceCommercialTypeCommerRent_2; $c<=$_maxPriceCommercialTypeCommerRent_2;){
											$current = "";
											if($c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommerRent_2){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 4 000 000." data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 8 000 000." data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommerRent_2){
												$c = $_maxPriceCommercialTypeCommerRent_2;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
										for($c=$_minPriceCommercialTypeCommerRent_2; $c<=$_maxPriceCommercialTypeCommerRent_2;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommerRent_2){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 4 000 000" data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 8 000 000" data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommerRent_2){
												$c = $_maxPriceCommercialTypeCommerRent_2;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_commercial_rent_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCommercialRent = '<li><a class="min" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="max" data-name_select="price_meter_commercial" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="min max" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMeterMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minMeterCommercialTypeCommer_2; $c<=$_maxMeterCommercialTypeCommer_2;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMeterMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$_minMeterCommercialTypeCommer_2; $c<=$_maxMeterCommercialTypeCommer_2;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_commercial_rent_var && $priceAll_commercial_rent_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">Цена за все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">Цена за м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_commercial_rent_var || $priceAll_commercial_rent_var && $type_commer_commercial_rent_var!=3?print'style="display:none"':print'style="display:block"';?> class="price_block commercial rent type_commer_3">
						<div <?$priceAll_commercial_rent_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCommercialRent = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCommercialRent = '<li><a class="min" data-name_select="pricecommercial_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="max" data-name_select="pricecommercial_rent" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="min max" data-name_select="pricecommercial_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minPriceCommercialTypeCommerRent_3; $c<=$_maxPriceCommercialTypeCommerRent_3;){
											$current = "";
											if($c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommerRent_3){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 4 000 000." data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 8 000 000." data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommerRent_3){
												$c = $_maxPriceCommercialTypeCommerRent_3;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
										for($c=$_minPriceCommercialTypeCommerRent_3; $c<=$_maxPriceCommercialTypeCommerRent_3;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommerRent_3){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 4 000 000" data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 8 000 000" data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommerRent_3){
												$c = $_maxPriceCommercialTypeCommerRent_3;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_commercial_rent_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCommercialRent = '<li><a class="min" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="max" data-name_select="price_meter_commercial" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="min max" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMeterMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minMeterCommercialTypeCommer_3; $c<=$_maxMeterCommercialTypeCommer_3;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMeterMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$_minMeterCommercialTypeCommer_3; $c<=$_maxMeterCommercialTypeCommer_3;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_commercial_rent_var && $priceAll_commercial_rent_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">Цена за все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">Цена за м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_commercial_rent_var || $priceAll_commercial_rent_var && $type_commer_commercial_rent_var!=4?print'style="display:none"':print'style="display:block"';?> class="price_block commercial rent type_commer_4">
						<div <?$priceAll_commercial_rent_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCommercialRent = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCommercialRent = '<li><a class="min" data-name_select="pricecommercial_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="max" data-name_select="pricecommercial_rent" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="min max" data-name_select="pricecommercial_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minPriceCommercialTypeCommerRent_4; $c<=$_maxPriceCommercialTypeCommerRent_4;){
											$current = "";
											if($c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommerRent_4){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 4 000 000." data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 8 000 000." data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommerRent_4){
												$c = $_maxPriceCommercialTypeCommerRent_4;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
										for($c=$_minPriceCommercialTypeCommerRent_4; $c<=$_maxPriceCommercialTypeCommerRent_4;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommerRent_4){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 4 000 000" data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 8 000 000" data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommerRent_4){
												$c = $_maxPriceCommercialTypeCommerRent_4;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_commercial_rent_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCommercialRent = '<li><a class="min" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="max" data-name_select="price_meter_commercial" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="min max" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMeterMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minMeterCommercialTypeCommer_4; $c<=$_maxMeterCommercialTypeCommer_4;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMeterMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$_minMeterCommercialTypeCommer_4; $c<=$_maxMeterCommercialTypeCommer_4;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_commercial_rent_var && $priceAll_commercial_rent_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">Цена за все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">Цена за м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_commercial_rent_var || $priceAll_commercial_rent_var && $type_commer_commercial_rent_var!=5?print'style="display:none"':print'style="display:block"';?> class="price_block commercial rent type_commer_5">
						<div <?$priceAll_commercial_rent_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCommercialRent = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCommercialRent = '<li><a class="min" data-name_select="pricecommercial_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="max" data-name_select="pricecommercial_rent" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="min max" data-name_select="pricecommercial_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minPriceCommercialTypeCommerRent_5; $c<=$_maxPriceCommercialTypeCommerRent_5;){
											$current = "";
											if($c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommerRent_5){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 4 000 000." data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 8 000 000." data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommerRent_5){
												$c = $_maxPriceCommercialTypeCommerRent_5;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
										for($c=$_minPriceCommercialTypeCommerRent_5; $c<=$_maxPriceCommercialTypeCommerRent_5;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommerRent_5){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 4 000 000" data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 8 000 000" data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommerRent_5){
												$c = $_maxPriceCommercialTypeCommerRent_5;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_commercial_rent_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCommercialRent = '<li><a class="min" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="max" data-name_select="price_meter_commercial" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="min max" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMeterMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minMeterCommercialTypeCommer_5; $c<=$_maxMeterCommercialTypeCommer_5;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMeterMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$_minMeterCommercialTypeCommer_5; $c<=$_maxMeterCommercialTypeCommer_5;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_commercial_rent_var && $priceAll_commercial_rent_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">Цена за все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">Цена за м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_commercial_rent_var || $priceAll_commercial_rent_var && $type_commer_commercial_rent_var!=6?print'style="display:none"':print'style="display:block"';?> class="price_block commercial rent type_commer_6">
						<div <?$priceAll_commercial_rent_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCommercialRent = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCommercialRent = '<li><a class="min" data-name_select="pricecommercial_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="max" data-name_select="pricecommercial_rent" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="min max" data-name_select="pricecommercial_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minPriceCommercialTypeCommerRent_6; $c<=$_maxPriceCommercialTypeCommerRent_6;){
											$current = "";
											if($c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommerRent_6){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 4 000 000." data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 8 000 000." data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommerRent_6){
												$c = $_maxPriceCommercialTypeCommerRent_6;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
										for($c=$_minPriceCommercialTypeCommerRent_6; $c<=$_maxPriceCommercialTypeCommerRent_6;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommerRent_6){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 4 000 000" data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 8 000 000" data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommerRent_6){
												$c = $_maxPriceCommercialTypeCommerRent_6;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_commercial_rent_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCommercialRent = '<li><a class="min" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="max" data-name_select="price_meter_commercial" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="min max" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMeterMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minMeterCommercialTypeCommer_6; $c<=$_maxMeterCommercialTypeCommer_6;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMeterMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$_minMeterCommercialTypeCommer_6; $c<=$_maxMeterCommercialTypeCommer_6;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_commercial_rent_var && $priceAll_commercial_rent_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">Цена за все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">Цена за м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_commercial_rent_var || $priceAll_commercial_rent_var && $type_commer_commercial_rent_var!=7?print'style="display:none"':print'style="display:block"';?> class="price_block commercial rent type_commer_7">
						<div <?$priceAll_commercial_rent_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCommercialRent = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCommercialRent = '<li><a class="min" data-name_select="pricecommercial_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="max" data-name_select="pricecommercial_rent" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="min max" data-name_select="pricecommercial_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$_minPriceCommercialTypeCommerRent_7; $c<=$_maxPriceCommercialTypeCommerRent_7;){
											$current = "";
											if($c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommerRent_7){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 4 000 000." data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 8 000 000." data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommerRent_7){
												$c = $_maxPriceCommercialTypeCommerRent_7;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
										for($c=$_minPriceCommercialTypeCommerRent_7; $c<=$_maxPriceCommercialTypeCommerRent_7;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommerRent_7){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 4 000 000" data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 8 000 000" data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommerRent_7){
												$c = $_maxPriceCommercialTypeCommerRent_7;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_commercial_rent_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCommercialRent = '<li><a class="min" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="max" data-name_select="price_meter_commercial" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="min max" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMeterMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minMeterCommercialTypeCommer_7; $c<=$_maxMeterCommercialTypeCommer_7;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMeterMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$_minMeterCommercialTypeCommer_7; $c<=$_maxMeterCommercialTypeCommer_7;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_commercial_rent_var && $priceAll_commercial_rent_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">Цена за все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">Цена за м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div <?!$priceAll_commercial_rent_var || $priceAll_commercial_rent_var && $type_commer_commercial_rent_var!=8?print'style="display:none"':print'style="display:block"';?> class="price_block commercial rent type_commer_8">
						<div <?$priceAll_commercial_rent_var=='meters'?print'style="display:none"':print'style="display:block"'?> class="prices_choose all">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'млн';
								$mult = 1000000;
								$priceChangeCommercialRent = '';
								if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$priceChangeCommercialRent = '<li><a class="min" data-name_select="pricecommercial_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="max" data-name_select="pricecommercial_rent" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMin']/$mult;
									$c2 = $arrayLinkSearch['priceMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="min max" data-name_select="pricecommercial_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								}
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minPriceCommercialTypeCommerRent_8; $c<=$_maxPriceCommercialTypeCommerRent_8;){
											$current = "";
											if($c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommerRent_8){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 4 000 000." data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 8 000 000." data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommerRent_8){
												$c = $_maxPriceCommercialTypeCommerRent_8;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								}
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?									
										for($c=$_minPriceCommercialTypeCommerRent_8; $c<=$_maxPriceCommercialTypeCommerRent_8;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											if($c==$_maxPriceCommercialTypeCommerRent_8){
												break;
											}
											if((string)$c==(string)3.9){
												$current = "";
												if((string)$currentParams==(string)4){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 4 000 000" data-type="min" data-id="'.(4*$mult).'">4 '.$name_type.'.</a></li>';
											}
											if((string)$c==(string)7.8){
												$current = "";
												if((string)$currentParams==(string)8){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от 8 000 000" data-type="min" data-id="'.(8*$mult).'">8 '.$name_type.'.</a></li>';
											}
											if($c<4){
												$c = $c+0.1;
											}
											if($c>=4 && $c<8){
												$c = $c+0.2;
											}
											if($c>=8 && $c<11){
												$c = $c+0.3;
											}
											if($c>=11 && $c<=20){
												$c = $c+0.5;
											}
											if($c>=20 && $c<=30){
												$c = $c+1;
											}
											if($c>=30 && $c<=50){
												$c = $c+2;
											}
											if($c>=50 && $c<=100){
												$c = $c+5;
											}
											if($c>=100 && $c<=300){
												$c = $c+10;
											}
											if($c>=300 && $c<=500){
												$c = $c+30;
											}
											if($c>=500){
												$c = $c+50;
											}
											if($c>$_maxPriceCommercialTypeCommerRent_8){
												$c = $_maxPriceCommercialTypeCommerRent_8;
											}
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_commercial_rent_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose meters">
							<!--<div class="label_select">Цена</div>-->
							<div class="select_block price ch_price">
								<?
								$name_type = 'тыс';
								$mult = 1000;
								if($arrayLinkSearch['priceMeterMin'] && !$arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //только цена "от"
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$priceChangeCommercialRent = '<li><a class="min" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //только цена "до"
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="max" data-name_select="price_meter_commercial" data-name="priceMeterMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['priceMeterMin'] && $arrayLinkSearch['priceMeterMax'] && $priceAll_commercial_rent_var){ //промежуток цен
									$c1 = $arrayLinkSearch['priceMeterMin']/$mult;
									$c2 = $arrayLinkSearch['priceMeterMax']/$mult;
									$priceChangeCommercialRent = '<li><a class="min max" data-name_select="price_meter_commercial" data-name="priceMeterMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMeterMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMeterMin']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMin']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMin']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMin'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMeterMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										for($c=$_minMeterCommercialTypeCommer_8; $c<=$_maxMeterCommercialTypeCommer_8;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMeterMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['priceMeterMax']/$mult).' '.$name_type.'.';
									$currentParams = $arrayLinkSearch['priceMeterMax']/$mult;
									$valueParams = ' value="'.$arrayLinkSearch['priceMeterMax'].'"';
								}
								?>
								<input type="hidden" name="priceMeterMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMeterMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$_minMeterCommercialTypeCommer_8; $c<=$_maxMeterCommercialTypeCommer_8;){
											$current = "";
											if((string)$c==(string)$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a href="javascript:void(0)" data-name="price_meter_commercial" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											$c = $c+10;
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							$nameParams = 'За все';
							$valueParams = ' value="all"';
							$allCurrent = ' class="current"';
							$meterCurrent = '';
							if($priceAll_commercial_rent_var && $priceAll_commercial_rent_var=='meters'){
								$nameParams = 'За м2';
								$valueParams = ' value="meters"';
								$allCurrent = '';
								$meterCurrent = ' class="current"';
							}
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<!--<li class="name">Цена за:</li>-->
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">Цена за все</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="meters">Цена за м<sup>2</sup></a></li>
								</ul>
							</div>
						</div>
					</div>					
					
					
					<!--<div <?!$priceAll_commercial_rent_var?print'style="display:none"':print'style="display:block"';?> class="price_block commercial rent">
						<div class="prices_choose all">
							<div class="select_block price ch_price">
								<?
								// $mult = 1000000;
								// $name_type = 'млн';
								// $priceChangeCommercialRent = '';
								// if($arrayLinkSearch['priceMin'] && !$arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //только цена "от"
									// $c1 = $arrayLinkSearch['priceMin']/$mult;
									// $priceChangeCommercialRent = '<li><a class="min" data-name_select="pricecommercial_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								// }
								// if(!$arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //только цена "до"
									// $c2 = $arrayLinkSearch['priceMax']/$mult;
									// $priceChangeCommercialRent = '<li><a class="max" data-name_select="pricecommercial_rent" data-name="priceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								// }
								// if($arrayLinkSearch['priceMin'] && $arrayLinkSearch['priceMax'] && $priceAll_commercial_rent_var){ //промежуток цен
									// $c1 = $arrayLinkSearch['priceMin']/$mult;
									// $c2 = $arrayLinkSearch['priceMax']/$mult;
									// $priceChangeCommercialRent = '<li><a class="min max" data-name_select="pricecommercial_rent" data-name="priceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								// }

								// $nameParams = 'От';
								// $valueParams = '';
								// $currentParams = '';
								// if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMin']){
									// $nameParams = str_replace(".", ",", $arrayLinkSearch['priceMin']/$mult).' '.$name_type.'.';
									// $currentParams = $arrayLinkSearch['priceMin']/$mult;
									// $valueParams = ' value="'.$arrayLinkSearch['priceMin'].'"';
								// }
								?>
								<input type="hidden" name="priceMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										// for($c=$_minPriceCommercialRent; $c<=$_maxPriceCommercialRent;){
											// $current = "";
											// if((string)$c==(string)$currentParams){
												// $current = ' class="current"';
											// }
											// echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											// $c = $c+1;
										// }
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								// $nameParams = 'До';
								// $valueParams = '';
								// $currentParams = '';
								// if($priceAll_commercial_rent_var && $arrayLinkSearch['priceMax']){
									// $nameParams = str_replace(".", ",", $arrayLinkSearch['priceMax']/$mult).' '.$name_type.'.';
									// $currentParams = $arrayLinkSearch['priceMax']/$mult;
									// $valueParams = ' value="'.$arrayLinkSearch['priceMax'].'"';
								// }
								?>
								<input type="hidden" name="priceMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										// for($c=$_minPriceCommercialRent; $c<=$_maxPriceCommercialRent;){
											// $current = "";
											// if((string)$c==(string)$currentParams){
												// $current = ' class="current"';
											// }
											// echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											// $c = $c+1;
										// }
										?>
									</ul>
								</div>
							</div>
						</div>
						<div <?$priceAll_commercial_rent_var=='all'?print'style="display:none"':print'style="display:block"'?> class="prices_choose night">
							<div class="select_block price ch_price">
								<?
								// $mult = 1000;
								// $name_type = 'тыс';
								// $priceChangeCountryRent = '';
								// if($arrayLinkSearch['priceNightMin'] && !$arrayLinkSearch['priceNightMax'] && $priceAll_commercial_rent_var){ //только цена "от"
									// $c1 = $arrayLinkSearch['priceNightMin']/$mult;
									// $priceChangeCountryRent = '<li><a class="min" data-name_select="pricecommercial_rent" data-name="priceNightMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceNightMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								// }
								// if(!$arrayLinkSearch['priceNightMin'] && $arrayLinkSearch['priceNightMax'] && $priceAll_commercial_rent_var){ //только цена "до"
									// $c2 = $arrayLinkSearch['priceNightMax']/$mult;
									// $priceChangeCountryRent = '<li><a class="max" data-name_select="pricecommercial_rent" data-name="priceNightMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceNightMax'].'" href="javascript:void(0)">до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								// }
								// if($arrayLinkSearch['priceNightMin'] && $arrayLinkSearch['priceNightMax'] && $priceAll_commercial_rent_var){ //промежуток цен
									// $c1 = $arrayLinkSearch['priceNightMin']/$mult;
									// $c2 = $arrayLinkSearch['priceNightMax']/$mult;
									// $priceChangeCountryRent = '<li><a class="min max" data-name_select="pricecommercial_rent" data-name="priceNightMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['priceNightMin'].'" href="javascript:void(0)">от '.price_cell(str_replace(".", ",", $c1*$mult),0).' до '.price_cell(str_replace(".", ",", $c2*$mult),0).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								// }

								// $nameParams = 'От';
								// $valueParams = '';
								// $currentParams = '';
								// if($priceAll_commercial_rent_var && $arrayLinkSearch['priceNightMin']){
									// $nameParams = str_replace(".", ",", $arrayLinkSearch['priceNightMin']/$mult).' '.$name_type.'.';
									// $currentParams = $arrayLinkSearch['priceNightMin']/$mult;
									// $valueParams = ' value="'.$arrayLinkSearch['priceNightMin'].'"';
								// }
								?>
								<input type="hidden" name="priceNightMin"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена От" type="text" value="<?=$arrayLinkSearch['priceNightMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										// for($c=$min_price_country_rent; $c<=$max_price_country_rent;){
											// $current = "";
											// if((string)$c==(string)$currentParams){
												// $current = ' class="current"';
											// }
											// echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="от '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="min" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											// if($c==$max_price_country_rent){
												// break;
											// }
											// $c = $c+1;
											// if($c>$max_price_country_rent){
												// $c = $max_price_country_rent;
											// }
										// }
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price ch_price">
								<?
								// $nameParams = 'До';
								// $valueParams = '';
								// $currentParams = '';
								// if($priceAll_commercial_rent_var && $arrayLinkSearch['priceNightMax']){
									// $nameParams = str_replace(".", ",", $arrayLinkSearch['priceNightMax']/$mult).' '.$name_type.'.';
									// $currentParams = $arrayLinkSearch['priceNightMax']/$mult;
									// $valueParams = ' value="'.$arrayLinkSearch['priceNightMax'].'"';
								// }
								?>
								<input type="hidden" name="priceNightMax"<?=$valueParams?>>
								<div style="width:100px" class="choosed_block"><input style="display:none" maxlength="17" placeholder="цена До" type="text" value="<?=$arrayLinkSearch['priceNightMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										// for($c=$min_price_country_rent; $c<=$max_price_country_rent;){
											// $current = "";
											// if((string)$c==(string)$currentParams){
												// $current = ' class="current"';
											// }
											// echo '<li'.$current.'><a href="javascript:void(0)" data-name="pricecommercial_rent" data-tags="до '.price_cell(str_replace(".", ",", $c*$mult),0).'" data-type="max" data-id="'.($c*$mult).'">'.str_replace(".", ",", $c).' '.$name_type.'.</a></li>';
											// if($c==$max_price_country_rent){
												// break;
											// }
											// $c = $c+1;
											// if($c>$max_price_country_rent){
												// $c = $max_price_country_rent;
											// }
										// }
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="gap"></div>
						<div class="select_block price all">
							<?
							// $nameParams = 'руб/мес';
							// $valueParams = ' value="all"';
							// $allCurrent = ' class="current"';
							// $meterCurrent = '';
							// if($priceAll_commercial_rent_var && $priceAll_commercial_rent_var=='night'){
								// $nameParams = 'руб/сутки';
								// $valueParams = ' value="night"';
								// $allCurrent = '';
								// $meterCurrent = ' class="current"';
							// }
							?>
							<input type="hidden" name="priceAll"<?=$valueParams?>>
							<div style="width:90px" class="choosed_block"><?=$nameParams?></div>
							<div class="scrollbar-inner">
								<ul>
									<li<?=$allCurrent?>><a href="javascript:void(0)" data-id="all">руб/мес</a></li>
									<li<?=$meterCurrent?>><a href="javascript:void(0)" data-id="night">руб/сутки</a></li>
								</ul>
							</div>
						</div>
					</div>-->
				</div>
				<div <?!$type_new_sell_var || !$estate_new_sell_var?print'style="display:none"':''?> class="change_block_filter new sell">
					<div class="checkbox_block">
						<?
						$ex_rooms_new_sell_var = array();
						$valueInput = '';
						$roomsChangeNewSell = '';
						if(isset($rooms_new_sell_var)){
							$ex_rooms_new_sell_var = explode(',',$rooms_new_sell_var);
							$valueInput = ' value="'.$rooms_new_sell_var.'"';
							$id_room = 0;
							$name_room = '';
							if(in_array(0,$ex_rooms_new_sell_var)){ // Студия
								$roomsChangeNewSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="0" href="javascript:void(0)">Студии<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(1,$ex_rooms_new_sell_var)){ // 1-комн.
								$roomsChangeNewSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="1" href="javascript:void(0)">1-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(2,$ex_rooms_new_sell_var)){ // 2-комн.
								$roomsChangeNewSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="2" href="javascript:void(0)">2-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(3,$ex_rooms_new_sell_var)){ // 3-комн.
								$roomsChangeNewSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="3" href="javascript:void(0)">3-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(4,$ex_rooms_new_sell_var)){ // 4+-комн.
								$roomsChangeNewSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="4" href="javascript:void(0)">4+-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
						}
						?>
						<input type="hidden" name="rooms"<?=$valueInput?>>
						<div class="block_checkbox">
							<ul>
								<li <?in_array(0,$ex_rooms_new_sell_var)?print'class="checked"':''?>><a data-tags="Студии" href="javascript:void(0)" data-id="0"><i class="triangle-bottomleft studio"></i>Студию</a></li>
								<li <?in_array(1,$ex_rooms_new_sell_var)?print'class="checked"':''?>><a data-tags="1-комн." href="javascript:void(0)" data-id="1"><i class="triangle-bottomleft one"></i>1</a></li>
								<li <?in_array(2,$ex_rooms_new_sell_var)?print'class="checked"':''?>><a data-tags="2-комн." href="javascript:void(0)" data-id="2"><i class="triangle-bottomleft two"></i>2</a></li>
								<li <?in_array(3,$ex_rooms_new_sell_var)?print'class="checked"':''?>><a data-tags="3-комн." href="javascript:void(0)" data-id="3"><i class="triangle-bottomleft three"></i>3</a></li>
								<li <?in_array(4,$ex_rooms_new_sell_var)?print'class="checked"':''?>><a data-tags="4+-комн." href="javascript:void(0)" data-id="4"><i class="triangle-bottomleft more"></i>4+</a></li>
								<li class="last">комнатную</li>
							</ul>
						</div>
					</div>
					<!--<div class="checkbox_block">
						<?
						// $valueInput = '';
						// $cessionChangeNewSell = '';
						// if($cession_new_sell_var){
							// $valueInput = ' value="'.$cession_new_sell_var.'"';
							// $cessionChangeNewSell .= '<li><a data-name="cession" data-type="checkbox_select" data-id="1" href="javascript:void(0)">Только по переуступке<span onclick="return delTags(this)" class="close"></span></a></li>';
						// }
						?>
						<input type="hidden" name="cession"<?=$valueInput?>>
						<div class="block_checkbox">
							<ul>
								<li class="vert_line"></li>
								<li <?isset($cession_new_sell_var)?print'class="checked"':''?> class="share"><a data-tags="Только по переуступке" href="javascript:void(0)" data-id="1">Только по переуступке</a></li>
							</ul>
						</div>
					</div>-->
				</div>
				<div <?$type_cession_sell_var?print'style="display:block"':'style="display:none"'?> class="change_block_filter cession sell">
					<div class="checkbox_block">
						<?
						$ex_rooms_cession_sell_var = array();
						$valueInput = '';
						$roomsChangeCessionSell = '';
						if(isset($rooms_cession_sell_var)){
							$ex_rooms_cession_sell_var = explode(',',$rooms_cession_sell_var);
							$valueInput = ' value="'.$rooms_cession_sell_var.'"';
							$id_room = 0;
							$name_room = '';
							if(in_array(0,$ex_rooms_cession_sell_var)){ // Студия
								$roomsChangeCessionSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="0" href="javascript:void(0)">Студии<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(1,$ex_rooms_cession_sell_var)){ // 1-комн.
								$roomsChangeCessionSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="1" href="javascript:void(0)">1-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(2,$ex_rooms_cession_sell_var)){ // 2-комн.
								$roomsChangeCessionSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="2" href="javascript:void(0)">2-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(3,$ex_rooms_cession_sell_var)){ // 3-комн.
								$roomsChangeCessionSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="3" href="javascript:void(0)">3-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(4,$ex_rooms_cession_sell_var)){ // 4+-комн.
								$roomsChangeCessionSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="4" href="javascript:void(0)">4+-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
						}
						?>
						<input type="hidden" name="rooms"<?=$valueInput?>>
						<div class="block_checkbox">
							<ul>
								<li <?in_array(0,$ex_rooms_cession_sell_var)?print'class="checked"':''?>><a data-tags="Студии" href="javascript:void(0)" data-id="0"><i class="triangle-bottomleft studio"></i>Студию</a></li>
								<li <?in_array(1,$ex_rooms_cession_sell_var)?print'class="checked"':''?>><a data-tags="1-комн." href="javascript:void(0)" data-id="1"><i class="triangle-bottomleft one"></i>1</a></li>
								<li <?in_array(2,$ex_rooms_cession_sell_var)?print'class="checked"':''?>><a data-tags="2-комн." href="javascript:void(0)" data-id="2"><i class="triangle-bottomleft two"></i>2</a></li>
								<li <?in_array(3,$ex_rooms_cession_sell_var)?print'class="checked"':''?>><a data-tags="3-комн." href="javascript:void(0)" data-id="3"><i class="triangle-bottomleft three"></i>3</a></li>
								<li <?in_array(4,$ex_rooms_cession_sell_var)?print'class="checked"':''?>><a data-tags="4+-комн." href="javascript:void(0)" data-id="4"><i class="triangle-bottomleft more"></i>4+</a></li>
								<li class="last">комнатную</li>
							</ul>
						</div>
					</div>
				</div>
				<div <?$type_flat_sell_var?print'style="display:block"':'style="display:none"'?> class="change_block_filter flat sell">
					<div class="checkbox_block">
						<?
						$ex_rooms_flat_sell_var = array();
						$valueInput = '';
						$roomsChangeFlatSell = '';
						if(isset($rooms_flat_sell_var)){
							$ex_rooms_flat_sell_var = explode(',',$rooms_flat_sell_var);
							$valueInput = ' value="'.$rooms_flat_sell_var.'"';
							$id_room = 0;
							$name_room = '';
							if(in_array(0,$ex_rooms_flat_sell_var)){ // Студия
								$roomsChangeFlatSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="0" href="javascript:void(0)">Студии<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(1,$ex_rooms_flat_sell_var)){ // 1-комн.
								$roomsChangeFlatSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="1" href="javascript:void(0)">1-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(2,$ex_rooms_flat_sell_var)){ // 2-комн.
								$roomsChangeFlatSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="2" href="javascript:void(0)">2-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(3,$ex_rooms_flat_sell_var)){ // 3-комн.
								$roomsChangeFlatSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="3" href="javascript:void(0)">3-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(4,$ex_rooms_flat_sell_var)){ // 4-комн.
								$roomsChangeFlatSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="4" href="javascript:void(0)">4-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(5,$ex_rooms_flat_sell_var)){ // 5+-комн.
								$roomsChangeFlatSell .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="5" href="javascript:void(0)">5+-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
						}
						?>
						<input type="hidden" name="rooms"<?=$valueInput?>>
						<div class="block_checkbox">
							<ul>
								<li <?in_array(0,$ex_rooms_flat_sell_var)?print'class="checked"':''?>><a data-tags="Студии" href="javascript:void(0)" data-id="0"><i class="triangle-bottomleft studio"></i>Студию</a></li>
								<li <?in_array(1,$ex_rooms_flat_sell_var)?print'class="checked"':''?>><a data-tags="1-комн." href="javascript:void(0)" data-id="1"><i class="triangle-bottomleft one"></i>1</a></li>
								<li <?in_array(2,$ex_rooms_flat_sell_var)?print'class="checked"':''?>><a data-tags="2-комн." href="javascript:void(0)" data-id="2"><i class="triangle-bottomleft two"></i>2</a></li>
								<li <?in_array(3,$ex_rooms_flat_sell_var)?print'class="checked"':''?>><a data-tags="3-комн." href="javascript:void(0)" data-id="3"><i class="triangle-bottomleft three"></i>3</a></li>
								<li <?in_array(4,$ex_rooms_flat_sell_var)?print'class="checked"':''?>><a data-tags="4-комн." href="javascript:void(0)" data-id="4"><i class="triangle-bottomleft more"></i>4</a></li>
								<li <?in_array(5,$ex_rooms_flat_sell_var)?print'class="checked"':''?>><a data-tags="5+-комн." href="javascript:void(0)" data-id="5"><i class="triangle-bottomleft five"></i>5+</a></li>
								<li class="last">комнатную</li>
							</ul>
						</div>
					</div>
					<!--<div style="margin-left:8px;" class="select_block balcony">
						<?
						// $shareChangeFlatSell = '';
						// if(isset($arrayLinkSearch['share']) && $share_flat_sell_var){
							// $c1 = $arrayLinkSearch['share'];
							// if($c1==0){
								// $shareChangeFlatSell = '<li><a class="min" data-name_select="share" data-name="share" data-type="checkbox_block" data-id="'.$arrayLinkSearch['share'].'" href="javascript:void(0)">Нет санузла<span onclick="return delTags(this)" class="close"></span></a></li>';
							// }
							// else {
								// $shareChangeFlatSell = '<li><a class="min" data-name_select="share" data-name="share" data-type="checkbox_block" data-id="'.$arrayLinkSearch['share'].'" href="javascript:void(0)">'.$_TYPE_SHARE[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
							// }
						// }
						
						// $nameParams = 'Доля: не важно';
						// $valueParams = '';
						// $currentParams = '';
						// if($share_flat_sell_var && isset($arrayLinkSearch['share'])){
							// $currentParams = (int)$arrayLinkSearch['share'];
							// $valueParams = ' value="'.$arrayLinkSearch['share'].'"';
						// }
						// $share = '';
						// for($n=0; $n<count($_TYPE_SHARE); $n++){
							// if($n==0){
								// $current = '';
								// $current2 = '';
								// if($currentParams=='' && !isset($arrayLinkSearch['share'])){
									// $current2 = ' class="current"';
									// $nameParams = $_TYPE_SHARE[0];
								// }
								// $share .= '<li'.$current2.'><a href="javascript:void(0)" data-id="">'.$_TYPE_SHARE[0].'</a></li>';
							// }
							// else {
								// $current = '';
								// if($currentParams==$n){
									// $current = ' class="current"';
									// $nameParams = $_TYPE_SHARE[$n];
								// }
								// $share .= '<li'.$current.'><a data-name="share" data-explanation="" data-tags="'.clearValueText($_TYPE_SHARE[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_SHARE[$n].'</a></li>';
							// }
						// }
						// echo '<input type="hidden" name="share"'.$valueParams.'>';
						// echo '<div style="width:138px" class="choosed_block">'.$nameParams.'</div>';
						// echo '<div class="scrollbar-inner">';
						// echo '<ul>';
						// echo $share;
						// echo '</ul>';
						// echo '</div>';
						?>
					</div>-->
					<!--<div class="checkbox_block">
						<?
						// $valueInput = '';
						// $shareChangeFlatSell = '';
						// if($share_flat_sell_var){
							// $valueInput = ' value="'.$share_flat_sell_var.'"';
							// $shareChangeFlatSell .= '<li><a data-name="share" data-type="checkbox_select" data-id="1" href="javascript:void(0)">Доля<span onclick="return delTags(this)" class="close"></span></a></li>';
						// }
						?>
						<input type="hidden" name="share"<?=$valueInput?>>
						<div class="block_checkbox">
							<ul>
								<li class="vert_line"></li>
								<li <?isset($share_flat_sell_var)?print'class="checked"':''?> class="share"><a data-tags="Доля" href="javascript:void(0)" data-id="1">Доля</a></li>
							</ul>
						</div>
					</div>-->
				</div>
				<div <?$type_flat_rent_var?print'style="display:block"':'style="display:none"'?> class="change_block_filter flat rent">
					<div class="checkbox_block">
						<?
						$ex_rooms_flat_rent_var = array();
						$valueInput = '';
						$roomsChangeFlatRent = '';
						if(isset($rooms_flat_rent_var)){
							$ex_rooms_flat_rent_var = explode(',',$rooms_flat_rent_var);
							$valueInput = ' value="'.$rooms_flat_rent_var.'"';
							if(in_array(0,$ex_rooms_flat_rent_var)){ // Студия
								$roomsChangeFlatRent .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="0" href="javascript:void(0)">Студии<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(1,$ex_rooms_flat_rent_var)){ // 1-комн.
								$roomsChangeFlatRent .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="1" href="javascript:void(0)">1-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(2,$ex_rooms_flat_rent_var)){ // 2-комн.
								$roomsChangeFlatRent .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="2" href="javascript:void(0)">2-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(3,$ex_rooms_flat_rent_var)){ // 3-комн.
								$roomsChangeFlatRent .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="3" href="javascript:void(0)">3-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(4,$ex_rooms_flat_rent_var)){ // 4+-комн.
								$roomsChangeFlatRent .= '<li><a data-name="rooms" data-type="checkbox_select" data-id="4" href="javascript:void(0)">4+-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
						}
						?>
						<input type="hidden" name="rooms"<?=$valueInput?>>
						<div class="block_checkbox">
							<ul>
								<li <?in_array(0,$ex_rooms_flat_rent_var)?print'class="checked"':''?>><a data-tags="Студии" href="javascript:void(0)" data-id="0"><i class="triangle-bottomleft studio"></i>Студию</a></li>
								<li <?in_array(1,$ex_rooms_flat_rent_var)?print'class="checked"':''?>><a data-tags="1-комн." href="javascript:void(0)" data-id="1"><i class="triangle-bottomleft one"></i>1</a></li>
								<li <?in_array(2,$ex_rooms_flat_rent_var)?print'class="checked"':''?>><a data-tags="2-комн." href="javascript:void(0)" data-id="2"><i class="triangle-bottomleft two"></i>2</a></li>
								<li <?in_array(3,$ex_rooms_flat_rent_var)?print'class="checked"':''?>><a data-tags="3-комн." href="javascript:void(0)" data-id="3"><i class="triangle-bottomleft three"></i>3</a></li>
								<li <?in_array(4,$ex_rooms_flat_rent_var)?print'class="checked"':''?>><a data-tags="4+-комн." href="javascript:void(0)" data-id="4"><i class="triangle-bottomleft more"></i>4+</a></li>
								<li class="last">комнатную</li>
							</ul>
						</div>
					</div>
				</div>
				<div <?$type_room_sell_var?print'style="display:block"':'style="display:none"'?> class="change_block_filter room sell">
					<div class="checkbox_block">
						<?
						$ex_rooms_flat_room_sell_var = array();
						$valueInput = '';
						$roomsChangeRoomSell = '';
						if(isset($rooms_flat_room_sell_var)){
							$ex_rooms_flat_room_sell_var = explode(',',$rooms_flat_room_sell_var);
							$valueInput = ' value="'.$rooms_flat_room_sell_var.'"';
							$id_room = 0;
							$name_room = '';
							// if(in_array(0,$ex_rooms_flat_room_sell_var)){ // Студия
								// $roomsChangeRoomSell .= '<li><a data-name="rooms_flat" data-type="checkbox_select" data-id="0" href="javascript:void(0)">Студии<span onclick="return delTags(this)" class="close"></span></a></li>';
							// }
							// if(in_array(1,$ex_rooms_flat_room_sell_var)){ // 1-комн.
								// $roomsChangeRoomSell .= '<li><a data-name="rooms_flat" data-type="checkbox_select" data-id="1" href="javascript:void(0)">1-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							// }
							if(in_array(2,$ex_rooms_flat_room_sell_var)){ // 2-комн.
								$roomsChangeRoomSell .= '<li><a data-name="rooms_flat" data-type="checkbox_select" data-id="2" href="javascript:void(0)">в 2-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(3,$ex_rooms_flat_room_sell_var)){ // 3-комн.
								$roomsChangeRoomSell .= '<li><a data-name="rooms_flat" data-type="checkbox_select" data-id="3" href="javascript:void(0)">в 3-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(4,$ex_rooms_flat_room_sell_var)){ // 4+-комн.
								$roomsChangeRoomSell .= '<li><a data-name="rooms_flat" data-type="checkbox_select" data-id="4" href="javascript:void(0)">в 4-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(5,$ex_rooms_flat_room_sell_var)){ // 5-комн.
								$roomsChangeRoomSell .= '<li><a data-name="rooms_flat" data-type="checkbox_select" data-id="5" href="javascript:void(0)">в 5-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(in_array(5,$ex_rooms_flat_room_sell_var)){ // 6+-комн.
								$roomsChangeRoomSell .= '<li><a data-name="rooms_flat" data-type="checkbox_select" data-id="6" href="javascript:void(0)">в 6+-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
						}
						?>
						<input type="hidden" name="rooms_flat"<?=$valueInput?>>
						<div class="block_checkbox">
							<ul>
								<li <?in_array(2,$ex_rooms_flat_room_sell_var)?print'class="checked"':''?>><a data-tags="в 2-комн." href="javascript:void(0)" data-id="2"><i class="triangle-bottomleft two"></i>2</a></li>
								<li <?in_array(3,$ex_rooms_flat_room_sell_var)?print'class="checked"':''?>><a data-tags="в 3-комн." href="javascript:void(0)" data-id="3"><i class="triangle-bottomleft three"></i>3</a></li>
								<li <?in_array(4,$ex_rooms_flat_room_sell_var)?print'class="checked"':''?>><a data-tags="в 4-комн." href="javascript:void(0)" data-id="4"><i class="triangle-bottomleft more"></i>4</a></li>
								<li <?in_array(5,$ex_rooms_flat_room_sell_var)?print'class="checked"':''?>><a data-tags="в 5-комн." href="javascript:void(0)" data-id="5"><i class="triangle-bottomleft five"></i>5</a></li>
								<li <?in_array(6,$ex_rooms_room_sell_vary)?print'class="checked"':''?>><a data-tags="в 6+-комн." href="javascript:void(0)" data-id="6"><i class="triangle-bottomleft five"></i>6+</a></li>
								<li class="last">комнатной квартире</li>
							</ul>
						</div>
					</div>
					<!--<div class="checkbox_block">
						<?
						// $valueInput = '';
						// $shareChangeRoomSell = '';
						// if($share_room_sell_var){
							// $valueInput = ' value="'.$share_room_sell_var.'"';
							// $shareChangeRoomSell .= '<li><a data-name="share" data-type="checkbox_select" data-id="1" href="javascript:void(0)">Доля<span onclick="return delTags(this)" class="close"></span></a></li>';
						// }
						?>
						<input type="hidden" name="share"<?=$valueInput?>>
						<div class="block_checkbox">
							<ul>
								<li class="vert_line"></li>
								<li <?isset($share_room_sell_var)?print'class="checked"':''?> class="share"><a data-tags="Доля" href="javascript:void(0)" data-id="1">Доля</a></li>
							</ul>
						</div>
					</div>-->
					<!--<div style="margin-left:8px;" class="select_block balcony">
						<?
						// $shareChangeRoomSell = '';
						// if(isset($arrayLinkSearch['share']) && $share_room_sell_var){
							// $c1 = $arrayLinkSearch['share'];
							// if($c1==0){
								// $shareChangeRoomSell = '<li><a class="min" data-name_select="share" data-name="share" data-type="checkbox_block" data-id="'.$arrayLinkSearch['share'].'" href="javascript:void(0)">Нет санузла<span onclick="return delTags(this)" class="close"></span></a></li>';
							// }
							// else {
								// $shareChangeRoomSell = '<li><a class="min" data-name_select="share" data-name="share" data-type="checkbox_block" data-id="'.$arrayLinkSearch['share'].'" href="javascript:void(0)">'.$_TYPE_SHARE[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
							// }
						// }
						
						// $nameParams = 'Доля: не важно';
						// $valueParams = '';
						// $currentParams = '';
						// if($share_room_sell_var && isset($arrayLinkSearch['share'])){
							// $currentParams = (int)$arrayLinkSearch['share'];
							// $valueParams = ' value="'.$arrayLinkSearch['share'].'"';
						// }
						// $share = '';
						// for($n=0; $n<count($_TYPE_SHARE); $n++){
							// if($n==0){
								// $current = '';
								// $current2 = '';
								// if($currentParams=='' && !isset($arrayLinkSearch['share'])){
									// $current2 = ' class="current"';
									// $nameParams = $_TYPE_SHARE[0];
								// }
								// $share .= '<li'.$current2.'><a href="javascript:void(0)" data-id="">'.$_TYPE_SHARE[0].'</a></li>';
							// }
							// else {
								// $current = '';
								// if($currentParams==$n){
									// $current = ' class="current"';
									// $nameParams = $_TYPE_SHARE[$n];
								// }
								// $share .= '<li'.$current.'><a data-name="share" data-explanation="" data-tags="'.clearValueText($_TYPE_SHARE[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_SHARE[$n].'</a></li>';
							// }
						// }
						// echo '<input type="hidden" name="share"'.$valueParams.'>';
						// echo '<div style="width:138px" class="choosed_block">'.$nameParams.'</div>';
						// echo '<div class="scrollbar-inner">';
						// echo '<ul>';
						// echo $share;
						// echo '</ul>';
						// echo '</div>';
						?>
					</div>-->
				</div>
				<div <?$type_room_rent_var?print'style="display:block"':'style="display:none"'?> class="change_block_filter room rent">
					<div class="metric ceil">
						<div class="floor">
							<!--<div style="width:110px" class="label_select">Комнат в квартире</div>-->
							<div style="margin-left:0;" class="checkbox_block">
								<?
								$ex_rooms_room_rent_var = array();
								$valueInput = '';
								$roomsChangeRoomRent = '';
								if(isset($rooms_flat_room_rent_var)){
									$ex_rooms_room_rent_var = explode(',',$rooms_flat_room_rent_var);
									$valueInput = ' value="'.$rooms_flat_room_rent_var.'"';
									$id_room = 0;
									$name_room = '';
									if(in_array(2,$ex_rooms_room_rent_var)){ // 2-комнаты
										$roomsChangeRoomRent .= '<li><a data-name="rooms_flat" data-type="checkbox_select" data-id="2" href="javascript:void(0)">2-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									if(in_array(3,$ex_rooms_room_rent_var)){ // 3-комнаты
										$roomsChangeRoomRent .= '<li><a data-name="rooms_flat" data-type="checkbox_select" data-id="3" href="javascript:void(0)">3-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									if(in_array(4,$ex_rooms_room_rent_var)){ // 4+-комнат
										$roomsChangeRoomRent .= '<li><a data-name="rooms_flat" data-type="checkbox_select" data-id="4" href="javascript:void(0)">4+-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
								}
								?>
								<input type="hidden" name="rooms_flat"<?=$valueInput?>>
								<div class="block_checkbox big">
									<ul>
										<li <?in_array(2,$ex_rooms_room_rent_var)?print'class="checked"':''?>><a data-tags="2-комн." href="javascript:void(0)" data-id="2">2</a></li>
										<li <?in_array(3,$ex_rooms_room_rent_var)?print'class="checked"':''?>><a data-tags="3-комн." href="javascript:void(0)" data-id="3">3</a></li>
										<li <?in_array(4,$ex_rooms_room_rent_var)?print'class="checked"':''?>><a data-tags="4+-комн." href="javascript:void(0)" data-id="4">4+</a></li>
										<li class="last">Комнат в квартире</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div <?$type_country_sell_var?print'style="display:block"':'style="display:none"'?> class="change_block_filter country sell">
					<div class="checkbox_block">
						<?
						$ex_type_country_sell_var = array();
						$valueInput = '';
						$typeCountryChangeCountrySell = '';
						if($type_country_country_sell_var){
							$ex_type_country_sell_var = explode(',',$type_country_country_sell_var);
							$valueInput = ' value="'.$type_country_country_sell_var.'"';
							$id_room = 0;
							$name_room = '';
							for($com=1; $com<=count($_TYPE_COUNTRY_ESTATE); $com++){
								if(in_array($com,$ex_type_country_sell_var)){
									$typeCountryChangeCountrySell .= '<li><a data-name="type_country" data-type="checkbox_select" data-id="'.$com.'" href="javascript:void(0)">'.$_TYPE_COUNTRY_ESTATE[$com].'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
							}
						}
						?>
						<input type="hidden" name="type_country"<?=$valueInput?>>
						<div style="width:610px" class="block_checkbox big comm">
							<ul>
								<?
								for($com=1; $com<=count($_TYPE_COUNTRY_ESTATE); $com++){
									echo '<li '.(in_array($com,$ex_type_country_sell_var)?'class="checked"':"").'><a style="font-size:15px" data-tags="'.$_TYPE_COUNTRY_ESTATE[$com].'" href="javascript:void(0)" data-id="'.$com.'">'.$_TYPE_COUNTRY_ESTATE[$com].'</a></li>';
								}
								?>
							</ul>
						</div>
					</div>
				</div>
				<div <?$type_country_rent_var?print'style="display:block"':'style="display:none"'?> class="change_block_filter country rent">
					<div class="checkbox_block">
						<?
						$ex_type_country_rent_var = array();
						$valueInput = '';
						$typeCountryChangeCountryRent = '';
						if($type_country_country_rent_var){
							$ex_type_country_rent_var = explode(',',$type_country_country_rent_var);
							$valueInput = ' value="'.$type_country_country_rent_var.'"';
							$id_room = 0;
							$name_room = '';
							for($com=1; $com<=count($_TYPE_COUNTRY_ESTATE); $com++){
								if(in_array($com,$ex_type_country_rent_var)){
									$typeCountryChangeCountryRent .= '<li><a data-name="type_country" data-type="checkbox_select" data-id="'.$com.'" href="javascript:void(0)">'.$_TYPE_COUNTRY_ESTATE[$com].'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
							}
						}
						?>
						<input type="hidden" name="type_country"<?=$valueInput?>>
						<div style="width:610px" class="block_checkbox big comm">
							<ul>
								<?
								for($com=1; $com<=count($_TYPE_COUNTRY_ESTATE); $com++){
									echo '<li '.(in_array($com,$ex_type_country_rent_var)?'class="checked"':"").'><a style="font-size:15px" data-tags="'.$_TYPE_COUNTRY_ESTATE[$com].'" href="javascript:void(0)" data-id="'.$com.'">'.$_TYPE_COUNTRY_ESTATE[$com].'</a></li>';
								}
								?>
							</ul>
						</div>
					</div>
				</div>
				<div <?$type_commercial_sell_var?print'style="display:block"':'style="display:none"'?> class="change_block_filter commercial sell">
					<div class="checkbox_block">
						<?
						$ex_type_commer_commercial_sell_var = array();
						$valueInput = '';
						$typeCommerChangeCommercialSell = '';
						if($type_commer_commercial_sell_var){
							$ex_type_commer_commercial_sell_var = explode(',',$type_commer_commercial_sell_var);
							$valueInput = ' value="'.$type_commer_commercial_sell_var.'"';
							$id_room = 0;
							$name_room = '';
							for($com=1; $com<=count($_TYPE_COMMERCIAL_ESTATE); $com++){
								if(in_array($com,$ex_type_commer_commercial_sell_var)){
									// $typeCommerChangeCommercialSell .= '<li><a data-name="type_commer" data-type="checkbox_select" data-id="'.$com.'" href="javascript:void(0)">'.$_TYPE_COMMERCIAL_ESTATE[$com].'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
							}
						}
						else {
							$valueInput = ' value="1"';
							// $typeCommerChangeCommercialSell .= '<li><a data-name="type_commer" data-type="checkbox_select" data-id="1" href="javascript:void(0)">'.$_TYPE_COMMERCIAL_ESTATE[1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
						}
						?>
						<input type="hidden" name="type_commer"<?=$valueInput?>>
						<div style="width:610px" class="block_checkbox big comm">
							<ul>
								<?
								for($com=1; $com<=count($_TYPE_COMMERCIAL_ESTATE); $com++){
									$checked = '';
									if(in_array($com,$ex_type_commer_commercial_sell_var)){
										$checked = 'class="checked"';
									}
									if(!$type_commer_commercial_sell_var && $com==1){
										$checked = 'class="checked"';
									}
									echo '<li '.$checked.'><a style="font-size:15px" data-tags="'.$_TYPE_COMMERCIAL_ESTATE[$com].'" href="javascript:void(0)" data-id="'.$com.'">'.$_TYPE_COMMERCIAL_ESTATE[$com].'</a></li>';
								}
								?>
							</ul>
						</div>
					</div>
				</div>
				<div <?$type_commercial_rent_var?print'style="display:block"':'style="display:none"'?> class="change_block_filter commercial rent">
					<div class="checkbox_block">
						<?
						$ex_type_commer_commercial_rent_var = array();
						$valueInput = '';
						$typeCommerChangeCommercialRent = '';
						if($type_commer_commercial_rent_var){
							$ex_type_commer_commercial_rent_var = explode(',',$type_commer_commercial_rent_var);
							$valueInput = ' value="'.$type_commer_commercial_rent_var.'"';
							$id_room = 0;
							$name_room = '';
							for($com=1; $com<=count($_TYPE_COMMERCIAL_ESTATE); $com++){
								if(in_array($com,$ex_type_commer_commercial_rent_var)){
									// $typeCommerChangeCommercialRent .= '<li><a data-name="type_commer" data-type="checkbox_select" data-id="'.$com.'" href="javascript:void(0)">'.$_TYPE_COMMERCIAL_ESTATE[$com].'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
							}
						}
						else {
							$valueInput = ' value="1"';
						}
						?>
						<input type="hidden" name="type_commer"<?=$valueInput?>>
						<div style="width:610px" class="block_checkbox big comm">
							<ul>
								<?
								for($com=1; $com<=count($_TYPE_COMMERCIAL_ESTATE); $com++){
									$checked = '';
									if(in_array($com,$ex_type_commer_commercial_rent_var)){
										$checked = 'class="checked"';
									}
									if(!$ex_type_commer_commercial_rent_var && $com==1){
										$checked = 'class="checked"';
									}
									echo '<li '.$checked.'><a style="font-size:15px" data-tags="'.$_TYPE_COMMERCIAL_ESTATE[$com].'" href="javascript:void(0)" data-id="'.$com.'">'.$_TYPE_COMMERCIAL_ESTATE[$com].'</a></li>';
								}
								?>
							</ul>
						</div>
					</div>
				</div>
				
				<div <?if($type_commercial_rent_var || $type_commercial_sell_var){print'style="margin-left:560px"';}else{if($type_country_rent_var || $type_country_sell_var){print'style="margin-left:340px"';}if($type_flat_sell_var){print'style="margin-left:375px"';}if($type_room_sell_var){print'style="margin-left:375px"';}}?> <?$type_new_sell_var?print'style="margin-left:200px"':''?> class="row new flat room country commercial cession margin">
					<div class="location_change<?if($type_commercial_sell_var){print ' commercial sell';}?><?if($type_commercial_rent_var){print ' commercial rent';}?><?if($type_country_sell_var){print ' country sell';}?><?if($type_country_rent_var){print ' country rent';}?><?if($type_new_sell_var){print ' new';}?><?if($type_flat_sell_var){print ' flat sell';}?><?if($type_flat_rent_var){print ' flat rent';}?><?if($type_room_sell_var){print ' room sell';}?><?if($type_room_rent_var){print ' room rent';}?>">
						<!--<div <?$type_commercial_rent_var || $type_commercial_sell_var || $type_country_rent_var || $type_country_sell_var?print'style="display:inline-block"':print'style="display:none"'?> data-type="city" class="city filter_local"><a href="javascript:void(0)"><span class="text">Санкт-Петербург</span></a>-->
						<?
						// echo '<pre>';
						// print_r($_SERVER);
						// echo '</pre>';
						$cityNameChoosed = 'Санкт-Петербург';
						$valueInput = ' value="1"';
						$ex_city_var = array(1);
						if($city_commercial_sell_var){
							$ex_city_var = explode(',',$city_commercial_sell_var);
							$valueInput = ' value="'.$city_commercial_sell_var.'"';
						}
						if($city_commercial_rent_var){
							$ex_city_var = explode(',',$city_commercial_rent_var);
							$valueInput = ' value="'.$city_commercial_rent_var.'"';
						}
						if($city_country_sell_var){
							$ex_city_var = explode(',',$city_country_sell_var);
							$valueInput = ' value="'.$city_country_sell_var.'"';
						}
						if($city_country_rent_var){
							$ex_city_var = explode(',',$city_country_rent_var);
							$valueInput = ' value="'.$city_country_rent_var.'"';
						}
						if($city_room_sell_var){
							$ex_city_var = explode(',',$city_room_sell_var);
							$valueInput = ' value="'.$city_room_sell_var.'"';
						}
						if($city_room_rent_var){
							$ex_city_var = explode(',',$city_room_rent_var);
							$valueInput = ' value="'.$city_room_rent_var.'"';
						}
						if($city_flat_sell_var){
							$ex_city_var = explode(',',$city_flat_sell_var);
							$valueInput = ' value="'.$city_flat_sell_var.'"';
						}
						if($city_flat_rent_var){
							$ex_city_var = explode(',',$city_flat_rent_var);
							$valueInput = ' value="'.$city_flat_rent_var.'"';
						}
						if($city_new_sell_var){
							$ex_city_var = explode(',',$city_new_sell_var);
							$valueInput = ' value="'.$city_new_sell_var.'"';
						}
						if($city_cession_sell_var){
							$ex_city_var = explode(',',$city_cession_sell_var);
							$valueInput = ' value="'.$city_cession_sell_var.'"';
						}
						
						if($location_id){
							$ex_city_var = array($location_id);
							$valueInput = '';
						}

						$listCities = '';
						$word = array();
						$data = array();
						$dataLocation = array();
						$res = mysql_query("
							SELECT *
							FROM ".$template."_location
							WHERE activation='1'
							ORDER BY name
						");
						if(mysql_num_rows($res)>0){
							while($row = mysql_fetch_assoc($res)){
								$w = substr(rawurldecode($row['name']),0,2);
								if(!array_search($w,$word)){
									array_push($word,$w);
								}
								$arr = array(
									"id" => $row['id'],
									"name" => $row['name'],
									"subdomen" => $row['subdomen'],
									"link" => $row['link'],
									"word" => $w,
								);
								$dataLocation[$row['link']] = $row['id'];
								array_push($data,$arr);
							}
						}
						$column_count = ceil(count($data)/4);
						$n2 = 0;
						$newWord = array();
						$column = array();
						$count = 1;
						for($d=0; $d<count($data); $d++){
							$open_ul = '';
							$close_ul = '';
							if(!in_array($n2,$column)){
								array_push($column,$n2);
								$open_ul = '<ul>';
							}
							$listCities .= $open_ul;
							if(!in_array($data[$d]['word'],$newWord)){
								array_push($newWord,$data[$d]['word']);
								$listCities .= '<li class="title">'.$data[$d]['word'].'</li>';
							}
							$current = '';
							$cityName = $data[$d]['name'];
							if($data[$d]['id']==2){
								$cityName = 'Лен. Обл.';
							}
							if(in_array($data[$d]['id'],$ex_city_var) || $location_id && $data[$d]['id']==$location_id){
								$current = ' class="current"';
								$cityNameChoosed = $cityName;
							}
							$subdomen = '';
							if(!empty($data[$d]['subdomen'])){
								$subdomen = $data[$d]['subdomen'].'.';
							}
							$ex_http_host = explode('.',$_SERVER['HTTP_HOST']);
							$addLocalParam = '';
							if($data[$d]['id']==2){
								$addLocalParam = '/'.$data[$d]['link'];
							}
							$REQUEST_URI = $_SERVER['REQUEST_URI'];
							$REQUEST_URI = str_replace('/leningradskaya-oblast','',$REQUEST_URI);
							if($ex_http_host[0]=='www'){
								if(!empty($subdomen)){
									$full_link_location = 'https://'.$subdomen.$ex_http_host[1].'.'.$ex_http_host[2].$REQUEST_URI;
								}
								else {
									$full_link_location = 'https://www.'.$ex_http_host[1].'.'.$ex_http_host[2].$REQUEST_URI;
								}
							}
							else {
								
								if(empty($subdomen)){
									$subdomen = 'www.';
								}
								$full_link_location = 'https://'.$subdomen.$ex_http_host[1].'.'.$ex_http_host[2].$REQUEST_URI;
							}
							// $full_link_location = 'javascript:void(0)';
							$listCities .= '<li'.$current.'><a onclick="return coordsChange(\'city\','.$data[$d]['id'].',\''.$data[$d]['name'].'\')" data-tags="'.$data[$d]['name'].'" data-id="'.$data[$d]['id'].'" href="'.$full_link_location.$addLocalParam.'"><span>'.$cityName.'</span></a></li>';
							if($count==$column_count){
								if(!in_array($data[$d+1]['word'],$newWord)){
									$close_ul = '</ul>';
									$n2++;
									$count = 1;
								}
							}
							else {
								$count++;
							}
							$listCities .= $close_ul;
						}
						?>
						<div data-type="city" class="city filter_local"><a href="javascript:void(0)"><span class="text"><?=$cityNameChoosed?></span></a>
							<input type="hidden" name="city"<?=$valueInput?>>
							<div class="hidden_block">
								<div class="angles">
									<div class="angle-top-border"></div>
									<div class="angle-top-white"></div>
								</div>
								<div class="inner">
									<span onclick="return closeLocDial(this)" class="close"></span>
									<div class="list_areas one_value">
										<?=$listCities?>
									</div>
								</div>
							</div>
						</div>
						<!--<div <?$type_flat_rent_var || $type_flat_sell_var || $type_room_rent_var || $type_room_sell_var?print'style="margin-left:130px"':''?> data-type="area" class="area filter_local">-->
						<div data-type="area" class="area filter_local">
							<a href="javascript:void(0)"><span class="icon"><i class="fa fa-plus-circle"></i></span><span class="text">Район</span></a>
							<?
							$ex_area_var = array();
							$valueInput = '';
							if($area_commercial_sell_var){
								$ex_area_var = explode(',',$area_commercial_sell_var);
								$valueInput = ' value="'.$area_commercial_sell_var.'"';
							}
							if($area_commercial_rent_var){
								$ex_area_var = explode(',',$area_commercial_rent_var);
								$valueInput = ' value="'.$area_commercial_rent_var.'"';
							}
							if($area_country_sell_var){
								$ex_area_var = explode(',',$area_country_sell_var);
								$valueInput = ' value="'.$area_country_sell_var.'"';
							}
							if($area_country_rent_var){
								$ex_area_var = explode(',',$area_country_rent_var);
								$valueInput = ' value="'.$area_country_rent_var.'"';
							}
							if($area_room_sell_var){
								$ex_area_var = explode(',',$area_room_sell_var);
								$valueInput = ' value="'.$area_room_sell_var.'"';
							}
							if($area_room_rent_var){
								$ex_area_var = explode(',',$area_room_rent_var);
								$valueInput = ' value="'.$area_room_rent_var.'"';
							}
							if($area_flat_sell_var){
								$ex_area_var = explode(',',$area_flat_sell_var);
								$valueInput = ' value="'.$area_flat_sell_var.'"';
							}
							if($area_flat_rent_var){
								$ex_area_var = explode(',',$area_flat_rent_var);
								$valueInput = ' value="'.$area_flat_rent_var.'"';
							}
							if($area_new_sell_var){
								$ex_area_var = explode(',',$area_new_sell_var);
								$valueInput = ' value="'.$area_new_sell_var.'"';
							}
							if($area_cession_sell_var){
								$ex_area_var = explode(',',$area_cession_sell_var);
								$valueInput = ' value="'.$area_cession_sell_var.'"';
							}
							?>
							<input type="hidden" name="area"<?=$valueInput?>>
							<div class="hidden_block">
								<div class="angles">
									<div class="angle-top-border"></div>
									<div class="angle-top-white"></div>
								</div>
								<div class="inner">
									<span onclick="return closeLocDial(this)" class="close"></span>
									<div class="btn_save top">
										<label class="btn"><input onclick="return list_areas_close(true,this)" type="button" value="Сохранить"><span class="angle-right"></span></label>
									</div>
									<div class="list_areas">
										<?
										$word = array();
										$data = array();
										$res = mysql_query("
											SELECT *
											FROM ".$template."_areas
											WHERE activation='1' && city_id='".implode(',',$ex_city_var)."'
											ORDER BY title
										");
										if(mysql_num_rows($res)>0){
											while($row = mysql_fetch_assoc($res)){
												$w = substr(rawurldecode($row['title']),0,2);
												if(!array_search($w,$word)){
													array_push($word,$w);
												}
												$arr = array(
													"id" => $row['id'],
													"title" => $row['title'],
													"word" => $w,
												);
												array_push($data,$arr);
											}
										}
										$column_count = ceil(count($data)/4);
										$n2 = 0;
										$newWord = array();
										$column = array();
										$count = 1;
										$areaChange = '';
										for($d=0; $d<count($data); $d++){
											$open_ul = '';
											$close_ul = '';
											if(!in_array($n2,$column)){
												array_push($column,$n2);
												$open_ul = '<ul>';
											}
											echo $open_ul;
											if(!in_array($data[$d]['word'],$newWord)){
												array_push($newWord,$data[$d]['word']);
												echo '<li class="title">'.$data[$d]['word'].'</li>';
											}
											$current = '';
											if(in_array($data[$d]['id'],$ex_area_var)){
												$current = ' class="current"';
												$areaChange .= '<li><a data-name="area" data-type="list_areas" data-id="'.$data[$d]['id'].'" href="javascript:void(0)">'.$data[$d]['title'].' рн.<span onclick="return delTags(this)" class="close"></span></a></li>';
											}
											echo '<li'.$current.'><a data-tags="'.$data[$d]['title'].' рн." data-id="'.$data[$d]['id'].'" href="javascript:void(0)"><span>'.$data[$d]['title'].'</span></a></li>';
											if($count==$column_count){
												if(!in_array($data[$d+1]['word'],$newWord)){
													$close_ul = '</ul>';
													$n2++;
													$count = 1;
												}
											}
											else {
												$count++;
											}
											echo $close_ul;
										}
										?>
									</div>
								</div>
							</div>
						</div>
						<div data-type="station" class="station filter_local">
							<a href="javascript:void(0)"><span class="icon"><i class="fa fa-plus-circle"></i></span><span class="text">Метро</span></a>
							<?
							$ex_station_var = array();
							$valueInput = '';
							if($station_commercial_sell_var){
								$ex_station_var = explode(',',$station_commercial_sell_var);
								$valueInput = ' value="'.$station_commercial_sell_var.'"';
							}
							if($station_commercial_rent_var){
								$ex_station_var = explode(',',$station_commercial_rent_var);
								$valueInput = ' value="'.$station_commercial_rent_var.'"';
							}
							if($station_country_sell_var){
								$ex_station_var = explode(',',$station_country_sell_var);
								$valueInput = ' value="'.$station_country_sell_var.'"';
							}
							if($station_country_rent_var){
								$ex_station_var = explode(',',$station_country_rent_var);
								$valueInput = ' value="'.$station_country_rent_var.'"';
							}
							if($station_room_sell_var){
								$ex_station_var = explode(',',$station_room_sell_var);
								$valueInput = ' value="'.$station_room_sell_var.'"';
							}
							if($station_room_rent_var){
								$ex_station_var = explode(',',$station_room_rent_var);
								$valueInput = ' value="'.$station_room_rent_var.'"';
							}
							if($station_flat_sell_var){
								$ex_station_var = explode(',',$station_flat_sell_var);
								$valueInput = ' value="'.$station_flat_sell_var.'"';
							}
							if($station_flat_rent_var){
								$ex_station_var = explode(',',$station_flat_rent_var);
								$valueInput = ' value="'.$station_flat_rent_var.'"';
							}
							if($station_cession_sell_var){
								$ex_station_var = explode(',',$station_cession_sell_var);
								$valueInput = ' value="'.$station_cession_sell_var.'"';
							}
							if($station_new_sell_var){
								$ex_station_var = explode(',',$station_new_sell_var);
								$valueInput = ' value="'.$station_new_sell_var.'"';
							}
							?>
							<input type="hidden" name="station"<?=$valueInput?>>
							<div class="hidden_block">
								<div class="angles">
									<div class="angle-top-border"></div>
									<div class="angle-top-white"></div>
								</div>
								<div class="inner">
									<span onclick="return closeLocDial(this)" class="close"></span>
									<div class="tabs_view">
										<ul>
											<li class="current"><a onclick="return tabsView(this)" data-type="map" href="javascript:void(0)"><span>Схема метрополитена</span></a></li>
											<li><a onclick="return tabsView(this)" data-type="list" href="javascript:void(0)"><span>Список станций</span></a></li>
											<li class="last">
												<div class="btn_save">
													<label class="btn"><input onclick="return list_areas_close(true,this)" type="button" value="Сохранить"><span class="angle-right"></span></label>
												</div>
											</li>
										</ul>
									</div>
									<div class="items map show">
										<div class="metro_stations">
											<div class="map_image">
												<img width="854" height="738" src="/images/Metro_map.png">
											</div>
											<div class="list_stations">
												<?
												if(count($ex_station_var)>0){
													$metroArray = $ex_station_var;
												}
												$id = 1;
												if($location_id){
													$id = $location_id;
												}
												$res = mysql_query("
													SELECT t.title,b.coords,b.station_id AS id_station,b.parent_id, 
													(SELECT COUNT(id) FROM ".$template."_borders_station WHERE parent_id=b.id) AS parent
													FROM ".$template."_stations AS t
													LEFT JOIN ".$template."_borders_station AS b
													ON b.station_id=t.id && b.activation='1'
													WHERE t.city_id='".$id."' && t.activation='1'
													ORDER BY title, b.parent_id
												",$db) or die(mysql_error());
												$metro = '';
												$text = '';
												$change_metro = '';
												$metro_list = '';

												$map = '/images/Metro_map.png';
												$map_hover = '/images/Metro_map_hover.png';
												if(mysql_num_rows($res)>0){
													$parentCount = 0;
													$main_id = 0;
													$parent = -1;
													$parent_id = '';
													while($row = mysql_fetch_assoc($res)){ // $width.":".$height.":".$x1.":".$y1.":".$x2.":".$y2;
														$coords = explode(':',$row['coords']);
														$width = $coords[0];
														$height = $coords[1];
														$left = $coords[2];
														$top = $coords[3];
														$styles = 'background-image:none;background-position:-'.$left.'px -'.$top.'px;';
														$checked = '';
														if($metroArray){
															for($m=0; $m<count($metroArray); $m++){
																if($metroArray[$m]==$row['id_station'] || !empty($parent_id) && $parent_id==$metroArray[$m]){
																	$checked = ' current';
																	$styles = 'background-image:url('.$map_hover.');background-position:-'.$left.'px -'.$top.'px;background-repeat: no-repeat;';
																	break;
																}
															}
														}
														$q = '';
														if(!empty($row['parent'])){ // 3
															$main_id = $row['id_station'];
															$metro .= '<div data-main="'.$main_id.'" class="parent_block">';
															$q = ' parent';
															$parentCount = $row['parent'];
															$parent_id = $main_id;
														}
														if(!empty($parentCount)){
															$parent = $parent+1;
														}
														$metro .= '<div style="left:'.$left.'px;top:'.$top.'px" class="metro item'.$checked.''.$q.'"><a style="width:'.$width.'px;height:'.$height.'px;'.$styles.'" title="'.$row['title'].'" data-id="'.$row['id_station'].'" data-tags="м. '.$row['title'].'" href="javascript:void(0)"></a></div>';
														if($parent==$parentCount && !empty($parentCount)){
															$metro .= '</div>';
															// $metro .= '<span></span>';
															$parentCount = 0;
															$parent = -1;
															$parent_id = '';
														}
														
														$metro_list .= '<li><a href="javascript:void(0)" class="cities" data-values="'.$row['id_station'].'"><span class="element">'.$row['title'].'</span></a></li>';
													}
												}
												echo $metro;
												?>
											</div>
										</div>
									</div>
									<div class="items list">
										<div style="width:859px" class="list_areas">
											<?
											$word = array();
											$data = array();
											$res = mysql_query("
												SELECT *
												FROM ".$template."_stations
												WHERE activation='1'".$paramLocation."
												ORDER BY title
											");
											if(mysql_num_rows($res)>0){
												while($row = mysql_fetch_assoc($res)){
													$w = substr(rawurldecode($row['title']),0,2);
													if(!array_search($w,$word)){
														array_push($word,$w);
													}
													$arr = array(
														"id" => $row['id'],
														"title" => $row['title'],
														"word" => $w,
													);
													array_push($data,$arr);
												}
											}
											$column_count = ceil(count($data)/4);
											$n2 = 0;
											$newWord = array();
											$column = array();
											$count = 1;
											$stationChange = '';
											for($d=0; $d<count($data); $d++){
												$open_ul = '';
												$close_ul = '';
												if(!in_array($n2,$column)){
													array_push($column,$n2);
													$open_ul = '<ul style="width:24%">';
												}
												echo $open_ul;
												if(!in_array($data[$d]['word'],$newWord)){
													array_push($newWord,$data[$d]['word']);
													echo '<li class="title">'.$data[$d]['word'].'</li>';
												}
												$current = '';
												if(in_array($data[$d]['id'],$ex_station_var)){
													$current = ' class="current"';
													$stationChange .= '<li><a data-name="station" data-type="list_areas" data-id="'.$data[$d]['id'].'" href="javascript:void(0)">м. '.$data[$d]['title'].'<span onclick="return delTags(this)" class="close"></span></a></li>';
												}
												echo '<li'.$current.'><a data-tags="м. '.$data[$d]['title'].'" data-id="'.$data[$d]['id'].'" href="javascript:void(0)"><span>'.$data[$d]['title'].'</span></a></li>';
												if($count==$column_count){
													if(!in_array($data[$d+1]['word'],$newWord)){
														$close_ul = '</ul>';
														$n2++;
														$count = 1;
													}
												}
												else {
													$count++;
												}
												echo $close_ul;
											}
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="just_country" <?$estateValue==4?print'style="display:inline-block"':print'style="display:none"'?> data-type="railway">
							<div data-type="railway" class="railway filter_local">
								<a href="javascript:void(0)"><span class="icon"><i class="fa fa-plus-circle"></i></span><span class="text">Ж/д</span></a>
								<?
								$ex_railway_var = array();
								$valueInput = '';
								if($railway_commercial_sell_var){
									$ex_railway_var = explode(',',$railway_commercial_sell_var);
									$valueInput = ' value="'.$railway_commercial_sell_var.'"';
								}
								if($railway_commercial_rent_var){
									$ex_railway_var = explode(',',$railway_commercial_rent_var);
									$valueInput = ' value="'.$railway_commercial_rent_var.'"';
								}
								if($railway_country_sell_var){
									$ex_railway_var = explode(',',$railway_country_sell_var);
									$valueInput = ' value="'.$railway_country_sell_var.'"';
								}
								if($railway_country_rent_var){
									$ex_railway_var = explode(',',$railway_country_rent_var);
									$valueInput = ' value="'.$railway_country_rent_var.'"';
								}
								if($railway_room_sell_var){
									$ex_railway_var = explode(',',$railway_room_sell_var);
									$valueInput = ' value="'.$railway_room_sell_var.'"';
								}
								if($railway_room_rent_var){
									$ex_railway_var = explode(',',$railway_room_rent_var);
									$valueInput = ' value="'.$railway_room_rent_var.'"';
								}
								if($railway_flat_sell_var){
									$ex_railway_var = explode(',',$railway_flat_sell_var);
									$valueInput = ' value="'.$railway_flat_sell_var.'"';
								}
								if($railway_flat_rent_var){
									$ex_railway_var = explode(',',$railway_flat_rent_var);
									$valueInput = ' value="'.$railway_flat_rent_var.'"';
								}
								if($railway_cession_sell_var){
									$ex_railway_var = explode(',',$railway_cession_sell_var);
									$valueInput = ' value="'.$railway_cession_sell_var.'"';
								}
								if($railway_new_sell_var){
									$ex_railway_var = explode(',',$railway_new_sell_var);
									$valueInput = ' value="'.$railway_new_sell_var.'"';
								}
								$railwayArray = array();
								$railwayArray2 = array();
								$railwayValues = '';
								
								if(count($ex_railway_var)>0){
									$railwayArray = $ex_railway_var;
								}
								
								$railwayChange = '';
								if(count($railwayArray)>0){
									$rlwArray = '(';
									for($p=0; $p<count($railwayArray); $p++){
										array_push($railwayArray2,"id='".intval($railwayArray[$p])."'");
									}
									$rlwArray .= implode(' || ',$railwayArray2);
									$rlwArray .= ')';
									$res = mysql_query("
										SELECT *
										FROM ".$template."_railways
										WHERE activation=1 && ".$rlwArray.$paramLocation."
									");
									if(mysql_num_rows($res)>0){
										while($rw = mysql_fetch_assoc($res)){
											$railwayValues .= '<li><a data-id="'.$rw['id'].'" href="javascript:void(0)">«'.$rw['name'].'»<span onclick="return removeRailway(this)" class="close"></span></a></li>';
											$railwayChange .= '<li><a data-name="railway" data-type="list_areas" data-id="'.$rw['id'].'" href="javascript:void(0)">'.$rw['name'].'<span onclick="return delTags(this)" class="close"></span></a></li>';
										}
									}
								}
								
								?>
								<input type="hidden" name="railway"<?=$valueInput?>>
								<div class="hidden_block">
									<div class="angles">
										<div class="angle-top-border"></div>
										<div class="angle-top-white"></div>
									</div>
									<div class="inner">
										<span onclick="return closeLocDial(this)" class="close"></span>
										<div class="btn_save top">
											<label class="btn"><input onclick="return list_areas_close(true,this)" type="button" value="Сохранить"><span class="angle-right"></span></label>
										</div>
										<div style="overflow:visible" class="list_areas">
											<div class="request_block railway">
												<input style="width:310px;height:26px;border:1px solid #858b91;font-size:13px;padding:0 8px" class="railway_input" placeholder="Начните вводить название станции" autocomplete="off" type="text" class="text">
												<div class="search_request_block">
													<div class="scrollbar-inner">
														<ul></ul>
													</div>
												</div>
												<ul class="railways"><?=$railwayValues?></ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--<div id="just_build" <?$estateValue==1 || $estateValue==6?print'style="display:inline-block"':print'style="display:none"'?> data-type="build">
							<div data-type="build" class="area filter_local">
								<a href="javascript:void(0)"><span class="icon"><i class="fa fa-plus-circle"></i></span><span class="text">ЖК</span></a>
								<?
								$ex_build_var = array();
								$valueInput = '';
								if($build_new_sell_var){
									$ex_build_var = explode(',',$build_new_sell_var);
									$valueInput = ' value="'.$build_new_sell_var.'"';
								}
								if($build_cession_sell_var){
									$ex_build_var = explode(',',$build_cession_sell_var);
									$valueInput = ' value="'.$build_cession_sell_var.'"';
								}
								?>
								<input type="hidden" name="build"<?=$valueInput?>>
								<div class="hidden_block">
									<div class="angles">
										<div class="angle-top-border"></div>
										<div class="angle-top-white"></div>
									</div>
									<div class="inner">
										<span onclick="return closeLocDial(this)" class="close"></span>
										<div class="btn_save top">
											<label class="btn"><input onclick="return list_areas_close(true,this)" type="button" value="Сохранить"><span class="angle-right"></span></label>
										</div>
										<div class="list_areas">
											<?
											$word = array();
											$data = array();
											$res = mysql_query("
												SELECT *
												FROM ".$template."_m_catalogue_left
												WHERE activation='1'".$paramLocation."
												ORDER BY name
											");
											if(mysql_num_rows($res)>0){
												while($row = mysql_fetch_assoc($res)){
													$name = trim(htmlspecialchars_decode($row['name']));
													$name = str_replace('ЖК ', '', $name);
													$name = str_replace('"', '', $name);
													$w = substr($name,0,2);
													if(mb_strlen($w,'UTF-8')==2){
														$w = substr($name,0,1);
													}
													if(!array_search($w,$word)){
														array_push($word,$w);
													}
													$arr = array(
														"id" => $row['id'],
														"title" => $name,
														"word" => $w,
													);
													array_push($data,$arr);
												}
											}
											$column_count = ceil(count($data)/5);
											$column_count_word = 0;
											$columns = mysql_query("
												SELECT *
												FROM ".$template."_build_info
												WHERE id='1'
											");
											if(mysql_num_rows($columns)>0){
												$column = mysql_fetch_assoc($columns);
												$column_count_word = $column['col'];
											}
											$n2 = 0;
											$newWord = array();
											$columnArray = array();
											$count = 1;
											$countColumn = 1;
											$buildChange = '';
											// for($d=0; $d<count($data); $d++){
												// $open_ul = '';
												// $close_ul = '';
												// if(!in_array($n2,$columnArray)){
													// array_push($columnArray,$n2);
													// $open_ul = '<ul>';
												// }
												// echo $open_ul;
												// if(!in_array($data[$d]['word'],$newWord)){
													// array_push($newWord,$data[$d]['word']);
													// echo '<li class="title">'.$data[$d]['word'].'</li>';
												// }
												// $current = '';
												// if(in_array($data[$d]['id'],$ex_build_var)){
													// $current = ' class="current"';
													// $buildChange .= '<li><a data-name="build" data-type="list_areas" data-id="'.$data[$d]['id'].'" href="javascript:void(0)">'.$data[$d]['title'].'<span onclick="return delTags(this)" class="close"></span></a></li>';
												// }
												// echo '<li'.$current.'><a data-tags="'.$data[$d]['title'].'" data-id="'.$data[$d]['id'].'" href="javascript:void(0)"><span>'.$data[$d]['title'].'</span></a></li>';
												// if($count==$column_count){
													// if(!in_array($data[$d+1]['word'],$newWord)){
														// $close_ul = '</ul>';
														// $n2++;
														// $count = 1;
													// }
												// }
												// else {
													// $count++;
												// }
												// echo $close_ul;
											// }
											$data2 = 0;
											for($g=1; $g<=$column_count_word; $g++){ // 6 колонок
												echo '<ul>';
												if($g==1){
													$d = 0;
												}
												else {
													$d = $data2;
												}
												$q = 1;
												for($d=$d; $d<count($data); $d++){ // 145 значений
													$countThisCol = $column['column_'.$g];
													// echo $q.'<br>'.$countThisCol;
													
													if($q<=$countThisCol || in_array($data[$d+1]['word'],$newWord) || $q==$countThisCol && !in_array($data[$d+2]['word'],$newWord)){
														if(!in_array($data[$d]['word'],$newWord)){
															array_push($newWord,$data[$d]['word']);
															echo '<li class="title">'.$data[$d]['word'].'</li>';
															$countColumn++;
															$q++;
														}
														$current = '';
														if(in_array($data[$d]['id'],$ex_build_var)){
															$current = ' class="current"';
															$buildChange .= '<li><a data-name="build" data-type="list_areas" data-id="'.$data[$d]['id'].'" href="javascript:void(0)">'.$data[$d]['title'].'<span onclick="return delTags(this)" class="close"></span></a></li>';
														}
														echo '<li'.$current.'><a data-tags="'.$data[$d]['title'].'" data-id="'.$data[$d]['id'].'" href="javascript:void(0)"><span>'.$data[$d]['title'].'</span></a></li>';
														$data2++;
													}
													else {
														$current = '';
														if(in_array($data[$d]['id'],$ex_build_var)){
															$current = ' class="current"';
															$buildChange .= '<li><a data-name="build" data-type="list_areas" data-id="'.$data[$d]['id'].'" href="javascript:void(0)">'.$data[$d]['title'].'<span onclick="return delTags(this)" class="close"></span></a></li>';
														}
														echo '<li'.$current.'><a data-tags="'.$data[$d]['title'].'" data-id="'.$data[$d]['id'].'" href="javascript:void(0)"><span>'.$data[$d]['title'].'</span></a></li>';
														$data2++;
														break;
													}
												}
												echo '</ul>';
											}
											?>
										</div>
									</div>
								</div>
							</div>
							<div data-type="developer" class="area filter_local">
								<a href="javascript:void(0)"><span class="icon"><i class="fa fa-plus-circle"></i></span><span class="text">Застройщики</span></a>
								<?
								$ex_developer_var = array();
								$valueInput = '';
								if($developer_new_sell_var){
									$ex_developer_var = explode(',',$developer_new_sell_var);
									$valueInput = ' value="'.$developer_new_sell_var.'"';
								}
								if($developer_cession_sell_var){
									$ex_developer_var = explode(',',$developer_cession_sell_var);
									$valueInput = ' value="'.$developer_cession_sell_var.'"';
								}
								?>
								<input type="hidden" name="developer"<?=$valueInput?>>
								<div class="hidden_block">
									<div class="angles">
										<div class="angle-top-border"></div>
										<div class="angle-top-white"></div>
									</div>
									<div class="inner">
										<span onclick="return closeLocDial(this)" class="close"></span>
										<div class="btn_save top">
											<label class="btn"><input onclick="return list_areas_close(true,this)" type="button" value="Сохранить"><span class="angle-right"></span></label>
										</div>
										<div class="list_areas one_value_sec">
											<?
											$word = array();
											$data = array();
											$res = mysql_query("
												SELECT *
												FROM ".$template."_developers
												WHERE activation='1'
												ORDER BY name
											");
											if(mysql_num_rows($res)>0){
												while($row = mysql_fetch_assoc($res)){
													$name = trim(htmlspecialchars_decode($row['name']));
													$w = substr($name,0,2);
													if(mb_strlen($w,'UTF-8')==2){
														$w = substr($name,0,1);
													}
													if(!array_search($w,$word)){
														array_push($word,$w);
													}
													$arr = array(
														"id" => $row['id'],
														"title" => $name,
														"word" => $w,
													);
													array_push($data,$arr);
												}
											}
											$column_count = ceil(count($data)/5);
											$column_count_word = 0;
											$columns = mysql_query("
												SELECT *
												FROM ".$template."_developer_info
												WHERE id='1'
											");
											if(mysql_num_rows($columns)>0){
												$column = mysql_fetch_assoc($columns);
												$column_count_word = $column['col'];
											}

											$n2 = 0;
											$newWord = array();
											$columnArray = array();
											$count = 1;
											$countColumn = 1;
											$developerChange = '';
											// for($d=0; $d<count($data); $d++){
												// $open_ul = '';
												// $close_ul = '';
												// if(!in_array($n2,$columns)){
													// array_push($columns,$n2);
													// $open_ul = '<ul>';
												// }
												// echo $open_ul;
												// if(!in_array($data[$d]['word'],$newWord)){
													// array_push($newWord,$data[$d]['word']);
													// echo '<li class="title">'.$data[$d]['word'].'</li>';
												// }
												// $current = '';
												// if(in_array($data[$d]['id'],$ex_developer_var)){
													// $current = ' class="current"';
													// $developerChange .= '<li><a data-name="developer" data-type="list_areas" data-id="'.$data[$d]['id'].'" href="javascript:void(0)">'.$data[$d]['title'].'<span onclick="return delTags(this)" class="close"></span></a></li>';
												// }
												// echo '<li'.$current.'><a data-tags="'.$data[$d]['title'].'" data-id="'.$data[$d]['id'].'" href="javascript:void(0)"><span>'.$data[$d]['title'].'</span></a></li>';
												// if($count==$column_count){
													// if(!in_array($data[$d+1]['word'],$newWord)){
														// $close_ul = '</ul>';
														// $n2++;
														// $count = 1;
													// }
												// }
												// else {
													// $count++;
												// }
												// echo $close_ul;
											// }


											$data2 = 0;
											for($g=1; $g<=$column_count_word; $g++){
												echo '<ul>';
												if($g==1){
													$d = 0;
												}
												else {
													$d = $data2;
												}
												$q = 1;
												for($d=$d; $d<count($data); $d++){
													$countThisCol = $column['column_'.$g];
													
													if($q<=$countThisCol || in_array($data[$d+1]['word'],$newWord) || $q==$countThisCol && !in_array($data[$d+2]['word'],$newWord)){
														if(!in_array($data[$d]['word'],$newWord)){
															array_push($newWord,$data[$d]['word']);
															echo '<li class="title">'.$data[$d]['word'].'</li>';
															$countColumn++;
															$q++;
														}
														$current = '';
														if(in_array($data[$d]['id'],$ex_developer_var)){
															$current = ' class="current"';
															$buildChange .= '<li><a data-name="developer" data-type="list_areas" data-id="'.$data[$d]['id'].'" href="javascript:void(0)">'.$data[$d]['title'].'<span onclick="return delTags(this)" class="close"></span></a></li>';
														}
														echo '<li'.$current.'><a data-tags="'.$data[$d]['title'].'" data-id="'.$data[$d]['id'].'" href="javascript:void(0)"><span>'.$data[$d]['title'].'</span></a></li>';
														$data2++;
													}
													else {
														$current = '';
														if(in_array($data[$d]['id'],$ex_developer_var)){
															$current = ' class="current"';
															$buildChange .= '<li><a data-name="developer" data-type="list_areas" data-id="'.$data[$d]['id'].'" href="javascript:void(0)">'.$data[$d]['title'].'<span onclick="return delTags(this)" class="close"></span></a></li>';
														}
														echo '<li'.$current.'><a data-tags="'.$data[$d]['title'].'" data-id="'.$data[$d]['id'].'" href="javascript:void(0)"><span>'.$data[$d]['title'].'</span></a></li>';
														$data2++;
														break;
													}
												}
												echo '</ul>';
											}
											?>
										</div>
									</div>
								</div>
							</div>
						</div>-->
					</div>
					<div <?$type_commercial_rent_var || $type_commercial_sell_var?print'style="margin-left:45px"':''?><?$type_new_sell_var?print'style="margin-left:53px"':''?> class="advance_open<?if($type_commercial_sell_var){print ' commercial sell';}?><?if($type_commercial_rent_var){print ' commercial rent';}?><?if($type_country_sell_var){print ' country sell';}?><?if($type_country_rent_var){print ' country rent';}?><?if($type_new_sell_var){print ' new';}?><?if($type_flat_sell_var){print ' flat sell';}?><?if($type_flat_rent_var){print ' flat rent';}?><?if($type_room_sell_var){print ' room sell';}?><?if($type_room_rent_var){print ' room rent';}?>">
						<a href="javascript:void(0)"><span>Расширенный поиск</span></a>
					</div>
				</div>
				<div class="btn_save">
					<label class="btn"><input type="submit" value="Подобрать"><span class="angle-right"></span></label>
					<div class="clear_filter"><span>Очистить поиск</span></div>
				</div>
			</div>
		</div>
		<div class="advanced_search">
			<div <?$estateValue!=1?print'style="display:none"':''?> class="inner_block new">
				<div style="position:relative" class="row sell">
					<div class="left_block">
						<div class="metric">
							<div class="label_select">Общая площадь</div>
							<div class="select_block price reg ch_price">
								<?
								$squareChangeNewSell = '';
								if($arrayLinkSearch['squareFullMin'] && !$arrayLinkSearch['squareFullMax'] && $sellNewParam){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareFullMin'];
									$squareChangeNewSell = '<li><a class="min" data-name_select="fullsquare" data-name="squareFullMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $sellNewParam){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareFullMax'];
									$squareChangeNewSell = '<li><a class="max" data-name_select="fullsquare" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMax'].'" href="javascript:void(0)">общая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $sellNewParam){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareFullMin'];
									$c2 = $arrayLinkSearch['squareFullMax'];
									$squareChangeNewSell = '<li><a class="min max" data-name_select="fullsquare" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['squareFullMin']){
									$nameParams = $arrayLinkSearch['squareFullMin'];
									$currentParams = $arrayLinkSearch['squareFullMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMin'].'"';
								}
								?>
								<input type="hidden" name="squareFullMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareFullMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$_min_value_square_full; $c<=$_max_value_square_full;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquare" data-explanation="общая" data-tags="от '.$c.' кв.м" data-type="min" data-id="'.$c.'" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$_max_value_square_full){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$_max_value_square_full){
												$c = $_max_value_square_full;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price reg ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['squareFullMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareFullMax']);
									$currentParams = $arrayLinkSearch['squareFullMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMax'].'"';
								}
								?>
								<input type="hidden" name="squareFullMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareFullMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										$n = 2;
										for($c=$_min_value_square_full; $c<=$_max_value_square_full;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquare" data-explanation="общая" data-tags="до '.$c.' кв.м" data-type="max" data-id="'.$c.'"  href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$_max_value_square_full){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$_max_value_square_full){
												$c = $_max_value_square_full;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div class="label_select">Кухня</div>
							<div class="select_block price reg ch_price">
								<?
								$squareKitchenChangeNewSell = '';
								if($arrayLinkSearch['squareKitchenMin'] && !$arrayLinkSearch['squareKitchenMax'] && $sellNewParam){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeNewSell = '<li><a class="min" data-name_select="kitchensquare" data-name="squareKitchenMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMin'].'" href="javascript:void(0)">кухня от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareKitchenMin'] && $arrayLinkSearch['squareKitchenMax'] && $sellNewParam){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeNewSell = '<li><a class="max" data-name_select="kitchensquare" data-name="squareKitchenMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMax'].'" href="javascript:void(0)">кухня до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareKitchenMin'] && $arrayLinkSearch['squareKitchenMax'] && $sellNewParam){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareKitchenMin'];
									$c2 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeNewSell = '<li><a class="min max" data-name_select="kitchensquare" data-name="squareKitchenMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMin'].'" href="javascript:void(0)">кухня от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['squareKitchenMin']){
									$nameParams = $arrayLinkSearch['squareKitchenMin'];
									$currentParams = $arrayLinkSearch['squareKitchenMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareKitchenMin'].'"';
								}
								?>
								<input type="hidden" name="squareKitchenMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="4" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareKitchenMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								
								
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_kitchen_square_new_sell; $c<=$max_kitchen_square_new_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="kitchensquare" data-explanation="кухня" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price reg ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['squareKitchenMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareKitchenMax']);
									$currentParams = $arrayLinkSearch['squareKitchenMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareKitchenMax'].'"';
								}
								?>
								<input type="hidden" name="squareKitchenMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="4" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareKitchenMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_kitchen_square_new_sell; $c<=$max_kitchen_square_new_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="kitchensquare" data-explanation="кухня" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<!--<div style="width:80px;margin-left:-30px;margin-top:-2px;" class="label_select">Дата публикации</div>-->
							<div class="label_select">Дата публикации</div>
							<div class="select_block count">
								<?
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['dates']){
									$currentParams = (int)$arrayLinkSearch['dates'];
									$valueParams = ' value="'.$arrayLinkSearch['dates'].'"';
								}
								$dates = '';
								$datesArray = array();
								for($n=0; $n<count($_DATE_POST); $n++){
									if($n==0){
										$current = '';
										if($currentParams=='' && !isset($arrayLinkSearch['dates'])){
											$current = ' class="current"';
											$nameParams = $_DATE_POST[0]['name'];
										}
										$dates .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_DATE_POST[0]['name'].'</a></li>';
									}
									else {
										$current = '';
										if($currentParams==$_DATE_POST[$n]['id']){
											$current = ' class="current"';
											$nameParams = $_DATE_POST[$n]['name'];
										}
										$dates .= '<li'.$current.'><a data-name="dates" data-explanation="" data-tags="'.$_DATE_POST[$n]['name'].'" href="javascript:void(0)" data-id="'.$_DATE_POST[$n]['id'].'">'.$_DATE_POST[$n]['name'].'</a></li>';
										$datesArray[$_DATE_POST[$n]['id']] = $_DATE_POST[$n]['name'];	
									}
								}
								$datesChangeNewSell = '';
								if($arrayLinkSearch['dates'] && $sellNewParam){
									$c1 = $arrayLinkSearch['dates'];
									// for(){
										
									// }
									$datesChangeNewSell = '<li><a class="min" data-name_select="dates" data-name="dates" data-type="checkbox_block" data-id="'.$arrayLinkSearch['dates'].'" href="javascript:void(0)">'.$datesArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								echo '<input type="hidden" name="dates"'.$valueParams.'>';
								echo '<div style="width:158px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $dates;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
					</div>
					<div style="margin-left:57px" class="center_block">
						<!--<div class="floor">
							<div style="margin-left:8px;" class="select_block balcony">
								<?
								/*
								$cessionChangeNewSell = '';
								if(isset($arrayLinkSearch['cession']) && $cession_new_sell_var){
									$c1 = $arrayLinkSearch['cession'];
									if($c1==0){
										$cessionChangeNewSell = '<li><a class="min" data-name_select="cession" data-name="cession" data-type="checkbox_block" data-id="'.$arrayLinkSearch['cession'].'" href="javascript:void(0)">Нет санузла<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									else {
										$cessionChangeNewSell = '<li><a class="min" data-name_select="cession" data-name="cession" data-type="checkbox_block" data-id="'.$arrayLinkSearch['cession'].'" href="javascript:void(0)">'.$_TYPE_CESSION[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
								}
								
								$nameParams = 'Переуступка: не важно';
								$valueParams = '';
								$currentParams = '';
								if($cession_new_sell_var && isset($arrayLinkSearch['cession'])){
									$currentParams = (int)$arrayLinkSearch['cession'];
									$valueParams = ' value="'.$arrayLinkSearch['cession'].'"';
								}
								$cession = '';
								for($n=0; $n<count($_TYPE_CESSION); $n++){
									if($n==0){
										$current = '';
										$current2 = '';
										if($currentParams=='' && !isset($arrayLinkSearch['cession'])){
											$current2 = ' class="current"';
											$nameParams = $_TYPE_CESSION[0];
										}
										$cession .= '<li'.$current2.'><a href="javascript:void(0)" data-id="">'.$_TYPE_CESSION[0].'</a></li>';
									}
									else {
										$current = '';
										if($currentParams==$n){
											$current = ' class="current"';
											$nameParams = $_TYPE_CESSION[$n];
										}
										$cession .= '<li'.$current.'><a data-name="cession" data-explanation="" data-tags="'.clearValueText($_TYPE_CESSION[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_CESSION[$n].'</a></li>';
									}
								}
								echo '<input type="hidden" name="cession"'.$valueParams.'>';
								echo '<div style="width:185px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $cession;
								echo '</ul>';
								echo '</div>';*/
								?>
							</div>
						</div>-->
						<!--<div class="floor">
							<div style="width:46px;margin-left:-6px;" class="label_select">Санузел</div>
							<div class="select_block balcony">
								<?
								$wcChangeNewSell = '';
								if(isset($arrayLinkSearch['wc']) && $type_new_sell_var){
									$c1 = $arrayLinkSearch['wc'];
									if($c1==0){
										$wcChangeNewSell = '<li><a class="min" data-name_select="wc" data-name="wc" data-type="checkbox_block" data-id="'.$arrayLinkSearch['wc'].'" href="javascript:void(0)">Нет санузла<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									else {
										$wcChangeNewSell = '<li><a class="min" data-name_select="wc" data-name="wc" data-type="checkbox_block" data-id="'.$arrayLinkSearch['wc'].'" href="javascript:void(0)">'.$_TYPE_WC[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
								}
								
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_new_sell_var && isset($arrayLinkSearch['wc'])){
									$currentParams = (int)$arrayLinkSearch['wc'];
									$valueParams = ' value="'.$arrayLinkSearch['wc'].'"';
								}
								$wc = '';
								for($n=0; $n<count($_TYPE_WC); $n++){
									if($n==0){
										$current = '';
										$current2 = '';
										// if($arrayLinkSearch['wc']==0 && isset($arrayLinkSearch['wc'])){
											// $current = ' class="current"';
											// $nameParams = 'Нет';
										// }
										if($currentParams=='' && !isset($arrayLinkSearch['wc'])){
											$current2 = ' class="current"';
											$nameParams = $_TYPE_WC[0];
										}
										$wc .= '<li'.$current2.'><a href="javascript:void(0)" data-id="">'.$_TYPE_WC[0].'</a></li>';
										// $wc .= '<li'.$current.'><a data-name="wc" data-explanation="" data-tags="Нет санузла" href="javascript:void(0)" data-id="0">Нет</a></li>';
									}
									else {
										$current = '';
										if($currentParams==$n){
											$current = ' class="current"';
											$nameParams = $_TYPE_WC[$n];
										}
										$wc .= '<li'.$current.'><a data-name="wc" data-explanation="" data-tags="'.clearValueText($_TYPE_WC[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_WC[$n].'</a></li>';
									}
								}
								// echo '<input type="hidden" name="wc"'.$valueParams.'>';
								// echo '<div style="width:143px" class="choosed_block">'.$nameParams.'</div>';
								// echo '<div class="scrollbar-inner">';
								// echo '<ul>';
								// echo $wc;
								// echo '</ul>';
								// echo '</div>';
								?>
							</div>
						</div>-->
						<div style="width:730px" class="floor">
							<div class="checkbox_block">
								<?
								$ex_deadline_new_sell_var = array();
								$valueInput = '';
								$deadlineChangeNewSell = '';
								if(isset($deadline_new_sell_var)){
									$ex_deadline_new_sell_var = explode(',',$deadline_new_sell_var);
									$valueInput = ' value="'.$deadline_new_sell_var.'"';
									$id_room = 0;
									$name_room = '';
									if(in_array(1,$ex_deadline_new_sell_var)){ // Сдан
										$deadlineChangeNewSell .= '<li><a data-name="deadline" data-type="checkbox_select" data-id="1" href="javascript:void(0)">Сдан<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									if(in_array(2015,$ex_deadline_new_sell_var)){ // Ранее
										$deadlineChangeNewSell .= '<li><a data-name="deadline" data-type="checkbox_select" data-id="2015" href="javascript:void(0)">Сдача ранее 2016<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									if(in_array(2016,$ex_deadline_new_sell_var)){ // 2016
										$deadlineChangeNewSell .= '<li><a data-name="deadline" data-type="checkbox_select" data-id="2016" href="javascript:void(0)">Сдача 2016<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									if(in_array(2017,$ex_deadline_new_sell_var)){ // 2017
										$deadlineChangeNewSell .= '<li><a data-name="deadline" data-type="checkbox_select" data-id="2017" href="javascript:void(0)">Сдача 2017<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									if(in_array(2018,$ex_deadline_new_sell_var)){ // 2018
										$deadlineChangeNewSell .= '<li><a data-name="deadline" data-type="checkbox_select" data-id="2018" href="javascript:void(0)">Сдача 2018<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									if(in_array(2019,$ex_deadline_new_sell_var)){ // 2019
										$deadlineChangeNewSell .= '<li><a data-name="deadline" data-type="checkbox_select" data-id="2019" href="javascript:void(0)">Сдача 2019<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									if(in_array(2020,$ex_deadline_new_sell_var)){ // Позднее
										$deadlineChangeNewSell .= '<li><a data-name="deadline" data-type="checkbox_select" data-id="2020" href="javascript:void(0)">Сдача позднее 2019<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
								}
								?>
								<input type="hidden" name="deadline"<?=$valueInput?>>
								<div class="block_checkbox">
									<ul>
										<li style="margin:6px 10px 0 0" class="last">Год сдачи</li>
										<li <?in_array(1,$ex_deadline_new_sell_var)?print'class="checked"':''?>><a data-tags="Сдан" href="javascript:void(0)" data-id="1">Сдан</a></li>
										<li <?in_array(2016,$ex_deadline_new_sell_var)?print'class="checked"':''?>><a data-tags="Сдача ранее 2016" href="javascript:void(0)" data-id="2016">Ранее</a></li>
										<li <?in_array(2017,$ex_deadline_new_sell_var)?print'class="checked"':''?>><a data-tags="Сдача 2017" href="javascript:void(0)" data-id="2017">2017</a></li>
										<li <?in_array(2018,$ex_deadline_new_sell_var)?print'class="checked"':''?>><a data-tags="Сдача 2018" href="javascript:void(0)" data-id="2018">2018</a></li>
										<li <?in_array(2019,$ex_deadline_new_sell_var)?print'class="checked"':''?>><a data-tags="Сдача 2019" href="javascript:void(0)" data-id="2019">2019</a></li>
										<li <?in_array(2020,$ex_deadline_new_sell_var)?print'class="checked"':''?>><a data-tags="Сдача 2020" href="javascript:void(0)" data-id="2020">2020</a></li>
										<li <?in_array(2021,$ex_deadline_new_sell_var)?print'class="checked"':''?>><a data-tags="Сдача позднее 2020" href="javascript:void(0)" data-id="2021">Позднее</a></li>
									</ul>
								</div>
							</div>
							<div class="metric">
								<div class="cell">
									<div style="width:100px;margin-left:0" class="label_select">Класс жилья</div>
									<div class="select_block count multiple">
										<?
										
										$ex_array = explode(',',$arrayLinkSearch['class']); 
										$classChangeNewSell = '';
										if($arrayLinkSearch['class'] && $sellNewParam){
											$c1 = $arrayLinkSearch['class'];
											$ex_c1 = explode(',',$c1);
											$arrTypesHouses = array();
											for($c=0; $c<count($class_objects_new); $c++){
												$arrTypesHouses[$c]=$class_objects_new[$c];
											}
											for($e=0; $e<count($ex_c1); $e++){
												$classChangeNewSell .= '<li><a class="min" data-name_select="class" data-name="class" data-type="checkbox_block" data-id="'.$ex_c1[$e].'" data-multiple="true" href="javascript:void(0)">'.$arrTypesHouses[$ex_c1[$e]].'<span onclick="return delTags(this)" class="close"></span></a></li>';
											}
										}

										$nameParams = 'Не важно';
										$valueParams = '';
										$currentParams = '';
										if($sellNewParam && $arrayLinkSearch['class']){
											$currentParams = (int)$arrayLinkSearch['class'];
											$valueParams = ' value="'.$arrayLinkSearch['class'].'"';
										}
										$class = '';
										$classArray = array();
										for($c=0; $c<count($class_objects_new); $c++){
											if($c==0){
												$current = '';
												if($currentParams==0){
													$current = ' class="current"';
													$nameParams = 'Не важно';
												}
											}
											else {
												$current = '';
												if(in_array($c,$ex_array)){
													$current = ' class="current"';
													$nameParams = $class_objects_new[$c];
												}
												$class .= '<li'.$current.'><a data-name="class" data-explanation="" data-tags="'.clearValueText($class_objects_new[$c]).'" href="javascript:void(0)" data-id="'.$c.'"><span class="check"></span><span class="label">'.$class_objects_new[$c].'</span></a></li>';
												$classArray[$c] = $class_objects_new[$c];										
											}
										}
										// $classChangeNewSell = '';
										// if($arrayLinkSearch['class'] && $sellNewParam){
											// $c1 = $arrayLinkSearch['class'];
											// $classChangeNewSell = '<li><a class="min" data-name_select="class" data-name="class" data-type="checkbox_block" data-id="'.$arrayLinkSearch['class'].'" href="javascript:void(0)">'.$classArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
										// }
										if(count($ex_array)>1){
											$nameParams = count($ex_array).' параметра';
										}
										echo '<input type="hidden" name="class"'.$valueParams.'>';
										echo '<div style="width:143px" class="choosed_block">'.$nameParams.'</div>';
										echo '<div class="scrollbar-inner">';
										echo '<ul>';
										echo $class;
										echo '</ul>';
										echo '</div>';
										?>
									</div>
								</div>
							</div>
						</div>
						<div style="margin-top:15px" class="floor">
							<div style="width:auto;margin-left:26px" class="label_select">Этаж</div>
							<div class="select_block price sec ch_price">
								<?
								$floorChangeNewSell = '';
								if($arrayLinkSearch['floorMin'] && !$arrayLinkSearch['floorMax'] && $sellNewParam){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorMin'];
									$floorChangeNewSell = '<li><a class="min" data-name_select="floorsquare" data-name="floorMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $sellNewParam){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeNewSell = '<li><a class="max" data-name_select="floorsquare" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMax'].'" href="javascript:void(0)">этаж по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $sellNewParam){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorMin'];
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeNewSell = '<li><a class="min max" data-name_select="floorsquare" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['floorMin']){
									$nameParams = $arrayLinkSearch['floorMin'];
									$currentParams = $arrayLinkSearch['floorMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMin'].'"';
								}
								?>
								<input type="hidden" name="floorMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="2" placeholder="От" type="text" value="<?=$arrayLinkSearch['floorMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$max_floors_new_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquare" data-explanation="этаж" data-tags="с '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price sec ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['floorMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorMax']);
									$currentParams = $arrayLinkSearch['floorMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMax'].'"';
								}
								?>
								<input type="hidden" name="floorMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="2" placeholder="До" type="text" value="<?=$arrayLinkSearch['floorMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_new_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquare" data-explanation="этаж" data-tags="по '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<!--<div class="floor">
							<div class="label_select">в доме</div>
							<div class="select_block count">
								<?
								// $floorsChangeNewSell = '';
								// if($arrayLinkSearch['floorsMin'] && !$arrayLinkSearch['floorsMax'] && $sellNewParam){ //только этажи "с"
									// $c1 = $arrayLinkSearch['floorsMin'];
									// $floorsChangeNewSell = '<li><a class="min" data-name_select="floorscount" data-name="floorsMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">в доме от '.$c1.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								// }
								// if(!$arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $sellNewParam){ //только этажи "по"
									// $c2 = $arrayLinkSearch['floorsMax'];
									// $floorsChangeNewSell = '<li><a class="max" data-name_select="floorscount" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMax'].'" href="javascript:void(0)">в доме по '.$c2.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								// }
								// if($arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $sellNewParam){ //промежуток этажей
									// $c1 = $arrayLinkSearch['floorsMin'];
									// $c2 = $arrayLinkSearch['floorsMax'];
									// $floorsChangeNewSell = '<li><a class="min max" data-name_select="floorscount" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">в доме от '.$c1.' по '.$c2.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								// }

								// $nameParams = 'От';
								// $valueParams = '';
								// $currentParams = '';
								// if($sellNewParam && $arrayLinkSearch['floorsMin']){
									// $nameParams = $arrayLinkSearch['floorsMin'];
									// $currentParams = $arrayLinkSearch['floorsMin'];
									// $valueParams = ' value="'.$arrayLinkSearch['floorsMin'].'"';
								// }
								?>
								<input type="hidden" name="floorsMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										// for($f=1; $f<=$max_floors_new_sell; $f++){
											// $current = "";
											// if($f==$currentParams){
												// $current = ' class="current"';
											// }
											// echo '<li'.$current.'><a data-name="floorscount" data-explanation="в доме" data-tags="от '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								// $nameParams = 'До';
								// $valueParams = '';
								// $currentParams = '';
								// if($sellNewParam && $arrayLinkSearch['floorsMax']){
									// $nameParams = str_replace(".", ",", $arrayLinkSearch['floorsMax']);
									// $currentParams = $arrayLinkSearch['floorsMax'];
									// $valueParams = ' value="'.$arrayLinkSearch['floorsMax'].'"';
								// }
								?>
								<input type="hidden" name="floorsMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										// for($f=1; $f<=$max_floors_new_sell; $f++){
											// $current = "";
											// if($f==$currentParams){
												// $current = ' class="current"';
											// }
											// echo '<li'.$current.'><a data-name="floorscount" data-explanation="в доме" data-tags="до '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>
							<div class="com">этажей</div>
						</div>-->
						<div class="floor">
							<div class="check_block">
								<?
								// $checked = '';
								// $valueParams = '';
								// $disabled = ' disabled="disabled"';
								// if(isset($except_first_new_sell_var)){
									// if($except_first_new_sell_var==1){
										// $disabled = '';
										// $checked = ' checked';
										// $valueParams = ' value="'.$except_first_new_sell_var.'"';
									// }
								// }
								?>
								<!--<div class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_first"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме первого</span>
									</div>
								</div>-->
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_last_new_sell_var)){
									if($except_last_new_sell_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_last_new_sell_var.'"';
									}
								}
								?>
								<div style="margin-left:64px" class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_last"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме последнего</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div style="height:68px;margin:53px 0 0 -49px" class="line"></div>
					<div style="position:absolute;right:0;bottom:0;right:33px" class="right_block">
						<!--<div class="metric">
							<div style="width:67px;margin-left:-27px" class="label_select">Сдача</div>
							<div class="select_block count">
								<?
								
								$ex_min_value_delivery_new_sell = explode(' ',$min_value_delivery_new_sell);
								$ex_max_value_delivery_new_sell = explode(' ',$max_value_delivery_new_sell);
								$_min_value_delivery_year = (int)$ex_min_value_delivery_new_sell[1];
								$_max_value_delivery_year = (int)$ex_max_value_delivery_new_sell[1];

								$n = 1;
								$deliveryAdd = '';
								$deliveryArrayMin = '';
								$_QUATRE = array("I","II","III","IV");
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['deliveryMin']){
									$currentParams = $arrayLinkSearch['deliveryMin'];
								}
								for($c=$_min_value_delivery_year; $c<=$_max_value_delivery_year; $c++){
									$min_search = array_search($ex_min_value_delivery_new_sell[0],$_QUATRE);
									$max_search = array_search($ex_max_value_delivery_new_sell[0],$_QUATRE);
									if($c==$_min_value_delivery_year){
										for($q=$min_search; $q<count($_QUATRE); $q++){
											$q_name = $_QUATRE[$q]." кв. ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMin = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="Срок сдачи" data-tags="'.$q_name.'" data-type="min" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
									else if($c==$_max_value_delivery_year){
										for($q=0; $q<=$max_search; $q++){
											$q_name = $_QUATRE[$q]." кв. ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMin = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="Срок сдачи" data-tags="'.$q_name.'" data-type="min" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
									else {
										for($q=0; $q<count($_QUATRE); $q++){
											$q_name = $_QUATRE[$q]." кв. ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMin = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="Срок сдачи" data-tags="'.$q_name.'" data-type="min" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
								}
								$nameParams = 'От';
								$valueParams = '';
								if($sellNewParam && $arrayLinkSearch['deliveryMin']){
									$nameParams = $arrayLinkSearch['deliveryMin'];
									if(!empty($deliveryArrayMin)){
										$nameParams = $deliveryArrayMin;
									}
									$currentParams = $arrayLinkSearch['deliveryMin'];
									$valueParams = ' value="'.$arrayLinkSearch['deliveryMin'].'"';
								}
								?>
								<input type="hidden" name="deliveryMin"<?=$valueParams?>>
								<div style="width:143px" class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?=$deliveryAdd?>
									</ul>
								</div>
							</div>
							<div style="margin:4px 4px 0" class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$n = 1;
								$deliveryAdd = '';
								$deliveryArrayMax = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['deliveryMax']){
									$currentParams = $arrayLinkSearch['deliveryMax'];
								}
								for($c=$_min_value_delivery_year; $c<=$_max_value_delivery_year; $c++){
									$min_search = array_search($ex_min_value_delivery_new_sell[0],$_QUATRE);
									$max_search = array_search($ex_max_value_delivery_new_sell[0],$_QUATRE);
									if($c==$_min_value_delivery_year){
										for($q=$min_search; $q<count($_QUATRE); $q++){
											$q_name = $_QUATRE[$q]." кв. ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMax = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="Срок сдачи" data-tags="по '.$q_name.'" data-type="max" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
									else if($c==$_max_value_delivery_year){
										for($q=0; $q<=$max_search; $q++){
											$q_name = $_QUATRE[$q]." кв. ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMax = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="Срок сдачи" data-tags="по '.$q_name.'" data-type="max" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
									else {
										for($q=0; $q<count($_QUATRE); $q++){
											$q_name = $_QUATRE[$q]." кв. ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMax = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="Срок сдачи" data-tags="по '.$q_name.'" data-type="max" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
								}

								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($sellNewParam && $arrayLinkSearch['deliveryMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['deliveryMax']);
									if(!empty($deliveryArrayMax)){
										$nameParams = $deliveryArrayMax;
									}
									$currentParams = $arrayLinkSearch['deliveryMax'];
									$valueParams = ' value="'.$arrayLinkSearch['deliveryMax'].'"';
								}
								
								$deliveryChangeNewSell = '';
								if($arrayLinkSearch['deliveryMin'] && !$arrayLinkSearch['deliveryMax'] && $sellNewParam){ //только метраж "от"
									$c1 = $deliveryArrayMin;
									$deliveryChangeNewSell = '<li><a class="min" data-name_select="delivery" data-name="deliveryMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['deliveryMin'].'" href="javascript:void(0)">Срок сдачи с '.$c1.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['deliveryMin'] && $arrayLinkSearch['deliveryMax'] && $sellNewParam){ //только метраж "до"
									$c2 = $deliveryArrayMax;
									$deliveryChangeNewSell = '<li><a class="max" data-name_select="delivery" data-name="deliveryMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['deliveryMax'].'" href="javascript:void(0)">Срок сдачи по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['deliveryMin'] && $arrayLinkSearch['deliveryMax'] && $sellNewParam){ //промежуток метражей
									$c1 = $deliveryArrayMin;
									$c2 = $deliveryArrayMax;
									$deliveryChangeNewSell = '<li><a class="min max" data-name_select="delivery" data-name="deliveryMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['deliveryMin'].'" href="javascript:void(0)">Срок сдачи с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								?>
								<input type="hidden" name="deliveryMax"<?=$valueParams?>>
								<div style="width:143px" class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?=$deliveryAdd?>
									</ul>
								</div>
							</div>
						</div>-->
						<div class="clear"></div>
						<div class="metric">
							<div class="cell">
								<div style="width:67px;margin-left:-27px" class="label_select">Тип дома</div>
								<div class="select_block count multiple">
									<?
									
									$ex_array = explode(',',$arrayLinkSearch['type_house']); 
									$type_houseChangeNewSell = '';
									if($arrayLinkSearch['type_house'] && $sellNewParam){
										$c1 = $arrayLinkSearch['type_house'];
										$ex_c1 = explode(',',$c1);
										$arrTypesHouses = array();
										$res = mysql_query("SELECT * FROM ".$template."_type_house WHERE activation='1' ORDER BY num");
										if(mysql_num_rows($res)>0){
											while($row = mysql_fetch_assoc($res)){
												$arrTypesHouses[$row['id']]=$row['name'];
											}
										}
										for($e=0; $e<count($ex_c1); $e++){
											$type_houseChangeNewSell .= '<li><a class="min" data-name_select="type_house" data-name="type_house" data-type="checkbox_block" data-id="'.$ex_c1[$e].'" data-multiple="true" href="javascript:void(0)">'.$arrTypesHouses[$ex_c1[$e]].'<span onclick="return delTags(this)" class="close"></span></a></li>';
										}
									}

									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($sellNewParam && $arrayLinkSearch['type_house']){
										$currentParams = (int)$arrayLinkSearch['type_house'];
										$valueParams = ' value="'.$arrayLinkSearch['type_house'].'"';
									}
									$type_house = '';
									$type_houseArray = array();;
									$devs = mysql_query("
										SELECT *, (SELECT name FROM ".$template."_type_house WHERE activation='1' && id='".$currentParams."') AS choose_value
										FROM ".$template."_type_house
										WHERE activation='1'
										ORDER BY num
									");
									if(mysql_num_rows($devs)>0){
										$n = 0;
										while($dev = mysql_fetch_assoc($devs)){
											if($n==0){
												$current = '';
												if($currentParams==0){
													$current = ' class="current"';
													$nameParams = 'Не важно';
												}
												// $type_house .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
											}
											else {
												$current = '';
												if(in_array($dev['id'],$ex_array)){
													$current = ' class="current"';
													$nameParams = $dev['name'];
												}
												$type_house .= '<li'.$current.'><a data-name="type_house" data-explanation="" data-tags="'.clearValueText($dev['name']).'" href="javascript:void(0)" data-id="'.$dev['id'].'"><span class="check"></span><span class="label">'.$dev['name'].'</span></a></li>';
												$type_houseArray[$dev['id']] = $dev['name'];										
											}
											$n++;
										}
									}
									// $type_houseChangeNewSell = '';
									// if($arrayLinkSearch['type_house'] && $sellNewParam){
										// $c1 = $arrayLinkSearch['type_house'];
										// $type_houseChangeNewSell = '<li><a class="min" data-name_select="type_house" data-name="type_house" data-type="checkbox_block" data-id="'.$arrayLinkSearch['type_house'].'" href="javascript:void(0)">'.$type_houseArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									// }
									if(count($ex_array)>1){
										$nameParams = count($ex_array).' параметра';
									}
									echo '<input type="hidden" name="type_house"'.$valueParams.'>';
									echo '<div style="width:143px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $type_house;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<div style="margin-left:13px" class="cell">
								<div class="select_block count">
									<?
									$far_subwayChangeNewSell = '';
									if($arrayLinkSearch['far_subway'] && $sellNewParam){
										$c1 = $arrayLinkSearch['far_subway'];
										$name_value = $_FAR_SUBWAY[$c1];
										if($c1==1){
											$name_value = '< 500 м';
										}
										$far_subwayChangeNewSell = '<li><a class="min" data-name_select="far_subway" data-name="far_subway" data-type="checkbox_block" data-id="'.$arrayLinkSearch['far_subway'].'" href="javascript:void(0)">Метро '.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									
									$nameParams = 'Метро не важно';
									$valueParams = '';
									$currentParams = '';
									if($sellNewParam && $arrayLinkSearch['far_subway']){
										$currentParams = (int)$arrayLinkSearch['far_subway'];
										$valueParams = ' value="'.$arrayLinkSearch['far_subway'].'"';
									}
									$far_subway = '';
									for($f=1; $f<=count($_FAR_SUBWAY); $f++){
										if($f==1){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = 'Метро не важно';
											}
											$far_subway .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Метро не важно</a></li>';
										}
										$current = '';
										if($currentParams==$f){
											$current = ' class="current"';
											$nameParams = 'Метро '.$_FAR_SUBWAY[$f];
										}
										$name_value = $_FAR_SUBWAY[$f];
										if($f==1){
											$name_value = '< 500 м';
										}
										$far_subway .= '<li'.$current.'><a data-name="far_subway" data-explanation="Метро" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'">Метро '.$_FAR_SUBWAY[$f].'</a></li>';
									}
									echo '<input type="hidden" name="far_subway"'.$valueParams.'>';
									echo '<div style="width:143px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $far_subway;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="metric">
							<div class="cell">
								<div style="width:46px;margin-left:-6px;" class="label_select">Отделка</div>
								<div class="select_block count multiple">
									<?
									$ex_array = explode(',',$arrayLinkSearch['finishing']); 
									$finishingChangeNewSell = '';
									if($arrayLinkSearch['finishing'] && $sellNewParam){
										$c1 = $arrayLinkSearch['finishing'];
										$ex_c1 = explode(',',$c1);
										for($e=0; $e<count($ex_c1); $e++){
											$finishingChangeNewSell .= '<li><a class="min" data-name_select="finishing" data-name="finishing" data-type="checkbox_block" data-id="'.$ex_c1[$e].'" data-multiple="true" href="javascript:void(0)">'.$_TYPE_FINISHING[$ex_c1[$e]].'<span onclick="return delTags(this)" class="close"></span></a></li>';
										}
									}
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($sellNewParam && $finishing_new_sell_var){
										$currentParams = (int)$finishing_new_sell_var;
										$valueParams = ' value="'.$finishing_new_sell_var.'"';
									}
									$finishing = '';
									for($n=0; $n<count($_TYPE_FINISHING); $n++){
										if($n==0){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = $_TYPE_FINISHING[0];
											}
											// $finishing .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_TYPE_FINISHING[0].'</a></li>';
										}
										else {
											$current = '';
											if(in_array($n,$ex_array)){
												$current = ' class="current"';
												$nameParams = $_TYPE_FINISHING[$n];
											}
											$tooltip = '';
											if(!empty($_TYPE_FIN_DESCR[$n])){
												$tooltip = ' data-much="true" data-pos="left" data-hover="tooltip" data-title="'.$_TYPE_FIN_DESCR[$n].'"';
											}
											$finishing .= '<li'.$current.'><a'.$tooltip.' data-name="finishing" data-explanation="" data-tags="'.clearValueText($_TYPE_FINISHING[$n]).'" href="javascript:void(0)" data-id="'.$n.'"><span class="check"></span><span class="label">'.$_TYPE_FINISHING[$n].'</span></a></li>';
										}
									}
									
									if(count($ex_array)>1){
										$nameParams = count($ex_array).' параметра';
									}
									echo '<input type="hidden" name="finishing"'.$valueParams.'>';
									echo '<div style="width:143px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $finishing;
									echo '</ul>';
									echo '</div>';
									?>

									
									
									
									
									
									
								</div>
							</div>
							<!--<div class="select_block balcony">
								<input type="hidden" name="balcony" value="0">
								<div class="choosed_block">Балкон не важен</div>
								<div class="scrollbar-inner">
									<ul>
										<?
										// for($c=0; $c<count($_TYPE_BALCONY); $c++){
											// $current = '';
											// if(!$balcony_new_sell_var){
												// if($c==0){
													// $current = ' class="current"';
												// }
											// }
											// else {
												// if($c==$balcony_new_sell_var){
													// $current = ' class="current"';
												// }
											// }
											// echo '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_BALCONY[$c].'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>-->
						</div>
					</div>
				</div>
			</div>
			<div <?$estateValue!=6?print'style="display:none"':''?> class="inner_block cession">
				<div class="row sell">
					<div class="left_block">
						<div class="metric">
							<div class="label_select">Общая площадь</div>
							<div class="select_block price reg ch_price">
								<?
								$squareChangeCessionSell = '';
								if($arrayLinkSearch['squareFullMin'] && !$arrayLinkSearch['squareFullMax'] && $type_cession_sell_var){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareFullMin'];
									$squareChangeCessionSell = '<li><a class="min" data-name_select="fullsquare" data-name="squareFullMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_cession_sell_var){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareFullMax'];
									$squareChangeCessionSell = '<li><a class="max" data-name_select="fullsquare" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMax'].'" href="javascript:void(0)">общая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_cession_sell_var){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareFullMin'];
									$c2 = $arrayLinkSearch['squareFullMax'];
									$squareChangeCessionSell = '<li><a class="min max" data-name_select="fullsquare" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['squareFullMin']){
									$nameParams = $arrayLinkSearch['squareFullMin'];
									$currentParams = $arrayLinkSearch['squareFullMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMin'].'"';
								}
								?>
								<input type="hidden" name="squareFullMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareFullMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$_min_value_square_full_cession; $c<=$_max_value_square_full_cession;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquare" data-explanation="общая" data-tags="от '.$c.' кв.м" data-type="min" data-id="'.$c.'" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$_max_value_square_full_cession){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$_max_value_square_full_cession){
												$c = $_max_value_square_full_cession;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price reg ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['squareFullMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareFullMax']);
									$currentParams = $arrayLinkSearch['squareFullMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMax'].'"';
								}
								?>
								<input type="hidden" name="squareFullMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareFullMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										$n = 2;
										for($c=$_min_value_square_full_cession; $c<=$_max_value_square_full_cession;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquare" data-explanation="общая" data-tags="до '.$c.' кв.м" data-type="max" data-id="'.$c.'"  href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$_max_value_square_full_cession){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$_max_value_square_full_cession){
												$c = $_max_value_square_full_cession;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div class="label_select">кухня</div>
							<div class="select_block price reg ch_price">
								<?
								$squareKitchenChangeCessionSell = '';
								if($arrayLinkSearch['squareKitchenMin'] && !$arrayLinkSearch['squareKitchenMax'] && $type_cession_sell_var){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeCessionSell = '<li><a class="min" data-name_select="kitchensquare" data-name="squareKitchenMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMin'].'" href="javascript:void(0)">кухня от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareKitchenMin'] && $arrayLinkSearch['squareKitchenMax'] && $type_cession_sell_var){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeCessionSell = '<li><a class="max" data-name_select="kitchensquare" data-name="squareKitchenMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMax'].'" href="javascript:void(0)">кухня до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareKitchenMin'] && $arrayLinkSearch['squareKitchenMax'] && $type_cession_sell_var){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareKitchenMin'];
									$c2 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeCessionSell = '<li><a class="min max" data-name_select="kitchensquare" data-name="squareKitchenMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMin'].'" href="javascript:void(0)">кухня от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['squareKitchenMin']){
									$nameParams = $arrayLinkSearch['squareKitchenMin'];
									$currentParams = $arrayLinkSearch['squareKitchenMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareKitchenMin'].'"';
								}
								?>
								<input type="hidden" name="squareKitchenMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareKitchenMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										if($max_kitchen_square_cession_sell==0){
											$max_kitchen_square_cession_sell = 10;
										}
										for($c=$min_kitchen_square_cession_sell; $c<=$max_kitchen_square_cession_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="kitchensquare" data-explanation="кухня" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price reg ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['squareKitchenMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareKitchenMax']);
									$currentParams = $arrayLinkSearch['squareKitchenMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareKitchenMax'].'"';
								}
								?>
								<input type="hidden" name="squareKitchenMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareKitchenMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										if($max_kitchen_square_cession_sell==0){
											$max_kitchen_square_cession_sell = 10;
										}
										for($c=$min_kitchen_square_cession_sell; $c<=$max_kitchen_square_cession_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="kitchensquare" data-explanation="кухня" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div class="label_select">Тип дома</div>
							<div class="select_block count">
								<?
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['type_house']){
									$currentParams = (int)$arrayLinkSearch['type_house'];
									$valueParams = ' value="'.$arrayLinkSearch['type_house'].'"';
								}
								$type_house = '';
								$type_houseArray = array();;
								$devs = mysql_query("
									SELECT *, (SELECT name FROM ".$template."_type_house WHERE activation='1' && id='".$currentParams."') AS choose_value
									FROM ".$template."_type_house
									WHERE activation='1'
									ORDER BY num
								");
								if(mysql_num_rows($devs)>0){
									$n = 0;
									while($dev = mysql_fetch_assoc($devs)){
										if($n==0){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = 'Не важно';
											}
											$type_house .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
										}
										$current = '';
										if($currentParams==$dev['id']){
											$current = ' class="current"';
											$nameParams = $dev['name'];
										}
										$type_house .= '<li'.$current.'><a data-name="type_house" data-explanation="" data-tags="'.clearValueText($dev['name']).'" href="javascript:void(0)" data-id="'.$dev['id'].'">'.$dev['name'].'</a></li>';
										$type_houseArray[$dev['id']] = $dev['name'];										
										$n++;
									}
								}
								$type_houseChangeCessionSell = '';
								if($arrayLinkSearch['type_house'] && $type_cession_sell_var){
									$c1 = $arrayLinkSearch['type_house'];
									$type_houseChangeCessionSell = '<li><a class="min" data-name_select="type_house" data-name="type_house" data-type="checkbox_block" data-id="'.$arrayLinkSearch['type_house'].'" href="javascript:void(0)">'.$type_houseArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								echo '<input type="hidden" name="type_house"'.$valueParams.'>';
								echo '<div style="width:140px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $type_house;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
					</div>
					<div class="center_block">
						<div class="floor">
							<div class="label_select">Этаж</div>
							<div class="select_block price sec ch_price">
								<?
								$floorChangeCessionSell = '';
								if($arrayLinkSearch['floorMin'] && !$arrayLinkSearch['floorMax'] && $type_cession_sell_var){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorMin'];
									$floorChangeCessionSell = '<li><a class="min" data-name_select="floorsquare" data-name="floorMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_cession_sell_var){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeCessionSell = '<li><a class="max" data-name_select="floorsquare" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMax'].'" href="javascript:void(0)">этаж по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_cession_sell_var){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorMin'];
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeCessionSell = '<li><a class="min max" data-name_select="floorsquare" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['floorMin']){
									$nameParams = $arrayLinkSearch['floorMin'];
									$currentParams = $arrayLinkSearch['floorMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMin'].'"';
								}
								?>
								<input type="hidden" name="floorMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="2" placeholder="От" type="text" value="<?=$arrayLinkSearch['floorMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$max_floors_cession_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquare" data-explanation="этаж" data-tags="с '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block sec price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['floorMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorMax']);
									$currentParams = $arrayLinkSearch['floorMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMax'].'"';
								}
								?>
								<input type="hidden" name="floorMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="2" placeholder="До" type="text" value="<?=$arrayLinkSearch['floorMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_cession_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquare" data-explanation="этаж" data-tags="по '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<!--<div class="floor">
							<div class="label_select">в доме</div>
							<div class="select_block count">
								<?
								// $floorsChangeCessionSell = '';
								// if($arrayLinkSearch['floorsMin'] && !$arrayLinkSearch['floorsMax'] && $type_cession_sell_var){ //только этажи "с"
									// $c1 = $arrayLinkSearch['floorsMin'];
									// $floorsChangeCessionSell = '<li><a class="min" data-name_select="floorscount" data-name="floorsMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">в доме от '.$c1.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								// }
								// if(!$arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $type_cession_sell_var){ //только этажи "по"
									// $c2 = $arrayLinkSearch['floorsMax'];
									// $floorsChangeCessionSell = '<li><a class="max" data-name_select="floorscount" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMax'].'" href="javascript:void(0)">в доме по '.$c2.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								// }
								// if($arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $type_cession_sell_var){ //промежуток этажей
									// $c1 = $arrayLinkSearch['floorsMin'];
									// $c2 = $arrayLinkSearch['floorsMax'];
									// $floorsChangeCessionSell = '<li><a class="min max" data-name_select="floorscount" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">в доме от '.$c1.' по '.$c2.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								// }

								// $nameParams = 'От';
								// $valueParams = '';
								// $currentParams = '';
								// if($type_cession_sell_var && $arrayLinkSearch['floorsMin']){
									// $nameParams = $arrayLinkSearch['floorsMin'];
									// $currentParams = $arrayLinkSearch['floorsMin'];
									// $valueParams = ' value="'.$arrayLinkSearch['floorsMin'].'"';
								// }
								?>
								<input type="hidden" name="floorsMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										// for($f=1; $f<=$max_floors_cession_sell; $f++){
											// $current = "";
											// if($f==$currentParams){
												// $current = ' class="current"';
											// }
											// echo '<li'.$current.'><a data-name="floorscount" data-explanation="в доме" data-tags="от '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								// $nameParams = 'До';
								// $valueParams = '';
								// $currentParams = '';
								// if($type_cession_sell_var && $arrayLinkSearch['floorsMax']){
									// $nameParams = str_replace(".", ",", $arrayLinkSearch['floorsMax']);
									// $currentParams = $arrayLinkSearch['floorsMax'];
									// $valueParams = ' value="'.$arrayLinkSearch['floorsMax'].'"';
								// }
								?>
								<input type="hidden" name="floorsMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										// for($f=1; $f<=$max_floors_cession_sell; $f++){
											// $current = "";
											// if($f==$currentParams){
												// $current = ' class="current"';
											// }
											// echo '<li'.$current.'><a data-name="floorscount" data-explanation="в доме" data-tags="до '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>
							<div class="com">этажей</div>
						</div>-->
						<div class="floor">
							<div class="check_block">
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_first_cession_sell_var)){
									if($except_first_cession_sell_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_first_cession_sell_var.'"';
									}
								}
								?>
								<div class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_first"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме первого</span>
									</div>
								</div>
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_last_cession_sell_var)){
									if($except_last_cession_sell_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_last_cession_sell_var.'"';
									}
								}
								?>
								<div class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_last"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме последнего</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="line"></div>
					<div class="right_block">
						<div class="metric">
							<div class="cell">
								<div style="width:40px" class="label_select">Стадия</div>
								<div class="select_block count">
									<?
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_cession_sell_var && $arrayLinkSearch['ready']){
										$currentParams = (int)$arrayLinkSearch['ready'];
										$valueParams = ' value="'.$arrayLinkSearch['ready'].'"';
									}
									$ready = '';
									$readyArray = array();
									$devs = mysql_query("
										SELECT *, (SELECT name FROM ".$template."_ready WHERE activation='1' && id='".$currentParams."') AS choose_value
										FROM ".$template."_ready
										WHERE activation='1'
										ORDER BY num
									");
									if(mysql_num_rows($devs)>0){
										$n = 0;
										while($dev = mysql_fetch_assoc($devs)){
											if($n==0){
												$current = '';
												if($currentParams==0){
													$current = ' class="current"';
													$nameParams = 'Не важно';
												}
												$ready .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
											}
											// else {
												$current = '';
												if($currentParams==$dev['id']){
													$current = ' class="current"';
													$nameParams = $dev['name'];
												}
												$ready .= '<li'.$current.'><a data-name="ready" data-explanation="" data-tags="'.clearValueText($dev['name']).'" href="javascript:void(0)" data-id="'.$dev['id'].'">'.$dev['name'].'</a></li>';
											// }
											$readyArray[$dev['id']] = $dev['name'];
											$n++;
										}
									}
									$readyChangeCessionSell = '';
									if($arrayLinkSearch['ready'] && $type_cession_sell_var){
										$c1 = $arrayLinkSearch['ready'];
										$readyChangeCessionSell = '<li><a class="min" data-name_select="ready" data-name="ready" data-type="checkbox_block" data-id="'.$arrayLinkSearch['ready'].'" href="javascript:void(0)">'.$readyArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									echo '<input type="hidden" name="ready"'.$valueParams.'>';
									echo '<div style="width:143px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $ready;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="metric">
							<!--<div class="select_block balcony">
								<?
								// $nameParams = 'Любой санузел';
								// $valueParams = '';
								// if($type_cession_sell_var && $arrayLinkSearch['wc']){
									// $nameParams = $_TYPE_WC[$arrayLinkSearch['wc']];
									// $valueParams = ' value="'.$arrayLinkSearch['wc'].'"';
								// }
								?>
								<input type="hidden" name="wc"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<?
										// for($c=0; $c<count($_TYPE_WC); $c++){
											// $current = '';
											// if(!$wc_cession_sell_var){
												// if($c==0){
													// $current = ' class="current"';
												// }
											// }
											// else {
												// if($c==$wc_cession_sell_var){
													// $current = ' class="current"';
												// }
											// }
											// echo '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_WC[$c].'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>-->
							<div style="width:67px;margin-left:-27px" class="label_select">Срок сдачи</div>
							<!--<div class="select_block count">
								<input type="hidden" name="delivery" value="<?=$delivery_cession_sell_var?>">
								<?
								// if(isset($delivery_cession_sell_var) && !empty($delivery_cession_sell_var)){
									// echo '<div style="width:95px" class="choosed_block">'.$delivery_cession_sell_var.'</div>';
								// }
								// else {
									// echo '<div style="width:95px" class="choosed_block">Не важно</div>';
								// }
								?>
								<div class="scrollbar-inner">
									<ul>
										<?
										// for($c=date("Y"); $c<=$_max_value_delivery; $c++){
											// $current = '';
											// $currentNull = '';
											// if(!$delivery_cession_sell_var){
												// if($c==date("Y")){
													// $currentNull = ' class="current"';
												// }
											// }
											// else {
												// if($c==$delivery_cession_sell_var){
													// $current = ' class="current"';
												// }
											// }
											// if($c==date("Y")){
												// echo '<li'.$currentNull.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
											// }
											// echo '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>-->
							
							
							
							
							
							<div class="select_block count">
								<?
								
								$ex_min_value_delivery_cession_sell = explode(' ',$min_value_delivery_cession_sell);
								$ex_max_value_delivery_cession_sell = explode(' ',$max_value_delivery_cession_sell);
								$_min_value_delivery_year = (int)$ex_min_value_delivery_cession_sell[1];
								$_max_value_delivery_year = (int)$ex_max_value_delivery_cession_sell[1];

								$n = 1;
								$deliveryAdd = '';
								$deliveryArrayMin = '';
								$_QUATRE = array("I","II","III","IV");
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['deliveryMin']){
									$currentParams = $arrayLinkSearch['deliveryMin'];
								}
								for($c=$_min_value_delivery_year; $c<=$_max_value_delivery_year; $c++){
									$min_search = array_search($ex_min_value_delivery_cession_sell[0],$_QUATRE);
									$max_search = array_search($ex_max_value_delivery_cession_sell[0],$_QUATRE);
									if($c==$_min_value_delivery_year){
										for($q=$min_search; $q<count($_QUATRE); $q++){
											$q_name = $_QUATRE[$q]." ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMin = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="Срок сдачи" data-tags="с '.$q_name.'" data-type="min" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
									else if($c==$_max_value_delivery_year){
										for($q=0; $q<=$max_search; $q++){
											$q_name = $_QUATRE[$q]." ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMin = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="Срок сдачи" data-tags="с '.$q_name.'" data-type="min" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
									else {
										for($q=0; $q<count($_QUATRE); $q++){
											$q_name = $_QUATRE[$q]." ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMin = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="Срок сдачи" data-tags="с '.$q_name.'" data-type="min" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
								}

								$nameParams = 'От';
								$valueParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['deliveryMin']){
									$nameParams = $arrayLinkSearch['deliveryMin'];
									if(!empty($deliveryArrayMin)){
										$nameParams = $deliveryArrayMin;
									}
									$currentParams = $arrayLinkSearch['deliveryMin'];
									$valueParams = ' value="'.$arrayLinkSearch['deliveryMin'].'"';
								}
								
								?>
								<input type="hidden" name="deliveryMin"<?=$valueParams?>>
								<div style="width:143px" class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?=$deliveryAdd?>
									</ul>
								</div>
							</div>
							<div style="margin:4px 4px 0" class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$n = 1;
								$deliveryAdd = '';
								$deliveryArrayMax = '';
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['deliveryMax']){
									$currentParams = $arrayLinkSearch['deliveryMax'];
								}
								for($c=$_min_value_delivery_year; $c<=$_max_value_delivery_year; $c++){
									$min_search = array_search($ex_min_value_delivery_cession_sell[0],$_QUATRE);
									$max_search = array_search($ex_max_value_delivery_cession_sell[0],$_QUATRE);
									if($c==$_min_value_delivery_year){
										for($q=$min_search; $q<count($_QUATRE); $q++){
											$q_name = $_QUATRE[$q]." ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMax = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="Срок сдачи по" data-tags="по '.$q_name.'" data-type="max" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
									else if($c==$_max_value_delivery_year){
										for($q=0; $q<=$max_search; $q++){
											$q_name = $_QUATRE[$q]." ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMax = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="Срок сдачи" data-tags="по '.$q_name.'" data-type="max" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
									else {
										for($q=0; $q<count($_QUATRE); $q++){
											$q_name = $_QUATRE[$q]." ".$c;
											$current = "";
											if($n==$currentParams){
												$current = ' class="current"';
												$deliveryArrayMax = $q_name;
											}
											$deliveryAdd .= '<li'.$current.'><a data-name="delivery" data-explanation="Срок сдачи" data-tags="по '.$q_name.'" data-type="max" href="javascript:void(0)" data-id="'.$n.'">'.$q_name.'</a></li>';
											$n++;
										}
									}
								}
								
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_cession_sell_var && $arrayLinkSearch['deliveryMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['deliveryMax']);
									if(!empty($deliveryArrayMax)){
										$nameParams = $deliveryArrayMax;
									}
									$currentParams = $arrayLinkSearch['deliveryMax'];
									$valueParams = ' value="'.$arrayLinkSearch['deliveryMax'].'"';
								}
								
								$deliveryChangeCessionSell = '';
								if($arrayLinkSearch['deliveryMin'] && !$arrayLinkSearch['deliveryMax'] && $type_cession_sell_var){ //только метраж "от"
									$c1 = $deliveryArrayMin;
									$deliveryChangeCessionSell = '<li><a class="min" data-name_select="delivery" data-name="deliveryMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['deliveryMin'].'" href="javascript:void(0)">Срок сдачи '.str_replace(".", ",", $c1).'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['deliveryMin'] && $arrayLinkSearch['deliveryMax'] && $type_cession_sell_var){ //только метраж "до"
									$c2 = $deliveryArrayMax;
									$deliveryChangeCessionSell = '<li><a class="max" data-name_select="delivery" data-name="deliveryMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['deliveryMax'].'" href="javascript:void(0)">Срок сдачи '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['deliveryMin'] && $arrayLinkSearch['deliveryMax'] && $type_cession_sell_var){ //промежуток метражей
									$c1 = $deliveryArrayMin;
									$c2 = $deliveryArrayMax;
									$deliveryChangeCessionSell = '<li><a class="min max" data-name_select="delivery" data-name="deliveryMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['deliveryMin'].'" href="javascript:void(0)">Срок сдачи с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								?>
								<input type="hidden" name="deliveryMax"<?=$valueParams?>>
								<div style="width:143px" class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?=$deliveryAdd?>
									</ul>
								</div>
							</div>
							
							
							
							
							
							
						</div>
						<div class="clear"></div>
						<div class="metric">
							<div class="cell">
								<div style="width:46px;margin-left:-6px;" class="label_select">Отделка</div>
								<div class="select_block count">
									<?
									$finishingChangeCessionSell = '';
									if($arrayLinkSearch['finishing'] && $finishing_cession_sell_var){
										$c1 = $arrayLinkSearch['finishing'];
										$finishingChangeCessionSell = '<li><a class="min" data-name_select="finishing" data-name="finishing" data-type="checkbox_block" data-id="'.$arrayLinkSearch['finishing'].'" href="javascript:void(0)">'.$_TYPE_FINISHING[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($finishing_cession_sell_var && $finishing_cession_sell_var){
										$currentParams = (int)$finishing_cession_sell_var;
										$valueParams = ' value="'.$finishing_cession_sell_var.'"';
									}
									$finishing = '';
									for($n=0; $n<count($_TYPE_FINISHING); $n++){
										if($n==0){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = $_TYPE_FINISHING[0];
											}
											$finishing .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_TYPE_FINISHING[0].'</a></li>';
										}
										else {
											$current = '';
											if($currentParams==$n){
												$current = ' class="current"';
												$nameParams = $_TYPE_FINISHING[$n];
											}
											$tooltip = '';
											if(!empty($_TYPE_FIN_DESCR[$n])){
												$tooltip = ' data-much="true" data-pos="left" data-hover="tooltip" data-title="'.$_TYPE_FIN_DESCR[$n].'"';
											}
											$finishing .= '<li'.$current.'><a'.$tooltip.' data-name="finishing" data-explanation="" data-tags="'.clearValueText($_TYPE_FINISHING[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_FINISHING[$n].'</a></li>';
										}
									}
									echo '<input type="hidden" name="finishing"'.$valueParams.'>';
									echo '<div style="width:143px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $finishing;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<div style="margin-left:13px" class="cell">
								<div class="select_block count">
									<?
									$far_subwayChangeCessionSell = '';
									if($arrayLinkSearch['far_subway'] && $type_cession_sell_var){
										$c1 = $arrayLinkSearch['far_subway'];
										$name_value = $_FAR_SUBWAY[$c1];
										if($c1==1){
											$name_value = '< 500 м';
										}
										$far_subwayChangeCessionSell = '<li><a class="min" data-name_select="far_subway" data-name="far_subway" data-type="checkbox_block" data-id="'.$arrayLinkSearch['far_subway'].'" href="javascript:void(0)">Метро '.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									
									$nameParams = 'Метро не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_cession_sell_var && $arrayLinkSearch['far_subway']){
										$currentParams = (int)$arrayLinkSearch['far_subway'];
										$valueParams = ' value="'.$arrayLinkSearch['far_subway'].'"';
									}
									$far_subway = '';
									for($f=1; $f<=count($_FAR_SUBWAY); $f++){
										if($f==1){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = 'Метро не важно';
											}
											$far_subway .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Метро не важно</a></li>';
										}
										$current = '';
										if($currentParams==$f){
											$current = ' class="current"';
											$nameParams = $_FAR_SUBWAY[$f];
										}
										$name_value = $_FAR_SUBWAY[$f];
										if($f==1){
											$name_value = '< 500 м';
										}
										$far_subway .= '<li'.$current.'><a data-name="far_subway" data-explanation="Метро" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'">'.$_FAR_SUBWAY[$f].'</a></li>';
									}
									echo '<input type="hidden" name="far_subway"'.$valueParams.'>';
									echo '<div style="width:143px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $far_subway;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<!--<div class="select_block balcony">
								<input type="hidden" name="balcony" value="0">
								<div class="choosed_block">Балкон не важен</div>
								<div class="scrollbar-inner">
									<ul>
										<?
										// for($c=0; $c<count($_TYPE_BALCONY); $c++){
											// $current = '';
											// if(!$balcony_cession_sell_var){
												// if($c==0){
													// $current = ' class="current"';
												// }
											// }
											// else {
												// if($c==$balcony_cession_sell_var){
													// $current = ' class="current"';
												// }
											// }
											// echo '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_BALCONY[$c].'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>-->
						</div>
					</div>
				</div>
			</div>
			<div <?$estateValue!=2?print'style="display:none"':'style="display:block"'?> class="inner_block flat">
				<div class="row rent">
					<div class="label_select" style="
						float: left;
						width: 105px;
						width: 110px;
						margin-right: -30px;
						margin-top: 5px;
						text-align: right;
					">Срок аренды</div>
					<div class="select_block type">
						<?
						$periodChangeFlatRent = '';
						if($arrayLinkSearch['period'] && $type_flat_rent_var){
							$c1 = $arrayLinkSearch['period'];
							$periodChangeFlatRent = '<li><a class="min" data-name_select="period" data-name="period" data-type="checkbox_block" data-id="'.$arrayLinkSearch['period'].'" href="javascript:void(0)">'.$_TYPE_PERIOD_RENT[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
						}
						
						$nameParams = 'Не важно';
						$valueParams = '';
						$currentParams = '';
						if($type_flat_rent_var && $arrayLinkSearch['period']){
							$currentParams = (int)$arrayLinkSearch['period'];
							$valueParams = ' value="'.$arrayLinkSearch['period'].'"';
						}
						$period = '';
						for($n=0; $n<count($_TYPE_PERIOD_RENT); $n++){
							if($n==0){
								$current = '';
								if($currentParams==0){
									$current = ' class="current"';
									$nameParams = $_TYPE_PERIOD_RENT[0];
								}
								$period .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_TYPE_PERIOD_RENT[0].'</a></li>';
							}
							else {
								$current = '';
								if($currentParams==$n){
									$current = ' class="current"';
									$nameParams = $_TYPE_PERIOD_RENT[$n];
								}
								$period .= '<li'.$current.'><a data-name="period" data-explanation="" data-tags="'.clearValueText($_TYPE_PERIOD_RENT[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_PERIOD_RENT[$n].'</a></li>';
							}
						}
						echo '<input type="hidden" name="period"'.$valueParams.'>';
						echo '<div class="choosed_block">'.$nameParams.'</div>';
						echo '<div class="scrollbar-inner">';
						echo '<ul>';
						echo $period;
						echo '</ul>';
						echo '</div>';
						?>
					</div>
					<div class="label_select" style="
						float: left;
						width: 82px;
						margin-right: -30px;
						margin-top: 5px;
						text-align: right;
					">Комиссия</div>
					<div class="select_block type">
						<?
						$prepaymentChangeFlatRent = '';
						if($arrayLinkSearch['prepayment'] && $type_flat_rent_var){
							$c1 = $arrayLinkSearch['prepayment'];
							$prepaymentChangeFlatRent = '<li><a class="min" data-name_select="prepayment" data-name="prepayment" data-type="checkbox_block" data-id="'.$arrayLinkSearch['prepayment'].'" href="javascript:void(0)">'.$_TYPE_PREPAYMENT[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
						}
						
						$nameParams = 'Не важно';
						$valueParams = '';
						$currentParams = '';
						if($type_flat_rent_var && $arrayLinkSearch['prepayment']){
							$currentParams = (int)$arrayLinkSearch['prepayment'];
							$valueParams = ' value="'.$arrayLinkSearch['prepayment'].'"';
						}
						$prepayment = '';
						for($n=0; $n<count($_TYPE_PREPAYMENT); $n++){
							if($n==0){
								$current = '';
								if($currentParams==0){
									$current = ' class="current"';
									$nameParams = $_TYPE_PREPAYMENT[0];
								}
								$prepayment .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_TYPE_PREPAYMENT[0].'</a></li>';
							}
							else {
								$current = '';
								if($currentParams==$n){
									$current = ' class="current"';
									$nameParams = $_TYPE_PREPAYMENT[$n];
								}
								$prepayment .= '<li'.$current.'><a data-name="prepayment" data-explanation="" data-tags="'.clearValueText($_TYPE_PREPAYMENT[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_PREPAYMENT[$n].'</a></li>';
							}
						}
						echo '<input type="hidden" name="prepayment"'.$valueParams.'>';
						echo '<div class="choosed_block">'.$nameParams.'</div>';
						echo '<div class="scrollbar-inner">';
						echo '<ul>';
						echo $prepayment;
						echo '</ul>';
						echo '</div>';
						?>
					</div>
					
					
					<?
					$checked = '';
					$valueParams = '';
					$disabled = ' disabled="disabled"';
					if(isset($deposit_flat_rent_var)){
						if($deposit_flat_rent_var==1){
							$disabled = '';
							$checked = ' checked';
							$valueParams = ' value="'.$deposit_flat_rent_var.'"';
						}
					}
					?>
					<div style="float:left;margin:5px 0 0 40px" class="checkbox_single<?=$checked?>">
						<input type="hidden"<?=$valueParams?> name="deposit"<?=$disabled?>>
						<div class="container">
							<span class="check"></span><span class="label">Без залога</span>
						</div>
					</div>
					<div class="can_block">
						<?
						$checked = '';
						$valueParams = '';
						$disabled = ' disabled="disabled"';
						if(isset($animal_flat_rent_var)){
							if($animal_flat_rent_var==1){
								$disabled = '';
								$checked = ' checked';
								$valueParams = ' value="'.$animal_flat_rent_var.'"';
							}
						}
						?>
						<div class="checkbox_single<?=$checked?>">
							<input type="hidden"<?=$valueParams?> name="animal"<?=$disabled?>>
							<div class="container">
								<span class="check"></span><span class="label">Можно с животным</span>
							</div>
						</div>
						<?
						$checked = '';
						$valueParams = '';
						$disabled = ' disabled="disabled"';
						if(isset($children_flat_rent_var)){
							if($children_flat_rent_var==1){
								$disabled = '';
								$checked = ' checked';
								$valueParams = ' value="'.$children_flat_rent_var.'"';
							}
						}
						?>
						<div class="checkbox_single<?=$checked?>">
							<input type="hidden"<?=$valueParams?> name="children"<?=$disabled?>>
							<div class="container">
								<span class="check"></span><span class="label">Можно с детьми</span>
							</div>
						</div>
					</div>
				</div>
				<div class="separate"></div>
				<div class="row sell">
					<div class="left_block">
						<div class="metric">
							<div style="width:105px" class="label_select">Общая площадь</div>
							<div class="select_block price reg ch_price">
							<?
							$squareChangeFlatSell = '';
							if($arrayLinkSearch['squareFullMin'] && !$arrayLinkSearch['squareFullMax'] && $type_flat_sell_var){ //только метраж "от"
								$c1 = $arrayLinkSearch['squareFullMin'];
								$squareChangeFlatSell = '<li><a class="min" data-name_select="fullsquareflatsell" data-name="squareFullMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(!$arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_flat_sell_var){ //только метраж "до"
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeFlatSell = '<li><a class="max" data-name_select="fullsquareflatsell" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMax'].'" href="javascript:void(0)">общая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if($arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_flat_sell_var){ //промежуток метражей
								$c1 = $arrayLinkSearch['squareFullMin'];
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeFlatSell = '<li><a class="min max" data-name_select="fullsquareflatsell" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}

							$nameParams = 'От';
							$valueParams = '';
							$currentParams = '';
							if($type_flat_sell_var && $arrayLinkSearch['squareFullMin']){
								$nameParams = $arrayLinkSearch['squareFullMin'];
								$currentParams = $arrayLinkSearch['squareFullMin'];
								$valueParams = ' value="'.$arrayLinkSearch['squareFullMin'].'"';
							}
							?>
							<input type="hidden" name="squareFullMin"<?=$valueParams?>>
							<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareFullMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_full_square_flat_sell; $c<=$max_full_square_flat_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquareflatsell" data-explanation="общая" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_flat_sell){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_flat_sell){
												$c = $max_full_square_flat_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price reg ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_sell_var && $arrayLinkSearch['squareFullMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareFullMax']);
									$currentParams = $arrayLinkSearch['squareFullMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMax'].'"';
								}
								?>
								<input type="hidden" name="squareFullMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareFullMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_full_square_flat_sell; $c<=$max_full_square_flat_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquareflatsell" data-explanation="общая" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_flat_sell){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_flat_sell){
												$c = $max_full_square_flat_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div style="width:105px" class="label_select">Жилая</div>
							<div class="select_block price reg ch_price">
								<?
								$squareLiveChangeFlatSell = '';
								if($arrayLinkSearch['squareLiveMin'] && !$arrayLinkSearch['squareLiveMax'] && $type_flat_sell_var){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareLiveMax'];
									$squareLiveChangeFlatSell = '<li><a class="min" data-name_select="livesquareflatsell" data-name="squareLiveMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareLiveMin'].'" href="javascript:void(0)">жилая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareLiveMin'] && $arrayLinkSearch['squareLiveMax'] && $type_flat_sell_var){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareLiveMax'];
									$squareLiveChangeFlatSell = '<li><a class="max" data-name_select="livesquareflatsell" data-name="squareLiveMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareLiveMax'].'" href="javascript:void(0)">жилая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareLiveMin'] && $arrayLinkSearch['squareLiveMax'] && $type_flat_sell_var){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareLiveMin'];
									$c2 = $arrayLinkSearch['squareLiveMax'];
									$squareLiveChangeFlatSell = '<li><a class="min max" data-name_select="livesquareflatsell" data-name="squareLiveMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareLiveMin'].'" href="javascript:void(0)">жилая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_sell_var && $arrayLinkSearch['squareLiveMin']){
									$nameParams = $arrayLinkSearch['squareLiveMin'];
									$currentParams = $arrayLinkSearch['squareLiveMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareLiveMin'].'"';
								}
								?>
								<input type="hidden" name="squareLiveMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareLiveMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_live_square_flat_sell; $c<=$max_live_square_flat_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="livesquareflatsell" data-explanation="жилая" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_live_square_flat_sell){
												break;
											}
											$c = $c+1;
											if($c>$max_live_square_flat_sell){
												$c = $max_live_square_flat_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price reg ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_sell_var && $arrayLinkSearch['squareLiveMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareLiveMax']);
									$currentParams = $arrayLinkSearch['squareLiveMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareLiveMax'].'"';
								}
								?>
								<input type="hidden" name="squareLiveMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareLiveMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_live_square_flat_sell; $c<=$max_live_square_flat_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="livesquareflatsell" data-explanation="жилая" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_live_square_flat_sell){
												break;
											}
											$c = $c+1;
											if($c>$max_live_square_flat_sell){
												$c = $max_live_square_flat_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div style="width:105px" class="label_select">Кухня</div>
							<div class="select_block price reg ch_price">
								<?
								$squareKitchenChangeFlatSell = '';
								if($arrayLinkSearch['squareKitchenMin'] && !$arrayLinkSearch['squareKitchenMax'] && $type_flat_sell_var){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeFlatSell = '<li><a class="min" data-name_select="kitchensquareflatsell" data-name="squareKitchenMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMin'].'" href="javascript:void(0)">кухня от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareKitchenMin'] && $arrayLinkSearch['squareKitchenMax'] && $type_flat_sell_var){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeFlatSell = '<li><a class="max" data-name_select="kitchensquareflatsell" data-name="squareKitchenMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMax'].'" href="javascript:void(0)">кухня до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareKitchenMin'] && $arrayLinkSearch['squareKitchenMax'] && $type_flat_sell_var){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareKitchenMin'];
									$c2 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeFlatSell = '<li><a class="min max" data-name_select="kitchensquareflatsell" data-name="squareKitchenMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMin'].'" href="javascript:void(0)">кухня от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_sell_var && $arrayLinkSearch['squareKitchenMin']){
									$nameParams = $arrayLinkSearch['squareKitchenMin'];
									$currentParams = $arrayLinkSearch['squareKitchenMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareKitchenMin'].'"';
								}
								?>
								<input type="hidden" name="squareKitchenMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareKitchenMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_kitchen_square_flat_sell; $c<=$max_kitchen_square_flat_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="kitchensquareflatsell" data-explanation="кухня" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_kitchen_square_flat_sell){
												break;
											}
											$c = $c+1;
											if($c>$max_kitchen_square_flat_sell){
												$c = $max_kitchen_square_flat_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price reg ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_sell_var && $arrayLinkSearch['squareKitchenMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareKitchenMax']);
									$currentParams = $arrayLinkSearch['squareKitchenMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareKitchenMax'].'"';
								}
								?>
								<input type="hidden" name="squareKitchenMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="Ло" type="text" value="<?=$arrayLinkSearch['squareKitchenMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_kitchen_square_flat_sell; $c<=$max_kitchen_square_flat_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="kitchensquareflatsell" data-explanation="кухня" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_kitchen_square_flat_sell){
												break;
											}
											$c = $c+1;
											if($c>$max_kitchen_square_flat_sell){
												$c = $max_kitchen_square_flat_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<!--<div style="width:80px;margin-left:-30px;margin-top:-2px;" class="label_select">Дата публикации</div>-->
							<div style="margin-left:-5px" class="label_select">Дата публикации</div>
							<div class="select_block count">
								<?
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_sell_var && $arrayLinkSearch['dates']){
									$currentParams = (int)$arrayLinkSearch['dates'];
									$valueParams = ' value="'.$arrayLinkSearch['dates'].'"';
								}
								$dates = '';
								$datesArray = array();
								for($n=0; $n<count($_DATE_POST); $n++){
									if($n==0){
										$current = '';
										if($currentParams=='' && !isset($arrayLinkSearch['dates'])){
											$current = ' class="current"';
											$nameParams = $_DATE_POST[0]['name'];
										}
										$dates .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_DATE_POST[0]['name'].'</a></li>';
									}
									else {
										$current = '';
										if($currentParams==$_DATE_POST[$n]['id']){
											$current = ' class="current"';
											$nameParams = $_DATE_POST[$n]['name'];
										}
										$dates .= '<li'.$current.'><a data-name="dates" data-explanation="" data-tags="'.$_DATE_POST[$n]['name'].'" href="javascript:void(0)" data-id="'.$_DATE_POST[$n]['id'].'">'.$_DATE_POST[$n]['name'].'</a></li>';
										$datesArray[$_DATE_POST[$n]['id']] = $_DATE_POST[$n]['name'];	
									}
								}
								$datesChangeFlatSell = '';
								if($arrayLinkSearch['dates'] && $type_flat_sell_var){
									$c1 = $arrayLinkSearch['dates'];
									$datesChangeFlatSell = '<li><a class="min" data-name_select="dates" data-name="dates" data-type="checkbox_block" data-id="'.$arrayLinkSearch['dates'].'" href="javascript:void(0)">'.$datesArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								echo '<input type="hidden" name="dates"'.$valueParams.'>';
								echo '<div style="width:158px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $dates;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
					</div>
					<div class="center_block">
						<div class="floor">
							<div class="label_select">Этаж</div>
							<div class="select_block price sec ch_price">
								<?
								$floorChangeFlatSell = '';
								if($arrayLinkSearch['floorMin'] && !$arrayLinkSearch['floorMax'] && $type_flat_sell_var){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorMin'];
									$floorChangeFlatSell = '<li><a class="min" data-name_select="floorsquareflatsell" data-name="floorMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_flat_sell_var){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeFlatSell = '<li><a class="max" data-name_select="floorsquareflatsell" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMax'].'" href="javascript:void(0)">этаж по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_flat_sell_var){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorMin'];
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeFlatSell = '<li><a class="min max" data-name_select="floorsquareflatsell" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_sell_var && $arrayLinkSearch['floorMin']){
									$nameParams = $arrayLinkSearch['floorMin'];
									$currentParams = $arrayLinkSearch['floorMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMin'].'"';
								}
								?>
								<input type="hidden" name="floorMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="2" placeholder="От" type="text" value="<?=$arrayLinkSearch['floorMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$max_floors_flat_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquareflatsell" data-explanation="этаж" data-tags="с '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price sec ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_sell_var && $arrayLinkSearch['floorMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorMax']);
									$currentParams = $arrayLinkSearch['floorMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMax'].'"';
								}
								?>
								<input type="hidden" name="floorMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="2" placeholder="До" type="text" value="<?=$arrayLinkSearch['floorMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_flat_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquareflatsell" data-explanation="этаж" data-tags="по '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<!--<div class="floor">
							<div class="label_select">в доме</div>
							<div class="select_block count">
								<?
								// $floorsChangeFlatSell = '';
								// if($arrayLinkSearch['floorsMin'] && !$arrayLinkSearch['floorsMax'] && $type_flat_sell_var){ //только этажи "с"
									// $c1 = $arrayLinkSearch['floorsMin'];
									// $floorsChangeFlatSell = '<li><a class="min" data-name_select="floorscountflatsell" data-name="floorsMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">в доме от '.$c1.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								// }
								// if(!$arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $type_flat_sell_var){ //только этажи "по"
									// $c2 = $arrayLinkSearch['floorsMax'];
									// $floorsChangeFlatSell = '<li><a class="max" data-name_select="floorscountflatsell" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMax'].'" href="javascript:void(0)">в доме по '.$c2.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								// }
								// if($arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $type_flat_sell_var){ //промежуток этажей
									// $c1 = $arrayLinkSearch['floorsMin'];
									// $c2 = $arrayLinkSearch['floorsMax'];
									// $floorsChangeFlatSell = '<li><a class="min max" data-name_select="floorscountflatsell" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">в доме от '.$c1.' по '.$c2.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								// }

								// $nameParams = 'От';
								// $valueParams = '';
								// $currentParams = '';
								// if($type_flat_sell_var && $arrayLinkSearch['floorsMin']){
									// $nameParams = $arrayLinkSearch['floorsMin'];
									// $currentParams = $arrayLinkSearch['floorsMin'];
									// $valueParams = ' value="'.$arrayLinkSearch['floorsMin'].'"';
								// }
								?>
								<input type="hidden" name="floorsMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										// for($f=1; $f<=$max_floors_flat_sell; $f++){
											// $current = "";
											// if($f==$currentParams){
												// $current = ' class="current"';
											// }
											// echo '<li'.$current.'><a data-name="floorscountflatsell" data-explanation="в доме" data-tags="от '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								// $nameParams = 'До';
								// $valueParams = '';
								// $currentParams = '';
								// if($type_flat_sell_var && $arrayLinkSearch['floorsMax']){
									// $nameParams = str_replace(".", ",", $arrayLinkSearch['floorsMax']);
									// $currentParams = $arrayLinkSearch['floorsMax'];
									// $valueParams = ' value="'.$arrayLinkSearch['floorsMax'].'"';
								// }
								?>
								<input type="hidden" name="floorsMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										// for($f=1; $f<=$max_floors_flat_sell; $f++){
											// $current = "";
											// if($f==$currentParams){
												// $current = ' class="current"';
											// }
											// echo '<li'.$current.'><a data-name="floorscountflatsell" data-explanation="в доме" data-tags="до '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>
							<div class="com">этажей</div>
						</div>-->
						<div class="floor">
							<div style="margin-left:53px" class="check_block">
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_first_flat_sell_var)){
									if($except_first_flat_sell_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_first_flat_sell_varexcept_first_flat_sell_var.'"';
									}
								}
								?>
								<div style="float:none" class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_first"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме первого</span>
									</div>
								</div>
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_last_flat_sell_var)){
									if($except_last_flat_sell_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_last_flat_sell_var.'"';
									}
								}
								?>
								<div style="margin:23px 0 0 8px" class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_last"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме последнего</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="line"></div>
					
					
					<div class="right_block">
						<div class="metric">
							<div class="cell">
								<div style="width:46px;margin-left:-6px;" class="label_select">Ремонт</div>
								<div class="select_block count multiple">
									<?
									$repairsChangeFlatSell = '';
									$ex_array = explode(',',$arrayLinkSearch['repairs']);
									if($arrayLinkSearch['repairs'] && $type_flat_sell_var){
										$c1 = $arrayLinkSearch['repairs'];
										// $repairsChangeFlatSell = '<li><a class="min" data-name_select="repairs" data-name="repairs" data-type="checkbox_block" data-id="'.$arrayLinkSearch['repairs'].'" data-multiple="true" href="javascript:void(0)">'.$_REPAIRS[$ex_c1[$e]].'<span onclick="return delTags(this)" class="close"></span></a></li>';
										
										$ex_c1 = explode(',',$c1);
										for($e=0; $e<count($ex_c1); $e++){
											$repairsChangeFlatSell .= '<li><a class="min" data-name_select="repairs" data-name="repairs" data-type="checkbox_block" data-id="'.$ex_c1[$e].'" data-multiple="true" href="javascript:void(0)">'.$_REPAIRS[$ex_c1[$e]].'<span onclick="return delTags(this)" class="close"></span></a></li>';
										}
									}
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_flat_sell_var && $arrayLinkSearch['repairs']){
										$currentParams = (int)$arrayLinkSearch['repairs'];
										$valueParams = ' value="'.$arrayLinkSearch['repairs'].'"';
									}
									$repairs = '';
									for($n=0; $n<count($_REPAIRS); $n++){
										if($n==0){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = $_REPAIRS[0];
											}
											// $repairs .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_REPAIRS[0].'</a></li>';
										}
										else {
											$current = '';
											// if($currentParams==$n){
												// $current = ' class="current"';
												// $nameParams = $_REPAIRS[$n];
											// }
											if(in_array($n,$ex_array)){
												$current = ' class="current"';
												$nameParams = $_REPAIRS[$n];
											}
											$tooltip = '';
											if(!empty($_REPAIRS_DESCR[$n])){
												$tooltip = ' data-much="true" data-pos="left" data-hover="tooltip" data-title="'.$_REPAIRS_DESCR[$n].'"';
											}
											$repairs .= '<li'.$current.'><a'.$tooltip.' data-name="repairs" data-explanation="" data-tags="'.clearValueText($_REPAIRS[$n]).'" href="javascript:void(0)" data-id="'.$n.'"><span class="check"></span><span class="label">'.$_REPAIRS[$n].'</span></a></li>';
										}
									}
									if(count($ex_array)>1){
										$nameParams = count($ex_array).' параметра';
									}
									echo '<input type="hidden" name="repairs"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $repairs;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<div class="cell">
								<div style="width:60px;margin-left:0;" class="label_select">Санузел</div>
								<div class="select_block balcony multiple">
									<?
									$wcChangeFlatSell = '';
									$ex_array = explode(',',$arrayLinkSearch['wc']);
									if(isset($arrayLinkSearch['wc']) && $type_flat_sell_var){
										$c1 = $arrayLinkSearch['wc'];
										$ex_c1 = explode(',',$c1);
										for($e=0; $e<count($ex_c1); $e++){
											$wcChangeFlatSell .= '<li><a class="min" data-name_select="wc" data-name="wc" data-type="checkbox_block" data-id="'.$ex_c1[$e].'" data-multiple="true" href="javascript:void(0)">'.$_TYPE_WC[$ex_c1[$e]].'<span onclick="return delTags(this)" class="close"></span></a></li>';
										}
									}
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_flat_sell_var && isset($arrayLinkSearch['wc'])){
										$currentParams = (int)$arrayLinkSearch['wc'];
										$valueParams = ' value="'.$arrayLinkSearch['wc'].'"';
									}
									$wc = '';
									for($n=1; $n<count($_TYPE_WC); $n++){
										if($n==0){
											$current = '';
											$current2 = '';
											// if($arrayLinkSearch['wc']==0 && isset($arrayLinkSearch['wc'])){
												// $current = ' class="current"';
												// $nameParams = 'Нет';
											// }
											if($currentParams=='' && !isset($arrayLinkSearch['wc'])){
												$current2 = ' class="current"';
												$nameParams = $_TYPE_WC[0];
											}
											// $wc .= '<li'.$current2.'><a data-name="wc" data-explanation="" data-tags="Санузел не важен" href="javascript:void(0)" data-id="0"><span class="check"></span><span class="label">'.$_TYPE_WC[0].'</span></a></li>';
											// $wc .= '<li'.$current.'><a data-name="wc" data-explanation="" data-tags="Нет санузла" href="javascript:void(0)" data-id="0">Нет</a></li>';
										}
										else {
											$current = '';
											// if($currentParams==$n){
												// $current = ' class="current"';
												// $nameParams = $_TYPE_WC[$n];
											// }
											if(in_array($n,$ex_array)){
												$current = ' class="current"';
												$nameParams = $_TYPE_WC[$n];
											}
											$wc .= '<li'.$current.'><a data-name="wc" data-explanation="" data-tags="'.clearValueText($_TYPE_WC[$n]).'" href="javascript:void(0)" data-id="'.$n.'"><span class="check"></span><span class="label">'.$_TYPE_WC[$n].'</span></a></li>';
										}
									}
									if(count($ex_array)>1){
										$nameParams = count($ex_array).' параметра';
									}
									echo '<input type="hidden" name="wc"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $wc;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="metric">
							<div class="cell">
								<div style="width:55px;margin-left:-15px;" class="label_select">Тип дома</div>
								<div class="select_block count multiple">
									<?
									
									$ex_array = array(); 
									$type_houseChangeFlatSell = '';
									if($arrayLinkSearch['type_house'] && $type_flat_sell_var){
										$ex_array = explode(',',$arrayLinkSearch['type_house']); 
										$c1 = $arrayLinkSearch['type_house'];
										$ex_c1 = explode(',',$c1);
										$arrTypesHouses = array();
										$res = mysql_query("SELECT * FROM ".$template."_type_house WHERE activation='1' ORDER BY num");
										if(mysql_num_rows($res)>0){
											while($row = mysql_fetch_assoc($res)){
												$arrTypesHouses[$row['id']]=$row['name'];
											}
										}
										for($e=0; $e<count($ex_c1); $e++){
											$type_houseChangeFlatSell .= '<li><a class="min" data-name_select="type_house" data-name="type_house" data-type="checkbox_block" data-id="'.$ex_c1[$e].'" data-multiple="true" href="javascript:void(0)">'.$arrTypesHouses[$ex_c1[$e]].'<span onclick="return delTags(this)" class="close"></span></a></li>';
										}
									}

									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_flat_sell_var && $arrayLinkSearch['type_house']){
										$currentParams = (int)$arrayLinkSearch['type_house'];
										$valueParams = ' value="'.$arrayLinkSearch['type_house'].'"';
									}
									
									$type_house = '';
									$type_houseArray = array();
									$devs = mysql_query("
										SELECT *, (SELECT name FROM ".$template."_type_house WHERE activation='1' && id='".$currentParams."') AS choose_value
										FROM ".$template."_type_house
										WHERE activation='1'
										ORDER BY name
									");
									if(mysql_num_rows($devs)>0){
										$n = 0;
										while($dev = mysql_fetch_assoc($devs)){
											if($n==0){
												$current = '';
												if($currentParams==0){
													$current = ' class="current"';
													$nameParams = 'Не важно';
												}
												// $type_house .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
											}
											else {
												$current = '';
												if(in_array($dev['id'],$ex_array) && $type_flat_sell_var){
													$current = ' class="current"';
													$nameParams = $dev['name'];
												}
												$type_house .= '<li'.$current.'><a data-name="type_house" data-explanation="" data-tags="'.clearValueText($dev['name']).'" href="javascript:void(0)" data-id="'.$dev['id'].'"><span class="check"></span><span class="label">'.$dev['name'].'</span></a></li>';
												$type_houseArray[$dev['id']] = clearValueText($dev['name']);
											}
											$n++;
										}
									}

									if(count($ex_array)>1){
										$nameParams = count($ex_array).' параметра';
									}
									echo '<input type="hidden" name="type_house"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $type_house;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<div class="cell">
								<div style="width:60px" class="label_select">До метро</div>
								<div class="select_block count">
									<?
									$far_subwayChangeFlatSell = '';
									if($arrayLinkSearch['far_subway'] && $type_flat_sell_var){
										$c1 = $arrayLinkSearch['far_subway'];
										$name_value = $_FAR_SUBWAY[$c1];
										if($c1==1){
											$name_value = '< 500 м';
										}
										$far_subwayChangeFlatSell = '<li><a class="min" data-name_select="far_subway" data-name="far_subway" data-type="checkbox_block" data-id="'.$arrayLinkSearch['far_subway'].'" href="javascript:void(0)">Метро '.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_flat_sell_var && $arrayLinkSearch['far_subway']){
										$currentParams = (int)$arrayLinkSearch['far_subway'];
										$valueParams = ' value="'.$arrayLinkSearch['far_subway'].'"';
									}
									$far_subway = '';
									for($f=1; $f<=count($_FAR_SUBWAY); $f++){
										if($f==1){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = 'Не важно';
											}
											$far_subway .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
										}
										$current = '';
										if($currentParams==$f){
											$current = ' class="current"';
											$nameParams = $_FAR_SUBWAY[$f];
										}
										$name_value = $_FAR_SUBWAY[$f];
										if($f==1){
											$name_value = '< 500 м';
										}
										$far_subway .= '<li'.$current.'><a data-name="far_subway" data-explanation="Метро" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'">'.$_FAR_SUBWAY[$f].'</a></li>';
									}
									echo '<input type="hidden" name="far_subway"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $far_subway;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<!--<div class="select_block balcony">
								<input type="hidden" name="balcony" value="0">
								<div class="choosed_block">Балкон не важен</div>
								<div class="scrollbar-inner">
									<ul>
										<?
										// for($c=0; $c<count($_TYPE_BALCONY); $c++){
											// $current = '';
											// if(!$balcony_new_sell_var){
												// if($c==0){
													// $current = ' class="current"';
												// }
											// }
											// else {
												// if($c==$balcony_new_sell_var){
													// $current = ' class="current"';
												// }
											// }
											// echo '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_BALCONY[$c].'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>-->
						</div>
						<div class="clear"></div>
						<div class="metric">
							<!--<div class="cell">
								<div style="width:70px;margin-left:-30px;" class="label_select">Тип сделки</div>
								<div class="select_block count">
									<?
									// $transactionChangeFlatSell = '';
									// if($arrayLinkSearch['transaction'] && $type_flat_sell_var){
										// $c1 = $arrayLinkSearch['transaction'];
										// $transactionChangeFlatSell = '<li><a class="min" data-name_select="transaction" data-name="transaction" data-type="checkbox_block" data-id="'.$arrayLinkSearch['transaction'].'" href="javascript:void(0)">'.$_TYPE_DEAL[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									// }
									
									// $nameParams = 'Не важно';
									// $valueParams = '';
									// $currentParams = '';
									// if($type_flat_sell_var && $arrayLinkSearch['transaction']){
										// $currentParams = (int)$arrayLinkSearch['transaction'];
										// $valueParams = ' value="'.$arrayLinkSearch['transaction'].'"';
									// }
									// $transaction = '';
									// for($n=0; $n<count($_TYPE_DEAL); $n++){
										// if($n==0){
											// $current = '';
											// if($currentParams==0){
												// $current = ' class="current"';
												// $nameParams = $_TYPE_DEAL[0];
											// }
											// $transaction .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_TYPE_DEAL[0].'</a></li>';
										// }
										// else {
											// $current = '';
											// if($currentParams==$n){
												// $current = ' class="current"';
												// $nameParams = $_TYPE_DEAL[$n];
											// }
											// $transaction .= '<li'.$current.'><a data-name="transaction" data-explanation="" data-tags="'.clearValueText($_TYPE_DEAL[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_DEAL[$n].'</a></li>';
										// }
									// }
									// echo '<input type="hidden" name="transaction"'.$valueParams.'>';
									// echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									// echo '<div class="scrollbar-inner">';
									// echo '<ul>';
									// echo $transaction;
									// echo '</ul>';
									// echo '</div>';
									?>
								</div>
							</div>-->
							<div class="cell">
								<div style="margin-top:5px;margin-left:-10px;" class="check_block">
									<?
									$checked = '';
									$valueParams = '';
									$disabled = ' disabled="disabled"';
									if(isset($transaction_flat_sell_var)){
										if($transaction_flat_sell_var==1){
											$disabled = '';
											$checked = ' checked';
											$valueParams = ' value="'.$transaction_flat_sell_var.'"';
										}
									}
									?>
									<div class="checkbox_single<?=$checked?>">
										<input type="hidden"<?=$valueParams?> name="transaction"<?=$disabled?>>
										<div class="container">
											<span class="check"></span><span class="label">Прямая продажа</span>
										</div>
									</div>
								</div>
							</div>
							<div class="cell">
								<div style="margin-top:5px;margin-left:120px;" class="check_block">
									<?
									$checked = '';
									$valueParams = '';
									$disabled = ' disabled="disabled"';
									if(isset($balcony_flat_sell_var)){
										if($balcony_flat_sell_var==1){
											$disabled = '';
											$checked = ' checked';
											$valueParams = ' value="'.$balcony_flat_sell_var.'"';
										}
									}
									?>
									<div class="checkbox_single<?=$checked?>">
										<input type="hidden"<?=$valueParams?> name="balcony"<?=$disabled?>>
										<div class="container">
											<span class="check"></span><span class="label">Балкон</span>
										</div>
									</div>
									<?
									$checked = '';
									$valueParams = '';
									$disabled = ' disabled="disabled"';
									if(isset($lift_flat_sell_var)){
										if($lift_flat_sell_var==1){
											$disabled = '';
											$checked = ' checked';
											$valueParams = ' value="'.$lift_flat_sell_var.'"';
										}
									}
									?>
									<div class="checkbox_single<?=$checked?>">
										<input type="hidden"<?=$valueParams?> name="lift"<?=$disabled?>>
										<div class="container">
											<span class="check"></span><span class="label">Лифт</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					
					
					<!--<div class="right_block">
						
						<div class="checkbox_block">
							<?
							$valueParams = '';
							if($type_flat_sell_var && $furniture_flat_sell_var){
								$valueParams = ' value="'.$furniture_flat_sell_var.'"';
							}
							?>
							<input type="hidden" name="furniture"<?=$valueParams?>>
							<div class="select_checkbox">
								<ul>
									<?
									for($c=0; $c<count($_TYPE_FURNITURE); $c++){
										$checked = '';
										if(!$furniture_flat_sell_var){
											if($c==0){
												$checked = ' class="checked"';
											}
										}
										else {
											if($c==$furniture_flat_sell_var){
												$checked = ' class="checked"';
											}
										}
										echo '<li'.$checked.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_FURNITURE[$c].'</a></li>';
									}
									?>
								</ul>
							</div>
						</div>
						<div class="clear"></div>
						<div class="check_block">
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($tv_flat_sell_var)){
								if($tv_flat_sell_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$tv_flat_sell_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="tv"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Телевизор</span>
								</div>
							</div>
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($phone_flat_sell_var)){
								if($phone_flat_sell_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$phone_flat_sell_var.'"';
								}
							}
							?>
							<div class="checkbox_single first<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="phone"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Городской телефон</span>
								</div>
							</div>
						</div>
						<div class="check_block last">
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($fridge_flat_sell_var)){
								if($fridge_flat_sell_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$fridge_flat_sell_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="fridge"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Холодильник</span>
								</div>
							</div>
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($washer_flat_sell_var)){
								if($washer_flat_sell_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$washer_flat_sell_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="washer"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Стиральная машина</span>
								</div>
							</div>
						</div>
					</div>-->
				</div>
				<div class="row rent">
					<div class="left_block">
						<div class="metric">
							<div style="width:105px" class="label_select">Общая площадь</div>
							<div class="select_block price reg ch_price">
							<?
							$squareChangeFlatRent = '';
							if($arrayLinkSearch['squareFullMin'] && !$arrayLinkSearch['squareFullMax'] && $type_flat_rent_var){ //только метраж "от"
								$c1 = $arrayLinkSearch['squareFullMin'];
								$squareChangeFlatRent = '<li><a class="min" data-name_select="fullsquareflatrent" data-name="squareFullMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(!$arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_flat_rent_var){ //только метраж "до"
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeFlatRent = '<li><a class="max" data-name_select="fullsquareflatrent" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMax'].'" href="javascript:void(0)">общая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if($arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_flat_rent_var){ //промежуток метражей
								$c1 = $arrayLinkSearch['squareFullMin'];
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeFlatRent = '<li><a class="min max" data-name_select="fullsquareflatrent" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}

							$nameParams = 'От';
							$valueParams = '';
							$currentParams = '';
							if($type_flat_rent_var && $arrayLinkSearch['squareFullMin']){
								$nameParams = $arrayLinkSearch['squareFullMin'];
								$currentParams = $arrayLinkSearch['squareFullMin'];
								$valueParams = ' value="'.$arrayLinkSearch['squareFullMin'].'"';
							}
							?>
							<input type="hidden" name="squareFullMin"<?=$valueParams?>>
							<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareFullMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										if($min_full_square_flat_rent==0){
											$min_full_square_flat_rent = 1;
										}
										if($max_full_square_flat_rent==0){
											$max_full_square_flat_rent = 10;
										}
										for($c=$min_full_square_flat_rent; $c<=$max_full_square_flat_rent;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquareflatrent" data-explanation="общая" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_flat_rent){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_flat_rent){
												$c = $max_full_square_flat_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price reg ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && $arrayLinkSearch['squareFullMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareFullMax']);
									$currentParams = $arrayLinkSearch['squareFullMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMax'].'"';
								}
								?>
								<input type="hidden" name="squareFullMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareFullMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										if($min_full_square_flat_rent==0){
											$min_full_square_flat_rent = 1;
										}
										if($max_full_square_flat_rent==0){
											$max_full_square_flat_rent = 10;
										}
										for($c=$min_full_square_flat_rent; $c<=$max_full_square_flat_rent;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquareflatrent" data-explanation="общая" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_flat_rent){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_flat_rent){
												$c = $max_full_square_flat_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<!--<div class="metric">
							<div class="label_select">жилая</div>
							<div class="select_block count">
								<?
								$squareLiveChangeFlatRent = '';
								if($arrayLinkSearch['squareLiveMin'] && !$arrayLinkSearch['squareLiveMax'] && $type_flat_rent_var){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareLiveMax'];
									$squareLiveChangeFlatRent = '<li><a class="min" data-name_select="livesquareflatrent" data-name="squareLiveMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareLiveMin'].'" href="javascript:void(0)">жилая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareLiveMin'] && $arrayLinkSearch['squareLiveMax'] && $type_flat_rent_var){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareLiveMax'];
									$squareLiveChangeFlatRent = '<li><a class="max" data-name_select="livesquareflatrent" data-name="squareLiveMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareLiveMax'].'" href="javascript:void(0)">жилая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareLiveMin'] && $arrayLinkSearch['squareLiveMax'] && $type_flat_rent_var){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareLiveMin'];
									$c2 = $arrayLinkSearch['squareLiveMax'];
									$squareLiveChangeFlatRent = '<li><a class="min max" data-name_select="livesquareflatrent" data-name="squareLiveMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareLiveMin'].'" href="javascript:void(0)">жилая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && $arrayLinkSearch['squareLiveMin']){
									$nameParams = $arrayLinkSearch['squareLiveMin'];
									$currentParams = $arrayLinkSearch['squareLiveMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareLiveMin'].'"';
								}
								?>
								<input type="hidden" name="squareLiveMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										if($min_live_square_flat_rent==0){
											$min_live_square_flat_rent = 1;
										}
										if($max_live_square_flat_rent==0){
											$max_live_square_flat_rent = 10;
										}
										for($c=$min_live_square_flat_rent; $c<=$max_live_square_flat_rent;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="livesquareflatrent" data-explanation="жилая" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_live_square_flat_rent){
												break;
											}
											$c = $c+1;
											if($c>$max_live_square_flat_rent){
												$c = $max_live_square_flat_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && $arrayLinkSearch['squareLiveMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareLiveMax']);
									$currentParams = $arrayLinkSearch['squareLiveMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareLiveMax'].'"';
								}
								?>
								<input type="hidden" name="squareLiveMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										if($min_live_square_flat_rent==0){
											$min_live_square_flat_rent = 1;
										}
										if($max_live_square_flat_rent==0){
											$max_live_square_flat_rent = 10;
										}
										for($c=$min_live_square_flat_rent; $c<=$max_live_square_flat_rent;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="livesquareflatrent" data-explanation="жилая" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_live_square_flat_rent){
												break;
											}
											$c = $c+1;
											if($c>$max_live_square_flat_rent){
												$c = $max_live_square_flat_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>-->
						<div class="metric">
							<div style="width:105px" class="label_select">кухня</div>
							<div class="select_block price reg ch_price">
								<?
								$squareKitchenChangeFlatRent = '';
								if($arrayLinkSearch['squareKitchenMin'] && !$arrayLinkSearch['squareKitchenMax'] && $type_flat_rent_var){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeFlatRent = '<li><a class="min" data-name_select="kitchensquareflatrent" data-name="squareKitchenMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMin'].'" href="javascript:void(0)">кухня от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareKitchenMin'] && $arrayLinkSearch['squareKitchenMax'] && $type_flat_rent_var){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeFlatRent = '<li><a class="max" data-name_select="kitchensquareflatrent" data-name="squareKitchenMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMax'].'" href="javascript:void(0)">кухня до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareKitchenMin'] && $arrayLinkSearch['squareKitchenMax'] && $type_flat_rent_var){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareKitchenMin'];
									$c2 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeFlatRent = '<li><a class="min max" data-name_select="kitchensquareflatrent" data-name="squareKitchenMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMin'].'" href="javascript:void(0)">кухня от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && $arrayLinkSearch['squareKitchenMin']){
									$nameParams = $arrayLinkSearch['squareKitchenMin'];
									$currentParams = $arrayLinkSearch['squareKitchenMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareKitchenMin'].'"';
								}
								?>
								<input type="hidden" name="squareKitchenMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareKitchenMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										if($min_kitchen_square_flat_rent==0){
											$min_kitchen_square_flat_rent = 1;
										}
										if($max_kitchen_square_flat_rent==0){
											$max_kitchen_square_flat_rent = 10;
										}
										for($c=$min_kitchen_square_flat_rent; $c<=$max_kitchen_square_flat_rent;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="kitchensquareflatrent" data-explanation="кухня" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_kitchen_square_flat_rent){
												break;
											}
											$c = $c+1;
											if($c>$max_kitchen_square_flat_rent){
												$c = $max_kitchen_square_flat_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price reg ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && $arrayLinkSearch['squareKitchenMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareKitchenMax']);
									$currentParams = $arrayLinkSearch['squareKitchenMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareKitchenMax'].'"';
								}
								?>
								<input type="hidden" name="squareKitchenMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareKitchenMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										if($min_kitchen_square_flat_rent==0){
											$min_kitchen_square_flat_rent = 1;
										}
										if($max_kitchen_square_flat_rent==0){
											$max_kitchen_square_flat_rent = 10;
										}
										for($c=$min_kitchen_square_flat_rent; $c<=$max_kitchen_square_flat_rent;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="kitchensquareflatrent" data-explanation="кухня" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_kitchen_square_flat_rent){
												break;
											}
											$c = $c+1;
											if($c>$max_kitchen_square_flat_rent){
												$c = $max_kitchen_square_flat_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div style="width:105px" class="label_select">До метро</div>
							<div class="select_block count">
								<?
								$far_subwayChangeFlatRent = '';
								if($arrayLinkSearch['far_subway'] && $type_flat_rent_var){
									$c1 = $arrayLinkSearch['far_subway'];
									$name_value = $_FAR_SUBWAY[$c1];
									if($c1==1){
										$name_value = '< 500 м';
									}
									$far_subwayChangeFlatRent = '<li><a class="min" data-name_select="far_subway" data-name="far_subway" data-type="checkbox_block" data-id="'.$arrayLinkSearch['far_subway'].'" href="javascript:void(0)">Метро '.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && $arrayLinkSearch['far_subway']){
									$currentParams = (int)$arrayLinkSearch['far_subway'];
									$valueParams = ' value="'.$arrayLinkSearch['far_subway'].'"';
								}
								$far_subway = '';
								for($f=1; $f<=count($_FAR_SUBWAY); $f++){
									if($f==1){
										$current = '';
										if($currentParams==0){
											$current = ' class="current"';
											$nameParams = 'Не важно';
										}
										$far_subway .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
									}
									$current = '';
									if($currentParams==$f){
										$current = ' class="current"';
										$nameParams = $_FAR_SUBWAY[$f];
									}
									$name_value = $_FAR_SUBWAY[$f];
									if($f==1){
										$name_value = '< 500 м';
									}
									$far_subway .= '<li'.$current.'><a data-name="far_subway" data-explanation="Метро" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'">'.$_FAR_SUBWAY[$f].'</a></li>';
								}
								echo '<input type="hidden" name="far_subway"'.$valueParams.'>';
								echo '<div style="width:158px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $far_subway;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
						<div class="metric">
							<div style="width:105px" class="label_select">Дата публикации</div>
							<div class="select_block count">
								<?
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && $arrayLinkSearch['dates']){
									$currentParams = (int)$arrayLinkSearch['dates'];
									$valueParams = ' value="'.$arrayLinkSearch['dates'].'"';
								}
								$dates = '';
								$datesArray = array();
								for($n=0; $n<count($_DATE_POST); $n++){
									if($n==0){
										$current = '';
										if($currentParams=='' && !isset($arrayLinkSearch['dates'])){
											$current = ' class="current"';
											$nameParams = $_DATE_POST[0]['name'];
										}
										$dates .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_DATE_POST[0]['name'].'</a></li>';
									}
									else {
										$current = '';
										if($currentParams==$_DATE_POST[$n]['id']){
											$current = ' class="current"';
											$nameParams = $_DATE_POST[$n]['name'];
										}
										$dates .= '<li'.$current.'><a data-name="dates" data-explanation="" data-tags="'.$_DATE_POST[$n]['name'].'" href="javascript:void(0)" data-id="'.$_DATE_POST[$n]['id'].'">'.$_DATE_POST[$n]['name'].'</a></li>';
										$datesArray[$_DATE_POST[$n]['id']] = $_DATE_POST[$n]['name'];	
									}
								}
								$datesChangeFlatRent = '';
								if($arrayLinkSearch['dates'] && $type_flat_rent_var){
									$c1 = $arrayLinkSearch['dates'];
									// for(){
										
									// }
									$datesChangeFlatRent = '<li><a class="min" data-name_select="dates" data-name="dates" data-type="checkbox_block" data-id="'.$arrayLinkSearch['dates'].'" href="javascript:void(0)">'.$datesArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								echo '<input type="hidden" name="dates"'.$valueParams.'>';
								echo '<div style="width:158px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $dates;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
					</div>
					<div style="width:310px" class="center_block">
						<div class="floor">
							<div class="label_select">Этаж</div>
							<div class="select_block sec price ch_price">
								<?
								$floorChangeFlatRent = '';
								if($arrayLinkSearch['floorMin'] && !$arrayLinkSearch['floorMax'] && $type_flat_rent_var){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorMin'];
									$floorChangeFlatRent = '<li><a class="min" data-name_select="floorflatrent" data-name="floorMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_flat_rent_var){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeFlatRent = '<li><a class="max" data-name_select="floorflatrent" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMax'].'" href="javascript:void(0)">этаж по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_flat_rent_var){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorMin'];
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeFlatRent = '<li><a class="min max" data-name_select="floorflatrent" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && $arrayLinkSearch['floorMin']){
									$nameParams = $arrayLinkSearch['floorMin'];
									$currentParams = $arrayLinkSearch['floorMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMin'].'"';
								}
								?>
								<input type="hidden" name="floorMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="2" placeholder="От" type="text" value="<?=$arrayLinkSearch['floorMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										if($max_floors_flat_rent==0){
											$max_floors_flat_rent = 10;
										}
										for($f=1; $f<=$max_floors_flat_rent; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorflatrent" data-explanation="этаж" data-tags="с '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block sec price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && $arrayLinkSearch['floorMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorMax']);
									$currentParams = $arrayLinkSearch['floorMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMax'].'"';
								}
								?>
								<input type="hidden" name="floorMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="2" placeholder="До" type="text" value="<?=$arrayLinkSearch['floorMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_flat_rent; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorflatrent" data-explanation="этаж" data-tags="по '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<!--<div class="floor">
							<div class="label_select">в доме</div>
							<div class="select_block count">
								<?
								$floorsChangeFlatRent = '';
								if($arrayLinkSearch['floorsMin'] && !$arrayLinkSearch['floorsMax'] && $type_flat_rent_var){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorsMin'];
									$floorsChangeFlatRent = '<li><a class="min" data-name_select="floorscountflatrent" data-name="floorsMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">в доме от '.$c1.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $type_flat_rent_var){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorsMax'];
									$floorsChangeFlatRent = '<li><a class="max" data-name_select="floorscountflatrent" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMax'].'" href="javascript:void(0)">в доме по '.$c2.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $type_flat_rent_var){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorsMin'];
									$c2 = $arrayLinkSearch['floorsMax'];
									$floorsChangeFlatRent = '<li><a class="min max" data-name_select="floorscountflatrent" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">в доме от '.$c1.' по '.$c2.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && $arrayLinkSearch['floorsMin']){
									$nameParams = $arrayLinkSearch['floorsMin'];
									$currentParams = $arrayLinkSearch['floorsMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorsMin'].'"';
								}
								?>
								<input type="hidden" name="floorsMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$max_floors_flat_rent; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorscountflatrent" data-explanation="в доме" data-tags="от '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && $arrayLinkSearch['floorsMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorsMax']);
									$currentParams = $arrayLinkSearch['floorsMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorsMax'].'"';
								}
								?>
								<input type="hidden" name="floorsMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_flat_rent; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorscountflatrent" data-explanation="в доме" data-tags="до '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">этажей</div>
						</div>-->
						<div class="floor">
							<div style="width:50px;margin-left:0;" class="label_select">Санузел</div>
							<div class="select_block balcony">
								<?
								$wcChangeFlatRent = '';
								if(isset($arrayLinkSearch['wc']) && $type_flat_rent_var){
									$c1 = $arrayLinkSearch['wc'];
									if($c1==0){
										$wcChangeFlatRent = '<li><a class="min" data-name_select="wc" data-name="wc" data-type="checkbox_block" data-id="'.$arrayLinkSearch['wc'].'" href="javascript:void(0)">Нет санузла<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									else {
										$wcChangeFlatRent = '<li><a class="min" data-name_select="wc" data-name="wc" data-type="checkbox_block" data-id="'.$arrayLinkSearch['wc'].'" href="javascript:void(0)">'.$_TYPE_WC[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
								}
								
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_flat_rent_var && isset($arrayLinkSearch['wc'])){
									$currentParams = (int)$arrayLinkSearch['wc'];
									$valueParams = ' value="'.$arrayLinkSearch['wc'].'"';
								}
								$wc = '';
								for($n=0; $n<count($_TYPE_WC); $n++){
									if($n==0){
										$current = '';
										$current2 = '';
										// if($arrayLinkSearch['wc']==0 && isset($arrayLinkSearch['wc'])){
											// $current = ' class="current"';
											// $nameParams = 'Нет';
										// }
										if($currentParams=='' && !isset($arrayLinkSearch['wc'])){
											$current2 = ' class="current"';
											$nameParams = $_TYPE_WC[0];
										}
										$wc .= '<li'.$current2.'><a href="javascript:void(0)" data-id="">'.$_TYPE_WC[0].'</a></li>';
										// $wc .= '<li'.$current.'><a data-name="wc" data-explanation="" data-tags="Нет санузла" href="javascript:void(0)" data-id="0">Нет</a></li>';
									}
									else {
										$current = '';
										if($currentParams==$n){
											$current = ' class="current"';
											$nameParams = $_TYPE_WC[$n];
										}
										$wc .= '<li'.$current.'><a data-name="wc" data-explanation="" data-tags="'.clearValueText($_TYPE_WC[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_WC[$n].'</a></li>';
									}
								}
								echo '<input type="hidden" name="wc"'.$valueParams.'>';
								echo '<div style="width:159px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $wc;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
						<div class="floor">
							<div class="check_block">
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_first_flat_rent_var)){
									if($except_first_flat_rent_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_first_flat_rent_var.'"';
									}
								}
								?>
								<div class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_first"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме первого</span>
									</div>
								</div>
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_last_flat_rent_var)){
									if($except_last_flat_rent_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_last_flat_rent_var.'"';
									}
								}
								?>
								<div class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_last"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме последнего</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="line"></div>
					<div class="right_block">
						<div style="width:110px;
						margin-left: 0;
						float: left;
						margin-right: -28px;
						margin-top: 5px;
						text-align: right;" class="label_select">Мебель</div>
						<div class="checkbox_block">
							<?
							$valueParams = '';
							if($type_flat_rent_var && $furniture_flat_rent_var){
								$valueParams = ' value="'.$furniture_flat_rent_var.'"';
							}
							?>
							<input type="hidden" name="furniture"<?=$valueParams?>>
							<div class="select_checkbox">
								<ul>
									<?
									for($c=1; $c<count($_TYPE_FURNITURE); $c++){
										$checked = '';
										if($c==$furniture_flat_rent_var){
											$checked = ' class="checked"';
										}
										echo '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_FURNITURE[$c].'</a></li>';
										if($c==2){
											$checked = '';
											if(!$furniture_flat_rent_var){
												$checked = ' class="checked"';
											}
											echo '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="">'.$_TYPE_FURNITURE[0].'</a></li>';
										}
									}
									?>
								</ul>
							</div>
						</div>
						<div style="height:14px" class="clear"></div>
						<div style="width:110px;
						margin-left: 0;
						float: left;
						margin-right: -28px;
						margin-top: 5px;
						text-align: right;" class="label_select">Кухонная мебель</div>
						<div class="checkbox_block">
							<?
							$valueParams = '';
							if($type_flat_rent_var && $furniture_kitchen_flat_rent_var){
								$valueParams = ' value="'.$furniture_kitchen_flat_rent_var.'"';
							}
							?>
							<input type="hidden" name="furniture_kitchen"<?=$valueParams?>>
							<div class="select_checkbox">
								<ul>
									<?
									for($c=1; $c<count($_TYPE_KITCHEN_FURNITURE); $c++){
										$checked = '';
										if($c==$furniture_kitchen_flat_rent_var){
											$checked = ' class="checked"';
										}
										echo '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_KITCHEN_FURNITURE[$c].'</a></li>';
										if($c==2){
											$checked = '';
											if(!$furniture_kitchen_flat_rent_var){
												$checked = ' class="checked"';
											}
											echo '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="">'.$_TYPE_KITCHEN_FURNITURE[0].'</a></li>';
										}
									}
									?>
								</ul>
							</div>
						</div>
						<div class="clear"></div>
						<div class="check_block">
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($tv_flat_rent_var)){
								if($tv_flat_rent_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$tv_flat_rent_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="tv"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Телевизор</span>
								</div>
							</div>
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($washer_flat_rent_var)){
								if($washer_flat_rent_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$washer_flat_rent_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="washer"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Стиральная машина</span>
								</div>
							</div>
						</div>
					</div>
					<div class="last_right">
						<div class="check_block">
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($lift_flat_rent_var)){
								if($lift_flat_rent_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$lift_flat_rent_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="lift"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Лифт</span>
								</div>
							</div>
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($balcony_flat_rent_var)){
								if($balcony_flat_rent_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$balcony_flat_rent_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="balcony"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Балкон</span>
								</div>
							</div>
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($fridge_flat_rent_var)){
								if($fridge_flat_rent_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$fridge_flat_rent_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="fridge"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Холодильник</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div <?$estateValue!=3?print'style="display:none"':'style="display:block"'?> class="inner_block room">
				<div class="row rent">
					<div class="label_select" style="
						float: left;
						width: 105px;
						width: 110px;
						margin-right: -30px;
						margin-top: 5px;
						text-align: right;
					">Срок аренды</div>
					<div class="select_block type">
						<?
						$periodChangeRoomRent = '';
						if($arrayLinkSearch['period'] && $type_room_rent_var){
							$c1 = $arrayLinkSearch['period'];
							$periodChangeRoomRent = '<li><a class="min" data-name_select="period" data-name="period" data-type="checkbox_block" data-id="'.$arrayLinkSearch['period'].'" href="javascript:void(0)">'.$_TYPE_PERIOD_RENT[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
						}
						
						$nameParams = 'Не важно';
						$valueParams = '';
						$currentParams = '';
						if($type_room_rent_var && $arrayLinkSearch['period']){
							$currentParams = (int)$arrayLinkSearch['period'];
							$valueParams = ' value="'.$arrayLinkSearch['period'].'"';
						}
						$period = '';
						for($n=0; $n<count($_TYPE_PERIOD_RENT); $n++){
							if($n==0){
								$current = '';
								if($currentParams==0){
									$current = ' class="current"';
									$nameParams = $_TYPE_PERIOD_RENT[0];
								}
								$period .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_TYPE_PERIOD_RENT[0].'</a></li>';
							}
							else {
								$current = '';
								if($currentParams==$n){
									$current = ' class="current"';
									$nameParams = $_TYPE_PERIOD_RENT[$n];
								}
								$period .= '<li'.$current.'><a data-name="period" data-explanation="" data-tags="'.clearValueText($_TYPE_PERIOD_RENT[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_PERIOD_RENT[$n].'</a></li>';
							}
						}
						echo '<input type="hidden" name="period"'.$valueParams.'>';
						echo '<div class="choosed_block">'.$nameParams.'</div>';
						echo '<div class="scrollbar-inner">';
						echo '<ul>';
						echo $period;
						echo '</ul>';
						echo '</div>';
						?>
					</div>
					<div class="label_select" style="
						float: left;
						width: 82px;
						margin-right: -30px;
						margin-top: 5px;
						text-align: right;
					">Комиссия</div>
					<div class="select_block type">
						<?
						$prepaymentChangeRoomRent = '';
						if($arrayLinkSearch['prepayment'] && $type_room_rent_var){
							$c1 = $arrayLinkSearch['prepayment'];
							$prepaymentChangeRoomRent = '<li><a class="min" data-name_select="prepayment" data-name="prepayment" data-type="checkbox_block" data-id="'.$arrayLinkSearch['prepayment'].'" href="javascript:void(0)">'.$_TYPE_PREPAYMENT[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
						}
						
						$nameParams = 'Не важно';
						$valueParams = '';
						$currentParams = '';
						if($type_room_rent_var && $arrayLinkSearch['prepayment']){
							$currentParams = (int)$arrayLinkSearch['prepayment'];
							$valueParams = ' value="'.$arrayLinkSearch['prepayment'].'"';
						}
						$prepayment = '';
						for($n=0; $n<count($_TYPE_PREPAYMENT); $n++){
							if($n==0){
								$current = '';
								if($currentParams==0){
									$current = ' class="current"';
									$nameParams = $_TYPE_PREPAYMENT[0];
								}
								$prepayment .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_TYPE_PREPAYMENT[0].'</a></li>';
							}
							else {
								$current = '';
								if($currentParams==$n){
									$current = ' class="current"';
									$nameParams = $_TYPE_PREPAYMENT[$n];
								}
								$prepayment .= '<li'.$current.'><a data-name="prepayment" data-explanation="" data-tags="'.clearValueText($_TYPE_PREPAYMENT[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_PREPAYMENT[$n].'</a></li>';
							}
						}
						echo '<input type="hidden" name="prepayment"'.$valueParams.'>';
						echo '<div class="choosed_block">'.$nameParams.'</div>';
						echo '<div class="scrollbar-inner">';
						echo '<ul>';
						echo $prepayment;
						echo '</ul>';
						echo '</div>';
						?>
					</div>
					<?
					$checked = '';
					$valueParams = '';
					$disabled = ' disabled="disabled"';
					if(isset($deposit_room_rent_var)){
						if($deposit_room_rent_var==1){
							$disabled = '';
							$checked = ' checked';
							$valueParams = ' value="'.$deposit_room_rent_var.'"';
						}
					}
					?>
					<div style="float:left;margin:5px 0 0 40px" class="checkbox_single<?=$checked?>">
						<input type="hidden"<?=$valueParams?> name="deposit"<?=$disabled?>>
						<div class="container">
							<span class="check"></span><span class="label">Без залога</span>
						</div>
					</div>
					<div class="can_block">
						<?
						$checked = '';
						$valueParams = '';
						$disabled = ' disabled="disabled"';
						if(isset($animal_room_rent_var)){
							if($animal_room_rent_var==1){
								$disabled = '';
								$checked = ' checked';
								$valueParams = ' value="'.$animal_room_rent_var.'"';
							}
						}
						?>
						<div class="checkbox_single<?=$checked?>">
							<input type="hidden"<?=$valueParams?> name="animal"<?=$disabled?>>
							<div class="container">
								<span class="check"></span><span class="label">Можно с животным</span>
							</div>
						</div>
						<?
						$checked = '';
						$valueParams = '';
						$disabled = ' disabled="disabled"';
						if(isset($children_room_rent_var)){
							if($children_room_rent_var==1){
								$disabled = '';
								$checked = ' checked';
								$valueParams = ' value="'.$children_room_rent_var.'"';
							}
						}
						?>
						<div class="checkbox_single<?=$checked?>">
							<input type="hidden"<?=$valueParams?> name="children"<?=$disabled?>>
							<div class="container">
								<span class="check"></span><span class="label">Можно с детьми</span>
							</div>
						</div>
					</div>
				</div>
				<div class="separate"></div>
				<div class="row rent">
					<div class="left_block">
						<div class="metric">
							<div style="width:110px;" class="label_select">Площадь комнаты</div>
							<div class="select_block price reg ch_price">
								<?
								$name_type = 'кв.м';
								if($arrayLinkSearch['squareRoomMin'] && !$arrayLinkSearch['squareRoomMax'] && $type_room_rent_var){ //только площадь "от"
									$c1 = $arrayLinkSearch['squareRoomMin'];
									$squareFullChangeRoomRent = '<li><a class="min" data-name_select="squaresellroom" data-name="squareRoomMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareRoomMin'].'" href="javascript:void(0)">от '.$c1.' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareRoomMin'] && $arrayLinkSearch['squareRoomMax'] && $type_room_rent_var){ //только площадь "до"
									$c2 = $arrayLinkSearch['squareRoomMax'];
									$squareFullChangeRoomRent = '<li><a class="max" data-name_select="squaresellroom" data-name="squareRoomMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareRoomMax'].'" href="javascript:void(0)">до '.$c2.' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareRoomMin'] && $arrayLinkSearch['squareRoomMax'] && $type_room_rent_var){ //промежуток площадей
									$c1 = $arrayLinkSearch['squareRoomMin'];
									$c2 = $arrayLinkSearch['squareRoomMax'];
									$squareFullChangeRoomRent = '<li><a class="min max" data-name_select="squaresellroom" data-name="squareRoomMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareRoomMin'].'" href="javascript:void(0)">от '.$c1.' '.$name_type.'. до '.$c2.' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_room_rent_var && $arrayLinkSearch['squareRoomMin']){
									$nameParams = $arrayLinkSearch['squareRoomMin'];
									$currentParams = $arrayLinkSearch['squareRoomMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareRoomMin'].'"';
								}
								?>
								<input type="hidden" name="squareRoomMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareRoomMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										if($min_room_square_room_rent==0){
											$min_room_square_room_rent = 1;
										}
										if($max_room_square_room_rent==0){
											$max_room_square_room_rent = 10;
										}
										for($c=$min_room_square_room_rent; $c<=$max_room_square_room_rent;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="squaresellroom" data-tags="от '.$c.' '.$name_type.'" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_room_square_room_rent){
												break;
											}
											$c = $c+1;
											if($c>$max_room_square_room_rent){
												$c = $max_room_square_room_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">–</div>
							<div class="select_block price reg ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_room_rent_var && $arrayLinkSearch['squareRoomMax']){
									$nameParams = $arrayLinkSearch['squareRoomMax'];
									$currentParams = $arrayLinkSearch['squareRoomMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareRoomMax'].'"';
								}
								?>
								<input type="hidden" name="squareRoomMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareRoomMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										if($min_room_square_room_rent==0){
											$min_room_square_room_rent = 1;
										}
										if($max_room_square_room_rent==0){
											$max_room_square_room_rent = 10;
										}
										for($c=$min_room_square_room_rent; $c<=$max_room_square_room_rent;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="squaresellroom" data-tags="до '.$c.' '.$name_type.'" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_room_square_room_rent){
												break;
											}
											$c = $c+1;
											if($c>$max_room_square_room_rent){
												$c = $max_room_square_room_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div style="margin-top:2px" class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div style="width:110px" class="label_select">До метро</div>
							<div class="select_block count">
								<?
								$far_subwayChangeRoomRent = '';
								if($arrayLinkSearch['far_subway'] && $type_room_rent_var){
									$c1 = $arrayLinkSearch['far_subway'];
									$name_value = $_FAR_SUBWAY[$c1];
									if($c1==1){
										$name_value = '< 500 м';
									}
									$far_subwayChangeRoomRent = '<li><a class="min" data-name_select="far_subway" data-name="far_subway" data-type="checkbox_block" data-id="'.$arrayLinkSearch['far_subway'].'" href="javascript:void(0)">Метро '.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_room_rent_var && $arrayLinkSearch['far_subway']){
									$currentParams = (int)$arrayLinkSearch['far_subway'];
									$valueParams = ' value="'.$arrayLinkSearch['far_subway'].'"';
								}
								$far_subway = '';
								for($f=1; $f<=count($_FAR_SUBWAY); $f++){
									if($f==1){
										$current = '';
										if($currentParams==0){
											$current = ' class="current"';
											$nameParams = 'Не важно';
										}
										$far_subway .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
									}
									$current = '';
									if($currentParams==$f){
										$current = ' class="current"';
										$nameParams = $_FAR_SUBWAY[$f];
									}
									$name_value = $_FAR_SUBWAY[$f];
									if($f==1){
										$name_value = '< 500 м';
									}
									$far_subway .= '<li'.$current.'><a data-name="far_subway" data-explanation="Метро" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'">'.$_FAR_SUBWAY[$f].'</a></li>';
								}
								echo '<input type="hidden" name="far_subway"'.$valueParams.'>';
								echo '<div style="width:158px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $far_subway;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
						<div class="metric">
							<div class="label_select">Дата публикации</div>
							<div class="select_block count">
								<?
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_room_rent_var && $arrayLinkSearch['dates']){
									$currentParams = (int)$arrayLinkSearch['dates'];
									$valueParams = ' value="'.$arrayLinkSearch['dates'].'"';
								}
								$dates = '';
								$datesArray = array();
								for($n=0; $n<count($_DATE_POST); $n++){
									if($n==0){
										$current = '';
										if($currentParams=='' && !isset($arrayLinkSearch['dates'])){
											$current = ' class="current"';
											$nameParams = $_DATE_POST[0]['name'];
										}
										$dates .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_DATE_POST[0]['name'].'</a></li>';
									}
									else {
										$current = '';
										if($currentParams==$_DATE_POST[$n]['id']){
											$current = ' class="current"';
											$nameParams = $_DATE_POST[$n]['name'];
										}
										$dates .= '<li'.$current.'><a data-name="dates" data-explanation="" data-tags="'.$_DATE_POST[$n]['name'].'" href="javascript:void(0)" data-id="'.$_DATE_POST[$n]['id'].'">'.$_DATE_POST[$n]['name'].'</a></li>';
										$datesArray[$_DATE_POST[$n]['id']] = $_DATE_POST[$n]['name'];	
									}
								}
								$datesChangeRoomRent = '';
								if($arrayLinkSearch['dates'] && $type_room_rent_var){
									$c1 = $arrayLinkSearch['dates'];
									// for(){
										
									// }
									$datesChangeRoomRent = '<li><a class="min" data-name_select="dates" data-name="dates" data-type="checkbox_block" data-id="'.$arrayLinkSearch['dates'].'" href="javascript:void(0)">'.$datesArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								echo '<input type="hidden" name="dates"'.$valueParams.'>';
								echo '<div style="width:158px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $dates;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
					</div>
					<div class="center_block">
						<div class="floor">
							<div class="label_select">Этаж</div>
							<div class="select_block price sec ch_price">
								<?
								$floorChangeRoomRent = '';
								if($arrayLinkSearch['floorMin'] && !$arrayLinkSearch['floorMax'] && $type_room_rent_var){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorMin'];
									$floorChangeRoomRent = '<li><a class="min" data-name_select="floorssquareroomrent" data-name="floorMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_room_rent_var){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeRoomRent = '<li><a class="max" data-name_select="floorssquareroomrent" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMax'].'" href="javascript:void(0)">этаж по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_room_rent_var){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorMin'];
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeRoomRent = '<li><a class="min max" data-name_select="floorssquareroomrent" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_room_rent_var && $arrayLinkSearch['floorMin']){
									$nameParams = $arrayLinkSearch['floorMin'];
									$currentParams = $arrayLinkSearch['floorMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMin'].'"';
								}
								?>
								<input type="hidden" name="floorMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="2" placeholder="От" type="text" value="<?=$arrayLinkSearch['floorMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$max_floors_room_rent; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorssquareroomrent" data-explanation="этаж" data-tags="с '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price sec ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_room_rent_var && $arrayLinkSearch['floorMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorMax']);
									$currentParams = $arrayLinkSearch['floorMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMax'].'"';
								}
								?>
								<input type="hidden" name="floorMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="2" placeholder="До" type="text" value="<?=$arrayLinkSearch['floorMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_room_rent; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorssquareroomrent" data-explanation="этаж" data-tags="по '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						<div class="floor">
							<div style="width:50px;margin-left:0;" class="label_select">Санузел</div>
							<div class="select_block balcony multiple">
								<?
								$wcChangeRoomRent = '';
								$ex_array = explode(',',$arrayLinkSearch['wc']);
								if(isset($arrayLinkSearch['wc']) && $type_room_rent_var){
									$c1 = $arrayLinkSearch['wc'];
									// if($c1==0){
										// $wcChangeRoomRent = '<li><a class="min" data-name_select="wc" data-name="wc" data-type="checkbox_block" data-id="'.$arrayLinkSearch['wc'].'" href="javascript:void(0)">Нет санузла<span onclick="return delTags(this)" class="close"></span></a></li>';
									// }
									// else {
										// $wcChangeRoomRent = '<li><a class="min" data-name_select="wc" data-name="wc" data-type="checkbox_block" data-id="'.$arrayLinkSearch['wc'].'" href="javascript:void(0)">'.$_TYPE_WC[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									// }
									$ex_c1 = explode(',',$c1);
									for($e=0; $e<count($ex_c1); $e++){
										$wcChangeRoomRent .= '<li><a class="min" data-name_select="wc" data-name="wc" data-type="checkbox_block" data-id="'.$ex_c1[$e].'" data-multiple="true" href="javascript:void(0)">'.$_TYPE_WC[$ex_c1[$e]].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
								}
								
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_room_rent_var && isset($arrayLinkSearch['wc'])){
									$currentParams = (int)$arrayLinkSearch['wc'];
									$valueParams = ' value="'.$arrayLinkSearch['wc'].'"';
								}
								$wc = '';
								for($n=0; $n<count($_TYPE_WC); $n++){
									if($n==0){
										$current = '';
										$current2 = '';
										// if($arrayLinkSearch['wc']==0 && isset($arrayLinkSearch['wc'])){
											// $current = ' class="current"';
											// $nameParams = 'Нет';
										// }
										if($currentParams=='' && !isset($arrayLinkSearch['wc'])){
											$current2 = ' class="current"';
											$nameParams = $_TYPE_WC[0];
										}
										// $wc .= '<li'.$current2.'><a data-name="wc" data-explanation="" data-tags="Санузел не важен" href="javascript:void(0)" data-id="0"><span class="check"></span><span class="label">'.$_TYPE_WC[0].'</span></a></li>';
										// $wc .= '<li'.$current.'><a data-name="wc" data-explanation="" data-tags="Нет санузла" href="javascript:void(0)" data-id="0">Нет</a></li>';
									}
									else {
										$current = '';
										// if($currentParams==$n){
											// $current = ' class="current"';
											// $nameParams = $_TYPE_WC[$n];
										// }
										if(in_array($n,$ex_array)){
											$current = ' class="current"';
											$nameParams = $_TYPE_WC[$n];
										}
										$wc .= '<li'.$current.'><a data-name="wc" data-explanation="" data-tags="'.clearValueText($_TYPE_WC[$n]).'" href="javascript:void(0)" data-id="'.$n.'"><span class="check"></span><span class="label">'.$_TYPE_WC[$n].'</span></a></li>';
									}
								}
								if(count($ex_array)>1){
									$nameParams = count($ex_array).' параметра';
								}
								echo '<input type="hidden" name="wc"'.$valueParams.'>';
								echo '<div style="width:141px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $wc;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
						<div class="floor">
							<div class="check_block">
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_first_room_rent_var)){
									if($except_first_room_rent_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_first_room_rent_var.'"';
									}
								}
								?>
								<div class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_first"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме первого</span>
									</div>
								</div>
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_last_room_rent_var)){
									if($except_last_room_rent_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_last_room_rent_var.'"';
									}
								}
								?>
								<div class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_last"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме последнего</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="line"></div>
					<div class="right_block">
						<div style="width:auto;
						margin-left: 0;
						float: left;
						margin-right: -28px;
						margin-top: 5px;
						text-align: right;" class="label_select">Мебель</div>
						<div class="checkbox_block">
							<?
							$valueParams = '';
							if($type_room_rent_var && $furniture_room_rent_var){
								$valueParams = ' value="'.$furniture_room_rent_var.'"';
							}
							?>
							<input type="hidden" name="furniture"<?=$valueParams?>>
							<div class="select_checkbox">
								<ul>
									<?
									for($c=1; $c<count($_TYPE_FURNITURE); $c++){
										$checked = '';
										if($c==$furniture_room_rent_var){
											$checked = ' class="checked"';
										}
										echo '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_FURNITURE[$c].'</a></li>';
										if($c==2){
											$checked = '';
											if(!$furniture_room_rent_var){
												$checked = ' class="checked"';
											}
											echo '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="">'.$_TYPE_FURNITURE[0].'</a></li>';
										}
									}
									?>
								</ul>
							</div>
						</div>
						<div class="clear"></div>
						<div style="margin-top:21px" class="check_block">
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($lift_room_rent_var)){
								if($lift_room_rent_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$lift_room_rent_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="lift"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Лифт</span>
								</div>
							</div>
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($balcony_room_rent_var)){
								if($balcony_room_rent_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$balcony_room_rent_var.'"';
								}
							}
							?>
							<div style="margin-left:60px" class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="balcony"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Балкон</span>
								</div>
							</div>
						</div>
						<div style="margin-top:21px" class="check_block">
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($tv_room_rent_var)){
								if($tv_room_rent_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$tv_room_rent_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="tv"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Телевизор</span>
								</div>
							</div>
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($washer_room_rent_var)){
								if($washer_room_rent_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$washer_room_rent_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="washer"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Стиральная машина</span>
								</div>
							</div>
						</div>
					</div>
					<div style="margin-top:80px" class="last_right">
						<div class="check_block">
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($fridge_room_rent_var)){
								if($fridge_room_rent_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$fridge_room_rent_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="fridge"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Холодильник</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row sell">
					<div class="left_block">
						<div class="metric">
							<div style="width:110px;" class="label_select">Площадь комнаты</div>
							<div class="select_block price reg ch_price">
								<?
								$name_type = 'кв.м';
								if($arrayLinkSearch['squareRoomMin'] && !$arrayLinkSearch['squareRoomMax'] && $type_room_sell_var){ //только площадь "от"
									$c1 = $arrayLinkSearch['squareRoomMin'];
									$squareFullChangeRoomSell = '<li><a class="min" data-name_select="squaresellroom" data-name="squareRoomMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareRoomMin'].'" href="javascript:void(0)">от '.$c1.' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareRoomMin'] && $arrayLinkSearch['squareRoomMax'] && $type_room_sell_var){ //только площадь "до"
									$c2 = $arrayLinkSearch['squareRoomMax'];
									$squareFullChangeRoomSell = '<li><a class="max" data-name_select="squaresellroom" data-name="squareRoomMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareRoomMax'].'" href="javascript:void(0)">до '.$c2.' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareRoomMin'] && $arrayLinkSearch['squareRoomMax'] && $type_room_sell_var){ //промежуток площадей
									$c1 = $arrayLinkSearch['squareRoomMin'];
									$c2 = $arrayLinkSearch['squareRoomMax'];
									$squareFullChangeRoomSell = '<li><a class="min max" data-name_select="squaresellroom" data-name="squareRoomMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareRoomMin'].'" href="javascript:void(0)">от '.$c1.' '.$name_type.'. до '.$c2.' '.$name_type.'.<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_room_sell_var && $arrayLinkSearch['squareRoomMin']){
									$nameParams = $arrayLinkSearch['squareRoomMin'];
									$currentParams = $arrayLinkSearch['squareRoomMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareRoomMin'].'"';
								}
								?>
								<input type="hidden" name="squareRoomMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="10" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareRoomMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?									
										if($min_room_square_room_sell==0){
											$min_room_square_room_sell = 1;
										}
										if($max_room_square_room_sell==0){
											$max_room_square_room_sell = 10;
										}
										for($c=$min_room_square_room_sell; $c<=$max_room_square_room_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="squaresellroom" data-tags="от '.$c.' '.$name_type.'" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_room_square_room_sell){
												break;
											}
											$c = $c+1;
											if($c>$max_room_square_room_sell){
												$c = $max_room_square_room_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">–</div>
							<div class="select_block price reg ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_room_sell_var && $arrayLinkSearch['squareRoomMax']){
									$nameParams = $arrayLinkSearch['squareRoomMax'];
									$currentParams = $arrayLinkSearch['squareRoomMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareRoomMax'].'"';
								}
								?>
								<input type="hidden" name="squareRoomMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="10" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareRoomMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										if($min_room_square_room_sell==0){
											$min_room_square_room_sell = 1;
										}
										if($max_room_square_room_sell==0){
											$max_room_square_room_sell = 10;
										}
										for($c=$min_room_square_room_sell; $c<=$max_room_square_room_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="squaresellroom" data-tags="до '.$c.' '.$name_type.'" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_room_square_room_sell){
												break;
											}
											$c = $c+1;
											if($c>$max_room_square_room_sell){
												$c = $max_room_square_room_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div style="margin-top:2px" class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div style="width:110px" class="label_select">кухня</div>
							<div class="select_block price reg ch_price">
							
								<?
								$squareKitchenChangeRoomSell = '';
								if($arrayLinkSearch['squareKitchenMin'] && !$arrayLinkSearch['squareKitchenMax'] && $type_room_sell_var){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeRoomSell = '<li><a class="min" data-name_select="kitchensquareroomsell" data-name="squareKitchenMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMin'].'" href="javascript:void(0)">кухня от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareKitchenMin'] && $arrayLinkSearch['squareKitchenMax'] && $type_room_sell_var){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeRoomSell = '<li><a class="max" data-name_select="kitchensquareroomsell" data-name="squareKitchenMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMax'].'" href="javascript:void(0)">кухня до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareKitchenMin'] && $arrayLinkSearch['squareKitchenMax'] && $type_room_sell_var){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareKitchenMin'];
									$c2 = $arrayLinkSearch['squareKitchenMax'];
									$squareKitchenChangeRoomSell = '<li><a class="min max" data-name_select="kitchensquareroomsell" data-name="squareKitchenMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareKitchenMin'].'" href="javascript:void(0)">кухня от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_room_sell_var && $arrayLinkSearch['squareKitchenMin']){
									$nameParams = $arrayLinkSearch['squareKitchenMin'];
									$currentParams = $arrayLinkSearch['squareKitchenMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareKitchenMin'].'"';
								}
								?>
								<input type="hidden" name="squareKitchenMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="10" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareKitchenMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_kitchen_square_room_sell; $c<=$max_kitchen_square_room_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="kitchensquareroomsell" data-explanation="кухня" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price reg ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_room_sell_var && $arrayLinkSearch['squareKitchenMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareKitchenMax']);
									$currentParams = $arrayLinkSearch['squareKitchenMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareKitchenMax'].'"';
								}
								?>
								<input type="hidden" name="squareKitchenMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="10" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareKitchenMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_kitchen_square_room_sell; $c<=$max_kitchen_square_room_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="kitchensquareroomsell" data-explanation="кухня" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											$c = $c+1;
										}
										?>
									</ul>
								</div>
							</div>
							<div style="margin-top:2px" class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div class="label_select">Дата публикации</div>
							<div class="select_block count">
								<?
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_room_sell_var && $arrayLinkSearch['dates']){
									$currentParams = (int)$arrayLinkSearch['dates'];
									$valueParams = ' value="'.$arrayLinkSearch['dates'].'"';
								}
								$dates = '';
								$datesArray = array();
								for($n=0; $n<count($_DATE_POST); $n++){
									if($n==0){
										$current = '';
										if($currentParams=='' && !isset($arrayLinkSearch['dates'])){
											$current = ' class="current"';
											$nameParams = $_DATE_POST[0]['name'];
										}
										$dates .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_DATE_POST[0]['name'].'</a></li>';
									}
									else {
										$current = '';
										if($currentParams==$_DATE_POST[$n]['id']){
											$current = ' class="current"';
											$nameParams = $_DATE_POST[$n]['name'];
										}
										$dates .= '<li'.$current.'><a data-name="dates" data-explanation="" data-tags="'.$_DATE_POST[$n]['name'].'" href="javascript:void(0)" data-id="'.$_DATE_POST[$n]['id'].'">'.$_DATE_POST[$n]['name'].'</a></li>';
										$datesArray[$_DATE_POST[$n]['id']] = $_DATE_POST[$n]['name'];	
									}
								}
								$datesChangeRoomSell = '';
								if($arrayLinkSearch['dates'] && $type_room_sell_var){
									$c1 = $arrayLinkSearch['dates'];
									// for(){
										
									// }
									$datesChangeRoomSell = '<li><a class="min" data-name_select="dates" data-name="dates" data-type="checkbox_block" data-id="'.$arrayLinkSearch['dates'].'" href="javascript:void(0)">'.$datesArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								echo '<input type="hidden" name="dates"'.$valueParams.'>';
								echo '<div style="width:158px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $dates;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
					</div>
					<div style="width:320px" class="center_block">
						<!--<div class="floor">
							<div style="width:110px" class="label_select">Комнат в квартире</div>
							<div style="margin-left:0;" class="checkbox_block">
								<?
								// $ex_rooms_room_sell_var = array();
								// $valueInput = '';
								// $roomsFlatChangeRoomSell = '';
								// if(isset($rooms_flat_room_sell_var)){
									// $ex_rooms_room_sell_var = explode(',',$rooms_flat_room_sell_var);
									// $valueInput = ' value="'.$rooms_flat_room_sell_var.'"';
									// $id_room = 0;
									// $name_room = '';
									// if(in_array(2,$ex_rooms_room_sell_var)){ // 2-комнаты
										// $roomsFlatChangeRoomSell .= '<li><a data-name="rooms_flat" data-type="checkbox_select" data-id="2" href="javascript:void(0)">2-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
									// }
									// if(in_array(3,$ex_rooms_room_sell_var)){ // 3-комнаты
										// $roomsFlatChangeRoomSell .= '<li><a data-name="rooms_flat" data-type="checkbox_select" data-id="3" href="javascript:void(0)">3-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
									// }
									// if(in_array(4,$ex_rooms_room_sell_var)){ // 4+-комнат
										// $roomsFlatChangeRoomSell .= '<li><a data-name="rooms_flat" data-type="checkbox_select" data-id="4" href="javascript:void(0)">4+-комн.<span onclick="return delTags(this)" class="close"></span></a></li>';
									// }
								// }
								?>
								<input type="hidden" name="rooms_flat"<?=$valueInput?>>
								<div class="block_checkbox big">
									<ul>
										<li <?in_array(2,$ex_rooms_room_sell_var)?print'class="checked"':''?>><a data-tags="2-комн." href="javascript:void(0)" data-id="2">2</a></li>
										<li <?in_array(3,$ex_rooms_room_sell_var)?print'class="checked"':''?>><a data-tags="3-комн." href="javascript:void(0)" data-id="3">3</a></li>
										<li <?in_array(4,$ex_rooms_room_sell_var)?print'class="checked"':''?>><a data-tags="4+-комн." href="javascript:void(0)" data-id="4">4+</a></li>
									</ul>
								</div>
							</div>
						</div>-->
						<div class="floor">
							<div class="label_select">Этаж</div>
							<div class="select_block price reg ch_price">
								<?
								$floorChangeRoomSell = '';
								if($arrayLinkSearch['floorMin'] && !$arrayLinkSearch['floorMax'] && $type_room_sell_var){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorMin'];
									$floorChangeRoomSell = '<li><a class="min" data-name_select="floorsquareroomsell" data-name="floorMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_room_sell_var){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeRoomSell = '<li><a class="max" data-name_select="floorsquareroomsell" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMax'].'" href="javascript:void(0)">этаж по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_room_sell_var){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorMin'];
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeRoomSell = '<li><a class="min max" data-name_select="floorsquareroomsell" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_room_sell_var && $arrayLinkSearch['floorMin']){
									$nameParams = $arrayLinkSearch['floorMin'];
									$currentParams = $arrayLinkSearch['floorMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMin'].'"';
								}
								?>
								<input type="hidden" name="floorMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="10" placeholder="От" type="text" value="<?=$arrayLinkSearch['floorMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$max_floors_room_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquareroomsell" data-explanation="этаж" data-tags="с '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price reg ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_room_sell_var && $arrayLinkSearch['floorMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorMax']);
									$currentParams = $arrayLinkSearch['floorMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMax'].'"';
								}
								?>
								<input type="hidden" name="floorMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="10" placeholder="До" type="text" value="<?=$arrayLinkSearch['floorMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li class="current"><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_room_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquareroomsell" data-explanation="этаж" data-tags="по '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
						</div>					
						<!--<div class="floor">
							<div class="label_select">в доме</div>
							<div class="select_block count">
								<?
								// $floorsChangeRoomSell = '';
								// if($arrayLinkSearch['floorsMin'] && !$arrayLinkSearch['floorsMax'] && $type_room_sell_var){ //только этажи "с"
									// $c1 = $arrayLinkSearch['floorsMin'];
									// $floorsChangeRoomSell = '<li><a class="min" data-name_select="fullsquarefloors" data-name="floorsMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">в доме от '.$c1.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								// }
								// if(!$arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $type_room_sell_var){ //только этажи "по"
									// $c2 = $arrayLinkSearch['floorsMax'];
									// $floorsChangeRoomSell = '<li><a class="max" data-name_select="fullsquarefloors" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMax'].'" href="javascript:void(0)">в доме по '.$c2.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								// }
								// if($arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $type_room_sell_var){ //промежуток этажей
									// $c1 = $arrayLinkSearch['floorsMin'];
									// $c2 = $arrayLinkSearch['floorsMax'];
									// $floorsChangeRoomSell = '<li><a class="min max" data-name_select="fullsquarefloors" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">в доме от '.$c1.' по '.$c2.' этажей<span onclick="return delTags(this)" class="close"></span></a></li>';
								// }

								// $nameParams = 'От';
								// $valueParams = '';
								// $currentParams = '';
								// if($type_room_sell_var && $arrayLinkSearch['floorsMin']){
									// $nameParams = $arrayLinkSearch['floorsMin'];
									// $currentParams = $arrayLinkSearch['floorsMin'];
									// $valueParams = ' value="'.$arrayLinkSearch['floorsMin'].'"';
								// }
								?>
								<input type="hidden" name="floorsMin"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										// if($max_floors_room_sell==0){
											// $max_floors_room_sell = 10;
										// }
										// for($f=1; $f<=$max_floors_room_sell; $f++){
											// $current = "";
											// if($f==$currentParams){
												// $current = ' class="current"';
											// }
											// echo '<li'.$current.'><a data-name="fullsquarefloors" data-explanation="в доме" data-tags="от '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								// $nameParams = 'До';
								// $valueParams = '';
								// $currentParams = '';
								// if($type_room_sell_var && $arrayLinkSearch['floorsMax']){
									// $nameParams = str_replace(".", ",", $arrayLinkSearch['floorsMax']);
									// $currentParams = $arrayLinkSearch['floorsMax'];
									// $valueParams = ' value="'.$arrayLinkSearch['floorsMax'].'"';
								// }
								?>
								<input type="hidden" name="floorsMax"<?=$valueParams?>>
								<div class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										// if($max_floors_room_sell==0){
											// $max_floors_room_sell = 10;
										// }
										// for($f=1; $f<=$max_floors_room_sell; $f++){
											// $current = "";
											// if($f==$currentParams){
												// $current = ' class="current"';
											// }
											// echo '<li'.$current.'><a data-name="fullsquarefloors" data-explanation="в доме" data-tags="до '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>
							<div class="com">этажей</div>
						</div>-->
						
						<div class="floor">
							<div style="margin-left:53px" class="check_block">
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_first_room_sell_var)){
									if($except_first_room_sell_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_first_room_sell_var.'"';
									}
								}
								?>
								<div style="float:none" class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_first"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме первого</span>
									</div>
								</div>
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($except_last_room_sell_var)){
									if($except_last_room_sell_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$except_last_room_sell_var.'"';
									}
								}
								?>
								<div style="margin:23px 0 0 8px" class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="except_last"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">кроме последнего</span>
									</div>
								</div>
							</div>
						</div>
						<!--<div class="floor">
							<div class="check_block">
								<?
								// $checked = '';
								// $valueParams = '';
								// $disabled = ' disabled="disabled"';
								// if(isset($separate_room_sell_var)){
									// if($separate_room_sell_var==1){
										// $disabled = '';
										// $checked = ' checked';
										// $valueParams = ' value="'.$separate_room_sell_var.'"';
									// }
								// }
								?>
								<div style="margin-left:84px" class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="separate"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">Раздельный санузел</span>
									</div>
								</div>
							</div>
						</div>-->
					</div>
					<div class="line"></div>
					<div class="right_block">
						<div class="metric">
							<div class="cell">
								<div style="width:46px;margin-left:-6px;" class="label_select">Ремонт</div>
								<div class="select_block count multiple">
									<?
									$repairsChangeRoomSell = '';
									$ex_array = explode(',',$arrayLinkSearch['repairs']);
									if($arrayLinkSearch['repairs'] && $type_room_sell_var){
										$c1 = $arrayLinkSearch['repairs'];
										$ex_c1 = explode(',',$c1);
										for($e=0; $e<count($ex_c1); $e++){
											$repairsChangeRoomSell .= '<li><a class="min" data-name_select="repairs" data-name="repairs" data-type="checkbox_block" data-id="'.$ex_c1[$e].'" data-multiple="true" href="javascript:void(0)">'.$_REPAIRS[$ex_c1[$e]].'<span onclick="return delTags(this)" class="close"></span></a></li>';
										}
									}
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_room_sell_var && $arrayLinkSearch['repairs']){
										$currentParams = (int)$arrayLinkSearch['repairs'];
										$valueParams = ' value="'.$arrayLinkSearch['repairs'].'"';
									}
									$repairs = '';
									for($n=0; $n<count($_REPAIRS); $n++){
										if($n==0){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = $_REPAIRS[0];
											}
											// $repairs .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_REPAIRS[0].'</a></li>';
										}
										else {
											$current = '';
											// if($currentParams==$n){
												// $current = ' class="current"';
												// $nameParams = $_REPAIRS[$n];
											// }
											if(in_array($n,$ex_array)){
												$current = ' class="current"';
												$nameParams = $_TYPE_WC[$n];
											}
											$tooltip = '';
											if(!empty($_REPAIRS_DESCR[$n])){
												$tooltip = ' data-much="true" data-pos="left" data-hover="tooltip" data-title="'.$_REPAIRS_DESCR[$n].'"';
											}
											$repairs .= '<li'.$current.'><a'.$tooltip.' data-name="repairs" data-explanation="" data-tags="'.clearValueText($_REPAIRS[$n]).'" href="javascript:void(0)" data-id="'.$n.'"><span class="check"></span><span class="label">'.$_REPAIRS[$n].'</span></a></li>';
											
										}
									}
									if(count($ex_array)>1){
										$nameParams = count($ex_array).' параметра';
									}
									echo '<input type="hidden" name="repairs"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $repairs;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<div class="cell">
								<div style="width:60px;margin-left:0;" class="label_select">Санузел</div>
								<div class="select_block balcony multiple">
									<?
									$wcChangeRoomSell = '';
									$ex_array = explode(',',$arrayLinkSearch['wc']);
									if(isset($arrayLinkSearch['wc']) && $type_room_sell_var){
										$c1 = $arrayLinkSearch['wc'];
										// if($c1==0){
											// $wcChangeRoomSell = '<li><a class="min" data-name_select="wc" data-name="wc" data-type="checkbox_block" data-id="'.$arrayLinkSearch['wc'].'" href="javascript:void(0)">Нет санузла<span onclick="return delTags(this)" class="close"></span></a></li>';
										// }
										// else {
											// $wcChangeRoomSell = '<li><a class="min" data-name_select="wc" data-name="wc" data-type="checkbox_block" data-id="'.$arrayLinkSearch['wc'].'" href="javascript:void(0)">'.$_TYPE_WC[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
										// }
										$ex_c1 = explode(',',$c1);
										for($e=0; $e<count($ex_c1); $e++){
											$wcChangeRoomSell .= '<li><a class="min" data-name_select="wc" data-name="wc" data-type="checkbox_block" data-id="'.$ex_c1[$e].'" data-multiple="true" href="javascript:void(0)">'.$_TYPE_WC[$ex_c1[$e]].'<span onclick="return delTags(this)" class="close"></span></a></li>';
										}
									}
									
									$nameParams = 'Любой санузел';
									$valueParams = '';
									$currentParams = '';
									if($type_room_sell_var && isset($arrayLinkSearch['wc'])){
										$currentParams = (int)$arrayLinkSearch['wc'];
										$valueParams = ' value="'.$arrayLinkSearch['wc'].'"';
									}
									$wc = '';
									for($n=0; $n<count($_TYPE_WC); $n++){
										if($n==0){
											$current = '';
											$current2 = '';
											// if($arrayLinkSearch['wc']==0 && isset($arrayLinkSearch['wc'])){
												// $current = ' class="current"';
												// $nameParams = 'Нет';
											// }
											if($currentParams=='' && !isset($arrayLinkSearch['wc'])){
												$current2 = ' class="current"';
												$nameParams = $_TYPE_WC[0];
											}
											// $wc .= '<li'.$current2.'><a data-name="wc" data-explanation="" data-tags="Санузел не важен" href="javascript:void(0)" data-id="0"><span class="check"></span><span class="label">'.$_TYPE_WC[0].'</span></a></li>';
											// $wc .= '<li'.$current.'><a data-name="wc" data-explanation="" data-tags="Нет санузла" href="javascript:void(0)" data-id="0">Нет</a></li>';
										}
										else {
											$current = '';
											// if($currentParams==$n){
												// $current = ' class="current"';
												// $nameParams = $_TYPE_WC[$n];
											// }
											if(in_array($n,$ex_array)){
												$current = ' class="current"';
												$nameParams = $_TYPE_WC[$n];
											}
											$wc .= '<li'.$current.'><a data-name="wc" data-explanation="" data-tags="'.clearValueText($_TYPE_WC[$n]).'" href="javascript:void(0)" data-id="'.$n.'"><span class="check"></span><span class="label">'.$_TYPE_WC[$n].'</span></a></li>';
										}
									}
									if(count($ex_array)>1){
										$nameParams = count($ex_array).' параметра';
									}
									echo '<input type="hidden" name="wc"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $wc;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
						</div>
						<div class="metric">
							<div class="cell">
								<div style="width:55px;margin-left:-15px;" class="label_select">Тип дома</div>
								<div class="select_block count multiple">
									<?
									$ex_array = array(); 
									$type_houseChangeRoomSell = '';
									if($arrayLinkSearch['type_house'] && $type_room_sell_var){
										$ex_array = explode(',',$arrayLinkSearch['type_house']); 
										$c1 = $arrayLinkSearch['type_house'];
										$ex_c1 = explode(',',$c1);
										$arrTypesHouses = array();
										$res = mysql_query("SELECT * FROM ".$template."_type_house WHERE activation='1' ORDER BY num");
										if(mysql_num_rows($res)>0){
											while($row = mysql_fetch_assoc($res)){
												$arrTypesHouses[$row['id']]=$row['name'];
											}
										}
										for($e=0; $e<count($ex_c1); $e++){
											$type_houseChangeRoomSell .= '<li><a class="min" data-name_select="type_house" data-name="type_house" data-type="checkbox_block" data-id="'.$ex_c1[$e].'" data-multiple="true" href="javascript:void(0)">'.$arrTypesHouses[$ex_c1[$e]].'<span onclick="return delTags(this)" class="close"></span></a></li>';
										}
									}
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_room_sell_var && $arrayLinkSearch['type_house']){
										$currentParams = (int)$arrayLinkSearch['type_house'];
										$valueParams = ' value="'.$arrayLinkSearch['type_house'].'"';
									}
									$type_house = '';
									$type_houseArray = array();
									$devs = mysql_query("
										SELECT *, (SELECT name FROM ".$template."_type_house WHERE activation='1' && id='".$currentParams."') AS choose_value
										FROM ".$template."_type_house
										WHERE activation='1'
										ORDER BY name
									");
									if(mysql_num_rows($devs)>0){
										$n = 0;
										while($dev = mysql_fetch_assoc($devs)){
											if($n==0){
												$current = '';
												if($currentParams==0){
													$current = ' class="current"';
													$nameParams = 'Не важно';
												}
												// $type_house .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
											}
											else {
												$current = '';
												if(in_array($dev['id'],$ex_array) && $type_room_sell_var){
													$current = ' class="current"';
													$nameParams = $dev['name'];
												}
												$type_house .= '<li'.$current.'><a data-name="type_house" data-explanation="" data-tags="'.clearValueText($dev['name']).'" href="javascript:void(0)" data-id="'.$dev['id'].'"><span class="check"></span><span class="label">'.$dev['name'].'</a></li>';
												$type_houseArray[$dev['id']] = clearValueText($dev['name']);
											}
											$n++;
										}
									}
									// $type_houseChangeRoomSell = '';
									// if($arrayLinkSearch['type_house'] && $type_room_sell_var){
										// $c1 = $arrayLinkSearch['type_house'];
										// $type_houseChangeRoomSell = '<li><a class="min" data-name_select="type_house" data-name="type_house" data-type="checkbox_block" data-id="'.$arrayLinkSearch['type_house'].'" href="javascript:void(0)">'.$type_houseArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									// }
									if(count($ex_array)>1){
										$nameParams = count($ex_array).' параметра';
									}
									echo '<input type="hidden" name="type_house"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $type_house;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<div class="cell">
								<div style="width:60px" class="label_select">До метро</div>
								<div class="select_block count">
									<?
									$far_subwayChangeRoomSell = '';
									if($arrayLinkSearch['far_subway'] && $type_room_sell_var){
										$c1 = $arrayLinkSearch['far_subway'];
										$name_value = $_FAR_SUBWAY[$c1];
										if($c1==1){
											$name_value = '< 500 м';
										}
										$far_subwayChangeRoomSell = '<li><a class="min" data-name_select="far_subway" data-name="far_subway" data-type="checkbox_block" data-id="'.$arrayLinkSearch['far_subway'].'" href="javascript:void(0)">Метро '.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_room_sell_var && $arrayLinkSearch['far_subway']){
										$currentParams = (int)$arrayLinkSearch['far_subway'];
										$valueParams = ' value="'.$arrayLinkSearch['far_subway'].'"';
									}
									$far_subway = '';
									for($f=1; $f<=count($_FAR_SUBWAY); $f++){
										if($f==1){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = 'Не важно';
											}
											$far_subway .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
										}
										$current = '';
										if($currentParams==$f){
											$current = ' class="current"';
											$nameParams = $_FAR_SUBWAY[$f];
										}
										$name_value = $_FAR_SUBWAY[$f];
										if($f==1){
											$name_value = '< 500 м';
										}
										$far_subway .= '<li'.$current.'><a data-name="far_subway" data-explanation="Метро" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'">'.$_FAR_SUBWAY[$f].'</a></li>';
									}
									echo '<input type="hidden" name="far_subway"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $far_subway;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
							<!--<div class="select_block balcony">
								<input type="hidden" name="balcony" value="0">
								<div class="choosed_block">Балкон не важен</div>
								<div class="scrollbar-inner">
									<ul>
										<?
										// for($c=0; $c<count($_TYPE_BALCONY); $c++){
											// $current = '';
											// if(!$balcony_new_sell_var){
												// if($c==0){
													// $current = ' class="current"';
												// }
											// }
											// else {
												// if($c==$balcony_new_sell_var){
													// $current = ' class="current"';
												// }
											// }
											// echo '<li'.$current.'><a href="javascript:void(0)" data-id="'.$c.'">'.$_TYPE_BALCONY[$c].'</a></li>';
										// }
										?>
									</ul>
								</div>
							</div>-->
						</div>						
						<div class="metric">
							<!--<div class="cell">
								<div style="width:70px;margin-left:-30px;" class="label_select">Тип сделки</div>
								<div class="select_block count">
									<?
									$transactionChangeRoomSell = '';
									if($arrayLinkSearch['transaction'] && $type_room_sell_var){
										$c1 = $arrayLinkSearch['transaction'];
										$transactionChangeRoomSell = '<li><a class="min" data-name_select="transaction" data-name="transaction" data-type="checkbox_block" data-id="'.$arrayLinkSearch['transaction'].'" href="javascript:void(0)">'.$_TYPE_DEAL[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_room_sell_var && $arrayLinkSearch['transaction']){
										$currentParams = (int)$arrayLinkSearch['transaction'];
										$valueParams = ' value="'.$arrayLinkSearch['transaction'].'"';
									}
									$transaction = '';
									for($n=0; $n<count($_TYPE_DEAL); $n++){
										if($n==0){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = $_TYPE_DEAL[0];
											}
											$transaction .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_TYPE_DEAL[0].'</a></li>';
										}
										else {
											$current = '';
											if($currentParams==$n){
												$current = ' class="current"';
												$nameParams = $_TYPE_DEAL[$n];
											}
											$transaction .= '<li'.$current.'><a data-name="transaction" data-explanation="" data-tags="'.clearValueText($_TYPE_DEAL[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_DEAL[$n].'</a></li>';
										}
									}
									echo '<input type="hidden" name="transaction"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $transaction;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>-->
							<div class="cell">
								<div style="margin-top:5px;margin-left:-10px;" class="check_block">
									<?
									$checked = '';
									$valueParams = '';
									$disabled = ' disabled="disabled"';
									if(isset($transaction_room_sell_var) && isset($type_room_sell_var)){
										if($transaction_room_sell_var==1){
											$disabled = '';
											$checked = ' checked';
											$valueParams = ' value="'.$transaction_room_sell_var.'"';
										}
									}
									?>
									<div class="checkbox_single<?=$checked?>">
										<input type="hidden"<?=$valueParams?> name="transaction"<?=$disabled?>>
										<div class="container">
											<span class="check"></span><span class="label">Прямая продажа</span>
										</div>
									</div>
								</div>
							</div>
							<div class="cell">
								<div style="margin-top:5px;margin-left:120px;" class="check_block">
									<?
									$checked = '';
									$valueParams = '';
									$disabled = ' disabled="disabled"';
									if(isset($balcony_room_sell_var)){
										if($balcony_room_sell_var==1){
											$disabled = '';
											$checked = ' checked';
											$valueParams = ' value="'.$balcony_room_sell_var.'"';
										}
									}
									?>
									<div class="checkbox_single<?=$checked?>">
										<input type="hidden"<?=$valueParams?> name="balcony"<?=$disabled?>>
										<div class="container">
											<span class="check"></span><span class="label">Балкон</span>
										</div>
									</div>
									<?
									$checked = '';
									$valueParams = '';
									$disabled = ' disabled="disabled"';
									if(isset($lift_room_sell_var)){
										if($lift_room_sell_var==1){
											$disabled = '';
											$checked = ' checked';
											$valueParams = ' value="'.$lift_room_sell_var.'"';
										}
									}
									?>
									<div class="checkbox_single<?=$checked?>">
										<input type="hidden"<?=$valueParams?> name="lift"<?=$disabled?>>
										<div class="container">
											<span class="check"></span><span class="label">Лифт</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div <?$estateValue!=4?print'style="display:none"':'style="display:block"'?> class="inner_block country">
				<div class="row sell">
					<div class="country_left_block">
						<div class="left_block">
							<div class="metric">
								<div style="width:100px" class="label_select">Площадь дома</div>
								<div class="select_block price reg ch_price">
								<?
								$squareChangeCountrySell = '';
								if($arrayLinkSearch['squareFullMin'] && !$arrayLinkSearch['squareFullMax'] && $type_country_sell_var){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareFullMin'];
									$squareChangeCountrySell = '<li><a class="min" data-name_select="fullsquarecountrysell" data-name="squareFullMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_country_sell_var){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareFullMax'];
									$squareChangeCountrySell = '<li><a class="max" data-name_select="fullsquarecountrysell" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMax'].'" href="javascript:void(0)">общая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_country_sell_var){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareFullMin'];
									$c2 = $arrayLinkSearch['squareFullMax'];
									$squareChangeCountrySell = '<li><a class="min max" data-name_select="fullsquarecountrysell" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_country_sell_var && $arrayLinkSearch['squareFullMin']){
									$nameParams = $arrayLinkSearch['squareFullMin'];
									$currentParams = $arrayLinkSearch['squareFullMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMin'].'"';
								}
								?>
								<input type="hidden" name="squareFullMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="10" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareFullMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
									<div class="scrollbar-inner">
										<ul>
											<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
											<?
											for($c=$min_full_square_country_sell; $c<=$max_full_square_country_sell;){
												$current = "";
												if($c==$currentParams){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a data-name="fullsquarecountrysell" data-explanation="общая" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
												if($c==$max_full_square_country_sell){
													break;
												}
												if($c<=40){
													$c = $c+1;
												}
												if($c>40 && $c<=100){
													$c = $c+2;
												}
												if($c>100){
													$c = $c+5;
												}
												if($c>$max_full_square_country_sell){
													$c = $max_full_square_country_sell;
												}
											}
											?>
										</ul>
									</div>
								</div>
								<div class="mdash">&ndash;</div>
								<div class="select_block price reg ch_price">
									<?
									$nameParams = 'До';
									$valueParams = '';
									$currentParams = '';
									if($type_country_sell_var && $arrayLinkSearch['squareFullMax']){
										$nameParams = str_replace(".", ",", $arrayLinkSearch['squareFullMax']);
										$currentParams = $arrayLinkSearch['squareFullMax'];
										$valueParams = ' value="'.$arrayLinkSearch['squareFullMax'].'"';
									}
									?>
									<input type="hidden" name="squareFullMax"<?=$valueParams?>>
									<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="10" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareFullMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
									<div class="scrollbar-inner">
										<ul>
											<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
											<?
											for($c=$min_full_square_country_sell; $c<=$max_full_square_country_sell;){
												$current = "";
												if($c==$currentParams){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a data-name="fullsquarecountrysell" data-explanation="общая" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
												if($c==$max_full_square_country_sell){
													break;
												}
												if($c<=40){
													$c = $c+1;
												}
												if($c>40 && $c<=100){
													$c = $c+2;
												}
												if($c>100){
													$c = $c+5;
												}
												if($c>$max_full_square_country_sell){
													$c = $max_full_square_country_sell;
												}
											}
											?>
										</ul>
									</div>
								</div>
								<div class="com">м<sup>2</sup></div>
							</div>
							<div class="metric">
								<div style="width:100px" class="label_select">Площадь участка</div>
								<div class="select_block price reg ch_price">
								<?
								$squareLandChangeCountrySell = '';
								if($arrayLinkSearch['squareLandMin'] && !$arrayLinkSearch['squareLandMax'] && $type_country_sell_var){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareLandMin'];
									$squareLandChangeCountrySell = '<li><a class="min" data-name_select="fulllandsquarecountrysell" data-name="squareLandMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareLandMin'].'" href="javascript:void(0)">общая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareLandMin'] && $arrayLinkSearch['squareLandMax'] && $type_country_sell_var){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareLandMax'];
									$squareLandChangeCountrySell = '<li><a class="max" data-name_select="fulllandsquarecountrysell" data-name="squareLandMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareLandMax'].'" href="javascript:void(0)">общая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareLandMin'] && $arrayLinkSearch['squareLandMax'] && $type_country_sell_var){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareLandMin'];
									$c2 = $arrayLinkSearch['squareLandMax'];
									$squareLandChangeCountrySell = '<li><a class="min max" data-name_select="fulllandsquarecountrysell" data-name="squareLandMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareLandMin'].'" href="javascript:void(0)">общая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_country_sell_var && $arrayLinkSearch['squareLandMin']){
									$nameParams = $arrayLinkSearch['squareLandMin'];
									$currentParams = $arrayLinkSearch['squareLandMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareLandMin'].'"';
								}
								?>
								<input type="hidden" name="squareLandMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="10" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareLandMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
									<div class="scrollbar-inner">
										<ul>
											<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
											<?
											for($c=$min_area_square_country_sell; $c<=$max_area_square_country_sell;){
												$current = "";
												if($c==$currentParams){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a data-name="fulllandsquarecountrysell" data-explanation="Площадь участка" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
												if($c==$max_area_square_country_sell){
													break;
												}
												if($c<=40){
													$c = $c+1;
												}
												if($c>40 && $c<=100){
													$c = $c+2;
												}
												if($c>100){
													$c = $c+5;
												}
												if($c>$max_area_square_country_sell){
													$c = $max_area_square_country_sell;
												}
											}
											?>
										</ul>
									</div>
								</div>
								<div class="mdash">&ndash;</div>
								<div class="select_block price reg ch_price">
									<?
									$nameParams = 'До';
									$valueParams = '';
									$currentParams = '';
									if($type_country_sell_var && $arrayLinkSearch['squareLandMax']){
										$nameParams = str_replace(".", ",", $arrayLinkSearch['squareLandMax']);
										$currentParams = $arrayLinkSearch['squareLandMax'];
										$valueParams = ' value="'.$arrayLinkSearch['squareLandMax'].'"';
									}
									?>
									<input type="hidden" name="squareLandMax"<?=$valueParams?>>
									<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="10" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareLandMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
									<div class="scrollbar-inner">
										<ul>
											<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
											<?
											for($c=$min_area_square_country_sell; $c<=$max_area_square_country_sell;){
												$current = "";
												if($c==$currentParams){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a data-name="fulllandsquarecountrysell" data-explanation="Площадь участка" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
												if($c==$max_area_square_country_sell){
													break;
												}
												if($c<=40){
													$c = $c+1;
												}
												if($c>40 && $c<=100){
													$c = $c+2;
												}
												if($c>100){
													$c = $c+5;
												}
												if($c>$max_area_square_country_sell){
													$c = $max_area_square_country_sell;
												}
											}
											?>
										</ul>
									</div>
								</div>
								<div class="com">м<sup>2</sup></div>
							</div>
							<div class="metric">
								<!--<div style="width:80px;margin-left:-30px;margin-top:-2px;" class="label_select">Дата публикации</div>-->
								<div style="margin-left:-10px" class="label_select">Дата публикации</div>
								<div class="select_block count">
									<?
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_country_sell_var && $arrayLinkSearch['dates']){
										$currentParams = (int)$arrayLinkSearch['dates'];
										$valueParams = ' value="'.$arrayLinkSearch['dates'].'"';
									}
									$dates = '';
									$datesArray = array();
									for($n=0; $n<count($_DATE_POST); $n++){
										if($n==0){
											$current = '';
											if($currentParams=='' && !isset($arrayLinkSearch['dates'])){
												$current = ' class="current"';
												$nameParams = $_DATE_POST[0]['name'];
											}
											$dates .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_DATE_POST[0]['name'].'</a></li>';
										}
										else {
											$current = '';
											if($currentParams==$_DATE_POST[$n]['id']){
												$current = ' class="current"';
												$nameParams = $_DATE_POST[$n]['name'];
											}
											$dates .= '<li'.$current.'><a data-name="dates" data-explanation="" data-tags="'.$_DATE_POST[$n]['name'].'" href="javascript:void(0)" data-id="'.$_DATE_POST[$n]['id'].'">'.$_DATE_POST[$n]['name'].'</a></li>';
											$datesArray[$_DATE_POST[$n]['id']] = $_DATE_POST[$n]['name'];	
										}
									}
									$datesChangeCountrySell = '';
									if($arrayLinkSearch['dates'] && $type_country_sell_var){
										$c1 = $arrayLinkSearch['dates'];
										// for(){
											
										// }
										$datesChangeCountrySell = '<li><a class="min" data-name_select="dates" data-name="dates" data-type="checkbox_block" data-id="'.$arrayLinkSearch['dates'].'" href="javascript:void(0)">'.$datesArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									echo '<input type="hidden" name="dates"'.$valueParams.'>';
									echo '<div style="width:158px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $dates;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
						</div>
						<!--<div style="margin-left:10px" class="center_block"></div>-->
						<div style="margin-left:0" class="right_block">
							<!--<div class="metric">
								<div style="width:130px;margin-left:-30px;margin-top:5px;" class="label_select">Юридический статус</div>
								<div class="select_block count">
									<?
									// $nameParams = 'Не важно';
									// $valueParams = '';
									// $currentParams = '';
									// if($type_country_sell_var && $arrayLinkSearch['legal_status']){
										// $currentParams = (int)$arrayLinkSearch['legal_status'];
										// $valueParams = ' value="'.$arrayLinkSearch['legal_status'].'"';
									// }
									// $legal_status = '';
									// $legal_statusArray = array();
									// for($n=0; $n<count($_LEGAL_STATUS); $n++){
										// if($n==0){
											// $current = '';
											// if($currentParams=='' && !isset($arrayLinkSearch['legal_status'])){
												// $current = ' class="current"';
												// $nameParams = $_LEGAL_STATUS[$n];
											// }
											// $legal_status .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_LEGAL_STATUS[$n].'</a></li>';
										// }
										// else {
											// $current = '';
											// if($currentParams==$n){
												// $current = ' class="current"';
												// $nameParams = $_LEGAL_STATUS[$n];
											// }
											// $legal_status .= '<li'.$current.'><a data-name="legal_status" data-explanation="Юридический статус" data-tags="'.$_LEGAL_STATUS[$n].'" href="javascript:void(0)" data-id="'.$n.'">'.$_LEGAL_STATUS[$n].'</a></li>';
											// $legal_statusArray[$n] = $_LEGAL_STATUS[$n];	
										// }
									// }
									// $legal_statusChangeCountrySell = '';
									// if($arrayLinkSearch['legal_status'] && $type_country_sell_var){
										// $c1 = $arrayLinkSearch['legal_status'];
										// $legal_statusChangeCountrySell = '<li><a class="min" data-name_select="legal_status" data-name="legal_status" data-type="checkbox_block" data-id="'.$arrayLinkSearch['legal_status'].'" href="javascript:void(0)">Юридический статус '.$legal_statusArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									// }
									// echo '<input type="hidden" name="legal_status"'.$valueParams.'>';
									// echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									// echo '<div class="scrollbar-inner">';
									// echo '<ul>';
									// echo $legal_status;
									// echo '</ul>';
									// echo '</div>';
									?>
								</div>
							</div>-->
						</div>
						<!--<div class="type_country">
							<div style="width:83px;text-align:left;margin-left:8px;margin-top:19px" class="label_select">Тип строения</div>
							<div class="check_block">
								<div class="checkbox_block">
									<?
									$ex_type_house_country_sell_var = array();
									$valueInput = '';
									$typeHouseChangeCountrySell = '';
									if($type_house_country_sell_var){
										$ex_type_house_country_sell_var = explode(',',$type_house_country_sell_var);
										$valueInput = ' value="'.$type_house_country_sell_var.'"';
										$id_room = 0;
										$name_room = '';
										for($com=1; $com<=count($_TYPE_HOUSE_COUNTRY); $com++){
											if(in_array($com,$ex_type_house_country_sell_var)){
												$typeHouseChangeCountrySell .= '<li><a data-name="type_house" data-type="checkbox_select" data-id="'.$com.'" href="javascript:void(0)">'.$_TYPE_HOUSE_COUNTRY[$com].'<span onclick="return delTags(this)" class="close"></span></a></li>';
											}
										}
									}
									?>
									<input type="hidden" name="type_house"<?=$valueInput?>>
									<div class="block_checkbox big comm">
										<ul>
											<?
											for($com=1; $com<=count($_TYPE_HOUSE_COUNTRY)-1; $com++){	
												echo '<div data-id="'.$com.'" class="checkbox_single '.(in_array($com,$ex_type_house_country_sell_var)?'checked':"").'">
													<div class="container">
														<span class="check"></span><span class="label">'.$_TYPE_HOUSE_COUNTRY[$com].'</span>
													</div>
												</div>';
											}
											?>
										</ul>
									</div>
								</div>
							</div>
						</div>-->
						
						
						
						<!--<div class="separated" style="display:block!important;"></div>
						<div class="country_params">
							<div class="item">
								<div class="metric">
									<div style="width:129px;margin-left:-30px;margin-top:5px;" class="label_select">Водопровод</div>
									<div class="select_block count">
										<?
										$nameParams = 'Не важно';
										$valueParams = '';
										$currentParams = '';
										if($type_country_sell_var && $arrayLinkSearch['plumbing']){
											$currentParams = (int)$arrayLinkSearch['plumbing'];
											$valueParams = ' value="'.$arrayLinkSearch['plumbing'].'"';
										}
										$plumbing = '';
										$plumbingArray = array();
										for($n=0; $n<count($_PLUMBING); $n++){
											if($n==0){
												$current = '';
												if($currentParams=='' && !isset($arrayLinkSearch['plumbing'])){
													$current = ' class="current"';
													$nameParams = $_PLUMBING[$n];
												}
												$plumbing .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_PLUMBING[$n].'</a></li>';
											}
											else {
												$current = '';
												if($currentParams==$n){
													$current = ' class="current"';
													$nameParams = $_PLUMBING[$n];
												}
												$plumbing .= '<li'.$current.'><a data-name="plumbing" data-explanation="Водопровод" data-tags="'.$_PLUMBING[$n].'" href="javascript:void(0)" data-id="'.$n.'">'.$_PLUMBING[$n].'</a></li>';
												$plumbingArray[$n] = $_PLUMBING[$n];
											}
										}
										$plumbingChangeCountrySell = '';
										if($arrayLinkSearch['plumbing'] && $type_country_sell_var){
											$c1 = $arrayLinkSearch['plumbing'];
											$plumbingChangeCountrySell = '<li><a class="min" data-name_select="plumbing" data-name="plumbing" data-type="checkbox_block" data-id="'.$arrayLinkSearch['plumbing'].'" href="javascript:void(0)">Водопровод '.$plumbingArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
										}
										echo '<input type="hidden" name="plumbing"'.$valueParams.'>';
										echo '<div style="width:148px" class="choosed_block">'.$nameParams.'</div>';
										echo '<div class="scrollbar-inner">';
										echo '<ul>';
										echo $plumbing;
										echo '</ul>';
										echo '</div>';
										?>
									</div>
								</div>
								<div class="metric">
									<div style="width:129px;margin-left:-30px;margin-top:5px;" class="label_select">Канализация</div>
									<div class="select_block count">
										<?
										$nameParams = 'Не важно';
										$valueParams = '';
										$currentParams = '';
										if($type_country_sell_var && $arrayLinkSearch['sewerage']){
											$currentParams = (int)$arrayLinkSearch['sewerage'];
											$valueParams = ' value="'.$arrayLinkSearch['sewerage'].'"';
										}
										$sewerage = '';
										$sewerageArray = array();
										for($n=0; $n<count($_SEWERAGE); $n++){
											if($n==0){
												$current = '';
												if($currentParams=='' && !isset($arrayLinkSearch['sewerage'])){
													$current = ' class="current"';
													$nameParams = $_SEWERAGE[$n];
												}
												$sewerage .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_SEWERAGE[$n].'</a></li>';
											}
											else {
												$current = '';
												if($currentParams==$n){
													$current = ' class="current"';
													$nameParams = $_SEWERAGE[$n];
												}
												$sewerage .= '<li'.$current.'><a data-name="sewerage" data-explanation="Канализация" data-tags="'.$_SEWERAGE[$n].'" href="javascript:void(0)" data-id="'.$n.'">'.$_SEWERAGE[$n].'</a></li>';
												$sewerageArray[$n] = $_SEWERAGE[$n];	
											}
										}
										$sewerageChangeCountrySell = '';
										if($arrayLinkSearch['sewerage'] && $type_country_sell_var){
											$c1 = $arrayLinkSearch['sewerage'];
											$sewerageChangeCountrySell = '<li><a class="min" data-name_select="sewerage" data-name="sewerage" data-type="checkbox_block" data-id="'.$arrayLinkSearch['sewerage'].'" href="javascript:void(0)">Канализация '.$sewerageArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
										}
										echo '<input type="hidden" name="sewerage"'.$valueParams.'>';
										echo '<div style="width:148px" class="choosed_block">'.$nameParams.'</div>';
										echo '<div class="scrollbar-inner">';
										echo '<ul>';
										echo $sewerage;
										echo '</ul>';
										echo '</div>';
										?>
									</div>
								</div>
							</div>
							<div style="margin-left:41px" class="item">
								<div style="width:110px;margin-left:0;float:left;margin-right:-28px;margin-top:5px;text-align:right;" class="label_select">Электричество</div>
								<div class="checkbox_block">
									<?
									// $valueParams = '';
									// if($type_country_sell_var && $electricity_country_sell_var){
										// $valueParams = ' value="'.$electricity_country_sell_var.'"';
									// }
									?>
									<input type="hidden" name="electricity"<?=$valueParams?>>
									<div class="select_checkbox">
										<ul>
											<?
											// for($c=1; $c<count($_WARM_PARAMS); $c++){
												// $checked = '';
												// if($c==$electricity_country_sell_var){
													// $checked = ' class="checked"';
												// }
												// echo '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_WARM_PARAMS[$c].'</a></li>';
												// if($c==2){
													// $checked = '';
													// if(!$electricity_country_sell_var){
														// $checked = ' class="checked"';
													// }
													// echo '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="">'.$_WARM_PARAMS[0].'</a></li>';
												// }
											// }
											?>
										</ul>
									</div>
								</div>
								<div style="height:14px" class="clear"></div>
								<div style="width:110px;margin-left:0;float:left;margin-right:-28px;margin-top:5px;text-align:right;" class="label_select">Отопление</div>
								<div class="checkbox_block">
									<?
									// $valueParams = '';
									// if($type_country_sell_var && $heating_country_sell_var){
										// $valueParams = ' value="'.$heating_country_sell_var.'"';
									// }
									?>
									<input type="hidden" name="heating"<?=$valueParams?>>
									<div class="select_checkbox">
										<ul>
											<?
											// for($c=1; $c<count($_WARM_PARAMS); $c++){
												// $checked = '';
												// if($c==$heating_country_sell_var){
													// $checked = ' class="checked"';
												// }
												// echo '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_WARM_PARAMS[$c].'</a></li>';
												// if($c==2){
													// $checked = '';
													// if(!$heating_country_sell_var){
														// $checked = ' class="checked"';
													// }
													// echo '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="">'.$_WARM_PARAMS[0].'</a></li>';
												// }
											// }
											?>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div style="width:110px;margin-left:0;float:left;margin-right:-28px;margin-top:5px;text-align:right;" class="label_select">Газ</div>
								<div class="checkbox_block">
									<?
									// $valueParams = '';
									// if($type_country_sell_var && $gas_country_sell_var){
										// $valueParams = ' value="'.$gas_country_sell_var.'"';
									// }
									?>
									<input type="hidden" name="gas"<?=$valueParams?>>
									<div class="select_checkbox">
										<ul>
											<?
											// for($c=1; $c<count($_WARM_PARAMS); $c++){
												// $checked = '';
												// if($c==$gas_country_sell_var){
													// $checked = ' class="checked"';
												// }
												// echo '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_WARM_PARAMS[$c].'</a></li>';
												// if($c==2){
													// $checked = '';
													// if(!$gas_country_sell_var){
														// $checked = ' class="checked"';
													// }
													// echo '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="">'.$_WARM_PARAMS[0].'</a></li>';
												// }
											// }
											?>
										</ul>
									</div>
								</div>
							</div>
						</div>-->
					</div>
					<!--<div style="margin-left:30px" class="line"></div>-->
					<div class="right_block">
						<div style="margin-top:0px;overflow:visible" class="check_block">
							<div style="width:auto" class="left">
								<div class="metric">
									<div class="cell">
										<div style="width:110px" class="label_select">Расстояние до СПб</div>
										<div class="select_block price reg ch_price">
											<?
											$far_subwayChangeCountrySell = '';
											if($arrayLinkSearch['far_subway'] && $type_country_sell_var){
												$c1 = $arrayLinkSearch['far_subway'];
												$name_value = $_FAR_SUBWAY[$c1];
												if($c1==1){
													$name_value = '< 500 м';
												}
												$far_subwayChangeCountrySell = '<li><a class="min" data-name_select="far_subway" data-name="far_subway" data-type="checkbox_block" data-id="'.$arrayLinkSearch['far_subway'].'" href="javascript:void(0)">До СПб '.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
											}
											
											$nameParams = 'До';
											$valueParams = '';
											$currentParams = '';
											if($type_country_sell_var && $arrayLinkSearch['far_subway']){
												$currentParams = (int)$arrayLinkSearch['far_subway'];
												$valueParams = ' value="'.$arrayLinkSearch['far_subway'].'"';
											}
											$far_subway = '';
											for($f=1; $f<=count($_FAR_SUBWAY); $f++){
												if($f==1){
													$current = '';
													if($currentParams==0){
														$current = ' class="current"';
														$nameParams = 'До';
													}
													$far_subway .= '<li'.$current.'><a href="javascript:void(0)" data-id="">До</a></li>';
												}
												$current = '';
												if($currentParams==$f){
													$current = ' class="current"';
													$nameParams = $_FAR_SUBWAY[$f];
												}
												$name_value = $_FAR_SUBWAY[$f];
												if($f==1){
													$name_value = '< 500 м';
												}
												$far_subway .= '<li'.$current.'><a data-name="far_subway" data-explanation="До СПб" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'">'.$_FAR_SUBWAY[$f].'</a></li>';
											}
											echo '<input type="hidden" name="far_subway"'.$valueParams.'>';
											echo '<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="10" placeholder="До" type="text" value="'.$arrayLinkSearch['squareLandMax'].'"><span class="namePrice">'.$nameParams.'</span></div>';
											echo '<div class="scrollbar-inner">';
											echo '<ul>';
											echo $far_subway;
											echo '</ul>';
											echo '</div>';
											?>
										</div>
										<div style="margin-top:6px" class="com">км</div>
									</div>
									<div class="cell">
										<div style="width:135px" class="label_select">Коммуникации</div>
										<div class="select_block count multiple">
											<?
											$communicationsChangeCountrySell = '';
											$ex_array = explode(',',$arrayLinkSearch['communications']);
											if($arrayLinkSearch['communications'] && $type_country_sell_var){
												$c1 = $arrayLinkSearch['communications'];
												$ex_c1 = explode(',',$c1);
												for($e=0; $e<count($ex_c1); $e++){
													$name_value = $_COMM_COUNTRY[$ex_c1[$e]];
													$communicationsChangeCountrySell .= '<li><a class="min" data-name_select="communications" data-name="communications" data-type="checkbox_block" data-id="'.$ex_c1[$e].'" data-multiple="true" href="javascript:void(0)">'.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
												}
											}
											
											$nameParams = 'Не важно';
											$valueParams = '';
											$currentParams = '';
											if($type_country_sell_var && $arrayLinkSearch['communications']){
												$currentParams = (int)$arrayLinkSearch['communications'];
												$valueParams = ' value="'.$arrayLinkSearch['communications'].'"';
											}
											$communications = '';
											for($f=1; $f<count($_COMM_COUNTRY); $f++){
												if($f==1){
													$current = '';
													if($currentParams==0){
														$current = ' class="current"';
														$nameParams = 'Не важно';
													}
													// $communications .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
												}
												$current = '';
												// if($currentParams==$f){
													// $current = ' class="current"';
													// $nameParams = $_COMM_COUNTRY[$f];
												// }
												if(in_array($f,$ex_array)){
													$current = ' class="current"';
													$nameParams = $_COMM_COUNTRY[$f];
												}
												$name_value = $_COMM_COUNTRY[$f];
												$communications .= '<li'.$current.'><a data-name="communications" data-explanation="" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'"><span class="check"></span><span class="label">'.$_COMM_COUNTRY[$f].'</span></a></li>';
											}
											if(count($ex_array)>1){
												$nameParams = count($ex_array).' параметра';
											}
											echo '<input type="hidden" name="communications"'.$valueParams.'>';
											echo '<div style="width:143px" class="choosed_block">'.$nameParams.'</div>';
											echo '<div class="scrollbar-inner">';
											echo '<ul>';
											echo $communications;
											echo '</ul>';
											echo '</div>';
											?>
										</div>
									</div>
									<div class="cell">
										<div style="width:95px" class="label_select">Тип дома</div>
										<div class="select_block count multiple">
											<?
											$type_houseChangeCountrySell = '';
											$ex_array = explode(',',$arrayLinkSearch['type_house']);
											if($arrayLinkSearch['type_house'] && $type_country_sell_var){
												$c1 = $arrayLinkSearch['type_house'];
												$ex_c1 = explode(',',$c1);
												for($e=0; $e<count($ex_c1); $e++){
													$name_value = $_TYPE_HOUSE_COUNTRY[$ex_c1[$e]];
													$type_houseChangeCountrySell .= '<li><a class="min" data-name_select="type_house" data-name="type_house" data-type="checkbox_block" data-id="'.$ex_c1[$e].'" data-multiple="true" href="javascript:void(0)">'.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
												}
											}
											
											$nameParams = 'Не важно';
											$valueParams = '';
											$currentParams = '';
											if($type_country_sell_var && $arrayLinkSearch['type_house']){
												$currentParams = (int)$arrayLinkSearch['type_house'];
												$valueParams = ' value="'.$arrayLinkSearch['type_house'].'"';
											}
											$type_house = '';
											for($f=1; $f<count($_TYPE_HOUSE_COUNTRY); $f++){
												if($f==1){
													$current = '';
													if($currentParams==0){
														$current = ' class="current"';
														$nameParams = 'Не важно';
													}
													// $type_house .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
												}
												$current = '';
												// if($currentParams==$f){
													// $current = ' class="current"';
													// $nameParams = $_TYPE_HOUSE_COUNTRY[$f];
												// }
												if(in_array($f,$ex_array)){
													$current = ' class="current"';
													$nameParams = $_TYPE_HOUSE_COUNTRY[$f];
												}
												$name_value = $_TYPE_HOUSE_COUNTRY[$f];
												$type_house .= '<li'.$current.'><a data-name="type_house" data-explanation="" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'"><span class="check"></span><span class="label">'.$_TYPE_HOUSE_COUNTRY[$f].'</span></a></li>';
											}
											if(count($ex_array)>1){
												$nameParams = count($ex_array).' параметра';
											}
											echo '<input type="hidden" name="type_house"'.$valueParams.'>';
											echo '<div style="width:143px" class="choosed_block">'.$nameParams.'</div>';
											echo '<div class="scrollbar-inner">';
											echo '<ul>';
											echo $type_house;
											echo '</ul>';
											echo '</div>';
											?>
										</div>
									</div>
								</div>
								<div class="type_country">
									<div class="check_block">
										<div class="checkbox_block">
											<div class="block_checkbox big comm">
												<?
												$checked = '';
												$valueParams = '';
												$disabled = ' disabled="disabled"';
												if(isset($water_country_sell_var)){
													if($water_country_sell_var==1){
														$disabled = '';
														$checked = ' checked';
														$valueParams = ' value="'.$water_country_sell_var.'"';
													}
												}
												?>
												<div class="checkbox_single<?=$checked?>">
													<input type="hidden"<?=$valueParams?> name="water"<?=$disabled?>>
													<div class="container">
														<span class="check"></span><span class="label">Водоем</span>
													</div>
												</div>
												<?
												$checked = '';
												$valueParams = '';
												$disabled = ' disabled="disabled"';
												if(isset($forest_country_sell_var)){
													if($forest_country_sell_var==1){
														$disabled = '';
														$checked = ' checked';
														$valueParams = ' value="'.$forest_country_sell_var.'"';
													}
												}
												?>
												<div class="checkbox_single<?=$checked?>">
													<input type="hidden"<?=$valueParams?> name="forest"<?=$disabled?>>
													<div class="container">
														<span class="check"></span><span class="label">Рядом лес</span>
													</div>
												</div>
												<?
												$checked = '';
												$valueParams = '';
												$disabled = ' disabled="disabled"';
												if(isset($sauna_country_sell_var)){
													if($sauna_country_sell_var==1){
														$disabled = '';
														$checked = ' checked';
														$valueParams = ' value="'.$sauna_country_sell_var.'"';
													}
												}
												?>
												<div class="checkbox_single<?=$checked?>">
													<input type="hidden"<?=$valueParams?>  name="sauna"<?=$disabled?>>
													<div class="container">
														<span class="check"></span><span class="label">Баня</span>
													</div>
												</div>
												<?
												$checked = '';
												$valueParams = '';
												$disabled = ' disabled="disabled"';
												if(isset($garage_country_sell_var)){
													if($garage_country_sell_var==1){
														$disabled = '';
														$checked = ' checked';
														$valueParams = ' value="'.$garage_country_sell_var.'"';
													}
												}
												?>
												<div class="checkbox_single<?=$checked?>">
													<input type="hidden"<?=$valueParams?> name="garage"<?=$disabled?>>
													<div class="container">
														<span class="check"></span><span class="label">Гараж</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div style="margin-top:0" class="row rent">
					<div class="country_left_block">
						<div class="left_block">
							<div class="metric">
								<div style="width:99px" class="label_select">Площадь дома</div>
								<div class="select_block price reg ch_price">
								<?
								$squareChangeCountryRent = '';
								if($arrayLinkSearch['squareFullMin'] && !$arrayLinkSearch['squareFullMax'] && $type_country_rent_var){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareFullMin'];
									$squareChangeCountryRent = '<li><a class="min" data-name_select="fullsquarecountryrent" data-name="squareFullMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_country_rent_var){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareFullMax'];
									$squareChangeCountryRent = '<li><a class="max" data-name_select="fullsquarecountryrent" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMax'].'" href="javascript:void(0)">общая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_country_rent_var){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareFullMin'];
									$c2 = $arrayLinkSearch['squareFullMax'];
									$squareChangeCountryRent = '<li><a class="min max" data-name_select="fullsquarecountryrent" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_country_rent_var && $arrayLinkSearch['squareFullMin']){
									$nameParams = $arrayLinkSearch['squareFullMin'];
									$currentParams = $arrayLinkSearch['squareFullMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMin'].'"';
								}
								?>
								<input type="hidden" name="squareFullMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="10" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareFullMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
									<div class="scrollbar-inner">
										<ul>
											<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
											<?
											for($c=$min_full_square_country_rent; $c<=$max_full_square_country_rent;){
												$current = "";
												if($c==$currentParams){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a data-name="fullsquarecountryrent" data-explanation="общая" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
												if($c==$max_full_square_country_rent){
													break;
												}
												if($c<=40){
													$c = $c+1;
												}
												if($c>40 && $c<=100){
													$c = $c+2;
												}
												if($c>100){
													$c = $c+5;
												}
												if($c>$max_full_square_country_rent){
													$c = $max_full_square_country_rent;
												}
											}
											?>
										</ul>
									</div>
								</div>
								<div class="mdash">&ndash;</div>
								<div class="select_block price reg ch_price">
									<?
									$nameParams = 'До';
									$valueParams = '';
									$currentParams = '';
									if($type_country_rent_var && $arrayLinkSearch['squareFullMax']){
										$nameParams = str_replace(".", ",", $arrayLinkSearch['squareFullMax']);
										$currentParams = $arrayLinkSearch['squareFullMax'];
										$valueParams = ' value="'.$arrayLinkSearch['squareFullMax'].'"';
									}
									?>
									<input type="hidden" name="squareFullMax"<?=$valueParams?>>
									<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="10" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareFullMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
									<div class="scrollbar-inner">
										<ul>
											<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
											<?
											for($c=$min_full_square_country_rent; $c<=$max_full_square_country_rent;){
												$current = "";
												if($c==$currentParams){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a data-name="fullsquarecountryrent" data-explanation="общая" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
												if($c==$max_full_square_country_rent){
													break;
												}
												if($c<=40){
													$c = $c+1;
												}
												if($c>40 && $c<=100){
													$c = $c+2;
												}
												if($c>100){
													$c = $c+5;
												}
												if($c>$max_full_square_country_rent){
													$c = $max_full_square_country_rent;
												}
											}
											?>
										</ul>
									</div>
								</div>
								<div class="com">м<sup>2</sup></div>
							</div>
							<div class="metric">
								<div style="width:110px;margin-left:-11px" class="label_select">Расстояние до СПб</div>
								<div class="select_block price reg ch_price">
									<?
									$far_subwayChangeCountryRent = '';
									if($arrayLinkSearch['far_subway'] && $type_country_rent_var){
										$c1 = $arrayLinkSearch['far_subway'];
										$name_value = $_FAR_SUBWAY[$c1];
										if($c1==1){
											$name_value = '< 500 м';
										}
										$far_subwayChangeCountryRent = '<li><a class="min" data-name_select="far_subway" data-name="far_subway" data-type="checkbox_block" data-id="'.$arrayLinkSearch['far_subway'].'" href="javascript:void(0)">До СПб '.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									
									$nameParams = 'До';
									$valueParams = '';
									$currentParams = '';
									if($type_country_rent_var && $arrayLinkSearch['far_subway']){
										$currentParams = (int)$arrayLinkSearch['far_subway'];
										$valueParams = ' value="'.$arrayLinkSearch['far_subway'].'"';
									}
									$far_subway = '';
									for($f=1; $f<=count($_FAR_SUBWAY); $f++){
										if($f==1){
											$current = '';
											if($currentParams==0){
												$current = ' class="current"';
												$nameParams = 'До';
											}
											$far_subway .= '<li'.$current.'><a href="javascript:void(0)" data-id="">До</a></li>';
										}
										$current = '';
										if($currentParams==$f){
											$current = ' class="current"';
											$nameParams = $_FAR_SUBWAY[$f];
										}
										$name_value = $_FAR_SUBWAY[$f];
										if($f==1){
											$name_value = '< 500 м';
										}
										$far_subway .= '<li'.$current.'><a data-name="far_subway" data-explanation="До СПб" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'">'.$_FAR_SUBWAY[$f].'</a></li>';
									}
									echo '<input type="hidden" name="far_subway"'.$valueParams.'>';
									echo '<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="10" placeholder="До" type="text" value="'.$arrayLinkSearch['squareLandMax'].'"><span class="namePrice">'.$nameParams.'</span></div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $far_subway;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
								<div style="margin-top:6px" class="com">км</div>
							</div>
							<div class="metric">
								<!--<div style="width:80px;margin-left:-30px;margin-top:-2px;" class="label_select">Дата публикации</div>-->
								<div style="margin-left:-10px" class="label_select">Дата публикации</div>
								<div class="select_block count">
									<?
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_country_rent_var && $arrayLinkSearch['dates']){
										$currentParams = (int)$arrayLinkSearch['dates'];
										$valueParams = ' value="'.$arrayLinkSearch['dates'].'"';
									}
									$dates = '';
									$datesArray = array();
									for($n=0; $n<count($_DATE_POST); $n++){
										if($n==0){
											$current = '';
											if($currentParams=='' && !isset($arrayLinkSearch['dates'])){
												$current = ' class="current"';
												$nameParams = $_DATE_POST[0]['name'];
											}
											$dates .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_DATE_POST[0]['name'].'</a></li>';
										}
										else {
											$current = '';
											if($currentParams==$_DATE_POST[$n]['id']){
												$current = ' class="current"';
												$nameParams = $_DATE_POST[$n]['name'];
											}
											$dates .= '<li'.$current.'><a data-name="dates" data-explanation="" data-tags="'.$_DATE_POST[$n]['name'].'" href="javascript:void(0)" data-id="'.$_DATE_POST[$n]['id'].'">'.$_DATE_POST[$n]['name'].'</a></li>';
											$datesArray[$_DATE_POST[$n]['id']] = $_DATE_POST[$n]['name'];	
										}
									}
									$datesChangeCountryRent = '';
									if($arrayLinkSearch['dates'] && $type_country_rent_var){
										$c1 = $arrayLinkSearch['dates'];
										// for(){
											
										// }
										$datesChangeCountryRent = '<li><a class="min" data-name_select="dates" data-name="dates" data-type="checkbox_block" data-id="'.$arrayLinkSearch['dates'].'" href="javascript:void(0)">'.$datesArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									echo '<input type="hidden" name="dates"'.$valueParams.'>';
									echo '<div style="width:158px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $dates;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
						</div>
						<!--<div style="margin-left:10px" class="center_block">
							<div class="metric">
								<div style="width:122px" class="label_select">Площадь участка</div>
								<div class="select_block count">
								<?
								$squareLandChangeCountryRent = '';
								if($arrayLinkSearch['squareLandMin'] && !$arrayLinkSearch['squareLandMax'] && $type_country_rent_var){ //только метраж "от"
									$c1 = $arrayLinkSearch['squareLandMin'];
									$squareLandChangeCountryRent = '<li><a class="min" data-name_select="fulllandsquarecountryrent" data-name="squareLandMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareLandMin'].'" href="javascript:void(0)">Площадь участка от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['squareLandMin'] && $arrayLinkSearch['squareLandMax'] && $type_country_rent_var){ //только метраж "до"
									$c2 = $arrayLinkSearch['squareLandMax'];
									$squareLandChangeCountryRent = '<li><a class="max" data-name_select="fulllandsquarecountryrent" data-name="squareLandMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareLandMax'].'" href="javascript:void(0)">Площадь участка до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['squareLandMin'] && $arrayLinkSearch['squareLandMax'] && $type_country_rent_var){ //промежуток метражей
									$c1 = $arrayLinkSearch['squareLandMin'];
									$c2 = $arrayLinkSearch['squareLandMax'];
									$squareLandChangeCountryRent = '<li><a class="min max" data-name_select="fulllandsquarecountryrent" data-name="squareLandMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareLandMin'].'" href="javascript:void(0)">Площадь участкаы от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_country_rent_var && $arrayLinkSearch['squareLandMin']){
									$nameParams = $arrayLinkSearch['squareLandMin'];
									$currentParams = $arrayLinkSearch['squareLandMin'];
									$valueParams = ' value="'.$arrayLinkSearch['squareLandMin'].'"';
								}
								?>
								<input type="hidden" name="squareLandMin"<?=$valueParams?>>
								<div style="width:65px" class="choosed_block"><?=$nameParams?></div>
									<div class="scrollbar-inner">
										<ul>
											<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
											<?
											for($c=$min_full_land_square_country_rent; $c<=$max_full_land_square_country_rent;){
												$current = "";
												if($c==$currentParams){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a data-name="fulllandsquarecountryrent" data-explanation="Площадь участка" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
												if($c==$max_full_land_square_country_rent){
													break;
												}
												if($c<=40){
													$c = $c+1;
												}
												if($c>40 && $c<=100){
													$c = $c+2;
												}
												if($c>100){
													$c = $c+5;
												}
												if($c>$max_full_land_square_country_rent){
													$c = $max_full_land_square_country_rent;
												}
											}
											?>
										</ul>
									</div>
								</div>
								<div class="mdash">&ndash;</div>
								<div class="select_block count">
									<?
									$nameParams = 'До';
									$valueParams = '';
									$currentParams = '';
									if($type_country_rent_var && $arrayLinkSearch['squareLandMax']){
										$nameParams = str_replace(".", ",", $arrayLinkSearch['squareLandMax']);
										$currentParams = $arrayLinkSearch['squareLandMax'];
										$valueParams = ' value="'.$arrayLinkSearch['squareLandMax'].'"';
									}
									?>
									<input type="hidden" name="squareLandMax"<?=$valueParams?>>
									<div style="width:65px" class="choosed_block"><?=$nameParams?></div>
									<div class="scrollbar-inner">
										<ul>
											<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
											<?
											for($c=$min_full_land_square_country_rent; $c<=$max_full_land_square_country_rent;){
												$current = "";
												if($c==$currentParams){
													$current = ' class="current"';
												}
												echo '<li'.$current.'><a data-name="fulllandsquarecountryrent" data-explanation="Площадь участка" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
												if($c==$max_full_land_square_country_rent){
													break;
												}
												if($c<=40){
													$c = $c+1;
												}
												if($c>40 && $c<=100){
													$c = $c+2;
												}
												if($c>100){
													$c = $c+5;
												}
												if($c>$max_full_land_square_country_rent){
													$c = $max_full_land_square_country_rent;
												}
											}
											?>
										</ul>
									</div>
								</div>
								<div style="margin-top:6px" class="com">соток</div>
							</div>
						</div>
						<div class="right_block">
							<div class="metric">
								<div style="width:130px;margin-left:-30px;margin-top:5px;" class="label_select">Юридический статус</div>
								<div class="select_block count">
									<?
									$nameParams = 'Не важно';
									$valueParams = '';
									$currentParams = '';
									if($type_country_rent_var && $arrayLinkSearch['legal_status']){
										$currentParams = (int)$arrayLinkSearch['legal_status'];
										$valueParams = ' value="'.$arrayLinkSearch['legal_status'].'"';
									}
									$legal_status = '';
									$legal_statusArray = array();
									for($n=0; $n<count($_LEGAL_STATUS); $n++){
										if($n==0){
											$current = '';
											if($currentParams=='' && !isset($arrayLinkSearch['legal_status'])){
												$current = ' class="current"';
												$nameParams = $_LEGAL_STATUS[$n];
											}
											$legal_status .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_LEGAL_STATUS[$n].'</a></li>';
										}
										else {
											$current = '';
											if($currentParams==$n){
												$current = ' class="current"';
												$nameParams = $_LEGAL_STATUS[$n];
											}
											$legal_status .= '<li'.$current.'><a data-name="legal_status" data-explanation="Юридический статус" data-tags="'.$_LEGAL_STATUS[$n].'" href="javascript:void(0)" data-id="'.$n.'">'.$_LEGAL_STATUS[$n].'</a></li>';
											$legal_statusArray[$n] = $_LEGAL_STATUS[$n];	
										}
									}
									$legal_statusChangeCountryRent = '';
									if($arrayLinkSearch['legal_status'] && $type_country_rent_var){
										$c1 = $arrayLinkSearch['legal_status'];
										$legal_statusChangeCountryRent = '<li><a class="min" data-name_select="legal_status" data-name="legal_status" data-type="checkbox_block" data-id="'.$arrayLinkSearch['legal_status'].'" href="javascript:void(0)">Юридический статус '.$legal_statusArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
									echo '<input type="hidden" name="legal_status"'.$valueParams.'>';
									echo '<div style="width:130px" class="choosed_block">'.$nameParams.'</div>';
									echo '<div class="scrollbar-inner">';
									echo '<ul>';
									echo $legal_status;
									echo '</ul>';
									echo '</div>';
									?>
								</div>
							</div>
						</div>
						<div class="type_country">
							<div style="width:83px;text-align:left;margin-left:8px;margin-top:19px" class="label_select">Тип строения</div>
							<div class="check_block">
								<div class="checkbox_block">
									<?
									$ex_type_house_country_rent_var = array();
									$valueInput = '';
									$typeHouseChangeCountryRent = '';
									if($type_house_country_rent_var){
										$ex_type_house_country_rent_var = explode(',',$type_house_country_rent_var);
										$valueInput = ' value="'.$type_house_country_rent_var.'"';
										$id_room = 0;
										$name_room = '';
										for($com=1; $com<=count($_TYPE_HOUSE_COUNTRY); $com++){
											if(in_array($com,$ex_type_house_country_rent_var)){
												$typeHouseChangeCountryRent .= '<li><a data-name="type_house" data-type="checkbox_select" data-id="'.$com.'" href="javascript:void(0)">'.$_TYPE_HOUSE_COUNTRY[$com].'<span onclick="return delTags(this)" class="close"></span></a></li>';
											}
										}
									}
									?>
									<input type="hidden" name="type_house"<?=$valueInput?>>
									<div class="block_checkbox big comm">
										<ul>
											<?
											for($com=1; $com<=count($_TYPE_HOUSE_COUNTRY)-1; $com++){	
												echo '<div data-id="'.$com.'" class="checkbox_single '.(in_array($com,$ex_type_house_country_rent_var)?'checked':"").'">
													<div class="container">
														<span class="check"></span><span class="label">'.$_TYPE_HOUSE_COUNTRY[$com].'</span>
													</div>
												</div>';
											}
											?>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="separated" style="display:block!important;"></div>
						<div class="country_params">
							<div class="item">
								<div class="metric">
									<div style="width:129px;margin-left:-30px;margin-top:5px;" class="label_select">Водопровод</div>
									<div class="select_block count">
										<?
										$nameParams = 'Не важно';
										$valueParams = '';
										$currentParams = '';
										if($type_country_rent_var && $arrayLinkSearch['plumbing']){
											$currentParams = (int)$arrayLinkSearch['plumbing'];
											$valueParams = ' value="'.$arrayLinkSearch['plumbing'].'"';
										}
										$plumbing = '';
										$plumbingArray = array();
										for($n=0; $n<count($_PLUMBING); $n++){
											if($n==0){
												$current = '';
												if($currentParams=='' && !isset($arrayLinkSearch['plumbing'])){
													$current = ' class="current"';
													$nameParams = $_PLUMBING[$n];
												}
												$plumbing .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_PLUMBING[$n].'</a></li>';
											}
											else {
												$current = '';
												if($currentParams==$n){
													$current = ' class="current"';
													$nameParams = $_PLUMBING[$n];
												}
												$plumbing .= '<li'.$current.'><a data-name="plumbing" data-explanation="Водопровод" data-tags="'.$_PLUMBING[$n].'" href="javascript:void(0)" data-id="'.$n.'">'.$_PLUMBING[$n].'</a></li>';
												$plumbingArray[$n] = $_PLUMBING[$n];
											}
										}
										$plumbingChangeCountryRent = '';
										if($arrayLinkSearch['plumbing'] && $type_country_rent_var){
											$c1 = $arrayLinkSearch['plumbing'];
											$plumbingChangeCountryRent = '<li><a class="min" data-name_select="plumbing" data-name="plumbing" data-type="checkbox_block" data-id="'.$arrayLinkSearch['plumbing'].'" href="javascript:void(0)">Водопровод '.$plumbingArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
										}
										echo '<input type="hidden" name="plumbing"'.$valueParams.'>';
										echo '<div style="width:148px" class="choosed_block">'.$nameParams.'</div>';
										echo '<div class="scrollbar-inner">';
										echo '<ul>';
										echo $plumbing;
										echo '</ul>';
										echo '</div>';
										?>
									</div>
								</div>
								<div class="metric">
									<div style="width:129px;margin-left:-30px;margin-top:5px;" class="label_select">Канализация</div>
									<div class="select_block count">
										<?
										$nameParams = 'Не важно';
										$valueParams = '';
										$currentParams = '';
										if($type_country_rent_var && $arrayLinkSearch['sewerage']){
											$currentParams = (int)$arrayLinkSearch['sewerage'];
											$valueParams = ' value="'.$arrayLinkSearch['sewerage'].'"';
										}
										$sewerage = '';
										$sewerageArray = array();
										for($n=0; $n<count($_SEWERAGE); $n++){
											if($n==0){
												$current = '';
												if($currentParams=='' && !isset($arrayLinkSearch['sewerage'])){
													$current = ' class="current"';
													$nameParams = $_SEWERAGE[$n];
												}
												$sewerage .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_SEWERAGE[$n].'</a></li>';
											}
											else {
												$current = '';
												if($currentParams==$n){
													$current = ' class="current"';
													$nameParams = $_SEWERAGE[$n];
												}
												$sewerage .= '<li'.$current.'><a data-name="sewerage" data-explanation="Канализация" data-tags="'.$_SEWERAGE[$n].'" href="javascript:void(0)" data-id="'.$n.'">'.$_SEWERAGE[$n].'</a></li>';
												$sewerageArray[$n] = $_SEWERAGE[$n];	
											}
										}
										$sewerageChangeCountryRent = '';
										if($arrayLinkSearch['sewerage'] && $type_country_rent_var){
											$c1 = $arrayLinkSearch['sewerage'];
											$sewerageChangeCountryRent = '<li><a class="min" data-name_select="sewerage" data-name="sewerage" data-type="checkbox_block" data-id="'.$arrayLinkSearch['sewerage'].'" href="javascript:void(0)">Канализация '.$sewerageArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
										}
										echo '<input type="hidden" name="sewerage"'.$valueParams.'>';
										echo '<div style="width:148px" class="choosed_block">'.$nameParams.'</div>';
										echo '<div class="scrollbar-inner">';
										echo '<ul>';
										echo $sewerage;
										echo '</ul>';
										echo '</div>';
										?>
									</div>
								</div>
							</div>
							<div style="margin-left:41px" class="item">
								<div style="width:110px;margin-left:0;float:left;margin-right:-28px;margin-top:5px;text-align:right;" class="label_select">Электричество</div>
								<div class="checkbox_block">
									<?
									$valueParams = '';
									if($type_country_rent_var && $electricity_country_rent_var){
										$valueParams = ' value="'.$electricity_country_rent_var.'"';
									}
									?>
									<input type="hidden" name="electricity"<?=$valueParams?>>
									<div class="select_checkbox">
										<ul>
											<?
											for($c=1; $c<count($_WARM_PARAMS); $c++){
												$checked = '';
												if($c==$electricity_country_rent_var){
													$checked = ' class="checked"';
												}
												echo '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_WARM_PARAMS[$c].'</a></li>';
												if($c==2){
													$checked = '';
													if(!$electricity_country_rent_var){
														$checked = ' class="checked"';
													}
													echo '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="">'.$_WARM_PARAMS[0].'</a></li>';
												}
											}
											?>
										</ul>
									</div>
								</div>
								<div style="height:14px" class="clear"></div>
								<div style="width:110px;margin-left:0;float:left;margin-right:-28px;margin-top:5px;text-align:right;" class="label_select">Отопление</div>
								<div class="checkbox_block">
									<?
									$valueParams = '';
									if($type_country_rent_var && $heating_country_rent_var){
										$valueParams = ' value="'.$heating_country_rent_var.'"';
									}
									?>
									<input type="hidden" name="heating"<?=$valueParams?>>
									<div class="select_checkbox">
										<ul>
											<?
											for($c=1; $c<count($_WARM_PARAMS); $c++){
												$checked = '';
												if($c==$heating_country_rent_var){
													$checked = ' class="checked"';
												}
												echo '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_WARM_PARAMS[$c].'</a></li>';
												if($c==2){
													$checked = '';
													if(!$heating_country_rent_var){
														$checked = ' class="checked"';
													}
													echo '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="">'.$_WARM_PARAMS[0].'</a></li>';
												}
											}
											?>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div style="width:110px;margin-left:0;float:left;margin-right:-28px;margin-top:5px;text-align:right;" class="label_select">Газ</div>
								<div class="checkbox_block">
									<?
									$valueParams = '';
									if($type_country_rent_var && $gas_country_rent_var){
										$valueParams = ' value="'.$gas_country_rent_var.'"';
									}
									?>
									<input type="hidden" name="gas"<?=$valueParams?>>
									<div class="select_checkbox">
										<ul>
											<?
											for($c=1; $c<count($_WARM_PARAMS); $c++){
												$checked = '';
												if($c==$gas_country_rent_var){
													$checked = ' class="checked"';
												}
												echo '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_WARM_PARAMS[$c].'</a></li>';
												if($c==2){
													$checked = '';
													if(!$gas_country_rent_var){
														$checked = ' class="checked"';
													}
													echo '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="">'.$_WARM_PARAMS[0].'</a></li>';
												}
											}
											?>
										</ul>
									</div>
								</div>
							</div>
						</div>-->
					</div>
					<!--<div style="margin-left:30px" class="line"></div>-->
					<div class="right_block">
						<div style="margin-top:0px;overflow:visible" class="check_block">
							<div style="width:auto" class="left">
								<div class="metric">
									<div class="cell">
										<div class="label_select" style="float:left;width:80px;margin-top:5px;text-align:right">Срок аренды</div>
										<div class="select_block count">
											<?
											$periodChangeCountryRent = '';
											if($arrayLinkSearch['period'] && $type_country_rent_var){
												$c1 = $arrayLinkSearch['period'];
												$periodChangeCountryRent = '<li><a class="min" data-name_select="period" data-name="period" data-type="checkbox_block" data-id="'.$arrayLinkSearch['period'].'" href="javascript:void(0)">'.$_TYPE_PERIOD_RENT[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
											}
											
											$nameParams = 'Не важно';
											$valueParams = '';
											$currentParams = '';
											if($type_country_rent_var && $arrayLinkSearch['period']){
												$currentParams = (int)$arrayLinkSearch['period'];
												$valueParams = ' value="'.$arrayLinkSearch['period'].'"';
											}
											$period = '';
											for($n=0; $n<count($_TYPE_PERIOD_RENT); $n++){
												if($n==0){
													$current = '';
													if($currentParams==0){
														$current = ' class="current"';
														$nameParams = $_TYPE_PERIOD_RENT[0];
													}
													$period .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_TYPE_PERIOD_RENT[0].'</a></li>';
												}
												else {
													$current = '';
													if($currentParams==$n){
														$current = ' class="current"';
														$nameParams = $_TYPE_PERIOD_RENT[$n];
													}
													$period .= '<li'.$current.'><a data-name="period" data-explanation="" data-tags="'.clearValueText($_TYPE_PERIOD_RENT[$n]).'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_PERIOD_RENT[$n].'</a></li>';
												}
											}
											echo '<input type="hidden" name="period"'.$valueParams.'>';
											echo '<div style="width:110px" class="choosed_block">'.$nameParams.'</div>';
											echo '<div class="scrollbar-inner">';
											echo '<ul>';
											echo $period;
											echo '</ul>';
											echo '</div>';
											?>
										</div>
									</div>
									<div class="cell">
										<div style="width:135px" class="label_select">Коммуникации</div>
										<div class="select_block count multiple">
											<?
											$communicationsChangeCountryRent = '';
											$ex_array = explode(',',$arrayLinkSearch['communications']);
											if($arrayLinkSearch['communications'] && $type_country_rent_var){
												$c1 = $arrayLinkSearch['communications'];
												$ex_c1 = explode(',',$c1);
												for($e=0; $e<count($ex_c1); $e++){
													$name_value = $_COMM_COUNTRY[$ex_c1[$e]];
													$communicationsChangeCountryRent .= '<li><a class="min" data-name_select="communications" data-name="communications" data-type="checkbox_block" data-id="'.$ex_c1[$e].'" data-multiple="true" href="javascript:void(0)">'.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
												}
											}
											
											$nameParams = 'Не важно';
											$valueParams = '';
											$currentParams = '';
											if($type_country_rent_var && $arrayLinkSearch['communications']){
												$currentParams = (int)$arrayLinkSearch['communications'];
												$valueParams = ' value="'.$arrayLinkSearch['communications'].'"';
											}
											$communications = '';
											for($f=1; $f<count($_COMM_COUNTRY); $f++){
												if($f==1){
													$current = '';
													if($currentParams==0){
														$current = ' class="current"';
														$nameParams = 'Не важно';
													}
													// $communications .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
												}
												$current = '';
												// if($currentParams==$f){
													// $current = ' class="current"';
													// $nameParams = $_COMM_COUNTRY[$f];
												// }
												if(in_array($f,$ex_array)){
													$current = ' class="current"';
													$nameParams = $_COMM_COUNTRY[$f];
												}
												$name_value = $_COMM_COUNTRY[$f];
												$communications .= '<li'.$current.'><a data-name="communications" data-explanation="" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'"><span class="check"></span><span class="label">'.$_COMM_COUNTRY[$f].'</span></a></li>';
											}
											if(count($ex_array)>1){
												$nameParams = count($ex_array).' параметра';
											}
											echo '<input type="hidden" name="communications"'.$valueParams.'>';
											echo '<div style="width:143px" class="choosed_block">'.$nameParams.'</div>';
											echo '<div class="scrollbar-inner">';
											echo '<ul>';
											echo $communications;
											echo '</ul>';
											echo '</div>';
											?>
										</div>
									</div>
									<div class="cell">
										<div style="width:95px" class="label_select">Тип дома</div>
										<div class="select_block count multiple">
											<?
											$type_houseChangeCountryRent = '';
											$ex_array = explode(',',$arrayLinkSearch['type_house']);
											if($arrayLinkSearch['type_house'] && $type_country_rent_var){
												$c1 = $arrayLinkSearch['type_house'];
												$ex_c1 = explode(',',$c1);
												for($e=0; $e<count($ex_c1); $e++){
													$name_value = $_TYPE_HOUSE_COUNTRY[$ex_c1[$e]];
													$type_houseChangeCountryRent .= '<li><a class="min" data-name_select="type_house" data-name="type_house" data-type="checkbox_block" data-id="'.$ex_c1[$e].'" data-multiple="true" href="javascript:void(0)">'.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
												}
											}
											
											$nameParams = 'Не важно';
											$valueParams = '';
											$currentParams = '';
											if($type_country_rent_var && $arrayLinkSearch['type_house']){
												$currentParams = (int)$arrayLinkSearch['type_house'];
												$valueParams = ' value="'.$arrayLinkSearch['type_house'].'"';
											}
											$type_house = '';
											for($f=1; $f<count($_TYPE_HOUSE_COUNTRY); $f++){
												if($f==1){
													$current = '';
													if($currentParams==0){
														$current = ' class="current"';
														$nameParams = 'Не важно';
													}
													// $type_house .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
												}
												$current = '';
												// if($currentParams==$f){
													// $current = ' class="current"';
													// $nameParams = $_TYPE_HOUSE_COUNTRY[$f];
												// }
												if(in_array($f,$ex_array)){
													$current = ' class="current"';
													$nameParams = $_TYPE_HOUSE_COUNTRY[$f];
												}
												$name_value = $_TYPE_HOUSE_COUNTRY[$f];
												$type_house .= '<li'.$current.'><a data-name="type_house" data-explanation="" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'"><span class="check"></span><span class="label">'.$_TYPE_HOUSE_COUNTRY[$f].'</span></a></li>';
											}
											if(count($ex_array)>1){
												$nameParams = count($ex_array).' параметра';
											}
											echo '<input type="hidden" name="type_house"'.$valueParams.'>';
											echo '<div style="width:143px" class="choosed_block">'.$nameParams.'</div>';
											echo '<div class="scrollbar-inner">';
											echo '<ul>';
											echo $type_house;
											echo '</ul>';
											echo '</div>';
											?>
										</div>
									</div>
								</div>
								<div class="type_country">
									<div class="check_block">
										<div class="checkbox_block">
											<div class="block_checkbox big comm">
												<?
												$checked = '';
												$valueParams = '';
												$disabled = ' disabled="disabled"';
												if(isset($water_country_rent_var)){
													if($water_country_rent_var==1){
														$disabled = '';
														$checked = ' checked';
														$valueParams = ' value="'.$water_country_rent_var.'"';
													}
												}
												?>
												<div class="checkbox_single<?=$checked?>">
													<input type="hidden"<?=$valueParams?> name="water"<?=$disabled?>>
													<div class="container">
														<span class="check"></span><span class="label">Водоем</span>
													</div>
												</div>
												<?
												$checked = '';
												$valueParams = '';
												$disabled = ' disabled="disabled"';
												if(isset($forest_country_rent_var)){
													if($forest_country_rent_var==1){
														$disabled = '';
														$checked = ' checked';
														$valueParams = ' value="'.$forest_country_rent_var.'"';
													}
												}
												?>
												<div class="checkbox_single<?=$checked?>">
													<input type="hidden"<?=$valueParams?> name="forest"<?=$disabled?>>
													<div class="container">
														<span class="check"></span><span class="label">Рядом лес</span>
													</div>
												</div>
												<?
												$checked = '';
												$valueParams = '';
												$disabled = ' disabled="disabled"';
												if(isset($sauna_country_rent_var)){
													if($sauna_country_rent_var==1){
														$disabled = '';
														$checked = ' checked';
														$valueParams = ' value="'.$sauna_country_rent_var.'"';
													}
												}
												?>
												<div class="checkbox_single<?=$checked?>">
													<input type="hidden"<?=$valueParams?>  name="sauna"<?=$disabled?>>
													<div class="container">
														<span class="check"></span><span class="label">Баня</span>
													</div>
												</div>
												<?
												$checked = '';
												$valueParams = '';
												$disabled = ' disabled="disabled"';
												if(isset($garage_country_rent_var)){
													if($garage_country_rent_var==1){
														$disabled = '';
														$checked = ' checked';
														$valueParams = ' value="'.$garage_country_rent_var.'"';
													}
												}
												?>
												<div class="checkbox_single<?=$checked?>">
													<input type="hidden"<?=$valueParams?> name="garage"<?=$disabled?>>
													<div class="container">
														<span class="check"></span><span class="label">Гараж</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div <?$estateValue!=5?print'style="display:none"':'style="display:block"'?> class="inner_block commercial">
				<div style="display:none" class="row sell comm_show_param type_commer_1">
					<div class="left_block">
						<div class="metric ">
							<div class="label_select">Общая площадь</div>
							<div class="select_block price reg ch_price">
							<?
							$squareChangeCommercialSell = '';
							if($arrayLinkSearch['squareFullMin'] && !$arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //только метраж "от"
								$c1 = $arrayLinkSearch['squareFullMin'];
								$squareChangeCommercialSell = '<li><a class="min" data-name_select="fullsquarecommercialsell" data-name="squareFullMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(!$arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //только метраж "до"
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeCommercialSell = '<li><a class="max" data-name_select="fullsquarecommercialsell" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMax'].'" href="javascript:void(0)">общая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if($arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //промежуток метражей
								$c1 = $arrayLinkSearch['squareFullMin'];
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeCommercialSell = '<li><a class="min max" data-name_select="fullsquarecommercialsell" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}

							$nameParams = 'От';
							$valueParams = '';
							$currentParams = '';
							if($type_commercial_sell_var && $arrayLinkSearch['squareFullMin']){
								$nameParams = $arrayLinkSearch['squareFullMin'];
								$currentParams = $arrayLinkSearch['squareFullMin'];
								$valueParams = ' value="'.$arrayLinkSearch['squareFullMin'].'"';
							}
							?>
							<input type="hidden" name="squareFullMin"<?=$valueParams?>>
							<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareFullMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_full_square_commercial_sell; $c<=$max_full_square_commercial_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquarecommercialsell" data-explanation="общая" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_commercial_sell){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_commercial_sell){
												$c = $max_full_square_commercial_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price reg ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['squareFullMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareFullMax']);
									$currentParams = $arrayLinkSearch['squareFullMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMax'].'"';
								}
								?>
								<input type="hidden" name="squareFullMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareFullMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_full_square_commercial_sell; $c<=$max_full_square_commercial_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquarecommercialsell" data-explanation="общая" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_commercial_sell){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_commercial_sell){
												$c = $max_full_square_commercial_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div style="margin-top:15px" class="metric">
							<div class="label_select">Этаж</div>
							<div class="select_block price sec ch_price">
								<?
								$floorChangeCommercialSell = '';
								if($arrayLinkSearch['floorMin'] && !$arrayLinkSearch['floorMax'] && $type_commercial_sell_var){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorMin'];
									$floorChangeCommercialSell = '<li><a class="min" data-name_select="floorsquare" data-name="floorMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_commercial_sell_var){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeCommercialSell = '<li><a class="max" data-name_select="floorsquare" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMax'].'" href="javascript:void(0)">этаж по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_commercial_sell_var){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorMin'];
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeCommercialSell = '<li><a class="min max" data-name_select="floorsquare" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['floorMin']){
									$nameParams = $arrayLinkSearch['floorMin'];
									$currentParams = $arrayLinkSearch['floorMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMin'].'"';
								}
								?>
								<input type="hidden" name="floorMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="2" placeholder="От" type="text" value="<?=$arrayLinkSearch['floorMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$max_floors_new_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquare" data-explanation="этаж" data-tags="с '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price sec ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['floorMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorMax']);
									$currentParams = $arrayLinkSearch['floorMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMax'].'"';
								}
								?>
								<input type="hidden" name="floorMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="2" placeholder="До" type="text" value="<?=$arrayLinkSearch['floorMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_new_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquare" data-explanation="этаж" data-tags="по '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
						</div>
						
						<!--<div class="metric">
							<div class="label_select">Площадь зала</div>
							<div class="select_block reg price ch_price">
							<?
/* 							$squarePlaceCommercialSell = '';
							if($arrayLinkSearch['squarePlaceMin'] && !$arrayLinkSearch['squarePlaceMax'] && $type_commercial_sell_var){ //только метраж "от"
								$c1 = $arrayLinkSearch['squarePlaceMin'];
								$squarePlaceCommercialSell = '<li><a class="min" data-name_select="fullsquareflatsell" data-name="squarePlaceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squarePlaceMin'].'" href="javascript:void(0)">Площадь зала от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(!$arrayLinkSearch['squarePlaceMin'] && $arrayLinkSearch['squarePlaceMax'] && $type_commercial_sell_var){ //только метраж "до"
								$c2 = $arrayLinkSearch['squarePlaceMax'];
								$squarePlaceCommercialSell = '<li><a class="max" data-name_select="fullsquareflatsell" data-name="squarePlaceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squarePlaceMax'].'" href="javascript:void(0)">Площадь зала до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if($arrayLinkSearch['squarePlaceMin'] && $arrayLinkSearch['squarePlaceMax'] && $type_commercial_sell_var){ //промежуток метражей
								$c1 = $arrayLinkSearch['squarePlaceMin'];
								$c2 = $arrayLinkSearch['squarePlaceMax'];
								$squarePlaceCommercialSell = '<li><a class="min max" data-name_select="fullsquareflatsell" data-name="squarePlaceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squarePlaceMin'].'" href="javascript:void(0)">Площадь зала от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}

							$nameParams = 'От';
							$valueParams = '';
							$currentParams = '';
							if($type_commercial_sell_var && $arrayLinkSearch['squarePlaceMin']){
								$nameParams = $arrayLinkSearch['squarePlaceMin'];
								$currentParams = $arrayLinkSearch['squarePlaceMin'];
								$valueParams = ' value="'.$arrayLinkSearch['squarePlaceMin'].'"';
							}
 */							?>
							<input type="hidden" name="squarePlaceMin"<?=$valueParams?>>
							<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="От" type="text" value="<?=$arrayLinkSearch['squarePlaceMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
/* 										for($c=$min_place_square_commercial_sell; $c<=$max_place_square_commercial_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquareflatsell" data-explanation="Площадь зала" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_place_square_commercial_sell){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_place_square_commercial_sell){
												$c = $max_place_square_commercial_sell;
											}
										}
 */										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block reg price ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['squarePlaceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squarePlaceMax']);
									$currentParams = $arrayLinkSearch['squarePlaceMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squarePlaceMax'].'"';
								}
								?>
								<input type="hidden" name="squarePlaceMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="До" type="text" value="<?=$arrayLinkSearch['squarePlaceMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_place_square_commercial_sell; $c<=$max_place_square_commercial_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquareflatsell" data-explanation="Площадь зала" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_place_square_commercial_sell){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_place_square_commercial_sell){
												$c = $max_place_square_commercial_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>-->
					</div>
					<div style="width:250px" class="center_block">
						<div class="metric">
							<div style="margin-left:-55px;margin-top:5px;" class="label_select">Тип здания</div>
							<div class="select_block count">
								<?
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['type_build']){
									$currentParams = (int)$arrayLinkSearch['type_build'];
									$valueParams = ' value="'.$arrayLinkSearch['type_build'].'"';
								}
								$type_build = '';
								$type_buildArray = array();
								for($n=0; $n<count($_TYPE_BUILD_OFFICE); $n++){
									if($n==0){
										$current = '';
										if($currentParams=='' && !isset($arrayLinkSearch['type_build'])){
											$current = ' class="current"';
											$nameParams = $_TYPE_BUILD_OFFICE[$n];
										}
										$type_build .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_TYPE_BUILD_OFFICE[$n].'</a></li>';
									}
									else {
										$current = '';
										if($currentParams==$n){
											$current = ' class="current"';
											$nameParams = $_TYPE_BUILD_OFFICE[$n];
										}
										$type_build .= '<li'.$current.'><a data-name="type_build" data-explanation="" data-tags="'.$_TYPE_BUILD_OFFICE[$n].'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_BUILD_OFFICE[$n].'</a></li>';
										$type_buildArray[$n] = $_TYPE_BUILD_OFFICE[$n];	 
									}
								}
								$type_buildChangeCommercialSell = '';
								if($arrayLinkSearch['type_build'] && $type_commercial_sell_var){
									$c1 = $arrayLinkSearch['type_build'];
									$type_buildChangeCommercialSell = '<li><a class="min" data-name_select="type_build" data-name="type_build" data-type="checkbox_block" data-id="'.$arrayLinkSearch['type_build'].'" href="javascript:void(0)">'.$type_buildArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								echo '<input type="hidden" name="type_build"'.$valueParams.'>';
								echo '<div style="width:150px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $type_build;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
						<div class="metric">
							<div style="margin-left:-54px;margin-right:20px;text-align:right;margin-top:5px;" class="label_select">Класс объекта</div>
							<?
							
							/*
							*  Класс объекта
							*/
							$classObject = '';
							$classObjects = '';
							$valueInput = '';
							$class_objectChangeCommercialSell = '';
							for($c=1; $c<=count($_CLASS_OBJECTS); $c++){
								$checked = '';
								$ex_class_object_commercial_sell_var = array();
								$valueInput = '';
								if(isset($arrayLinkSearch['class_object'])){
									$ex_class_object_commercial_sell_var = explode(',',$arrayLinkSearch['class_object']);
									$valueInput = ' value="'.$arrayLinkSearch['class_object'].'"';
									$id_room = 0;
									$name_room = '';
									if(in_array($c,$ex_class_object_commercial_sell_var)){
										$checked = ' class="checked"';
										$valueInput = ' value="'.$c.'"';
										$class_objectChangeCommercialSell .= '<li><a data-name="class_object" data-type="checkbox_select" data-id="0" href="javascript:void(0)">'.$_CLASS_OBJECTS[$c].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
								}
								if(empty($arrayLinkSearch['class_object'])){
									if($c==0){
										$checked = ' class="checked"';
										$valueInput = ' value="'.$c.'"';				
									}
								}
								$classObjects .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-tags="'.$_CLASS_OBJECTS[$c].'" data-id="'.$c.'">'.$_CLASS_OBJECTS[$c].'</a></li>';
							}
							$classObject = '<div style="margin-left:-10px" class="checkbox_block">';
							$classObject .= '<input type="hidden" name="class_object"'.$valueInput.'>';
							$classObject .= '<div class="block_checkbox">';
							$classObject .= '<ul>';
							$classObject .= $classObjects;
							$classObject .= '</ul>';
							$classObject .= '</div>';
							$classObject .= '</div>';
							
							
							$squareAreaCommercialSell = '';
							if($arrayLinkSearch['squareAreaMin'] && !$arrayLinkSearch['squareAreaMax'] && $type_commercial_sell_var){ //только метраж "от"
								$c1 = $arrayLinkSearch['squareAreaMin'];
								$squareAreaCommercialSell = '<li><a class="min" data-name_select="fullsquareflatsell" data-name="squareAreaMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareAreaMin'].'" href="javascript:void(0)">Площадь участка от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(!$arrayLinkSearch['squareAreaMin'] && $arrayLinkSearch['squareAreaMax'] && $type_commercial_sell_var){ //только метраж "до"
								$c2 = $arrayLinkSearch['squareAreaMax'];
								$squareAreaCommercialSell = '<li><a class="max" data-name_select="fullsquareflatsell" data-name="squareAreaMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareAreaMax'].'" href="javascript:void(0)">Площадь участка до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if($arrayLinkSearch['squareAreaMin'] && $arrayLinkSearch['squareAreaMax'] && $type_commercial_sell_var){ //промежуток метражей
								$c1 = $arrayLinkSearch['squareAreaMin'];
								$c2 = $arrayLinkSearch['squareAreaMax'];
								$squareAreaCommercialSell = '<li><a class="min max" data-name_select="fullsquareflatsell" data-name="squareAreaMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareAreaMin'].'" href="javascript:void(0)">Площадь участка от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}

							$nameParams = 'От';
							$valueParams = '';
							$currentParams = '';
							if($type_commercial_sell_var && $arrayLinkSearch['squareAreaMin']){
								$nameParams = $arrayLinkSearch['squareAreaMin'];
								$currentParams = $arrayLinkSearch['squareAreaMin'];
								$valueParams = ' value="'.$arrayLinkSearch['squareAreaMin'].'"';
							}
							echo $classObject;
							?>
						</div>
					</div>
					<div style="height:66px" class="line"></div>
					<div class="left_block">
						<div class="metric">
							<div style="width:90px" class="label_select">Состояние</div>
							<div class="select_block count multiple">
								<?
								$categoriesChangeCommercialSell = '';
								$ex_array = explode(',',$arrayLinkSearch['categories']);
								if($arrayLinkSearch['categories'] && $type_country_sell_var){
									$c1 = $arrayLinkSearch['categories'];
									$ex_c1 = explode(',',$c1);
									for($e=0; $e<count($ex_c1); $e++){
										$name_value = $_CONDITIONS_SKLAD[$ex_c1[$e]];
										$categoriesChangeCommercialSell .= '<li><a class="min" data-name_select="categories" data-name="categories" data-type="checkbox_block" data-id="'.$ex_c1[$e].'" data-multiple="true" href="javascript:void(0)">'.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
								}
								
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_country_sell_var && $arrayLinkSearch['conditions']){
									$currentParams = (int)$arrayLinkSearch['conditions'];
									$valueParams = ' value="'.$arrayLinkSearch['conditions'].'"';
								}
								$conditions = '';
								for($f=1; $f<count($_CONDITIONS_SKLAD); $f++){
									if($f==1){
										$current = '';
										if($currentParams==0){
											$current = ' class="current"';
											$nameParams = 'Не важно';
										}
										// $conditions .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
									}
									$current = '';
									// if($currentParams==$f){
										// $current = ' class="current"';
										// $nameParams = $_CONDITIONS_SKLAD[$f];
									// }
									if(in_array($f,$ex_array)){
										$current = ' class="current"';
										$nameParams = $_CONDITIONS_SKLAD[$f];
									}
									$name_value = $_CONDITIONS_SKLAD[$f];
									$conditions .= '<li'.$current.'><a data-name="conditions" data-explanation="" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'"><span class="check"></span><span class="label">'.$_CONDITIONS_SKLAD[$f].'</span></a></li>';
								}
								if(count($ex_array)>1){
									$nameParams = count($ex_array).' параметра';
								}
								echo '<input type="hidden" name="conditions"'.$valueParams.'>';
								echo '<div style="width:143px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $conditions;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
						<div class="metric">
							<div style="width:90px" class="label_select">Мебель</div>
							<div class="select_block count multiple">
								<?
								/*
								*  Мебель
								*/
								$furnitures = '';
								$furniture = '';
								$valueInput = '';
								$furnitureChangeCommercialSell .= '';
								for($c=0; $c<count($_FURNITURE); $c++){
									$checked = '';
									if(isset($arrayLinkSearch['furniture']) && $arrayLinkSearch['furniture']==$c){
										$checked = ' class="checked"';
										$valueInput = ' value="'.$c.'"';
									}
									$furniture .= '<li'.$checked.'><a style="padding:4px 7px;font-size:14px" href="javascript:void(0)" data-id="'.$c.'">'.$_FURNITURE[$c].'</a></li>';
								}
								$furnitures = '<div class="checkbox_block">';
								$furnitures .= '<input type="hidden" name="furniture"'.$valueInput.'>';
								$furnitures .= '<div class="select_checkbox">';
								$furnitures .= '<ul>';
								$furnitures .= $furniture;
								$furnitures .= '</ul>';
								$furnitures .= '</div>';
								$furnitures .= '</div>';
								
								echo $furnitures;
								?>
							</div>
						</div>
						<div class="metric">
							<div style="width:90px" class="label_select">Готовность</div>
							<div class="select_block count multiple">
								<?
								/*
								*  Готовность
								*/
								$readis = '';
								$readiss = '';
								$valueInput = '';
								for($c=0; $c<count($_READIS); $c++){
									$checked = '';
									if(isset($arrayLinkSearch['readis']) && $arrayLinkSearch['readis']==$c){
										$checked = ' class="checked"';
										$valueInput = ' value="'.$c.'"';
									}
									$readiss .= '<li'.$checked.'><a style="padding:4px 7px;font-size:14px" href="javascript:void(0)" data-id="'.$c.'">'.$_READIS[$c].'</a></li>';
								}
								$readis = '<div class="checkbox_block">';
								$readis .= '<input type="hidden" name="readis"'.$valueInput.'>';
								$readis .= '<div class="select_checkbox">';
								$readis .= '<ul>';
								$readis .= $readiss;
								$readis .= '</ul>';
								$readis .= '</div>';
								$readis .= '</div>';
								
								echo $readis;
								?>
							</div>
						</div>
					</div>
					<!--<div style="margin-left:10px" class="right_block">
						<div style="margin-top:6px" class="check_block">
							<?
/* 							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($entrance_commercial_sell_var)){
								if($entrance_commercial_sell_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$entrance_commercial_sell_var.'"';
								}
							}
 */							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="entrance"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Отдельный вход</span>
								</div>
							</div>
						</div>-->
						<!--<div class="metric">
							<div style="width:67px;margin-left:-14px" class="label_select">До метро</div>
							<div class="select_block count">
								<?
/* 								$far_subwayChangeCommercialSell = '';
								if($arrayLinkSearch['far_subway'] && $type_commercial_sell_var){
									$c1 = $arrayLinkSearch['far_subway'];
									$name_value = $_FAR_SUBWAY[$c1];
									if($c1==1){
										$name_value = '< 500 м';
									}
									$far_subwayChangeCommercialSell = '<li><a class="min" data-name_select="far_subway" data-name="far_subway" data-type="checkbox_block" data-id="'.$arrayLinkSearch['far_subway'].'" href="javascript:void(0)">Метро '.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['far_subway']){
									$currentParams = (int)$arrayLinkSearch['far_subway'];
									$valueParams = ' value="'.$arrayLinkSearch['far_subway'].'"';
								}
								$far_subway = '';
								for($f=1; $f<=count($_FAR_SUBWAY); $f++){
									if($f==1){
										$current = '';
										if($currentParams==0){
											$current = ' class="current"';
											$nameParams = 'Не важно';
										}
										$far_subway .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
									}
									$current = '';
									if($currentParams==$f){
										$current = ' class="current"';
										$nameParams = $_FAR_SUBWAY[$f];
									}
									$name_value = $_FAR_SUBWAY[$f];
									if($f==1){
										$name_value = '< 500 м';
									}
									$far_subway .= '<li'.$current.'><a data-name="far_subway" data-explanation="Метро" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'">'.$_FAR_SUBWAY[$f].'</a></li>';
								}
								echo '<input type="hidden" name="far_subway"'.$valueParams.'>';
								echo '<div style="width:143px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $far_subway;
								echo '</ul>';
								echo '</div>';
 */								?>
							</div>
						</div>
						<div class="clear"></div>
					</div>-->
				</div>
				<div style="display:none" class="row sell comm_show_param type_commer_2">
					<div class="left_block">
						<div class="metric ">
							<div class="label_select">Общая площадь</div>
							<div class="select_block price reg ch_price">
							<?
							$squareChangeCommercialSell = '';
							if($arrayLinkSearch['squareFullMin'] && !$arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //только метраж "от"
								$c1 = $arrayLinkSearch['squareFullMin'];
								$squareChangeCommercialSell = '<li><a class="min" data-name_select="fullsquarecommercialsell" data-name="squareFullMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(!$arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //только метраж "до"
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeCommercialSell = '<li><a class="max" data-name_select="fullsquarecommercialsell" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMax'].'" href="javascript:void(0)">общая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if($arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //промежуток метражей
								$c1 = $arrayLinkSearch['squareFullMin'];
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeCommercialSell = '<li><a class="min max" data-name_select="fullsquarecommercialsell" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}

							$nameParams = 'От';
							$valueParams = '';
							$currentParams = '';
							if($type_commercial_sell_var && $arrayLinkSearch['squareFullMin']){
								$nameParams = $arrayLinkSearch['squareFullMin'];
								$currentParams = $arrayLinkSearch['squareFullMin'];
								$valueParams = ' value="'.$arrayLinkSearch['squareFullMin'].'"';
							}
							?>
							<input type="hidden" name="squareFullMin"<?=$valueParams?>>
							<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareFullMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_full_square_commercial_sell; $c<=$max_full_square_commercial_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquarecommercialsell" data-explanation="общая" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_commercial_sell){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_commercial_sell){
												$c = $max_full_square_commercial_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price reg ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['squareFullMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareFullMax']);
									$currentParams = $arrayLinkSearch['squareFullMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMax'].'"';
								}
								?>
								<input type="hidden" name="squareFullMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareFullMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_full_square_commercial_sell; $c<=$max_full_square_commercial_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquarecommercialsell" data-explanation="общая" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_commercial_sell){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_commercial_sell){
												$c = $max_full_square_commercial_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div style="margin-top:15px" class="metric">
							<div class="label_select">Этаж</div>
							<div class="select_block price sec ch_price">
								<?
								$floorChangeCommercialSell = '';
								if($arrayLinkSearch['floorMin'] && !$arrayLinkSearch['floorMax'] && $type_commercial_sell_var){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorMin'];
									$floorChangeCommercialSell = '<li><a class="min" data-name_select="floorsquare" data-name="floorMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_commercial_sell_var){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeCommercialSell = '<li><a class="max" data-name_select="floorsquare" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMax'].'" href="javascript:void(0)">этаж по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_commercial_sell_var){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorMin'];
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeCommercialSell = '<li><a class="min max" data-name_select="floorsquare" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['floorMin']){
									$nameParams = $arrayLinkSearch['floorMin'];
									$currentParams = $arrayLinkSearch['floorMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMin'].'"';
								}
								?>
								<input type="hidden" name="floorMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="2" placeholder="От" type="text" value="<?=$arrayLinkSearch['floorMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$max_floors_new_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquare" data-explanation="этаж" data-tags="с '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price sec ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['floorMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorMax']);
									$currentParams = $arrayLinkSearch['floorMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMax'].'"';
								}
								?>
								<input type="hidden" name="floorMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="2" placeholder="До" type="text" value="<?=$arrayLinkSearch['floorMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_new_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquare" data-explanation="этаж" data-tags="по '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div style="width:250px" class="center_block">
						<div class="metric">
							<div style="margin-left:-55px;margin-top:5px;" class="label_select">Тип здания</div>
							<div class="select_block count">
								<?
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['type_build']){
									$currentParams = (int)$arrayLinkSearch['type_build'];
									$valueParams = ' value="'.$arrayLinkSearch['type_build'].'"';
								}
								$type_build = '';
								$type_buildArray = array();
								for($n=0; $n<count($_TYPE_BUILD_OFFICE); $n++){
									if($n==0){
										$current = '';
										if($currentParams=='' && !isset($arrayLinkSearch['type_build'])){
											$current = ' class="current"';
											$nameParams = $_TYPE_BUILD_OFFICE[$n];
										}
										$type_build .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_TYPE_BUILD_OFFICE[$n].'</a></li>';
									}
									else {
										$current = '';
										if($currentParams==$n){
											$current = ' class="current"';
											$nameParams = $_TYPE_BUILD_OFFICE[$n];
										}
										$type_build .= '<li'.$current.'><a data-name="type_build" data-explanation="" data-tags="'.$_TYPE_BUILD_OFFICE[$n].'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_BUILD_OFFICE[$n].'</a></li>';
										$type_buildArray[$n] = $_TYPE_BUILD_OFFICE[$n];	 
									}
								}
								$type_buildChangeCommercialSell = '';
								if($arrayLinkSearch['type_build'] && $type_commercial_sell_var){
									$c1 = $arrayLinkSearch['type_build'];
									$type_buildChangeCommercialSell = '<li><a class="min" data-name_select="type_build" data-name="type_build" data-type="checkbox_block" data-id="'.$arrayLinkSearch['type_build'].'" href="javascript:void(0)">'.$type_buildArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								echo '<input type="hidden" name="type_build"'.$valueParams.'>';
								echo '<div style="width:150px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $type_build;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
						<div class="metric">
							<div style="margin-left:-54px;margin-right:20px;text-align:right;margin-top:5px;" class="label_select">Класс объекта</div>
							<?
							
							/*
							*  Класс объекта
							*/
							$classObject = '';
							$classObjects = '';
							$valueInput = '';
							$class_objectChangeCommercialSell = '';
							for($c=1; $c<=count($_CLASS_OBJECTS); $c++){
								$checked = '';
								$ex_class_object_commercial_sell_var = array();
								$valueInput = '';
								if(isset($arrayLinkSearch['class_object'])){
									$ex_class_object_commercial_sell_var = explode(',',$arrayLinkSearch['class_object']);
									$valueInput = ' value="'.$arrayLinkSearch['class_object'].'"';
									$id_room = 0;
									$name_room = '';
									if(in_array($c,$ex_class_object_commercial_sell_var)){
										$checked = ' class="checked"';
										$valueInput = ' value="'.$c.'"';
										$class_objectChangeCommercialSell .= '<li><a data-name="class_object" data-type="checkbox_select" data-id="0" href="javascript:void(0)">'.$_CLASS_OBJECTS[$c].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
								}
								if(empty($arrayLinkSearch['class_object'])){
									if($c==0){
										$checked = ' class="checked"';
										$valueInput = ' value="'.$c.'"';				
									}
								}
								$classObjects .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-tags="'.$_CLASS_OBJECTS[$c].'" data-id="'.$c.'">'.$_CLASS_OBJECTS[$c].'</a></li>';
							}
							$classObject = '<div style="margin-left:-10px" class="checkbox_block">';
							$classObject .= '<input type="hidden" name="class_object"'.$valueInput.'>';
							$classObject .= '<div class="block_checkbox">';
							$classObject .= '<ul>';
							$classObject .= $classObjects;
							$classObject .= '</ul>';
							$classObject .= '</div>';
							$classObject .= '</div>';
							
							
							$squareAreaCommercialSell = '';
							if($arrayLinkSearch['squareAreaMin'] && !$arrayLinkSearch['squareAreaMax'] && $type_commercial_sell_var){ //только метраж "от"
								$c1 = $arrayLinkSearch['squareAreaMin'];
								$squareAreaCommercialSell = '<li><a class="min" data-name_select="fullsquareflatsell" data-name="squareAreaMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareAreaMin'].'" href="javascript:void(0)">Площадь участка от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(!$arrayLinkSearch['squareAreaMin'] && $arrayLinkSearch['squareAreaMax'] && $type_commercial_sell_var){ //только метраж "до"
								$c2 = $arrayLinkSearch['squareAreaMax'];
								$squareAreaCommercialSell = '<li><a class="max" data-name_select="fullsquareflatsell" data-name="squareAreaMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareAreaMax'].'" href="javascript:void(0)">Площадь участка до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if($arrayLinkSearch['squareAreaMin'] && $arrayLinkSearch['squareAreaMax'] && $type_commercial_sell_var){ //промежуток метражей
								$c1 = $arrayLinkSearch['squareAreaMin'];
								$c2 = $arrayLinkSearch['squareAreaMax'];
								$squareAreaCommercialSell = '<li><a class="min max" data-name_select="fullsquareflatsell" data-name="squareAreaMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareAreaMin'].'" href="javascript:void(0)">Площадь участка от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}

							$nameParams = 'От';
							$valueParams = '';
							$currentParams = '';
							if($type_commercial_sell_var && $arrayLinkSearch['squareAreaMin']){
								$nameParams = $arrayLinkSearch['squareAreaMin'];
								$currentParams = $arrayLinkSearch['squareAreaMin'];
								$valueParams = ' value="'.$arrayLinkSearch['squareAreaMin'].'"';
							}
							echo $classObject;
							?>
						</div>
					</div>
					<div style="height:66px" class="line"></div>
					<div class="left_block">
						<div class="metric">
							<div style="width:90px" class="label_select">Состояние</div>
							<div class="select_block count multiple">
								<?
								$categoriesChangeCommercialSell = '';
								$ex_array = explode(',',$arrayLinkSearch['categories']);
								if($arrayLinkSearch['categories'] && $type_country_sell_var){
									$c1 = $arrayLinkSearch['categories'];
									$ex_c1 = explode(',',$c1);
									for($e=0; $e<count($ex_c1); $e++){
										$name_value = $_CONDITIONS_SKLAD[$ex_c1[$e]];
										$categoriesChangeCommercialSell .= '<li><a class="min" data-name_select="categories" data-name="categories" data-type="checkbox_block" data-id="'.$ex_c1[$e].'" data-multiple="true" href="javascript:void(0)">'.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
								}
								
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_country_sell_var && $arrayLinkSearch['conditions']){
									$currentParams = (int)$arrayLinkSearch['conditions'];
									$valueParams = ' value="'.$arrayLinkSearch['conditions'].'"';
								}
								$conditions = '';
								for($f=1; $f<count($_CONDITIONS_SKLAD); $f++){
									if($f==1){
										$current = '';
										if($currentParams==0){
											$current = ' class="current"';
											$nameParams = 'Не важно';
										}
										// $conditions .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
									}
									$current = '';
									// if($currentParams==$f){
										// $current = ' class="current"';
										// $nameParams = $_CONDITIONS_SKLAD[$f];
									// }
									if(in_array($f,$ex_array)){
										$current = ' class="current"';
										$nameParams = $_CONDITIONS_SKLAD[$f];
									}
									$name_value = $_CONDITIONS_SKLAD[$f];
									$conditions .= '<li'.$current.'><a data-name="conditions" data-explanation="" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'"><span class="check"></span><span class="label">'.$_CONDITIONS_SKLAD[$f].'</span></a></li>';
								}
								if(count($ex_array)>1){
									$nameParams = count($ex_array).' параметра';
								}
								echo '<input type="hidden" name="conditions"'.$valueParams.'>';
								echo '<div style="width:143px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $conditions;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
						<div class="metric">
							<div style="width:90px" class="label_select">Мебель</div>
							<div class="select_block count multiple">
								<?
								/*
								*  Мебель
								*/
								$furnitures = '';
								$furniture = '';
								$valueInput = '';
								$furnitureChangeCommercialSell .= '';
								for($c=0; $c<count($_FURNITURE); $c++){
									$checked = '';
									if(isset($arrayLinkSearch['furniture']) && $arrayLinkSearch['furniture']==$c){
										$checked = ' class="checked"';
										$valueInput = ' value="'.$c.'"';
									}
									$furniture .= '<li'.$checked.'><a style="padding:4px 7px;font-size:14px" href="javascript:void(0)" data-id="'.$c.'">'.$_FURNITURE[$c].'</a></li>';
								}
								$furnitures = '<div class="checkbox_block">';
								$furnitures .= '<input type="hidden" name="furniture"'.$valueInput.'>';
								$furnitures .= '<div class="select_checkbox">';
								$furnitures .= '<ul>';
								$furnitures .= $furniture;
								$furnitures .= '</ul>';
								$furnitures .= '</div>';
								$furnitures .= '</div>';
								
								echo $furnitures;
								?>
							</div>
						</div>
						<div class="metric">
							<div class="check_block">
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($basement_commercial_sell_var)){
									if($basement_commercial_sell_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$basement_commercial_sell_var.'"';
									}
								}
								?>
								<div style="margin-left:99px;float:left;" class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="basement"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">подвал</span>
									</div>
								</div>
								<?
								$checked = '';
								$valueParams = '';
								$disabled = ' disabled="disabled"';
								if(isset($semibasement_commercial_sell_var)){
									if($semibasement_commercial_sell_var==1){
										$disabled = '';
										$checked = ' checked';
										$valueParams = ' value="'.$semibasement_commercial_sell_var.'"';
									}
								}
								?>
								<div style="margin-left:14px;float:left;" class="checkbox_single<?=$checked?>">
									<input type="hidden"<?=$valueParams?> name="semibasement"<?=$disabled?>>
									<div class="container">
										<span class="check"></span><span class="label">полуподвал</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div style="display:none" class="row sell comm_show_param type_commer_3">
					<div class="left_block">
						<div class="metric ">
							<div class="label_select">Общая площадь</div>
							<div class="select_block price reg ch_price">
							<?
							$squareChangeCommercialSell = '';
							if($arrayLinkSearch['squareFullMin'] && !$arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //только метраж "от"
								$c1 = $arrayLinkSearch['squareFullMin'];
								$squareChangeCommercialSell = '<li><a class="min" data-name_select="fullsquarecommercialsell" data-name="squareFullMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(!$arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //только метраж "до"
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeCommercialSell = '<li><a class="max" data-name_select="fullsquarecommercialsell" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMax'].'" href="javascript:void(0)">общая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if($arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //промежуток метражей
								$c1 = $arrayLinkSearch['squareFullMin'];
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeCommercialSell = '<li><a class="min max" data-name_select="fullsquarecommercialsell" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}

							$nameParams = 'От';
							$valueParams = '';
							$currentParams = '';
							if($type_commercial_sell_var && $arrayLinkSearch['squareFullMin']){
								$nameParams = $arrayLinkSearch['squareFullMin'];
								$currentParams = $arrayLinkSearch['squareFullMin'];
								$valueParams = ' value="'.$arrayLinkSearch['squareFullMin'].'"';
							}
							?>
							<input type="hidden" name="squareFullMin"<?=$valueParams?>>
							<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareFullMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_full_square_commercial_sell; $c<=$max_full_square_commercial_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquarecommercialsell" data-explanation="общая" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_commercial_sell){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_commercial_sell){
												$c = $max_full_square_commercial_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price reg ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['squareFullMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareFullMax']);
									$currentParams = $arrayLinkSearch['squareFullMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMax'].'"';
								}
								?>
								<input type="hidden" name="squareFullMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareFullMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_full_square_commercial_sell; $c<=$max_full_square_commercial_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquarecommercialsell" data-explanation="общая" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_commercial_sell){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_commercial_sell){
												$c = $max_full_square_commercial_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div style="margin-top:15px" class="metric">
							<div class="label_select">Высота потолков</div>
							<div class="select_block price sec ch_price">
								<?
								$floorChangeCommercialSell = '';
								if($arrayLinkSearch['ceiling_height_min'] && !$arrayLinkSearch['ceiling_height_max'] && $type_commercial_sell_var){ //только этажи "с"
									$c1 = $arrayLinkSearch['ceiling_height_min'];
									$floorChangeCommercialSell = '<li><a class="min" data-name_select="floorsquare" data-name="ceiling_height_min" data-type="checkbox_block" data-id="'.$arrayLinkSearch['ceiling_height_min'].'" href="javascript:void(0)">этаж с '.$c1.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['ceiling_height_min'] && $arrayLinkSearch['ceiling_height_max'] && $type_commercial_sell_var){ //только этажи "по"
									$c2 = $arrayLinkSearch['ceiling_height_max'];
									$floorChangeCommercialSell = '<li><a class="max" data-name_select="floorsquare" data-name="ceiling_height_max" data-type="checkbox_block" data-id="'.$arrayLinkSearch['ceiling_height_max'].'" href="javascript:void(0)">этаж по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['ceiling_height_min'] && $arrayLinkSearch['ceiling_height_max'] && $type_commercial_sell_var){ //промежуток этажей
									$c1 = $arrayLinkSearch['ceiling_height_min'];
									$c2 = $arrayLinkSearch['ceiling_height_max'];
									$floorChangeCommercialSell = '<li><a class="min max" data-name_select="floorsquare" data-name="ceiling_height_max" data-type="checkbox_block" data-id="'.$arrayLinkSearch['ceiling_height_min'].'" href="javascript:void(0)">этаж с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['ceiling_height_min']){
									$nameParams = $arrayLinkSearch['ceiling_height_min'];
									$currentParams = $arrayLinkSearch['ceiling_height_min'];
									$valueParams = ' value="'.$arrayLinkSearch['ceiling_height_min'].'"';
								}
								?>
								<input type="hidden" name="ceiling_height_min"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="2" placeholder="От" type="text" value="<?=$arrayLinkSearch['ceiling_height_min']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$max_floors_new_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquare" data-explanation="этаж" data-tags="с '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price sec ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['ceiling_height_max']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['ceiling_height_max']);
									$currentParams = $arrayLinkSearch['ceiling_height_max'];
									$valueParams = ' value="'.$arrayLinkSearch['ceiling_height_max'].'"';
								}
								?>
								<input type="hidden" name="ceiling_height_max"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="2" placeholder="До" type="text" value="<?=$arrayLinkSearch['ceiling_height_max']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_new_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquare" data-explanation="этаж" data-tags="по '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div style="width:auto;margin-top:9px" class="center_block">
						<?
						/*
						*  АЗС
						*/
						$checked = '';
						$valueParams = '';
						$disabled = ' disabled="disabled"';
						if(isset($arrayLinkSearch['azs'])){
							if($arrayLinkSearch['azs']){
								$disabled = '';
								$checked = ' checked';
								$valueParams = ' value="'.$arrayLinkSearch['azs'].'"';
							}
						}
						$azs = '<div class="checkbox_single'.$checked.'">
							<input type="hidden"'.$valueParams.' name="s[azs]"'.$disabled.'>
							<div class="container">
								<span class="check"></span><span class="label">АЗС</span>
							</div>
						</div>';

						/*
						*  Автомойка
						*/
						$checked = '';
						$valueParams = '';
						$disabled = ' disabled="disabled"';
						if(isset($arrayLinkSearch['wash'])){
							if($arrayLinkSearch['wash']){
								$disabled = '';
								$checked = ' checked';
								$valueParams = ' value="'.$arrayLinkSearch['wash'].'"';
							}
						}
						$wash = '<div class="checkbox_single'.$checked.'">
							<input type="hidden"'.$valueParams.' name="s[wash]"'.$disabled.'>
							<div class="container">
								<span class="check"></span><span class="label">Автомойка</span>
							</div>
						</div>';

						/*
						*  Автосервис
						*/
						$checked = '';
						$valueParams = '';
						$disabled = ' disabled="disabled"';
						if(isset($arrayLinkSearch['autoservice'])){
							if($arrayLinkSearch['autoservice']){
								$disabled = '';
								$checked = ' checked';
								$valueParams = ' value="'.$arrayLinkSearch['autoservice'].'"';
							}
						}
						$autoservice = '<div class="checkbox_single'.$checked.'">
							<input type="hidden"'.$valueParams.' name="s[autoservice]"'.$disabled.'>
							<div class="container">
								<span class="check"></span><span class="label">Автосервис</span>
							</div>
						</div>';

						/*
						*  Автостоянка
						*/
						$checked = '';
						$valueParams = '';
						$disabled = ' disabled="disabled"';
						if(isset($arrayLinkSearch['autostop'])){
							if($arrayLinkSearch['autostop']){
								$disabled = '';
								$checked = ' checked';
								$valueParams = ' value="'.$arrayLinkSearch['autostop'].'"';
							}
						}
						$autostop = '<div class="checkbox_single'.$checked.'">
							<input type="hidden"'.$valueParams.' name="s[autostop]"'.$disabled.'>
							<div class="container">
								<span class="check"></span><span class="label">Автостоянка</span>
							</div>
						</div>';

						/*
						*  Машиноместо
						*/
						$checked = '';
						$valueParams = '';
						$disabled = ' disabled="disabled"';
						if(isset($arrayLinkSearch['car_place'])){
							if($arrayLinkSearch['car_place']){
								$disabled = '';
								$checked = ' checked';
								$valueParams = ' value="'.$arrayLinkSearch['car_place'].'"';
							}
						}
						$car_place = '<div class="checkbox_single'.$checked.'">
							<input type="hidden"'.$valueParams.' name="s[car_place]"'.$disabled.'>
							<div class="container">
								<span class="check"></span><span class="label">Машиноместо</span>
							</div>
						</div>';
						
						/*
						*  Гараж
						*/
						$checked = '';
						$valueParams = '';
						$disabled = ' disabled="disabled"';
						if(isset($arrayLinkSearch['garage'])){
							if($arrayLinkSearch['garage']){
								$disabled = '';
								$checked = ' checked';
								$valueParams = ' value="'.$arrayLinkSearch['garage'].'"';
							}
						}
						$garage = '<div class="checkbox_single'.$checked.'">
							<input type="hidden"'.$valueParams.' name="s[garage]"'.$disabled.'>
							<div class="container">
								<span class="check"></span><span class="label">Гараж</span>
							</div>
						</div>';
						?>
						<div style="width:140px;float:left;" class="metric">
							<div class="block">
								<div class="cell one_second last_col">
									<?=$azs?>
								</div>
							</div>
							<div style="margin-top:16px" class="block">
								<div class="cell one_second last_col">
									<?=$wash?>
								</div>
							</div>
						</div>
						<div style="width:140px;float:left;margin-top:0;" class="metric">
							<div class="block">
								<div class="cell one_second last_col">
									<?=$autoservice?>
								</div>
							</div>
							<div style="margin-top:16px" class="block">
								<div class="cell one_second last_col">
									<?=$autostop?>
								</div>
							</div>
						</div>
						<div style="width:140px;float:left;margin-top:0;" class="metric">
							<div class="block">
								<div class="cell one_second last_col">
									<?=$car_place?>
								</div>
							</div>
							<div style="margin-top:16px" class="block">
								<div class="cell one_second last_col">
									<?=$garage?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div style="display:none" class="row sell comm_show_param type_commer_4">
					<div class="left_block">
						<div class="metric ">
							<div class="label_select">Общая площадь</div>
							<div class="select_block price reg ch_price">
							<?
							$squareChangeCommercialSell = '';
							if($arrayLinkSearch['squareFullMin'] && !$arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //только метраж "от"
								$c1 = $arrayLinkSearch['squareFullMin'];
								$squareChangeCommercialSell = '<li><a class="min" data-name_select="fullsquarecommercialsell" data-name="squareFullMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(!$arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //только метраж "до"
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeCommercialSell = '<li><a class="max" data-name_select="fullsquarecommercialsell" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMax'].'" href="javascript:void(0)">общая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if($arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //промежуток метражей
								$c1 = $arrayLinkSearch['squareFullMin'];
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeCommercialSell = '<li><a class="min max" data-name_select="fullsquarecommercialsell" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}

							$nameParams = 'От';
							$valueParams = '';
							$currentParams = '';
							if($type_commercial_sell_var && $arrayLinkSearch['squareFullMin']){
								$nameParams = $arrayLinkSearch['squareFullMin'];
								$currentParams = $arrayLinkSearch['squareFullMin'];
								$valueParams = ' value="'.$arrayLinkSearch['squareFullMin'].'"';
							}
							?>
							<input type="hidden" name="squareFullMin"<?=$valueParams?>>
							<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareFullMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_full_square_commercial_sell; $c<=$max_full_square_commercial_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquarecommercialsell" data-explanation="общая" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_commercial_sell){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_commercial_sell){
												$c = $max_full_square_commercial_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price reg ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['squareFullMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareFullMax']);
									$currentParams = $arrayLinkSearch['squareFullMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMax'].'"';
								}
								?>
								<input type="hidden" name="squareFullMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareFullMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_full_square_commercial_sell; $c<=$max_full_square_commercial_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquarecommercialsell" data-explanation="общая" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_commercial_sell){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_commercial_sell){
												$c = $max_full_square_commercial_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div style="margin-top:15px" class="metric">
							<div class="label_select">Этажей</div>
							<div class="select_block price sec ch_price">
								<?
								$floorChangeCommercialSell = '';
								if($arrayLinkSearch['floorsMin'] && !$arrayLinkSearch['floorsMax'] && $type_commercial_sell_var){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorsMin'];
									$floorChangeCommercialSell = '<li><a class="min" data-name_select="floorsquare" data-name="floorsMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">этаж с '.$c1.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $type_commercial_sell_var){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorsMax'];
									$floorChangeCommercialSell = '<li><a class="max" data-name_select="floorsquare" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMax'].'" href="javascript:void(0)">этаж по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorsMin'] && $arrayLinkSearch['floorsMax'] && $type_commercial_sell_var){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorsMin'];
									$c2 = $arrayLinkSearch['floorsMax'];
									$floorChangeCommercialSell = '<li><a class="min max" data-name_select="floorsquare" data-name="floorsMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorsMin'].'" href="javascript:void(0)">этаж с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['floorsMin']){
									$nameParams = $arrayLinkSearch['floorsMin'];
									$currentParams = $arrayLinkSearch['floorsMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorsMin'].'"';
								}
								?>
								<input type="hidden" name="floorsMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="2" placeholder="От" type="text" value="<?=$arrayLinkSearch['floorsMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$max_floors_new_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquare" data-explanation="этаж" data-tags="с '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price sec ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['floorsMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorsMax']);
									$currentParams = $arrayLinkSearch['floorsMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorsMax'].'"';
								}
								?>
								<input type="hidden" name="floorsMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="2" placeholder="До" type="text" value="<?=$arrayLinkSearch['floorsMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_new_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquare" data-explanation="этаж" data-tags="по '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div style="width:250px" class="center_block">
						<div class="metric">
							<div style="margin-left:-55px;margin-top:5px;" class="label_select">Тип здания</div>
							<div class="select_block count">
								<?
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['type_build']){
									$currentParams = (int)$arrayLinkSearch['type_build'];
									$valueParams = ' value="'.$arrayLinkSearch['type_build'].'"';
								}
								$type_build = '';
								$type_buildArray = array();
								for($n=0; $n<count($_TYPE_BUILD_OFFICE); $n++){
									if($n==0){
										$current = '';
										if($currentParams=='' && !isset($arrayLinkSearch['type_build'])){
											$current = ' class="current"';
											$nameParams = $_TYPE_BUILD_OFFICE[$n];
										}
										$type_build .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_TYPE_BUILD_OFFICE[$n].'</a></li>';
									}
									else {
										$current = '';
										if($currentParams==$n){
											$current = ' class="current"';
											$nameParams = $_TYPE_BUILD_OFFICE[$n];
										}
										$type_build .= '<li'.$current.'><a data-name="type_build" data-explanation="" data-tags="'.$_TYPE_BUILD_OFFICE[$n].'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_BUILD_OFFICE[$n].'</a></li>';
										$type_buildArray[$n] = $_TYPE_BUILD_OFFICE[$n];	 
									}
								}
								$type_buildChangeCommercialSell = '';
								if($arrayLinkSearch['type_build'] && $type_commercial_sell_var){
									$c1 = $arrayLinkSearch['type_build'];
									$type_buildChangeCommercialSell = '<li><a class="min" data-name_select="type_build" data-name="type_build" data-type="checkbox_block" data-id="'.$arrayLinkSearch['type_build'].'" href="javascript:void(0)">'.$type_buildArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								echo '<input type="hidden" name="type_build"'.$valueParams.'>';
								echo '<div style="width:150px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $type_build;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
						<div class="metric">
							<div style="margin-left:-54px;margin-right:20px;text-align:right;margin-top:5px;" class="label_select">Класс объекта</div>
							<?
							
							/*
							*  Класс объекта
							*/
							$classObject = '';
							$classObjects = '';
							$valueInput = '';
							$class_objectChangeCommercialSell = '';
							for($c=1; $c<=count($_CLASS_OBJECTS); $c++){
								$checked = '';
								$ex_class_object_commercial_sell_var = array();
								$valueInput = '';
								if(isset($arrayLinkSearch['class_object'])){
									$ex_class_object_commercial_sell_var = explode(',',$arrayLinkSearch['class_object']);
									$valueInput = ' value="'.$arrayLinkSearch['class_object'].'"';
									$id_room = 0;
									$name_room = '';
									if(in_array($c,$ex_class_object_commercial_sell_var)){
										$checked = ' class="checked"';
										$valueInput = ' value="'.$c.'"';
										$class_objectChangeCommercialSell .= '<li><a data-name="class_object" data-type="checkbox_select" data-id="0" href="javascript:void(0)">'.$_CLASS_OBJECTS[$c].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
								}
								if(empty($arrayLinkSearch['class_object'])){
									if($c==0){
										$checked = ' class="checked"';
										$valueInput = ' value="'.$c.'"';				
									}
								}
								$classObjects .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-tags="'.$_CLASS_OBJECTS[$c].'" data-id="'.$c.'">'.$_CLASS_OBJECTS[$c].'</a></li>';
							}
							$classObject = '<div style="margin-left:-10px" class="checkbox_block">';
							$classObject .= '<input type="hidden" name="class_object"'.$valueInput.'>';
							$classObject .= '<div class="block_checkbox">';
							$classObject .= '<ul>';
							$classObject .= $classObjects;
							$classObject .= '</ul>';
							$classObject .= '</div>';
							$classObject .= '</div>';
							
							
							$squareAreaCommercialSell = '';
							if($arrayLinkSearch['squareAreaMin'] && !$arrayLinkSearch['squareAreaMax'] && $type_commercial_sell_var){ //только метраж "от"
								$c1 = $arrayLinkSearch['squareAreaMin'];
								$squareAreaCommercialSell = '<li><a class="min" data-name_select="fullsquareflatsell" data-name="squareAreaMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareAreaMin'].'" href="javascript:void(0)">Площадь участка от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(!$arrayLinkSearch['squareAreaMin'] && $arrayLinkSearch['squareAreaMax'] && $type_commercial_sell_var){ //только метраж "до"
								$c2 = $arrayLinkSearch['squareAreaMax'];
								$squareAreaCommercialSell = '<li><a class="max" data-name_select="fullsquareflatsell" data-name="squareAreaMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareAreaMax'].'" href="javascript:void(0)">Площадь участка до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if($arrayLinkSearch['squareAreaMin'] && $arrayLinkSearch['squareAreaMax'] && $type_commercial_sell_var){ //промежуток метражей
								$c1 = $arrayLinkSearch['squareAreaMin'];
								$c2 = $arrayLinkSearch['squareAreaMax'];
								$squareAreaCommercialSell = '<li><a class="min max" data-name_select="fullsquareflatsell" data-name="squareAreaMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareAreaMin'].'" href="javascript:void(0)">Площадь участка от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}

							$nameParams = 'От';
							$valueParams = '';
							$currentParams = '';
							if($type_commercial_sell_var && $arrayLinkSearch['squareAreaMin']){
								$nameParams = $arrayLinkSearch['squareAreaMin'];
								$currentParams = $arrayLinkSearch['squareAreaMin'];
								$valueParams = ' value="'.$arrayLinkSearch['squareAreaMin'].'"';
							}
							echo $classObject;
							?>
						</div>
					</div>
					<div style="height:66px" class="line"></div>
					<div class="left_block">
						<div class="metric">
							<div style="width:90px" class="label_select">Состояние</div>
							<div class="select_block count multiple">
								<?
								$categoriesChangeCommercialSell = '';
								$ex_array = explode(',',$arrayLinkSearch['categories']);
								if($arrayLinkSearch['categories'] && $type_country_sell_var){
									$c1 = $arrayLinkSearch['categories'];
									$ex_c1 = explode(',',$c1);
									for($e=0; $e<count($ex_c1); $e++){
										$name_value = $_CONDITIONS_SKLAD[$ex_c1[$e]];
										$categoriesChangeCommercialSell .= '<li><a class="min" data-name_select="categories" data-name="categories" data-type="checkbox_block" data-id="'.$ex_c1[$e].'" data-multiple="true" href="javascript:void(0)">'.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
								}
								
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_country_sell_var && $arrayLinkSearch['conditions']){
									$currentParams = (int)$arrayLinkSearch['conditions'];
									$valueParams = ' value="'.$arrayLinkSearch['conditions'].'"';
								}
								$conditions = '';
								for($f=1; $f<count($_CONDITIONS_SKLAD); $f++){
									if($f==1){
										$current = '';
										if($currentParams==0){
											$current = ' class="current"';
											$nameParams = 'Не важно';
										}
										// $conditions .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
									}
									$current = '';
									// if($currentParams==$f){
										// $current = ' class="current"';
										// $nameParams = $_CONDITIONS_SKLAD[$f];
									// }
									if(in_array($f,$ex_array)){
										$current = ' class="current"';
										$nameParams = $_CONDITIONS_SKLAD[$f];
									}
									$name_value = $_CONDITIONS_SKLAD[$f];
									$conditions .= '<li'.$current.'><a data-name="conditions" data-explanation="" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'"><span class="check"></span><span class="label">'.$_CONDITIONS_SKLAD[$f].'</span></a></li>';
								}
								if(count($ex_array)>1){
									$nameParams = count($ex_array).' параметра';
								}
								echo '<input type="hidden" name="conditions"'.$valueParams.'>';
								echo '<div style="width:143px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $conditions;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
					</div>
				</div>
				<div style="display:none" class="row sell comm_show_param type_commer_5">
					<div class="left_block">
						<div class="metric ">
							<div class="label_select">Общая площадь</div>
							<div class="select_block price reg ch_price">
							<?
							$squareChangeCommercialSell = '';
							if($arrayLinkSearch['squareFullMin'] && !$arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //только метраж "от"
								$c1 = $arrayLinkSearch['squareFullMin'];
								$squareChangeCommercialSell = '<li><a class="min" data-name_select="fullsquarecommercialsell" data-name="squareFullMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(!$arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //только метраж "до"
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeCommercialSell = '<li><a class="max" data-name_select="fullsquarecommercialsell" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMax'].'" href="javascript:void(0)">общая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if($arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //промежуток метражей
								$c1 = $arrayLinkSearch['squareFullMin'];
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeCommercialSell = '<li><a class="min max" data-name_select="fullsquarecommercialsell" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}

							$nameParams = 'От';
							$valueParams = '';
							$currentParams = '';
							if($type_commercial_sell_var && $arrayLinkSearch['squareFullMin']){
								$nameParams = $arrayLinkSearch['squareFullMin'];
								$currentParams = $arrayLinkSearch['squareFullMin'];
								$valueParams = ' value="'.$arrayLinkSearch['squareFullMin'].'"';
							}
							?>
							<input type="hidden" name="squareFullMin"<?=$valueParams?>>
							<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareFullMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_full_square_commercial_sell; $c<=$max_full_square_commercial_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquarecommercialsell" data-explanation="общая" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_commercial_sell){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_commercial_sell){
												$c = $max_full_square_commercial_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price reg ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['squareFullMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareFullMax']);
									$currentParams = $arrayLinkSearch['squareFullMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMax'].'"';
								}
								?>
								<input type="hidden" name="squareFullMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareFullMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_full_square_commercial_sell; $c<=$max_full_square_commercial_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquarecommercialsell" data-explanation="общая" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_commercial_sell){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_commercial_sell){
												$c = $max_full_square_commercial_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div style="margin-top:15px" class="metric">
							<div class="label_select">Этаж</div>
							<div class="select_block price sec ch_price">
								<?
								$floorChangeCommercialSell = '';
								if($arrayLinkSearch['floorMin'] && !$arrayLinkSearch['floorMax'] && $type_commercial_sell_var){ //только этажи "с"
									$c1 = $arrayLinkSearch['floorMin'];
									$floorChangeCommercialSell = '<li><a class="min" data-name_select="floorsquare" data-name="floorMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if(!$arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_commercial_sell_var){ //только этажи "по"
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeCommercialSell = '<li><a class="max" data-name_select="floorsquare" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMax'].'" href="javascript:void(0)">этаж по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								if($arrayLinkSearch['floorMin'] && $arrayLinkSearch['floorMax'] && $type_commercial_sell_var){ //промежуток этажей
									$c1 = $arrayLinkSearch['floorMin'];
									$c2 = $arrayLinkSearch['floorMax'];
									$floorChangeCommercialSell = '<li><a class="min max" data-name_select="floorsquare" data-name="floorMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floorMin'].'" href="javascript:void(0)">этаж с '.$c1.' по '.$c2.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}

								$nameParams = 'От';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['floorMin']){
									$nameParams = $arrayLinkSearch['floorMin'];
									$currentParams = $arrayLinkSearch['floorMin'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMin'].'"';
								}
								?>
								<input type="hidden" name="floorMin"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="2" placeholder="От" type="text" value="<?=$arrayLinkSearch['floorMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($f=1; $f<=$max_floors_new_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquare" data-explanation="этаж" data-tags="с '.$f.'" data-type="min" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price sec ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['floorMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['floorMax']);
									$currentParams = $arrayLinkSearch['floorMax'];
									$valueParams = ' value="'.$arrayLinkSearch['floorMax'].'"';
								}
								?>
								<input type="hidden" name="floorMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="2" placeholder="До" type="text" value="<?=$arrayLinkSearch['floorMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($f=1; $f<=$max_floors_new_sell; $f++){
											$current = "";
											if($f==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="floorsquare" data-explanation="этаж" data-tags="по '.$f.'" data-type="max" href="javascript:void(0)" data-id="'.$f.'">'.$f.'</a></li>';
										}
										?>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div style="width:250px" class="center_block">
						<div class="metric">
							<div style="margin-left:-55px;margin-top:5px;" class="label_select">Тип здания</div>
							<div class="select_block count">
								<?
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['type_build']){
									$currentParams = (int)$arrayLinkSearch['type_build'];
									$valueParams = ' value="'.$arrayLinkSearch['type_build'].'"';
								}
								$type_build = '';
								$type_buildArray = array();
								for($n=0; $n<count($_TYPE_BUILD_FRESH); $n++){
									if($n==0){
										$current = '';
										if($currentParams=='' && !isset($arrayLinkSearch['type_build'])){
											$current = ' class="current"';
											$nameParams = $_TYPE_BUILD_FRESH[$n];
										}
										$type_build .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_TYPE_BUILD_FRESH[$n].'</a></li>';
									}
									else {
										$current = '';
										if($currentParams==$n){
											$current = ' class="current"';
											$nameParams = $_TYPE_BUILD_FRESH[$n];
										}
										$type_build .= '<li'.$current.'><a data-name="type_build" data-explanation="" data-tags="'.$_TYPE_BUILD_FRESH[$n].'" href="javascript:void(0)" data-id="'.$n.'">'.$_TYPE_BUILD_FRESH[$n].'</a></li>';
										$type_buildArray[$n] = $_TYPE_BUILD_FRESH[$n];	 
									}
								}
								$type_buildChangeCommercialSell = '';
								if($arrayLinkSearch['type_build'] && $type_commercial_sell_var){
									$c1 = $arrayLinkSearch['type_build'];
									$type_buildChangeCommercialSell = '<li><a class="min" data-name_select="type_build" data-name="type_build" data-type="checkbox_block" data-id="'.$arrayLinkSearch['type_build'].'" href="javascript:void(0)">'.$type_buildArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								echo '<input type="hidden" name="type_build"'.$valueParams.'>';
								echo '<div style="width:150px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $type_build;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
						<div class="metric">
							<div style="margin-left:-54px;margin-right:20px;text-align:right;margin-top:5px;" class="label_select">Класс объекта</div>
							<?
							
							/*
							*  Класс объекта
							*/
							$classObject = '';
							$classObjects = '';
							$valueInput = '';
							$class_objectChangeCommercialSell = '';
							for($c=1; $c<=count($_CLASS_OBJECTS); $c++){
								$checked = '';
								$ex_class_object_commercial_sell_var = array();
								$valueInput = '';
								if(isset($arrayLinkSearch['class_object'])){
									$ex_class_object_commercial_sell_var = explode(',',$arrayLinkSearch['class_object']);
									$valueInput = ' value="'.$arrayLinkSearch['class_object'].'"';
									$id_room = 0;
									$name_room = '';
									if(in_array($c,$ex_class_object_commercial_sell_var)){
										$checked = ' class="checked"';
										$valueInput = ' value="'.$c.'"';
										$class_objectChangeCommercialSell .= '<li><a data-name="class_object" data-type="checkbox_select" data-id="0" href="javascript:void(0)">'.$_CLASS_OBJECTS[$c].'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
								}
								if(empty($arrayLinkSearch['class_object'])){
									if($c==0){
										$checked = ' class="checked"';
										$valueInput = ' value="'.$c.'"';				
									}
								}
								$classObjects .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-tags="'.$_CLASS_OBJECTS[$c].'" data-id="'.$c.'">'.$_CLASS_OBJECTS[$c].'</a></li>';
							}
							$classObject = '<div style="margin-left:-10px" class="checkbox_block">';
							$classObject .= '<input type="hidden" name="class_object"'.$valueInput.'>';
							$classObject .= '<div class="block_checkbox">';
							$classObject .= '<ul>';
							$classObject .= $classObjects;
							$classObject .= '</ul>';
							$classObject .= '</div>';
							$classObject .= '</div>';
							
							
							$squareAreaCommercialSell = '';
							if($arrayLinkSearch['squareAreaMin'] && !$arrayLinkSearch['squareAreaMax'] && $type_commercial_sell_var){ //только метраж "от"
								$c1 = $arrayLinkSearch['squareAreaMin'];
								$squareAreaCommercialSell = '<li><a class="min" data-name_select="fullsquareflatsell" data-name="squareAreaMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareAreaMin'].'" href="javascript:void(0)">Площадь участка от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(!$arrayLinkSearch['squareAreaMin'] && $arrayLinkSearch['squareAreaMax'] && $type_commercial_sell_var){ //только метраж "до"
								$c2 = $arrayLinkSearch['squareAreaMax'];
								$squareAreaCommercialSell = '<li><a class="max" data-name_select="fullsquareflatsell" data-name="squareAreaMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareAreaMax'].'" href="javascript:void(0)">Площадь участка до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if($arrayLinkSearch['squareAreaMin'] && $arrayLinkSearch['squareAreaMax'] && $type_commercial_sell_var){ //промежуток метражей
								$c1 = $arrayLinkSearch['squareAreaMin'];
								$c2 = $arrayLinkSearch['squareAreaMax'];
								$squareAreaCommercialSell = '<li><a class="min max" data-name_select="fullsquareflatsell" data-name="squareAreaMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareAreaMin'].'" href="javascript:void(0)">Площадь участка от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}

							$nameParams = 'От';
							$valueParams = '';
							$currentParams = '';
							if($type_commercial_sell_var && $arrayLinkSearch['squareAreaMin']){
								$nameParams = $arrayLinkSearch['squareAreaMin'];
								$currentParams = $arrayLinkSearch['squareAreaMin'];
								$valueParams = ' value="'.$arrayLinkSearch['squareAreaMin'].'"';
							}
							echo $classObject;
							?>
						</div>
					</div>
					<div style="height:66px" class="line"></div>
					<div class="left_block">
						<div class="metric">
							<div style="width:90px" class="label_select">Состояние</div>
							<div class="select_block count multiple">
								<?
								$categoriesChangeCommercialSell = '';
								$ex_array = explode(',',$arrayLinkSearch['categories']);
								if($arrayLinkSearch['categories'] && $type_country_sell_var){
									$c1 = $arrayLinkSearch['categories'];
									$ex_c1 = explode(',',$c1);
									for($e=0; $e<count($ex_c1); $e++){
										$name_value = $_CONDITIONS_SKLAD[$ex_c1[$e]];
										$categoriesChangeCommercialSell .= '<li><a class="min" data-name_select="categories" data-name="categories" data-type="checkbox_block" data-id="'.$ex_c1[$e].'" data-multiple="true" href="javascript:void(0)">'.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
								}
								
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_country_sell_var && $arrayLinkSearch['conditions']){
									$currentParams = (int)$arrayLinkSearch['conditions'];
									$valueParams = ' value="'.$arrayLinkSearch['conditions'].'"';
								}
								$conditions = '';
								for($f=1; $f<count($_CONDITIONS_SKLAD); $f++){
									if($f==1){
										$current = '';
										if($currentParams==0){
											$current = ' class="current"';
											$nameParams = 'Не важно';
										}
										// $conditions .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
									}
									$current = '';
									// if($currentParams==$f){
										// $current = ' class="current"';
										// $nameParams = $_CONDITIONS_SKLAD[$f];
									// }
									if(in_array($f,$ex_array)){
										$current = ' class="current"';
										$nameParams = $_CONDITIONS_SKLAD[$f];
									}
									$name_value = $_CONDITIONS_SKLAD[$f];
									$conditions .= '<li'.$current.'><a data-name="conditions" data-explanation="" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'"><span class="check"></span><span class="label">'.$_CONDITIONS_SKLAD[$f].'</span></a></li>';
								}
								if(count($ex_array)>1){
									$nameParams = count($ex_array).' параметра';
								}
								echo '<input type="hidden" name="conditions"'.$valueParams.'>';
								echo '<div style="width:143px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $conditions;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
					</div>
				</div>
				<div style="display:none" class="row sell comm_show_param type_commer_6">
					<div class="left_block">
						<div class="metric ">
							<div class="label_select">Общая площадь</div>
							<div class="select_block price reg ch_price">
							<?
							$squareChangeCommercialSell = '';
							if($arrayLinkSearch['squareFullMin'] && !$arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //только метраж "от"
								$c1 = $arrayLinkSearch['squareFullMin'];
								$squareChangeCommercialSell = '<li><a class="min" data-name_select="fullsquarecommercialsell" data-name="squareFullMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(!$arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //только метраж "до"
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeCommercialSell = '<li><a class="max" data-name_select="fullsquarecommercialsell" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMax'].'" href="javascript:void(0)">общая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if($arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //промежуток метражей
								$c1 = $arrayLinkSearch['squareFullMin'];
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeCommercialSell = '<li><a class="min max" data-name_select="fullsquarecommercialsell" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}

							$nameParams = 'От';
							$valueParams = '';
							$currentParams = '';
							if($type_commercial_sell_var && $arrayLinkSearch['squareFullMin']){
								$nameParams = $arrayLinkSearch['squareFullMin'];
								$currentParams = $arrayLinkSearch['squareFullMin'];
								$valueParams = ' value="'.$arrayLinkSearch['squareFullMin'].'"';
							}
							?>
							<input type="hidden" name="squareFullMin"<?=$valueParams?>>
							<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareFullMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_full_square_commercial_sell; $c<=$max_full_square_commercial_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquarecommercialsell" data-explanation="общая" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_commercial_sell){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_commercial_sell){
												$c = $max_full_square_commercial_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price reg ch_price">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['squareFullMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareFullMax']);
									$currentParams = $arrayLinkSearch['squareFullMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMax'].'"';
								}
								?>
								<input type="hidden" name="squareFullMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareFullMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_full_square_commercial_sell; $c<=$max_full_square_commercial_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquarecommercialsell" data-explanation="общая" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_commercial_sell){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_commercial_sell){
												$c = $max_full_square_commercial_sell;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						<div class="metric">
							<div style="margin-left:0" class="label_select">Категория</div>
							<div class="select_block count multiple">
								<?
								$categoriesChangeCommercialSell = '';
								$ex_array = explode(',',$arrayLinkSearch['categories']);
								if($arrayLinkSearch['categories'] && $type_country_sell_var){
									$c1 = $arrayLinkSearch['categories'];
									$ex_c1 = explode(',',$c1);
									for($e=0; $e<count($ex_c1); $e++){
										$name_value = $_CATEGORY_COMM_COUNTRY[$ex_c1[$e]];
										$categoriesChangeCommercialSell .= '<li><a class="min" data-name_select="categories" data-name="categories" data-type="checkbox_block" data-id="'.$ex_c1[$e].'" data-multiple="true" href="javascript:void(0)">'.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
								}
								
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_country_sell_var && $arrayLinkSearch['categories']){
									$currentParams = (int)$arrayLinkSearch['categories'];
									$valueParams = ' value="'.$arrayLinkSearch['categories'].'"';
								}
								$categories = '';
								for($f=1; $f<=count($_CATEGORY_COMM_COUNTRY); $f++){
									if($f==1){
										$current = '';
										if($currentParams==0){
											$current = ' class="current"';
											$nameParams = 'Не важно';
										}
										// $categories .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
									}
									$current = '';
									// if($currentParams==$f){
										// $current = ' class="current"';
										// $nameParams = $_CATEGORY_COMM_COUNTRY[$f];
									// }
									if(in_array($f,$ex_array)){
										$current = ' class="current"';
										$nameParams = $_CATEGORY_COMM_COUNTRY[$f];
									}
									$name_value = $_CATEGORY_COMM_COUNTRY[$f];
									$categories .= '<li'.$current.'><a data-name="categories" data-explanation="" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'"><span class="check"></span><span class="label">'.$_CATEGORY_COMM_COUNTRY[$f].'</span></a></li>';
								}
								if(count($ex_array)>1){
									$nameParams = count($ex_array).' параметра';
								}
								echo '<input type="hidden" name="categories"'.$valueParams.'>';
								echo '<div style="width:158px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $categories;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
					</div>
					<div style="width:400px;margin-left:45px;" class="center_block">
						<?
						/*
						*  Право на ЗУ
						*/
						$rights = '';
						$rights2 = '';
						$valueInput = '';
						$rightsChangeCommercialSell = '';
						for($c=1; $c<count($_RIGHTS); $c++){
							$checked = '';
							if(isset($arrayLinkSearch['rights']) && $arrayLinkSearch['rights']==$c){
								$checked = ' class="checked"';
								$valueInput = ' value="'.$c.'"';
								$rightsChangeCommercialSell .= '<li><a class="min" data-name_select="rights" data-name="rights" data-type="checkbox_block" data-id="'.$c.'" data-multiple="true" href="javascript:void(0)">'.$_RIGHTS[$c].'<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							$rights2 .= '<li'.$checked.'><a style="padding:4px 7px" href="javascript:void(0)" data-id="'.$c.'">'.$_RIGHTS[$c].'</a></li>';
						}
						$rights = '<div style="margin-left:0" class="checkbox_block">';
						$rights .= '<input type="hidden" name="rights"'.$valueInput.'>';
						$rights .= '<div class="block_checkbox">';
						$rights .= '<ul>';
						$rights .= $rights2;
						$rights .= '</ul>';
						$rights .= '</div>';
						$rights .= '</div>';
						?>
						<div class="metric">
							<div style="margin-left:-55px" class="label_select">Право на ЗУ</div>
							<?=$rights?>
						</div>
						
						<div class="metric">
							<div style="margin-left:-55px;margin-top:-2px;" class="label_select">Максимальная<br>мощность</div>
							<div class="block">
								<input style="width:85px;height:26px;padding:0 7px;border:1px solid #858b91!important;float:left;" type="text" class="text" name="power_max" value="<?=$arrayLinkSearch['power_max']?>">
								<div style="margin-top:6px" class="com">кВт</div>
							</div>
						</div>
					</div>
					<div style="height:66px" class="line"></div>
					<div class="left_block">
						
					</div>
				</div>
				<div style="display:none" class="row sell comm_show_param type_commer_7">
					<!--
					<div class="left_block">
						<div class="metric">
							<div class="label_select">Общая площадь</div>
							<div class="select_block price reg ch_price">
							<?
/* 							$squareChangeCommercialSell = '';
							if($arrayLinkSearch['squareFullMin'] && !$arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //только метраж "от"
								$c1 = $arrayLinkSearch['squareFullMin'];
								$squareChangeCommercialSell = '<li><a class="min" data-name_select="fullsquarecommercialsell" data-name="squareFullMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(!$arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //только метраж "до"
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeCommercialSell = '<li><a class="max" data-name_select="fullsquarecommercialsell" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMax'].'" href="javascript:void(0)">общая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if($arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_commercial_sell_var){ //промежуток метражей
								$c1 = $arrayLinkSearch['squareFullMin'];
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeCommercialSell = '<li><a class="min max" data-name_select="fullsquarecommercialsell" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}

							$nameParams = 'От';
							$valueParams = '';
							$currentParams = '';
							if($type_commercial_sell_var && $arrayLinkSearch['squareFullMin']){
								$nameParams = $arrayLinkSearch['squareFullMin'];
								$currentParams = $arrayLinkSearch['squareFullMin'];
								$valueParams = ' value="'.$arrayLinkSearch['squareFullMin'].'"';
							}
 */							?>
							<input type="hidden" name="squareFullMin"<?=$valueParams?>>
							<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="От" type="text" value="<?=$arrayLinkSearch['squareFullMin']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
/* 										for($c=$min_full_square_commercial_sell; $c<=$max_full_square_commercial_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquarecommercialsell" data-explanation="общая" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_commercial_sell){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_commercial_sell){
												$c = $max_full_square_commercial_sell;
											}
										}
 */										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block price reg ch_price">
								<?
/* 								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_sell_var && $arrayLinkSearch['squareFullMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareFullMax']);
									$currentParams = $arrayLinkSearch['squareFullMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMax'].'"';
								}
 */								?>
								<input type="hidden" name="squareFullMax"<?=$valueParams?>>
								<div style="width:70px!important" class="choosed_block"><input style="display:none" maxlength="17" placeholder="До" type="text" value="<?=$arrayLinkSearch['squareFullMax']?>"><span class="namePrice"><?=$nameParams?></span></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
/* 										for($c=$min_full_square_commercial_sell; $c<=$max_full_square_commercial_sell;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquarecommercialsell" data-explanation="общая" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_commercial_sell){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_commercial_sell){
												$c = $max_full_square_commercial_sell;
											}
										}
 */										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
					</div>
					<div style="width:250px" class="center_block">&nbsp;</div>
					<div style="height:66px" class="line"></div>
					<div class="left_block">
						<div class="metric">
							<div style="width:115px" class="label_select">Коммуникации</div>
							<div class="select_block count multiple">
								<?
/* 								$communicationsChangeCountrySell = '';
								$ex_array = explode(',',$arrayLinkSearch['communications']);
								if($arrayLinkSearch['communications'] && $type_country_sell_var){
									$c1 = $arrayLinkSearch['communications'];
									$ex_c1 = explode(',',$c1);
									for($e=0; $e<count($ex_c1); $e++){
										$name_value = $_COMM_COUNTRY[$ex_c1[$e]];
										$communicationsChangeCountrySell .= '<li><a class="min" data-name_select="communications" data-name="communications" data-type="checkbox_block" data-id="'.$ex_c1[$e].'" data-multiple="true" href="javascript:void(0)">'.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
									}
								}
								
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_country_sell_var && $arrayLinkSearch['communications']){
									$currentParams = (int)$arrayLinkSearch['communications'];
									$valueParams = ' value="'.$arrayLinkSearch['communications'].'"';
								}
								$communications = '';
								for($f=1; $f<count($_COMM_COUNTRY); $f++){
									if($f==1){
										$current = '';
										if($currentParams==0){
											$current = ' class="current"';
											$nameParams = 'Не важно';
										}
										// $communications .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
									}
									$current = '';
									// if($currentParams==$f){
										// $current = ' class="current"';
										// $nameParams = $_COMM_COUNTRY[$f];
									// }
									if(in_array($f,$ex_array)){
										$current = ' class="current"';
										$nameParams = $_COMM_COUNTRY[$f];
									}
									$name_value = $_COMM_COUNTRY[$f];
									$communications .= '<li'.$current.'><a data-name="communications" data-explanation="" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'"><span class="check"></span><span class="label">'.$_COMM_COUNTRY[$f].'</span></a></li>';
								}
								if(count($ex_array)>1){
									$nameParams = count($ex_array).' параметра';
								}
								echo '<input type="hidden" name="communications"'.$valueParams.'>';
								echo '<div style="width:143px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $communications;
								echo '</ul>';
								echo '</div>';
 */								?>
							</div>
						</div>
					</div>
					<div style="margin-left:20px" class="right_block">
						<div style="margin-top:6px" class="check_block">
							<?
/* 							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($entrance_commercial_sell_var)){
								if($entrance_commercial_sell_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$entrance_commercial_sell_var.'"';
								}
							}
 */							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="entrance"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Отдельный вход</span>
								</div>
							</div>
						</div>
					</div>-->
				</div>
				<div style="margin-top:0" class="row rent">
					<div class="left_block">
						<div class="metric">
							<div class="label_select">Общая площадь</div>
							<div class="select_block count">
							<?
							$squareChangeCommercialRent = '';
							if($arrayLinkSearch['squareFullMin'] && !$arrayLinkSearch['squareFullMax'] && $type_commercial_rent_var){ //только метраж "от"
								$c1 = $arrayLinkSearch['squareFullMin'];
								$squareChangeCommercialRent = '<li><a class="min" data-name_select="fullsquarecommercialrent" data-name="squareFullMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(!$arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_commercial_rent_var){ //только метраж "до"
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeCommercialRent = '<li><a class="max" data-name_select="fullsquarecommercialrent" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMax'].'" href="javascript:void(0)">общая до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if($arrayLinkSearch['squareFullMin'] && $arrayLinkSearch['squareFullMax'] && $type_commercial_rent_var){ //промежуток метражей
								$c1 = $arrayLinkSearch['squareFullMin'];
								$c2 = $arrayLinkSearch['squareFullMax'];
								$squareChangeCommercialRent = '<li><a class="min max" data-name_select="fullsquarecommercialrent" data-name="squareFullMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareFullMin'].'" href="javascript:void(0)">общая от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}

							$nameParams = 'От';
							$valueParams = '';
							$currentParams = '';
							if($type_commercial_rent_var && $arrayLinkSearch['squareFullMin']){
								$nameParams = $arrayLinkSearch['squareFullMin'];
								$currentParams = $arrayLinkSearch['squareFullMin'];
								$valueParams = ' value="'.$arrayLinkSearch['squareFullMin'].'"';
							}
							?>
							<input type="hidden" name="squareFullMin"<?=$valueParams?>>
							<div style="width:95px" class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_full_square_commercial_rent; $c<=$max_full_square_commercial_rent;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquarecommercialrent" data-explanation="общая" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_commercial_rent){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_commercial_rent){
												$c = $max_full_square_commercial_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_rent_var && $arrayLinkSearch['squareFullMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareFullMax']);
									$currentParams = $arrayLinkSearch['squareFullMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareFullMax'].'"';
								}
								?>
								<input type="hidden" name="squareFullMax"<?=$valueParams?>>
								<div style="width:95px" class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_full_square_commercial_rent; $c<=$max_full_square_commercial_rent;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquarecommercialrent" data-explanation="общая" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_full_square_commercial_rent){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_full_square_commercial_rent){
												$c = $max_full_square_commercial_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						
						<div class="metric">
							<div class="label_select">Площадь зала</div>
							<div class="select_block count">
							<?
							$squarePlaceCommercialRent = '';
							if($arrayLinkSearch['squarePlaceMin'] && !$arrayLinkSearch['squarePlaceMax'] && $type_commercial_rent_var){ //только метраж "от"
								$c1 = $arrayLinkSearch['squarePlaceMin'];
								$squarePlaceCommercialRent = '<li><a class="min" data-name_select="fullsquareflatrent" data-name="squarePlaceMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squarePlaceMin'].'" href="javascript:void(0)">Площадь зала от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(!$arrayLinkSearch['squarePlaceMin'] && $arrayLinkSearch['squarePlaceMax'] && $type_commercial_rent_var){ //только метраж "до"
								$c2 = $arrayLinkSearch['squarePlaceMax'];
								$squarePlaceCommercialRent = '<li><a class="max" data-name_select="fullsquareflatrent" data-name="squarePlaceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squarePlaceMax'].'" href="javascript:void(0)">Площадь зала до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if($arrayLinkSearch['squarePlaceMin'] && $arrayLinkSearch['squarePlaceMax'] && $type_commercial_rent_var){ //промежуток метражей
								$c1 = $arrayLinkSearch['squarePlaceMin'];
								$c2 = $arrayLinkSearch['squarePlaceMax'];
								$squarePlaceCommercialRent = '<li><a class="min max" data-name_select="fullsquareflatrent" data-name="squarePlaceMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squarePlaceMin'].'" href="javascript:void(0)">Площадь зала от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}

							$nameParams = 'От';
							$valueParams = '';
							$currentParams = '';
							if($type_commercial_rent_var && $arrayLinkSearch['squarePlaceMin']){
								$nameParams = $arrayLinkSearch['squarePlaceMin'];
								$currentParams = $arrayLinkSearch['squarePlaceMin'];
								$valueParams = ' value="'.$arrayLinkSearch['squarePlaceMin'].'"';
							}
							?>
							<input type="hidden" name="squarePlaceMin"<?=$valueParams?>>
							<div style="width:95px" class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_place_square_commercial_rent; $c<=$max_place_square_commercial_rent;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquareflatsell" data-explanation="Площадь зала" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_place_square_commercial_rent){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_place_square_commercial_rent){
												$c = $max_place_square_commercial_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_rent_var && $arrayLinkSearch['squarePlaceMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squarePlaceMax']);
									$currentParams = $arrayLinkSearch['squarePlaceMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squarePlaceMax'].'"';
								}
								?>
								<input type="hidden" name="squarePlaceMax"<?=$valueParams?>>
								<div style="width:95px" class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_place_square_commercial_rent; $c<=$max_place_square_commercial_rent;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquareflatrent" data-explanation="Площадь зала" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_place_square_commercial_rent){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_place_square_commercial_rent){
												$c = $max_place_square_commercial_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
						
						<div class="metric">
							<div class="label_select">Площадь участка</div>
							<div class="select_block count">
							<?
							$squareAreaCommercialRent = '';
							if($arrayLinkSearch['squareAreaMin'] && !$arrayLinkSearch['squareAreaMax'] && $type_commercial_rent_var){ //только метраж "от"
								$c1 = $arrayLinkSearch['squareAreaMin'];
								$squareAreaCommercialRent = '<li><a class="min" data-name_select="fullsquareflatsell" data-name="squareAreaMin" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareAreaMin'].'" href="javascript:void(0)">Площадь участка от '.str_replace(".", ",", $c1).' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if(!$arrayLinkSearch['squareAreaMin'] && $arrayLinkSearch['squareAreaMax'] && $type_commercial_rent_var){ //только метраж "до"
								$c2 = $arrayLinkSearch['squareAreaMax'];
								$squareAreaCommercialRent = '<li><a class="max" data-name_select="fullsquareflatsell" data-name="squareAreaMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareAreaMax'].'" href="javascript:void(0)">Площадь участка до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}
							if($arrayLinkSearch['squareAreaMin'] && $arrayLinkSearch['squareAreaMax'] && $type_commercial_rent_var){ //промежуток метражей
								$c1 = $arrayLinkSearch['squareAreaMin'];
								$c2 = $arrayLinkSearch['squareAreaMax'];
								$squareAreaCommercialRent = '<li><a class="min max" data-name_select="fullsquareflatsell" data-name="squareAreaMax" data-type="checkbox_block" data-id="'.$arrayLinkSearch['squareAreaMin'].'" href="javascript:void(0)">Площадь участка от '.$c1.' кв.м до '.$c2.' кв.м<span onclick="return delTags(this)" class="close"></span></a></li>';
							}

							$nameParams = 'От';
							$valueParams = '';
							$currentParams = '';
							if($type_commercial_rent_var && $arrayLinkSearch['squareAreaMin']){
								$nameParams = $arrayLinkSearch['squareAreaMin'];
								$currentParams = $arrayLinkSearch['squareAreaMin'];
								$valueParams = ' value="'.$arrayLinkSearch['squareAreaMin'].'"';
							}
							?>
							<input type="hidden" name="squareAreaMin"<?=$valueParams?>>
							<div style="width:95px" class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">От</a></li>
										<?
										for($c=$min_area_square_commercial_rent; $c<=$max_area_square_commercial_rent;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquareflatsell" data-explanation="Плозадь участка" data-tags="от '.$c.' кв.м" data-type="min" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_area_square_commercial_rent){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_area_square_commercial_rent){
												$c = $max_area_square_commercial_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="mdash">&ndash;</div>
							<div class="select_block count">
								<?
								$nameParams = 'До';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_rent_var && $arrayLinkSearch['squareAreaMax']){
									$nameParams = str_replace(".", ",", $arrayLinkSearch['squareAreaMax']);
									$currentParams = $arrayLinkSearch['squareAreaMax'];
									$valueParams = ' value="'.$arrayLinkSearch['squareAreaMax'].'"';
								}
								?>
								<input type="hidden" name="squareAreaMax"<?=$valueParams?>>
								<div style="width:95px" class="choosed_block"><?=$nameParams?></div>
								<div class="scrollbar-inner">
									<ul>
										<li <?empty($currentParams)?print 'class="current"':''?>><a href="javascript:void(0)" data-id="">До</a></li>
										<?
										for($c=$min_area_square_commercial_rent; $c<=$max_area_square_commercial_rent;){
											$current = "";
											if($c==$currentParams){
												$current = ' class="current"';
											}
											echo '<li'.$current.'><a data-name="fullsquareflatsell" data-explanation="Площадь участка" data-tags="до '.$c.' кв.м" data-type="max" href="javascript:void(0)" data-id="'.$c.'">'.$c.'</a></li>';
											if($c==$max_area_square_commercial_rent){
												break;
											}
											if($c<=40){
												$c = $c+1;
											}
											if($c>40 && $c<=100){
												$c = $c+2;
											}
											if($c>100){
												$c = $c+5;
											}
											if($c>$max_area_square_commercial_rent){
												$c = $max_area_square_commercial_rent;
											}
										}
										?>
									</ul>
								</div>
							</div>
							<div class="com">м<sup>2</sup></div>
						</div>
					</div>
					<div style="width:375px" class="center_block">
						<div class="metric">
							<div style="width:220px;margin-left:-30px;margin-top:5px;" class="label_select">Высота потолков</div>
							<div class="select_block count">
								<?
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_rent_var && $arrayLinkSearch['height_ceiling']){
									$currentParams = (int)$arrayLinkSearch['height_ceiling'];
									$valueParams = ' value="'.$arrayLinkSearch['height_ceiling'].'"';
								}
								$height_ceiling = '';
								$height_ceilingArray = array();
								for($n=0; $n<count($_HEIGHT_CEILING); $n++){
									if($n==0){
										$current = '';
										if($currentParams=='' && !isset($arrayLinkSearch['height_ceiling'])){
											$current = ' class="current"';
											$nameParams = $_HEIGHT_CEILING[$n];
										}
										$height_ceiling .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_HEIGHT_CEILING[$n].'</a></li>';
									}
									else {
										$current = '';
										if($currentParams==$n){
											$current = ' class="current"';
											$nameParams = $_HEIGHT_CEILING[$n];
										}
										$height_ceiling .= '<li'.$current.'><a data-name="height_ceiling" data-explanation="Высота потолков" data-tags="'.$_HEIGHT_CEILING[$n].'" href="javascript:void(0)" data-id="'.$n.'">'.$_HEIGHT_CEILING[$n].'</a></li>';
										$height_ceilingArray[$n] = $_HEIGHT_CEILING[$n];	
									}
								}
								$height_ceilingChangeCommercialRent = '';
								if($arrayLinkSearch['height_ceiling'] && $type_commercial_rent_var){
									$c1 = $arrayLinkSearch['height_ceiling'];
									$height_ceilingChangeCommercialRent = '<li><a class="min" data-name_select="height_ceiling" data-name="height_ceiling" data-type="checkbox_block" data-id="'.$arrayLinkSearch['height_ceiling'].'" href="javascript:void(0)">Высота потолков '.$height_ceilingArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								echo '<input type="hidden" name="height_ceiling"'.$valueParams.'>';
								echo '<div style="width:150px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $height_ceiling;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
						<div class="metric">
							<div style="width:220px;margin-left:-30px;margin-top:5px;" class="label_select">Этаж</div>
							<div class="select_block count">
								<?
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_rent_var && $arrayLinkSearch['floor']){
									$currentParams = (int)$arrayLinkSearch['floor'];
									$valueParams = ' value="'.$arrayLinkSearch['floor'].'"';
								}
								$floor = '';
								$floorArray = array();
								for($n=0; $n<count($_FLOOR_COMMERCIAL); $n++){
									if($n==0){
										$current = '';
										if($currentParams=='' && !isset($arrayLinkSearch['floor'])){
											$current = ' class="current"';
											$nameParams = $_FLOOR_COMMERCIAL[$n];
										}
										$floor .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_FLOOR_COMMERCIAL[$n].'</a></li>';
									}
									else {
										$current = '';
										if($currentParams==$n){
											$current = ' class="current"';
											$nameParams = $_FLOOR_COMMERCIAL[$n];
										}
										$floor .= '<li'.$current.'><a data-name="floor" data-explanation="" data-tags="'.$_FLOOR_COMMERCIAL[$n].'" href="javascript:void(0)" data-id="'.$n.'">'.$_FLOOR_COMMERCIAL[$n].'</a></li>';
										$floorArray[$n] = $_FLOOR_COMMERCIAL[$n];	
									}
								}
								$floorChangeCommercialRent = '';
								if($arrayLinkSearch['floor'] && $type_commercial_rent_var){
									$c1 = $arrayLinkSearch['floor'];
									$floorChangeCommercialRent = '<li><a class="min" data-name_select="floor" data-name="floor" data-type="checkbox_block" data-id="'.$arrayLinkSearch['floor'].'" href="javascript:void(0)">'.$floorArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								echo '<input type="hidden" name="floor"'.$valueParams.'>';
								echo '<div style="width:150px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $floor;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
						<div class="metric">
							<div style="width:220px;margin-left:-30px;margin-top:5px;" class="label_select">Дата публикации</div>
							<div class="select_block count">
								<?
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_rent_var && $arrayLinkSearch['dates']){
									$currentParams = (int)$arrayLinkSearch['dates'];
									$valueParams = ' value="'.$arrayLinkSearch['dates'].'"';
								}
								$dates = '';
								$datesArray = array();
								for($n=0; $n<count($_DATE_POST); $n++){
									if($n==0){
										$current = '';
										if($currentParams=='' && !isset($arrayLinkSearch['dates'])){
											$current = ' class="current"';
											$nameParams = $_DATE_POST[0]['name'];
										}
										$dates .= '<li'.$current.'><a href="javascript:void(0)" data-id="">'.$_DATE_POST[0]['name'].'</a></li>';
									}
									else {
										$current = '';
										if($currentParams==$_DATE_POST[$n]['id']){
											$current = ' class="current"';
											$nameParams = $_DATE_POST[$n]['name'];
										}
										$dates .= '<li'.$current.'><a data-name="dates" data-explanation="Публикации" data-tags="'.$_DATE_POST[$n]['name'].'" href="javascript:void(0)" data-id="'.$_DATE_POST[$n]['id'].'">'.$_DATE_POST[$n]['name'].'</a></li>';
										$datesArray[$_DATE_POST[$n]['id']] = $_DATE_POST[$n]['name'];	
									}
								}
								$datesChangeCommercialRent = '';
								if($arrayLinkSearch['dates'] && $type_commercial_rent_var){
									$c1 = $arrayLinkSearch['dates'];
									$datesChangeCommercialRent = '<li><a class="min" data-name_select="dates" data-name="dates" data-type="checkbox_block" data-id="'.$arrayLinkSearch['dates'].'" href="javascript:void(0)">Публикации '.$datesArray[$c1].'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								echo '<input type="hidden" name="dates"'.$valueParams.'>';
								echo '<div style="width:150px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $dates;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
					</div>
					<div class="line"></div>
					<div class="right_block">
						<div class="metric">
							<div style="width:67px;margin-left:-14px" class="label_select">До метро</div>
							<div class="select_block count">
								<?
								$far_subwayChangeCommercialRent = '';
								if($arrayLinkSearch['far_subway'] && $type_commercial_rent_var){
									$c1 = $arrayLinkSearch['far_subway'];
									$name_value = $_FAR_SUBWAY[$c1];
									if($c1==1){
										$name_value = '< 500 м';
									}
									$far_subwayChangeCommercialRent = '<li><a class="min" data-name_select="far_subway" data-name="far_subway" data-type="checkbox_block" data-id="'.$arrayLinkSearch['far_subway'].'" href="javascript:void(0)">Метро '.$name_value.'<span onclick="return delTags(this)" class="close"></span></a></li>';
								}
								
								$nameParams = 'Не важно';
								$valueParams = '';
								$currentParams = '';
								if($type_commercial_rent_var && $arrayLinkSearch['far_subway']){
									$currentParams = (int)$arrayLinkSearch['far_subway'];
									$valueParams = ' value="'.$arrayLinkSearch['far_subway'].'"';
								}
								$far_subway = '';
								for($f=1; $f<=count($_FAR_SUBWAY); $f++){
									if($f==1){
										$current = '';
										if($currentParams==0){
											$current = ' class="current"';
											$nameParams = 'Не важно';
										}
										$far_subway .= '<li'.$current.'><a href="javascript:void(0)" data-id="">Не важно</a></li>';
									}
									$current = '';
									if($currentParams==$f){
										$current = ' class="current"';
										$nameParams = $_FAR_SUBWAY[$f];
									}
									$name_value = $_FAR_SUBWAY[$f];
									if($f==1){
										$name_value = '< 500 м';
									}
									$far_subway .= '<li'.$current.'><a data-name="far_subway" data-explanation="Метро" data-tags="'.$name_value.'" href="javascript:void(0)" data-id="'.$f.'">'.$_FAR_SUBWAY[$f].'</a></li>';
								}
								echo '<input type="hidden" name="far_subway"'.$valueParams.'>';
								echo '<div style="width:143px" class="choosed_block">'.$nameParams.'</div>';
								echo '<div class="scrollbar-inner">';
								echo '<ul>';
								echo $far_subway;
								echo '</ul>';
								echo '</div>';
								?>
							</div>
						</div>
						<div class="clear"></div>
						<div style="margin-top:18px" class="check_block">
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($ppa_commercial_rent_var)){
								if($ppa_commercial_rent_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$ppa_commercial_rent_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="ppa"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">ППА</span>
								</div>
							</div>
							<?
							$checked = '';
							$valueParams = '';
							$disabled = ' disabled="disabled"';
							if(isset($entrance_commercial_rent_var)){
								if($entrance_commercial_rent_var==1){
									$disabled = '';
									$checked = ' checked';
									$valueParams = ' value="'.$entrance_commercial_rent_var.'"';
								}
							}
							?>
							<div class="checkbox_single<?=$checked?>">
								<input type="hidden"<?=$valueParams?> name="entrance"<?=$disabled?>>
								<div class="container">
									<span class="check"></span><span class="label">Отдельный вход</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div <?!$type_new_sell_var || !$estate_new_sell_var?print'style="display:none"':''?> class="selected_params new sell"><ul><?=$priceChangeNewSell?><?=$roomsChangeNewSell?><?=$deadlineChangeNewSell?><?=$areaChange?><?=$stationChange?><?=$buildChange?><?=$developerChange?><?=$squareChangeNewSell?><?=$squareLiveChangeNewSell?><?=$squareKitchenChangeNewSell?><?=$floorChangeNewSell?><?=$floorsChangeNewSell?><?=$deliveryChangeNewSell?><?=$finishingChangeNewSell?><?=$readyChangeNewSell?><?=$far_subwayChangeNewSell?><?=$type_houseChangeNewSell?><?=$classChangeNewSell?><?=$wcChangeNewSell?><?=$cessionChangeNewSell?><?=$datesChangeNewSell?></ul><div id="btn_block"><div class="btns-show"><div class="show"><div class="item list"><a data-pos="bottom" data-hover="tooltip" data-title="Список" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div><div class="item table<?if(isset($arrayLinkSearch['view']) && $arrayLinkSearch['view']=='table'){echo ' current';}?>"><a data-pos="bottom" data-hover="tooltip" data-title="Таблица" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div><div class="item map<?if(isset($arrayLinkSearch['view']) && $arrayLinkSearch['view']=='map'){echo ' current';}?>"><a data-pos="bottom" data-hover="tooltip" data-title="Карта" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div></div></div></div></div>
		<div <?$type_cession_sell_var?print'style="display:block"':'style="display:none"'?> class="selected_params cession sell"><ul><?=$priceChangeCessionSell?><?=$roomsChangeCessionSell?><?=$areaChange?><?=$stationChange?><?=$buildChange?><?=$developerChange?><?=$squareChangeCessionSell?><?=$squareLiveChangeCessionSell?><?=$squareKitchenChangeCessionSell?><?=$floorChangeCessionSell?><?=$floorsChangeCessionSell?><?=$deliveryChangeCessionSell?><?=$finishingChangeCessionSell?><?=$readyChangeCessionSell?><?=$far_subwayChangeCessionSell?><?=$type_houseChangeCessionSell?><?=$datesChangeCessionSell?></ul><div id="btn_block"><div class="btns-show"><div class="show"><div class="item list"><a data-pos="bottom" data-hover="tooltip" data-title="Список" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div><div class="item table<?if(isset($arrayLinkSearch['view']) && $arrayLinkSearch['view']=='table'){echo ' current';}?>"><a data-pos="bottom" data-hover="tooltip" data-title="Таблица" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div><div class="item map<?if(isset($arrayLinkSearch['view']) && $arrayLinkSearch['view']=='map'){echo ' current';}?>"><a data-pos="bottom" data-hover="tooltip" data-title="Карта" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div></div></div></div></div>
		<div <?$type_flat_sell_var?print'style="display:block"':'style="display:none"'?> class="selected_params flat sell"><ul><?=$priceChangeFlatSell?><?=$roomsChangeFlatSell?><?=$areaChange?><?=$stationChange?><?=$squareChangeFlatSell?><?=$squareLiveChangeFlatSell?><?=$squareKitchenChangeFlatSell?><?=$floorChangeFlatSell?><?=$floorsChangeFlatSell?><?=$far_subwayChangeFlatSell?><?=$repairsChangeFlatSell?><?=$wcChangeFlatSell?><?=$type_houseChangeFlatSell?><?=$datesChangeFlatSell?><?=$transactionChangeFlatSell?><?=$shareChangeFlatSell?></ul><div id="btn_block"><div class="btns-show"><div class="show"><div class="item list"><a data-pos="bottom" data-hover="tooltip" data-title="Список" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div><div class="item table<?if(isset($arrayLinkSearch['view']) && $arrayLinkSearch['view']=='table'){echo ' current';}?>"><a data-pos="bottom" data-hover="tooltip" data-title="Таблица" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div><div class="item map<?if(isset($arrayLinkSearch['view']) && $arrayLinkSearch['view']=='map'){echo ' current';}?>"><a data-pos="bottom" data-hover="tooltip" data-title="Карта" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div></div></div></div></div>
		<div <?$type_flat_rent_var?print'style="display:block"':'style="display:none"'?> class="selected_params flat rent"><ul><?=$priceChangeFlatRent?><?=$roomsChangeFlatRent?><?=$areaChange?><?=$stationChange?><?=$squareChangeFlatRent?><?=$squareLiveChangeFlatRent?><?=$squareKitchenChangeFlatRent?><?=$floorChangeFlatRent?><?=$floorsChangeFlatRent?><?=$far_subwayChangeFlatRent?><?=$repairsChangeFlatRent?><?=$wcChangeFlatRent?><?=$type_houseChangeFlatRent?><?=$datesChangeFlatRent?><?=$periodChangeFlatRent?><?=$prepaymentChangeFlatRent?></ul><div id="btn_block"><div class="btns-show"><div class="show"><div class="item list"><a data-pos="bottom" data-hover="tooltip" data-title="Список" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div><div class="item table<?if(isset($arrayLinkSearch['view']) && $arrayLinkSearch['view']=='table'){echo ' current';}?>"><a data-pos="bottom" data-hover="tooltip" data-title="Таблица" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div><div class="item map<?if(isset($arrayLinkSearch['view']) && $arrayLinkSearch['view']=='map'){echo ' current';}?>"><a data-pos="bottom" data-hover="tooltip" data-title="Карта" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div></div></div></div></div>
		<div <?$type_room_sell_var?print'style="display:block"':'style="display:none"'?> class="selected_params room sell"><ul><?=$priceChangeRoomSell?><?=$roomsChangeRoomSell?><?=$squareFullChangeRoomSell?><?=$areaChange?><?=$stationChange?><?=$squareChangeRoomSell?><?=$squareLiveChangeRoomSell?><?=$squareKitchenChangeRoomSell?><?=$floorChangeRoomSell?><?=$floorsChangeRoomSell?><?=$far_subwayChangeRoomSell?><?=$repairsChangeRoomSell?><?=$wcChangeRoomSell?><?=$type_houseChangeRoomSell?><?=$datesChangeRoomSell?><?=$transactionChangeRoomSell?><?=$shareChangeRoomSell?></ul><div id="btn_block"><div class="btns-show"><div class="show"><div class="item list"><a data-pos="bottom" data-hover="tooltip" data-title="Список" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div><div class="item table<?if(isset($arrayLinkSearch['view']) && $arrayLinkSearch['view']=='table'){echo ' current';}?>"><a data-pos="bottom" data-hover="tooltip" data-title="Таблица" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div><div class="item map<?if(isset($arrayLinkSearch['view']) && $arrayLinkSearch['view']=='map'){echo ' current';}?>"><a data-pos="bottom" data-hover="tooltip" data-title="Карта" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div></div></div></div></div>
		<div <?$type_room_rent_var?print'style="display:block"':'style="display:none"'?> class="selected_params room rent"><ul><?=$priceChangeRoomRent?><?=$roomsChangeRoomRent?><?=$squareFullChangeRoomRent?><?=$areaChange?><?=$stationChange?><?=$squareChangeRoomRent?><?=$squareLiveChangeRoomRent?><?=$squareKitchenChangeRoomRent?><?=$floorChangeRoomRent?><?=$floorsChangeRoomRent?><?=$far_subwayChangeRoomRent?><?=$repairsChangeRoomRent?><?=$wcChangeRoomRent?><?=$type_houseChangeRoomRent?><?=$datesChangeRoomRent?><?=$periodChangeRoomRent?><?=$prepaymentChangeRoomRent?></ul><div id="btn_block"><div class="btns-show"><div class="show"><div class="item list"><a data-pos="bottom" data-hover="tooltip" data-title="Список" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div><div class="item table<?if(isset($arrayLinkSearch['view']) && $arrayLinkSearch['view']=='table'){echo ' current';}?>"><a data-pos="bottom" data-hover="tooltip" data-title="Таблица" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div><div class="item map<?if(isset($arrayLinkSearch['view']) && $arrayLinkSearch['view']=='map'){echo ' current';}?>"><a data-pos="bottom" data-hover="tooltip" data-title="Карта" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div></div></div></div></div>
		<div <?$type_country_sell_var?print'style="display:block"':'style="display:none"'?> class="selected_params country sell"><ul><?=$priceChangeCountrySell?><?=$typeCountryChangeCountrySell?><?=$areaChange?><?=$stationChange?><?=$railwayChange?><?=$squareChangeCountrySell?><?=$squareLandChangeCountrySell?><?=$squareLiveChangeCountrySell?><?=$squareKitchenChangeCountrySell?><?=$floorChangeCountrySell?><?=$floorsChangeCountrySell?><?=$far_subwayChangeCountrySell?><?=$repairsChangeCountrySell?><?=$wcChangeCountrySell?><?=$categoriesChangeCountrySell?><?=$communicationsChangeCountrySell?><?=$type_houseChangeCountrySell?><?=$datesChangeCountrySell?><?=$transactionChangeCountrySell?><?=$legal_statusChangeCountrySell?><?=$plumbingChangeCountrySell?><?=$sewerageChangeCountrySell?></ul><div id="btn_block"><div class="btns-show"><div class="show"><div class="item list"><a data-pos="bottom" data-hover="tooltip" data-title="Список" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div><div class="item table<?if(isset($arrayLinkSearch['view']) && $arrayLinkSearch['view']=='table'){echo ' current';}?>"><a data-pos="bottom" data-hover="tooltip" data-title="Таблица" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div><div class="item map<?if(isset($arrayLinkSearch['view']) && $arrayLinkSearch['view']=='map'){echo ' current';}?>"><a data-pos="bottom" data-hover="tooltip" data-title="Карта" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div></div></div></div></div>
		<div <?$type_country_rent_var?print'style="display:block"':'style="display:none"'?> class="selected_params country rent"><ul><?=$priceChangeCountryRent?><?=$typeCountryChangeCountryRent?><?=$areaChange?><?=$stationChange?><?=$railwayChange?><?=$squareChangeCountryRent?><?=$squareLandChangeCountryRent?><?=$squareLiveChangeCountryRent?><?=$squareKitchenChangeCountryRent?><?=$floorChangeCountryRent?><?=$floorsChangeCountryRent?><?=$far_subwayChangeCountryRent?><?=$repairsChangeCountryRent?><?=$wcChangeCountryRent?><?=$periodChangeCountryRent?><?=$categoriesChangeCountryRent?><?=$communicationsChangeCountryRent?><?=$type_houseChangeCountryRent?><?=$datesChangeCountryRent?><?=$transactionChangeCountryRent?><?=$legal_statusChangeCountryRent?><?=$plumbingChangeCountryRent?><?=$sewerageChangeCountryRent?></ul><div id="btn_block"><div class="btns-show"><div class="show"><div class="item list"><a data-pos="bottom" data-hover="tooltip" data-title="Список" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div><div class="item table<?if(isset($arrayLinkSearch['view']) && $arrayLinkSearch['view']=='table'){echo ' current';}?>"><a data-pos="bottom" data-hover="tooltip" data-title="Таблица" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div><div class="item map<?if(isset($arrayLinkSearch['view']) && $arrayLinkSearch['view']=='map'){echo ' current';}?>"><a data-pos="bottom" data-hover="tooltip" data-title="Карта" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div></div></div></div></div>
		<div <?$type_commercial_sell_var?print'style="display:block"':'style="display:none"'?> class="selected_params commercial sell"><ul><?=$priceChangeCommercialSell?><?=$typeCommerChangeCommercialSell?><?=$areaChange?><?=$stationChange?><?=$squareChangeCommercialSell?><?=$squarePlaceCommercialSell?><?=$squareAreaCommercialSell?><?=$height_ceilingChangeCommercialSell?><?=$squareLiveChangeCommercialSell?><?=$squareKitchenChangeCommercialSell?><?=$floorChangeCommercialSell?><?=$floorsChangeCommercialSell?><?=$far_subwayChangeCommercialSell?><?=$repairsChangeCommercialSell?><?=$wcChangeCommercialSell?><?=$type_houseChangeCommercialSell?><?=$type_buildChangeCommercialSell?><?=$class_objectChangeCommercialSell?><?=$categoriesChangeCommercialSell?><?=$datesChangeCommercialSell?><?=$transactionChangeCommercialSell?></ul><div id="btn_block"><div class="btns-show"><div class="show"><div class="item list"><a data-pos="bottom" data-hover="tooltip" data-title="Список" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div><div class="item table<?if(isset($arrayLinkSearch['view']) && $arrayLinkSearch['view']=='table'){echo ' current';}?>"><a data-pos="bottom" data-hover="tooltip" data-title="Таблица" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div><div class="item map<?if(isset($arrayLinkSearch['view']) && $arrayLinkSearch['view']=='map'){echo ' current';}?>"><a data-pos="bottom" data-hover="tooltip" data-title="Карта" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div></div></div></div></div>
		<div <?$type_commercial_rent_var?print'style="display:block"':'style="display:none"'?> class="selected_params commercial rent"><ul><?=$priceChangeCommercialRent?><?=$typeCommerChangeCommercialRent?><?=$areaChange?><?=$stationChange?><?=$squareChangeCommercialRent?><?=$squarePlaceCommercialRent?><?=$squareAreaCommercialRent?><?=$height_ceilingChangeCommercialRent?><?=$squareLiveChangeCommercialRent?><?=$squareKitchenChangeCommercialRent?><?=$periodChangeCommercialRent?><?=$type_buildChangeCommercialRent?><?=$class_objectChangeCommercialRent?><?=$floorChangeCommercialRent?><?=$floorsChangeCommercialRent?><?=$far_subwayChangeCommercialRent?><?=$repairsChangeCommercialRent?><?=$wcChangeCommercialRent?><?=$type_houseChangeCommercialRent?><?=$categoriesChangeCommercialRent?><?=$datesChangeCommercialRent?></ul><div id="btn_block"><div class="btns-show"><div class="show"><div class="item list"><a data-pos="bottom" data-hover="tooltip" data-title="Список" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div><div class="item table<?if(isset($arrayLinkSearch['view']) && $arrayLinkSearch['view']=='table'){echo ' current';}?>"><a data-pos="bottom" data-hover="tooltip" data-title="Таблица" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div><div class="item map<?if(isset($arrayLinkSearch['view']) && $arrayLinkSearch['view']=='map'){echo ' current';}?>"><a data-pos="bottom" data-hover="tooltip" data-title="Карта" onclick="return chooseShow(this)" href="javascript:void(0)"></a></div></div></div></div></div>
	</form>
</div>
<?
// echo '<div>Время выполнения скрипта: '.(microtime(true) - $start).' сек.</div>';
?>