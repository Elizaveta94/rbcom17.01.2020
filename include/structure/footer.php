<div class="inner">
	<div class="left_block">
		<div class="bottom_menu">
			<?initialMenu('','','bottom');?>
		</div>
		<div class="copy">
			<?
				if(date("Y")==$year_created){
					$year = $year_created;
				}
				else {
					$year = $year_created." - ".date("Y");
				}
			?>
			<p><span>&copy; <?=$year?>,</span> Портал недвижимости <strong>&laquo;РБКом&raquo;</strong></p>
		</div>
	</div>
	<div class="right_block">
		<?
		/*
		*  Социальные кнопки v.1.0
		*/
		$socialSites = array();
		foreach($_MAINSET as $key => $value){
			if(in_array($key,$socialArray)){
				if(!empty($value)){
					array_push($socialSites,$key);
				}
			}
		}
		
		$socials = '';
		$socialsArray = array();
		$socialButtons = array();
		if(count($socialSites)>0){
			for($s=0; $s<count($socialSites); $s++){
				$socialsArray[$socialSites[$s]] = $_MAINSET[$socialSites[$s]];
			}
			$socials = '<ul>';
			foreach($socialsArray as $key => $value){
				$k_name = $key;
				$target = ' target="_blank"';
				if($key=='google_plus'){
					$k_name = 'google-plus';
				}
				if($key=='skype'){
					$value = 'skype:'.$value."?call";
					$target = '';
				}
				$socials .= '<li class="'.$k_name.'"><a'.$target.' href="'.$value.'"><i class="fa fa-'.$k_name.'" aria-hidden="true"></i></a></li>';
			}
			$socials .= '</ul>';
		}
		?>
		<div style="display:none" class="creator">
			<p>Веб дизайн, UI/UX - <img src="/images/ebm.png"></p>
			<p><a target="_blank" href="http://www.vasilev-dv.ru/">Создание сайта в спб</a></p>
		</div>
		<div class="social-buttons">
			<?=$socials?>
		</div>
	</div>
	<!--<div class="contacts">
		<div class="address"><?=$_MAINSET['address']?></div>
		<div class="phone">Тел.: <span>(<?=substr($_MAINSET['phone_clear'],3,3)?>)<strong><?=substr($_MAINSET['phone_clear'],7,3)?>-<?=substr($_MAINSET['phone_clear'],11,2)?><?=substr($_MAINSET['phone_clear'],14,2)?></strong></span></div>
		<div class="hours_work">с Пн. по Пт, с <span><?=$_MAINSET['mode_from']?></span> до <span><?=$_MAINSET['mode_to']?></span></div>
	</div>-->
</div>