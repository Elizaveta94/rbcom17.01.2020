<?php
header("content-type:text/html;charset=UTF-8");
require_once 'Classes/PHPExcel.php';
$objPHPExcel = new PHPExcel();

$objPHPExcel->setActiveSheetIndex(0);
$active_sheet = $objPHPExcel->getActiveSheet();

/* 
 * Считывает данные из любого excel файла и созадет из них массив.
 * $filename (строка) путь к файлу от корня сервера
 */
function parse_excel_file( $filename ){
	// подключаем библиотеку
	// require_once dirname(__FILE__) . '/PHPExcel/Classes/PHPExcel.php';

	$result = array();

	// получаем тип файла (xls, xlsx), чтобы правильно его обработать
	$file_type = PHPExcel_IOFactory::identify( $filename );
	// создаем объект для чтения
	$objReader = PHPExcel_IOFactory::createReader( $file_type );
	$objPHPExcel = $objReader->load( $filename ); // загружаем данные файла в объект
	$result = $objPHPExcel->getActiveSheet()->toArray(); // выгружаем данные из объекта в массив

	return $result;
}

function is_url($url) {
  return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
}

/*
*  Настройки для парсера из файла Excel
*/
$file = '301.xls';

$res = parse_excel_file($file);
for($c=1; $c<count($res); $c++){
	$link = trim($res[$c][0]);
	$linkPage = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	if(is_url($link)){
		if($linkPage==$link){
			header("HTTP/1.1 301 Moved Permanently");
			header("Location: /");
			exit;
		}
	}
}
?>