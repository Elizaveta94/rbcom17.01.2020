<?
/*
*  Страница для запуска парсера сайта lsr.ru
*/
header('Content-Type: text/html; charset=utf-8');
if($_SERVER['HTTP_HOST']=='localhost'){
	include('admin_bn/include/bd.php');
	include('admin_bn/include/functions.php');
}
else {
	include('admin_2/include/bd.php');
	include('admin_2/include/functions.php');
}

// $res = mysql_query("
	// SELECT *
	// FROM ".$template."_new_flats
	// WHERE p_main IN (193,4,22,28,30,50,52,63,73,81,108,163,189,193,196,197)
// ");
// if(mysql_num_rows($res)>0){
	// while($row = mysql_fetch_assoc($res)){
		// if($row['parser_type']==0){
			// $p_main = $row['id'];
			// mysql_query("
				// DELETE FROM ".$template."_new_flats
				// WHERE id='".$row['id']."'
			// ");
			// $photos = mysql_query("
				// SELECT images,id
				// FROM ".$template."_photo_catalogue
				// WHERE p_main='".$p_main."' && estate='new_flats'
			// ");
			// if(mysql_num_rows($photos)>0){
				// while($photo = mysql_fetch_assoc($photos)){
					// $images = $photo['images'];
					// $ex_images = explode(',',$photo['images']);
					// for($i=0; $i<count($ex_images); $i++){
						// unlink($_SERVER['DOCUMENT_ROOT'].'/admin_2/uploads/'.$ex_images[$i]);
						// mysql_query("
							// DELETE FROM ".$template."_photo_catalogue
							// WHERE id='".$photo['id']."'
						// ");
					// }
				// }
			// }
		// }
	// }
// }
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Парсер</title>
<link type="text/css" rel="stylesheet" href="/css/style.css?ver=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js?ver=<?=$client_version;?>"></script>
<script type="text/javascript">
$(document).ready(function(){
	var xhr;
	var xhr2;
	/*
	*  Загрузка информации и сохранение в файлы
	*/
	function parserLoad(rooms,count){
		var link = {
			"1" :"zap_bd.phtml?kkv1="+rooms+"&kkv2="+rooms+"&price1=&price2=&start="+count,
			"2" :"zap_fl.phtml?kkv1="+rooms+"&kkv2="+rooms+"&price1=&price2=&start="+count,
			"3" :"zap_rm.phtml?kkv1="+rooms+"&kkv2="+rooms+"&price1=&price2=&start="+count,
			"4" :"zap_ct.phtml?operation%5B%5D=1&su1=&su2=&price1=&price2=&operation%5B%5D=1&start="+count,  // загородная купить
			"5" :"zap_fdof.phtml?so1=&so2=&price1=&price2=&start="+count,// коммерческая
			"21":"zap_ar.phtml?kkv1="+rooms+"&kkv2="+rooms+"&price1=&price2=&type%5B%5D=1&start="+count,    // снять квартиру
			// "31":"zap_ar.phtml?kkv1="+rooms+"&kkv2="+rooms+"&price1=&price2=&type%5B%5D=1&start="+count,    // снять комнату
			"41":"zap_ct.phtml?operation%5B%5D=2&su1=&su2=&price1=&price2=&operation%5B%5D=2&start="+count, // загородная снять
		};// zap_bd - новостройки, zap_fl - квартиры, zap_rm - комнаты
		if(xhr && xhr.readyState != 4){
			xhr.abort();
		}
		$('#success').append('<tr class="loading"><td align="center" colspan="4">Загружаю...</td></tr>');
		xhr = $.ajax({
			type: "POST",
			url: "parser_bn.php",
			data: {"arrayLinks":link[<?=$_GET['type']?>],"page":count, "type":<?=$_GET['type']?>, "rooms":<?=$_GET['rooms']?>},
			dataType: "json",
			success: function(data) {
				var json = eval(data);
				if(data.html!=''){
					if(json.action=='success'){
						$('#success tr.loading').remove();
						$('#success').append('<tr class="item"> \
							<td style="width:120px">с '+count+' по '+(count+50)+'</td> \
							<td><strong>'+json.update.length+'</strong></td> \
							<td><strong>'+json.insert.length+'</strong></td> \
							<td class="foto_block"><strong></strong></td> \
						</tr> \
						');
						var fullArrayIds = [];
						for(var c=0; c<json.insert.length; c++){
							fullArrayIds.push(json.insert[c]);
						}
						for(var c=0; c<json.update.length; c++){
							fullArrayIds.push(json.update[c]);
						}
						if(json.finish==0){
							parserLoad(<?=$_GET['rooms']?>,count+50);
							// parserLoadImages(fullArrayIds,count);
						}
					}
				}
			}
		});
	}
	
	/*
	*  Загрузка изображений к объектам
	*/
	function parserLoadImages(ids,count){
		if(xhr2 && xhr2.readyState != 4){
			xhr2.abort();
		}
		var totalCount = ids.length;
		if(totalCount>0){
			var c=0;
			parserSendImages(c,ids,count,totalCount);
		}
	}
	
	/*
	*  Функция отправки данных для загрузки фото
	*/
	function parserSendImages(c,ids,count,totalCount){
		if(xhr2 && xhr2.readyState != 4){
			xhr2.abort();
		}
		xhr2 = $.ajax({
			type: "POST",
			url: "parser_bn.php",
			data: {"loadImages":"true","flats_id":ids[c]},
			dataType: "json",
			success: function(data) {
				var json = eval(data);
				if(data.html!=''){
					if(json.action=='success'){
						c++;
						$('#success tr.loading').remove();
						$('#success').find('td.foto_block:last strong').text(c);
						if(totalCount==c){
							parserLoad(<?=$_GET['rooms']?>,count+30);
						}
						else {
							parserSendImages(c,ids,count,totalCount);
						}
					}
				}
			}
		});
	}
	
	$('a.clickMe').click(function(){ // ?page=2&pageSize=30
		parserLoad(<?=$_GET['rooms']?>,0);
	});
});
</script>
</head>

<body>
<a class="clickMe" href="javascript:void(0)">Нажми на меня для парсинга</a>
<style type="text/css">
table.template {width:100%;margin:15px 0}
table.template td,table.template th {border:1px solid #cccccc;border-spacing:1px;font-size:13px;line-height:15px;padding:7px}
table.template td.first_tr {font-weight:bold;text-align:center;background:#f5f5f5}
table.template td {text-align:center}
table.template th {background:#f5f5f5}
</style>
<div id="content">
	<table style="width:500px" id="success" class="template">
		<tr>
			<th style="width:120px">Кол-во</th>
			<th>Обновил</th>
			<th>Добавил</th>
			<th>Фото</th>
		</tr>
	</table>
</div>
</body>
</html>